<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
use App\Http\Controllers\SuppliersController;

Route::get('/', function () {
    return redirect('/index');
});

Auth::routes();
Auth::routes(['verify' => true]);
Route::get('/exportdataexcel/{id?}', 'MwsInventoryController@show_new_calculated');
Route::get('/SalesInventoryExport/{id?}','MwsInventoryController@SalesInventoryExport');
Route::get('calculated-excel/{id}', 'HomeController@calculatedExcel');
Route::get('adjusted_inventory_export/{id}', 'HomeController@adjustedInventoryExport');

Route::get('logintoamazon','HomeController@logintoamazon');

Route::group(['middleware' => 'auth'], function() {
    Route::get('/index', 'HomeController@dashboard');
    Route::post('set_marketplace', 'HomeController@set_marketplace');
    Route::resource('users', 'UserController');
    Route::get('sub_user_create/{id}', 'UserController@sub_user_create');
    Route::get('display_sub_user/{userid}', 'UserController@display_sub_user');
    Route::get('display_access/{user_id}', 'UserController@display_access');
    Route::resource('roles', 'RoleController');

    Route::resource('setting', 'SettingController');
    Route::resource('blakoutedatesetting', 'BlakoutedateSettingController');
    Route::resource('modules', 'UserModuleController');
//    Route::resource('app-modules', 'AppModulesController');
    Route::resource('amazonsettings', 'AmazonsettingsController');
    Route::resource('inventory', 'InventoryController');
    Route::resource('inbound', 'InboundController');
    Route::resource('replenish', 'ReplenishController');

    Route::resource('report', 'ReportController');
    Route::resource('moduletable', 'ModuletableController');

    Route::resource('memberorder', 'MemberorderController')->middleware('checkType');
    Route::resource('orderreturns', 'MwsOrderReturnsController')->middleware('checkType');
    Route::resource('mws_excess_inventory', 'MwsexcesssinventoryController')->middleware('checkType');
    Route::resource('mws_inventory', 'MwsInventoryController')->middleware('checkType');
    Route::resource('mws_afn_inventory', 'MwsAfnInventoryController')->middleware('checkType');

    Route::resource('purchaseinstances', 'PurchaseInstancesController')->middleware('checkType');
    Route::post('createMore', 'PurchaseInstancesController@createMore');
    Route::resource('purchaseorder', 'PurchaseorderController')->middleware('checkType');
    Route::resource('defaultlogic', 'LogicController')->middleware('checkType');
    Route::get('reorder/{id?}','PurchaseorderController@daily_logic_calculations');

    Route::resource('vendors', 'VendorsController')->middleware('checkType');;
    Route::resource('vendorscontact', 'VendorsContactController')->middleware('checkType');
    Route::resource('vendorsetting', 'VendorsettingController')->middleware('checkType');
    Route::resource('vendor_blackoute_date', 'VendorBlackouteDateController')->middleware('checkType');
    Route::post('changeVendorContact','VendorsController@changeContact');
    Route::get('vendors_bulk_export', 'VendorsController@bulkExport')->name('vendors_bulk_export');
    Route::delete('vendors_bulk_delete', 'VendorsController@bulkDelete')->name('vendors_bulk_delete');
    Route::post('update_vendors_cols', 'VendorsController@updateCols')->name('update_vendors_cols');

    Route::resource('saleshistory', 'SalesHistoryController')->middleware('checkType');
    Route::resource('suppliers', 'SuppliersController')->middleware('checkType');
    Route::delete('suppliers_bulk_delete', 'SuppliersController@bulkDelete')->name('suppliers_bulk_delete');
    Route::get('suppliers_bulk_export', 'SuppliersController@bulkExport')->name('suppliers_bulk_export');
    Route::post('update_suppliers_cols', 'SuppliersController@updateCols')->name('update_suppliers_cols');

    Route::resource('supplierscontact', 'SuppliersContactController')->middleware('checkType');
    Route::resource('suppliersetting', 'SuppliersettingController')->middleware('checkType');
    Route::resource('supplier_blackoute_date', 'SupplierBlackouteDateController')->middleware('checkType');
    Route::post('changeSupplierContact','SuppliersController@changeContact');

    Route::resource('warehouses', 'WarehousesController')->middleware('checkType');
    Route::resource('warehoussetting', 'WarehousesettingController')->middleware('checkType');
    Route::post('changeWarehouseContact','WarehousesController@changeContact');
    Route::delete('warehouses_bulk_delete', 'WarehousesController@bulkDelete')->name('warehouses_bulk_delete');
    Route::get('warehouses_bulk_export', 'WarehousesController@bulkExport')->name('warehouses_bulk_export');
    Route::post('get_warehouses', 'WarehousesController@getData')->name('get_warehouses');
    Route::post('update_warehouses_cols', 'WarehousesController@updateCols')->name('update_warehouses_cols');

    Route::resource('supplyorderlogic','SupplyreorderlogicController')->middleware('checkType');
    Route::resource('fbasetting', 'FbasettingController')->middleware('checkType');

    Route::resource('affiliate_programs', 'AffiliateProgramController');
    Route::post('check_email_exists', 'AffiliateProgramController@check_email_exists');

    /** Products Page */

    Route::get('productsnew', 'MwsProductController@index');
    Route::get('search_product/{search}','MwsProductController@search');
    Route::post('filter_product','MwsProductController@filter');
    Route::delete('products_bulk_delete', function(){ return ['success' => true];})->name('products_bulk_delete');
    Route::post('get_products', function(){ return [];})->name('get_products');
    Route::post('product_add_label', 'MwsProductController@product_add_label')->name('product_add_label');
    Route::post('product_delete_label','MwsProductController@delete_bulk_label')->name('product_delete_label');
    Route::post('product_detail_save', 'MwsProductController@product_detail_save')->name('product_detail_save');
    Route::post('product_cell_change','MwsProductController@product_cell_change')->name('product_cell_change');

    Route::resource('products', 'MwsProductController')->middleware('checkType');
    //Route::get('products-data', 'MwsProductController@products_data');
    Route::get('products_warehouse_assign/{id}','MwsProductController@product_warehouse_assign');
    Route::get('products_sales_rate/{id}','MwsProductController@products_sales_rate');
    Route::post('product_calculation','MwsProductController@product_calculation');

    Route::post('get_data','MwsProductController@get_data');
    Route::get('product_details/{product_id}','MwsProductController@product_details');
    Route::post('crud_process','MwsProductController@crud_process');

    Route::post('import_suppliers', 'SuppliersController@ImportSupplier')->name('import_suppliers');
    Route::get('supplier_import_message', 'SuppliersController@supplier_import_message')->name('supplier_import_message');

    Route::post('import_vendors', 'VendorsController@ImportVendor')->name('import_vendors');
    Route::get('vendor_import_message', 'VendorsController@vendor_import_message')->name('vendor_import_message');

    Route::post('import_shippingagent', 'ShippingAgentController@ImportShippingagent')->name('import_shippingagent');
    Route::get('shippingagent_import_message', 'ShippingAgentController@shippingagent_import_message')->name('shippingagent_import_message');

    Route::post('import_warehouse', 'WarehousesController@ImportWarehouse')->name('import_warehouse');
    Route::get('warehouse_import_message', 'WarehousesController@warehouse_import_message')->name('warehouse_import_message');

    // Route::get('get_products_labels', function(){
    //     return [ 'Label1', 'Label2', 'Label3', 'Label4'];
    // })->name('get_products_labels');

    Route::get('get_products_labels', 'MwsProductController@get_products_labels')->name('get_products_labels');
    //Route::post('product_assign_to_label', 'MwsProductController@product_assign_to_label')->name('product_assign_to_label');
    //Route::get('product_assign_to_label/{label_id}/{product_id}', 'MwsProductController@product_assign_to_label')->name('product_assign_to_label');
    Route::get('get_labels_for_product', 'MwsProductController@get_labels_for_product')->name('get_labels_for_product');

    Route::get('get_sources','MwsProductController@get_sources')->name('get_sources');
    Route::get('apply_bulk_sources','MwsProductController@apply_bulk_sources')->name('apply_bulk_sources');
    Route::post('insert_update_sources','MwsProductController@insert_update_sources')->name('insert_update_sources');
    Route::post('products_bulk_label','MwsProductController@product_assign_to_label')->name('products_bulk_label');
   // Route::post('delete_bulk_label','MwsProductController@delete_bulk_label')->name('delete_bulk_label');

    // Route::post('products_bulk_label', function(Request $request){
    //     return [
    //         'labels' => [
    //             [
    //                 'slug' => 'label1',
    //                 'text' => 'Label 1'
    //             ],
    //             [
    //                 'slug' => 'label2',
    //                 'text' => 'Label 2'
    //             ],
    //             [
    //                 'slug' => 'label3',
    //                 'text' => 'Label 3'
    //             ],
    //             [
    //                 'slug' => 'label4',
    //                 'text' => 'Label 4'
    //             ]
    //         ]
    //     ];
    // })->name('products_bulk_label');

    Route::resource('shippingagent', 'ShippingAgentController')->middleware('checkType');
    Route::get('get_shipping_agent_list','ShippingAgentController@get_shipping_agent_list');
    Route::delete('shipping_agents_bulk_delete', 'ShippingAgentController@bulkDelete')->name('shipping_agents_bulk_delete');
    Route::get('shipping_agents_bulk_export', 'ShippingAgentController@bulkExport')->name('shipping_agents_bulk_export');
    Route::get('get_shipping_agents', 'ShippingAgentController@getData')->name('get_shipping_agents');
    Route::post('update_shipping_agents_cols', 'ShippingAgentController@updateCols')->name('update_shipping_agents_cols');

    Route::get('registertoamazon','AmazonsettingsController@registertoamazon');

    Route::get('logic', function(){ return view('logic.index');});
});
Route::get('/affiliate/{id?}', 'AffiliateProgramController@create_user');
Route::post('forgetpassword', 'SkoteController@forgetpassword');
Route::post('resetpassword', 'SkoteController@resetpassword');
Route::get('/auth/verify/{token}', 'Auth\RegisterController@verifyUser');
//Route::get('/exportdataexcel/{id?}', 'MwsInventoryController@show');

Route::get('dashboard', function(){
    return view('dashboard');
});

Route::post('suppliers_filter', function(){ return []; })->name('suppliers.filter');
Route::post('vendors_filter', function(){  return []; })->name('vendors.filter');
Route::post('warehoused_filter', function(){ return [];})->name('warehouses.filter');
Route::post('shipping_agents_filter', function(){ return [];})->name('shipping_agents.filter');

Route::post('products_filter', function(){ return [];})->name('products.filter');

Route::post('get_suppliers', 'SuppliersController@getData')->name('get_suppliers');
Route::post('get_vendors', 'VendorsController@getData')->name('get_vendors');

Route::get('countries', function(){
    return get_country_list();
})->name('countries');


<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('api_token')->post('/inventory','ApiController@inventory', function (Request $request) {
    return $request->user();
});
Route::middleware('api_token')->post('/inbound_breakdown','ApiController@inbound_breakdown', function (Request $request) {
    return $request->user();
});
Route::middleware('api_token')->post('/sales_history','ApiController@sales_history', function (Request $request) {
    return $request->user();
});
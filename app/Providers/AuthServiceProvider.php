<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('view', function ($user, $userpermission) {
            if($userpermission->view == 1){
                return true;
            }else {
                return false;
            }
        });
        Gate::define('create', function ($user, $userpermission) {
            if($userpermission->create == 1){
                return true;
            }else {
                return false;
            }
        });
        Gate::define('edit', function ($user, $userpermission) {
            if($userpermission->edit == 1){
                return true;
            }else {
                return false;
            }
        });
        Gate::define('delete', function ($user, $userpermission) {
            if($userpermission->delete == 1){
                return true;
            }else {
                return false;
            }
        });
        Gate::define('access', function ($user, $userpermission) {
            if($userpermission->access == 1){
                return true;
            }else {
                return false;
            }
        });
    }
}

<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Auth;
use Illuminate\Support\Facades\Session;
use App\Models\Warehouse;
use App\Models\Field_list;
use App\Models\Warehouse_users;
use App\Models\Warehouse_wise_users;
use App\Models\Warehouse_contacts;
use App\Models\User_field_access;
use App\Models\Warehouse_wise_default_setting;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use DB;
use DateTime;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;



class WarehouseImport implements ToCollection, WithHeadingRow, WithMultipleSheets, WithCalculatedFormulas
{
    public function sheets(): array
    {
        return [
            0 => $this,
        ];
    }

	public function collection(Collection $collection){
        $success_msg_array=[];
        $error_msg_array=[];
        $count = 0;
        $success_count = 0;
        $error_count = 0;
        $total_count = 0;
		foreach ($collection as $item)
        { 
            $count ++;
            $type = $item['type'] == 'self' ? 1 : 2;

            //if( (!empty($item['warehouse_name'])) || (empty($item['warehouse_name']) && !empty($warehouse_id)) ) {
            if( (!empty($item['type']) && !empty($item['warehouse_name']) && !empty($item['country']) && !empty($item['state']) && !empty($item['address']) && !empty($item['postal_code']))) 
            {
                $total_count = $total_count + 1;
    			$warehouse_exist=Warehouse::where('warehouse_name',$item['warehouse_name'])->first();
                if($warehouse_exist){
                    $error_msg_array[]=array('warehouse'=>($warehouse_exist ? $warehouse_exist->warehouse_name : 0),'message'=>'Warehouse already exist');
                    $error_count = $error_count+1;
                    continue;
                }

                $countryData = get_country_list();
                $country='';
                if(in_array($item['country'], $countryData)){
                    $country=array_search($item['country'],$countryData);
                }
                if($country==''){
                    $error_msg_array[]=array('warehouse'=>($warehouse_exist ? $warehouse_exist->id : 0),'message'=>"Country-".$item['country']." not found");
                    $error_count = $error_count+1;
                    continue;
                }

                $stateData=get_state_list($country);
                $state='';
                if(in_array($item['state'], $stateData, true)) {
                    $state=array_search($item['state'],$stateData);
                } 
                if($state==''){
                    $error_msg_array[]=array('warehouse'=>($warehouse_exist ? $warehouse_exist->id : 0),'message'=>"State-".$item['state']." not found");
                    $error_count = $error_count+1;
                    continue;
                }

                
                    if($warehouse_exist){
                        $warehouse_id = $warehouse_exist->id;
                        $warehouse_name = $warehouse_exist->warehouse_name;
                        $req_data = [
                            'type' => $type,
                            'warehouse_name' => $item['warehouse_name'],
                            'user_marketplace_id' => session('MARKETPLACE_ID'), //todo
                            'country_id' => $country,
                            'address_line_1' => $item['address'],                
                            'state' => $state,
                            'zipcode' => $item['postal_code'],
                        ];
                        // dump('if',$req_data);
                        $datas_of = Warehouse::findOrFail($warehouse_id);
                        $datas_of->update($req_data);
                        $success_count = $success_count+1;
                    }else{
                        $req_data = [
                            'type' => $type,
                            'warehouse_name' => $item['warehouse_name'],
                            'user_marketplace_id' => session('MARKETPLACE_ID'), //todo
                            'country_id' => $country,
                            'address_line_1' => $item['address'],                
                            'state' => $state,
                            'zipcode' => $item['postal_code'],
                        ];
                        // dump('else',$req_data);
                        /*DB::enableQueryLog();
                        $datas_of = Supplier::create($req_data)->toSql();
                        $query = DB::getQueryLog();
                        print_r($query);exit;*/
                        $datas_of = Warehouse::create($req_data);
                        $userLastInsertId = $datas_of->toArray();
                        $warehouse_id = $datas_of['id'];
                        $warehouse_name = $datas_of['warehouse_name'];
                        $success_count = $success_count+1;
                    }
            
                    
                if($type == 1){
                    if(!empty($item['user_manage'])){ 
                        $list = explode(',', $item['user_manage']);
                        if(!empty($list)){
                            foreach($list as $val){
                                $get_user_id = Warehouse_users::where(array('user_marketplace_id'=>session('MARKETPLACE_ID'),'full_name'=>trim($val)))->first();
                                if(!empty($get_user_id)){
                                    $req_data = [
                                        'warehouse_id' => $warehouse_id,
                                        'warehouse_user_id' => $get_user_id->id,
                                    ];
                                    Warehouse_wise_users::create($req_data);
                                }
                            }
                        }  
                    }
                } else {
                    if($item['contact_fname'] && !empty($item['contact_fname'])){   
                        $emailval = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/';
                        $mob="/^[1-9][0-9]*$/"; 

                        if($item['contact_email'] != '' && !filter_var(trim($item['contact_email']), FILTER_VALIDATE_EMAIL)) {

                            $error_msg_array[]=array('warehouse'=>$item['warehouse_name'],'message'=>''.$item['contact_email'].' invalid email adddress');
                            $error_count = $error_count + 1;
                            continue;

                        } if($item['contact_phone'] != '' && !preg_match($mob, $item['contact_phone'])) {

                            $error_msg_array[]=array('warehouse'=>$item['warehouse_name'],'message'=>'Invalid phone number');
                            continue;
                        }
                        
                        $warehouse_id = isset($warehouse_id) && $warehouse_id != '' ? $warehouse_id : '';
                        $req_data = [
                            'first_name' => $item['contact_fname'],
                            'last_name' => $item['contact_lname'],
                            'title' => $item['contact_title'],
                            'email' => $item['contact_email'],
                            'mobile' => $item['contact_phone'],
                            'primary' => ($item['primary'] ?? 0),
                            'warehouse_id' =>$warehouse_id,
                        ];
        
                        $check_email = Warehouse_contacts::where(array('email' => $item['contact_email'], 'warehouse_id' => $warehouse_id))->first();
                        if (empty($check_email)) {
                            $datas_of = Warehouse_contacts::create($req_data);
                            $userLastInsertId = $datas_of->toArray();
                            if ($userLastInsertId != '') {
                                $warehouseContactID = $userLastInsertId['id'];
                            }
                        } else {
                            $warehouseContact = Warehouse_contacts::findOrFail($check_email['id']);
                            $warehouseContact->update($req_data);
                            $warehouseContactID = $check_email['id'];
                        }
                    }
                }
            

            
                /*Order Settings*/
                if($item['leadtime'] != ''){
                    if(!empty($item['warehouse_name'])){
                        $check_exists = Warehouse_wise_default_setting::where(array('warehouse_id' => $warehouse_id, 'lead_time' => $item['leadtime']))->first();
                        if (!empty($check_exists)) {
                            $req_data = [
                                'warehouse_id' => $warehouse_id,
                                'lead_time' => $item['leadtime']
                            ];
                            Warehouse_wise_default_setting::where(array('id' => $check_exists->id))->update($req_data);
                        }else{
                            $req_data = [
                                'warehouse_id' => $warehouse_id,
                                'lead_time' => $item['leadtime']
                            ];
                            $lead_data = Warehouse_wise_default_setting::create($req_data);
                        }
                    }
                }
        
            }else{
                if($type == 2 && $item['warehouse_name'] == '' && $item['country'] == '' && $item['state'] == '' && $item['address'] == '' && empty($item['postal_code'])){
                    if($item['contact_fname'] && !empty($item['contact_fname'])){   
                        $emailval = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/';
                        $mob="/^[1-9][0-9]*$/"; 

                        if($item['contact_email'] != '' && !filter_var(trim($item['contact_email']), FILTER_VALIDATE_EMAIL)) {

                            $error_msg_array[]=array('warehouse'=>$item['warehouse_name'],'message'=>''.$item['contact_email'].' invalid email adddress');
                            continue;

                        } if($item['contact_phone'] != '' && !preg_match($mob, $item['contact_phone'])) {

                            $error_msg_array[]=array('warehouse'=>$item['warehouse_name'],'message'=>'Invalid phone number');
                            continue;
                        }
                        $warehouse_id = isset($warehouse_id) && $warehouse_id != '' ? $warehouse_id : '';
                        $req_data = [
                            'first_name' => $item['contact_fname'],
                            'last_name' => $item['contact_lname'],
                            'title' => $item['contact_title'],
                            'email' => $item['contact_email'],
                            'mobile' => $item['contact_phone'],
                            'primary' => ($item['primary'] ?? 0),
                            'warehouse_id' => $warehouse_id,
                        ];
        
                        $check_email = Warehouse_contacts::where(array('email' => $item['contact_email'], 'warehouse_id' => $warehouse_id))->first();
                        if (empty($check_email)) {
                            $datas_of = Warehouse_contacts::create($req_data);
                            $userLastInsertId = $datas_of->toArray();
                            if ($userLastInsertId != '') {
                                $warehouseContactID = $userLastInsertId['id'];
                            }
                        } else {
                            $warehouseContact = Warehouse_contacts::findOrFail($check_email['id']);
                            $warehouseContact->update($req_data);
                            $warehouseContactID = $check_email['id'];
                        }
                    }
                }
                if( !empty($item['type']) || !empty($item['warehouse_name']) || !empty($item['country']) || !empty($item['state']) || !empty($item['address']) || !empty($item['postal_code']) ) {
                    if(!empty($item['warehouse_name'])){
                        $errorname = $item['warehouse_name'];
                    }else{
                        $tcount = $count+1;
                        $errorname = 'Excel sheet line no '. $tcount ;
                    }
                    $error_msg_array[]=array('warehouse'=>$errorname,'message'=>'All compulsory fields must be required');
                    $error_count = $error_count+1;
                    continue;   
                }
            }
    
	    }
        Session::put('total_count', $total_count);
        Session::put('error_count', $error_count);
        Session::put('success_count', $success_count);
    if(isset($error_msg_array) && $error_msg_array!=null){
            \Session::put('error_msg_array', $error_msg_array);
        }else{
            \Session::put('success_message', "Warehouse saved successfully.");
        }
        return;
    }
}
<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Auth;
use Illuminate\Support\Facades\Session;
use App\Models\ShippingAgent;
use App\Models\Country;
use App\Models\Field_list;
use DB;
use DateTime;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;



class ShippingagentImport implements ToCollection, WithHeadingRow, WithMultipleSheets, WithCalculatedFormulas
{
    public function sheets(): array
    {
        return [
            0 => $this,
        ];
    }

	public function collection(Collection $collection){
        $success_msg_array=[];
        $error_msg_array=[];
        $count = 0;
        $success_count = 0;
        $error_count = 0;
        $total_count = 0;
		foreach ($collection as  $item)
        { 
            $count ++;
            if( !empty($item['companyname']) && !empty($item['email']) && !empty($item['firstname']) && !empty($item['lastname']) && !empty($item['phone']) ) 
            {
                $total_count = $total_count + 1;
                    $shipping_exists=ShippingAgent::where('company_name',$item['companyname'])->first();
                    if($shipping_exists){
                        $error_msg_array[]=array('companyname'=>($shipping_exists ? $shipping_exists->company_name : 0),'message'=>'company already exist');
                        $error_count = $error_count+1;
                        continue;
                    }
                    $shipping_exists=ShippingAgent::where('email',$item['email'])->first();
                    if($shipping_exists){
                        $error_msg_array[]=array('companyname'=>($shipping_exists ? $shipping_exists->email : 0),'message'=>'email already exist');
                        $error_count = $error_count+1;
                        continue;
                    }
                    $emailval = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/';
                        $mob="/^[1-9][0-9]*$/"; 

                        if($item['email'] != '' && !filter_var(trim($item['email']), FILTER_VALIDATE_EMAIL)) {

                            $error_msg_array[]=array('companyname'=>$item['email'],'message'=>''.$item['email'].' invalid email adddress');
                            $error_count = $error_count+1;
                            continue;

                        } if($item['phone'] != '' && !preg_match($mob, $item['phone'])) {

                            $error_msg_array[]=array('companyname'=>$item['phone'],'message'=>'Invalid phone number');
                            $error_count = $error_count+1;
                            continue;
                        }
                        
                    if($shipping_exists){
                        $shipping_id = $shipping_exists->id;
                        $company_name = $shipping_exists->company_name;
                        $email = $shipping_exists->email;
                        $req_data = [
                            'company_name' => $item['companyname'],
                            'user_marketplace_id' => session('MARKETPLACE_ID'), //todo
                            'first_name' => $item['firstname'],
                            'last_name' => $item['lastname'],         
                            'email' => $item['email'],
                            'phone' => $item['phone'],
                        ];
                        // dump('if',$req_data);
                        $datas_of = ShippingAgent::findOrFail($shipping_id);
                        $datas_of->update($req_data);
                        $success_count = $success_count+1;
                    }else{
                        $req_data = [
                            'company_name' => $item['companyname'],
                            'user_marketplace_id' => session('MARKETPLACE_ID'), //todo
                            'first_name' => $item['firstname'],
                            'last_name' => $item['lastname'],         
                            'email' => $item['email'],
                            'phone' => $item['phone'],
                        ];
                        // dump('else',$req_data);
                        /*DB::enableQueryLog();
                        $datas_of = Supplier::create($req_data)->toSql();
                        $query = DB::getQueryLog();
                        print_r($query);exit;*/
                        $datas_of = ShippingAgent::create($req_data);
                        $userLastInsertId = $datas_of->toArray();
                        $shipping_id = $datas_of['id'];
                        $company_name = $datas_of['company_name'];
                        $success_count = $success_count+1;
                    }
                    // dd($supplier_id);
                //}
            }else{
                if( !empty($item['companyname']) || !empty($item['email']) || !empty($item['firstname']) || !empty($item['lastname']) || !empty($item['phone']) ) {
                    if(!empty($item['companyname'])){
                        $errorname = $item['companyname'];
                    }else if(!empty($item['email'])){
                        $errorname = $item['email'];
                    }else{
                        $errorname = 'Record No '. $count ;
                    }
                    $error_msg_array[]=array('companyname'=>$errorname,'message'=>'All compulsory fields must be required');
                    $error_count = $error_count+1;
                    continue;   
                }
            }
        // dd($error_msg_array);
        // dump($supplier_id);
        
        

	}//dd($error_msg_array);
    Session::put('total_count', $total_count);
    Session::put('error_count', $error_count);
    Session::put('success_count', $success_count);
        if(isset($error_msg_array) && $error_msg_array!=null){
            //$total_message['error_message'] = $error_msg_array; 
            // $error_msg_array=implode(' ',$error_msg_array);
            // \Session::flash('message', "Supplier saved successfully.");
            //dd($error_msg_array);
            \Session::put('error_msg_array', $error_msg_array);
        }else{
            //$total_message['success_message'] = $error_msg_array;
            \Session::put('success_message', "Shipping agent saved successfully.");
        }
        return;
    }
}
<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Auth;
use Illuminate\Support\Facades\Session;
use App\Models\Vendor;
use App\Models\Vendor_contact;
use App\Models\Vendor_default_setting;
use App\Models\Vendor_wise_default_setting;
use App\Models\Vendor_blackout_date;
use App\Models\Country;
use App\Models\Payment_details;
use App\Models\Payment;
use App\Models\Payment_terms_type;
use App\Models\Lead_time;
use App\Models\Lead_time_check;
use App\Models\Lead_time_type;
use App\Models\Lead_time_value;
use App\Models\Order_volume_type;
use App\Models\Reorder_schedule_detail;
use App\Models\Reorder_schedule_type;
use App\Models\Shipping;
use App\Models\ShippingAgent;
use App\Models\Shippingair;
use App\Models\Shippingcarrier;
use App\Models\ShippingContainerSettings;
use App\Models\Shippingcutoffdeliverytime;
use App\Models\ShippingDeliveryDetails;

use App\Models\ShipToWarehouse;
use App\Models\Warehouse;
use DB;
use DateTime;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;



class VendorImport implements ToCollection, WithHeadingRow
{
    public function sheets(): array
    {
        return [
            0 => $this,
        ];
    }

	public function collection(Collection $collection){
        $success_msg_array=[];
        $error_msg_array=[];
        $count = 0;
        $success_count = 0;
        $error_count = 0;
        $total_count = 0;
        //dd($collection);
		foreach ($collection as $item)
        { 
            $count ++;
            if(!empty($item['type']) && !empty($item['vendor_name']) && !empty($item['country']) && !empty($item['state']) && !empty($item['address']) && !empty($item['postal_code']))
            {
                $total_count = $total_count + 1;
                $vendor_type =  $item['type'] == 'Domestic' ? 1 : 2 ; 
                /*Vendor Basic Details*/
    			$vendor_exist=Vendor::where('vendor_name',$item['vendor_name'])->first();
                if($vendor_exist){
                    $error_msg_array[]=array('vendor_name'=>($vendor_exist ? $vendor_exist->vendor_name : 0),'message'=>'Vendor already exist');
                    continue;
                }
                
                $countryData = get_country_list();
                $country='';
                if(in_array($item['country'], $countryData)){
                    $country=array_search($item['country'],$countryData);
                }
                if($country==''){
                    $error_msg_array[]=array('vendor_name'=>($vendor_exist ? $vendor_exist->id : 0),'message'=>"Country-".$item['country']." not found");
                    continue;
                }

                $stateData=get_state_list($country);
                $state='';
                if(in_array($item['state'], $stateData, true)) {
                    $state=array_search($item['state'],$stateData);
                } 
                if($state==''){
                    $error_msg_array[]=array('vendor_name'=>($vendor_exist ? $vendor_exist->id : 0),'message'=>"State-".$item['state']." not found");
                    continue;
                }

    			if($vendor_exist){
    				$vendor_id = $vendor_exist->id;
                    $vendor_name = $vendor_exist->vendor_name;
                    $req_data = [
                        'vendor_type'=>$vendor_type,
                        'vendor_name' => $item['vendor_name'],
                        'user_marketplace_id' => session('MARKETPLACE_ID'), //todo
                        'country_id' => $country,
                        'address_line_1' => $item['address'],                
                        'state' => $state,
                        'zipcode' => $item['postal_code'],
                    ];
                    $datas_of = Vendor::findOrFail($vendor_id);
                    $datas_of->update($req_data);
    			}else{
    				$req_data = [
                        'vendor_type'=>$vendor_type,
    	                'vendor_name' => $item['vendor_name'],
    	                'user_marketplace_id' => session('MARKETPLACE_ID'), //todo
    	                'country_id' => $country,
    	                'address_line_1' => $item['address'],                
    	                'state' => $state,
    	                'zipcode' => $item['postal_code'],
    	            ];
    	            $datas_of = Vendor::create($req_data);
    	            $userLastInsertId = $datas_of->toArray();
    	            $vendor_id = $datas_of['id'];
                    $vendor_name = $datas_of['vendor_name'];
    	        }
                  // dd($supplier_id);
            
                // dd($error_msg_array,$item['supplier_name']);
                // if(isset($supplier_id)){
                //     dump($supplier_id);
                // }
                /*Contact Details*/
                if($item['contact_fname'] && !empty($item['contact_fname'])){  
                    
                    $emailval = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/';
                    $mob="/^[1-9][0-9]*$/"; 

                    if($item['contact_email'] != '' && !filter_var(trim($item['contact_email']), FILTER_VALIDATE_EMAIL)) {

                        $error_msg_array[]=array('vendor_name'=>$item['vendor_name'],'message'=>''.$item['contact_email'].' invalid email adddress');
                        continue;

                    } if($item['contact_phone'] != '' && !preg_match($mob, $item['contact_phone'])) {

                        $error_msg_array[]=array('vendor_name'=>$item['vendor_name'],'message'=>'Invalid phone number');
                        continue;
                    }
                    
                    $vendor_id = isset($vendor_id) && $vendor_id != '' ? $vendor_id : '';

                    $req_data = [
                        'first_name' => $item['contact_fname'],
                        'last_name' => $item['contact_lname'],
                        'title' => $item['contact_title'],
                        'email' => $item['contact_email'],
                        'phone_number' => $item['contact_phone'],
                        'primary' => ($item['primary'] ?? 0),
                        'vendor_id' => $vendor_id,
                    ];

                    $check_email = Vendor_contact::where(array('email' => $item['contact_email'], 'vendor_id' => $vendor_id))->first();
                    if (empty($check_email)) {
                        $datas_of = Vendor_contact::create($req_data);
                        $userLastInsertId = $datas_of->toArray();
                        if ($userLastInsertId != '') {
                            $vendorContactID = $userLastInsertId['id'];
                        }
                    } else {
                        $vendorContact = Vendor_contact::findOrFail($check_email['id']);
                        $vendorContact->update($req_data);
                        $vendorContactID = $check_email['id'];
                    }
                }

                $check_exists = Lead_time::where(array('supplier_vendor' => env('VENDOR'), 'supplier_vendor_id' => $vendor_id))->first();
                if($vendor_type == 1){
                    $types = 0;
                }else{
                    $types = strtolower($item['transit_type']) == 'boat' ? 1 : 2 ;
                }
                if(!empty($item['vendor_name'])){
                    $req_data = [
                        'supplier_vendor' => env('VENDOR'),
                        'supplier_vendor_id' => $vendor_id,
                        'product_manuf_days' => $item['production'],
                        'to_port_days' => $item['to_port'],
                        'transit_time_days' => $item['transit_time'],
                        'to_warehouse_days' => $item['to_warehouse'],
                        'to_amazon' => $item['to_amazon'],
                        'po_to_production_days' => $item['po_to_production'],
                        'safety_days' => $item['safety_days'],
                        'total_lead_time_days' => $item['total_lead_time'],
                        'order_prep_days' => $item['order_prep'], //todo
                        'po_to_prep_days' => $item['po_to_prep'], //todo
                        'type' => $types,
                    ];
                    
                    if (!empty($check_exists)) {
                        Lead_time::where(array('id' => $check_exists->id))->update($req_data);
                        $lead_data_id=$check_exists->id;
                    } else {
                        $lead_data = Lead_time::create($req_data);
                        $lead_data_id=$lead_data->id;
                    }
                }else{
                    $lead_data_id=$check_exists->id;
                }

                if($lead_data_id>0){
                    if($vendor_type != 1)
                    {
                    /*production_started*/
                        if($item['contact_confirm_production_started'] && !empty($item['contact_confirm_production_started'])){
                            if(empty($item['notify_confirm_production_started']) && !empty($item['contact_confirm_production_started'])){
                                $lead_time_value=Lead_time_value::where( array('lead_time_id' => $lead_data_id, 'lead_time_detail_id'=>'1') )->first();
                                $lead_time_value_data = [
                                    'lead_time_id' => $lead_data_id,
                                    'lead_time_detail_id' => '1',
                                    'send_mail' => $lead_time_value->send_mail,
                                    'no_of_days' => $lead_time_value->no_of_days,
                                    'contact_detail' => $item['contact_confirm_production_started'],
                                ];
                                // dump('contact_confirm_production_started_if',$lead_time_value_data);
                                Lead_time_value::create($lead_time_value_data);
                            }else{
                                Lead_time_value::where(array('lead_time_id' => $lead_data_id))->delete();
                                $lead_time_value_data = [
                                    'lead_time_id' => $lead_data_id,
                                    'lead_time_detail_id' => '1',
                                    'send_mail' => $item['notify_confirm_production_started'],
                                    'no_of_days' => $item['x_days_confirm_production_started'],
                                    'contact_detail' => $item['contact_confirm_production_started'],
                                ];
                                // dump('contact_confirm_production_started_else',$lead_time_value_data);
                                Lead_time_value::create($lead_time_value_data);
                            }
                        }

                        /*production_ended*/
                        if($item['contact_confirm_production_end'] && !empty($item['contact_confirm_production_end'])){
                            if(empty($item['notify_confirm_production_end']) && !empty($item['contact_confirm_production_end'])){
                                $lead_time_value=Lead_time_value::where( array('lead_time_id' => $lead_data_id, 'lead_time_detail_id'=>'2') )->first();
                                $lead_time_value_data = [
                                    'lead_time_id' => $lead_data_id,
                                    'lead_time_detail_id' => '2',
                                    'send_mail' => $lead_time_value->send_mail,
                                    'no_of_days' => $lead_time_value->no_of_days,
                                    'contact_detail' => $item['contact_confirm_production_end'],
                                ];
                                // dump('contact_confirm_production_end_if',$lead_time_value_data);
                                Lead_time_value::create($lead_time_value_data);
                            }else{
                                $lead_time_value_data = [
                                    'lead_time_id' => $lead_data_id,
                                    'lead_time_detail_id' => '2',
                                    'send_mail' => $item['notify_confirm_production_end'],
                                    'no_of_days' => $item['x_days_confirm_production_end'],
                                    'contact_detail' => $item['contact_confirm_production_end'], ///based on leade_time_id nd detail_id
                                ];
                                // dump('contact_confirm_production_end_else',$lead_time_value_data);
                                Lead_time_value::create($lead_time_value_data);
                            }
                        }

                        /*port departure*/
                        if($item['contact_to_port'] && !empty($item['contact_to_port'])){
                            if(empty($item['notify_to_port']) && !empty($item['contact_to_port'])){
                                $lead_time_value=Lead_time_value::where( array('lead_time_id' => $lead_data_id, 'lead_time_detail_id'=>'3') )->first();
                                $lead_time_value_data = [
                                    'lead_time_id' => $lead_data_id,
                                    'lead_time_detail_id' => '3',
                                    'send_mail' => $lead_time_value->send_mail,
                                    'no_of_days' => $lead_time_value->no_of_days,
                                    'contact_detail' => $item['contact_to_port'],
                                ];
                                // dump('contact_to_port_if',$lead_time_value_data);
                                Lead_time_value::create($lead_time_value_data);
                            }else{
                                $lead_time_value_data = [
                                    'lead_time_id' => $lead_data_id,
                                    'lead_time_detail_id' => '3',
                                    'send_mail' => $item['notify_to_port'],
                                    'no_of_days' => 0,
                                    'contact_detail' => $item['contact_to_port'],
                                ];
                                // dump('contact_to_port_else',$lead_time_value_data);
                                Lead_time_value::create($lead_time_value_data);
                            }
                        }

                        /*port transit*/
                        if($item['contact_in_transit'] && !empty($item['contact_in_transit'])){
                            if(empty($item['notify_in_transit']) && !empty($item['contact_in_transit'])){
                                $lead_time_value=Lead_time_value::where( array('lead_time_id' => $lead_data_id, 'lead_time_detail_id'=>'4') )->first();
                                $lead_time_value_data = [
                                    'lead_time_id' => $lead_data_id,
                                    'lead_time_detail_id' => '4',
                                    'send_mail' => $lead_time_value->send_mail,
                                    'no_of_days' => $lead_time_value->no_of_days,
                                    'contact_detail' => $item['contact_in_transit'],
                                ];
                                // dump('contact_in_transit_if',$lead_time_value_data);
                                Lead_time_value::create($lead_time_value_data);
                            }else{
                                $lead_time_value_data = [
                                    'lead_time_id' => $lead_data_id,
                                    'lead_time_detail_id' => '4',
                                    'send_mail' => $item['notify_in_transit'],
                                    'no_of_days' => 0,
                                    'contact_detail' => $item['contact_in_transit'],
                                ];
                                // dump('contact_in_transit_else',$lead_time_value_data);
                                Lead_time_value::create($lead_time_value_data);
                            }
                        }

                        /*port arrival*/
                        if($item['contact_port_arrival'] && !empty($item['contact_port_arrival'])){
                            if(empty($item['notify_port_arrival']) && !empty($item['contact_port_arrival'])){
                                $lead_time_value=Lead_time_value::where( array('lead_time_id' => $lead_data_id, 'lead_time_detail_id'=>'5') )->first();
                                $lead_time_value_data = [
                                    'lead_time_id' => $lead_data_id,
                                    'lead_time_detail_id' => '5',
                                    'send_mail' => $lead_time_value->send_mail,
                                    'no_of_days' => $lead_time_value->no_of_days,
                                    'contact_detail' => $item['contact_port_arrival'],
                                ];
                                // dump('contact_port_arrival_if',$lead_time_value_data);
                                Lead_time_value::create($lead_time_value_data);
                            }else{
                                $lead_time_value_data = [
                                    'lead_time_id' => $lead_data_id,
                                    'lead_time_detail_id' => '5',
                                    'send_mail' => $item['notify_port_arrival'],
                                    'no_of_days' => 0,
                                    'contact_detail' => $item['contact_port_arrival'],
                                ];
                                // dump('contact_port_arrival_else',$lead_time_value_data);
                                Lead_time_value::create($lead_time_value_data);
                            }
                        }

                        /*warehouse*/
                        if($item['contact_to_warehouse'] && !empty($item['contact_to_warehouse'])){
                            if(empty($item['notify_to_warehouse']) && !empty($item['contact_to_warehouse'])){
                                $lead_time_value=Lead_time_value::where( array('lead_time_id' => $lead_data_id, 'lead_time_detail_id'=>'6') )->first();
                                $lead_time_value_data = [
                                    'lead_time_id' => $lead_data_id,
                                    'lead_time_detail_id' => '6',
                                    'send_mail' => $lead_time_value->send_mail,
                                    'no_of_days' => $lead_time_value->no_of_days,
                                    'contact_detail' => $item['contact_to_warehouse'],
                                ];
                                // dump('contact_to_warehouse_oif',$lead_time_value_data);
                                Lead_time_value::create($lead_time_value_data);
                            }else{
                                $lead_time_value_data = [
                                    'lead_time_id' => $lead_data_id,
                                    'lead_time_detail_id' => '6',
                                    'send_mail' => $item['notify_to_warehouse'],
                                    'no_of_days' => 0,
                                    'contact_detail' => $item['contact_to_warehouse'],
                                ];
                                // dump('contact_to_warehouse_els',$lead_time_value_data);
                                Lead_time_value::create($lead_time_value_data);
                            }
                        }

                        /*warehouse receipt*/
                        if( $item['contact_warehouse_receipt'] && !empty( $item['contact_warehouse_receipt'])){
                            if(empty($item['notify_warehouse_receipt']) && !empty($item['contact_warehouse_receipt'])){
                                $lead_time_value=Lead_time_value::where( array('lead_time_id' => $lead_data_id, 'lead_time_detail_id'=>'7') )->first();
                                $lead_time_value_data = [
                                    'lead_time_id' => $lead_data_id,
                                    'lead_time_detail_id' => '7',
                                    'send_mail' => $lead_time_value->send_mail,
                                    'no_of_days' => $lead_time_value->no_of_days,
                                    'contact_detail' => $item['contact_warehouse_receipt'],
                                ];
                                // dump('contact_warehouse_receipt_if',$lead_time_value_data);
                                Lead_time_value::create($lead_time_value_data);
                            }else{
                                $lead_time_value_data = [
                                    'lead_time_id' => $lead_data_id,
                                    'lead_time_detail_id' => '7',
                                    'send_mail' => $item['notify_warehouse_receipt'],
                                    'no_of_days' => 0,
                                    'contact_detail' => $item['contact_warehouse_receipt'],
                                ];
                                // dump('contact_warehouse_receipt_els',$lead_time_value_data);
                                Lead_time_value::create($lead_time_value_data);
                            }
                        }
                    }else{
                        if( $item['contact_domestic_order_prep'] && !empty( $item['contact_domestic_order_prep'])){
                            if(empty($item['notify_domestic_order_prep']) && !empty($item['contact_domestic_order_prep'])){
                                $lead_time_value=Lead_time_value::where( array('lead_time_id' => $lead_data_id, 'lead_time_detail_id'=>'8') )->first();
                                $lead_time_value_data = [
                                    'lead_time_id' => $lead_data_id,
                                    'lead_time_detail_id' => '8',
                                    'send_mail' => $lead_time_value->send_mail,
                                    'no_of_days' => $lead_time_value->no_of_days,
                                    'contact_detail' => $item['contact_domestic_order_prep'],
                                ];
                                // dump('contact_warehouse_receipt_if',$lead_time_value_data);
                                Lead_time_value::create($lead_time_value_data);
                            }else{
                                $lead_time_value_data = [
                                    'lead_time_id' => $lead_data_id,
                                    'lead_time_detail_id' => '8',
                                    'send_mail' => $item['notify_domestic_order_prep'],
                                    'no_of_days' => $item['x_days_domestic_order_prep'],
                                    'contact_detail' => $item['contact_domestic_order_prep'],
                                ];
                                // dump('contact_warehouse_receipt_els',$lead_time_value_data);
                                Lead_time_value::create($lead_time_value_data);
                            }
                        }

                        if( $item['contact_domestic_ship_confirmation'] && !empty( $item['contact_domestic_ship_confirmation'])){
                            if(empty($item['notify_domestic_ship_confirmation']) && !empty($item['contact_domestic_ship_confirmation'])){
                                $lead_time_value=Lead_time_value::where( array('lead_time_id' => $lead_data_id, 'lead_time_detail_id'=>'9') )->first();
                                $lead_time_value_data = [
                                    'lead_time_id' => $lead_data_id,
                                    'lead_time_detail_id' => '9',
                                    'send_mail' => $lead_time_value->send_mail,
                                    'no_of_days' => $lead_time_value->no_of_days,
                                    'contact_detail' => $item['contact_domestic_ship_confirmation'],
                                ];
                                // dump('contact_warehouse_receipt_if',$lead_time_value_data);
                                Lead_time_value::create($lead_time_value_data);
                            }else{
                                $lead_time_value_data = [
                                    'lead_time_id' => $lead_data_id,
                                    'lead_time_detail_id' => '9',
                                    'send_mail' => $item['notify_domestic_ship_confirmation'],
                                    'no_of_days' =>0,
                                    'contact_detail' => $item['contact_domestic_ship_confirmation'],
                                ];
                                // dump('contact_warehouse_receipt_els',$lead_time_value_data);
                                Lead_time_value::create($lead_time_value_data);
                            }
                        }

                        if( $item['contact_domestic_to_warehouse'] && !empty( $item['contact_domestic_to_warehouse'])){
                            if(empty($item['notify_domestic_to_warehouse']) && !empty($item['contact_domestic_to_warehouse'])){
                                $lead_time_value=Lead_time_value::where( array('lead_time_id' => $lead_data_id, 'lead_time_detail_id'=>'10') )->first();
                                $lead_time_value_data = [
                                    'lead_time_id' => $lead_data_id,
                                    'lead_time_detail_id' => '10',
                                    'send_mail' => $lead_time_value->send_mail,
                                    'no_of_days' => $lead_time_value->no_of_days,
                                    'contact_detail' => $item['contact_domestic_to_warehouse'],
                                ];
                                // dump('contact_warehouse_receipt_if',$lead_time_value_data);
                                Lead_time_value::create($lead_time_value_data);
                            }else{
                                $lead_time_value_data = [
                                    'lead_time_id' => $lead_data_id,
                                    'lead_time_detail_id' => '10',
                                    'send_mail' => $item['notify_domestic_to_warehouse'],
                                    'no_of_days' =>0,
                                    'contact_detail' => $item['contact_domestic_to_warehouse'],
                                ];
                                // dump('contact_warehouse_receipt_els',$lead_time_value_data);
                                Lead_time_value::create($lead_time_value_data);
                            }
                        }

                    }

                }

                /*Order Settings*/
                if(!empty($item['vendor_name'])){
                    $check_exists = Lead_time::where(array('supplier_vendor' => env('VENDOR'), 'supplier_vendor_id' => $vendor_id))->first();
                    if (!empty($check_exists)) {
                        $req_data = [
                            'order_volume_value' => $item['order_volume'],
                            'order_volume_id' => (strtolower($item['order_volume_type'])=='Days' ? 1 : 2)
                        ];
                        Lead_time::where(array('id' => $check_exists->id))->update($req_data);
                    }else{
                        $req_data = [
                            'supplier_vendor' => env('VENDOR'),
                            'supplier_vendor_id' => $vendor_id,
                            'type' =>  1,
                            'order_volume_value' => $item['order_volume'],
                            'order_volume_id' => (strtolower($item['order_volume_type'])=='Days' ? 1 : 2)
                        ];
                        $lead_data = Lead_time::create($req_data);
                    }

                    /*Scedule Detail*/
                
                    $typeArray = ['on_demand'=>0 ,'weekly' => 1, 'bi_weekly' => 2, 'monthly' => 3, 'bi_monthly' => 4];
                    $reorder_schedule_id=null;
                    if(in_array($item['reorder_schedule'], $typeArray)){
                        $reorder_schedule_id=$typeArray[$item['reorder_schedule']];
                    }else{
                        $error_msg_array[]=array('vendor_id'=>$vendor_id,'vendor_name'=>$vendor_name,'message'=>"Reorder Schedule not found");
                        continue;
                    }

                    Reorder_schedule_detail::where(array('supplier_vendor' => env('VENDOR'), 'supplier_vendor_id' => $vendor_id))->delete();
                    $reorder_schedule = ['supplier_vendor' => env('VENDOR'), 'supplier_vendor_id' => $vendor_id, 'reorder_schedule_type_id' => $reorder_schedule_id];
                    if(!$item['reorder_schedule_calendar']){
                        $reorder_schedule_data = Reorder_schedule_detail::create($reorder_schedule);
                    }else{
                        $reorder_schedule_calendar=explode(',', $item['reorder_schedule_calendar']);
                        // dd($reorder_schedule_calendar);
                        if($reorder_schedule_id == 1 || $reorder_schedule_id == 3 ){
                            if(count($reorder_schedule_calendar) > 1){
                                $error_msg_array[]=array('vendor_id'=>$vendor_id,'vendor_name'=>'order setting','message'=>'Please enter no more than 1 characters');
                                continue;
                            }
                        }
                        for ($i=0; $i < count($reorder_schedule_calendar) ; $i++) { 
                            $day=strtolower($reorder_schedule_calendar[$i]);

                            if ($day == 'sun' || $day == 'sunday') $values_d = 1;
                            if ($day == 'mon' || $day == 'monday') $values_d = 2;
                            if ($day == 'tue' || $day == 'tueday') $values_d = 3;
                            if ($day == 'wed' || $day == 'wedday') $values_d = 4;
                            if ($day == 'thu' || $day == 'thuday') $values_d = 5;
                            if ($day == 'fri' || $day == 'friday') $values_d = 6;
                            if ($day == 'sat' || $day == 'satday') $values_d = 7;

                            $reorder_schedule = ['supplier_vendor' => env('VENDOR'), 'supplier_vendor_id' => $vendor_id, 'reorder_schedule_type_id' => $reorder_schedule_id, 'schedule_days' => $values_d];
                            // dd($reorder_schedule);
                            $reorder_schedule_data = Reorder_schedule_detail::create($reorder_schedule);
                        }
                    }
                }

                /*Blackout Days*/

                $events = $item['blackout_event_name'];
                if($events && !empty($events)){
                    $eventexists=Vendor_blackout_date::where(['vendor_id'=>$vendor_id,'event_name'=>$events])->first();

                    $EXCEL_start_DATE= $item['blackout_start_date'];
                    $UNIX_DATE = ($EXCEL_start_DATE - 25569) * 86400;
                    $EXCEL_DATE = 25569 + ($UNIX_DATE / 86400);
                    $UNIX_DATE = ($EXCEL_DATE - 25569) * 86400;
                    $date1=gmdate("Y-m-d", $UNIX_DATE);
                    // $date1=date('d-m-Y', strtotime($date1));

                    $EXCEL_end_DATE= $item['blackout_end_date'];
                    $UNIX_DATE = ($EXCEL_end_DATE - 25569) * 86400;
                    $EXCEL_DATE = 25569 + ($UNIX_DATE / 86400);
                    $UNIX_DATE = ($EXCEL_DATE - 25569) * 86400;
                    $date2=gmdate("Y-m-d", $UNIX_DATE);
                    // $date2=date('d-m-Y', strtotime($date2));

                    $diff = date_diff(new DateTime($date1), new DateTime($date2));
                
                    $days_diff = $diff->format("%a");

                    $req_data = [
                        'vendor_id' => $vendor_id,
                        'event_name' => $events,
                        'start_date' => $date1,
                        'end_date' => $date2,
                        'number_of_days' => $days_diff,
                        'type' => $item['blackout_type'],
                    ];
                    if(!$eventexists){
                        $create = Vendor_blackout_date::create($req_data);
                    }
                }
                /*Shipping*/
                if(!empty($item['vendor_name'])){
                    $shipagentId=0;
                    if(!empty($item['shipping_agent'])){
                        $shipagentId=ShippingAgent::where(['user_marketplace_id' => session('MARKETPLACE_ID')])
                        ->whereRaw("concat('first_name','-','last_name') = ".$item['shipping_agent'])->first();
                        if(!empty($shipagentId)){
                            $shipagentId=$shipagentId->id;
                        }else{
                            $error_msg_array[]=array('vendor_id'=>$vendor_id,'vendor_name'=>$vendor_name,'message'=>'shipping agent not found');
                            continue;
                        }
                    }
                    
                    $req_data_shipping = [
                        'supplier_vendor' => env('VENDOR'),
                        'supplier_vendor_id' => $vendor_id,
                        'logistic_type_id' => $item['logistics_handler'],
                        'shipping_agent_id' => $item['shipping_agent'],
                        'lcl_cost_settings' => ($item['lcl_cost'] != '' ? 1 : 0),
                        'lcl_cost_per' => $item['lcl_metric'], //kg/cbm
                        'lcl_cost' => $item['lcl_cost'],
                        'duties_cost' => $item['duties_cost'],
                        'direct_to_amazon' => $item['duties_cost']
                    ];

                    $shipping_id = Shipping::where('supplier_vendor', env('VENDOR'))->where('supplier_vendor_id',$vendor_id)->get()->first();
                    if(!empty($shipping_id))
                        $shipping_id = $shipping_id->id;
                    if (empty($shipping_id)) {
                        $datas_of = Shipping::create($req_data_shipping);
                        $userLastInsertId = $datas_of->toArray();
                        $shipping_id = $userLastInsertId['id'];
                    } else {
                        $shippingData = Shipping::findOrFail($shipping_id);
                        $shippingData->update($req_data_shipping);
                    }

                    if (!empty($shipping_id)) {

                        if($vendor_type == 1 && $item['logistics_handler'] == 'vendor'){
                            $typeArray = ['on-demand'=>0,'weekly' => 1, 'monthly' => 2];
                    
                            $req_data_shipping = [
                                'supplier_vendor' => env('VENDOR'),
                                'supplier_vendor_id' => $vendor_id,
                                'direct_to_amazon' => 0,
                                'shipping_type' => 1,
                            ];
                            if (empty($shipping_id)) {
                                $datas_of = Shipping::create($req_data_shipping);
                                $userLastInsertId = $datas_of->toArray();
                                $shipping_id = $userLastInsertId['id'];
                            } else {
                                $shippingData = Shipping::findOrFail($shipping_id);
                                $shippingData->update($req_data_shipping);
                            }
                        
                            $warehouses = $item['ship_to_warehouse'];
                            $warehouses = explode(',', $warehouses);
                            if (!empty($warehouses)) {
                                ShipToWarehouse::where('shipping_id', $shipping_id)->delete();
                                for ($i=0; $i < count($warehouses); $i++) { 
                                    $warehouseData=Warehouse::where('warehouse_name',$warehouses[$i])->first();
                                    // dd($warehouseData);
                                    if(empty($warehouseData)){
                                        $error_msg_array[]=array('vendor_id'=>$vendor_id,'vendor_name'=>$vendor_name,'message'=>"Warehouse- ".$warehouses[$i]." not found");
                                        continue;
                                    }
                                    $req_data_warehouse = [
                                        'shipping_id' => $shipping_id,
                                        'warehouse_value' => $warehouseData->id
                                    ];
                                    ShipToWarehouse::create($req_data_warehouse);
                                }
                            }
                            
                            ShippingDeliveryDetails::where('shipping_id', $shipping_id)->delete();
                            Shippingcutoffdeliverytime::where('shipping_id', $shipping_id)->delete();
                            // $shipping_cut_off_delivery_time = $request->cut_off_deliver_time;
                            // $shippDeliveryScheduleType = $request->delivery_schedule;
                            if($item['delivery_schedule'] != 'on-demand'){
                                $days = array();
                                if($item['days'] != ''){
                                    $days = explode (",", $item['days']); 
                                }
                                if(!empty($days)){
                                    foreach ($days as $key => $schedule_type) {
                                        $shipping_schedule = [
                                            'shipping_id' => $shipping_id,
                                            'delivery_schedule_type' => $typeArray[$item['delivery_schedule']],
                                            'schedule_day' => $schedule_type
                                        ];
                                        $shipping_schedule_data = ShippingDeliveryDetails::create($shipping_schedule);
                                        $shipScheduleLastInsertId = $shipping_schedule_data->toArray();
                                        $shipScheduleLastInsert_id = $shipScheduleLastInsertId['id'];
                                        if (!empty($shipScheduleLastInsert_id)) {
                                            $cutOffDay = $cutOffTime = '';
                                            if($item['delivery_schedule'] == 'monthly'){
                                                $cutOffDay = $item['cut_off_delivery_monthly_days'];
                                                $cutOffTime = $item['cut_off_delivery_monthly_time'];
                                            } else{
                                                $cutOffDay = strtolower($schedule_type);
                                                $name = 'cut_off_delivery_time_'.$cutOffDay.'_time';
                                                $cutOffTime = $item[$name];
                                            }
                                            $shippingCutoffDeliveryTimeArray = [
                                                'shipping_id' => $shipping_id,
                                                'shipping_delivery_id' => $shipScheduleLastInsert_id,
                                                'cut_off_day' => $cutOffDay,
                                                'cut_off_time' => $cutOffTime
                                            ];
                                            Shippingcutoffdeliverytime::create($shippingCutoffDeliveryTimeArray);
                                        }
                                    }
                                }
                                
                            } else {
                                $shipping_schedule = [
                                    'shipping_id' => $shipping_id,
                                    'delivery_schedule_type' => $typeArray[$item['delivery_schedule']],
                                    'schedule_day' => ''
                                ];
                                $shipping_schedule_data = ShippingDeliveryDetails::create($shipping_schedule);
                            }
                        
                        






                        }else if($vendor_type == 1 && $item['logistics_handler']=='shipping-carrier'){
                            $shippingcarrierId=0;
                            if(!empty($item['shipping_agent'])){
                                $shippingcarrierId=Shippingcarrier::where(['user_marketplace_id' => session('MARKETPLACE_ID')])
                                ->whereRaw("carrier_name = ".$item['shipping_carrier'])->first();
                                if(!empty($shippingcarrierId)){
                                    $shippingcarrierId=$shippingcarrierId->id;
                                }else{
                                    $error_msg_array[]=array('vendor_id'=>$vendor_id,'vendor_name'=>$vendor_name,'message'=>'shipping carrier not found');
                                    continue;
                                }
                            }
                            $req_data_shipping = [
                                'supplier_vendor' => env('VENDOR'),
                                'supplier_vendor_id' => $vendor_id,
                                'shipping_carrier_id' => $shippingcarrierId,
                                'tracking_provided' => $item['provided_by'],
                                'shipping_type' => 2,
                            ];
                            if (empty($shipping_id)) {
                                $datas_of = Shipping::create($req_data_shipping);
                                $userLastInsertId = $datas_of->toArray();
                                $shipping_id = $userLastInsertId['id'];
                            } else {
                                $shippingData = Shipping::findOrFail($shipping_id);
                                $shippingData->update($req_data_shipping);
                            }
                        
                            $warehouses = $item['ship_to_warehouse'];
                            $warehouses = explode(',', $warehouses);
                            if (!empty($warehouses)) {
                                ShipToWarehouse::where('shipping_id', $shipping_id)->delete();
                                for ($i=0; $i < count($warehouses); $i++) { 
                                    $warehouseData=Warehouse::where('warehouse_name',$warehouses[$i])->first();
                                    // dd($warehouseData);
                                    if(empty($warehouseData)){
                                        $error_msg_array[]=array('vendor_id'=>$vendor_id,'vendor_name'=>$vendor_name,'message'=>"Warehouse- ".$warehouses[$i]." not found");
                                        continue;
                                    }
                                    $req_data_warehouse = [
                                        'shipping_id' => $shipping_id,
                                        'warehouse_value' => $warehouseData->id
                                    ];
                                    ShipToWarehouse::create($req_data_warehouse);
                                }
                            }








                            
                        }else{
                            $req_data_container_20ft = [
                                'shipping_id' => $shipping_id,
                                'container_size' => '20ft',
                                'select_container' => $item['container_20ft'],
                                'cbm_per_container' => $item['container_20ft_cbm'],
                                'container_cost' => $item['container_20ft_cost'],
                            ];
                            $check20ftContainer = ShippingContainerSettings::where('shipping_id', $shipping_id)->where('container_size', '20ft')->get()->first();
                            if ($check20ftContainer == '') {
                                ShippingContainerSettings::create($req_data_container_20ft);
                            } else {
                                $check20ftContainer->update($req_data_container_20ft);
                            }
                            
                            $req_data_container_40ft = [
                                'shipping_id' => $shipping_id,
                                'container_size' => '40ft',
                                'select_container' => $item['container_40ft'],
                                'cbm_per_container' => $item['container_40ft_cbm'],
                                'container_cost' => $item['container_40ft_cost'],
                            ];
                            $check40ftContainer = ShippingContainerSettings::where('shipping_id', $shipping_id)->where('container_size', '40ft')->get()->first();
                            if ($check20ftContainer == '') {
                                ShippingContainerSettings::create($req_data_container_40ft);
                            } else {
                                $check40ftContainer->update($req_data_container_40ft);
                            }
        
                            $req_data_container_40hq = [
                                'shipping_id' => $shipping_id,
                                'container_size' => '40hq',
                                'select_container' => $item['container_40hq'],
                                'cbm_per_container' => $item['container_40hq_cbm'],
                                'container_cost' => $item['container_40hq_cost'],
                            ];
                            $check40hqContainer = ShippingContainerSettings::where('shipping_id', $shipping_id)->where('container_size', '40hq')->get()->first();
                            if ($check20ftContainer == '') {
                                ShippingContainerSettings::create($req_data_container_40hq);
                            } else {
                                $check40hqContainer->update($req_data_container_40hq);
                            }
        
                            $req_data_container_45hq = [
                                'shipping_id' => $shipping_id,
                                'container_size' => '45hq',
                                'select_container' => $item['container_45hq'],
                                'cbm_per_container' => $item['container_45hq_cbm'],
                                'container_cost' => $item['container_45hq_cost'],
                            ];
                            $check45hqContainer = ShippingContainerSettings::where('shipping_id', $shipping_id)->where('container_size', '45hq')->get()->first();
                            if ($check20ftContainer == '') {
                                ShippingContainerSettings::create($req_data_container_45hq);
                            } else {
                                $check45hqContainer->update($req_data_container_45hq);
                            }
        
                            if($item['by_air_standard']!=''){
                                $req_data_shipping = [
                                    'shipping_id' => $shipping_id,
                                    'ship_type' => 'Standard',
                                    'price' => $item['by_air_standard_cost'],
                                    'messuare_in' => $item['by_air_standard_metric']
                                ]; 
                                $shippingAirCheck = Shippingair::where('shipping_id', $shipping_id)->where('ship_type', 'Standard')->where('messuare_in', $item['by_air_standard_metric'])->get()->first();
                                if ($shippingAirCheck == '') {
                                    Shippingair::create($req_data_shipping);
                                } else {
                                    $shippingAirCheck->update($req_data_shipping);
                                }
                            }
                            if($item['by_air_express']!=''){
                                $req_data_shipping = [
                                    'shipping_id' => $shipping_id,
                                    'ship_type' => 'Express',
                                    'price' => $item['by_air_express_cost'],
                                    'messuare_in' => $item['by_air_express_metric']
                                ]; 
                                $shippingAirCheck = Shippingair::where('shipping_id', $shipping_id)->where('ship_type', 'Express')->where('messuare_in', $item['by_air_express_metric'])->get()->first();
                                if ($shippingAirCheck == '') {
                                    Shippingair::create($req_data_shipping);
                                } else {
                                    $shippingAirCheck->update($req_data_shipping);
                                }
                            }
        
                            $warehouses = $item['ship_to_warehouse'];
                            $warehouses = explode(',', $warehouses);
                            if (!empty($warehouses)) {
                                ShipToWarehouse::where('shipping_id', $shipping_id)->delete();
                                for ($i=0; $i < count($warehouses); $i++) { 
                                    $warehouseData=Warehouse::where('warehouse_name',$warehouses[$i])->first();
                                    // dd($warehouseData);
                                    if(empty($warehouseData)){
                                        $error_msg_array[]=array('vendor_id'=>$vendor_id,'vendor_name'=>$vendor_name,'message'=>"Warehouse- ".$warehouses[$i]." not found");
                                        continue;
                                    }
                                    $req_data_warehouse = [
                                        'shipping_id' => $shipping_id,
                                        'warehouse_value' => $warehouseData->id
                                    ];
                                    ShipToWarehouse::create($req_data_warehouse);
                                }
                            }
                        }
                    }
                    

                    $payment = [
                        'supplier_vendor_id' => $vendor_id,
                        'supplier_vendor' =>env('VENDOR'),
                    ];
                    $payment_data = Payment::create($payment);
                    $payment_id=$payment_data->id;

                    $due_days=explode(' ',$item['payment_deposit_due_days']);
                    $dueday=$due_days[0];
                    $dayType=$due_days[1];

                    $payment_term_data=Payment_terms_type::where('payment_type',$item['payment_deposit_event'])->first();
                    if(empty($payment_term_data)){
                        $error_msg_array[]=array('vendor_id'=>$vendor_id,'vendor_name'=>$vendor_name,'message'=>"Payment term type- ".$item['payment_deposit_event']." not found");
                        continue;
                    }
                    $payment_term_type_id=$payment_term_data->id;

                    $deposit = [
                        'payment_terms' => 1,
                        'percentage' => $item['payment_deposit'],
                        'dues' => $dueday,
                        'days_before_after' => (strtolower($dayType)=='before' ? 1 : 2),
                        'payment_term_type_id' => $payment_term_type_id,
                        'payment_id'=>$payment_id
                    ];
                    // dump('deposit',$deposit);
                    Payment_details::create($deposit);

                    $due_days=explode(' ',$item['payment_balance_due_days']);
                    $dueday=$due_days[0];
                    $dayType=$due_days[1];

                    $payment_term_data=Payment_terms_type::where('payment_type',$item['payment_balance_event'])->first();
                    if(empty($payment_term_data)){
                        $error_msg_array[]=array('vendor_id'=>$vendor_id,'vendor_name'=>$vendor_name,'message'=>"Payment term type- ".$item['payment_balance_event']." not found");
                        continue;
                    }
                    $payment_term_type_id=$payment_term_data->id;

                    $balance = [
                        'payment_terms' => 2,
                        'percentage' => $item['payment_balance'],
                        'dues' => $dueday,
                        'days_before_after' => (strtolower($dayType)=='after' ? 1 : 2),
                        'payment_term_type_id' => $payment_term_type_id,//$item['payment_deposit_event']
                        'payment_id'=>$payment_id
                    ];
                    
                    // dd('balance',$balance);
                    Payment_details::create($balance);
                }
            }else{
                if(empty($item['type']) && empty($item['vendor_name']) && empty($item['country']) && empty($item['state']) && empty($item['address']) && empty($item['postal_code']))
                {
                    if(isset($item['contact_fname']) && !empty($item['contact_fname'])){   
                        $emailval = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/';
                        $mob="/^[1-9][0-9]*$/"; 

                        if($item['contact_email'] != '' && !filter_var(trim($item['contact_email']), FILTER_VALIDATE_EMAIL)) {

                            $error_msg_array[]=array('vendor_name'=>$item['vendor_name'],'message'=>''.$item['contact_email'].' invalid email adddress');
                            continue;

                        } if($item['contact_phone'] != '' && !preg_match($mob, $item['contact_phone'])) {

                            $error_msg_array[]=array('vendor_name'=>$item['vendor_name'],'message'=>'Invalid phone number');
                            continue;
                        }
                        
                        $vendor_id = isset($vendor_id) && $vendor_id != '' ? $vendor_id : '';

                        
                        $req_data = [
                            'first_name' => $item['contact_fname'],
                            'last_name' => $item['contact_lname'],
                            'title' => $item['contact_title'],
                            'email' => $item['contact_email'],
                            'phone_number' => $item['contact_phone'],
                            'primary' => ($item['primary'] ?? 0),
                            'vendor_id' => $vendor_id,
                        ];
    
                        $check_email = Vendor_contact::where(array('email' => $item['contact_email'], 'vendor_id' => $vendor_id))->first();
                        if (empty($check_email)) {
                            $datas_of = Vendor_contact::create($req_data);
                            $userLastInsertId = $datas_of->toArray();
                            if ($userLastInsertId != '') {
                                $supplierContactID = $userLastInsertId['id'];
                            }
                        } else {
                            $vendorContact = Vendor_contact::findOrFail($check_email['id']);
                            $vendorContact->update($req_data);
                            $supplierContactID = $check_email['id'];
                        }
                        
                    }
                    $events = isset($item['blackout_event_name']) ? $item['blackout_event_name'] : '';
                    if($events && !empty($events)){
                        $events = $item['blackout_event_name'];
                        $eventexists=Vendor_blackout_date::where(['vendor_id'=>$vendor_id,'event_name'=>$events])->first();

                        $EXCEL_start_DATE= $item['blackout_start_date'];
                        $UNIX_DATE = ($EXCEL_start_DATE - 25569) * 86400;
                        $EXCEL_DATE = 25569 + ($UNIX_DATE / 86400);
                        $UNIX_DATE = ($EXCEL_DATE - 25569) * 86400;
                        $date1=gmdate("Y-m-d", $UNIX_DATE);
                        // $date1=date('d-m-Y', strtotime($date1));

                        $EXCEL_end_DATE= $item['blackout_end_date'];
                        $UNIX_DATE = ($EXCEL_end_DATE - 25569) * 86400;
                        $EXCEL_DATE = 25569 + ($UNIX_DATE / 86400);
                        $UNIX_DATE = ($EXCEL_DATE - 25569) * 86400;
                        $date2=gmdate("Y-m-d", $UNIX_DATE);
                        // $date2=date('d-m-Y', strtotime($date2));

                        $diff = date_diff(new DateTime($date1), new DateTime($date2));
                    
                        $days_diff = $diff->format("%a");

                        $req_data = [
                            'vendor_id' => $vendor_id,
                            'event_name' => $events,
                            'start_date' => $date1,
                            'end_date' => $date2,
                            'number_of_days' => $days_diff,
                            'type' => $item['blackout_type'],
                        ];
                        if(!$eventexists){
                            $create = Vendor_blackout_date::create($req_data);
                        }
                        
                    }
                }
                else if (empty($item['type']) || empty($item['vendor_name']) || empty($item['country']) || empty($item['state']) || empty($item['address']) || empty($item['postal_code'])){
                    if(!empty($item['vendor_name'])){
                        $errorname = $item['vendor_name'];
                    }else{
                        $tcount = $count+1;
                        $errorname = 'Excel sheet line no '. $tcount ;
                    }
                    $error_msg_array[]=array('vendor_name'=>$errorname,'message'=>'All compulsory fields must be required');
                    $error_count = $error_count+1;
                    continue;   
                }
        
            }

        }//dd($error_msg_array);
        Session::put('total_count', $total_count);
        Session::put('error_count', $error_count);
        Session::put('success_count', $success_count);
        //dd(Session::get('total_count'));
        if(isset($error_msg_array) && $error_msg_array!=null){
            // $error_msg_array=implode(' ',$error_msg_array);
            // \Session::flash('message', "Supplier saved successfully.");
            \Session::put('error_msg_array', $error_msg_array);
        }else{
            \Session::put('success_message', "Vendor saved successfully.");
        }
        return;
    }
}
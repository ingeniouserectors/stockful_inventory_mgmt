<?php

namespace App\Jobs;
use App\Classes\helper;
use App\Models\Mws_product;
use App\Models\Usermarketplace;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Bus\Queueable;

class FetchProductDetail extends Job implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $marketplace;
    protected $report_type;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Usermarketplace $marketplace)
    {
        Log::debug("Initializing Product Detail for account {$marketplace->id}");

        $this->marketplace = $marketplace;
        parent::__construct();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Log::debug("Executing job Product Detail for account {$this->marketplace->id}");
            $mwsProduct = Mws_product::whereNull('prod_name')->where('user_marketplace_id', $this->marketplace->id)->get()->toArray();
            foreach ($mwsProduct as $product) {
                $fetchImage = new helper();
                $productData = $fetchImage->getProductImage($product['asin'], $this->marketplace);
                if(!empty($productData['title'])) {
                    Mws_product::where('id', $product)->update(['prod_name' => $productData['title']]);
                }
            }
            Log::error("Error while executing job Product Detail for account {$this->marketplace->id}");
            $this->log_it("Done Product Detail Calculations");
        } catch (\Exception $ex) {
            Log::error("Could not execute job Product Detail: " . $ex->getMessage());
            $this->log_it($ex->getTraceAsString());
            throw new \Exception($ex->getMessage());
        }
    }
}

<?php

namespace App\Jobs;

use App\Models\Account;
use App\Models\Product;
use Carbon\Carbon;
use App\Models\Amazon_requestlog;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Mockery\CountValidator\Exception;
use App\Jobs\Job;
use Illuminate\Bus\Queueable;

class FetchProductInventory extends Job implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $account;
    protected $report_type;
    protected $from_date_time;
    protected $to_date_time;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Account $account, $report_type,
                                Carbon $from_date_time = null, Carbon $to_date_time = null)
    {
        Log::debug("Initializing RequestReport for account {$account->id} with report type $report_type");

        $this->account = $account;
        $this->report_type = $report_type;
        $this->from_date_time = "2016-12-01T07:43:29Z";
        $this->to_date_time = null;
        $this->operation = 'ListInventorySupply';
        parent::__construct();
    }


    /**
     * @return \MarketplaceWebService_Model_RequestReportRequest
     */
    private function getRequest()
    {
        $request = new \FBAInventoryServiceMWS_Model_ListInventorySupplyRequest();
        $request->setQueryStartDateTime($this->from_date_time);
        $this->initRequest($request);
        return $request;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Log::debug("Executing job RequestReport for account {$this->account->id}");
            $request = $this->getRequest();
            $service = $this->getReportsClientFBAInventory();
            $response = $this->invokeListInventorySupply($service, $request);
            if ($response) {
                foreach ($response as $new_response) {
                    foreach ($new_response->InventorySupplyList as $inventory_supply) {
                        foreach ($inventory_supply as $item) {
                            $item = json_decode(json_encode($item));
                            console($item->SellerSKU);
                            console($this->account->id);
                            $productArray = Product::where('sku', $item->SellerSKU)->where('account_id', $this->account->id)->first();
                            console($productArray);
                            if ($productArray) {
                                $productArray->inventory = $item->InStockSupplyQuantity;
                                $productArray->save();
                            }
                        }
                    }
                }
            }
        } catch (\Exception $ex) {
            Log::error("Could not execute job RequestReport: " . $ex->getMessage());
            $this->log_it($ex->getTraceAsString());
            throw new \Exception($ex->getMessage());
        }
    }

    public function invokeListInventorySupply(\FBAInventoryServiceMWS_Interface $service, $request)
    {
        try {
            $response = $service->ListInventorySupply($request);
            //echo ("Service Response\n");
            //echo ("=============================================================================\n");
            $dom = new \DOMDocument();
            $dom->loadXML($response->toXML());
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $dom->saveXML();
            //echo("ResponseHeaderMetadata: " . $response->getResponseHeaderMetadata() . "\n");
            //$arr_response = new \SimpleXMLElement($dom->saveXML());
            return $arr_response = new \SimpleXMLElement($dom->saveXML());


        } catch (\FBAInventoryServiceMWS_Exception $ex) {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
        }
    }
}

<?php

namespace App\Jobs;

use ClouSale\AmazonSellingPartnerAPI\Api\ReportsApi;
use ClouSale\AmazonSellingPartnerAPI\Models\Reports\CreateReportSpecification;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Log;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

abstract class Jobspapi
{
    /*
    |--------------------------------------------------------------------------
    | Queueable Jobs
    |--------------------------------------------------------------------------
    |
    | This job base class provides a central location to place any logic that
    | is shared across all of your jobs. The trait included with the class
    | provides access to the "onQueue" and "delay" queue helper methods.
    |
    */

    use Queueable;
    protected $mLogger;

    public function __construct()
    {
        $this->mLogger = new Logger(__CLASS__);
        $this->mLogger->pushHandler(
            new StreamHandler(storage_path('logs') . "/jobs/".get_dashed_name($this)."/{$this->marketplace->id}_jobs.log",
                Logger::WARNING));
    }

    public function log_it($msg){
        //$this->mLogger->addAlert($msg);
    }

    /**
     * @var
     */
    protected $background_id;

    /**
     * @var Logger
     */
    protected $logger;

    protected function initRequest($config, $options){
        $assumedRole = $this->getAssumeRole($options);
        $accessToken = $this->getAccessKeys($options);
        $config->setAccessToken($accessToken);
        $config->setAccessKey($assumedRole->getAccessKeyId());
        $config->setSecretKey($assumedRole->getSecretAccessKey());
        $config->setSecurityToken($assumedRole->getSessionToken());
    }

    protected function getSPAPIReport($config){
        return new ReportsApi($config);
    }

    protected function getReportSepc(){
        return new CreateReportSpecification();
    }

    protected function getAccessKeys($options){
        $accessToken = $options->access_token;
        try {
            if (empty($options->access_token)) {
                $accessToken = \ClouSale\AmazonSellingPartnerAPI\SellingPartnerOAuth::getAccessTokenFromRefreshToken(
                    $options->refresh_token,
                    $options->client_id,
                    $options->client_secret
                );
                if (!empty($accessToken)) {
                    $options->access_token = $accessToken;
                    $options->expire_time = date('Y-m-d H:i:s');
                    $options->save();
                }
            }
            if (!empty($options->expire_time)) {
                $diff = strtotime(date('Y-m-d H:i:s')) - strtotime($options->expire_time);
                if ($diff >= 3500) {
                    $accessToken = \ClouSale\AmazonSellingPartnerAPI\SellingPartnerOAuth::getAccessTokenFromRefreshToken(
                        $options->refresh_token,
                        $options->client_id,
                        $options->client_secret
                    );
                    if (!empty($accessToken)) {
                        $options->access_token = $accessToken;
                        $options->expire_time = date('Y-m-d H:i:s');
                        $options->save();
                    }
                }
            }
            return $accessToken;
        } catch (\Exception $ex) {
            Log::error("Can't Generate Access Token : " . $ex->getMessage());
            $this->log_it($ex->getTraceAsString());
            throw new \Exception($ex->getMessage());
        }
    }

    protected function getAssumeRole($options){
        try {
            return \ClouSale\AmazonSellingPartnerAPI\AssumeRole::assume(
                \ClouSale\AmazonSellingPartnerAPI\SellingPartnerRegion:: $NORTH_AMERICA,
                $options['access_key'],
                $options['secret_key'],
                $options['role_arn']
            );
        } catch (\Exception $ex) {
            Log::error("Can't Generate Assume role : " . $ex->getMessage());
            $this->log_it($ex->getTraceAsString());
            throw new \Exception($ex->getMessage());
        }
    }

}

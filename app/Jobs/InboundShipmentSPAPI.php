<?php

namespace App\Jobs;

use App\Models\Amazon_requestlog;
use App\Models\SPApiUserMarketplace;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Bus\Queueable;
use ClouSale\AmazonSellingPartnerAPI\Configuration;
use App\Models\Inbound_shipments;
use App\Models\Inbound_shipment_items;
use App\Models\Mws_product;

class InboundShipmentSPAPI extends Jobspapi implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $marketplace;
    protected $statusArray;
    protected $amazon_date_itetration;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(SPApiUserMarketplace $marketplace, $amzDateitetration = null)
    {
        Log::debug("Initializing Inbound Shipment for account {$marketplace->id}");

        $this->marketplace = $marketplace;
        //dd($this->marketplace);
        // if (Carbon::now()->format('h') <= 8) {
        //     $this->statusArray = array('CLOSED', 'DELIVERED');
        // } else if (Carbon::now()->format('h') >= 8 && Carbon::now()->format('h') <= 16) {
        //     $this->statusArray = array('RECEIVING', 'WORKING');
        // } else {
        //     $this->statusArray = array('SHIPPED', 'IN_TRANSIT', 'CHECKED_IN');
        // }
        $this->statusArray = array('CLOSED', 'DELIVERED','RECEIVING', 'WORKING','SHIPPED', 'IN_TRANSIT', 'CHECKED_IN');
        parent::__construct();
    }
    

    /**
     * @return \ClouSale\AmazonSellingPartnerAPI\Configuration
     */
    private function getConfiguration($options)
    {
        $config = \ClouSale\AmazonSellingPartnerAPI\Configuration::getDefaultConfiguration();
        $config->setHost(\ClouSale\AmazonSellingPartnerAPI\SellingPartnerEndpoint::$NORTH_AMERICA);
        $config->setRegion(\ClouSale\AmazonSellingPartnerAPI\SellingPartnerRegion::$NORTH_AMERICA);
        $this->initRequest($config, $options);
        return $config;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Log::debug("Executing job Inbound Shipment for account {$this->marketplace->id}");
            $requestConfiguration = $this->getConfiguration($this->marketplace);
            $this->inboundShipments($requestConfiguration, $this->marketplace->id);

        } catch (\Exception $ex) {
            Log::error("Could not execute job RequestReport: " . $ex->getMessage());
            $this->log_it($ex->getTraceAsString());
            throw new \Exception($ex->getMessage());
        }
    }
    
    public function inboundShipments($requestConfiguration,$user_marketplace_id){
        $finance = new \ClouSale\AmazonSellingPartnerAPI\Api\FbaInboundApi($requestConfiguration);
        //echo $createdAfter = new \DateTime(date('Y-m-d H:i:s', strtotime('-6 month')), new \DateTimeZone('UTC'));
        //echo $createdBefore = new \DateTime(date('Y-m-d H:i:s', strtotime('+1 month')), new \DateTimeZone('UTC'));
        $date = date_create(date('Y-m-d 00:00:00', strtotime('-1 day')),new \DateTimeZone('UTC'));
        $createdAfter=date_format($date, 'Y-m-d H:i:s');
        $date = date_create(date('Y-m-d H:i:s', strtotime('+1 month')),new \DateTimeZone('UTC'));
        $createdBefore=date_format($date, 'Y-m-d H:i:s');

        //  echo '<pre>';
        //  print_r($createdAfter);
        //  dd($createdBefore);
        // $token = '';
        
        $response = $finance->getShipments(true,$requestConfiguration->getRegion(),$this->statusArray,null,$createdAfter,$createdBefore,null);
        $payload = $response->getPayload();
        $shipmentData = $payload->getShipmentData();
        $sipment = $this->insertShipmentData($shipmentData, $user_marketplace_id);
       // $nextToken = (isset($payload['next_token']) ? $payload['next_token'] : '');
        
        // print_r($payload);
        // print_r($nextToken);
        // exit;
        // while (!empty($nextToken)) {
        //     $tokenResponse = $finance->getShipments(true,$requestConfiguration->getRegion(),$this->statusArray,null,null,null,"AAAAAAAAAACsAB7hafz7hh4DJXb6kEknmgEAAAAAAADK/XGfIzv5Til9LsGVmQagRw1YGzgOP6RzDAgPwkVllHiE6ZMBPx2zrPkNGm3yIrrctrW1/ykbk0x0VOmB0dA8HTqPkReip8t0r2BBSZ3eQvGOqnvx3WvYpBtFHixi+15AszfEpVxusBQ7GFE8fjmWrUFEg0ez0N0hXpSUTC4zHJAuiOcbO2hoZU2iFEBwzPVmRUR+QSdmqoXsdD2Cg5KeHi51X5B45o7ynp+WwKqnxIhZn67zOtw3SStlIV15KD61c98g263LUD+UTEGIrLLA0mKMugTAsDqaPWClr+AKMuIs25cxkk9CrTnlzLOdYsXOF/kzTuL0UijOR+/rgbTfOE17s0ivU/8Dy9nuZUL5/FtEx9llBU09Tfr5FPbifgf18VgNPjxAB4CAuiQqm1O3II/22N44ZDUMWUNS4bvsE1eX0WrUSn6rwUXKht/+nz5ZxoVozYv2fcJYVgn2v/MP2xhOt6U6ueDVcWK40NMptSj6lZLspGSg+zrI3KqSa9x4DfUnaR/z+O8+u4KDzTGFxufy8MoTwnB5LQ==");
        //     $tokenpayload = $tokenResponse->getPayload();
        //     $shipmentDataToken = $tokenpayload->getShipmentData();
        //     $sipment = $this->insertShipmentData($shipmentData, $user_marketplace_id);
        //     //dd($sipment);
        //     $nextToken = (isset($tokenpayload['next_token']) ? $tokenpayload['next_token'] : '');
        //     print_r('/n '.$nextToken);
        //     // if(isset($tokenResponse->ListInboundShipmentByNextTokenResult)) {
        //     //     $nextToken = (isset($tokenResponse->ListInboundShipmentItemsByNextTokenResult->NextToken) ? $tokenResponse->ListInboundShipmentItemsByNextTokenResult->NextToken : '');
        //     //    print_r($tokenResponse->ListInboundShipmentItemsByNextTokenResult);
        //     //     // $shipmentItem = $this->insertShipmentData($tokenResponse->ListInboundShipmentItemsByNextTokenResult,$user_marketplace_id);
        //     // }
        // }
        // exit;
        //$shipmentID =$shipmentData[0]->getShipmentId();

        if(!empty($sipment)){
            foreach($sipment as $shipmentId){
                $responseItem = $finance->getShipmentItemsByShipmentId($shipmentId,$requestConfiguration->getRegion());
                $payloads = $responseItem->getPayload();
                $shipmentItem = $this->insertShipmentItemsData($payloads,$user_marketplace_id,$shipmentId);
                // $nextToken = (isset($payloads['next_token']) ? $payloads['next_token'] : '');
                // while (!empty($nextToken)) {
                //     $tokenResponse = $finance->getShipmentItemsByShipmentId($shipmentId,$requestConfiguration->getRegion(),$nextToken);
                //     if(isset($tokenResponse->ListInboundShipmentItemsByNextTokenResult)) {
                //         $nextToken = (isset($tokenResponse->ListInboundShipmentItemsByNextTokenResult->NextToken) ? $tokenResponse->ListInboundShipmentItemsByNextTokenResult->NextToken : '');
                //         $shipmentItem = $this->insertShipmentItemsData($tokenResponse->ListInboundShipmentItemsByNextTokenResult,$user_marketplace_id,$shipmentId);
                //     }
                // }
            }
        }
        
        exit;

    }

    private function insertShipmentData($shipmentData, $user_marketplace_id)
    {
        //dd($shipmentData);
        $shipment = array();
        if (isset($shipmentData)) {
          
           $products_data = (array)$shipmentData;

             foreach ($products_data as $key => $member) {
              
                $shipmentId = $member->getShipmentId();
                $shipmentName = $member->getShipmentName();
                $addresses = $member->getShipFromAddress();
            
                $fromAddress = $addresses['name'] . ", " . $addresses['address_line1']. ", " . $addresses['city'] . ", " . $addresses['state_or_province_code'] . ", " . $addresses['country_code'] . ", " . $addresses['postal_code'];

                $centerId = $member->getDestinationFulfillmentCenterId();
                $status = $member->getShipmentStatus();
                $prepType = $member->getLabelPrepType();
                $inboundShip = Inbound_shipments::where('ShipmentId', $shipmentId)->first();
                if (empty($inboundShip)) {
                    $inboundShip = new Inbound_shipments();
                    $inboundShip->user_marketplace_id = $user_marketplace_id;
                }
                $inboundShip->ShipmentId = $shipmentId;
                $inboundShip->ShipmentName = $shipmentName;
                $inboundShip->ShipFromAddress = $fromAddress;
                $inboundShip->DestinationFulfillmentCenterId = $centerId;
                $inboundShip->ShipmentStatus = $status;
                $inboundShip->LabelPrepType = $prepType;
               $inboundShip->save();
                if (isset($shipmentId)) {
                    $shipment[] = $shipmentId;
                }
             }
        }
        return $shipment;
    }

    private function insertShipmentItemsData($payloads,$user_marketplace_id,$shipmentId){
        $shipData = Inbound_shipments::where('ShipmentId', $shipmentId)->first();
        foreach ($payloads['item_data'] as $members) {

            $shipmentID = $members->getShipmentId(); 
            $QuantityShipped = $members->getQuantityShipped(); 
            $getQuantityReceived = $members->getQuantityReceived(); 
            $getQuantityInCase = $members->getQuantityInCase();
            if($members->getSellerSKU() != ''){
                $product = Mws_product::where('sku', $members->getSellerSKU())->first();
                if (isset($product->id)) {
                    $itemData = Inbound_shipment_items::where('inbound_shipment_id', $shipData->id)->where('product_id', $product->id)->first();
                    if (empty($itemData)) {
                        $itemData = new Inbound_shipment_items();
                        $itemData->inbound_shipment_id = $shipData->id;
                        $itemData->product_id = $product->id;
                        $itemData->QuantityShipped = !empty($members->getQuantityShipped()) ? $members->getQuantityShipped() : 0 ;
                        $itemData->QuantityReceived = !empty($members->getQuantityReceived()) ? $members->getQuantityReceived() : 0 ;
                        $itemData->QuantityInCase = !empty($members->getQuantityInCase()) ? $members->getQuantityInCase() : 0 ;
                    } else {
                        $itemData->QuantityShipped = !empty($members->getQuantityShipped()) ? $members->getQuantityShipped() : 0 ;
                        $itemData->QuantityReceived = !empty($members->getQuantityReceived()) ? $members->getQuantityReceived() : 0 ;
                        $itemData->QuantityInCase = !empty($members->getQuantityInCase()) ? $members->getQuantityInCase() : 0 ;
                    }
                    $itemData->save();
                }
            }
        }
        return true;
    }
}

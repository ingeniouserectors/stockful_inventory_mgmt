<?php

namespace App\Jobs;

use App\Models\Amzdateiteration;
use App\Models\Calculated_sales_qty;
use App\Models\Daily_logic_Calculations;
use App\Models\Default_logic_setting;
use App\Models\Mws_calculated_inventory_daily;
use App\Models\Mws_out_of_stock_detail;
use App\Models\Mws_product;
use App\Models\Mws_unsuppressed_inventory_data;
use App\Models\Product_assign_supply_logic;
use App\Models\Purchase_order_details;
use App\Models\Purchase_orders;
use App\Models\Sales_total_trend_rate;
use App\Models\Supplier_blackout_date;
use App\Models\Supplier_product;
use App\Models\Usermarketplace;
use App\Models\Vendor_blackout_date;
use App\Models\Vendor_product;
use App\Models\Warehouse_product;
use App\Models\Warehouse_wise_default_setting;
use App\Models\Vendor_default_setting;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Bus\Queueable;

class StockCalculations extends Job implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    public $marketplace;
    protected $report_type;
    protected $from_date_time;
    protected $to_date_time;
    protected $amazon_date_itetration;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Usermarketplace $marketplace, $amzDateitetration = '')
    {
        Log::debug("Initializing CalculateTrend for account {$marketplace->id}");

        $this->marketplace = $marketplace;
        $this->amazon_date_itetration = $amzDateitetration;
        if (empty($amzDateitetration)) {
            $this->from_date_time = Carbon::yesterday();
            $this->to_date_time = Carbon::now();
        } else {
            $this->from_date_time = $amzDateitetration['startDate'];
            $this->to_date_time = $amzDateitetration['endDate'];
        }

        parent::__construct();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Log::debug("Executing job Out of Stock Calculations for account {$this->marketplace->id}");
            $countDays = date('t', strtotime($this->to_date_time));
            for ($counter = 0; $counter < $countDays; $counter++) {
                $checkDate = new Carbon($this->from_date_time);
                $checkDate = $checkDate->addDays($counter)->format('Y-m-d');
                $productsArray = Mws_product::where('user_marketplace_id', $this->marketplace->id)->get()->toArray();
                foreach ($productsArray as $product) {
                    $calculatedInventoryCount = Mws_calculated_inventory_daily::where('product_id', $product['id'])->where('inventory_date', $checkDate)->first();
                    if (!empty($calculatedInventoryCount)) {
                        if ($calculatedInventoryCount->qty == 0) {
                            $zeroStock = Mws_out_of_stock_detail::create();
                            $zeroStock->user_marketplace_id = $product['user_marketplace_id'];
                            $zeroStock->product_id = $product['id'];
                            $zeroStock->calculated_inventory_daily_id = $calculatedInventoryCount->id;
                            $zeroStock->fnsku = $calculatedInventoryCount->fnsku;
                            $zeroStock->sku = $calculatedInventoryCount->sku;
                            $zeroStock->stock_date = $checkDate;
                            $zeroStock->qty = $calculatedInventoryCount->qty;
                            $zeroStock->save();
                        }
                    }
                }
            }
            if(!empty($this->amazon_date_itetration)){
                $amzDateitetration = Amzdateiteration::where('id', $this->amazon_date_itetration->id)->first();
                if (!empty($amzDateitetration->id)) {
                    $amzDateitetration->InventoryStatus = '2';
                    $amzDateitetration->save();
                }
            }
            Log::error("Error while executing job CalculateTrend for account {$this->marketplace->id}");
            $this->log_it("Done Out of Stock Calculations");
        } catch (\Exception $ex) {
            Log::error("Could not execute job Out of Stock Calculations: " . $ex->getMessage());
            $this->log_it($ex->getTraceAsString());
            throw new \Exception($ex->getMessage());
        }
    }
}

<?php

namespace App\Jobs;

use App\Models\Amzdateiteration;
use App\Models\Calculated_order_qty;
use App\Models\Mws_adjusted_inventory;
use App\Models\Mws_calculated_inventory_daily;
use App\Models\Mws_order_returns;
use App\Models\Mws_product;
use App\Models\Usermarketplace;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class AdjustedInventoryDaily extends Job implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $marketplace;
    protected $amazon_date_itetration;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Usermarketplace $marketplace, $amzDateitetration = null)
    {
        Log::debug("Calculate adjusted inventory for {$marketplace->id}");

        $this->marketplace = $marketplace;
        $this->amazon_date_itetration = $amzDateitetration;
        parent::__construct();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Log::debug("Calculate adjusted inventory for {$this->marketplace->id}");

            if(empty($this->amazon_date_itetration)){
                $start = new DateTime((new DateTime(now()))->sub(new DateInterval("P7D"))->format('Y-m-d 00:00:00'));
                $endDate = (new DateTime(now()));;
            }else{
                $start = (new DateTime($this->amazon_date_itetration->startDate));
                $endDate = (new DateTime($this->amazon_date_itetration->endDate));;
            }
            $amzDateitetrationCount = Amzdateiteration::where('user_marketplace_id', $this->marketplace->id)
                ->where('adjustedCalculations', '1')
                ->count();
            if ($amzDateitetrationCount > 0)
                $amzDateitetrationFlag = 1;
            else
                $amzDateitetrationFlag = 0;
            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($start, $interval, $endDate);
            $products = Mws_product::where('user_marketplace_id', $this->marketplace->id)->get()->toArray();
            foreach ($products as $product) {
                $productEmptyCount = 0;
                foreach ($period as $dateValue) {
                    $totalSevenDaysSales = $totalSevenDaysSalesAverage = 0;
                    $date = $dateValue->format("Y-m-d 00:00:00");
                    $dateDay = $dateValue->format("d");
                    $nextDayInventoryStart = (new DateTime($date))->sub(new DateInterval("P1D"))->format('Y-m-d 00:00:00');
                    $nextDayInventoryEnd = (new DateTime($date))->sub(new DateInterval("P1D"))->format('Y-m-d 23:59:59');
                    $nextDayInventoryDate = (new DateTime($date))->sub(new DateInterval("P1D"))->format('Y-m-d');
                    if ($amzDateitetrationFlag == 1 || $dateDay >= 8) {
                        $sevenDaysBackDate = (new DateTime($date))->sub(new DateInterval("P7D"))->format('Y-m-d');
                        $oneDaysBackDate = (new DateTime($date))->sub(new DateInterval("P1D"))->format('Y-m-d');
                        $totalSevenDaysSales = Calculated_order_qty::where('order_date', '<=', $oneDaysBackDate)
                            ->where('order_date', '>=', $sevenDaysBackDate)
                            ->where('product_id', $product['id'])
                            ->sum('total_qty_shipped');
                        if ($totalSevenDaysSales > 0) {
                            $totalSevenDaysSalesAverage = $totalSevenDaysSales / 7;
                        }
                    }

                    $previousDayInventory = Mws_calculated_inventory_daily::where('product_id',$product['id'])
                        ->where('inventory_date', $nextDayInventoryDate)
                        ->first();

                    $sales = Calculated_order_qty::where('product_id', $product['id'])
                        ->where('order_date', $dateValue->format("Y-m-d"))
                        ->first();

                    $orderReturn = Mws_order_returns::where('product_id', $product['id'])
                        ->where('return_date', '>=', $dateValue->format("Y-m-d 00:00:00"))
                        ->where('return_date', '<=', $dateValue->format("Y-m-d 23:59:59"))
                        ->sum('quantity');

                    $calculated_inventories = Mws_calculated_inventory_daily::where('product_id', $product['id'])
                        ->where('inventory_date', $date)
                        ->first();

                    if(empty($previousDayInventory)){
                        $adjustmentInventory = Mws_adjusted_inventory::where('product_id', $product['id'])
                            ->where('date',$nextDayInventoryStart)
                            ->first();
                    }

                    $adjustedInventory = (!empty($previousDayInventory) ? $previousDayInventory->qty : (!empty($adjustmentInventory) ? $adjustmentInventory->quantity : 0)) -
                        (!empty($sales) ? $sales->total_qty_shipped : 0) +
                        (!empty($orderReturn) ? $orderReturn : 0);


                    $inventoryToBeConsider = (!empty($calculated_inventories) ? $calculated_inventories->qty : $adjustedInventory);

                    $calculationInventories = Mws_adjusted_inventory::where('date', $date)->where('product_id', $product['id'])->first();
                    if (empty($calculationInventories)) {
                        $calculationInventories = new Mws_adjusted_inventory();
                        $productEmptyCount += $productEmptyCount;
                    }
                    $calculationInventories->user_marketplace_id = $this->marketplace->id;
                    $calculationInventories->product_id = $product['id'];
                    $calculationInventories->sku = $product['sku'];
                    $calculationInventories->quantity = (!empty($calculated_inventories) ? $calculated_inventories->qty : 0);
                    $calculationInventories->date = $date;
                    $calculationInventories->total_sales = (!empty($sales) ? $sales->total_qty_shipped : 0);
                    $calculationInventories->total_return = (!empty($orderReturn) ? $orderReturn : 0);
                    $calculationInventories->adjusted_inventory = $adjustedInventory;
                    $calculationInventories->seven_day_sales = $totalSevenDaysSales;
                    $calculationInventories->average_sales = $totalSevenDaysSalesAverage;
                    $calculationInventories->mod_sales = ($totalSevenDaysSalesAverage > $inventoryToBeConsider ? $totalSevenDaysSalesAverage : (!empty($sales) ? $sales->total_qty_shipped : 0));
                    $calculationInventories->modified_sales_flag = ($totalSevenDaysSalesAverage > $inventoryToBeConsider ? 1 : 0);
                    $calculationInventories->save();
                }
            }
            if(!empty($amzDateitetration)) {
                $amzDateitetration->adjustedCalculations = '1';
                $amzDateitetration->save();
            }
            Log::debug("Done Adjusted Inventory Calculation {$this->marketplace->id}");
            $this->log_it("Done Adjusted Inventory Calculation");
        } catch (\Exception $ex) {
            Log::error("Could not execute job of adjusted inventory Detail: " . $ex->getMessage());
            $this->log_it($ex->getTraceAsString());
            throw new \Exception($ex->getMessage());
        }
    }
}

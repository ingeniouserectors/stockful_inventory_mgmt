<?php

namespace App\Jobs;

use App\Models\Calculated_sales_qty;
use App\Models\Daily_logic_Calculations;
use App\Models\Default_logic_setting;
use App\Models\Mws_product;
use App\Models\Mws_unsuppressed_inventory_data;
use App\Models\Product_assign_supply_logic;
use App\Models\Purchase_order_details;
use App\Models\Purchase_orders;
use App\Models\Sales_total_trend_rate;
use App\Models\Supplier_blackout_date;
use App\Models\Supplier_product;
use App\Models\Usermarketplace;
use App\Models\Vendor_blackout_date;
use App\Models\Vendor_product;
use App\Models\Warehouse_product;
use App\Models\Warehouse_wise_default_setting;
use App\Models\Vendor_default_setting;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Bus\Queueable;

class LogicCalculations extends Job implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    public $marketplace;
    protected $report_type;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Usermarketplace $marketplace)
    {
        Log::debug("Initializing CalculateTrend for account {$marketplace->id}");

        $this->marketplace = $marketplace;
        parent::__construct();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Log::debug("Executing job CalculateTrend for account {$this->marketplace->id}");
            $fetchProducts = Mws_product::where('user_marketplace_id', $this->marketplace->id)->get()->toArray();
            foreach ($fetchProducts as $product) {
//                $amazonInventory = Mws_afn_inventory_data::where('product_id', $product['id'])->where('warehouse_condition_code', 'SELLABLE')->first();
                $amazonInventory = Mws_unsuppressed_inventory_data::where('product_id', $product['id'])->first();
                if($amazonInventory) {
                    $wareHouseInventory = Warehouse_product::where('product_id', $product['id'])->get();
                    $warehouseInventoryTotal = $wareHouseLeadTime = 0;
                    $logicCalculations = Product_assign_supply_logic::with(['supply_reoder_logic'])->where('product_id', $product['id'])->first();
                    if (empty($logicCalculations)) {
                        $logicCalculations = Default_logic_setting::where('user_marketplace_id', $product['user_marketplace_id'])->first();
                    }
                    if (isset($wareHouseInventory[0])) {
                        foreach ($wareHouseInventory as $warehouse) {
                            $warehouseInventoryTotal += $warehouse->qty;
                            $leadTimeWarehouse = Warehouse_wise_default_setting::where('id', $warehouse->warehouse_id)->first();
                            if ($wareHouseLeadTime < $leadTimeWarehouse->lead_time) {
                                $wareHouseLeadTime = $leadTimeWarehouse->lead_time;
                            }
                        }
                    }
                    $productSettings = Supplier_product::where('product_id', $product['id'])->first();
                    $blackOutDayFlat = 0;
                    if (empty($productSettings)) {
                        $productSettings = Vendor_product::where('product_id', $product['id'])->first();
                        $blackOutDayFlat = 1;
                    }
                    if (empty($productSettings)) {
                        $productSettings = Vendor_default_setting::first();
                        $blackOutDayFlat = 2;
                    }
                    if ($productSettings) {
                        $fetchPO = Purchase_orders::where('user_marketplace_id', $this->marketplace->id)->where('reorder_date','>=',Carbon::now()->format('Y-m-d'));
                        $fetchProductPOInventory = 0;
                        foreach($fetchPO as $poDetails){
                            $fetchProductPODetail = Purchase_order_details::where('purchase_orders_id',$poDetails->id)->where('product_id',$product['id'])->first();
                            $fetchProductPOInventory += $fetchProductPODetail->projected_units;
                        }
                        $TotalInventory = $amazonInventory->afn_total_qty - $amazonInventory->afn_unsellable_qty + $warehouseInventoryTotal + $fetchProductPOInventory;
                        if ($TotalInventory > 0) {
                            $TotalLeadTime = $wareHouseLeadTime + $productSettings->lead_time;
                            $orderVolume = $productSettings->order_volume;
                            //Seven Days Calculations
                            $sevenDaysBackDate = Carbon::now()->subDays(7)->format('Y-m-d');
                            $sevenDaysSales = Calculated_sales_qty::where('product_id', $product['id'])->where('order_date', '>=', $sevenDaysBackDate)->sum('total_qty');
                            if ($sevenDaysSales > 0) {
                                $sevenDaysDayRate = round(($sevenDaysSales / 7), 6);
                                $sevenDaysDayOfSupply = round(($TotalInventory / $sevenDaysDayRate), 2);
                            } else {
                                $sevenDaysSales = $sevenDaysDayRate = $sevenDaysDayOfSupply = 0;
                            }

                            //Fourteen Days Calculations
                            $fourteenDaysBackDate = Carbon::now()->subDays(14)->format('Y-m-d');
                            $fourteenDaysSales = Calculated_sales_qty::where('product_id', $product['id'])->where('order_date', '>=', $fourteenDaysBackDate)->sum('total_qty');
                            if ($fourteenDaysSales > 0) {
                                $fourteenDaysDayRate = round(($fourteenDaysSales / 14), 6);
                                $fourteenDaysDayOfSupply = round(($TotalInventory / $fourteenDaysDayRate), 2);
                            } else {
                                $fourteenDaysSales = $fourteenDaysDayRate = $fourteenDaysDayOfSupply = 0;
                            }

                            //Thirty Days Calculations
                            $thirtyDaysBackDate = Carbon::now()->subDays(30)->format('Y-m-d');
                            $thirtyDaysSales = Calculated_sales_qty::where('product_id', $product['id'])->where('order_date', '>=', $thirtyDaysBackDate)->sum('total_qty');
                            if ($thirtyDaysSales > 0) {
                                $thirtyDaysDayRate = round(($thirtyDaysSales / 30), 6);
                                $thirtyDaysDayOfSupply = round(($TotalInventory / $thirtyDaysDayRate), 2);
                            } else {
                                $thirtyDaysSales = $thirtyDaysDayRate = $thirtyDaysDayOfSupply = 0;
                            }

                            //Previous Seven Days Calculations
                            $previousSevenDaysSales = Calculated_sales_qty::where('product_id', $product['id'])->where('order_date', '<=', $sevenDaysBackDate)->where('order_date', '>=', $fourteenDaysBackDate)->sum('total_qty');
                            if ($previousSevenDaysSales > 0) {
                                $previousSevenDaysDayRate = round(($previousSevenDaysSales / 7), 6);
                            } else {
                                $previousSevenDaysDayRate = 0;
                            }

                            //Previous Thirty Days Calculations
                            $sixtyDaysBackDate = Carbon::now()->subDays(60)->format('Y-m-d');
                            $previousThirtyDaysSales = Calculated_sales_qty::where('product_id', $product['id'])->where('order_date', '<=', $thirtyDaysBackDate)->where('order_date', '>=', $sixtyDaysBackDate)->sum('total_qty');
                            if ($previousThirtyDaysSales > 0) {
                                $previousThirtyDaysDayRate = round(($previousThirtyDaysSales / 30), 6);
                            } else {
                                $previousThirtyDaysDayRate = 0;
                            }
                            //Last Year 30 Days Sales
                            $thirtyDaysStartDateLastYear = Carbon::now()->subDays(395)->format('Y-m-d');
                            $thirtyDaysEndDateLastYear = Carbon::now()->subDays(365)->format('Y-m-d');
                            $thirtyDaysSalesLastyear = Calculated_sales_qty::where('product_id', $product['id'])->where('order_date', '<=', $thirtyDaysEndDateLastYear)->where('order_date', '>=', $thirtyDaysStartDateLastYear)->sum('total_qty');
                            if ($thirtyDaysSalesLastyear > 0) {
                                $ThirtyDaysDayRateLastYear = round(($thirtyDaysSalesLastyear / 30), 6);
                            } else {
                                $ThirtyDaysDayRateLastYear = 0;
                            }

                            $currentMonthPreviousYearStartDate = Carbon::now()->subYear(1)->format('Y-m-01');
                            $currentMonthPreviousYearEndDate = Carbon::now()->subYear(1)->format('Y-m-t');
                            $currentMonthLastYearStartDate = Carbon::now()->subYear(2)->format('Y-m-01');
                            $currentMonthLastYearEndDate = Carbon::now()->subYear(2)->format('Y-m-t');

                            $previousYearSameMonthSales = Calculated_sales_qty::where('product_id', $product['id'])->where('order_date', '<=', $currentMonthPreviousYearEndDate)->where('order_date', '>=', $currentMonthPreviousYearStartDate)->sum('total_qty');
                            $lastYearSameMonthSales = Calculated_sales_qty::where('product_id', $product['id'])->where('order_date', '<=', $currentMonthLastYearEndDate)->where('order_date', '>=', $currentMonthLastYearStartDate)->sum('total_qty');
                            $trendDirectionPrevious = $breakStatus = $monthlySalesTrendTotal = 0;

                            // Multi Month Trend Calculations
                            $trendDirectionPrevious = $breakStatus = $monthlySalesTrendTotal = $monthToMonthAverageTrend = 0;
                            for ($counter = 1; $counter < 18; $counter++) {
                                $calculationMonth = Carbon::now()->subMonth($counter)->format('Y-m');
                                $monthlySales = Sales_total_trend_rate::where('prod_id', $product['id'])->where('month_year', $calculationMonth)->first();
                                $trendDirection = $this->sign($monthlySales['trend_percentage']);
                                if ($counter > 1) {
                                    if ($trendDirection == $trendDirectionPrevious) {
                                        $monthlySalesTrendTotal += $monthlySales['trend_percentage'];
                                    } else {
                                        $monthToMonthAverageTrend = $monthlySalesTrendTotal / ($counter - 1);
                                        $breakStatus = 1;
                                    }
                                } else {
                                    $monthlySalesTrendTotal += $monthlySales['trend_percentage'];
                                }
                                $trendDirectionPrevious = $trendDirection;
                                if ($breakStatus == 1)
                                    break;
                            }
                            // Multi Week Trend Calculations
                            $trendDirectionPrevious = $breakStatus = $weeklySalesTrendTotal = $multiWeekAverageTrend = $weeklySalesPreviousDayRate = 0;
                            for ($counter = 1; $counter < 18; $counter++) {
                                $subtractStartDateValue = Carbon::now()->subDays($counter * 7)->format('Y-m-d');
                                if ($counter == 1)
                                    $subtractEndDateValue = Carbon::now()->format('Y-m-d');
                                else
                                    $subtractEndDateValue = Carbon::now()->subDays(($counter - 1) * 7)->format('Y-m-d');

                                $weeklySales = Calculated_sales_qty::where('product_id', $product['id'])->where('order_date', '>=', $subtractStartDateValue)->where('order_date', '<=', $subtractEndDateValue)->sum('total_qty');
                                $weeklySalesDayRate = round($weeklySales / 7, 6);
                                $trendPercentage = 0;
                                if ($counter > 1) {
                                    if (($weeklySalesDayRate - $weeklySalesPreviousDayRate) != 0 && $weeklySalesDayRate != 0) {
                                        $trendPercentage = round(($weeklySalesDayRate - $weeklySalesPreviousDayRate) / $weeklySalesDayRate, 2) * 100;
                                    } else {
                                        $trendPercentage = 0;
                                    }
                                    $trendDirection = $this->sign($trendPercentage);
                                    if ($trendDirection == $trendDirectionPrevious || $trendDirection == 0 || $trendDirectionPrevious == 0) {
                                        $weeklySalesTrendTotal += $trendPercentage;
                                    } else {
                                        $multiWeekAverageTrend = $weeklySalesTrendTotal / ($counter - 1);
                                        $breakStatus = 1;
                                    }
                                    $trendDirectionPrevious = $trendDirection;
                                } else {
                                    $weeklySalesTrendTotal += $trendPercentage;
                                }
                                $weeklySalesPreviousDayRate = $weeklySalesDayRate;
                                if ($breakStatus == 1)
                                    break;
                            }

                            //Trend Calculations
                            if ($previousYearSameMonthSales > 0 && $lastYearSameMonthSales > 0)
                                $yearOverYearHistoricalTrend = round((($previousYearSameMonthSales - $lastYearSameMonthSales) / $lastYearSameMonthSales *100), 2);
                            else
                                $yearOverYearHistoricalTrend = 0;

                            //Year Over year Trend
                            $previousMonthYearDate = Carbon::now()->subMonth(1)->subYear(1)->format('Y-m');
                            $salesTrend = Sales_total_trend_rate::where('prod_id', $product['id'])->where('month_year', $previousMonthYearDate)->first();
                            if (!empty($salesTrend) && $thirtyDaysSales > 0) {
                                $yearOveryearCurrent = round(($thirtyDaysSales - $salesTrend->sales_total) / $salesTrend->sales_total *100 , 2);
                            } else {
                                $yearOveryearCurrent = 0;
                            }
                            //Multi Month Trend
                            $multiMonthTrend = $monthToMonthAverageTrend;

                            if ($thirtyDaysDayRate > 0 && $previousThirtyDaysDayRate > 0)
                                $thirtyOverThirtyTrend = round((($thirtyDaysDayRate - $previousThirtyDaysDayRate) / $thirtyDaysDayRate)* 100, 2);
                            else
                                $thirtyOverThirtyTrend = 0;

                            //Multi Week Trend
                            $multiWeekTrend = $multiWeekAverageTrend;

                            if ($sevenDaysDayRate > 0 && $previousSevenDaysDayRate > 0)
                                $sevenOverSevenTrend = round((($sevenDaysDayRate - $previousSevenDaysDayRate) / $sevenDaysDayRate)* 100, 2);
                            else
                                $sevenOverSevenTrend = 0;

                            if($logicCalculations->supply_last_year_sales_percentage > 0){
                                $historicalRecentDifference = (100 - $logicCalculations->supply_last_year_sales_percentage) / 100;
                            }else {
                                $historicalRecentDifference = 1;
                            }

                            if($logicCalculations->reorder_last_year_sales_percentage > 0){
                                $historicalRecentReorderDifference = (100 - $logicCalculations->reorder_last_year_sales_percentage) / 100;
                            }else {
                                $historicalRecentReorderDifference = 1;
                            }

                            $supplyCalculationSalesDayRate = (($ThirtyDaysDayRateLastYear != 0 && $logicCalculations->supply_last_year_sales_percentage > 0) ? round(($ThirtyDaysDayRateLastYear * $logicCalculations->supply_last_year_sales_percentage / 100), 6) : 0) +
                                (($sevenDaysDayRate != 0 && $logicCalculations->supply_recent_last_7_day_sales_percentage > 0) ? round(($sevenDaysDayRate * round(($logicCalculations->supply_recent_last_7_day_sales_percentage * $historicalRecentDifference),6) / 100), 6) : 0) +
                                (($fourteenDaysDayRate != 0 && $logicCalculations->supply_recent_last_14_day_sales_percentage > 0) ? round(($fourteenDaysDayRate * round(($logicCalculations->supply_recent_last_14_day_sales_percentage * $historicalRecentDifference),6) / 100), 6) : 0) +
                                (($thirtyDaysDayRate != 0 && $logicCalculations->supply_recent_last_30_day_sales_percentage > 0) ? round(($thirtyDaysDayRate * round(($logicCalculations->supply_recent_last_30_day_sales_percentage * $historicalRecentDifference),6) / 100), 6) : 0);

                            $supplyCalculationsTrend = (($yearOverYearHistoricalTrend != 0 && $logicCalculations->supply_trends_year_over_year_historical_percentage > 0) ? round(($yearOverYearHistoricalTrend * $logicCalculations->supply_trends_year_over_year_historical_percentage / 100), 2) : 0) +
                                (($yearOveryearCurrent != 0 && $logicCalculations->supply_trends_year_over_year_current_percentage > 0) ? round(($yearOveryearCurrent * $logicCalculations->supply_trends_year_over_year_current_percentage / 100), 2) : 0) +
                                (($multiMonthTrend != 0 && $logicCalculations->supply_trends_multi_month_trend_percentage > 0) ? round(($multiMonthTrend * $logicCalculations->supply_trends_multi_month_trend_percentage / 100), 2) : 0) +
                                (($thirtyOverThirtyTrend != 0 && $logicCalculations->supply_trends_30_over_30_days_percentage > 0) ? round(($thirtyOverThirtyTrend * $logicCalculations->supply_trends_30_over_30_days_percentage / 100), 2) : 0) +
                                (($multiWeekTrend != 0 && $logicCalculations->supply_trends_multi_week_trend_percentage > 0) ? round(($multiWeekTrend * $logicCalculations->supply_trends_multi_week_trend_percentage / 100), 2) : 0) +
                                (($sevenOverSevenTrend != 0 && $logicCalculations->supply_trends_7_over_7_days_percentage > 0) ? round(($sevenOverSevenTrend * $logicCalculations->supply_trends_7_over_7_days_percentage / 100), 2) : 0);

                            $reorderCalculationsTrend = (($yearOverYearHistoricalTrend != 0 && $logicCalculations->reorder_trends_year_over_year_historical_percentage > 0) ? round(($yearOverYearHistoricalTrend * $logicCalculations->reorder_trends_year_over_year_historical_percentage / 100), 2) : 0) +
                                (($yearOveryearCurrent != 0 && $logicCalculations->reorder_trends_year_over_year_current_percentage > 0) ? round(($yearOveryearCurrent * $logicCalculations->reorder_trends_year_over_year_current_percentage / 100), 2) : 0) +
                                (($multiMonthTrend != 0 && $logicCalculations->reorder_trends_multi_month_trend_percentage > 0) ? round(($multiMonthTrend * $logicCalculations->reorder_trends_multi_month_trend_percentage / 100), 2) : 0) +
                                (($thirtyOverThirtyTrend != 0 && $logicCalculations->reorder_trends_30_over_30_days_percentage > 0) ? round(($thirtyOverThirtyTrend * $logicCalculations->reorder_trends_30_over_30_days_percentage / 100), 2) : 0) +
                                (($multiWeekTrend != 0 && $logicCalculations->reorder_trends_multi_week_trend_percentage > 0) ? round(($multiWeekTrend * $logicCalculations->reorder_trends_multi_week_trend_percentage / 100), 2) : 0) +
                                (($sevenOverSevenTrend != 0 && $logicCalculations->reorder_trends_7_over_7_days_percentage > 0) ? round(($sevenOverSevenTrend * $logicCalculations->reorder_trends_7_over_7_days_percentage / 100), 2) : 0);

                            $reorderCalculationSales = (($sevenDaysSales > 0 && $logicCalculations->reorder_recent_last_7_day_sales_percentage > 0) ? round(($sevenDaysSales * round(($logicCalculations->reorder_recent_last_7_day_sales_percentage * $historicalRecentReorderDifference),6) / 100), 6) : 0) +
                                (($fourteenDaysSales > 0 && $logicCalculations->reorder_recent_last_14_day_sales_percentage > 0) ? round(($fourteenDaysSales * round(($logicCalculations->reorder_recent_last_14_day_sales_percentage * $historicalRecentReorderDifference),6) / 100), 6) : 0) +
                                (($thirtyDaysSales > 0 && $logicCalculations->reorder_recent_last_30_day_sales_percentage > 0) ? round(($thirtyDaysSales * round(($logicCalculations->reorder_recent_last_30_day_sales_percentage * $historicalRecentReorderDifference),6) / 100), 6) : 0);

                            $overAllDayRate = round(($supplyCalculationsTrend * $supplyCalculationSalesDayRate / 100) + $supplyCalculationSalesDayRate, 6);
                            $overAllDaysSupply = $overAllProjectedReorderDate = 0;
                            if ($overAllDayRate > 0) {
                                $overAllDaysSupply = round(($TotalInventory / $overAllDayRate), 6);
                                $startDate = $overAllProjectedOrderStartDateRange = Carbon::now()->addDays($overAllDaysSupply);
                                $overAllProjectedOrderEndDateRange = Carbon::now()->addDays($overAllDaysSupply + $orderVolume)->format('Y-m-d');
                                $blackoutDays = 0;
                                if($blackOutDayFlat == 0){
                                    $blackoutDays = Supplier_blackout_date::where('supplier_id',$productSettings->id)->where('blackout_date','>=',Carbon::now()->format('Y-m-d'))->where('blackout_date','<=',$overAllProjectedOrderStartDateRange->format('Y-m-d'))->count();
                                } else{
                                    $blackoutDays = Vendor_blackout_date::where('vendor_id',$productSettings->id)->where('blackout_date','>=',Carbon::now()->format('Y-m-d'))->where('blackout_date','<=',$overAllProjectedOrderStartDateRange->format('Y-m-d'))->count();
                                }
                                $addDaysForProjectedDate = round($overAllDaysSupply - $TotalLeadTime - $blackoutDays);
                                $overAllProjectedReorderDate = Carbon::now()->addDays($addDaysForProjectedDate)->format('Y-m-d');
                                $todaySDate = Carbon::now();
                                $overAllDaysLeftToOrder = $todaySDate->diffInDays($overAllProjectedReorderDate, false);
                                $count = $counter = $breakLoop = $projectedReorderQty = 0;
                                $projectedReorderQty += $reorderCalculationSales;
                                if ($logicCalculations->reorder_last_year_sales_percentage > 0) {
                                    while ($counter != $orderVolume) {
                                        $calculatedDays = 0;
                                        $monthCalculation = Carbon::parse($startDate)->addMonths($count)->format('Y-m-t');
                                        if ($overAllProjectedOrderEndDateRange <= $monthCalculation) {
                                            $calculatedDays = Carbon::parse($overAllProjectedOrderEndDateRange)->format('d');
                                            $breakLoop = 1;
                                        } else if ($counter == 0) {
                                            $calculatedDays = $overAllProjectedOrderStartDateRange->diffInDays($monthCalculation) + 1;
                                            $monthCalculation = Carbon::parse($startDate)->addMonths($count);
                                        } else {
                                            $calculatedDays = Carbon::parse($startDate)->addMonths($count)->format('t');
                                        }
                                        $counter += (int)$calculatedDays;
                                        $count++;
                                        $newCalculatedMonthYear = Carbon::parse($monthCalculation)->subYear(1)->format('Y-m');
                                        $salesTrendValue = Sales_total_trend_rate::where('prod_id', $product['id'])->where('month_year', $newCalculatedMonthYear)->first();
                                        if ($salesTrendValue) {
                                            $salesTrendValue = round($salesTrendValue->day_rate * $logicCalculations->reorder_last_year_sales_percentage / 100, 6);
                                            if ($salesTrendValue) {
                                                $projectedReorderQty += ($salesTrendValue * $reorderCalculationsTrend / 100) + ($salesTrendValue * $calculatedDays);
                                            }
                                        }
                                        if ($breakLoop == 1) {
                                            break;
                                        }
                                    }
                                }
                            }
                            //Saving Calculated Logic
                            $dailyLogicCalculations = Daily_logic_Calculations::where('prod_id', $product['id'])->first();
                            if (empty($dailyLogicCalculations)) {
                                $dailyLogicCalculations = new Daily_logic_Calculations();
                                $dailyLogicCalculations->user_marketplace_id = $product['user_marketplace_id'];
                                $dailyLogicCalculations->prod_id = $product['id'];
                            }
                            $dailyLogicCalculations->total_inventory = $TotalInventory;
                            $dailyLogicCalculations->lead_time_amazon = $TotalLeadTime;
                            $dailyLogicCalculations->order_frequency = $orderVolume;
                            $dailyLogicCalculations->day_rate = $overAllDayRate;
                            $dailyLogicCalculations->days_supply = $overAllDaysSupply;
                            $dailyLogicCalculations->blackout_days = 0;
                            $dailyLogicCalculations->projected_reorder_date = ($overAllProjectedReorderDate ? $overAllProjectedReorderDate : 0);
                            $dailyLogicCalculations->projected_reorder_qty = $projectedReorderQty;
                            $dailyLogicCalculations->days_left_to_order = $overAllDaysLeftToOrder;
                            $dailyLogicCalculations->projected_order_date_range = $overAllProjectedOrderStartDateRange->format('Y-m-d') . ' - ' . $overAllProjectedOrderEndDateRange;
                            $dailyLogicCalculations->sevendays_sales = $sevenDaysSales;
                            $dailyLogicCalculations->sevenday_day_rate = $sevenDaysDayRate;
                            $dailyLogicCalculations->fourteendays_sales = $fourteenDaysSales;
                            $dailyLogicCalculations->fourteendays_day_rate = $fourteenDaysDayRate;
                            $dailyLogicCalculations->thirtydays_sales = $thirtyDaysSales;
                            $dailyLogicCalculations->thirtyday_day_rate = $thirtyDaysDayRate;
                            $dailyLogicCalculations->sevendays_previous = $previousSevenDaysSales;
                            $dailyLogicCalculations->sevendays_previous_day_rate = $previousSevenDaysDayRate;
                            $dailyLogicCalculations->thirtydays_previous = $previousThirtyDaysSales;
                            $dailyLogicCalculations->thirtydays_previous_day_rate = $previousThirtyDaysDayRate;
                            $dailyLogicCalculations->thirtydays_last_year = $thirtyDaysSalesLastyear;
                            $dailyLogicCalculations->thirtydays_last_year_day_rate = $ThirtyDaysDayRateLastYear;
                            $dailyLogicCalculations->save();
                        }
                    }
                }
            }
            Log::error("Error while executing job CalculateTrend for account {$this->marketplace->id}");
            $this->log_it("Done CalculateTrend Calculations");
        } catch (\Exception $ex) {
            Log::error("Could not execute job CalculateTrend: " . $ex->getMessage());
            $this->log_it($ex->getTraceAsString());
            throw new \Exception($ex->getMessage());
        }
    }

    public function sign($number)
    {
        return ($number > 0) ? 1 : (($number < 0) ? -1 : 0);
    }
}

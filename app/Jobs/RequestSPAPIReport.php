<?php

namespace App\Jobs;

use App\Models\Amazon_requestlog;
use App\Models\SPApiUserMarketplace;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Bus\Queueable;

class RequestSPAPIReport extends Jobspapi implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $marketplace;
    protected $report_type;
    protected $from_date_time;
    protected $to_date_time;
    protected $amazon_date_itetration;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(SPApiUserMarketplace $marketplace, $report_type, $amzDateitetration = null)
    {
        Log::debug("Initializing RequestReport for account {$marketplace->id} with report type $report_type");

        $this->marketplace = $marketplace;
        $this->report_type = $report_type;
        $this->amazon_date_itetration = $amzDateitetration;
        if(empty($amzDateitetration)){
            $this->from_date_time = Carbon::yesterday();
            $this->to_date_time = Carbon::now();
        } else{
            $this->from_date_time = $amzDateitetration['startDate'];
            $this->to_date_time = $amzDateitetration['endDate'];
        }
        if(empty($amzDateitetration) && $report_type == '_GET_FBA_FULFILLMENT_CURRENT_INVENTORY_DATA_'){
            $this->from_date_time = Carbon::now()->subDays(2);
            $this->to_date_time = Carbon::now();
        }
        if(empty($amzDateitetration) && $report_type == '_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_LAST_UPDATE_'){
            if(Carbon::now()->format('h') <= 5) {
                $this->from_date_time = Carbon::now()->subDays(5);
                $this->to_date_time = Carbon::now();
            }
        }
        if(Carbon::now()->format('d') % 5 == 0 && empty($amzDateitetration) && $report_type != '_GET_FBA_MYI_UNSUPPRESSED_INVENTORY_DATA_'){
            if(Carbon::now()->format('h') <= 5){
                $this->from_date_time = Carbon::now()->subDays(6);
                $this->to_date_time = Carbon::now();
            }
        }
        parent::__construct();
    }


    /**
     * @return \ClouSale\AmazonSellingPartnerAPI\Configuration
     */
    private function getConfiguration($options)
    {
        $config = \ClouSale\AmazonSellingPartnerAPI\Configuration::getDefaultConfiguration();
        $config->setHost(\ClouSale\AmazonSellingPartnerAPI\SellingPartnerEndpoint::$NORTH_AMERICA);
        $config->setRegion(\ClouSale\AmazonSellingPartnerAPI\SellingPartnerRegion::$NORTH_AMERICA);
        $this->initRequest($config, $options);
        return $config;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Log::debug("Executing job RequestReport for account {$this->marketplace->id}");
            $requestConfiguration = $this->getConfiguration($this->marketplace);
            $service = $this->getSPAPIReport($requestConfiguration);
            $apiReportSpecification = $this->getReportSepc();
            $apiReportSpecification->setDataStartTime(str_replace(' ','T',$this->from_date_time));
            $apiReportSpecification->setDataEndTime(str_replace(' ','T',$this->to_date_time));
            $apiReportSpecification->setMarketplaceIds(array($this->marketplace->market_place_developer->marketplace_id));
            $apiReportSpecification->setReportType($this->report_type);
            $apiReportSpecification->setReportOptions('ShowSalesChannel%3Dtrue');
            $response = $service->createReport($apiReportSpecification);
            $payLoad = $response->getPayload();
            $requestReportID = $payLoad->getReportId();
            if(!empty($requestReportID)){
                $amazon_report = new Amazon_requestlog([
                    'user_marketplace_id' => $this->marketplace->id,
                    'request_id' => $requestReportID,
                    'status' => Amazon_requestlog::$IN_QUEUESPAPI,
                    'last_checked' => Carbon::now(),
                    'report_type' => $this->report_type,
                    'amz_dateitetration_id' => (isset($this->amazon_date_itetration->id) ? $this->amazon_date_itetration->id : 0)
                ]);
                $amazon_report->save();

                Log::debug("Job RequestReport for account {$this->marketplace->id} with request id $requestReportID executed with status PROCESSING");
                $this->log_it("Done Requesting Report");
            }else {
                $responseMessage = $response['error'];
                Log::error("Error while executing job RequestReport for account {$this->marketplace->id}: $responseMessage");
                if ($response['code'] == 400 || $response['code'] == 401) {
                    $amazon_report = new Amazon_requestlog([
                        'user_marketplace_id' => $this->marketplace->id,
                        'request_id' => '',
                        'status' => 'Invalid Credentials',
                        'last_checked' => Carbon::now(),
                        'request_response' => $responseMessage,
                        'report_type' => $this->report_type,
                        'processed' => 1
                    ]);
                    $amazon_report->save();
                    $this->log_it($responseMessage);
                }
            }
        } catch (\Exception $ex) {
            Log::error("Could not execute job RequestReport: " . $ex->getMessage());
            $this->log_it($ex->getTraceAsString());
            throw new \Exception($ex->getMessage());
        }
    }
}

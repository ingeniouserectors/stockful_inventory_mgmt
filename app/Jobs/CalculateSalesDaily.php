<?php

namespace App\Jobs;

use App\Models\Amzdateiteration;
use App\Models\Calculated_order_qty;
use App\Models\Mws_order;
use App\Models\Mws_product;
use App\Models\Sales_total_trend_rate;
use App\Models\Usermarketplace;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Bus\Queueable;

class CalculateSalesDaily extends Job implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    public $marketplace;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Usermarketplace $marketplace)
    {
        Log::debug("Initializing Calculate Sales Daily for account {$marketplace->id}");

        $this->marketplace = $marketplace;
        parent::__construct();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Log::debug("Executing job CalculateSalesDaily for account {$this->marketplace->id}");
            Mws_order::where('user_marketplace_id', $this->marketplace->id)
                ->select('id', 'order_status', 'quantity', 'purchase_date', 'product_id', 'user_marketplace_id', 'calculation_status')
                ->where('calculation_status', 0)
                ->chunk(500, function($salesValues) {
                    $this->calculations_sales($salesValues);
            });
            Mws_order::where('user_marketplace_id', $this->marketplace->id)
                ->select('id', 'order_status', 'quantity', 'purchase_date', 'product_id', 'user_marketplace_id', 'calculation_status')
                ->where('calculation_status', 1)
                ->where('order_status', 'Shipped')
                ->chunk(500, function($salesValues) {
                    $this->calculations_sales($salesValues);
                });
            Mws_order::where('user_marketplace_id', $this->marketplace->id)
                ->select('id', 'order_status', 'quantity', 'purchase_date', 'product_id', 'user_marketplace_id', 'calculation_status')
                ->where('calculation_status', 2)
                ->where('order_status', 'Shipped')
                ->chunk(500, function($salesValues) {
                    $this->calculations_sales($salesValues);
                });
            Log::error("Error while executing job CalculateTrend for account {$this->marketplace->id}");
            $this->log_it("Done CalculateSalesDaily CRON");
        } catch (\Exception $ex) {
            Log::error("Could not execute job CalculateSalesDaily: " . $ex->getMessage());
            $this->log_it($ex->getTraceAsString());
            throw new \Exception($ex->getMessage());
        }
    }

    public function calculations_sales($salesValues)
    {
        foreach ($salesValues as $sales) {
            $salesDate = TimeToServer($sales->purchase_date);
            $totalPendingQty = $totalShippedQty = $totalShippingQty = 0;
            switch ($sales->order_status) {
                case 'Pending':
                    $totalPendingQty = $sales->quantity;
                    break;
                case 'Shipped':
                    $totalShippedQty = $sales->quantity;
                    break;
                case 'Shipping':
                    $totalShippingQty = $sales->quantity;
                    break;
            }
            $calculateOrderQty = Calculated_order_qty::where('product_id', $sales->product_id)
                ->where('order_date', date('Y-m-d', strtotime($salesDate)))->first();
            if (!empty($calculateOrderQty)) {
                if ($sales->calculation_status == 1 && $sales->order_status == 'Shipped') {
                    $calculateOrderQty->total_qty_pending = $calculateOrderQty->total_qty_pending - $totalPendingQty;
                }
                if ($sales->calculation_status == 2 && $sales->order_status == 'Shipped') {
                    $calculateOrderQty->total_qty_shipping = $calculateOrderQty->total_qty_shipping - $totalShippingQty;
                }
//                $calculateOrderQty->total_qty_pending = $calculateOrderQty->total_qty_pending + $totalPendingQty;
//                $calculateOrderQty->total_qty_shipping = $calculateOrderQty->total_qty_shipping + $totalShippingQty;
                $calculateOrderQty->total_qty_shipped = $calculateOrderQty->total_qty_shipped + $totalShippedQty;
                $calculateOrderQty->total_qty_shipping_shipped = $calculateOrderQty->total_qty_shipping_shipped + $totalShippingQty + $totalShippedQty;
                $calculateOrderQty->total_qty = $calculateOrderQty->total_qty + $totalShippingQty + $totalShippedQty + $totalPendingQty;
            } else {
                $calculateOrderQty = new Calculated_order_qty();
                $calculateOrderQty->user_marketplace_id = $sales->user_marketplace_id;
                $calculateOrderQty->product_id = $sales->product_id;
                $calculateOrderQty->order_date = date('Y-m-d', strtotime($salesDate));
                $calculateOrderQty->total_qty_pending = $totalPendingQty;
                $calculateOrderQty->total_qty_shipping = $totalShippingQty;
                $calculateOrderQty->total_qty_shipped = $totalShippedQty;
                $calculateOrderQty->total_qty_shipping_shipped = $totalShippingQty + $totalShippedQty;
                $calculateOrderQty->total_qty = $totalShippingQty + $totalShippedQty + $totalPendingQty;
            }
            $calculateOrderQty->save();
            if ($sales->order_status == 'Shipped') {
                $calculationStatus = 3;
            } elseif ($sales->order_status == 'Shipping') {
                $calculationStatus = 2;
            } else {
                $calculationStatus = 1;
            }
            Mws_order::where('id', $sales->id)->update(['converted_date' => $salesDate, 'calculation_status' => $calculationStatus]);
        }
    }
}

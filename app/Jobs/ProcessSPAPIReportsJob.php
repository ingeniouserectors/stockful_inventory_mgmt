<?php

namespace App\Jobs;

use App\Models\Amazon_requestlog;
use App\Models\Amzdateiteration;
use App\Models\Mws_adjustment_inventory_data;
use App\Models\Mws_afn_inventory_data;
use App\Models\Mws_excess_inventory_data;
use App\Models\Mws_inventory_details;
use App\Models\Mws_order;
use App\Models\Mws_order_returns;
use App\Models\Mws_product;
use App\Models\Mws_reserved_inventory;
use App\Models\Mws_unsuppressed_inventory_data;
use App\Models\SPApiUserMarketplace;
use App\Models\Mws_restock_inventory_recommendations;
use Aws\Crypto\AesDecryptingStream;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class ProcessSPAPIReportsJob extends Jobspapi implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $marketplace;
    /**
     * @var \MarketplaceWebService_Client
     */
    protected $service;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(SPApiUserMarketplace $marketplace)
    {
        $this->marketplace = $marketplace;
        parent::__construct();
    }


    /**
     * @return \ClouSale\AmazonSellingPartnerAPI\Configuration
     */
    private function getConfiguration($options)
    {
        $config = \ClouSale\AmazonSellingPartnerAPI\Configuration::getDefaultConfiguration();
        $config->setHost(\ClouSale\AmazonSellingPartnerAPI\SellingPartnerEndpoint::$NORTH_AMERICA);
        $config->setRegion(\ClouSale\AmazonSellingPartnerAPI\SellingPartnerRegion::$NORTH_AMERICA);
        $this->initRequest($config, $options);
        return $config;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            foreach (Amazon_requestlog::getPendingSPAPIReports($this->marketplace->id) as $Amazon_requestlog) {
                try {
                    $this->processReport($Amazon_requestlog);

                } catch (\Exception $ex) {
                    Log::error("Could not process ProcessReportsJob for account {$this->marketplace->id}: " . $ex->getMessage());
                    $this->log_it($ex->getMessage());
                    $this->log_it($ex->getTraceAsString());
                    throw new \Exception($ex->getMessage());
                }
            }

        } catch (\Exception $ex) {
            Log::error("ProcessReportsJob for account {$this->marketplace->id} failed: " . $ex->getMessage());
            $this->log_it($ex->getTraceAsString());
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     * @param Amazon_requestlog $amazon_report
     */
    private function processReport(Amazon_requestlog $amazon_report)
    {
        Log::debug("Processing amazon_report {$amazon_report->id}");

        if (!$this->canProcess($amazon_report)) {
            Log::error("Could not process report {$amazon_report->id}");
            print("this report is not processable " . $amazon_report->id);
            return;
        }
        switch ($amazon_report->report_type) {
            case Amazon_requestlog::$SP_API_MERCHANT_LISTINGS:
                Mws_product::processSPAPIReport($amazon_report, $this->readReport($amazon_report), $this->marketplace);
                break;

            case Amazon_requestlog::$ORDER_DATA_LAST_UPDATE_GENERAL:
                Mws_order::processSPAPIReport($amazon_report, $this->readReport($amazon_report), $this->marketplace);
                break;
            case Amazon_requestlog::$RETURNS_DATA_BY_RETURN_DATE:
                Mws_order_returns::processSPAPIReport($amazon_report, $this->readReport($amazon_report), $this->marketplace);
                break;
            case Amazon_requestlog::$SP_API_EXCESS_INVENTORY_DATA_LISTINGS:
                Mws_excess_inventory_data::processSPAPIReport($amazon_report, $this->readReport($amazon_report), $this->marketplace);
                break;

            case Amazon_requestlog::$SP_API_AFN_INVENTORY_DATA:
                Mws_afn_inventory_data::processSPAPIReport($amazon_report, $this->readReport($amazon_report), $this->marketplace);
                break;
            case Amazon_requestlog::$SP_API_ADJUSTMENT_DATA:
                Mws_adjustment_inventory_data::processSPAPIReport($amazon_report, $this->readReport($amazon_report), $this->marketplace);
                break;
            case Amazon_requestlog::$SP_API_DAILY_INVENTORY_REPORT:
                Mws_inventory_details::processSPAPIReport($amazon_report, $this->readReport($amazon_report), $this->marketplace);
                break;
            case Amazon_requestlog::$SP_API_RESERVED_INVENTORY_DATA:
                Mws_reserved_inventory::processSPAPIReport($amazon_report, $this->readReport($amazon_report), $this->marketplace);
                break;

            case Amazon_requestlog::$SP_API_UNSUPPRESSED_INVENTORY_DATA:
                Mws_unsuppressed_inventory_data::processSPAPIReport($amazon_report, $this->readReport($amazon_report), $this->marketplace);
                break;

            case Amazon_requestlog::$SP_API_RESTOCK_INVENTORY_RECOMMENDATIONS:
                Mws_restock_inventory_recommendations::processSPAPIReport($amazon_report, $this->readReport($amazon_report), $this->marketplace);
                break;
            default:
                Log::error("Can't process reports of type {$amazon_report->report_type}");
        }

        $amazon_report->save();
        $this->log_it("Amazon Report {$amazon_report->id} is processed");
        Log::debug("Amazon Report {$amazon_report->id} has been processed");
    }

    /**
     * @param Amazon_requestlog $amazon_report
     * @return mixed
     */
    private function readReport(Amazon_requestlog $amazon_report)
    {
        $requestConfiguration = $this->getConfiguration($this->marketplace);
        $service = $this->getSPAPIReport($requestConfiguration);
        $finalResult = $service->getReportDocument($amazon_report->report_id);
        $encryptedText = file_get_contents($finalResult->getPayload()->getUrl());
        $finalData = \AESCryptoStreamFactory::decrypt($encryptedText, base64_decode($finalResult->getPayload()->getEncryptionDetails()->getKey(), true), base64_decode($finalResult->getPayload()->getEncryptionDetails()->getInitializationVector(), true));
        if ($finalData) {
            return $finalData;
        }
        return null;
    }


    /**
     * @param Amazon_requestlog $amazon_report
     * @param bool|true $check_status
     * @return bool
     */
    private function canProcess(Amazon_requestlog $amazon_report, $check_status = true)
    {
        if ($amazon_report->status == Amazon_requestlog::$DONESPAPI && $amazon_report->processed == 0) {
           return true;
        } else if ($check_status && in_array($amazon_report->status, [Amazon_requestlog::$IN_PROGRESSSPAPI, Amazon_requestlog::$IN_QUEUESPAPI])) {
            $requestConfiguration = $this->getConfiguration($this->marketplace);
            $service = $this->getSPAPIReport($requestConfiguration);
            $reportResponse = $service->getReport($amazon_report->request_id);
            $payloadResponse = $reportResponse->getPayload();
            $amazon_report->status = $payloadResponse->getProcessingStatus();
            if ($amazon_report->status == Amazon_requestlog::$DONESPAPI) {
                $amazon_report->available_at = Carbon::now();
                $amazon_report->report_id = $payloadResponse->getReportDocumentId();
            } elseif ($amazon_report->status == Amazon_requestlog::$NO_DATASPAPI) {
                $amzDateitetration = Amzdateiteration::where('id', $amazon_report->amz_dateitetration_id)->first();
                $amazon_report->available_at = Carbon::now();
                $amazon_report->processed = 1;
                if(!empty($amzDateitetration)){
                    switch ($amazon_report->report_type) {
                        case Amazon_requestlog::$ORDER_MERCHANGT_LISTINGS:
                            $amzDateitetration->OrderStatus = '1';
                            break;
                        case Amazon_requestlog::$MWS_DAILY_INVENTORY_REPORT:
                            $amzDateitetration->InventoryStatus = '1';
                            break;
                        case Amazon_requestlog::$MWS_ORDER_RETURNS_LISTINGS:
                            $amzDateitetration->ReturnStatus = '1';
                            break;
                    }
                    $amzDateitetration->save();
                }
            } else if ($amazon_report->status == Amazon_requestlog::$CANCELLEDSPAPI || $amazon_report->status == Amazon_requestlog::$FATALSPAPI) {
                $amazon_report->processed = 1;
                /*$amzDateitetration = Amzdateiteration::where('id', $amazon_report->amz_dateiteration_id)->first();
                $requestJob = new RequestReport($this->marketplace, $amazon_report->report_type,$amzDateitetration);
                dispatch($requestJob)->onQueue('low'); */
                $data['request_id']= $amazon_report->request_id;
                $data['status'] = $amazon_report->status;
                $data['report_type'] = $amazon_report->report_type;
                /*Mail::send('emailtemplate.cancelreport', $data, function($message) use ($data){
                    $message->to(env('FROM_EMAIL'), '')
                        ->subject('Report cancelled');
                });
                if (Mail::failures()) {
                    Log::error('Cancel report mail not sent');
                }else{
                    Log::debug('Mail sent for cancel report');
                }*/
            }
            $amazon_report->save();
            return $this->canProcess($amazon_report, false);
        } else if ($amazon_report->status == Amazon_requestlog::$CANCELLEDSPAPI || $amazon_report->status == Amazon_requestlog::$FATALSPAPI) {
            $amazon_report->processed = 1;
            /*$amzDateitetration = Amzdateiteration::where('id', $amazon_report->amz_dateiteration_id)->first();
            $requestJob = new RequestReport($this->marketplace, $amazon_report->report_type,$amzDateitetration);
            dispatch($requestJob)->onQueue('low'); */
            $data['request_id']= $amazon_report->request_id;
            $data['status'] = $amazon_report->status;
            $data['report_type'] = $amazon_report->report_type;
            /*Mail::send('emailtemplate.cancelreport', $data, function($message) use ($data){
                $message->to(env('FROM_EMAIL'), '')
                    ->subject('Report cancelled');
            });
            if (Mail::failures()) {
                Log::error('Cancel report mail not sent');
            }else{
                Log::debug('Mail sent for cancel report');
            }*/
            $amazon_report->save();
            return $this->canProcess($amazon_report, false);
        }
        return false;
    }
}

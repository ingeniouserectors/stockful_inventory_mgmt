<?php

namespace App\Jobs;
use App\Models\Amazon_requestlog;
use App\Models\Mws_unsuppressed_historical_inventory;
use App\Models\Usermarketplace;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Bus\Queueable;
class HistoricalUnsuppressed extends Job implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $marketplace;
    protected $report_type;
    protected $service;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Usermarketplace $marketplace)
    {
        Log::debug("Initializing Historical Unsuppressed for account {$marketplace->id}");

        $this->marketplace = $marketplace;
        parent::__construct();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Log::debug("Executing job Historical Unsuppressed for account {$this->marketplace->id}");
            $reports = Amazon_requestlog::where('user_marketplace_id','3')->where('report_type','_GET_FBA_MYI_UNSUPPRESSED_INVENTORY_DATA_')->whereNotNull('available_at')->whereNotNull('report_id')->where('status','_DONE_')->groupBy(\DB::raw('DATE(created_at)'))->orderBy('available_at')->get()->toArray();
            $previousdata = $nextdata = array();
            foreach ($reports as $key=>$newReports){
                $date = explode(' ',$newReports['created_at']);
                if($date[1] <= date('h:i:s',strtotime('7:30:00'))) {
                    $previousdata[$newReports['id']]= date('Y-m-d',strtotime('-1 day', strtotime($newReports['created_at'])));
                    $actualDate[$newReports['id']]= date('Y-m-d', strtotime($newReports['created_at']));
                } else {
                    $currentdata[$newReports['id']] = date('Y-m-d', strtotime($newReports['created_at']));
                }
            }
            $newData = array_unique ($previousdata + $currentdata);
            $this->service = $this->getReportsClient();
            foreach ($newData as $key=>$data){
                try {
                    $reportData = Amazon_requestlog::where('id',$key)->where('processed',1)->first();
                    if(empty($reportData))
                        continue;
                    $reportData['previousDate'] = $data;
                    $this->processReport($reportData);
                } catch (\Exception $ex) {
                    Log::error("Could not process Historical Unsuppressed for account {$this->marketplace->id}: " . $ex->getMessage());
                    $this->log_it($ex->getMessage());
                    $this->log_it($ex->getTraceAsString());
                    throw new \Exception($ex->getMessage());
                }
            }
            Log::error("Error while executing job Historical Unsuppressed for account {$this->marketplace->id}");
            $this->log_it("Done Historical Unsuppressed");
        } catch (\Exception $ex) {
            Log::error("Could not execute job Historical Unsuppressed: " . $ex->getMessage());
            $this->log_it($ex->getTraceAsString());
            throw new \Exception($ex->getMessage());
        }
    }
    private function processReport($amazon_report)
    {
        Log::debug("Processing amazon_report {$amazon_report['id']}");
        Mws_unsuppressed_historical_inventory::processReport($amazon_report, $this->readReport($amazon_report), $this->marketplace);
        $this->log_it("Amazon Report {$amazon_report['id']} is processed");
        Log::debug("Amazon Report {$amazon_report['report_id']} has been processed");
    }
    private function readReport($amazon_report)
    {
        $request = new \MarketplaceWebService_Model_GetReportRequest();
        $this->initRequest($request, 'setMerchant', 'setMarketplace');
        $request->setReport(@fopen('php://memory', 'rw+'));
        $request->setReportId($amazon_report['report_id']);
        $response = $this->service->getReport($request);

        if ($response->isSetGetReportResult()) {
            return $request->getReport();
        }

        return null;
    }

}

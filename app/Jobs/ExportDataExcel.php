<?php

namespace App\Jobs;
use App\Models\Mws_product;
use App\Models\Usermarketplace;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Bus\Queueable;
use App\Models\Calculated_sales_qty;
use App\Models\Mws_calculated_inventory_daily;
use Carbon\Carbon;
use App\Models\Inbound_shipment_items;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Models\Mws_unsuppressed_historical_inventory;


class ExportDataExcel extends Job implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    protected $marketplace;
    protected $report_type;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Usermarketplace $marketplace)
    {
        Log::debug("Initializing Export data for account {$marketplace->id}");

        $this->marketplace = $marketplace;
        parent::__construct();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Log::debug("Executing job Export Data for account {$this->marketplace->id}");
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', '-1');
            $objPHPExcel = new Spreadsheet();
            $sheetsstitle=array("Inventory","Inbound BreakDown","Sales History","Order Returns");
            $skuArray = [
                'CB4-GD-IS1-12-03',
                'CP3-GD1-IS1-12-07',
                'CB3-GD-IS1-12-03',
                'CP2-GD1-IS1-12-07',
                'CB2-GD-IS1-12-03',
                'CB9-GD-IS1-07-25',
                'CB10-GD-IS1-11-18',
                'CP4-GD-IS1-11-18',
                'WB1-GD-IS1-5-17',
                'WB2-GD-IS1-5-17',
                'WB3-GD-IS1-5-17',
                'LWB4-GD-IS1-05-20',
                'MW1-GD-IS1-08-16',
                'MW2-GD-IS1-08-16',
                'MW3-GD-IS1-08-14',
                'MW4-GD-IS1-05-20',
                'WB4-GD-IS1-9-13',
                'WB5-GD-IS1-9-17',
                'WB6-GD-IS1-9-17',
                'WB7-GD-IS1-05-20',
                'CB5-GD-IS1-05-03',
                'CB6-GD-IS1-05-03',
                'CB7-GD-IS1-04-08',
                'CB8-GD-IS1-05-08',
                'CB11-GD-IS1-12-19',
                'TT6-GD-IS1-12-19',
                'TT7-GD-IS1-12-19',
                '6G-OHEF-0LNU',
                'CR2-GD-IS1-08-09',
                'CR3-GD-IS1-04-11',
                'CR4-GD-IS1-04-11',
                'CR5-GD-IS1-03-20',
                'SCR1-GD-IS1-08-09',
                'SCR2-GD-IS1-08-09',
                'SCR3-GD-IS1-04-11',
                'SCR4-GD-IS1-04-11',
                'SCR5-GD-IS1-04-11',
                'CMR1- GD-IS1-08-10',
                'CMR2- GD-IS1-08-10',
                'CMR3- GD-IS1-11-18',
                'BCM16-GD-IS1',
                'BCM20-GD-IS1',
                'BCM24-GD-IS1',
                'BCM30-GD-IS1',
                'BCM36-GD-IS1',
                'GCM16-GD-IS1',
                'GCM20-GD-IS1',
                'GCM24-GD-IS1',
                'GCM30-GD-IS1',
                'GCM36-GD-IS1',
                'DOS-GD-IS1-11-18',
                'DOS2-GD-IS1-02-20',
                'DOS3-GD-IS1-04-20',
                'FC1-GD-IS1-11-18',
                'FC2-GD-IS1-11-18',
                'FC3-GD-IS1-02-20',
                'CCR1-GD-IS1-11-18',
                'PCB1-GD-IS1-11-18',
                'PCB2-GD-IS1-11-18',
                'BGCM27.5-GD-IS1-11-18',
                'BCM27.5-GD-IS1-05-09',
                'GUMM-GD-IS1-11-19',
                'BUMM-GD-IS1-11-19',
                'US1-GD-IS1-11-19',
                'US2-GD-IS1-08-19',
                'LCM1-GD1-12-05',
                'LCM2-GD1-12-05',
                'LCM3-GD1-12-05',
                'SS1-GD-IS1-03-15',
                'SS2-GD-IS1-11-12',
                'TT8-GD-IS1-08-20',
                'TT11-GD-IS1-08-20' ];
            $get_all_product_sku = Mws_product::with(['mws_reserved_inventory','mws_afn_inventory','mws_unsuppressed_inventory_data','daily_logic_calculations','mws_order_returns'])->whereIn('sku', $skuArray)->where(array('user_marketplace_id' => $this->marketplace->id))->selectRaw('sku,id')->get()->toArray();
            $i=0;
            while ($i < 4) {
                // Add new sheet
                $objWorkSheet = $objPHPExcel->createSheet($i); //Setting index when creating
                if($i == 0){
                    $objWorkSheet->setCellValue('A1', 'SKU');
                    $objWorkSheet->setCellValue('B1', 'Quantity Available');
                    $objWorkSheet->setCellValue('C1', 'Afn-fulfilled-quantity');
                    $objWorkSheet->setCellValue('D1', 'Current Stock');
                    $objWorkSheet->setCellValue('E1', 'FC Transfer');
                    $objWorkSheet->setCellValue('F1', 'FC Processing');
                    $objWorkSheet->setCellValue('G1', 'Customer Order');
                    $objWorkSheet->setCellValue('H1', 'Reserved Qty');
                    $objWorkSheet->setCellValue('I1', 'Afn Research qty');
                    $objWorkSheet->setCellValue('J1', 'Afn Unsellable qty');
                    $objWorkSheet->setCellValue('K1', 'Afn-warehouse-qty');
                    $objWorkSheet->setCellValue('L1', 'Amazon Inbound Receving Qty');
                    $objWorkSheet->setCellValue('M1', 'Amazon Inbound Shipment Qty');
                    $objWorkSheet->setCellValue('N1', 'Amazon Inbound Working Qty');
                    $objWorkSheet->setCellValue('O1', 'AFN Total Qty');
                    $column = 2;
                    foreach ($get_all_product_sku as $key=>$result) {
                        $objWorkSheet->setCellValue('A' . $column, $result['sku']);
                        $objWorkSheet->setCellValue('B' . $column, $result['mws_afn_inventory']['quantity_available']);
                        $objWorkSheet->setCellValue('C' . $column, $result['mws_unsuppressed_inventory_data']['afn_fulfillable_qty']);
                        $objWorkSheet->setCellValue('D' . $column, '');
                        $objWorkSheet->setCellValue('E' . $column, $result['mws_reserved_inventory']['reserved_fc_transfer']);
                        $objWorkSheet->setCellValue('F' . $column, $result['mws_reserved_inventory']['reserved_fc_processing']);
                        $objWorkSheet->setCellValue('G' . $column, $result['mws_reserved_inventory']['reserved_customer_orders']);
                        $objWorkSheet->setCellValue('H' . $column, $result['mws_reserved_inventory']['reserved_qty']);
                        $objWorkSheet->setCellValue('I'.$column,$result['mws_unsuppressed_inventory_data']['afn_research_qty']);
                        $objWorkSheet->setCellValue('J'.$column,$result['mws_unsuppressed_inventory_data']['afn_unsellable_qty']);
                        $objWorkSheet->setCellValue('K'.$column,$result['mws_unsuppressed_inventory_data']['afn_warehouse_qty']);
                        $objWorkSheet->setCellValue('L' . $column, $result['mws_unsuppressed_inventory_data']['afn_inbound_receving_qty']);
                        $objWorkSheet->setCellValue('M'.$column,$result['mws_unsuppressed_inventory_data']['afn_inbound_shipment_qty']);
                        $objWorkSheet->setCellValue('N'.$column,$result['mws_unsuppressed_inventory_data']['afn_inbound_working_qty']);
                        $objWorkSheet->setCellValue('O'.$column,$result['mws_unsuppressed_inventory_data']['afn_total_qty']);
                        $column++;
                    }
                }
                if ($i == 1) {
                    //SellerSettlement data
                    $objWorkSheet->setCellValue('A1', 'SKU');
                    $objWorkSheet->setCellValue('B1', 'Name  ');
                    $objWorkSheet->setCellValue('C1', 'Shipment ID ');
                    $objWorkSheet->setCellValue('D1', 'Shipment Status ');
                    $objWorkSheet->setCellValue('E1', 'Sent ');
                    $objWorkSheet->setCellValue('F1', 'Received ');
                    $column = 2;
                    foreach ($get_all_product_sku as $key=>$result) {
                        $datas = Inbound_shipment_items::with(['inbound_shipment'])->where(array('product_id' => $result['id']))->get()->toArray();
                        $newData = array();
                        foreach ($datas as $inc=>$inbound){
//                        if($inbound['QuantityShipped'] != $inbound['QuantityReceived']) {
                            $differenceInventory = $inbound['QuantityShipped'] - $inbound['QuantityReceived'];
                            $newData[$inc]['inbound'] = $differenceInventory;
                            $newData[$inc]['ShipmentName'] = $inbound['inbound_shipment']['ShipmentName'];
                            $newData[$inc]['ShipmentId'] = $inbound['inbound_shipment']['ShipmentId'];
                            $newData[$inc]['ShipmentStatus'] = $inbound['inbound_shipment']['ShipmentStatus'];
                            $newData[$inc]['QuantityShipped'] = $inbound['QuantityShipped'];
                            $newData[$inc]['QuantityReceived'] = $inbound['QuantityReceived'];
//                        }
                        }
                        $objWorkSheet->setCellValue('A' . $column, $result['sku']);
                        if (empty($newData)) {
                            $objWorkSheet->setCellValue('D' . $column, '');
                            $objWorkSheet->setCellValue('E' . $column, '');
                            $objWorkSheet->setCellValue('F' . $column, '');
                            $column++;
                        } else {
                            foreach ($newData as $inbound) {
                                $objWorkSheet->setCellValue('B' . $column, $inbound['ShipmentName']);
                                $objWorkSheet->setCellValue('C' . $column, $inbound['ShipmentId']);
                                $objWorkSheet->setCellValue('D' . $column, $inbound['ShipmentStatus']);
                                $objWorkSheet->setCellValue('E' . $column, $inbound['QuantityShipped']);
                                $objWorkSheet->setCellValue('F' . $column, $inbound['QuantityReceived']);
                                $column++;
                            }
                        }
                    }
                }
                if ($i == 2) {
                    //SellerSettlement data
                    $objWorkSheet->setCellValue('A1', 'SKU');
                    $objWorkSheet->setCellValue('B1', '1 Day (Returns/Sellable)');
                    $objWorkSheet->setCellValue('C1', '1 Day (24 Hours Sales)');
                    $objWorkSheet->setCellValue('D1', '7 Days');
                    $objWorkSheet->setCellValue('E1', 'Start Date');
                    $objWorkSheet->setCellValue('F1', 'End Date');
                    $objWorkSheet->setCellValue('G1', '14 Days');
                    $objWorkSheet->setCellValue('H1', 'Start Date');
                    $objWorkSheet->setCellValue('I1', 'End Date');
                    $objWorkSheet->setCellValue('J1', '30 Days');
                    $objWorkSheet->setCellValue('K1', 'Start Date');
                    $objWorkSheet->setCellValue('L1', 'End Date');
                    $objWorkSheet->setCellValue('M1', 'Previous 7 Days');
                    $objWorkSheet->setCellValue('N1', 'Start Date');
                    $objWorkSheet->setCellValue('O1', 'End Date');
                    $objWorkSheet->setCellValue('P1', 'Previous 30 Days');
                    $objWorkSheet->setCellValue('Q1', 'Start Date');
                    $objWorkSheet->setCellValue('R1', 'End Date');
                    $objWorkSheet->setCellValue('S1', '30 Days Sales - Last Year');
                    $dynamicArray = ['T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO','AP','AQ','AR','AS'];

                    for ($count=0;$count<=24;$count++) {
                        $result = date('m-Y', strtotime("-$count month"));
                        $objWorkSheet->setCellValue($dynamicArray[$count].'1', $result);
                    }
                    $column = 2;
                    foreach ($get_all_product_sku as $record) {
                        $sevenDaysStartDate = $sevenDaysEndDate = '';
                        $sevenDaysSales = $fourteenDaysSales = $thirtyDaysSales = $previousSevenDaysSales = $previousThirtyDaysSales = 0;
                        $previoussevenDaysStartDate = $previoussevenDaysEndDate = '';
                        $fourteenDaysStartDate = $fourteenDaysEndDate = '';
                        $thirtyDaysStartDate = $thirtyDaysEndDate = '';
                        $previousthirtyDaysStartDate = $previousthirtyDaysEndDate = '';
                        $breakFlag = 0;
                        $sevenDayFlag = $fourteenDayFlag = $thirtyDayFlag = $previousSevenFlag = $previousThirtyFlag = 0;
                        $startDate = date('Y-m-d');
                        $endDate = date('Y-m-d',strtotime('2020-01-01'));
                        $dailySales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '>=', $endDate)->orderBy('order_date', 'desc')->pluck('total_qty', 'order_date')->toArray();
                        $dailyInventory = Mws_calculated_inventory_daily::where('product_id', $record['id'])->where('inventory_date', '>=', date('Y-m-d 00:00:00',strtotime($endDate)))->orderBy('inventory_date', 'desc')->pluck('qty', 'inventory_date')->toArray();
                        $sevendayCount = $previoussevendayCount = $fourteendayCount = $thirtydayCount = $previousthirtydayCount = 0;
                        $dateArray = $this->displayDates($startDate,$endDate);
                        foreach ($dateArray as $key => $result) {
                            $count = ($key + 1);
                            if (!isset($dailyInventory[$result . ' 00:00:00']))
                                $dailyInventory[$result . ' 00:00:00'] = 0;
                            if (!isset($dailySales[$result]))
                                $dailySales[$result] = 0;
                            if ($count == 1) {
                                $sevenDaysStartDate = $result;
                                $fourteenDaysStartDate = $result;
                                $thirtyDaysStartDate = $result;
                            }
                            if ($sevendayCount < 7) {
                                if ($dailySales[$result] == 0 || $dailyInventory[$result . ' 00:00:00'] == 0) {
                                    $sevendayCount = $fourteendayCount = $thirtydayCount = 0;
                                    $sevenDayFlag++;
                                    $fourteenDayFlag++;
                                    $thirtyDayFlag++;
                                    $previousSevenFlag++;
                                    $previousThirtyFlag++;
                                    $sevenDaysStartDate = date('Y-m-d', strtotime("-1 days", strtotime($result)));
                                    $fourteenDaysStartDate = date('Y-m-d', strtotime("-1 days", strtotime($result)));
                                    $thirtyDaysStartDate = date('Y-m-d', strtotime("-1 days", strtotime($result)));
                                } else {
                                    $sevendayCount++;
                                    $fourteendayCount++;
                                    $thirtydayCount++;
                                }
                            }
                            if ($sevendayCount == 7) {
                                if (empty($sevenDaysEndDate)) {
                                    $sevenDaysEndDate = $result;
                                    $previoussevenDaysStartDate = date('Y-m-d', strtotime("-1 days", strtotime($result)));
                                }
                                if ($fourteendayCount <= 14) {
                                    if ($dailySales[$result] == 0 || $dailyInventory[$result . ' 00:00:00'] == 0) {
                                        $fourteendayCount = $thirtydayCount = $previoussevendayCount = 0;
                                        $previoussevenDaysStartDate = date('Y-m-d', strtotime("-2 days", strtotime($result)));
                                        $fourteenDaysStartDate = date('Y-m-d', strtotime("-2 days", strtotime($result)));
                                        $thirtyDaysStartDate = date('Y-m-d', strtotime("-2 days", strtotime($result)));
                                    } else {
                                        if ($previoussevendayCount == 7) {
                                            $previoussevenDaysEndDate = $result;
                                        }
                                        $previoussevendayCount++;
                                        $fourteendayCount++;
                                        $thirtydayCount++;
                                    }
                                }
                                if ($fourteendayCount == 15) {
                                    if (empty($fourteenDaysEndDate)) {
                                        $fourteenDaysEndDate = $result;
//                                        $previoussevenDaysEndDate = $result;
                                    }
                                    if ($thirtydayCount <= 31) {
                                        if ($dailySales[$result] == 0 || $dailyInventory[$result . ' 00:00:00'] == 0) {
                                            $thirtydayCount = 0;
                                            $thirtyDaysStartDate = date('Y-m-d', strtotime("-3 days", strtotime($result)));
                                        } else {
                                            $thirtydayCount++;
                                        }
                                    }
                                }
                                if ($thirtydayCount == 32) {
                                    if (empty($thirtyDaysEndDate)) {
                                        $thirtyDaysEndDate = $result;
                                        $previousthirtyDaysStartDate = date('Y-m-d', strtotime("-1 days", strtotime($result)));
                                    }
                                    if ($previousthirtydayCount <= 31) {
                                        if ($dailySales[$result] == 0 || $dailyInventory[$result . ' 00:00:00'] == 0) {
                                            $previousthirtydayCount = 0;
                                            $previousthirtyDaysStartDate = date('Y-m-d', strtotime("-2 days", strtotime($result)));
                                        } else {
                                            if ($previousthirtydayCount == 30) {
                                                $previousthirtyDaysEndDate = $result;
                                            }
                                            $previousthirtydayCount++;
                                        }
                                    }
                                }
                                if ($previousthirtydayCount == 32 && empty($previousthirtyDaysEndDate)) {
                                    $previousthirtyDaysEndDate = $result;
                                    $breakFlag = 1;
                                }
                            }
                            if ($breakFlag == 1)
                                break;
                        }
                        if(empty($fourteenDaysEndDate)){
                            $previoussevenDaysEndDate = '';
                        }

                        $objWorkSheet->setCellValue('A' . $column, $record['sku']);
                        $objWorkSheet->setCellValue('B' . $column, '');
                        $twentyFourHoursBackDate = Carbon::yesterday()->format('Y-m-d');
                        $twentyFourHoursSales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '>=', $twentyFourHoursBackDate)->sum('total_qty');
                        if ($twentyFourHoursSales > 0) {
                            $twentyFourHoursyRate = round(($twentyFourHoursSales / 1), 6);
                        } else {
                            $twentyFourHoursSales = $twentyFourHoursyRate = 0;
                        }

                        $objWorkSheet->setCellValue('C' . $column, $twentyFourHoursSales);
                        if(!empty($sevenDaysStartDate) && !empty($sevenDaysEndDate))
                            $sevenDaysSales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '>=', $sevenDaysEndDate)->where('order_date', '<=', $sevenDaysStartDate)->sum('total_qty');

                        if ($sevenDaysSales > 0) {
                            $sevenDaysDayRate = round(($sevenDaysSales / 7), 6);
                        } else {
                            $sevenDaysSales = $sevenDaysDayRate = 0;
                        }

                        if(!empty($fourteenDaysStartDate) && !empty($fourteenDaysEndDate))
                            $fourteenDaysSales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '>=', $fourteenDaysEndDate)->where('order_date', '<=', $fourteenDaysStartDate)->sum('total_qty');

                        if ($fourteenDaysSales > 0) {
                            $fourteenDaysDayRate = round(($fourteenDaysSales / 14), 6);
                        } else {
                            $fourteenDaysSales = $fourteenDaysDayRate = $fourteenDaysDayOfSupply = 0;
                        }
                        if(!empty($thirtyDaysStartDate) && !empty($thirtyDaysEndDate))
                            $thirtyDaysSales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '>=', $thirtyDaysEndDate)->where('order_date', '<=', $thirtyDaysStartDate)->sum('total_qty');
                        if ($thirtyDaysSales > 0) {
                            $thirtyDaysDayRate = round(($thirtyDaysSales / 30), 6);
                        } else {
                            $thirtyDaysSales = $thirtyDaysDayRate = $thirtyDaysDayOfSupply = 0;
                        }
                        if (!empty($previoussevenDaysStartDate) && !empty($previoussevenDaysEndDate))
                            $previousSevenDaysSales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '>=', $previoussevenDaysEndDate)->where('order_date', '<=', $previoussevenDaysStartDate)->sum('total_qty');
                        if ($previousSevenDaysSales > 0) {
                            $previousSevenDaysDayRate = round(($previousSevenDaysSales / 7), 6);
                        } else {
                            $previousSevenDaysSales = $previousSevenDaysDayRate = 0;
                        }
                        if (!empty($previousthirtyDaysStartDate) && !empty($previousthirtyDaysEndDate))
                            $previousThirtyDaysSales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '>=', $previousthirtyDaysEndDate)->where('order_date', '<=', $previousthirtyDaysStartDate)->sum('total_qty');
                        if ($previousThirtyDaysSales > 0) {
                            $previousThirtyDaysDayRate = round(($previousThirtyDaysSales / 30), 6);
                        } else {
                            $previousThirtyDaysSales = $previousThirtyDaysDayRate = 0;
                        }
                        $thirtyDaysStartDateLastYear = Carbon::now()->subDays(395)->format('Y-m-d');
                        $thirtyDaysEndDateLastYear = Carbon::now()->subDays(365)->format('Y-m-d');
                        $thirtyDaysSalesLastyear = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '<=', $thirtyDaysEndDateLastYear)->where('order_date', '>=', $thirtyDaysStartDateLastYear)->sum('total_qty');
                        if ($thirtyDaysSalesLastyear > 0) {
                            $ThirtyDaysDayRateLastYear = round(($thirtyDaysSalesLastyear / 30), 6);
                        } else {
                            $ThirtyDaysDayRateLastYear = 0;
                        }

                        $objWorkSheet->setCellValue('D' . $column, $sevenDaysSales);
                        $objWorkSheet->setCellValue('E' . $column, (($sevenDayFlag >0 && isset($sevenDaysStartDate) && $sevenDaysStartDate != '01-01-1970' && $sevenDaysStartDate != '' && isset($sevenDaysEndDate) && $sevenDaysEndDate != '01-01-1970' && $sevenDaysEndDate != '') ? date('m-d-Y',strtotime($sevenDaysStartDate)) : ''));
                        $objWorkSheet->setCellValue('F' . $column, (($sevenDayFlag >0 && isset($sevenDaysEndDate) && $sevenDaysEndDate != '01-01-1970' && $sevenDaysEndDate != '') ? date('m-d-Y',strtotime($sevenDaysEndDate)) : ''));
                        $objWorkSheet->setCellValue('G' . $column, $fourteenDaysSales);
                        $objWorkSheet->setCellValue('H' . $column, (($fourteenDayFlag >0 && isset($fourteenDaysStartDate) && isset($fourteenDaysEndDate) && $fourteenDaysStartDate != '01-01-1970' && $fourteenDaysStartDate != '' && $fourteenDaysEndDate != '01-01-1970' && $fourteenDaysEndDate != '') ? date('m-d-Y',strtotime($fourteenDaysStartDate)) : ''));
                        $objWorkSheet->setCellValue('I' . $column, (($fourteenDayFlag >0 && isset($fourteenDaysEndDate) && $fourteenDaysEndDate != '01-01-1970' && $fourteenDaysEndDate != '') ? date('m-d-Y',strtotime($fourteenDaysEndDate)): ''));
                        $objWorkSheet->setCellValue('J' . $column, $thirtyDaysSales);
                        $objWorkSheet->setCellValue('K' . $column, (($thirtyDayFlag >0 && isset($thirtyDaysStartDate) && $thirtyDaysStartDate != '01-01-1970' && $thirtyDaysStartDate != ''  && isset($thirtyDaysEndDate) && $thirtyDaysEndDate != '01-01-1970' && $thirtyDaysEndDate != '') ? date('m-d-Y',strtotime($thirtyDaysStartDate)) : ''));
                        $objWorkSheet->setCellValue('L' . $column, (($thirtyDayFlag >0 && isset($thirtyDaysEndDate) && $thirtyDaysEndDate != '01-01-1970' && $thirtyDaysEndDate != '') ? date('m-d-Y',strtotime($thirtyDaysEndDate)) : ''));
                        $objWorkSheet->setCellValue('M' . $column, $previousSevenDaysSales);
                        $objWorkSheet->setCellValue('N' . $column, (($previousSevenFlag >0 && isset($previoussevenDaysStartDate) && $previoussevenDaysStartDate != '01-01-1970' && $previoussevenDaysStartDate != '' && isset($previoussevenDaysEndDate) && $previoussevenDaysEndDate != '01-01-1970' && $previoussevenDaysEndDate != '') ? date('m-d-Y',strtotime($previoussevenDaysStartDate)) : ''));
                        $objWorkSheet->setCellValue('O' . $column, (($previousSevenFlag >0 && isset($previoussevenDaysEndDate) && $previoussevenDaysEndDate != '01-01-1970' && $previoussevenDaysEndDate != '') ? date('m-d-Y',strtotime($previoussevenDaysEndDate)) : ''));
                        $objWorkSheet->setCellValue('P' . $column, $previousThirtyDaysSales);
                        $objWorkSheet->setCellValue('Q' . $column, (($previousThirtyFlag >0 && isset($previousthirtyDaysStartDate) && $previousthirtyDaysStartDate != '01-01-1970' && $previousthirtyDaysStartDate != '' && isset($previousthirtyDaysEndDate) && $previousthirtyDaysEndDate != '01-01-1970' && $previousthirtyDaysEndDate != '') ? date('m-d-Y',strtotime($previousthirtyDaysStartDate)) : ''));
                        $objWorkSheet->setCellValue('R' . $column, (($previousThirtyFlag >0 && isset($previousthirtyDaysEndDate) && $previousthirtyDaysEndDate != '01-01-1970' && $previousthirtyDaysEndDate != '') ? date('m-d-Y',strtotime($previousthirtyDaysEndDate)) : ''));
                        $objWorkSheet->setCellValue('S' . $column, $thirtyDaysSalesLastyear);
                        for ($count=0;$count<=24;$count++){
                            $get_datemonth =  date('Y-m',strtotime("-$count month"));
                            $get_supply_total_trands = get_supply_total_trands($get_datemonth,$record['id']);
                            if(!empty($get_supply_total_trands)){
                                $objWorkSheet->setCellValue($dynamicArray[$count]. $column,$get_supply_total_trands->sales_total);
                            }else{
                                $objWorkSheet->setCellValue($dynamicArray[$count]. $column,'');
                            }
                        }
                        $column++;
                        $objWorkSheet->setCellValue('A' . $column, '');
                        $objWorkSheet->setCellValue('B' . $column, '');
                        $objWorkSheet->setCellValue('C' . $column, $twentyFourHoursyRate);
                        $objWorkSheet->setCellValue('D' . $column, $sevenDaysDayRate);
                        $objWorkSheet->setCellValue('E' . $column, '');
                        $objWorkSheet->setCellValue('F' . $column, '');
                        $objWorkSheet->setCellValue('G' . $column, $fourteenDaysDayRate);
                        $objWorkSheet->setCellValue('H' . $column, '');
                        $objWorkSheet->setCellValue('I' . $column, '');
                        $objWorkSheet->setCellValue('J' . $column, $thirtyDaysDayRate);
                        $objWorkSheet->setCellValue('K' . $column, '');
                        $objWorkSheet->setCellValue('L' . $column, '');
                        $objWorkSheet->setCellValue('M' . $column, $previousSevenDaysDayRate);
                        $objWorkSheet->setCellValue('N' . $column, '');
                        $objWorkSheet->setCellValue('O' . $column, '');
                        $objWorkSheet->setCellValue('P' . $column, $previousThirtyDaysDayRate);
                        $objWorkSheet->setCellValue('Q' . $column, '');
                        $objWorkSheet->setCellValue('R' . $column, '');
                        $objWorkSheet->setCellValue('S' . $column, $ThirtyDaysDayRateLastYear);
                        for ($count=0;$count<=24;$count++){
                            $get_datemonth =  date('Y-m',strtotime("-$count month"));
                            $get_supply_total_trands = get_supply_total_trands($get_datemonth,$record['id']);
                            if(!empty($get_supply_total_trands)){
                                $objWorkSheet->setCellValue($dynamicArray[$count]. $column,$get_supply_total_trands->day_rate);
                            }else{
                                $objWorkSheet->setCellValue($dynamicArray[$count]. $column,'');
                            }
                        }
                        $column++;
                    }
                }

                if ($i == 3) 
                {
                    //order returns data
                    $objWorkSheet->setCellValue('A1', 'SKU');
                    $objWorkSheet->setCellValue('B1', 'Return Date');
                    $objWorkSheet->setCellValue('C1', 'Product Name');
                    $objWorkSheet->setCellValue('D1', 'ASIN');
                    $objWorkSheet->setCellValue('E1', 'FNSKU');
                    $objWorkSheet->setCellValue('F1', 'Quantity');
                    $objWorkSheet->setCellValue('G1', 'Fulfillment Center Id');
                    $objWorkSheet->setCellValue('H1', 'Detailed Disposition');
                    $objWorkSheet->setCellValue('I1', 'Reason');
                    $objWorkSheet->setCellValue('J1', 'Status');
                    $objWorkSheet->setCellValue('K1', 'License Plate Number');
                    $objWorkSheet->setCellValue('L1', 'Customer Comments');
                    $column = 2;

                    foreach ($get_all_product_sku as $key => $result1) 
                    {   
                        $objWorkSheet->setCellValue('A' . $column, $result1['sku']);
                        if(!empty($result1['mws_order_returns'])){
                            foreach ($result1['mws_order_returns'] as $key => $result) {
                                $date_rt = date('m/d/Y',strtotime($result['return_date']));
                                $cmt_rt = $result['customer_comments'] != null ? $result['customer_comments'] : "-";
                                $objWorkSheet->setCellValue('B' . $column, $date_rt);
                                $objWorkSheet->setCellValue('C' . $column, $result['product_name']);
                                $objWorkSheet->setCellValue('D' . $column, $result['asin']);
                                $objWorkSheet->setCellValue('E' . $column, $result['fnsku']);
                                $objWorkSheet->setCellValue('F' . $column, $result['quantity']);
                                $objWorkSheet->setCellValue('G' . $column, $result['fulfillment_center_id']);
                                $objWorkSheet->setCellValue('H' . $column, $result['detailed_disposition']);
                                $objWorkSheet->setCellValue('I' . $column, $result['reason']);
                                $objWorkSheet->setCellValue('J' . $column, $result['status']);
                                $objWorkSheet->setCellValue('K' . $column, $result['license_plate_number']);
                                $objWorkSheet->setCellValue('L' . $column, $cmt_rt);
                                $column++;
                            }
                        }else{
                            $column++;
                        }    
                    }
                }
                $objWorkSheet->setTitle($sheetsstitle[$i]);
                $i++;
            }
            $objPHPExcel->setActiveSheetIndex($i-4);
            $writer = new Xlsx($objPHPExcel);
            $writer->save(base_path().'/download/Stockful_'.$this->marketplace->id.'_'.date('Y-m-d').'.xls');
            Log::error("Error while executing job Export Data for account {$this->marketplace->id}");
            $this->log_it("Done Export");
        } catch (\Exception $ex) {
            Log::error("Could not execute job Export Data: " . $ex->getMessage());
            $this->log_it($ex->getTraceAsString());
            throw new \Exception($ex->getMessage());
        }
    }
    function displayDates($date1, $date2, $format = 'Y-m-d' ) {
        $dates = array();
        $current = strtotime($date1);
        $date2 = strtotime($date2);
        $stepVal = '-1 day';
        while( $current >= $date2 ) {
            $dates[] = date($format, $current);
            $current = strtotime($stepVal, $current);
        }
        return $dates;
    }

}

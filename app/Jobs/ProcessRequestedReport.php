<?php

namespace App\Jobs;

use App\Models\Amazon_requestlog;
use App\Models\Mws_unsuppressed_inventory_data;
use App\Models\Usermarketplace;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ProcessRequestedReport extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $marketplace;
    /**
     * @var \MarketplaceWebService_Client
     */
    protected $service;
    protected $report_type;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Usermarketplace $marketplace)
    {
        $this->marketplace = $marketplace;
        $this->report_type = '_GET_FBA_MYI_UNSUPPRESSED_INVENTORY_DATA_';
        parent::__construct();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $this->service = $this->getReportsClient();

            try {
                $this->processReport();

            } catch (\Exception $ex) {
                Log::error("Could not process ProcessRequestedReportsJob for account {$this->marketplace->id}: " . $ex->getMessage());
                $this->log_it($ex->getMessage());
                $this->log_it($ex->getTraceAsString());
                throw new \Exception($ex->getMessage());
            }

        } catch (\Exception $ex) {
            Log::error("ProcessRequestedReportsJob for account {$this->marketplace->id} failed: " . $ex->getMessage());
            $this->log_it($ex->getTraceAsString());
            throw new \Exception($ex->getMessage());
        }
    }

    private function processReport()
    {
        Log::debug("Started Fetching ProcessRequstedReport");
        $fromDate = Carbon::now()->subMinutes('60');
        $toDate = Carbon::now();
        $request = new \MarketplaceWebService_Model_GetReportRequestListRequest();
        $this->initRequest($request, 'setMerchant');
        $reportListType = new \MarketplaceWebService_Model_TypeList();
        $request->setReportTypeList($reportListType->withType($this->report_type));
        $request->setRequestedFromDate($fromDate);
        $request->setRequestedToDate($toDate);
        $response = $this->service->getReportRequestList($request);
        if (is_array($response)) {
            $responseMessage = json_decode(json_encode(simplexml_load_string($response['ResponseBody'])))->Error->Message;

            Log::error("Error while executing job RequestReport for account {$this->marketplace->id}: $responseMessage");

            if ($response['code'] == 400 || $response['code'] == 401) {
                $amazon_report = new Amazon_requestlog([
                    'user_marketplace_id' => $this->marketplace->id,
                    'request_id' => '',
                    'status' => 'Invalid Credentials',
                    'last_checked' => Carbon::now(),
                    'request_response' => $responseMessage,
                    'report_type' => $this->report_type,
                    'processed' => 1
                ]);
                $amazon_report->save();
                $this->log_it($responseMessage);
            }
        } else {
            $result = $response->getGetReportRequestListResult();
            $reportRequestedValues = $result->getReportRequestInfoList();
            $amazon_report = '';
            if (is_array($reportRequestedValues)) {
                foreach ($reportRequestedValues as $reportRequestedValue) {
                    $reportStatus = $reportRequestedValue->getReportProcessingStatus();
                    if (isset($reportStatus)) {
                        if ($reportStatus == '_DONE_') {
                            $amazon_report = Amazon_requestlog::where('request_id', $reportRequestedValue->getReportRequestId())->first();
                            if (empty($amazon_report)) {
                                $amazon_report = new Amazon_requestlog([
                                    'user_marketplace_id' => $this->marketplace->id,
                                    'request_id' => $reportRequestedValue->getReportRequestId(),
                                    'report_id' => $reportRequestedValue->getGeneratedReportId(),
                                    'status' => $reportStatus,
                                    'last_checked' => Carbon::now(),
                                    'report_type' => $this->report_type,
                                    'processed' => 0
                                ]);
                                $amazon_report->save();
                            }
                            switch ($this->report_type) {
                                case Amazon_requestlog::$MWS_UNSUPPRESSED_INVENTORY_DATA:
                                    Mws_unsuppressed_inventory_data::processReport($amazon_report, $this->readReport($amazon_report), $this->marketplace);
                                    break;
                                default:
                                    Log::error("Can't process reports of type {$amazon_report->report_type}");
                            }
                            $amazon_report->save();
                            $this->log_it("Amazon Report {$amazon_report->id} is processed");
                            Log::debug("Amazon Report {$amazon_report->id} has been processed");
                        }
                    }
                    if ($reportStatus == '_DONE_')
                        break;
                }
            }
        }
    }

    /**
     * @param Amazon_requestlog $amazon_report
     * @return mixed
     */
    private function readReport(Amazon_requestlog $amazon_report)
    {
        $request = new \MarketplaceWebService_Model_GetReportRequest();
        $this->initRequest($request, 'setMerchant', 'setMarketplace');
        $request->setReport(@fopen('php://memory', 'rw+'));
        $request->setReportId($amazon_report->report_id);
        $response = $this->service->getReport($request);

        if ($response->isSetGetReportResult()) {
            return $request->getReport();
        }

        return null;
    }
}

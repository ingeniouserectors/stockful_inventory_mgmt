<?php

namespace App\Jobs;

use App\Models\Inbound_shipment_items;
use App\Models\Inbound_shipments;
use Complex\Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Usermarketplace;
use Illuminate\Support\Facades\Log;
use App\Models\Mws_product;
use Carbon\Carbon;

class InboundShipments extends Job implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $marketplace;
    private $statusArray;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Usermarketplace $marketplace)
    {
        //
        Log::debug("Initializing Inbound Shipment for account {$marketplace->id}");

        $this->marketplace = $marketplace;
        if (Carbon::now()->format('h') <= 8) {
            $this->statusArray = array('CLOSED', 'DELIVERED');
        } else if (Carbon::now()->format('h') >= 8 && Carbon::now()->format('h') <= 16) {
            $this->statusArray = array('RECEIVING', 'WORKING');
        } else {
            $this->statusArray = array('SHIPPED', 'IN_TRANSIT', 'CHECKED_IN');
        }
        parent::__construct();

    }

    private function getRequest()
    {
        $request = new \FBAInboundServiceMWS_Model_ListInboundShipmentsRequest();
        $this->initRequest($request);
        $createdAfter = new \DateTime(date('Y-m-d H:i:s', strtotime('-6 month')), new \DateTimeZone('UTC'));
        $createdBefore = new \DateTime(date('Y-m-d H:i:s', strtotime('+1 month')), new \DateTimeZone('UTC'));
        $request->setLastUpdatedAfter($createdAfter->format("c"));
        $request->setLastUpdatedBefore($createdBefore->format("c"));
        $listStatus = $this->getStatus();
        $request->setShipmentStatusList($listStatus);
        return $request;
    }

    private function getStatus()
    {
        $listRequest = new \FBAInboundServiceMWS_Model_ShipmentStatusList();
        $listRequest->setmember($this->statusArray);
        return $listRequest;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Log::debug("Executing job Inbound Shipment for account {$this->marketplace->id}");
            $service = $this->getReportsClientFBAInbound();
            $this->inboundShipments($service, $this->marketplace->id);
        } catch (\Exception $ex) {
            Log::error("Could not execute job RequestReport: " . $ex->getMessage());
            $this->log_it($ex->getTraceAsString());
            throw new \Exception($ex->getMessage());
        }
    }

    private function inboundShipments($service, $user_marketplace_id)
    {
        $request = $this->getRequest();
        $response = $this->invokeListInboundShipments($service, $request);
        if (isset($response->ListInboundShipmentsResult)) {
            $shipment = $this->insertShipmentData($response->ListInboundShipmentsResult, $user_marketplace_id);
            $this->inboundShipmentItems($shipment, $service);
            $nextToken = (isset($response->ListInboundShipmentsResult->NextToken) ? $response->ListInboundShipmentsResult->NextToken : '');
            while (!empty($nextToken)) {
                Log::debug("Fetching Inbound Shipment data for account {$this->marketplace->id}");
                $tokenRequest = new \FBAInboundServiceMWS_Model_ListInboundShipmentsByNextTokenRequest();
                $this->initRequest($tokenRequest);
                $tokenRequest->setNextToken($nextToken);
                $tokenResponse = $this->invokeListInboundShipmentsByNextToken($service, $tokenRequest);
                $nextToken = (isset($tokenResponse->ListInboundShipmentsByNextTokenResult->NextToken) ? $tokenResponse->ListInboundShipmentsByNextTokenResult->NextToken : '');
                $newshipment = $this->insertShipmentData($tokenResponse->ListInboundShipmentsByNextTokenResult, $user_marketplace_id);
                $this->inboundShipmentItems($newshipment, $service);
                sleep(60);
//                array_merge($shipment, $newshipment);
            }
//            return $shipment;
        }
    }

    private function insertShipmentData($response, $user_marketplace_id)
    {
        $shipment = array();
        Log::debug("Started Inserting/Updating Inbound Shipment data for account {$this->marketplace->id}");
        if (isset($response->ShipmentData->member)) {
            foreach ($response->ShipmentData->member as $member) {
                $shipmentId = (array)$member->ShipmentId;
                $shipmentName = (array)$member->ShipmentName;
                $address = $member->ShipFromAddress;
                $fromAddress = '';
                foreach ($address as $addresses) {
                    $fromAddress = $addresses->Name . ", " . $addresses->AddressLine1 . ", " . $addresses->City . ", " . $addresses->StateOrProvinceCode . ", " . $addresses->CountryCode . ", " . $addresses->PostalCode;
                }
                $centerId = (array)$member->DestinationFulfillmentCenterId;
                $status = (array)$member->ShipmentStatus;
                $prepType = (array)$member->LabelPrepType;
                $inboundShip = Inbound_shipments::where('ShipmentId', $shipmentId[0])->first();
                if (empty($inboundShip)) {
                    $inboundShip = new Inbound_shipments();
                    $inboundShip->user_marketplace_id = $user_marketplace_id;
                }
                $inboundShip->ShipmentId = $shipmentId[0];
                $inboundShip->ShipmentName = $shipmentName[0];
                $inboundShip->ShipFromAddress = $fromAddress;
                $inboundShip->DestinationFulfillmentCenterId = $centerId[0];
                $inboundShip->ShipmentStatus = $status[0];
                $inboundShip->LabelPrepType = $prepType[0];
                $inboundShip->save();
                if (isset($shipmentId[0])) {
                    $shipment[] = $shipmentId[0];
                }
            }
        }
        return $shipment;
    }

    private function inboundShipmentItems($shipment, $service)
    {
        foreach ($shipment as $shipments) {
            $request = new \FBAInboundServiceMWS_Model_ListInboundShipmentItemsRequest();
            $this->initRequest($request);
            $request->setShipmentId($shipments);
            $tokenRequest = new \FBAInboundServiceMWS_Model_ListInboundShipmentItemsByNextTokenRequest();
            $this->initRequest($tokenRequest);
            $response = $this->invokeListInboundShipmentItems($service, $request);
            if (isset($response->ListInboundShipmentItemsResult)) {
                $this->insertShipmentItemData($response->ListInboundShipmentItemsResult, $shipments);
                $nextToken = (isset($response->ListInboundShipmentItemsResult->NextToken) ? $response->ListInboundShipmentItemsResult->NextToken : '');
                while (!empty($nextToken)) {
                    $tokenRequest->setNextToken($nextToken);
                    $tokenResponse = $this->invokeListInboundShipmentItemsByNextToken($service, $tokenRequest);
                    if(isset($tokenResponse->ListInboundShipmentItemsByNextTokenResult)) {
                    $nextToken = (isset($tokenResponse->ListInboundShipmentItemsByNextTokenResult->NextToken) ? $tokenResponse->ListInboundShipmentItemsByNextTokenResult->NextToken : '');
                        $this->insertShipmentItemData($tokenResponse->ListInboundShipmentItemsByNextTokenResult, $shipments);
                    }
                }
            }
        }
    }

    private function insertShipmentItemData($response, $shipments)
    {
        if (!empty($response->ItemData->member)) {
            $shipData = Inbound_shipments::where('ShipmentId', $shipments)->first();
            foreach ($response->ItemData->member as $members) {
                $product = Mws_product::where('sku', $members->SellerSKU)->first();
                if (isset($product->id)) {
                    $itemData = Inbound_shipment_items::where('inbound_shipment_id', $shipData->id)->where('product_id', $product->id)->first();
                    if (empty($itemData)) {
                        $itemData = new Inbound_shipment_items();
                        $itemData->inbound_shipment_id = $shipData->id;
                        $itemData->product_id = $product->id;
                        $itemData->QuantityShipped = $members->QuantityShipped;
                        $itemData->QuantityReceived = $members->QuantityReceived;
                        $itemData->QuantityInCase = $members->QuantityInCase;
                    } else {
                        $itemData->QuantityShipped = $members->QuantityShipped;
                        $itemData->QuantityReceived = $members->QuantityReceived;
                        $itemData->QuantityInCase = $members->QuantityInCase;
                    }
                    $itemData->save();
                }
            }
        }
    }

    function invokeListInboundShipments(\FBAInboundServiceMWS_Interface $service, $request)
    {

        try {
            $response = $service->ListInboundShipments($request);
            $dom = new \DOMDocument();
            $dom->loadXML($response->toXML());
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $dom->saveXML();
            return $list_response = new \SimpleXMLElement($dom->saveXML());


        } catch (\FBAInboundServiceMWS_Exception $ex) {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
        }
    }

    function invokeListInboundShipmentItems(\FBAInboundServiceMWS_Interface $service, $request)
    {
        try {
            $response = $service->ListInboundShipmentItems($request);
            $dom = new \DOMDocument();
            $dom->loadXML($response->toXML());
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $dom->saveXML();
            return $list_response = new \SimpleXMLElement($dom->saveXML());

        } catch (\FBAInboundServiceMWS_Exception $ex) {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
        }
    }

    function invokeListInboundShipmentsByNextToken(\FBAInboundServiceMWS_Interface $service, $request)
    {
        try {
            $response = $service->ListInboundShipmentsByNextToken($request);
            $dom = new \DOMDocument();
            $dom->loadXML($response->toXML());
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $dom->saveXML();
            return $list_response = new \SimpleXMLElement($dom->saveXML());

        } catch (\FBAInboundServiceMWS_Exception $ex) {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
        }
    }

    function invokeListInboundShipmentItemsByNextToken(\FBAInboundServiceMWS_Interface $service, $request)
    {
        try {
            $response = $service->ListInboundShipmentItemsByNextToken($request);
            $dom = new \DOMDocument();
            $dom->loadXML($response->toXML());
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $dom->saveXML();
            return $list_response = new \SimpleXMLElement($dom->saveXML());

        } catch (\FBAInboundServiceMWS_Exception $ex) {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
        }
    }
}

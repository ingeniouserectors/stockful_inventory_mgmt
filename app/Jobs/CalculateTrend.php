<?php

namespace App\Jobs;

use App\Models\Amzdateiteration;
use App\Models\Mws_product;
use App\Models\Sales_total_trend_rate;
use App\Models\Usermarketplace;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Bus\Queueable;

class CalculateTrend extends Job implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    public $marketplace;
    protected $report_type;
    protected $from_date_time;
    protected $to_date_time;
    protected $amazon_date_itetration;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Usermarketplace $marketplace, $amzDateitetration = null)
    {
        Log::debug("Initializing CalculateTrend for account {$marketplace->id}");

        $this->marketplace = $marketplace;
        $this->amazon_date_itetration = $amzDateitetration;
        if (empty($amzDateitetration)) {
            $this->from_date_time = Carbon::today();
            $this->to_date_time = Carbon::now();
        } else {
            $this->from_date_time = $amzDateitetration['startDate'];
            $this->to_date_time = $amzDateitetration['endDate'];
        }
        parent::__construct();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Log::debug("Executing job CalculateTrend for account {$this->marketplace->id}");
            $fetchProducts = Mws_product::where('user_marketplace_id', $this->marketplace->id)->get()->toArray();
            foreach ($fetchProducts as $producct) {
                $calculationTrendValues = '';
                if ($this->amazon_date_itetration) {
                    $nineteenMonthBack = Carbon::now()->subMonths(19)->format('Y-m');
                    $calculationTrendValues = Sales_total_trend_rate::where('prod_id', $producct['id'])->orderByDesc('month_year','<=',$nineteenMonthBack)->orderByDesc('month_year')->get()->toArray();
                } else {
                    $sixMonthBack = Carbon::now()->subMonths(6)->format('Y-m');
                    $calculationTrendValues = Sales_total_trend_rate::where('prod_id', $producct['id'])->where('month_year', '>=', $sixMonthBack)->orderByDesc('month_year')->get()->toArray();
                }
                foreach ($calculationTrendValues as $index => $trendValue) {
                    $calculatedPercentageTrend = 0;
                    if (!empty($calculationTrendValues[$index + 1]['day_rate'])) {
                        $calculatedPercentageTrend = round((($trendValue['day_rate'] - $calculationTrendValues[$index + 1]['day_rate']) / $trendValue['day_rate']) * 100, 2);
                    }
                    Sales_total_trend_rate::where('id',$trendValue['id'])->update(['trend_percentage' => $calculatedPercentageTrend]);
                }
            }
            Amzdateiteration::where('user_marketplace_id', $this->marketplace->id)->where('OrderStatus', '1')->update(['OrderStatus' => 2]);
            Log::error("Error while executing job CalculateTrend for account {$this->marketplace->id}");
            $this->log_it("Done CalculateTrend Calculations");
        } catch (\Exception $ex) {
            Log::error("Could not execute job CalculateTrend: " . $ex->getMessage());
            $this->log_it($ex->getTraceAsString());
            throw new \Exception($ex->getMessage());
        }
    }
}

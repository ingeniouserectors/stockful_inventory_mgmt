<?php

use App\Models\User_access_modules;
use App\Models\Settings;
use App\Models\Users;
use Stringy\Stringy;
use App\Models\Application_modules;
use App\Models\Role_access_modules;
use Illuminate\Support\Facades\Gate;
use App\Models\Usermarketplace;
use App\Models\Vendor_default_setting;
use App\Models\Supplier_default_setting;
use App\Models\Vendor_product;
use App\Models\Supplier_product;
use App\Models\Warehouse_product;
use App\Models\Warehouse_default_setting;
use App\Models\User_assigned_role;
use App\Models\Purchase_instances;
use App\Models\Purchase_instance_details;
use App\Models\Supplier;
use App\Models\Vendor;
use App\Models\Warehouse;
use App\Models\Mws_product;
use App\Models\Mws_order;
use App\Models\Sales_total_trend_rate;
use App\Models\Member_blackoutdate_setting;
use App\Models\Daily_logic_Calculations;
use App\Models\Warehouse_wise_default_setting;
use Carbon\Carbon;
use App\Models\Calculated_sales_qty;
use App\Models\Country;
use App\Models\Mws_unsuppressed_inventory_data;
use App\Models\Purchase_order_details;
use App\Models\Purchase_orders;
use App\Models\Supplier_blackout_date;
use App\Models\Vendor_blackout_date;
use App\Models\Mws_out_of_stock_detail;
use App\Models\Temp_session;
use App\Models\Field_list;

if (!function_exists('get_marketplaces')) {
    function get_marketplaces($marketplaceid = null)
    {
        if($marketplaceid){
            return \App\Models\SPApiMarketplaceDetails::where('id', $marketplaceid)->pluck('marketplace_country')->first();
        }else{
            return \App\Models\SPApiMarketplaceDetails::where('marketplace_id', $marketplaceid)->get()->toArray();
        }
    }
}

/**
 * Remove the value from temp. session.
 * @param  $user_id
 * @return \Illuminate\Http\Response
 */
if (!function_exists('remove_temp_session')) {
    function remove_temp_session($user_id)
    {
        return Temp_session::where(array('user_id' => $user_id))->delete();
    }
}

/**
 * create or update temp. session data
 * @param  \Illuminate\Http\Response $request
 * @return \Illuminate\Http\Response
 */

if (!function_exists('update_insert_temp_session')) {
    function update_insert_temp_session($data)
    {
        $check_temp = Temp_session::where(array('user_id' => $data['user_id'], 'marketplace_id' => $data['marketplace_id'], 'product_id' => $data['product_id']))->first();
        if (empty($check_temp)) {
            $record = [
                'user_id' => $data['user_id'],
                'marketplace_id' => $data['marketplace_id'],
                'product_id' => $data['product_id'],
                'recalulated_qty' => $data['recalulated_qty'],
            ];
            return $get_create = Temp_session::create($record);
        } else {
            $record = [
                'user_id' => $data['user_id'],
                'marketplace_id' => $data['marketplace_id'],
                'product_id' => $data['product_id'],
                'recalulated_qty' => $data['recalulated_qty'],
            ];
            return $get_create = Temp_session::where(array('id' => $check_temp))->update($record);
        }
    }
}


/**
 * Gel all days sales count result
 * @param  $result ,$days
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_all_counters')) {
    function get_all_counters($result, $days)
    {
        $current = Carbon::now();
        $trialExpires = $current->addDays($days)->format('Y-m-d');
        $daysSales = Mws_out_of_stock_detail::where('sku', $result)->where('stock_date', '>=', $trialExpires)->count('id');
        if ($daysSales > 0) {
            return $daysSales;
        } else {
            return 0;
        }
    }
}

/**
 * Get purchase order details
 * @param  $orderid
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_purchase_order_details')) {
    function get_purchase_order_details($order_id)
    {
        return Purchase_orders::where(array('id' => $order_id))->first();
    }
}

/**
 * Get warehouse leadtime from warehouse
 * @param  $warehouse_id
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_warehouse_leadtime')) {
    function get_warehouse_leadtime($warehouse_id)
    {
        $get_warehouse_wise = Warehouse_wise_default_setting::where(array('warehouse_id' => $warehouse_id))->first();
        if (!empty($get_warehouse_wise)) {
            return $get_warehouse_wise->lead_time;
        } else {
            $get_warehouse = Warehouse_default_setting::first();
            return $get_warehouse->lead_time;
        }
    }
}

/**
 * Get calulation for product
 * @param \Illuminate\Http\Response $request
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_product_calculation')) {
    function get_product_calculation($request)
    {
        $product_id = $request->product_id;
        $global_marketplace = session('MARKETPLACE_ID');
        $amazonInventory = Mws_unsuppressed_inventory_data::where('product_id', $product_id)->first();
        if ($amazonInventory) {
            $wareHouseInventory = Warehouse_product::where('product_id', $product_id)->get();
            $warehouseInventoryTotal = $wareHouseLeadTime = 0;
            if (isset($wareHouseInventory[0])) {
                foreach ($wareHouseInventory as $warehouse) {
                    $warehouseInventoryTotal += $warehouse->qty;
                    $leadTimeWarehouse = Warehouse_wise_default_setting::where('id', $warehouse->warehouse_id)->first();
                    if ($wareHouseLeadTime < $leadTimeWarehouse->lead_time) {
                        $wareHouseLeadTime = $leadTimeWarehouse->lead_time;
                    }
                }
            }
            $productSettings = Supplier_product::where('product_id', $product_id)->first();
            $blackOutDayFlat = 0;
            if (empty($productSettings)) {
                $productSettings = Vendor_product::where('product_id', $product_id)->first();
                $blackOutDayFlat = 1;
            }
            if (empty($productSettings)) {
                $productSettings = Vendor_default_setting::first();
                $blackOutDayFlat = 2;
            }
            if ($productSettings) {
                $fetchPO = Purchase_orders::where('user_marketplace_id', $global_marketplace)->where('reorder_date', '>=', Carbon::now()->format('Y-m-d'));
                $fetchProductPOInventory = 0;
                foreach ($fetchPO as $poDetails) {
                    $fetchProductPODetail = Purchase_order_details::where('purchase_orders_id', $poDetails->id)->where('product_id', $product_id)->first();
                    $fetchProductPOInventory += $fetchProductPODetail->projected_units;
                }
                $TotalInventory = $amazonInventory->afn_total_qty - $amazonInventory->afn_unsellable_qty + $warehouseInventoryTotal + $fetchProductPOInventory;
                if ($TotalInventory > 0) {
                    $TotalLeadTime = $wareHouseLeadTime + $productSettings->lead_time;
                    $orderVolume = $productSettings->order_volume;
                    //Seven Days Calculations
                    $sevenDaysBackDate = Carbon::now()->subDays(7)->format('Y-m-d');
                    $sevenDaysSales = $request->sevendays_sales;
                    if ($sevenDaysSales > 0) {
                        $sevenDaysDayRate = round(($sevenDaysSales / 7), 6);
                        $sevenDaysDayOfSupply = round(($TotalInventory / $sevenDaysDayRate), 2);
                    } else {
                        $sevenDaysSales = $sevenDaysDayRate = $sevenDaysDayOfSupply = 0;
                    }

                    //Fourteen Days Calculations
                    $fourteenDaysBackDate = Carbon::now()->subDays(14)->format('Y-m-d');
                    $fourteenDaysSales = $request->fourteendays_sales;
                    if ($fourteenDaysSales > 0) {
                        $fourteenDaysDayRate = round(($fourteenDaysSales / 14), 6);
                        $fourteenDaysDayOfSupply = round(($TotalInventory / $fourteenDaysDayRate), 2);
                    } else {
                        $fourteenDaysSales = $fourteenDaysDayRate = $fourteenDaysDayOfSupply = 0;
                    }

                    //Thirty Days Calculations
                    $thirtyDaysBackDate = Carbon::now()->subDays(30)->format('Y-m-d');
                    $thirtyDaysSales = $request->thirtydays_sales;
                    if ($thirtyDaysSales > 0) {
                        $thirtyDaysDayRate = round(($thirtyDaysSales / 30), 6);
                        $thirtyDaysDayOfSupply = round(($TotalInventory / $thirtyDaysDayRate), 2);
                    } else {
                        $thirtyDaysSales = $thirtyDaysDayRate = $thirtyDaysDayOfSupply = 0;
                    }

                    //Previous Seven Days Calculations
                    $previousSevenDaysSales = $request->sevendays_previous;
                    if ($previousSevenDaysSales > 0) {
                        $previousSevenDaysDayRate = round(($previousSevenDaysSales / 7), 6);
                    } else {
                        $previousSevenDaysDayRate = 0;
                    }

                    //Previous Thirty Days Calculations
                    $sixtyDaysBackDate = Carbon::now()->subDays(60)->format('Y-m-d');
                    $previousThirtyDaysSales = $request->thirtydays_previous;
                    if ($previousThirtyDaysSales > 0) {
                        $previousThirtyDaysDayRate = round(($previousThirtyDaysSales / 30), 6);
                    } else {
                        $previousThirtyDaysDayRate = 0;
                    }
                    //Last Year 30 Days Sales
                    $thirtyDaysStartDateLastYear = Carbon::now()->subDays(395)->format('Y-m-d');
                    $thirtyDaysEndDateLastYear = Carbon::now()->subDays(365)->format('Y-m-d');
                    $thirtyDaysSalesLastyear = $request->thirtydays_last_year;
                    if ($thirtyDaysSalesLastyear > 0) {
                        $ThirtyDaysDayRateLastYear = round(($thirtyDaysSalesLastyear / 30), 6);
                    } else {
                        $ThirtyDaysDayRateLastYear = 0;
                    }

                    $currentMonthPreviousYearStartDate = Carbon::now()->subYear(1)->format('Y-m-01');
                    $currentMonthPreviousYearEndDate = Carbon::now()->subYear(1)->format('Y-m-t');
                    $currentMonthLastYearStartDate = Carbon::now()->subYear(2)->format('Y-m-01');
                    $currentMonthLastYearEndDate = Carbon::now()->subYear(2)->format('Y-m-t');

                    $previousYearSameMonthSales = Calculated_sales_qty::where('product_id', $product_id)->where('order_date', '<=', $currentMonthPreviousYearEndDate)->where('order_date', '>=', $currentMonthPreviousYearStartDate)->sum('total_qty');
                    $lastYearSameMonthSales = Calculated_sales_qty::where('product_id', $product_id)->where('order_date', '<=', $currentMonthLastYearEndDate)->where('order_date', '>=', $currentMonthLastYearStartDate)->sum('total_qty');
                    $trendDirectionPrevious = $breakStatus = $monthlySalesTrendTotal = 0;

                    // Multi Month Trend Calculations
                    $trendDirectionPrevious = $breakStatus = $monthlySalesTrendTotal = $monthToMonthAverageTrend = 0;
                    for ($counter = 1; $counter < 18; $counter++) {
                        $calculationMonth = Carbon::now()->subMonth($counter)->format('Y-m');
                        $monthlySales = Sales_total_trend_rate::where('prod_id', $product_id)->where('month_year', $calculationMonth)->first();
                        $trendDirection = sign($monthlySales['trend_percentage']);
                        if ($counter > 1) {
                            if ($trendDirection == $trendDirectionPrevious) {
                                $monthlySalesTrendTotal += $monthlySales['trend_percentage'];
                            } else {
                                $monthToMonthAverageTrend = $monthlySalesTrendTotal / ($counter - 1);
                                $breakStatus = 1;
                            }
                        } else {
                            $monthlySalesTrendTotal += $monthlySales['trend_percentage'];
                        }
                        $trendDirectionPrevious = $trendDirection;
                        if ($breakStatus == 1)
                            break;
                    }
                    // Multi Week Trend Calculations
                    $trendDirectionPrevious = $breakStatus = $weeklySalesTrendTotal = $multiWeekAverageTrend = $weeklySalesPreviousDayRate = 0;
                    for ($counter = 1; $counter < 18; $counter++) {
                        $subtractStartDateValue = Carbon::now()->subDays($counter * 7)->format('Y-m-d');
                        if ($counter == 1)
                            $subtractEndDateValue = Carbon::now()->format('Y-m-d');
                        else
                            $subtractEndDateValue = Carbon::now()->subDays(($counter - 1) * 7)->format('Y-m-d');

                        $weeklySales = Calculated_sales_qty::where('product_id', $product_id)->where('order_date', '>=', $subtractStartDateValue)->where('order_date', '<=', $subtractEndDateValue)->sum('total_qty');
                        $weeklySalesDayRate = round($weeklySales / 7, 6);
                        $trendPercentage = 0;
                        if ($counter > 1) {
                            if (($weeklySalesDayRate - $weeklySalesPreviousDayRate) != 0 && $weeklySalesDayRate != 0) {
                                $trendPercentage = round(($weeklySalesDayRate - $weeklySalesPreviousDayRate) / $weeklySalesDayRate, 2) * 100;
                            } else {
                                $trendPercentage = 0;
                            }
                            $trendDirection = sign($trendPercentage);
                            if ($trendDirection == $trendDirectionPrevious || $trendDirection == 0 || $trendDirectionPrevious == 0) {
                                $weeklySalesTrendTotal += $trendPercentage;
                            } else {
                                $multiWeekAverageTrend = $weeklySalesTrendTotal / ($counter - 1);
                                $breakStatus = 1;
                            }
                            $trendDirectionPrevious = $trendDirection;
                        } else {
                            $weeklySalesTrendTotal += $trendPercentage;
                        }
                        $weeklySalesPreviousDayRate = $weeklySalesDayRate;
                        if ($breakStatus == 1)
                            break;
                    }

                    //Trend Calculations
                    if ($previousYearSameMonthSales > 0 && $lastYearSameMonthSales > 0)
                        $yearOverYearHistoricalTrend = round((($previousYearSameMonthSales - $lastYearSameMonthSales) / $lastYearSameMonthSales * 100), 2);
                    else
                        $yearOverYearHistoricalTrend = 0;
                    //Year Over year Trend
                    $previousMonthYearDate = Carbon::now()->subMonth(1)->subYear(1)->format('Y-m');
                    $salesTrend = Sales_total_trend_rate::where('prod_id', $product_id)->where('month_year', $previousMonthYearDate)->first();
                    if (!empty($salesTrend) && $thirtyDaysSales > 0) {
                        $yearOveryearCurrent = round(($thirtyDaysSales - $salesTrend->sales_total) / $salesTrend->sales_total * 100, 2);
                    } else {
                        $yearOveryearCurrent = 0;
                    }
                    //Multi Month Trend
                    $multiMonthTrend = $monthToMonthAverageTrend;

                    if ($thirtyDaysDayRate > 0 && $previousThirtyDaysDayRate > 0)
                        $thirtyOverThirtyTrend = round((($thirtyDaysDayRate - $previousThirtyDaysDayRate) / $thirtyDaysDayRate) * 100, 2);
                    else
                        $thirtyOverThirtyTrend = 0;

                    //Multi Week Trend
                    $multiWeekTrend = $multiWeekAverageTrend;

                    if ($sevenDaysDayRate > 0 && $previousSevenDaysDayRate > 0)
                        $sevenOverSevenTrend = round((($sevenDaysDayRate - $previousSevenDaysDayRate) / $sevenDaysDayRate) * 100, 2);
                    else
                        $sevenOverSevenTrend = 0;
                    if ($request->supply_last_year_sales_percentage > 0) {
                        $historicalRecentDifference = (100 - $request->supply_last_year_sales_percentage) / 100;
                    } else {
                        $historicalRecentDifference = 1;
                    }
                    if ($request->reorder_last_year_sales_percentage > 0) {

                        $historicalRecentReorderDifference = (100 - $request->reorder_last_year_sales_percentage) / 100;
                    } else {
                        $historicalRecentReorderDifference = 1;
                    }
                    $supplyCalculationSalesDayRate = (($ThirtyDaysDayRateLastYear != 0 && $request->supply_last_year_sales_percentage > 0) ? round(($ThirtyDaysDayRateLastYear * $request->supply_last_year_sales_percentage / 100), 6) : 0) +
                        (($sevenDaysDayRate != 0 && $request->supply_recent_last_7_day_sales_percentage > 0) ? round(($sevenDaysDayRate * round(($request->supply_recent_last_7_day_sales_percentage * $historicalRecentDifference), 6) / 100), 6) : 0) +
                        (($fourteenDaysDayRate != 0 && $request->supply_recent_last_14_day_sales_percentage > 0) ? round(($fourteenDaysDayRate * round(($request->supply_recent_last_14_day_sales_percentage * $historicalRecentDifference), 6) / 100), 6) : 0) +
                        (($thirtyDaysDayRate != 0 && $request->supply_recent_last_30_day_sales_percentage > 0) ? round(($thirtyDaysDayRate * round(($request->supply_recent_last_30_day_sales_percentage * $historicalRecentDifference), 6) / 100), 6) : 0);
                    $supplyCalculationsTrend = (($yearOverYearHistoricalTrend != 0 && $request->supply_trends_year_over_year_historical_percentage > 0) ? round(($yearOverYearHistoricalTrend * $request->supply_trends_year_over_year_historical_percentage / 100), 2) : 0) +
                        (($yearOveryearCurrent != 0 && $request->supply_trends_year_over_year_current_percentage > 0) ? round(($yearOveryearCurrent * $request->supply_trends_year_over_year_current_percentage / 100), 2) : 0) +
                        (($multiMonthTrend != 0 && $request->supply_trends_multi_month_trend_percentage > 0) ? round(($multiMonthTrend * $request->supply_trends_multi_month_trend_percentage / 100), 2) : 0) +
                        (($thirtyOverThirtyTrend != 0 && $request->supply_trends_30_over_30_days_percentage > 0) ? round(($thirtyOverThirtyTrend * $request->supply_trends_30_over_30_days_percentage / 100), 2) : 0) +
                        (($multiWeekTrend != 0 && $request->supply_trends_multi_week_trend_percentage > 0) ? round(($multiWeekTrend * $request->supply_trends_multi_week_trend_percentage / 100), 2) : 0) +
                        (($sevenOverSevenTrend != 0 && $request->supply_trends_7_over_7_days_percentage > 0) ? round(($sevenOverSevenTrend * $request->supply_trends_7_over_7_days_percentage / 100), 2) : 0);
                    $reorderCalculationsTrend = (($yearOverYearHistoricalTrend != 0 && $request->reorder_trends_year_over_year_historical_percentage > 0) ? round(($yearOverYearHistoricalTrend * $request->reorder_trends_year_over_year_historical_percentage / 100), 2) : 0) +
                        (($yearOveryearCurrent != 0 && $request->reorder_trends_year_over_year_current_percentage > 0) ? round(($yearOveryearCurrent * $request->reorder_trends_year_over_year_current_percentage / 100), 2) : 0) +
                        (($multiMonthTrend != 0 && $request->reorder_trends_multi_month_trend_percentage > 0) ? round(($multiMonthTrend * $request->reorder_trends_multi_month_trend_percentage / 100), 2) : 0) +
                        (($thirtyOverThirtyTrend != 0 && $request->reorder_trends_30_over_30_days_percentage > 0) ? round(($thirtyOverThirtyTrend * $request->reorder_trends_30_over_30_days_percentage / 100), 2) : 0) +
                        (($multiWeekTrend != 0 && $request->reorder_trends_multi_week_trend_percentage > 0) ? round(($multiWeekTrend * $request->reorder_trends_multi_week_trend_percentage / 100), 2) : 0) +
                        (($sevenOverSevenTrend != 0 && $request->reorder_trends_7_over_7_days_percentage > 0) ? round(($sevenOverSevenTrend * $request->reorder_trends_7_over_7_days_percentage / 100), 2) : 0);
                    $reorderCalculationSales = (($sevenDaysSales > 0 && $request->reorder_recent_last_7_day_sales_percentage > 0) ? round(($sevenDaysSales * round(($request->reorder_recent_last_7_day_sales_percentage * $historicalRecentReorderDifference), 6) / 100), 6) : 0) +
                        (($fourteenDaysSales > 0 && $request->reorder_recent_last_14_day_sales_percentage > 0) ? round(($fourteenDaysSales * round(($request->reorder_recent_last_14_day_sales_percentage * $historicalRecentReorderDifference), 6) / 100), 6) : 0) +
                        (($thirtyDaysSales > 0 && $request->reorder_recent_last_30_day_sales_percentage > 0) ? round(($thirtyDaysSales * round(($request->reorder_recent_last_30_day_sales_percentage * $historicalRecentReorderDifference), 6) / 100), 6) : 0);

                    $overAllDayRate = round(($supplyCalculationsTrend * $supplyCalculationSalesDayRate / 100) + $supplyCalculationSalesDayRate, 6);
                    $overAllDaysSupply = 0;
                    if ($overAllDayRate > 0) {
                        $overAllDaysSupply = round(($TotalInventory / $overAllDayRate), 6);
                        $startDate = $overAllProjectedOrderStartDateRange = Carbon::now()->addDays($overAllDaysSupply);
                        $overAllProjectedOrderEndDateRange = Carbon::now()->addDays($overAllDaysSupply + $orderVolume)->format('Y-m-d');
                        $blackoutDays = 0;
                        if ($blackOutDayFlat == 0) {
                            $blackoutDays = Supplier_blackout_date::where('supplier_id', $productSettings->id)->where('blackout_date', '>=', Carbon::now()->format('Y-m-d'))->where('blackout_date', '<=', $overAllProjectedOrderStartDateRange->format('Y-m-d'))->count();
                        } else {
                            $blackoutDays = Vendor_blackout_date::where('vendor_id', $productSettings->id)->where('blackout_date', '>=', Carbon::now()->format('Y-m-d'))->where('blackout_date', '<=', $overAllProjectedOrderStartDateRange->format('Y-m-d'))->count();
                        }
                        $addDaysForProjectedDate = round($overAllDaysSupply - $TotalLeadTime - $blackoutDays);
                        $overAllProjectedReorderDate = Carbon::now()->addDays($addDaysForProjectedDate)->format('Y-m-d');
                        $todaySDate = Carbon::now();
                        $overAllDaysLeftToOrder = $todaySDate->diffInDays($overAllProjectedReorderDate, false);
                        $count = $counter = $breakLoop = $projectedReorderQty = 0;

                        $projectedReorderQty += $reorderCalculationSales;
                        if ($request->reorder_last_year_sales_percentage > 0) {
                            while ($counter != $orderVolume) {
                                $calculatedDays = 0;
                                $monthCalculation = Carbon::parse($startDate)->addMonths($count)->format('Y-m-t');
                                if ($overAllProjectedOrderEndDateRange <= $monthCalculation) {
                                    $calculatedDays = Carbon::parse($overAllProjectedOrderEndDateRange)->format('d');
                                    $breakLoop = 1;
                                } else if ($counter == 0) {
                                    $calculatedDays = $overAllProjectedOrderStartDateRange->diffInDays($monthCalculation) + 1;
                                    $monthCalculation = Carbon::parse($startDate)->addMonths($count);
                                } else {
                                    $calculatedDays = Carbon::parse($startDate)->addMonths($count)->format('t');
                                }
                                $counter += (int)$calculatedDays;
                                $count++;
                                $newCalculatedMonthYear = Carbon::parse($monthCalculation)->subYear(1)->format('Y-m');
                                $salesTrendValue = Sales_total_trend_rate::where('prod_id', $product_id)->where('month_year', $newCalculatedMonthYear)->first();
                                if ($salesTrendValue) {
                                    $salesTrendValue = round($salesTrendValue->day_rate * $request->reorder_last_year_sales_percentage / 100, 6);
                                    if ($salesTrendValue) {
                                        $projectedReorderQty += ($salesTrendValue * $reorderCalculationsTrend / 100) + ($salesTrendValue * $calculatedDays);
                                    }

                                }
                                if ($breakLoop == 1) {
                                    break;
                                }
                            }
                        }
                    }

                    $dailyLogicCalculations['total_inventory'] = $TotalInventory;
                    $dailyLogicCalculations['lead_time_amazon'] = $TotalLeadTime;
                    $dailyLogicCalculations['order_frequency'] = $orderVolume;
                    $dailyLogicCalculations['day_rate'] = $overAllDayRate;
                    $dailyLogicCalculations['days_supply'] = $overAllDaysSupply;
                    $dailyLogicCalculations['blackout_days'] = $blackoutDays;
                    $dailyLogicCalculations['projected_reorder_date'] = $overAllProjectedReorderDate;
                    $dailyLogicCalculations['projected_reorder_qty'] = round($projectedReorderQty);
                    $dailyLogicCalculations['days_left_to_order'] = $overAllDaysLeftToOrder;
                    $dailyLogicCalculations['projected_order_date_range'] = $overAllProjectedOrderStartDateRange->format('Y-m-d') . ' - ' . $overAllProjectedOrderEndDateRange;
                    $dailyLogicCalculations['sevendays_sales'] = $sevenDaysSales;
                    $dailyLogicCalculations['sevenday_day_rate'] = $sevenDaysDayRate;
                    $dailyLogicCalculations['fourteendays_sales'] = $fourteenDaysSales;
                    $dailyLogicCalculations['fourteendays_day_rate'] = $fourteenDaysDayRate;
                    $dailyLogicCalculations['thirtydays_sales'] = $thirtyDaysSales;
                    $dailyLogicCalculations['thirtyday_day_rate'] = $thirtyDaysDayRate;
                    $dailyLogicCalculations['sevendays_previous'] = $previousSevenDaysSales;
                    $dailyLogicCalculations['sevendays_previous_day_rate'] = $previousSevenDaysDayRate;
                    $dailyLogicCalculations['thirtydays_previous'] = $previousThirtyDaysSales;
                    $dailyLogicCalculations['thirtydays_previous_day_rate'] = $previousThirtyDaysDayRate;
                    $dailyLogicCalculations['thirtydays_last_year'] = $thirtyDaysSalesLastyear;
                    $dailyLogicCalculations['thirtydays_last_year_day_rate'] = $ThirtyDaysDayRateLastYear;
                    return $dailyLogicCalculations;
                }
            }
        }
    }
}

function TimeToServer($DateAndTime)
{
    //echo $this->TimeZoneOffset;
    if ($DateAndTime == '') {
        $datetime = new DateTime();
    } else {
        $datetime = new DateTime($DateAndTime);
    }
    $la_time = new DateTimeZone('America/Los_Angeles');
    $datetime->setTimezone($la_time);
    return $datetime->format('Y-m-d H:i:s');
}

function sign($number)
{
    return ($number > 0) ? 1 : (($number < 0) ? -1 : 0);
}

/**
 * Get product order from product order details
 * @param $po_details_id
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_po_from_po_details')) {
    function get_po_from_po_details($po_details_id)
    {
        $get_po_track_id = Purchase_orders::where(array('id' => $po_details_id))->first();
        if (!empty($get_po_track_id)) {
            return $get_po_track_id->track_id;
        } else {
            return ' - ';
        }
    }
}
/**
 * Get product order from product order details with days of supply
 * @param $po_details_id
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_po_from_po_details_days_of_supply')) {
    function get_po_from_po_details_days_of_supply($po_details_id)
    {
        $get_po_track_id = Purchase_orders::where(array('id' => $po_details_id))->first();
        if (!empty($get_po_track_id)) {
            return $get_po_track_id->days_of_supply;
        } else {
            return ' - ';
        }
    }
}

/**
 * get total of items price in mws orders with particular date
 * @param $params1 ,$params2
 * @return \Illuminate\Http\Response
 */


if (!function_exists('supply_order_login')) {
    function supply_order_login($params1, $params2)
    {
        $get_sum = Mws_order::where('purchase_date', '>=', $params1)->where('purchase_date', '<=', $params2)->sum('item_price');
        return $get_sum != '' ? $get_sum : '0.00';
    }
}

/**
 * get supply total trands with product , date & month wise
 * @param $get_datemonth ,$product_id
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_supply_total_trands')) {
    function get_supply_total_trands($get_datemonth, $product_id)
    {
        return Sales_total_trend_rate::where(array('prod_id' => $product_id, 'month_year' => $get_datemonth))->first();
    }
}

/**
 * replace keyword to messages in array
 * @param $array , $message
 * @return \Illuminate\Http\Response
 */

if (!function_exists('replace_keywords')) {
    function replace_keywords($array, $message)
    {
        foreach ($array as $k => $v) {
            $message = str_replace($k, $v, $message);
        }
        return $message;
    }
}

/**
 * these function is show the message with success & error
 * @param $message ,$type
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_messages')) {
    function get_messages($message, $type)
    {
        if ($type == 1) {
            return '<div class="alert alert-success">' . $message . '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>';
        } else {
            return '<div class="alert alert-danger">' . $message . '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>';
        }
        return $message;
    }
}

/**
 * get setting data
 * @param $message ,$type
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_settings')) {
    function get_settings()
    {
        $settings = settings::first();
        return (!empty($settings) ? $settings->toArray() : $settings);
    }
}
/**
 * get login user access modules
 * @param $user_id
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_user_access_modules')) {
    function get_user_access_modules($user_id)
    {
        $moduleIdArray = [];
        $usersAccessModules = User_access_modules::get();
        if (!empty($usersAccessModules)) {
            $application_module = User_access_modules::with(['userrole', 'application_modules'])->where(['user_role_id' => $user_id])->get()->toArray();
            foreach ($application_module as $module_id) {
                $moduleIdArray[] = $module_id['application_modules']['module'];
            }
        }
        return $moduleIdArray;
    }
}

/**
 * get login user role
 * @param $user_id
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_user_role')) {
    function get_user_role($user_id)
    {
        $userDetails = Users::with(['user_assigned_role', 'user_roles'])->where(array('id' => $user_id))->first()->toArray();
        return $userDetails;
    }
}
/**
 * check the user access modules or not
 * @param $module_type
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_check_access')) {
    function get_check_access($module_type)
    {
        $moduleId = Application_modules::where('module', $module_type)->first()->id;
        $userRoleID = Users::with('user_roles')->whereIn('id', [Auth::id()])->first()->user_roles[0]->id;
        $userRolePermissionId = Role_access_modules::where('role_id', $userRoleID)->where('application_module_id', $moduleId)->first();
        if (Gate::allows('access', $userRolePermissionId)) {
            return 1;
        } else {
            return 0;
        }
        /// return $this->userRolePermissionId;
    }
}

/**
 * get the login user access modules
 * @param $module_type ,$type
 * @return \Illuminate\Http\Response
 */


if (!function_exists('get_access')) {
    function get_access($module_type, $type)
    {
        $moduleId = Application_modules::where('module', $module_type)->first()->id;
        $userRoleID = Users::with('user_roles')->whereIn('id', [Auth::id()])->first()->user_roles[0]->id;
        $userRolePermissionId = Role_access_modules::where('role_id', $userRoleID)->where('application_module_id', $moduleId)->first();
        if (Gate::allows($type, $userRolePermissionId)) {
            return 1;
        } else {
            return 0;
        }
    }
}

/**
 * check the user access this modules or not
 * @param $module_type ,$type
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_user_check_access')) {
    function get_user_check_access($module_type, $type)
    {
        $moduleId = Application_modules::where('module', $module_type)->get()->toArray();
        $userRoleID = Users::with('user_roles')->whereIn('id', [Auth::id()])->first()->user_roles[0]->id;
        $userRolePermissionId = User_access_modules::where('user_id', Auth::id())->where('application_module_id', $moduleId)->first();
        if (!empty($userRolePermissionId)) {
            if (Gate::allows($type, $userRolePermissionId)) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }

    }
}

if (!function_exists('get_module_id')) {
    function get_module_id($module_type)
    {
        $moduleId = Application_modules::where('module', $module_type)->first();
        if (!empty($moduleId)) {
            return $moduleId->id;
        } else {
            return 0;
        }
    }
}

/**
 * check the users get it's sub users or not
 * @param $user_id
 * @return \Illuminate\Http\Response
 */


if (!function_exists('get_sub_user_access')) {
    function get_sub_user_access($user_id)
    {
        return User_access_modules::with(['application_modules'])->where(array('user_id' => $user_id, 'create' => 1, 'edit' => 1, 'delete' => 1, 'view' => 1, 'access' => 1))->get()->toArray();
        //return Role_access_modules::where(array('role_id'=>3,'create'=>1,'edit'=>1,'delete'=>1,'view'=>1,'access'=>1))->get()->toArray();
    }
}

/**
 * get user all marketplaces
 * @param $user_id
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_user_marketplace_list')) {
    function get_user_marketplace_list($user_id)
    {
        return Usermarketplace::where(array('user_id' => $user_id))->get()->toArray();
    }
}

/**
 * get user all marketplaces with it's country
 * @param $user_id
 * @return \Illuminate\Http\Response
 */


if (!function_exists('get_usermarketplace')) {
    function get_usermarketplace($user_id)
    {
        return Usermarketplace::with('mws_market_place_country')->where(array('user_id' => $user_id))->get()->toArray();
    }
}

function get_dashed_name($object)
{
    return Stringy::create(get_real_class($object))->dasherize()->__toString();
}

function get_real_class($object)
{
    $class_name = get_class($object);
    if (preg_match('@\\\\([\w]+)$@', $class_name, $matches)) {
        $class_name = $matches[1];
    }
    return $class_name;
}

function add_to_path($path)
{
    $lib_path = app_path($path);
    $include_path = get_include_path();
    if (!str_contains($include_path, $lib_path)) {
        set_include_path($include_path . PATH_SEPARATOR . $lib_path);
    }
}

/**
 * get default vendor setting
 * @param no -params
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_default_vendor_setting')) {
    function get_default_vendor_setting()
    {
        return Vendor_default_setting::first();
    }
}
/**
 * get default warehouse setting
 * @param no -params
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_default_warehouse_setting')) {
    function get_default_warehouse_setting()
    {
        return Warehouse_default_setting::first();
    }
}

if(!function_exists('get_warehouse_name')){
    function get_warehouse_name($id){
        return Warehouse::where(array('id'=>$id))->first();
    }
}
/**
 * get default suppliers setting
 * @param no -params
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_default_suppliers_setting')) {
    function get_default_suppliers_setting()
    {
        return Supplier_default_setting::first();
    }
}

/**
 * check vendors assign in product or not
 * @param $productid
 * @return \Illuminate\Http\Response
 */


if (!function_exists('check_vendors')) {
    function check_vendors($product_id)
    {
        return Vendor_product::where(array('product_id' => $product_id))->get()->toArray();
    }
}

if(!function_exists('get_selected_fileds')){
    function get_selected_fileds($marketplace,$type){
        return App\Models\User_field_access::where(array('marketplace_id'=>$marketplace,'type'=>$type))->selectRaw('GROUP_CONCAT(field_list_id) as fileds')->first();
    }
}

/**
 * check suppliers assign in product or not
 * @param $productid
 * @return \Illuminate\Http\Response
 */

if (!function_exists('check_suppliers')) {
    function check_suppliers($product_id)
    {
        return Supplier_product::where(array('product_id' => $product_id))->get()->toArray();
    }
}

/**
 * get purchase instance with particular marketplace
 * @param $marketplace
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_purchase_instace')) {
    function get_purchase_instace($marketplace)
    {
        return Purchase_instances::where(array('user_marketplace_id' => $marketplace, 'status' => 1))->get()->toArray();
    }
}
/**
 * check warehouse assign in product or not
 * @param $productid
 * @return \Illuminate\Http\Response
 */

if (!function_exists('check_warehouse')) {
    function check_warehouse($product_id)
    {
        return Warehouse_product::where(array('product_id' => $product_id))->get()->toArray();
    }
}

/**
 * get product wise daily calulations
 * @param $productid
 * @return \Illuminate\Http\Response
 */


if (!function_exists('check_daily_caculation')) {
    function check_daily_caculation($product_id)
    {
        return Daily_logic_Calculations::where(array('prod_id' => $product_id))->first();

    }
}

/**
 * get parent user marketplace
 * @param $user_id
 * @return \Illuminate\Http\Response
 */


if (!function_exists('get_parent_usermarketplace')) {
    function get_parent_usermarketplace($user_id)
    {
        $total_marketplace = array();
        $get_parent_user = Users::where(array('id' => $user_id))->first();
        if (!empty($get_parent_user) && $get_parent_user->parent_id != '') {
            $total_marketplace = Usermarketplace::with('mws_market_place_country')->where(array('user_id' => $get_parent_user->parent_id))->get()->toArray();
            return $total_marketplace;
        } else {
            return $total_marketplace;
        }
    }
}

/*if(!function_exists('get_backout_date')){
    function get_backout_date($countryid,$user_marketplace_id){
        $getCountry = Country::where(array('id'=>$countryid))->first();
        $get_usermarketplace = Usermarketplace::where(array('id'=>$user_marketplace_id))->first();
        if(!empty($getCountry) && ($get_usermarketplace) ){
            return Member_blackoutdate_setting::where(array('user_marketplace_id'=>$get_usermarketplace->id,'country_id'=>$countryid->id,['blackout_date','>=',date('Y-m-d')]))->get()->toArray();
        }
    }
}*/


/**
 * get blackout date setting
 * @param $countryid
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_backout_date')) {
    function get_backout_date($countryid)
    {
        $getCountry = Country::where(array('id' => $countryid))->first();
        if (!empty($getCountry)) {
            return Member_blackoutdate_setting::where(array('user_marketplace_id' => session('MARKETPLACE_ID'), 'country_id' => $getCountry->id, ['blackout_date', '>=', date('Y-m-d')]))->get()->toArray();
        }
    }
}

/**
 * get add old members permission
 * @param no -params
 * @return \Illuminate\Http\Response
 */


if (!function_exists('add_old_members_permission')) {
    function add_old_members_permission()
    {
        $get_membersids = User_assigned_role::where(array('user_role_id' => 3))->selectRaw('GROUP_CONCAT(id) as members_id')->get();
        if (!empty($get_membersids) && $get_membersids[0]->members_id != '') {
            $get_child_member = Users::whereIn('parent_id', [$get_membersids[0]->members_id])->get()->toArray();
            if (!empty($get_child_member)) {
                foreach ($get_child_member as $submemberids) {
                    $get_application_module = Application_modules::get()->toArray();
                    if (!empty($get_application_module)) {
                        foreach ($get_application_module as $module) {
                            $check_application_check = Role_access_modules::where(array('role_id' => 3, 'application_module_id' => $module['id']))->first();
                            $check_already_user_permission = User_access_modules::where(array('application_module_id' => $module['id'], 'user_id' => $submemberids['id']))->get()->toArray();
                            if (empty($check_already_user_permission)) {
                                $modue['role_id'] = 3;
                                $modue['user_id'] = $submemberids['id'];
                                $modue['application_module_id'] = $module['id'];
                                $modue['create'] = 0;
                                $modue['edit'] = 0;
                                $modue['delete'] = 0;
                                $modue['view'] = 0;
                                $modue['access'] = 0;
                                User_access_modules::create($modue);
                            }
                        }
                    }
                }
            }
        }

    }
}

/**
 * get suppliers name
 * @param $id
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_suppliers_name')) {
    function get_suppliers_name($id)
    {
        $suppliers = Supplier::where(array('id' => $id))->first();
        return isset($suppliers->supplier_name) && $suppliers->supplier_name != '' ? $suppliers->supplier_name : '';
    }
}

/**
 * get vendors name
 * @param $id
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_vendors_name')) {
    function get_vendors_name($id)
    {
        $vendors = Vendor::where(array('id' => $id))->first();
        return isset($vendors->vendor_name) && $vendors->vendor_name != '' ? $vendors->vendor_name : '';
    }
}

/**
 * get warehouse name
 * @param $id
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_warehouse_name')) {
    function get_warehouse_name($id)
    {
        $warehouse = Warehouse::where(array('id' => $id))->first();
        return isset($warehouse->warehouse_name) && $warehouse->warehouse_name != '' ? $warehouse->warehouse_name : ' - ';
    }
}

/**
 * get vendors product with productid
 * @param $product_id
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_product_vendors')) {
    function get_product_vendors($product_id)
    {
        return Vendor_product::with(['vendor'])->where(array('product_id' => $product_id))->get()->toArray();
    }
}
/**
 * get suppliers product with productid
 * @param $product_id
 * @return \Illuminate\Http\Response
 */
if (!function_exists('get_country_name')) {
    function get_country_name($country_id)
    {
        $country = Country::where(array('id' => $country_id))->first();
        return $country->country_name != '' ? $country->country_name : ' - ';
    }
}


if (!function_exists('get_product_suppliers')) {
    function get_product_suppliers($product_id)
    {
        return Supplier_product::with(['supplier'])->where(array('product_id' => $product_id))->get()->toArray();
    }
}
/**
 * get warehouse product with productid
 * @param $product_id
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_product_warehouse')) {
    function get_product_warehouse($product_id)
    {
        return Warehouse_product::with(['warehouse'])->where(array('product_id' => $product_id))->get()->toArray();
    }
}
/**
 * get product details with particular product id
 * @param $product_id
 * @return \Illuminate\Http\Response
 */
if (!function_exists('get_product_details')) {
    function get_product_details($product_id)
    {
        $get_product = Mws_product::where(array('id' => $product_id))->first();
        return $get_product->prod_name;
    }
}
/**
 * get product sku with particular product id
 * @param $product_id
 * @return \Illuminate\Http\Response
 */
if (!function_exists('get_product_sku')) {
    function get_product_sku($product_id)
    {
        $get_product = Mws_product::where(array('id' => $product_id))->first();
        return $get_product->sku;
    }
}

if (!function_exists('table_module_data')) {
    function table_module_data($module_id, $value)
    {
        return Field_list::where(array('module_id' => $module_id, 'original_name' => $value))->first();
    }
}

/**
 * get product warehouse name
 * @param $product_id
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_product_warehouse_name')) {
    function get_product_warehouse_name($product_id)
    {
        return Warehouse_product::with(['warehouse'])->where(array('product_id' => $product_id))->get()->first();
    }
}

if(!function_exists('get_reorder_sehedule')){
    function get_reorder_sehedule($id){
        return \App\Models\Reorder_schedule_detail::where('supplier_vendor', env('VENDOR'))->where('supplier_vendor_id', $id)->get()->toArray();
    }
}
if(!function_exists('get_reorder_sehedule_supplier')){
    function get_reorder_sehedule_supplier($id){
        return \App\Models\Reorder_schedule_detail::where('supplier_vendor', env('SUPPLIER'))->where('supplier_vendor_id', $id)->get()->toArray();
    }
}
if(!function_exists('get_lead_time')){
    function get_lead_time($lead_time_id,$lead_time_details_id,$type){
        //if($type != ''){
            $get_lead_time_ids = \App\Models\Lead_time::where(array('id'=>$lead_time_id,'type'=>$type))->first();
            if(!empty($get_lead_time_ids)){
                $get_id = $get_lead_time_ids->id;
                return \App\Models\Lead_time_value::where(array('lead_time_id'=>$get_id,'lead_time_detail_id'=>$lead_time_details_id))->first();
            }else{
                $get_id = $lead_time_id;
                return array();
            }
        // }else{
        //     echo 2;
        //     $get_id = $lead_time_id;
        //     return array();
        // }


    }
}

/**
 * check product is already exists or not
 * @param  $global_marketplace ,$product_id
 * @return \Illuminate\Http\Response
 */


if(!function_exists('check_product_already_exist')){
    function check_product_already_exist($global_marketplace,$product_id){
        $get_all_purchase_inastnce = Purchase_instances::where(array('user_marketplace_id'=>$global_marketplace,'status'=>1))->selectRaw('group_concat(id) as  product_instance')->get()->toArray();
       if(!empty($get_all_purchase_inastnce)){
           $purchase_instancesid = explode(',',$get_all_purchase_inastnce[0]['product_instance']);
           $get_purchase_instace = Purchase_instance_details::where(array('product_id'=>$product_id))->whereIn('purchesinstances_id',$purchase_instancesid)->get()->toArray();
            if(!empty($get_purchase_instace)){
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
}

/**
 * check user is authenticated or not
 * @param  $user_id , $token
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_authenticate_user')) {
    function get_authenticate_user($user_id, $token)
    {
        $userDetails = Usermarketplace::where(array('id' => $user_id, 'api_token' => $token))->first();
        return $userDetails;
    }
}
/**
 * get country full name on the code
 * @param  $code
 * @return \Illuminate\Http\Response
 */
if (!function_exists('get_vendors_total_product')) {
    function get_vendors_total_product($vendor_id)
    {
        return Vendor_product::where(array('vendor_id' => $vendor_id))->count();
    }
}

if (!function_exists('get_country_fullname')) {
    function get_country_fullname($code)
    {
        $data = get_country_list();
        $name = "";
        if (!empty($data)) {
            foreach ($data as $key => $data1) {
                if ($code == $key) {
                    $name = $data1;
                    break;
                }
            }
        }
        return $name;
    }
}

if (!function_exists('get_country_fullname_code_name')) {
    function get_country_fullname_code_name($id)
    {
        $name = Country::where(array('id' => $id))->first();
        return !empty($name) ? $name->country_name : '';
    }
}

if(!function_exists('get_vendor_supplier_name')){
    function get_vendor_supplier_name($user_type,$supplier_vendor){
        $name = '';
        if($user_type == 'suppliers'){
            $get_suppliers = Supplier::where(array('id'=>$supplier_vendor))->first();
            $name = @$get_suppliers->supplier_name;
        }else{
            $get_vendors = Vendor::where(array('id'=>$supplier_vendor))->first();
            $name = @$get_vendors->vendor_name;
        }
        return $name;
    }
}

/**
 * get state full name on the country code
 * @param  $cn_code ,$st_code
 * @return \Illuminate\Http\Response
 */
if (!function_exists('get_state_fullname')) {
    function get_state_fullname($cn_code, $st_code)
    {
        $data = get_state_list($cn_code);

        $name = "";
        if (!empty($data)) {
            foreach ($data as $key => $data1) {
                if ($st_code == $key) {
                    $name = $data1;
                    break;
                }
            }
        }
        return $name;
    }
}
/**
 * get city full name on the state code
 * @param  $cn_code ,$st_code,$ct_code
 * @return \Illuminate\Http\Response
 */
if (!function_exists('get_city_fullname')) {
    function get_city_fullname($cn_code, $st_code, $ct_code)
    {
        $data = get_city_list($st_code);

        $name = "";
        if (!empty($data)) {
            foreach ($data as $key => $data1) {
                if ($ct_code == $key) {
                    $name = $data1;
                    break;
                }
            }
        }
        return $name;
    }
}
if (!function_exists('get_fulfillmentCenter_details')) {
    function get_fulfillmentCenter_details($fulfillementCenterID)
    {
        $data = array(
                "LAX9"=>"11263 Oleander Ave, Fontana, California",
                "SMF3"=>"3923 S B St, Stockton, CA 95206",
                "LAS1"=>"601 E Dale, Henderson, NV 89044",
                "DET2"=>"50500 Mound Rd, Shelby Township, MI 48317",
                "MEM6"=>"11505 Progress Way, OLIVE BRANCH, MS, 38654",
                "JVL1"=>"3150 Colley Road",
                "DCA6"=>"6001 Bethlehem Blvd, Edgemere, MD 21219",
                "ACY2"=>"1101 E Pearl St, Burlington, NJ 08016",
                "CLT3"=>"6500 Davidson Hwy, Concord, NC 28027",
                "ALB1"=>"1835 US ROUTE 9. Castleton, NY 12033",
                "SLC2"=>"7148 W. Old Bingham Hwy in West Jordan",
                "PIT2"=>"1200 Westport Rd, Imperial, PA 15126",
                "GSO1"=>"1656 Old Greensboro Rd, Kernersville, NC 27284",
                "ORD2"=>"2731 S Frontage Rd W, Channahon - Waze",
                "GEG2"=>"18007 Garland Ave, Spokane Valley, WA 99216",
                "SAT4"=>"10384 W US Hwy 90, San Antonio, TX 78245",
                "IGQ2"=>"IGQ2, 23257 Central Ave, University Park",
                "OKC2"=> "9201 S. Portland Ave.",
                "FOE1"=> "9700 Leavenworth Rd, Kansas City, KS 66109",
                "PDX7"=> "4775 Depot Ct SE, Salem, OR 97317",
                "FTW5" => "1475 Akron Way, Forney, TX 75126",
                "SBD2" => "1494 S Waterman Ave, San Bernardino, CA 92408",
                "SAV3" => " 6803-6999 Skipper Rd, Macon, GA 31216",
                "CMH3" => "700 Gateway Blvd, Monroe, OH 45050",
                "HSV1" => "23366 BIBB GARRETT RD, AL 35671",
                "TEB4" => "742 Courses Landing Rd, NJ 08069-2956",
                "TPA3" => "576 C Fred Jones Blvd, FL 33823",
                "MCO2" => "2600 N NORMANDY BLVD, FL 32725",
                "SMF6" => "4930 Allbaugh Dr, CA 95837-9109",
                "SCK1" => "4611 Newcastle Rd, CA 95215-9446",
                "LFT1" => "3620 NE Evangeline Throughway, LA 70520-5948",

//            "Mexico" => array(
                "MEX1" => "Cuautitlán Izcalli",
//            ),
//            "British Columbia" => array(
                "YVR2" => "450 Derwent PL Delta, British Columbia V3M 5Y9",
                "YVR3" => "109 Braid St, New Westminster, BC V3L 3R9, Canada",
                "YYC1" => "Construction is expected to be done fall 2018",
//            ),
//            "Ontario" => array(
                "YYZ1" => "6363 Millcreek Drive Mississauga, Ontario L5N 1L8",
                "YYZ2" => "2750 Peddie Rd. Milton, Ontario L9T 6Y9",
                "YYZ3" => "7995 Winston Churchill Blvd. Brampton, Ontario L6Y 0B2",
                "YYZ4" => "8050 Heritage Rd., Brampton, Ontario L6Y 0C9",
                "YYZ6" => "8050 Heritage Rd., Brampton, Ontario L6Y 0C9",
                "PRTO" => "6110 Cantay Rd. Mississauga, Ontario L5R 3W",
//            ),
//            "Arizona" => array(
                "PHX3" => "6835 W. Buckeye Rd. Phoenix, AZ, 85043",
                "PHX5" => "16920 W. Commerce Drive, Goodyear, AZ 85338",
                "PHX6" => "4750 W. Mohave Street, Phoenix, AZ 85043",
                "PHX7" => "800 N. 75th Avenue, Phoenix, AZ 85043",
                "TFC1" => "5050 W. Mohave Street, Phoenix, AZ 85043",
                "PHX9" => "777 S 79th Avenue, Tolleson, Maricopa County, AZ 85353 Arizona",
//            ),
//            "California" => array(
                "SNA4" => "2496 W Walnut St. Rialto CA 92376-3009",
                "SNA6" => "5250 Goodman Road, Eastvale, CA 92880",
                "SNA7/8" => "555 East Orange Show Rd. San Bernardino, CA 92408-2453",
                "SNA9" => "5250 Goodman Road, Eastvale, CA 92880",
                "SJC7" => "188 Mountain House Pkwy, Tracy, CA 95391",
                "ONT2/5/7" => "2020 E Central Avenue, Southgate Building 4, San Bernardino, CA 92408-2606",
                "ONT5" => "2020 E Central Avenue, Southgate Building 4, San Bernardino, CA 92408-2606",
                "ONT7" => "2020 E Central Avenue, Southgate Building 4, San Bernardino, CA 92408-2606",
                "ONT6" => "24208 San Michele, Moreno Valley, CA 92551",
                "ONT9" => "2125 W San Bernardino Ave, Redlands, CA, United States",
                "OAK3" => "255 Park Center Drive, Patterson City, CA 95363-8876",
                "OAK4" => "1555 N. Chrisman Road, Tracy, CA 95304-9370",
                "OAK5" => "38811 Cherry Street, Newark, CA 94560",
                "XUSD" => "1909 Zephyr Street, Stockton, CA 95206",
                "LGB3" => "4590 Goodman Way, Building 1, Eastvale, CA 91752-5088",
                "LGB4" => "27517 Pioneer Ave, Redlands, CA 92374",
                "LGB6" => "20901 Krameria Ave, Riverside, CA 92518-1513",
                "SMF1" => "4900 W Elkhorn Blvd, Metro Air Park, Sacramento, CA 95835-9505",
                "SNA7" => "555 East Orange Show Rd. San Bernardino, CA 92408-2453",
                "SNA8" => "555 East Orange Show Rd. San Bernardino, CA 92408-2453",
                "ONT2" => "E Central Avenue, Southgate Building 3, San Bernardino, CA 92408-0123",
                "OAK6" => "38811 Cherry Street, Newark, California, 94560-4939",
                "ONT8" => "24300 Nandina Ave., Moreno Valley, California, 92551-9534",
                "LGB8" => "1568 N Linden Ave, Rialto, CA 92376",
//            ),
//            "Colorado" => array(
                "DEN5" => "19799 E 36th Dr, Aurora, CO 80011",
                "DEN2" => "24006 East 19th Avenue, Aurora, CO 80019-3705",
                "DEN6" => "480 E 55th Ave, Denver, CO 80216",
//            ),
//            "Connecticut" => array(
                "BDL1" => "801 Day Hill Road Windsor, CT 06095",
                "BDL5" => "29 Research Parkway, Wallingford, CT 06492",
                "BDL2" => "409 Washington Ave, North Haven, CT 06473",
//            ),
//            "Delaware" => array(
                "PHL1" => "1 Centerpoint Boulevard, New Castle, DE 19720",
                "PHL3" => "1600 Johnson Way, New Castle, DE 19720",
                "PHL7" => "560 Merrimac Avenue, Middletown, DE 19709-4652",
                "PHL8" => "727 N. Broad Street, Middletown, DE 19709-1166",
//            ),
//            "Florida" => array(
                "TPA1" => "351 30th Street NE, Ruskin, FL 33570",
                "TPA2" => "1760 County Line Road, Lakeland, FL 33811",
                "LAL1" => "1760 County Line Road, Lakeland, FL 33811",
                "MIA1/5" => "1900 NW 132nd Place, Doral, FL, 33182",
                "MCO1/5" => "205 Deen Still Road, Davenport, FL 33897",
                "JAX2" => "12900 Pecan Park Rd., Jacksonville, FL 32218-2432",
                "JAX3" => "13333 103rd St. Cecil Commerce Center, Jacksonville, FL 32221-8686",
                "JAX5" => "4948 Bulls Bay Hwy, Jacksonville, FL 32219-3235",
//            ),
//            "Georgia" => array(
                "ATL6" => "4200 North Commerce, Atlanta, GA 30344-5707",
                "ATL8" => "2201 Thornton Rd., Lithia Springs, GA 30122",
                "ATL7" => "6855 Shannon Pkwy, Union City, GA 30291-2091",
                "MGE1" => "650 Broadway Ave, Braselton, GA 30517-3002",
                "MGE3" => "808 Hog Mountain Rd., Building F, Jefferson, GA, 30549",
//            ),
//            "Illinois" => array(
                "MDW2" => "401 E Laraway Road, Joliet, Illinois 60433",
                "MDW4" => "201 Emerald Drive, Joliet, IL 60433",
                "MDW6" => "1125 Remington Blvd, Romeoville, IL 60446",
                "STL4" => "3050 Gateway Commerce Center Drive South, Edwardsville, IL 62025",
                "STL6" => "3931 Lakeview Corporate Drive, Edwardsville, IL 62025",
                "STL7" => "3931 Lakeview Corporate Drive, Edwardsville, IL 62025",
                "MDW7" => "6521 W Monee Manhattan Road, Monee, IL 60449",
                "MDW8" => "1750 Bridge Dr., Waukegan, IL 60085-3004",
                "MDW9" => "2865 Duke Parkway, Aurora, IL 60502-9551",
                "MDW5" => "1125 W Remington Blvd, Romeoville, IL 60556",
//            ),
//            "Indiana" => array(
                "XUSE" => "5100 S Indianapolis Road, Whitestown, IN 46075",
                "IND1" => "4255 Anson Boulevard, Whitestown, IN 46075",
                "IND2" => "715 Airtech Parkway, Plainfield, IN 46168",
                "IND3" => "715 Airtech Parkway, Plainfield, IN 46168",
                "IND4" => "710 South Girls School Road, Indianapolis, IN 46214",
                "IND5" => "800 S Perry Road, Plainfield, IN 46168",
                "IND6" => "1453 10th Street, Jeffersonville, IN 47130",
                "IND7" => "9101 Orly Dr, Indianapolis, IN 46241",
                "SDF8" => "900 Patrol Road, Jeffersonville, IN 47130",
//            ),
//            "Kansas" => array(
                "MCI1" => "113th Street and Renner Boulevard, Lenexa, KS, 66219",
                "MKC4" => "19645 Waverly Road, Edgerton, KS 66021-9588",
                "MCI5" => "16851 W 113th Street, Lenexa, KS 66219",
                "MKC6" => "6925 Riverview Ave., Kansas City, KS 66102",
                "TUL1" => "2654 N US Highway 169 Coffeyville, KS 67337",
//            ),
//            "Kentucky" => array(
                "SDF1" => "1105 S Columbia Avenue, Campbellsville, KY 42718",
                "SDF2" => "4360 Robards Lane, Louisville, KY 40218",
                "CVG1" => "1155 Worldwide Blvd. Hebron, KY 41048",
                "CVG2" => "1600 Worldwide Blvd. Hebron, KY 41048",
                "CVG3" => "3680 Langley Drive, Hebron, KY 41048",
                "CVG5/7" => "Hebron Building 2, 2285 Litton Lane, Hebron, KY 41048-8435",
                "LEX1" => "1850 Mercer Road, Lexington, KY 40511",
                "LEX2" => "172 Trade Street, Lexington, Kentucky 40511",
                "SDF4" => "376 Zappos.com Boulevard, Shepherdesville, KY 40165",
                "SDF6" => "271 Omega Parkway, Shepherdsville, KY 40165",
                "SDF7" => "300 Omicron Court, Shepherdsville, KY 40165",
                "SDF9" => "100 W. Thomas P. Echols Lane, Shepherdsville, KY 40165",
//            ),
//            "Maryland" => array(
                "BWI1" => "2010 Broening Highway, Baltimore, MD 21224",
                "BWI2" => "5001 Holabird Avenue, Baltimore, MD 21224",
                "BWI5" => "5001 Holabird Avenue, Baltimore, MD 21224",
                "MDT2" => "600 Principio Parkway West, North East, MD 21901-2914",
//            ),
//            "Massachusetts" => array(
                "BOS5" => "1000 Tech Center Drive, Stoughton, MA 0207",
                "BOS7" => "1180 Innovation Way, Fall River, MA 02722",
//            ),
//            "Michigan" => array(
                "DTW5" => "19991 Brownstown Center Dr, Brownstown Charter Twp, MI 48183",
                "DTW1" => "39000 Amrhein Rd., Livonia, MI 48150-1043",
                "DET1" => "39000 Amrhein Rd, Livonia, MI 48150",
//            ),
//            "Minnesota" => array(
                "MSP1" => "2601 4th Avenue East, Shakopee, MN 55379",
                "MSP5" => "5825 11th Avenue East, Shakopee, MN 55379",
//            ),
//            "Nevada" => array(
                "RNO4" => "8000 North Virginia Street, Reno, NV 89506",
                "LAS2" => "3837 Bay Lake Trail, Suite 113, Las Vegas, NV 89193-0001",
                "LAS6" => "4550 Nexus Way, North Las Vegas, NV 89115",
                "RNO1" => "1600 East Newlands Dr. Fernley, NV 89408",
                "RNO2" => "8000 North Virginia Street Reno, Reno, NV 85906",
//            ),
//            "New Hampshire" => array(
                "BOS1" => "10 State Street, Nashua, NH 03063",
//            ),
//            "New Jersey" => array(
                "ACY5" => "2277 Center Square Road, Swedesboro, Logan Township, NJ 08085",
                "EWR4" => "50 New Canton Way, Robbinsville, NJ 08691",
                "EWR5" => "301 Blair Road #100, Avenel, NJ 07001",
                "EWR6" => "275 Omar Avenue, Avenel, NJ 07001",
                "EWR7" => "275 Omar Avenue, Avenel, NJ 07001",
                "EWR9" => "8003 Industrial Avenue, Carteret, NJ 07008",
                "LGA7" => "380 Middlesex Avenue, Carteret, NJ 07008",
                "LGA8" => "380 Middlesex Avenue, Carteret, NJ 07008",
                "LGA6" => "8003 Industrial Ave. Carteret, NJ 07008-3529",
                "LGA9" => "2170 State Route 27, Edison, NJ 08817-3332",
                "TEB3" => "2651 Oldmans Creek Rd, Swedesboro, Logan Township, NJ 08085-4503",
                "TEB6" => "22 Highstown-Cranbury Station Road, Cranbury Township, NJ 08512",
                "ABE8" => "309 Cedar Lane, Florence Township, NJ 08518",
                "EWR8" => "698 US-46, Teterboro, NJ 07608",
//            ),
//            "New York" => array(
                "BUF5" => "4201 Walden Avenue, Lancaster, NY 14086",
//            ),
//            "North Carolina" => array(
                "CLT5" => "1745 Derita Road, Concord, NC 28027",
                "CLT1" => "1745 Derita Rd., Concord, NC 28027-3353",
                "RDU5" => "1805 TW Alexander Drive, Durham, NC 27703",
                "CLT2" => "10240 Old Dowd Rd, Charlotte, NC 28214",
//            ),
//            "Ohio" => array(
                "CLE5" => "8685 Independence Parkway, Twinsburg, OH 44087",
                "CMH1" => "11903 National Road SW, Etna, OH 43062",
                "CMH2" => "2700 Rohr Road, Groveport, OH 43125",
//            ),
//            "Oregon" => array(
                "PDX5" => "5647 NE Huffman St, Hillsboro, OR 97124",
//            ),
//            "Pennsylvania" => array(
                "AVP1" => "550 Oak Ridge Road, Hazleton, PA 18202",
                "AVP6" => "1 Commerce Road, Pittston, PA 18640",
                "AVP8" => "250 Enterprise Way, Pittston, PA 18640",
                "ABE2" => "705 Boulder Drive, Breinigsville, PA 18031",
                "ABE3" => "650 Boulder Drive, Breinigsville, PA 18031",
                "ABE4" => "1610 Van Buren Rd., Easton, PA 18045-1707",
                "ABE5" => "6455 Allentown Boulevard, Harrisburg, PA 17112",
                "MDT1" => "2 Ames Drive, Carlisle, PA 17015",
                "PHL4" => "21 Roadway Drive, Carlisle, PA 17015",
                "PHL5" => "500 McCarthy Drive, Lewisberry, PA 17339",
                "PHL6" => "675 Allen Road, Carlisle, PA 17015",
                "PIT5" => "2250 Roswell Drive, Pittsburgh, PA 15205",
                "XUSC" => "40 Logistics Drive, Carlisle, PA 17013",
                "AVP2/3" => "298 1st Ave, Gouldsboro, PA 18424",
                "AVP3" => "298 1st Ave, Gouldsboro, PA 18424-9464",
                "PHL9" => "2 Ames Drive, Carlisle, PA 17015",
                "PIT1" => "2250 Roswell Drive, Pittsburgh, PA 15205",
//            ),
//            "South Carolina	" => array(
                "CAE1" => "4400 12th Street Extension, West Columbia, SC 29172",
                "GSP1" => "402 John Dodd Road, Spartanburg, SC 29303-6312",
//            ),
//            "Tennessee" => array(
                "BNA1" => "14840 Central Pike, Lebanon, TN 37090",
                "BNA2" => "500 Duke Drive, Lebanon, TN 37090",
                "BNA3" => "2020 Joe B Jackson Parkway, Murfreesboro, TN 37127",
                "BNA5" => "50 Airways Blvd, Nashville, TN 37217",
                "CHA1" => "7200 Discovery Drive, Chattanooga, TN 37416",
                "CHA2" => "225 Infinity Drive NW, Charleston, TN 37310",
//            ),
//            "Texas" => array(
                "DFW1" => "2700 Regent Boulevard, DFW Airport, TX 75261",
                "DFW8" => "2700 Regent Boulevard, DFW Airport, TX 75261",
                "DFW6" => "940 W Bethel Road, Coppell, TX 75019-4424",
                "DFW7" => "700 Westport Parkway, Fort Worth, TX 76177-4513",
                "FTW3" => "15201 Heritage Parkway, Fort Worth, TX 76177-2517",
                "FTW4" => "15201 Heritage Parkway, Fort Worth, TX 76177-2517",
                "SAT1" => "6000 Enterprise Avenue, Schertz, TX 78154",
                "SAT5" => "1410 S. Callaghan Road, San Antonio, TX 78227",
                "XUSB" => "14900 Frye Road, Fort Worth, TX 76155",
                "HOU1" => "8120 Humble Westfield Road, Humble, TX 77338",
                "FTW2" => "940 W Bethel Rd, Coppell, TX 75019-4424",
                "FTW7" => "944 W. Sandy Lake Rd, Coppell, TX 75019-3989",
                "FTW8" => "3351 Balmorhea Dr. Dallas, TX 75241-7304",
                "SAT2" => "1401 E McCarty Ln, San Marcos, TX 78666",
                "HOU2" => "10550 Ella St., Houston, TX 77038-2324",
                "HOU3" => "31555 Highway 90 E, Brookshire, TX 77423-2769",
//            ),
//            "Virginia" => array(
                "RIC1" => "5000 Commerce Way, Petersburg, VA 23803",
                "RIC2" => "1901 Meadowville Technology Parkway, Chester, VA 23836",
                "RIC3" => "1901 Meadowville Technology Parkway, Chester, VA 23836",
//                "BWI1" => "45121 Global Plaza, Sterling, VA 20166",
                "BWI4" => "165 Business Blvd, Clear Brook, VA 22624-1568",
                "RIC5" => "11600 N Lakeridge Pkwy, Ashland, VA 23005",
//            ),
//            "Washington" => array(
                "BFI1" => "1800 140th Avenue, E Sumner, WA",
                "BFI3" => "2700 Center Drive, Dupont, WA 98327",
                "BFI4" => "21005 64th Ave S, Kent, WA 98032",
                "BFI5" => "Building B, 20526 59th Pl S, Kent, WA 98032",
                "SEA6" => "2646 Rainier Avenue, South Seattle, WA 98144",
                "SEA8" => "1227 124th Avenue Northeast, Bellevue, WA, 98005",
                "BFI7" => "1800 140th Ave E, Sumner, WA 98390",
                "BFI6" => "21005 64th Ave. South; Kent, Washington 98032",
//            ),
//            "Wisconsin" => array(
                "MKE5" => "11211 Burlington Road, Kenosha, WI 53144",
                "MKE1" => "3501 120th Ave, Kenosha, Wisconsin, 53144-7502",
//            ),
        );

        $returnData = array();
        if ($fulfillementCenterID != null) {
            if (array_key_exists($fulfillementCenterID, $data)) {
                $returnData = $data[$fulfillementCenterID];
                return $returnData;
            }
        }
    }
}
/**
 * get country list
 * @param  no -params
 * @return \Illuminate\Http\Response
 */
if (!function_exists('get_country_list')) {
    function get_country_list()
    {
        return array(
            'US' => 'United States',
            'AF' => 'Afghanistan',
            'AL' => 'Albania',
            'DZ' => 'Algeria',
            'AS' => 'American Samoa',
            'AD' => 'Andorra',
            'AO' => 'Angola',
            'AI' => 'Anguilla',
            'AQ' => 'Antarctica',
            'AG' => 'Antigua And Barbuda',
            'AR' => 'Argentina',
            'AM' => 'Armenia',
            'AW' => 'Aruba',
            'AU' => 'Australia',
            'AT' => 'Austria',
            'AZ' => 'Azerbaijan',
            'BS' => 'Bahamas',
            'BH' => 'Bahrain',
            'BD' => 'Bangladesh',
            'BB' => 'Barbados',
            'BY' => 'Belarus',
            'BE' => 'Belgium',
            'BZ' => 'Belize',
            'BJ' => 'Benin',
            'BM' => 'Bermuda',
            'BT' => 'Bhutan',
            'BO' => 'Bolivia',
            'BA' => 'Bosnia And Herzegowina',
            'BW' => 'Botswana',
            'BV' => 'Bouvet Island',
            'BR' => 'Brazil',
            'IO' => 'British Indian Ocean Territory',
            'BN' => 'Brunei Darussalam',
            'BG' => 'Bulgaria',
            'BF' => 'Burkina Faso',
            'BI' => 'Burundi',
            'KH' => 'Cambodia',
            'CM' => 'Cameroon',
            'CA' => 'Canada',
            'CV' => 'Cape Verde',
            'KY' => 'Cayman Islands',
            'CF' => 'Central African Republic',
            'TD' => 'Chad',
            'CL' => 'Chile',
            'CN' => 'China',
            'CX' => 'Christmas Island',
            'CC' => 'Cocos (Keeling) Islands',
            'CO' => 'Colombia',
            'KM' => 'Comoros',
            'CG' => 'Congo',
            'CD' => 'Congo, The Democratic Republic Of The',
            'CK' => 'Cook Islands',
            'CR' => 'Costa Rica',
            'CI' => 'Cote D\'Ivoire',
            'HR' => 'Croatia (Local Name: Hrvatska)',
            'CU' => 'Cuba',
            'CY' => 'Cyprus',
            'CZ' => 'Czech Republic',
            'DK' => 'Denmark',
            'DJ' => 'Djibouti',
            'DM' => 'Dominica',
            'DO' => 'Dominican Republic',
            'TP' => 'East Timor',
            'EC' => 'Ecuador',
            'EG' => 'Egypt',
            'SV' => 'El Salvador',
            'GQ' => 'Equatorial Guinea',
            'ER' => 'Eritrea',
            'EE' => 'Estonia',
            'ET' => 'Ethiopia',
            'FK' => 'Falkland Islands (Malvinas)',
            'FO' => 'Faroe Islands',
            'FJ' => 'Fiji',
            'FI' => 'Finland',
            'FR' => 'France',
            'FX' => 'France, Metropolitan',
            'GF' => 'French Guiana',
            'PF' => 'French Polynesia',
            'TF' => 'French Southern Territories',
            'GA' => 'Gabon',
            'GM' => 'Gambia',
            'GE' => 'Georgia',
            'DE' => 'Germany',
            'GH' => 'Ghana',
            'GI' => 'Gibraltar',
            'GR' => 'Greece',
            'GL' => 'Greenland',
            'GD' => 'Grenada',
            'GP' => 'Guadeloupe',
            'GU' => 'Guam',
            'GT' => 'Guatemala',
            'GN' => 'Guinea',
            'GW' => 'Guinea-Bissau',
            'GY' => 'Guyana',
            'HT' => 'Haiti',
            'HM' => 'Heard And Mc Donald Islands',
            'HN' => 'Honduras',
            'HK' => 'Hong Kong',
            'HU' => 'Hungary',
            'IS' => 'Iceland',
            'IN' => 'India',
            'ID' => 'Indonesia',
            'IR' => 'Iran (Islamic Republic Of)',
            'IQ' => 'Iraq',
            'IE' => 'Ireland',
            'IL' => 'Israel',
            'IT' => 'Italy',
            'JM' => 'Jamaica',
            'JP' => 'Japan',
            'JO' => 'Jordan',
            'KZ' => 'Kazakhstan',
            'KE' => 'Kenya',
            'KI' => 'Kiribati',
            'KP' => 'Korea, Democratic People\'S Republic Of',
            'KR' => 'Korea, Republic Of',
            'KW' => 'Kuwait',
            'KG' => 'Kyrgyzstan',
            'LA' => 'Lao People\'S Democratic Republic',
            'LV' => 'Latvia',
            'LB' => 'Lebanon',
            'LS' => 'Lesotho',
            'LR' => 'Liberia',
            'LY' => 'Libyan Arab Jamahiriya',
            'LI' => 'Liechtenstein',
            'LT' => 'Lithuania',
            'LU' => 'Luxembourg',
            'MO' => 'Macau',
            'MK' => 'Macedonia, Former Yugoslav Republic Of',
            'MG' => 'Madagascar',
            'MW' => 'Malawi',
            'MY' => 'Malaysia',
            'MV' => 'Maldives',
            'ML' => 'Mali',
            'MT' => 'Malta',
            'MH' => 'Marshall Islands, Republic of the',
            'MQ' => 'Martinique',
            'MR' => 'Mauritania',
            'MU' => 'Mauritius',
            'YT' => 'Mayotte',
            'MX' => 'Mexico',
            'FM' => 'Micronesia, Federated States Of',
            'MD' => 'Moldova, Republic Of',
            'MC' => 'Monaco',
            'MN' => 'Mongolia',
            'MS' => 'Montserrat',
            'MA' => 'Morocco',
            'MZ' => 'Mozambique',
            'MM' => 'Myanmar',
            'NA' => 'Namibia',
            'NR' => 'Nauru',
            'NP' => 'Nepal',
            'NL' => 'Netherlands',
            'AN' => 'Netherlands Antilles',
            'NC' => 'New Caledonia',
            'NZ' => 'New Zealand',
            'NI' => 'Nicaragua',
            'NE' => 'Niger',
            'NG' => 'Nigeria',
            'NU' => 'Niue',
            'NF' => 'Norfolk Island',
            'MP' => 'Northern Mariana Islands, Commonwealth of the',
            'NO' => 'Norway',
            'OM' => 'Oman',
            'PK' => 'Pakistan',
            'PW' => 'Palau, Republic of',
            'PA' => 'Panama',
            'PG' => 'Papua New Guinea',
            'PY' => 'Paraguay',
            'PE' => 'Peru',
            'PH' => 'Philippines',
            'PN' => 'Pitcairn',
            'PL' => 'Poland',
            'PT' => 'Portugal',
            'PR' => 'Puerto Rico',
            'QA' => 'Qatar',
            'RE' => 'Reunion',
            'RO' => 'Romania',
            'RU' => 'Russian Federation',
            'RW' => 'Rwanda',
            'KN' => 'Saint Kitts And Nevis',
            'LC' => 'Saint Lucia',
            'VC' => 'Saint Vincent And The Grenadines',
            'WS' => 'Samoa',
            'SM' => 'San Marino',
            'ST' => 'Sao Tome And Principe',
            'SA' => 'Saudi Arabia',
            'SN' => 'Senegal',
            'SC' => 'Seychelles',
            'SL' => 'Sierra Leone',
            'SG' => 'Singapore',
            'SK' => 'Slovakia (Slovak Republic)',
            'SI' => 'Slovenia',
            'SB' => 'Solomon Islands',
            'SO' => 'Somalia',
            'ZA' => 'South Africa',
            'GS' => 'South Georgia, South Sandwich Islands',
            'ES' => 'Spain',
            'LK' => 'Sri Lanka',
            'SH' => 'St. Helena',
            'PM' => 'St. Pierre And Miquelon',
            'SD' => 'Sudan',
            'SR' => 'Suriname',
            'SJ' => 'Svalbard And Jan Mayen Islands',
            'SZ' => 'Swaziland',
            'SE' => 'Sweden',
            'CH' => 'Switzerland',
            'SY' => 'Syrian Arab Republic',
            'TW' => 'Taiwan',
            'TJ' => 'Tajikistan',
            'TZ' => 'Tanzania, United Republic Of',
            'TH' => 'Thailand',
            'TG' => 'Togo',
            'TK' => 'Tokelau',
            'TO' => 'Tonga',
            'TT' => 'Trinidad And Tobago',
            'TN' => 'Tunisia',
            'TR' => 'Turkey',
            'TM' => 'Turkmenistan',
            'TC' => 'Turks And Caicos Islands',
            'TV' => 'Tuvalu',
            'UG' => 'Uganda',
            'UA' => 'Ukraine',
            'AE' => 'United Arab Emirates',
            'GB' => 'United Kingdom',
            'UM' => 'United States Minor Outlying Islands',
            'UY' => 'Uruguay',
            'UZ' => 'Uzbekistan',
            'VU' => 'Vanuatu',
            'VA' => 'Vatican City, State of the',
            'VE' => 'Venezuela',
            'VN' => 'Viet Nam',
            'VG' => 'Virgin Islands (British)',
            'VI' => 'Virgin Islands (U.S.)',
            'WF' => 'Wallis And Futuna Islands',
            'EH' => 'Western Sahara',
            'YE' => 'Yemen',
            'YU' => 'Yugoslavia',
            'ZM' => 'Zambia',
            'ZW' => 'Zimbabwe');

    }
}

/**
 * get state list on country code
 * @param  $country_code
 * @return \Illuminate\Http\Response
 */
if (!function_exists('get_state_list')) {
    function get_state_list($country_code = null)
    {

        $data = array(
//            'US' => array('AL' => 'Alabama',
//                'AK' => 'Alaska',
//                'AS' => 'American Samoa',
//                'AZ' => 'Arizona',
//                'AR' => 'Arkansas',
//                'AE' => 'Armed Forces - Europe',
//                'AP' => 'Armed Forces - Pacific',
//                'AA' => 'Armed Forces - USA/Canada',
//                'CA' => 'California',
//                'CO' => 'Colorado',
//                'CT' => 'Connecticut',
//                'DE' => 'Delaware',
//                'DC' => 'District of Columbia',
//                'FL' => 'Florida',
//                'GA' => 'Georgia',
//                'GU' => 'Guam',
//                'HI' => 'Hawaii',
//                'ID' => 'Idaho',
//                'IL' => 'Illinois',
//                'IN' => 'Indiana',
//                'IA' => 'Iowa',
//                'KS' => 'Kansas',
//                'KY' => 'Kentucky',
//                'LA' => 'Louisiana',
//                'ME' => 'Maine',
//                'MD' => 'Maryland',
//                'MA' => 'Massachusetts',
//                'MI' => 'Michigan',
//                'MN' => 'Minnesota',
//                'MS' => 'Mississippi',
//                'MO' => 'Missouri',
//                'MT' => 'Montana',
//                'NE' => 'Nebraska',
//                'NV' => 'Nevada',
//                'NH' => 'New Hampshire',
//                'NJ' => 'New Jersey',
//                'NM' => 'New Mexico',
//                'NY' => 'New York',
//                'NC' => 'North Carolina',
//                'ND' => 'North Dakota',
//                'OH' => 'Ohio',
//                'OK' => 'Oklahoma',
//                'OR' => 'Oregon',
//                'PA' => 'Pennsylvania',
//                'PR' => 'Puerto Rico',
//                'RI' => 'Rhode Island',
//                'SC' => 'South Carolina',
//                'SD' => 'South Dakota',
//                'TN' => 'Tennessee',
//                'TX' => 'Texas',
//                'UT' => 'Utah',
//                'VT' => 'Vermont',
//                'VI' => 'Virgin Islands',
//                'VA' => 'Virginia',
//                'WA' => 'Washington',
//                'WV' => 'West Virginia',
//                'WI' => 'Wisconsin',
//                'WY' => 'Wyoming'
//            ),
//            'CA' => array('AB' => 'Alberta',
//                'BC' => 'British Columbia',
//                'MB' => 'Manitoba',
//                'NB' => 'New Brunswick',
//                'NF' => 'Newfoundland and Labrador',
//                'NT' => 'Northwest Territories',
//                'NS' => 'Nova Scotia',
//                'NU' => 'Nunavut',
//                'ON' => 'Ontario',
//                'PE' => 'Prince Edward Island',
//                'QC' => 'Quebec',
//                'SK' => 'Saskatchewan',
//                'YT' => 'Yukon Territory'),
//            'IN' => array(
//                "AA" => "Alabama",
//            ),
            'AF' => Array
            (
                'BDS' => 'Badakhshān',
                'BGL' => 'Baghlān',
                'BAL' => 'Balkh',
                'BDG' => 'Bādghīs',
                'BAM' => 'Bāmyān',
                'DAY' => 'Dāykundī',
                'FRA' => 'Farāh',
                'FYB' => 'Fāryāb',
                'GHA' => 'Ghaznī',
                'GHO' => 'Ghōr',
                'HEL' => 'Helmand',
                'HER' => 'Herāt',
                'JOW' => 'Jowzjān',
                'KAN' => 'Kandahār',
                'KHO' => 'Khōst',
                'KNR' => 'Kunar',
                'KDZ' => 'Kunduz',
                'KAB' => 'Kābul',
                'KAP' => 'Kāpīsā',
                'LAG' => 'Laghmān',
                'LOG' => 'Lōgar',
                'NAN' => 'Nangarhār',
                'NIM' => 'Nīmrōz',
                'NUR' => 'Nūristān',
                'PIA' => 'Paktiyā',
                'PKA' => 'Paktīkā',
                'PAN' => 'Panjshayr',
                'PAR' => 'Parwān',
                'SAM' => 'Samangān',
                'SAR' => 'Sar-e Pul',
                'TAK' => 'Takhār',
                'URU' => 'Uruzgān',
                'WAR' => 'Wardak',
                'ZAB' => 'Zābul',
            ),
            'AL' => Array
            (
                '01' => 'Berat',
                '09' => 'Dibër',
                '02' => 'Durrës',
                '03' => 'Elbasan',
                '04' => 'Fier',
                '05' => 'Gjirokastër',
                '06' => 'Korçë',
                '07' => 'Kukës',
                '08' => 'Lezhë',
                '10' => 'Shkodër',
                '11' => 'Tiranë',
                '12' => 'Vlorë',
            ),
            'DZ' => Array
            (
                '01' => 'Adrar',
                '16' => 'Alger',
                '23' => 'Annaba',
                '44' => 'Aïn Defla',
                '46' => 'Aïn Témouchent',
                '05' => 'Batna',
                '07' => 'Biskra',
                '09' => 'Blida',
                '34' => 'Bordj Bou Arréridj',
                '10' => 'Bouira',
                '35' => 'Boumerdès',
                '08' => 'Béchar',
                '06' => 'Béjaïa',
                '02' => 'Chlef',
                '25' => 'Constantine',
                '17' => 'Djelfa',
                '32' => 'El Bayadh',
                '39' => 'El Oued',
                '36' => 'El Tarf',
                '47' => 'Ghardaïa',
                '24' => 'Guelma',
                '33' => 'Illizi',
                '18' => 'Jijel',
                '40' => 'Khenchela',
                '03' => 'Laghouat',
                '29' => 'Mascara',
                '43' => 'Mila',
                '27' => 'Mostaganem',
                '28' => 'Msila',
                '26' => 'Médéa',
                '45' => 'Naama',
                '31' => 'Oran',
                '30' => 'Ouargla',
                '04' => 'Oum el Bouaghi',
                '48' => 'Relizane',
                '20' => 'Saïda',
                '22' => 'Sidi Bel Abbès',
                '21' => 'Skikda',
                '41' => 'Souk Ahras',
                '19' => 'Sétif',
                '11' => 'Tamanghasset',
                '14' => 'Tiaret',
                '37' => 'Tindouf',
                '42' => 'Tipaza',
                '38' => 'Tissemsilt',
                '15' => 'Tizi Ouzou',
                '13' => 'Tlemcen',
                '12' => 'Tébessa',
            ),
            'AD' => Array
            (
                '07' => 'Andorra la Vella',
                '02' => 'Canillo',
                '03' => 'Encamp',
                '08' => 'Escaldes-Engordany',
                '04' => 'La Massana',
                '05' => 'Ordino',
                '06' => 'Sant Julià de Lòria',
            ),
            'AO' => Array
            (
                'BGO' => 'Bengo',
                'BGU' => 'Benguela',
                'BIE' => 'Bié',
                'CAB' => 'Cabinda',
                'CNN' => 'Cunene',
                'HUA' => 'Huambo',
                'HUI' => 'Huíla',
                'CCU' => 'Kuando Kubango',
                'CNO' => 'Kwanza Norte',
                'CUS' => 'Kwanza Sul',
                'LUA' => 'Luanda',
                'LNO' => 'Lunda Norte',
                'LSU' => 'Lunda Sul',
                'MAL' => 'Malange',
                'MOX' => 'Moxico',
                'NAM' => 'Namibe',
                'UIG' => 'Uíge',
                'ZAI' => 'Zaire',
            ),
            'AG' => Array
            (
                '10' => 'Barbuda',
                '11' => 'Redonda',
                '03' => 'Saint George',
                '04' => 'Saint John',
                '05' => 'Saint Mary',
                '06' => 'Saint Paul',
                '07' => 'Saint Peter',
                '08' => 'Saint Philip',
            ),
            'AR' => Array
            (
                'B' => 'Buenos Aires',
                'K' => 'Catamarca',
                'H' => 'Chaco',
                'U' => 'Chubut',
                'C' => 'Ciudad Autónoma de Buenos Aires',
                'W' => 'Corrientes',
                'X' => 'Córdoba',
                'E' => 'Entre Ríos',
                'P' => 'Formosa',
                'Y' => 'Jujuy',
                'L' => 'La Pampa',
                'F' => 'La Rioja',
                'M' => 'Mendoza',
                'N' => 'Misiones',
                'Q' => 'Neuquén',
                'R' => 'Río Negro',
                'A' => 'Salta',
                'J' => 'San Juan',
                'D' => 'San Luis',
                'Z' => 'Santa Cruz',
                'S' => 'Santa Fe',
                'G' => 'Santiago del Estero',
                'V' => 'Tierra del Fuego',
                'T' => 'Tucumán',
            ),
            'AM' => Array
            (
                'AG' => 'Aragac̣otn',
                'AR' => 'Ararat',
                'AV' => 'Armavir',
                'ER' => 'Erevan',
                'GR' => 'Geġarkunik',
                'KT' => 'Kotayk',
                'LO' => 'Loṙi',
                'SU' => 'Syunik',
                'TV' => 'Tavuš',
                'VD' => 'Vayoć Jor',
                'SH' => 'Širak',
            ),
            'AU' => Array
            (
                'ACT' => 'Australian Capital Territory',
                'NSW' => 'New South Wales',
                'NT' => 'Northern Territory',
                'QLD' => 'Queensland',
                'SA' => 'South Australia',
                'TAS' => 'Tasmania',
                'VIC' => 'Victoria',
                'WA' => 'Western Australia',
            ),
            'AT' => Array
            (
                'B' => 'Burgenland',
                'K' => 'Kärnten',
                'NÖ' => 'Niederösterreich',
                'OÖ' => 'Oberösterreich',
                'S' => 'Salzburg',
                'ST' => 'Steiermark',
                'T' => 'Tirol',
                'V' => 'Vorarlberg',
                'W' => 'Wien',
            ),
            'AZ' => Array
            (
                'NX' => 'Naxçıvan',
            ),
            'BS' => Array
            (
                'AK' => 'Acklins',
                'BY' => 'Berry Islands',
                'BI' => 'Bimini',
                'BP' => 'Black Point',
                'CI' => 'Cat Island',
                'CO' => 'Central Abaco',
                'CS' => 'Central Andros',
                'CE' => 'Central Eleuthera',
                'FP' => 'City of Freeport',
                'CK' => 'Crooked Island and Long Cay',
                'EG' => 'East Grand Bahama',
                'EX' => 'Exuma',
                'GC' => 'Grand Cay',
                'HI' => 'Harbour Island',
                'HT' => 'Hope Town',
                'IN' => 'Inagua',
                'LI' => 'Long Island',
                'MC' => 'Mangrove Cay',
                'MG' => 'Mayaguana',
                'MI' => 'Moores Island',
                'NO' => 'North Abaco',
                'NS' => 'North Andros',
                'NE' => 'North Eleuthera',
                'RI' => 'Ragged Island',
                'RC' => 'Rum Cay',
                'SS' => 'San Salvador',
                'SO' => 'South Abaco',
                'SA' => 'South Andros',
                'SE' => 'South Eleuthera',
                'SW' => 'Spanish Wells',
                'WG' => 'West Grand Bahama',
            ),
            'BH' => Array
            (
                '14' => 'Al Janūbīyah',
                '13' => 'Al Manāmah',
                '15' => 'Al Muḩarraq',
                '16' => 'Al Wusţá',
                '17' => 'Ash Shamālīyah',
            ),
            'BD' => Array
            (
                'A' => 'Barisal',
                'B' => 'Chittagong',
                'C' => 'Dhaka',
                'D' => 'Khulna',
                'E' => 'Rajshahi',
                'F' => 'Rangpur',
                'G' => 'Sylhet',
            ),
            'BB' => Array
            (
                '01' => 'Christ Church',
                '02' => 'Saint Andrew',
                '03' => 'Saint George',
                '04' => 'Saint James',
                '05' => 'Saint John',
                '06' => 'Saint Joseph',
                '07' => 'Saint Lucy',
                '08' => 'Saint Michael',
                '09' => 'Saint Peter',
                '10' => 'Saint Philip',
                '11' => 'Saint Thomas',
            ),
            'BY' => Array
            (
                'BR' => 'Brestskaya voblasts',
                'HO' => 'Homyel skaya voblasts',
                'HM' => 'Horad Minsk',
                'HR' => 'Hrodzenskaya voblasts',
                'MA' => 'Mahilyowskaya voblasts',
                'MI' => 'Minskaya voblasts',
                'VI' => 'Vitsyebskaya voblasts',
            ),
            'BE' => Array
            (
                'BRU' => 'Brussels Hoofdstedelijk Gewest',
                'WAL' => 'Région Wallonne',
                'VLG' => 'Vlaams Gewest',
            ),
            'BZ' => Array
            (
                'BZ' => 'Belize',
                'CY' => 'Cayo',
                'CZL' => 'Corozal',
                'OW' => 'Orange Walk',
                'SC' => 'Stann Creek',
                'TOL' => 'Toledo',
            ),
            'BJ' => Array
            (
                'AL' => 'Alibori',
                'AK' => 'Atakora',
                'AQ' => 'Atlantique',
                'BO' => 'Borgou',
                'CO' => 'Collines',
                'DO' => 'Donga',
                'KO' => 'Kouffo',
                'LI' => 'Littoral',
                'MO' => 'Mono',
                'OU' => 'Ouémé',
                'PL' => 'Plateau',
                'ZO' => 'Zou',
            ),
            'BT' => Array
            (
                '33' => 'Bumthang',
                '12' => 'Chhukha',
                '22' => 'Dagana',
                'GA' => 'Gasa',
                '13' => 'Ha',
                '44' => 'Lhuentse',
                '42' => 'Monggar',
                '11' => 'Paro',
                '43' => 'Pemagatshel',
                '23' => 'Punakha',
                '45' => 'Samdrup Jongkha',
                '14' => 'Samtse',
                '31' => 'Sarpang',
                '15' => 'Thimphu',
                'TY' => 'Trashi Yangtse',
                '41' => 'Trashigang',
                '32' => 'Trongsa',
                '21' => 'Tsirang',
                '24' => 'Wangdue Phodrang',
                '34' => 'Zhemgang',
            ),
            'BO' => Array
            (
                'H' => 'Chuquisaca',
                'C' => 'Cochabamba',
                'B' => 'El Beni',
                'L' => 'La Paz',
                'O' => 'Oruro',
                'N' => 'Pando',
                'P' => 'Potosí',
                'S' => 'Santa Cruz',
                'T' => 'Tarija',
            ),
            'BA' => Array
            (
                'BRC' => 'Brčko distrikt',
                'BIH' => 'Federacija Bosna i Hercegovina',
                'SRP' => 'Republika Srpska',
            ),
            'BW' => Array
            (
                'CE' => 'Central',
                'CH' => 'Chobe',
                'FR' => 'Francistown',
                'GA' => 'Gaborone',
                'GH' => 'Ghanzi',
                'JW' => 'Jwaneng',
                'KG' => 'Kgalagadi',
                'KL' => 'Kgatleng',
                'KW' => 'Kweneng',
                'LO' => 'Lobatse',
                'NE' => 'North-East',
                'NW' => 'North-West',
                'SP' => 'Selibe Phikwe',
                'SE' => 'South-East',
                'SO' => 'Southern',
                'ST' => 'Sowa Town',
            ),
            'BR' => Array
            (
                'AC' => 'Acre',
                'AL' => 'Alagoas',
                'AP' => 'Amapá',
                'AM' => 'Amazonas',
                'BA' => 'Bahia',
                'CE' => 'Ceará',
                'DF' => 'Distrito Federal',
                'ES' => 'Espírito Santo',
                'GO' => 'Goiás',
                'MA' => 'Maranhão',
                'MT' => 'Mato Grosso',
                'MS' => 'Mato Grosso do Sul',
                'MG' => 'Minas Gerais',
                'PR' => 'Paraná',
                'PB' => 'Paraíba',
                'PA' => 'Pará',
                'PE' => 'Pernambuco',
                'PI' => 'Piauí',
                'RN' => 'Rio Grande do Norte',
                'RS' => 'Rio Grande do Sul',
                'RJ' => 'Rio de Janeiro',
                'RO' => 'Rondônia',
                'RR' => 'Roraima',
                'SC' => 'Santa Catarina',
                'SE' => 'Sergipe',
                'SP' => 'São Paulo',
                'TO' => 'Tocantins',
            ),
            'UM' => Array
            (
                '81' => 'Baker Island',
                '84' => 'Howland Island',
                '86' => 'Jarvis Island',
                '67' => 'Johnston Atoll',
                '89' => 'Kingman Reef',
                '71' => 'Midway Islands',
                '76' => 'Navassa Island',
                '95' => 'Palmyra Atoll',
                '79' => 'Wake Island',
            ),
            'BN' => Array
            (
                'BE' => 'Belait',
                'BM' => 'Brunei-Muara',
                'TE' => 'Temburong',
                'TU' => 'Tutong',
            ),
            'BG' => Array
            (
                '01' => 'Blagoevgrad',
                '02' => 'Burgas',
                '08' => 'Dobrich',
                '07' => 'Gabrovo',
                '26' => 'Haskovo',
                '09' => 'Kardzhali',
                '10' => 'Kyustendil',
                '11' => 'Lovech',
                '12' => 'Montana',
                '13' => 'Pazardzhik',
                '14' => 'Pernik',
                '15' => 'Pleven',
                '16' => 'Plovdiv',
                '17' => 'Razgrad',
                '18' => 'Ruse',
                '27' => 'Shumen',
                '19' => 'Silistra',
                '20' => 'Sliven',
                '21' => 'Smolyan',
                '23' => 'Sofia',
                '22' => 'Sofia-Grad',
                '24' => 'Stara Zagora',
                '25' => 'Targovishte',
                '03' => 'Varna',
                '04' => 'Veliko Tarnovo',
                '05' => 'Vidin',
                '06' => 'Vratsa',
                '28' => 'Yambol',
            ),
            'BF' => Array
            (
                '01' => 'Boucle du Mouhoun',
                '02' => 'Cascades',
                '03' => 'Centre',
                '04' => 'Centre-Est',
                '05' => 'Centre-Nord',
                '06' => 'Centre-Ouest',
                '07' => 'Centre-Sud',
                '08' => 'Est',
                '09' => 'Hauts-Bassins',
                '10' => 'Nord',
                '11' => 'Plateau-Central',
                '12' => 'Sahel',
                '13' => 'Sud-Ouest',
            ),
            'BI' => Array
            (
                'BB' => 'Bubanza',
                'BM' => 'Bujumbura Mairie',
                'BL' => 'Bujumbura Rural',
                'BR' => 'Bururi',
                'CA' => 'Cankuzo',
                'CI' => 'Cibitoke',
                'GI' => 'Gitega',
                'KR' => 'Karuzi',
                'KY' => 'Kayanza',
                'KI' => 'Kirundo',
                'MA' => 'Makamba',
                'MU' => 'Muramvya',
                'MY' => 'Muyinga',
                'MW' => 'Mwaro',
                'NG' => 'Ngozi',
                'RT' => 'Rutana',
                'RY' => 'Ruyigi',
            ),
            'KH' => Array
            (
                '2' => 'Baat Dambang',
                '1' => 'Banteay Mean Chey',
                '3' => 'Kampong Chaam',
                '4' => 'Kampong Chhnang',
                '5' => 'Kampong Spueu',
                '6' => 'Kampong Thum',
                '7' => 'Kampot',
                '8' => 'Kandaal',
                '9' => 'Kaoh Kong',
                '10' => 'Kracheh',
                '23' => 'Krong Kaeb',
                '24' => 'Krong Pailin',
                '18' => 'Krong Preah Sihanouk',
                '11' => 'Mondol Kiri',
                '22' => 'Otdar Mean Chey',
                '12' => 'Phnom Penh',
                '15' => 'Pousaat',
                '13' => 'Preah Vihear',
                '14' => 'Prey Veaeng',
                '16' => 'Rotanak Kiri',
                '17' => 'Siem Reab',
                '19' => 'Stueng Traeng',
                '20' => 'Svaay Rieng',
                '21' => 'Taakaev',
            ),
            'CM' => Array
            (
                'AD' => 'Adamaoua',
                'CE' => 'Centre',
                'ES' => 'East',
                'EN' => 'Far North',
                'LT' => 'Littoral',
                'NO' => 'North',
                'NW' => 'North-West',
                'SU' => 'South',
                'SW' => 'South-West',
                'OU' => 'West',
            ),
            'CA' => Array
            (
                'AB' => 'Alberta',
                'BC' => 'British Columbia',
                'MB' => 'Manitoba',
                'NB' => 'New Brunswick',
                'NL' => 'Newfoundland and Labrador',
                'NS' => 'Nova Scotia',
                'ON' => 'Ontario',
                'PE' => 'Prince Edward Island',
                'QC' => 'Quebec',
                'SK' => 'Saskatchewan',
                'NT' => 'Northwest Territories',
                'NU' => 'Nunavut',
                'YT' => 'Yukon',
            ),
            'CV' => Array
            (
                'B' => 'Ilhas de Barlavento',
                'S' => 'Ilhas de Sotavento',
            ),
            'CF' => Array
            (
                'BB' => 'Bamingui-Bangoran',
                'BGF' => 'Bangui',
                'BK' => 'Basse-Kotto',
                'KB' => 'Gribingui',
                'HM' => 'Haut-Mbomou',
                'HK' => 'Haute-Kotto',
                'HS' => 'Haute-Sangha / Mambéré-Kadéï',
                'KG' => 'Kémo-Gribingui',
                'LB' => 'Lobaye',
                'MB' => 'Mbomou',
                'NM' => 'Nana-Mambéré',
                'MP' => 'Ombella-Mpoko',
                'UK' => 'Ouaka',
                'AC' => 'Ouham',
                'OP' => 'Ouham-Pendé',
                'SE' => 'Sangha',
                'VK' => 'Vakaga',
            ),
            'TD' => Array
            (
                'BA' => 'Al Baṭḩah',
                'LC' => 'Al Buḩayrah',
                'BG' => 'Baḩr al Ghazāl',
                'BO' => 'Būrkū',
                'EN' => 'Innīdī',
                'KA' => 'Kānim',
                'LO' => 'Lūqūn al Gharbī',
                'LR' => 'Lūqūn ash Sharqī',
                'ND' => 'Madīnat Injamīnā',
                'MA' => 'Māndūl',
                'MO' => 'Māyū Kībbī al Gharbī',
                'ME' => 'Māyū Kībbī ash Sharqī',
                'GR' => 'Qīrā',
                'SA' => 'Salāmāt',
                'CB' => 'Shārī Bāqirmī',
                'MC' => 'Shārī al Awsaṭ',
                'SI' => 'Sīlā',
                'TI' => 'Tibastī',
                'TA' => 'Tānjilī',
                'OD' => 'Waddāy',
                'WF' => 'Wādī Fīrā',
                'HL' => 'Ḥajjar Lamīs',
            ),
            'CL' => Array
            (
                'AI' => 'Aisén del General Carlos Ibañez del Campo',
                'AN' => 'Antofagasta',
                'AR' => 'Araucanía',
                'AP' => 'Arica y Parinacota',
                'AT' => 'Atacama',
                'BI' => 'Bío-Bío',
                'CO' => 'Coquimbo',
                'LI' => 'Libertador General Bernardo O"Higgins',
                'LL' => 'Los Lagos',
                'LR' => 'Los Ríos',
                'MA' => 'Magallanes',
                'ML' => 'Maule',
                'RM' => 'Región Metropolitana de Santiago',
                'TA' => 'Tarapacá',
                'VS' => 'Valparaíso',
            ),
            'CN' => Array
            (
                '45' => 'Guangxi',
                '15' => 'Nei Mongol',
                '64' => 'Ningxia',
                '65' => 'Xinjiang',
                '54' => 'Xizang',
                '11' => 'Beijing',
                '50' => 'Chongqing',
                '31' => 'Shanghai',
                '12' => 'Tianjin',
                '34' => 'Anhui',
                '35' => 'Fujian',
                '62' => 'Gansu',
                '44' => 'Guangdong',
                '52' => 'Guizhou',
                '46' => 'Hainan',
                '13' => 'Hebei',
                '23' => 'Heilongjiang',
                '41' => 'Henan',
                '42' => 'Hubei',
                '43' => 'Hunan',
                '32' => 'Jiangsu',
                '36' => 'Jiangxi',
                '22' => 'Jilin',
                '21' => 'Liaoning',
                '63' => 'Qinghai',
                '61' => 'Shaanxi',
                '37' => 'Shandong',
                '14' => 'Shanxi',
                '51' => 'Sichuan',
                '71' => 'Taiwan',
                '53' => 'Yunnan',
                '33' => 'Zhejiang',
                '91' => 'Hong Kong',
                '92' => 'Macao',
                '' => 'Hong Kong Island',
                'KOWLOON' => 'Kowloon',
            ),
            'CO' => Array
            (
                'AMA' => 'Amazonas',
                'ANT' => 'Antioquia',
                'ARA' => 'Arauca',
                'ATL' => 'Atlántico',
                'BOL' => 'Bolívar',
                'BOY' => 'Boyacá',
                'CAL' => 'Caldas',
                'CAQ' => 'Caquetá',
                'CAS' => 'Casanare',
                'CAU' => 'Cauca',
                'CES' => 'Cesar',
                'CHO' => 'Chocó',
                'CUN' => 'Cundinamarca',
                'COR' => 'Córdoba',
                'DC' => 'Distrito Capital de Bogotá',
                'GUA' => 'Guainía',
                'GUV' => 'Guaviare',
                'HUI' => 'Huila',
                'LAG' => 'La Guajira',
                'MAG' => 'Magdalena',
                'MET' => 'Meta',
                'NAR' => 'Nariño',
                'NSA' => 'Norte de Santander',
                'PUT' => 'Putumayo',
                'QUI' => 'Quindío',
                'RIS' => 'Risaralda',
                'SAP' => 'San Andrés, Providencia y Santa Catalina',
                'SAN' => 'Santander',
                'SUC' => 'Sucre',
                'TOL' => 'Tolima',
                'VAC' => 'Valle del Cauca',
                'VAU' => 'Vaupés',
                'VID' => 'Vichada',
            ),
            'KM' => Array
            (
                'A' => 'Anjouan',
                'G' => 'Grande Comore',
                'M' => 'Mohéli',
            ),
            'CG' => Array
            (
                '11' => 'Bouenza',
                'BZV' => 'Brazzaville',
                '8' => 'Cuvette',
                '15' => 'Cuvette-Ouest',
                '5' => 'Kouilou',
                '7' => 'Likouala',
                '2' => 'Lékoumou',
                '9' => 'Niari',
                '14' => 'Plateaux',
                '16' => 'Pointe-Noire',
                '12' => 'Pool',
                '13' => 'Sangha',
            ),
            'CD' => Array
            (
                'BN' => 'Bandundu',
                'BC' => 'Bas-Congo',
                'KW' => 'Kasai-Occidental',
                'KE' => 'Kasai-Oriental',
                'KA' => 'Katanga',
                'KN' => 'Kinshasa',
                'MA' => 'Maniema',
                'NK' => 'Nord-Kivu',
                'OR' => 'Orientale',
                'SK' => 'Sud-Kivu',
                'EQ' => 'Équateur',
            ),
            'CR' => Array
            (
                'A' => 'Alajuela',
                'C' => 'Cartago',
                'G' => 'Guanacaste',
                'H' => 'Heredia',
                'L' => 'Limón',
                'P' => 'Puntarenas',
                'SJ' => 'San José',
            ),
            'HR' => Array
            (
                '07' => 'Bjelovarsko-bilogorska županija',
                '12' => 'Brodsko - posavska županija',
                '19' => 'Dubrovačko-neretvanska županija',
                '21' => 'Grad Zagreb',
                '18' => 'Istarska županija',
                '04' => 'Karlovačka županija',
                '06' => 'Koprivničko-križevačka županija',
                '02' => 'Krapinsko - zagorska županija',
                '09' => 'Ličko-senjska županija',
                '20' => 'Međimurska županija',
                '14' => 'Osječko-baranjska županija',
                '11' => 'Požeško - slavonska županija',
                '08' => 'Primorsko-goranska županija',
                '03' => 'Sisačko - moslavačka županija',
                '17' => 'Splitsko-dalmatinska županija',
                '05' => 'Varaždinska županija',
                '10' => 'Virovitičko-podravska županija',
                '16' => 'Vukovarsko - srijemska županija',
                '13' => 'Zadarska županija',
                '01' => 'Zagrebačka županija',
                '15' => 'Šibensko-kninska županija',
            ),
            'CU' => Array
            (
                '15' => 'Artemisa',
                '09' => 'Camagüey',
                '08' => 'Ciego de Ávila',
                '06' => 'Cienfuegos',
                '12' => 'Granma',
                '14' => 'Guantánamo',
                '11' => 'Holguín',
                '99' => 'Isla de la Juventud',
                '03' => 'La Habana',
                '10' => 'Las Tunas',
                '04' => 'Matanzas',
                '16' => 'Mayabeque',
                '01' => 'Pinar del Río',
                '07' => 'Sancti Spíritus',
                '13' => 'Santiago de Cuba',
                '05' => 'Villa Clara',
            ),
            'CY' => Array
            (
                '04' => 'Ammochostos',
                '06' => 'Keryneia',
                '03' => 'Larnaka',
                '01' => 'Lefkosia',
                '02' => 'Lemesos',
                '05' => 'Pafos',
            ),
            'CZ' => Array
            (
                'JM' => 'Jihomoravský kraj',
                'JC' => 'Jihočeský kraj',
                'KA' => 'Karlovarský kraj',
                'KR' => 'Královéhradecký kraj',
                'LI' => 'Liberecký kraj',
                'MO' => 'Moravskoslezský kraj',
                'OL' => 'Olomoucký kraj',
                'PA' => 'Pardubický kraj',
                'PL' => 'Plzeňský kraj',
                'PR' => 'Praha, hlavní město',
                'ST' => 'Středočeský kraj',
                'VY' => 'Vysočina',
                'ZL' => 'Zlínský kraj',
                'US' => 'Ústecký kraj',
            ),
            'DK' => Array
            (
                '84' => 'Hovedstaden',
                '82' => 'Midtjylland',
                '81' => 'Nordjylland',
                '85' => 'Sjælland',
                '83' => 'Syddanmark',
            ),
            'DJ' => Array
            (
                'AS' => 'Ali Sabieh',
                'AR' => 'Arta',
                'DI' => 'Dikhil',
                'DJ' => 'Djibouti',
                'OB' => 'Obock',
                'TA' => 'Tadjourah',
            ),
            'DM' => Array
            (
                '02' => 'Saint Andrew',
                '03' => 'Saint David',
                '04' => 'Saint George',
                '05' => 'Saint John',
                '06' => 'Saint Joseph',
                '07' => 'Saint Luke',
                '08' => 'Saint Mark',
                '09' => 'Saint Patrick',
                '10' => 'Saint Paul',
                '11' => 'Saint Peter',
            ),
            'DO' => Array
            (
                '33' => 'Cibao Nordeste',
                '34' => 'Cibao Noroeste',
                '35' => 'Cibao Norte',
                '36' => 'Cibao Sur',
                '37' => 'El Valle',
                '38' => 'Enriquillo',
                '39' => 'Higuamo',
                '40' => 'Ozama',
                '41' => 'Valdesia',
                '42' => 'Yuma',
            ),
            'EC' => Array
            (
                'A' => 'Azuay',
                'B' => 'Bolívar',
                'C' => 'Carchi',
                'F' => 'Cañar',
                'H' => 'Chimborazo',
                'X' => 'Cotopaxi',
                'O' => 'El Oro',
                'E' => 'Esmeraldas',
                'W' => 'Galápagos',
                'G' => 'Guayas',
                'I' => 'Imbabura',
                'L' => 'Loja',
                'R' => 'Los Ríos',
                'M' => 'Manabí',
                'S' => 'Morona-Santiago',
                'N' => 'Napo',
                'D' => 'Orellana',
                'Y' => 'Pastaza',
                'P' => 'Pichincha',
                'SE' => 'Santa Elena',
                'SD' => 'Santo Domingo de los Tsáchilas',
                'U' => 'Sucumbíos',
                'T' => 'Tungurahua',
                'Z' => 'Zamora - Chinchipe',
            ),
            'EG' => Array
            (
                'DK' => 'Ad Daqahlīyah',
                'BA' => 'Al Baḩr al Aḩmar',
                'BH' => 'Al Buḩayrah',
                'FYM' => 'Al Fayyūm',
                'GH' => 'Al Gharbīyah',
                'ALX' => 'Al Iskandarīyah',
                'IS' => 'Al Ismāٰīlīyah',
                'GZ' => 'Al Jīzah',
                'MN' => 'Al Minyā',
                'MNF' => 'Al Minūfīyah',
                'KB' => 'Al Qalyūbīyah',
                'C' => 'Al Qāhirah',
                'LX' => 'Al Uqşur',
                'WAD' => 'Al Wādī al Jadīd',
                'SUZ' => 'As Suways',
                'SU' => 'As Sādis min Uktūbar',
                'SHR' => 'Ash Sharqīyah',
                'ASN' => 'Aswān',
                'AST' => 'Asyūţ',
                'BNS' => 'Banī Suwayf',
                'PTS' => 'Būr Saٰīd',
                'DT' => 'Dumyāţ',
                'JS' => 'Janūb Sīnā',
                'KFS' => 'Kafr ash Shaykh',
                'MT' => 'Maţrūḩ',
                'KN' => 'Qinā',
                'SIN' => 'Shamāl Sīnā',
                'SHG' => 'Sūhāj',
                'HU' => 'Ḩulwān',
            ),
            'SV' => Array
            (
                'AH' => 'Ahuachapán',
                'CA' => 'Cabañas',
                'CH' => 'Chalatenango',
                'CU' => 'Cuscatlán',
                'LI' => 'La Libertad',
                'PA' => 'La Paz',
                'UN' => 'La Unión',
                'MO' => 'Morazán',
                'SM' => 'San Miguel',
                'SS' => 'San Salvador',
                'SV' => 'San Vicente',
                'SA' => 'Santa Ana',
                'SO' => 'Sonsonate',
                'US' => 'Usulután',
            ),
            'GQ' => Array
            (
                'C' => 'Región Continental',
                'I' => 'Región Insular',
            ),
            'ER' => Array
            (
                'MA' => 'Al Awsaţ',
                'DU' => 'Al Janūbĩ',
                'AN' => 'Ansabā',
                'DK' => 'Janūbī al Baḩrī al Aḩmar',
                'GB' => 'Qāsh - Barkah',
                'SK' => 'Shimālī al Baḩrī al Aḩmar',
            ),
            'EE' => Array
            (
                '37' => 'Harjumaa',
                '39' => 'Hiiumaa',
                '44' => 'Ida - Virumaa',
                '51' => 'Järvamaa',
                '49' => 'Jõgevamaa',
                '59' => 'Lääne - Virumaa',
                '57' => 'Läänemaa',
                '67' => 'Pärnumaa',
                '65' => 'Põlvamaa',
                '70' => 'Raplamaa',
                '74' => 'Saaremaa',
                '78' => 'Tartumaa',
                '82' => 'Valgamaa',
                '84' => 'Viljandimaa',
                '86' => 'Võrumaa',
            ),
            'ET' => Array
            (
                'BE' => 'Bīnshangul Gumuz',
                'DD' => 'Dirē Dawa',
                'GA' => 'Gambēla Hizboch',
                'HA' => 'Hārerī Hizb',
                ' OR ' => 'Oromīya',
                'SO' => 'Sumalē',
                'TI' => 'Tigray',
                'SN' => 'YeDebub Bihēroch Bihēreseboch na Hizboch',
                'AA' => 'Ādīs Ābeba',
                'AF' => 'Āfar',
                'AM' => 'Āmara',
            ),
            'FJ' => Array
            (
                'C' => 'Central',
                'E' => 'Eastern',
                'N' => 'Northern',
                'R' => 'Rotuma',
                'W' => 'Western',
            ),
            'FI' => Array
            (
                '01' => 'Ahvenanmaan maakunta',
                '02' => 'Etelä - Karjala',
                '03' => 'Etelä - Pohjanmaa',
                '04' => 'Etelä - Savo',
                '05' => 'Kainuu',
                '06' => 'Kanta - Häme',
                '07' => 'Keski - Pohjanmaa',
                '08' => 'Keski - Suomi',
                '09' => 'Kymenlaakso',
                '10' => 'Lappi',
                '11' => 'Pirkanmaa',
                '12' => 'Pohjanmaa',
                '13' => 'Pohjois - Karjala',
                '14' => 'Pohjois - Pohjanmaa',
                '15' => 'Pohjois - Savo',
                '16' => 'Päijät - Häme',
                '17' => 'Satakunta',
                '18' => 'Uusimaa',
                '19' => 'Varsinais - Suomi',
            ),
            'FR' => Array
            (
                'A' => 'Alsace',
                'B' => 'Aquitaine',
                'C' => 'Auvergne',
                'E' => 'Brittany',
                'D' => 'Burgundy',
                'F' => 'Centre - Val de Loire',
                'G' => 'Champagne - Ardenne',
                'H' => 'Corsica',
                'I' => 'Franche - Comté',
                'K' => 'Languedoc - Roussillon',
                'L' => 'Limousin',
                'M' => 'Lorraine',
                'P' => 'Lower Normandy',
                'N' => 'Midi - Pyrénées',
                'O' => 'Nord - Pas - de - Calais',
                'R' => 'Pays de la Loire',
                'S' => 'Picardy',
                'T' => 'Poitou - Charentes',
                'U' => 'Provence - Alpes - Côte d"Azur',
                'V' => 'Rhône - Alpes',
                'Q' => 'Upper Normandy',
                'J' => 'Île - de - France',
            ),
            'GA' => Array
            (
                '1' => 'Estuaire',
                '2' => 'Haut - Ogooué',
                '3' => 'Moyen - Ogooué',
                '4' => 'Ngounié',
                '5' => 'Nyanga',
                '6' => 'Ogooué - Ivindo',
                '7' => 'Ogooué - Lolo',
                '8' => 'Ogooué - Maritime',
                '9' => 'Woleu - Ntem',
            ),
            'GM' => Array
            (
                'B' => 'Banjul',
                'M' => 'Central River',
                'L' => 'Lower River',
                'N' => 'North Bank',
                'U' => 'Upper River',
                'W' => 'Western',
            ),
            'GE' => Array
            (
                'AB' => 'Abkhazia',
                'AJ' => 'Ajaria',
                'GU' => 'Guria',
                'IM' => 'Imereti',
                'KA' => 'K"akheti',
                'KK' => 'Kvemo Kartli',
                'MM' => 'Mtskheta - Mtianeti',
                'RL' => 'Rach"a - Lechkhumi - Kvemo Svaneti',
                'SZ' => 'Samegrelo - Zemo Svaneti',
                'SJ' => 'Samtskhe - Javakheti',
                'SK' => 'Shida Kartli',
                'TB' => 'Tbilisi',
            ),
            'DE' => Array
            (
                'BW' => 'Baden - Württemberg',
                'BY' => 'Bayern',
                'BE' => 'Berlin',
                'BB' => 'Brandenburg',
                'HB' => 'Bremen',
                'HH' => 'Hamburg',
                'HE' => 'Hessen',
                'MV' => 'Mecklenburg - Vorpommern',
                'NI' => 'Niedersachsen',
                'NW' => 'Nordrhein - Westfalen',
                'RP' => 'Rheinland - Pfalz',
                'SL' => 'Saarland',
                'SN' => 'Sachsen',
                'ST' => 'Sachsen - Anhalt',
                'SH' => 'Schleswig - Holstein',
                'TH' => 'Thüringen',
            ),
            'GH' => Array
            (
                'AH' => 'Ashanti',
                'BA' => 'Brong - Ahafo',
                'CP' => 'Central',
                'EP' => 'Eastern',
                'AA' => 'Greater Accra',
                'NP' => 'Northern',
                'UE' => 'Upper East',
                'UW' => 'Upper West',
                'TV' => 'Volta',
                'WP' => 'Western',
            ),
            'GR' => Array
            (
                'A' => 'Anatoliki Makedonia kai Thraki',
                'I' => 'Attiki',
                'G' => 'Dytiki Ellada',
                'C' => 'Dytiki Makedonia',
                'F' => 'Ionia Nisia',
                'D' => 'Ipeiros',
                'B' => 'Kentriki Makedonia',
                'M' => 'Kriti',
                'L' => 'Notio Aigaio',
                'J' => 'Peloponnisos',
                'H' => 'Sterea Ellada',
                'E' => 'Thessalia',
                'K' => 'Voreio Aigaio',
            ),
            'GL' => Array
            (
                'KU' => 'Kommune Kujalleq',
                'SM' => 'Kommuneqarfik Sermersooq',
                'QA' => 'Qaasuitsup Kommunia',
                'QE' => 'Qeqqata Kommunia',
            ),
            'GD' => Array
            (
                '01' => 'Saint Andrew',
                '02' => 'Saint David',
                '03' => 'Saint George',
                '04' => 'Saint John',
                '05' => 'Saint Mark',
                '06' => 'Saint Patrick',
                '10' => 'Southern Grenadine Islands',
            ),
            'GT' => Array
            (
                'AV' => 'Alta Verapaz',
                'BV' => 'Baja Verapaz',
                'CM' => 'Chimaltenango',
                'CQ' => 'Chiquimula',
                'PR' => 'El Progreso',
                'ES' => 'Escuintla',
                'GU' => 'Guatemala',
                'HU' => 'Huehuetenango',
                'IZ' => 'Izabal',
                'JA' => 'Jalapa',
                'JU' => 'Jutiapa',
                'PE' => 'Petén',
                'QZ' => 'Quetzaltenango',
                'QC' => 'Quiché',
                'RE' => 'Retalhuleu',
                'SA' => 'Sacatepéquez',
                'SM' => 'San Marcos',
                'SR' => 'Santa Rosa',
                'SO' => 'Sololá',
                'SU' => 'Suchitepéquez',
                'TO' => 'Totonicapán',
                'ZA' => 'Zacapa',
            ),
            'GN' => Array
            (
                'B' => 'Boké',
                'C' => 'Conakry',
                'F' => 'Faranah',
                'K' => 'Kankan',
                'D' => 'Kindia',
                'L' => 'Labé',
                'M' => 'Mamou',
                'N' => 'Nzérékoré',
            ),
            'GW' => Array
            (
                'L' => 'Leste',
                'N' => 'Norte',
                'S' => 'Sul',
            ),
            'GY' => Array
            (
                'BA' => 'Barima - Waini',
                'CU' => 'Cuyuni - Mazaruni',
                'DE' => 'Demerara - Mahaica',
                'EB' => 'East Berbice - Corentyne',
                'ES' => 'Essequibo Islands - West Demerara',
                'MA' => 'Mahaica - Berbice',
                'PM' => 'Pomeroon - Supenaam',
                'PT' => 'Potaro - Siparuni',
                'UD' => 'Upper Demerara - Berbice',
                'UT' => 'Upper Takutu - Upper Essequibo',
            ),
            'HT' => Array
            (
                'AR' => 'Artibonite',
                'CE' => 'Centre',
                'GA' => 'Grande - Anse',
                'NI' => 'Nippes',
                'ND' => 'Nord',
                'NE' => 'Nord - Est',
                'NO' => 'Nord - Ouest',
                'OU' => 'Ouest',
                'SD' => 'Sud',
                'SE' => 'Sud - Est',
            ),
            'HN' => Array
            (
                'AT' => 'Atlántida',
                'CH' => 'Choluteca',
                'CL' => 'Colón',
                'CM' => 'Comayagua',
                'CP' => 'Copán',
                'CR' => 'Cortés',
                'EP' => 'El Paraíso',
                'FM' => 'Francisco Morazán',
                'GD' => 'Gracias a Dios',
                'IN' => 'Intibucá',
                'IB' => 'Islas de la Bahía',
                'LP' => 'La Paz',
                'LE' => 'Lempira',
                'OC' => 'Ocotepeque',
                'OL' => 'Olancho',
                'SB' => 'Santa Bárbara',
                'VA' => 'Valle',
                'YO' => 'Yoro',
            ),
            'HU' => Array
            (
                'BA' => 'Baranya',
                'BZ' => 'Borsod - Abaúj - Zemplén',
                'BU' => 'Budapest',
                'BK' => 'Bács - Kiskun',
                'BE' => 'Békés',
                'BC' => 'Békéscsaba',
                'CS' => 'Csongrád',
                'DE' => 'Debrecen',
                'DU' => 'Dunaújváros',
                'EG' => 'Eger',
                'FE' => 'Fejér',
                'GY' => 'Győr',
                'GS' => 'Győr - Moson - Sopron',
                'HB' => 'Hajdú - Bihar',
                'HE' => 'Heves',
                'HV' => 'Hódmezővásárhely',
                'JN' => 'Jász - Nagykun - Szolnok',
                'KV' => 'Kaposvár',
                'KM' => 'Kecskemét',
                'KE' => 'Komárom - Esztergom',
                'MI' => 'Miskolc',
                'NK' => 'Nagykanizsa',
                'NY' => 'Nyíregyháza',
                'NO' => 'Nógrád',
                'PE' => 'Pest',
                'PS' => 'Pécs',
                'ST' => 'Salgótarján',
                'SO' => 'Somogy',
                'SN' => 'Sopron',
                'SZ' => 'Szabolcs - Szatmár - Bereg',
                'SD' => 'Szeged',
                'SS' => 'Szekszárd',
                'SK' => 'Szolnok',
                'SH' => 'Szombathely',
                'SF' => 'Székesfehérvár',
                'TB' => 'Tatabánya',
                'TO' => 'Tolna',
                'VA' => 'Vas',
                'VE' => 'Veszprém',
                'VM' => 'Veszprém',
                'ZA' => 'Zala',
                'ZE' => 'Zalaegerszeg',
                'ER' => 'Érd',
            ),
            'IS' => Array
            (
                '7' => 'Austurland',
                '1' => 'Höfuðborgarsvæði utan Reykjavíkur',
                '6' => 'Norðurland eystra',
                '5' => 'Norðurland vestra',
                '0' => 'Reykjavík',
                '8' => 'Suðurland',
                '2' => 'Suðurnes',
                '4' => 'Vestfirðir',
                '3' => 'Vesturland',
            ),
            'IN' => Array
            (
                'AN' => 'Andaman and Nicobar Islands',
                'CH' => 'Chandigarh',
                'DN' => 'Dadra and Nagar Haveli',
                'DD' => 'Daman and Diu',
                'DL' => 'Delhi',
                'LD' => 'Lakshadweep',
                'PY' => 'Puducherry',
                'AP' => 'Andhra Pradesh',
                'AR' => 'Arunachal Pradesh',
                'AS' => 'Assam',
                'BR' => 'Bihar',
                'CT' => 'Chhattisgarh',
                'GA' => 'Goa',
                'GJ' => 'Gujarat',
                'HR' => 'Haryana',
                'HP' => 'Himachal Pradesh',
                'JK' => 'Jammu and Kashmir',
                'JH' => 'Jharkhand',
                'KA' => 'Karnataka',
                'KL' => 'Kerala',
                'MP' => 'Madhya Pradesh',
                'MH' => 'Maharashtra',
                'MN' => 'Manipur',
                'ML' => 'Meghalaya',
                'MZ' => 'Mizoram',
                'NL' => 'Nagaland',
                ' OR' => 'Odisha',
                'PB' => 'Punjab',
                'RJ' => 'Rajasthan',
                'SK' => 'Sikkim',
                'TN' => 'Tamil Nadu',
                'TG' => 'Telangana',
                'TR' => 'Tripura',
                'UP' => 'Uttar Pradesh',
                'UT' => 'Uttarakhand',
                'WB' => 'West Bengal',
            ),
            'ID' => Array
            (
                'JW' => 'Jawa',
                'KA' => 'Kalimantan',
                'ML' => 'Maluku',
                'NU' => 'Nusa Tenggara',
                'PP' => 'Papua',
                'SL' => 'Sulawesi',
                'SM' => 'Sumatera',
            ),
            'CI' => Array
            (
                '06' => '18 Montagnes',
                '16' => 'Agnébi',
                '17' => 'Bafing',
                '09' => 'Bas - Sassandra',
                '10' => 'Denguélé',
                '18' => 'Fromager',
                '02' => 'Haut - Sassandra',
                '07' => 'Lacs',
                '01' => 'Lagunes',
                '12' => 'Marahoué',
                '19' => 'Moyen - Cavally',
                '05' => 'Moyen - Comoé',
                '11' => 'Nzi - Comoé',
                '03' => 'Savanes',
                '15' => 'Sud - Bandama',
                '13' => 'Sud - Comoé',
                '04' => 'Vallée du Bandama',
                '14' => 'Worodougou',
                '08' => 'Zanzan',
            ),
            'IR' => Array
            (
                '32' => 'Alborz',
                '03' => 'Ardabīl',
                '06' => 'Būshehr',
                '08' => 'Chahār Maḩāll va Bakhtīārī',
                '04' => 'Eşfahān',
                '14' => 'Fārs',
                '27' => 'Golestān',
                '19' => 'Gīlān',
                '24' => 'Hamadān',
                '23' => 'Hormozgān',
                '15' => 'Kermān',
                '17' => 'Kermānshāh',
                '29' => 'Khorāsān - e Janūbī',
                '30' => 'Khorāsān - e Razavī',
                '31' => 'Khorāsān - e Shemālī',
                '10' => 'Khūzestān',
                '18' => 'Kohgīlūyeh va Būyer Aḩmad',
                '16' => 'Kordestān',
                '20' => 'Lorestān',
                '22' => 'Markazī',
                '21' => 'Māzandarān',
                '28' => 'Qazvīn',
                '26' => 'Qom',
                '12' => 'Semnān',
                '13' => 'Sīstān va Balūchestān',
                '07' => 'Tehrān',
                '25' => 'Yazd',
                '11' => 'Zanjān',
                '02' => 'Āz̄arbāyjān - e Gharbī',
                '01' => 'Āz̄arbāyjān - e Sharqī',
                '05' => 'Īlām',
            ),
            'IQ' => Array
            (
                'AN' => 'Al Anbār',
                'BA' => 'Al Başrah',
                'MU' => 'Al Muthanná',
                'QA' => 'Al Qādisīyah',
                'NA' => 'An Najaf',
                'AR' => 'Arbīl',
                'SU' => 'As Sulaymānīyah',
                'TS' => 'At Ta"mīm',
                'BG' => 'Baghdād',
                'BB' => 'Bābil',
                'DA' => 'Dahūk',
                'DQ' => 'Dhī Qār',
                'DI' => 'Diyālá',
                'KA' => 'Karbalā',
                'MA' => 'Maysān',
                'NI' => 'Nīnawá',
                'WA' => 'Wāsiţ',
                'SD' => 'Şalāḩ ad Dīn',
            ),
            'IE' => Array
            (
                'C' => 'Connaught',
                'L' => 'Leinster',
                'M' => 'Munster',
                'U' => 'Ulster',
            ),
            'IL' => Array
            (
                'D' => 'HaDarom',
                'M' => 'HaMerkaz',
                'Z' => 'HaTsafon',
                'HA' => 'H̱efa',
                'TA' => 'Tel - Aviv',
                'JM' => 'Yerushalayim',
            ),
            'IT' => Array
            (
                '65' => 'Abruzzo',
                '77' => 'Basilicata',
                '78' => 'Calabria',
                '72' => 'Campania',
                '45' => 'Emilia - Romagna',
                '36' => 'Friuli - Venezia Giulia',
                '62' => 'Lazio',
                '42' => 'Liguria',
                '25' => 'Lombardia',
                '57' => 'Marche',
                '67' => 'Molise',
                '21' => 'Piemonte',
                '75' => 'Puglia',
                '88' => 'Sardegna',
                '82' => 'Sicilia',
                '52' => 'Toscana',
                '32' => 'Trentino - Alto Adige',
                '55' => 'Umbria',
                '23' => 'Valle d"Aosta',
                '34' => 'Veneto',
            ),
            'JM' => Array
            (
                '13' => 'Clarendon',
                '09' => 'Hanover',
                '01' => 'Kingston',
                '12' => 'Manchester',
                '04' => 'Portland',
                '02' => 'Saint Andrew',
                '06' => 'Saint Ann',
                '14' => 'Saint Catherine',
                '11' => 'Saint Elizabeth',
                '08' => 'Saint James',
                '05' => 'Saint Mary',
                '03' => 'Saint Thomas',
                '07' => 'Trelawny',
                '10' => 'Westmoreland',
            ),
            'JP' => Array
            (
                '23' => 'Aiti',
                '05' => 'Akita',
                '02' => 'Aomori',
                '38' => 'Ehime',
                '21' => 'Gihu',
                '10' => 'Gunma',
                '34' => 'Hirosima',
                '01' => 'Hokkaidô',
                '18' => 'Hukui',
                '40' => 'Hukuoka',
                '07' => 'Hukusima',
                '28' => 'Hyôgo',
                '08' => 'Ibaraki',
                '17' => 'Isikawa',
                '03' => 'Iwate',
                '37' => 'Kagawa',
                '46' => 'Kagosima',
                '14' => 'Kanagawa',
                '43' => 'Kumamoto',
                '26' => 'Kyôto',
                '39' => 'Kôti',
                '24' => 'Mie',
                '04' => 'Miyagi',
                '45' => 'Miyazaki',
                '20' => 'Nagano',
                '42' => 'Nagasaki',
                '29' => 'Nara',
                '15' => 'Niigata',
                '33' => 'Okayama',
                '47' => 'Okinawa',
                '41' => 'Saga',
                '11' => 'Saitama',
                '25' => 'Siga',
                '32' => 'Simane',
                '22' => 'Sizuoka',
                '12' => 'Tiba',
                '36' => 'Tokusima',
                '09' => 'Totigi',
                '31' => 'Tottori',
                '16' => 'Toyama',
                '13' => 'Tôkyô',
                '30' => 'Wakayama',
                '06' => 'Yamagata',
                '35' => 'Yamaguti',
                '19' => 'Yamanasi',
                '44' => 'Ôita',
                '27' => 'Ôsaka',
            ),
            'JO' => Array
            (
                'BA' => 'Al Balqā',
                'AQ' => 'Al ʽAqabah',
                'AZ' => 'Az Zarqā',
                'AT' => 'Aţ Ţafīlah',
                'IR' => 'Irbid',
                'JA' => 'Jerash',
                'KA' => 'Karak',
                'MN' => 'Ma"ān',
                'MA' => 'Mafraq',
                'MD' => 'Mādabā',
                'AJ' => 'ʽAjlūn',
                'AM' => '‘Ammān',
            ),
            'KZ' => Array
            (
                'ALA' => 'Almaty',
                'ALM' => 'Almaty oblysy',
                'AKM' => 'Aqmola oblysy',
                'AKT' => 'Aqtöbe oblysy',
                'AST' => 'Astana',
                'ATY' => 'Atyraū oblysy',
                'ZAP' => 'Batys Qazaqstan oblysy',
                'MAN' => 'Mangghystaū oblysy',
                'YUZ' => 'Ongtüstik Qazaqstan oblysy',
                'PAV' => 'Pavlodar oblysy',
                'KAR' => 'Qaraghandy oblysy',
                'KUS' => 'Qostanay oblysy',
                'KZY' => 'Qyzylorda oblysy',
                'VOS' => 'Shyghys Qazaqstan oblysy',
                'SEV' => 'Soltüstik Qazaqstan oblysy',
                'ZHA' => 'Zhambyl oblysy',
            ),
            'KE' => Array
            (
                '200' => 'Central',
                '300' => 'Coast',
                '400' => 'Eastern',
                '110' => 'Nairobi',
                '500' => 'North - Eastern',
                '600' => 'Nyanza',
                '700' => 'Rift Valley',
                '800' => 'Western',
            ),
            'KI' => Array
            (
                'G' => 'Gilbert Islands',
                'L' => 'Line Islands',
                'P' => 'Phoenix Islands',
            ),
            'KW' => Array
            (
                'AH' => 'Al Aḩmadi',
                'FA' => 'Al Farwānīyah',
                'JA' => 'Al Jahrā’',
                'KU' => 'Al Kuwayt',
                'MU' => 'Mubārak al Kabīr',
                'HA' => 'Ḩawallī',
            ),
            'KG' => Array
            (
                'B' => 'Batken',
                'GB' => 'Bishkek',
                'C' => 'Chü',
                'J' => 'Jalal - Abad',
                'N' => 'Naryn',
                'O' => 'Osh',
                'T' => 'Talas',
                'Y' => 'Ysyk - Köl',
            ),
            'LA' => Array
            (
                'AT' => 'Attapu',
                'BK' => 'Bokèo',
                'BL' => 'Bolikhamxai',
                'CH' => 'Champasak',
                'HO' => 'Houaphan',
                'KH' => 'Khammouan',
                'LM' => 'Louang Namtha',
                'LP' => 'Louangphabang',
                'OU' => 'Oudômxai',
                'PH' => 'Phôngsali',
                'SL' => 'Salavan',
                'SV' => 'Savannakhét',
                'VT' => 'Vientiane',
                'VI' => 'Vientiane',
                'XA' => 'Xaignabouli',
                'XN' => 'Xaisômboun',
                'XI' => 'Xiangkhoang',
                'XE' => 'Xékong',
            ),
            'LV' => Array
            (
                '001' => 'Aglonas novads',
                '002' => 'Aizkraukles novads',
                '003' => 'Aizputes novads',
                '004' => 'Aknīstes novads',
                '005' => 'Alojas novads',
                '006' => 'Alsungas novads',
                '007' => 'Alūksnes novads',
                '008' => 'Amatas novads',
                '009' => 'Apes novads',
                '010' => 'Auces novads',
                '012' => 'Babītes novads',
                '013' => 'Baldones novads',
                '014' => 'Baltinavas novads',
                '015' => 'Balvu novads',
                '016' => 'Bauskas novads',
                '017' => 'Beverīnas novads',
                '018' => 'Brocēnu novads',
                '019' => 'Burtnieku novads',
                '020' => 'Carnikavas novads',
                '021' => 'Cesvaines novads',
                '023' => 'Ciblas novads',
                '022' => 'Cēsu novads',
                '024' => 'Dagdas novads',
                'DGV' => 'Daugavpils',
                '025' => 'Daugavpils novads',
                '026' => 'Dobeles novads',
                '027' => 'Dundagas novads',
                '028' => 'Durbes novads',
                '029' => 'Engures novads',
                '031' => 'Garkalnes novads',
                '032' => 'Grobiņas novads',
                '033' => 'Gulbenes novads',
                '034' => 'Iecavas novads',
                '035' => 'Ikšķiles novads',
                '036' => 'Ilūkstes novads',
                '037' => 'Inčukalna novads',
                '038' => 'Jaunjelgavas novads',
                '039' => 'Jaunpiebalgas novads',
                '040' => 'Jaunpils novads',
                'JEL' => 'Jelgava',
                '041' => 'Jelgavas novads',
                'JKB' => 'Jēkabpils',
                '042' => 'Jēkabpils novads',
                'JUR' => 'Jūrmala',
                '043' => 'Kandavas novads',
                '045' => 'Kocēnu novads',
                '046' => 'Kokneses novads',
                '048' => 'Krimuldas novads',
                '049' => 'Krustpils novads',
                '047' => 'Krāslavas novads',
                '050' => 'Kuldīgas novads',
                '044' => 'Kārsavas novads',
                '053' => 'Lielvārdes novads',
                'LPX' => 'Liepāja',
                '054' => 'Limbažu novads',
                '057' => 'Lubānas novads',
                '058' => 'Ludzas novads',
                '055' => 'Līgatnes novads',
                '056' => 'Līvānu novads',
                '059' => 'Madonas novads',
                '060' => 'Mazsalacas novads',
                '061' => 'Mālpils novads',
                '062' => 'Mārupes novads',
                '063' => 'Mērsraga novads',
                '064' => 'Naukšēnu novads',
                '065' => 'Neretas novads',
                '066' => 'Nīcas novads',
                '067' => 'Ogres novads',
                '068' => 'Olaines novads',
                '069' => 'Ozolnieku novads',
                '073' => 'Preiļu novads',
                '074' => 'Priekules novads',
                '075' => 'Priekuļu novads',
                '070' => 'Pārgaujas novads',
                '071' => 'Pāvilostas novads',
                '072' => 'Pļaviņu novads',
                '076' => 'Raunas novads',
                '078' => 'Riebiņu novads',
                '079' => 'Rojas novads',
                '080' => 'Ropažu novads',
                '081' => 'Rucavas novads',
                '082' => 'Rugāju novads',
                '083' => 'Rundāles novads',
                'REZ' => 'Rēzekne',
                '077' => 'Rēzeknes novads',
                'RIX' => 'Rīga',
                '084' => 'Rūjienas novads',
                '086' => 'Salacgrīvas novads',
                '085' => 'Salas novads',
                '087' => 'Salaspils novads',
                '088' => 'Saldus novads',
                '089' => 'Saulkrastu novads',
                '091' => 'Siguldas novads',
                '093' => 'Skrundas novads',
                '092' => 'Skrīveru novads',
                '094' => 'Smiltenes novads',
                '095' => 'Stopiņu novads',
                '096' => 'Strenču novads',
                '090' => 'Sējas novads',
                '097' => 'Talsu novads',
                '099' => 'Tukuma novads',
                '098' => 'Tērvetes novads',
                '100' => 'Vaiņodes novads',
                '101' => 'Valkas novads',
                'VMR' => 'Valmiera',
                '102' => 'Varakļānu novads',
                '104' => 'Vecpiebalgas novads',
                '105' => 'Vecumnieku novads',
                'VEN' => 'Ventspils',
                '106' => 'Ventspils novads',
                '107' => 'Viesītes novads',
                '108' => 'Viļakas novads',
                '109' => 'Viļānu novads',
                '103' => 'Vārkavas novads',
                '110' => 'Zilupes novads',
                '011' => 'Ādažu novads',
                '030' => 'Ērgļu novads',
                '051' => 'Ķeguma novads',
                '052' => 'Ķekavas novads',
            ),
            'LB' => Array
            (
                'AK' => 'Aakkâr',
                'BH' => 'Baalbek - Hermel',
                'BA' => 'Beyrouth',
                'BI' => 'Béqaa',
                'AS' => 'Liban - Nord',
                'JA' => 'Liban - Sud',
                'JL' => 'Mont - Liban',
                'NA' => 'Nabatîyé',
            ),
            'LS' => Array
            (
                'D' => 'Berea',
                'B' => 'Butha - Buthe',
                'C' => 'Leribe',
                'E' => 'Mafeteng',
                'A' => 'Maseru',
                'F' => 'Mohale"s Hoek',
                'J' => 'Mokhotlong',
                'H' => 'Qacha"s Nek',
                'G' => 'Quthing',
                'K' => 'Thaba - Tseka',
            ),
            'LR' => Array
            (
                'BM' => 'Bomi',
                'BG' => 'Bong',
                'GP' => 'Gbarpolu',
                'GB' => 'Grand Bassa',
                'CM' => 'Grand Cape Mount',
                'GG' => 'Grand Gedeh',
                'GK' => 'Grand Kru',
                'LO' => 'Lofa',
                'MG' => 'Margibi',
                'MY' => 'Maryland',
                'MO' => 'Montserrado',
                'NI' => 'Nimba',
                'RG' => 'River Gee',
                'RI' => 'Rivercess',
                'SI' => 'Sinoe',
            ),
            'LY' => Array
            (
                'BU' => 'Al Buţnān',
                'JA' => 'Al Jabal al Akhḑar',
                'JG' => 'Al Jabal al Gharbī',
                'JI' => 'Al Jifārah',
                'JU' => 'Al Jufrah',
                'KF' => 'Al Kufrah',
                'MJ' => 'Al Marj',
                'MB' => 'Al Marqab',
                'WA' => 'Al Wāḩāt',
                'NQ' => 'An Nuqaţ al Khams',
                'ZA' => 'Az Zāwiyah',
                'BA' => 'Banghāzī',
                'DR' => 'Darnah',
                'GT' => 'Ghāt',
                'MI' => 'Mişrātah',
                'MQ' => 'Murzuq',
                'NL' => 'Nālūt',
                'SB' => 'Sabhā',
                'SR' => 'Surt',
                'WD' => 'Wadi al hayat',
                'WS' => 'Wadi ash shati',
                'TB' => 'Tarabulus',
            ),
            'LI' => Array
            (
                '01' => 'Balzers',
                '02' => 'Eschen',
                '03' => 'Gamprin',
                '04' => 'Mauren',
                '05' => 'Planken',
                '06' => 'Ruggell',
                '07' => 'Schaan',
                '08' => 'Schellenberg',
                '09' => 'Triesen',
                '10' => 'Triesenberg',
                '11' => 'Vaduz',
            ),
            'LT' => Array
            (
                'AL' => 'Alytaus Apskritis',
                'KU' => 'Kauno Apskritis',
                'KL' => 'Klaipėdos Apskritis',
                'MR' => 'Marijampolės Apskritis',
                'PN' => 'Panevėžio Apskritis',
                'TA' => 'Tauragės Apskritis',
                'TE' => 'Telšių Apskritis',
                'UT' => 'Utenos Apskritis',
                'VL' => 'Vilniaus Apskritis',
                'SA' => 'Šiaulių Apskritis',
            ),
            'LU' => Array
            (
                'D' => 'Diekirch',
                'G' => 'Grevenmacher',
                'L' => 'Luxembourg',
            ),
            'MK' => Array
            (
                '01' => 'Aerodrom',
                '02' => 'Aračinovo',
                '03' => 'Berovo',
                '04' => 'Bitola',
                '05' => 'Bogdanci',
                '06' => 'Bogovinje',
                '07' => 'Bosilovo',
                '08' => 'Brvenica',
                '09' => 'Butel',
                '77' => 'Centar',
                '78' => 'Centar Župa',
                '21' => 'Debar',
                '22' => 'Debarca',
                '23' => 'Delčevo',
                '25' => 'Demir Hisar',
                '24' => 'Demir Kapija',
                '26' => 'Dojran',
                '27' => 'Dolneni',
                '28' => 'Drugovo',
                '17' => 'Gazi Baba',
                '18' => 'Gevgelija',
                '29' => 'Gjorče Petrov',
                '19' => 'Gostivar',
                '20' => 'Gradsko',
                '34' => 'Ilinden',
                '35' => 'Jegunovce',
                '37' => 'Karbinci',
                '38' => 'Karpoš',
                '36' => 'Kavadarci',
                '39' => 'Kisela Voda',
                '40' => 'Kičevo',
                '41' => 'Konče',
                '42' => 'Kočani',
                '43' => 'Kratovo',
                '44' => 'Kriva Palanka',
                '45' => 'Krivogaštani',
                '46' => 'Kruševo',
                '47' => 'Kumanovo',
                '48' => 'Lipkovo',
                '49' => 'Lozovo',
                '51' => 'Makedonska Kamenica',
                '52' => 'Makedonski Brod',
                '50' => 'Mavrovo i Rostuša',
                '53' => 'Mogila',
                '54' => 'Negotino',
                '55' => 'Novaci',
                '56' => 'Novo Selo',
                '58' => 'Ohrid',
                '57' => 'Oslomej',
                '60' => 'Pehčevo',
                '59' => 'Petrovec',
                '61' => 'Plasnica',
                '62' => 'Prilep',
                '63' => 'Probištip',
                '64' => 'Radoviš',
                '65' => 'Rankovce',
                '66' => 'Resen',
                '67' => 'Rosoman',
                '68' => 'Saraj',
                '70' => 'Sopište',
                '71' => 'Staro Nagoričane',
                '72' => 'Struga',
                '73' => 'Strumica',
                '74' => 'Studeničani',
                '69' => 'Sveti Nikole',
                '75' => 'Tearce',
                '76' => 'Tetovo',
                '10' => 'Valandovo',
                '11' => 'Vasilevo',
                '13' => 'Veles',
                '12' => 'Vevčani',
                '14' => 'Vinica',
                '15' => 'Vraneštica',
                '16' => 'Vrapčište',
                '31' => 'Zajas',
                '32' => 'Zelenikovo',
                '33' => 'Zrnovci',
                '79' => 'Čair',
                '80' => 'Čaška',
                '81' => 'Češinovo - Obleševo',
                '82' => 'Čučer Sandevo',
                '83' => 'Štip',
                '84' => 'Šuto Orizari',
                '30' => 'Želino',
            ),
            'MG' => Array
            (
                'T' => 'Antananarivo',
                'D' => 'Antsiranana',
                'F' => 'Fianarantsoa',
                'M' => 'Mahajanga',
                'A' => 'Toamasina',
                'U' => 'Toliara',
            ),
            'MW' => Array
            (
                'C' => 'Central Region',
                'N' => 'Northern Region',
                'S' => 'Southern Region',
            ),
            'MY' => Array
            (
                '14' => 'Wilayah Persekutuan Kuala Lumpur',
                '15' => 'Wilayah Persekutuan Labuan',
                '16' => 'Wilayah Persekutuan Putrajaya',
                '01' => 'Johor',
                '02' => 'Kedah',
                '03' => 'Kelantan',
                '04' => 'Melaka',
                '05' => 'Negeri Sembilan',
                '06' => 'Pahang',
                '08' => 'Perak',
                '09' => 'Perlis',
                '07' => 'Pulau Pinang',
                '12' => 'Sabah',
                '13' => 'Sarawak',
                '10' => 'Selangor',
                '11' => 'Terengganu',
            ),
            'MV' => Array
            (
                'CE' => 'Central',
                'MLE' => 'Male',
                'NO' => 'North',
                'NC' => 'North Central',
                'SU' => 'South',
                'SC' => 'South Central',
                'UN' => 'Upper North',
                'US' => 'Upper South',
            ),
            'ML' => Array
            (
                'BKO' => 'Bamako',
                '7' => 'Gao',
                '1' => 'Kayes',
                '8' => 'Kidal',
                '2' => 'Koulikoro',
                '5' => 'Mopti',
                '3' => 'Sikasso',
                '4' => 'Ségou',
                '6' => 'Tombouctou',
            ),
            'MT' => Array
            (
                '01' => 'Attard',
                '02' => 'Balzan',
                '03' => 'Birgu',
                '04' => 'Birkirkara',
                '05' => 'Birżebbuġa',
                '06' => 'Bormla',
                '07' => 'Dingli',
                '08' => 'Fgura',
                '09' => 'Floriana',
                '10' => 'Fontana',
                '11' => 'Gudja',
                '13' => 'Għajnsielem',
                '14' => 'Għarb',
                '15' => 'Għargħur',
                '16' => 'Għasri',
                '17' => 'Għaxaq',
                '12' => 'Gżira',
                '19' => 'Iklin',
                '20' => 'Isla',
                '21' => 'Kalkara',
                '22' => 'Kerċem',
                '23' => 'Kirkop',
                '24' => 'Lija',
                '25' => 'Luqa',
                '26' => 'Marsa',
                '27' => 'Marsaskala',
                '28' => 'Marsaxlokk',
                '29' => 'Mdina',
                '30' => 'Mellieħa',
                '32' => 'Mosta',
                '33' => 'Mqabba',
                '34' => 'Msida',
                '35' => 'Mtarfa',
                '36' => 'Munxar',
                '31' => 'Mġarr',
                '37' => 'Nadur',
                '38' => 'Naxxar',
                '39' => 'Paola',
                '40' => 'Pembroke',
                '41' => 'Pietà',
                '42' => 'Qala',
                '43' => 'Qormi',
                '44' => 'Qrendi',
                '45' => 'Rabat Għawdex',
                '46' => 'Rabat Malta',
                '47' => 'Safi',
                '50' => 'San Lawrenz',
                '51' => 'San Pawl il - Baħar',
                '48' => 'San Ġiljan',
                '49' => 'San Ġwann',
                '52' => 'Sannat',
                '53' => 'Santa Luċija',
                '54' => 'Santa Venera',
                '55' => 'Siġġiewi',
                '56' => 'Sliema',
                '57' => 'Swieqi',
                '58' => 'Ta Xbiex',
                '59' => 'Tarxien',
                '60' => 'Valletta',
                '61' => 'Xagħra',
                '62' => 'Xewkija',
                '63' => 'Xgħajra',
                '18' => 'Ħamrun',
                '64' => 'Żabbar',
                '65' => 'Żebbuġ Għawdex',
                '66' => 'Żebbuġ Malta',
                '67' => 'Żejtun',
                '68' => 'Żurrieq',
            ),
            'MH' => Array
            (
                'L' => 'Ralik chain',
                'T' => 'Ratak chain',
            ),
            'MR' => Array
            (
                '07' => 'Adrar',
                '03' => 'Assaba',
                '05' => 'Brakna',
                '08' => 'Dakhlet Nouâdhibou',
                '04' => 'Gorgol',
                '10' => 'Guidimaka',
                '01' => 'Hodh ech Chargui',
                '02' => 'Hodh el Gharbi',
                '12' => 'Inchiri',
                'NKC' => 'Nouakchott',
                '09' => 'Tagant',
                '11' => 'Tiris Zemmour',
                '06' => 'Trarza',
            ),
            'MU' => Array
            (
                'AG' => 'Agalega Islands',
                'BR' => 'Beau Bassin - Rose Hill',
                'BL' => 'Black River',
                'CC' => 'Cargados Carajos Shoals',
                'CU' => 'Curepipe',
                'FL' => 'Flacq',
                'GP' => 'Grand Port',
                'MO' => 'Moka',
                'PA' => 'Pamplemousses',
                'PW' => 'Plaines Wilhems',
                'PL' => 'Port Louis',
                'PU' => 'Port Louis',
                'QB' => 'Quatre Bornes',
                'RR' => 'Rivière du Rempart',
                'RO' => 'Rodrigues Island',
                'SA' => 'Savanne',
                'VP' => 'Vacoas - Phoenix',
            ),
            'MX' => Array
            (
                'DIF' => 'Distrito Federal',
                'AGU' => 'Aguascalientes',
                'BCN' => 'Baja California',
                'BCS' => 'Baja California Sur',
                'CAM' => 'Campeche',
                'CHP' => 'Chiapas',
                'CHH' => 'Chihuahua',
                'COA' => 'Coahuila',
                'COL' => 'Colima',
                'DUR' => 'Durango',
                'GUA' => 'Guanajuato',
                'GRO' => 'Guerrero',
                'HID' => 'Hidalgo',
                'JAL' => 'Jalisco',
                'MIC' => 'Michoacán',
                'MOR' => 'Morelos',
                'MEX' => 'México',
                'NAY' => 'Nayarit',
                'NLE' => 'Nuevo León',
                'OAX' => 'Oaxaca',
                'PUE' => 'Puebla',
                'QUE' => 'Querétaro',
                'ROO' => 'Quintana Roo',
                'SLP' => 'San Luis Potosí',
                'SIN' => 'Sinaloa',
                'SON' => 'Sonora',
                'TAB' => 'Tabasco',
                'TAM' => 'Tamaulipas',
                'TLA' => 'Tlaxcala',
                'VER' => 'Veracruz',
                'YUC' => 'Yucatán',
                'ZAC' => 'Zacatecas',
            ),
            'FM' => Array
            (
                'TRK' => 'Chuuk',
                'KSA' => 'Kosrae',
                'PNI' => 'Pohnpei',
                'YAP' => 'Yap',
            ),
            'MD' => Array
            (
                'AN' => 'Anenii Noi',
                'BS' => 'Basarabeasca',
                'BR' => 'Briceni',
                'BA' => 'Bălţi',
                'CA' => 'Cahul',
                'CT' => 'Cantemir',
                'CU' => 'Chişinău',
                'CM' => 'Cimişlia',
                'CR' => 'Criuleni',
                'CL' => 'Călăraşi',
                'CS' => 'Căuşeni',
                'DO' => 'Donduşeni',
                'DR' => 'Drochia',
                'DU' => 'Dubăsari',
                'ED' => 'Edineţ',
                'FL' => 'Floreşti',
                'FA' => 'Făleşti',
                'GL' => 'Glodeni',
                'GA' => 'Găgăuzia, Unitatea teritorială autonomă',
                'HI' => 'Hînceşti',
                'IA' => 'Ialoveni',
                'LE' => 'Leova',
                'NI' => 'Nisporeni',
                'OC' => 'Ocniţa',
                'OR' => 'Orhei',
                'RE' => 'Rezina',
                'RI' => 'Rîşcani',
                'SO' => 'Soroca',
                'ST' => 'Străşeni',
                'SN' => 'Stînga Nistrului, unitatea teritorială din',
                'SI' => 'Sîngerei',
                'TA' => 'Taraclia',
                'TE' => 'Teleneşti',
                'BD' => 'Tighina',
                'UN' => 'Ungheni',
                'SD' => 'Şoldăneşti',
                'SV' => 'Ştefan Vodă',
            ),
            'MC' => Array
            (
                'FO' => 'Fontvieille',
                'JE' => 'Jardin Exotique',
                'CL' => 'La Colle',
                'CO' => 'La Condamine',
                'GA' => 'La Gare',
                'SO' => 'La Source',
                'LA' => 'Larvotto',
                'MA' => 'Malbousquet',
                'MO' => 'Monaco - Ville',
                'MG' => 'Moneghetti',
                'MC' => 'Monte - Carlo',
                'MU' => 'Moulins',
                'PH' => 'Port - Hercule',
                'SR' => 'Saint - Roman',
                'SD' => 'Sainte - Dévote',
                'SP' => 'Spélugues',
                'VR' => 'Vallon de la Rousse',
            ),
            'MN' => Array
            (
                '073' => 'Arhangay',
                '071' => 'Bayan - Ölgiy',
                '069' => 'Bayanhongor',
                '067' => 'Bulgan',
                '037' => 'Darhan uul',
                '061' => 'Dornod',
                '063' => 'Dornogovĭ',
                '059' => 'Dundgovĭ',
                '057' => 'Dzavhan',
                '065' => 'Govĭ - Altay',
                '064' => 'Govĭ - Sümber',
                '039' => 'Hentiy',
                '043' => 'Hovd',
                '041' => 'Hövsgöl',
                '035' => 'Orhon',
                '049' => 'Selenge',
                '051' => 'Sühbaatar',
                '047' => 'Töv',
                '1' => 'Ulaanbaatar',
                '046' => 'Uvs',
                '053' => 'Ömnögovĭ',
                '055' => 'Övörhangay',
            ),
            'ME' => Array
            (
                '01' => 'Andrijevica',
                '02' => 'Bar',
                '03' => 'Berane',
                '04' => 'Bijelo Polje',
                '05' => 'Budva',
                '06' => 'Cetinje',
                '07' => 'Danilovgrad',
                '22' => 'Gusinje',
                '08' => 'Herceg - Novi',
                '09' => 'Kolašin',
                '10' => 'Kotor',
                '11' => 'Mojkovac',
                '12' => 'Nikšić',
                '23' => 'Petnjica',
                '13' => 'Plav',
                '14' => 'Pljevlja',
                '15' => 'Plužine',
                '16' => 'Podgorica',
                '17' => 'Rožaje',
                '19' => 'Tivat',
                '20' => 'Ulcinj',
                '18' => 'Šavnik',
                '21' => 'Žabljak',
            ),
            'MA' => Array
            (
                '09' => 'Chaouia - Ouardigha',
                '10' => 'Doukhala - Abda',
                '05' => 'Fès - Boulemane',
                '02' => 'Gharb - Chrarda - Beni Hssen',
                '08' => 'Grand Casablanca',
                '14' => 'Guelmim - Es Smara',
                '04' => 'L"Oriental',
                '15' => 'Laâyoune - Boujdour - Sakia el Hamra',
                '11' => 'Marrakech - Tensift - Al Haouz',
                '06' => 'Meknès - Tafilalet',
                '16' => 'Oued ed Dahab - Lagouira',
                '07' => 'Rabat - Salé - Zemmour - Zaer',
                '13' => 'Souss - Massa - Drâa',
                '12' => 'Tadla - Azilal',
                '01' => 'Tanger - Tétouan',
                '03' => 'Taza - Al Hoceima - Taounate',
            ),
            'MZ' => Array
            (
                'P' => 'Cabo Delgado',
                'G' => 'Gaza',
                'I' => 'Inhambane',
                'B' => 'Manica',
                'MPM' => 'Maputo',
                'L' => 'Maputo',
                'N' => 'Nampula',
                'A' => 'Niassa',
                'S' => 'Sofala',
                'T' => 'Tete',
                'Q' => 'Zambézia',
            ),
            'MM' => Array
            (
                '07' => 'Ayeyarwady',
                '02' => 'Bago',
                '14' => 'Chin',
                '11' => 'Kachin',
                '12' => 'Kayah',
                '13' => 'Kayin',
                '03' => 'Magway',
                '04' => 'Mandalay',
                '15' => 'Mon',
                '16' => 'Rakhine',
                '01' => 'Sagaing',
                '17' => 'Shan',
                '05' => 'Tanintharyi',
                '06' => 'Yangon',
            ),
            'NA' => Array
            (
                'ER' => 'Erongo',
                'HA' => 'Hardap',
                'KA' => 'Karas',
                'KE' => 'Kavango East',
                'KW' => 'Kavango West',
                'KH' => 'Khomas',
                'KU' => 'Kunene',
                'OW' => 'Ohangwena',
                'OH' => 'Omaheke',
                'OS' => 'Omusati',
                'ON' => 'Oshana',
                'OT' => 'Oshikoto',
                'OD' => 'Otjozondjupa',
                'CA' => 'Zambezi',
            ),
            'NR' => Array
            (
                '01' => 'Aiwo',
                '02' => 'Anabar',
                '03' => 'Anetan',
                '04' => 'Anibare',
                '05' => 'Baiti',
                '06' => 'Boe',
                '07' => 'Buada',
                '08' => 'Denigomodu',
                '09' => 'Ewa',
                '10' => 'Ijuw',
                '11' => 'Meneng',
                '12' => 'Nibok',
                '13' => 'Uaboe',
                '14' => 'Yaren',
            ),
            'NP' => Array
            (
                '2' => 'Madhya Pashchimanchal',
                '1' => 'Madhyamanchal',
                '3' => 'Pashchimanchal',
                '4' => 'Purwanchal',
                '5' => 'Sudur Pashchimanchal',
            ),
            'NL' => Array
            (
                'DR' => 'Drenthe',
                'FL' => 'Flevoland',
                'FR' => 'Fryslân',
                'GE' => 'Gelderland',
                'GR' => 'Groningen',
                'LI' => 'Limburg',
                'NB' => 'Noord - Brabant',
                'NH' => 'Noord - Holland',
                'OV' => 'Overijssel',
                'UT' => 'Utrecht',
                'ZE' => 'Zeeland',
                'ZH' => 'Zuid - Holland',
                'AW' => 'Aruba',
                'CW' => 'Curaçao',
                'SX' => 'Sint Maarten',
                'BQ1' => 'Bonaire',
                'BQ2' => 'Saba',
                'BQ3' => 'Sint Eustatius',
            ),
            'NZ' => Array
            (
                'N' => 'North Island',
                'S' => 'South Island',
                'AUK' => 'Auckland',
                'BOP' => 'Bay of Plenty',
                'CAN' => 'Canterbury',
                'HKB' => 'Hawke"s Bay',
                'MWT' => 'Manawatu - Wanganui',
                'NTL' => 'Northland',
                'OTA' => 'Otago',
                'STL' => 'Southland',
                'TKI' => 'Taranaki',
                'WKO' => 'Waikato',
                'WGN' => 'Wellington',
                'WTC' => 'West Coast',
                'CIT' => 'Chatham Islands Territory',
                'GIS' => 'Gisborne District',
                'MBH' => 'Marlborough District',
                'NSN' => 'Nelson City',
                'TAS' => 'Tasman District',
            ),
            'NI' => Array
            (
                'AN' => 'Atlántico Norte',
                'AS' => 'Atlántico Sur',
                'BO' => 'Boaco',
                'CA' => 'Carazo',
                'CI' => 'Chinandega',
                'CO' => 'Chontales',
                'ES' => 'Estelí',
                'GR' => 'Granada',
                'JI' => 'Jinotega',
                'LE' => 'León',
                'MD' => 'Madriz',
                'MN' => 'Managua',
                'MS' => 'Masaya',
                'MT' => 'Matagalpa',
                'NS' => 'Nueva Segovia',
                'RI' => 'Rivas',
                'SJ' => 'Río San Juan',
            ),
            'NE' => Array
            (
                '1' => 'Agadez',
                '2' => 'Diffa',
                '3' => 'Dosso',
                '4' => 'Maradi',
                '8' => 'Niamey',
                '5' => 'Tahoua',
                '6' => 'Tillabéri',
                '7' => 'Zinder',
            ),
            'NG' => Array
            (
                'AB' => 'Abia',
                'FC' => 'Abuja Federal Capital Territory',
                'AD' => 'Adamawa',
                'AK' => 'Akwa Ibom',
                'AN' => 'Anambra',
                'BA' => 'Bauchi',
                'BY' => 'Bayelsa',
                'BE' => 'Benue',
                'BO' => 'Borno',
                'CR' => 'Cross River',
                'DE' => 'Delta',
                'EB' => 'Ebonyi',
                'ED' => 'Edo',
                'EK' => 'Ekiti',
                'EN' => 'Enugu',
                'GO' => 'Gombe',
                'IM' => 'Imo',
                'JI' => 'Jigawa',
                'KD' => 'Kaduna',
                'KN' => 'Kano',
                'KT' => 'Katsina',
                'KE' => 'Kebbi',
                'KO' => 'Kogi',
                'KW' => 'Kwara',
                'LA' => 'Lagos',
                'NA' => 'Nassarawa',
                'NI' => 'Niger',
                'OG' => 'Ogun',
                'ON' => 'Ondo',
                'OS' => 'Osun',
                'OY' => 'Oyo',
                'PL' => 'Plateau',
                'RI' => 'Rivers',
                'SO' => 'Sokoto',
                'TA' => 'Taraba',
                'YO' => 'Yobe',
                'ZA' => 'Zamfara',
            ),
            'KP' => Array
            (
                '04' => 'Chagang',
                '07' => 'Kangwon',
                '09' => 'North Hamgyong',
                '06' => 'North Hwanghae',
                '03' => 'North Pyongan',
                '01' => 'Pyongyang',
                '13' => 'Rason',
                '10' => 'Ryanggang',
                '08' => 'South Hamgyong',
                '05' => 'South Hwanghae',
                '02' => 'South Pyongan',
            ),
            'NO' => Array
            (
                '02' => 'Akershus',
                '09' => 'Aust - Agder',
                '06' => 'Buskerud',
                '20' => 'Finnmark',
                '04' => 'Hedmark',
                '12' => 'Hordaland',
                '22' => 'Jan Mayen',
                '15' => 'Møre og Romsdal',
                '17' => 'Nord - Trøndelag',
                '18' => 'Nordland',
                '05' => 'Oppland',
                '03' => 'Oslo',
                '11' => 'Rogaland',
                '14' => 'Sogn og Fjordane',
                '21' => 'Svalbard',
                '16' => 'Sør - Trøndelag',
                '08' => 'Telemark',
                '19' => 'Troms',
                '10' => 'Vest - Agder',
                '07' => 'Vestfold',
                '01' => 'Østfold',
            ),
            'OM' => Array
            (
                'DA' => 'Ad Dākhilīyah',
                'BU' => 'Al Buraymī',
                'BA' => 'Al Bāţinah',
                'WU' => 'Al Wusţá',
                'SH' => 'Ash Sharqīyah',
                'ZA' => 'Az̧ Z̧āhirah',
                'MA' => 'Masqaţ',
                'MU' => 'Musandam',
                'ZU' => 'Z̧ufār',
            ),
            'PK' => Array
            (
                'JK' => 'Azad Kashmir',
                'BA' => 'Balochistan',
                'TA' => 'Federally Administered Tribal Areas',
                'GB' => 'Gilgit - Baltistan',
                'IS' => 'Islamabad',
                'KP' => 'Khyber Pakhtunkhwa',
                'PB' => 'Punjab',
                'SD' => 'Sindh',
            ),
            'PW' => Array
            (
                '002' => 'Aimeliik',
                '004' => 'Airai',
                '010' => 'Angaur',
                '050' => 'Hatobohei',
                '100' => 'Kayangel',
                '150' => 'Koror',
                '212' => 'Melekeok',
                '214' => 'Ngaraard',
                '218' => 'Ngarchelong',
                '222' => 'Ngardmau',
                '224' => 'Ngatpang',
                '226' => 'Ngchesar',
                '227' => 'Ngeremlengui',
                '228' => 'Ngiwal',
                '350' => 'Peleliu',
                '370' => 'Sonsorol',
            ),
            'PS' => Array
            (
                'BTH' => 'Bethlehem',
                'DEB' => 'Deir El Balah',
                'GZA' => 'Gaza',
                'HBN' => 'Hebron',
                'JEN' => 'Jenin',
                'JRH' => 'Jericho – Al Aghwar',
                'JEM' => 'Jerusalem',
                'KYS' => 'Khan Yunis',
                'NBS' => 'Nablus',
                'NGZ' => 'North Gaza',
                'QQA' => 'Qalqilya',
                'RFH' => 'Rafah',
                'RBH' => 'Ramallah',
                'SLT' => 'Salfit',
                'TBS' => 'Tubas',
                'TKM' => 'Tulkarm',
            ),
            'PA' => Array
            (
                '1' => 'Bocas del Toro',
                '4' => 'Chiriquí',
                '2' => 'Coclé',
                '3' => 'Colón',
                '5' => 'Darién',
                'EM' => 'Emberá',
                '6' => 'Herrera',
                'KY' => 'Kuna Yala',
                '7' => 'Los Santos',
                'NB' => 'Ngöbe - Buglé',
                '8' => 'Panamá',
                '10' => 'Panamá Oeste',
                '9' => 'Veraguas',
            ),
            'PG' => Array
            (
                'NSB' => 'Bougainville',
                'CPM' => 'Central',
                'CPK' => 'Chimbu',
                'EBR' => 'East New Britain',
                'ESW' => 'East Sepik',
                'EHG' => 'Eastern Highlands',
                'EPW' => 'Enga',
                'GPK' => 'Gulf',
                'MPM' => 'Madang',
                'MRL' => 'Manus',
                'MBA' => 'Milne Bay',
                'MPL' => 'Morobe',
                'NCD' => 'National Capital District',
                'NIK' => 'New Ireland',
                'NPP' => 'Northern',
                'SAN' => 'Sandaun',
                'SHM' => 'Southern Highlands',
                'WBK' => 'West New Britain',
                'WPD' => 'Western',
                'WHM' => 'Western Highlands',
            ),
            'PY' => Array
            (
                '16' => 'Alto Paraguay',
                '10' => 'Alto Paraná',
                '13' => 'Amambay',
                'ASU' => 'Asunción',
                '19' => 'Boquerón',
                '5' => 'Caaguazú',
                '6' => 'Caazapá',
                '14' => 'Canindeyú',
                '11' => 'Central',
                '1' => 'Concepción',
                '3' => 'Cordillera',
                '4' => 'Guairá',
                '7' => 'Itapúa',
                '8' => 'Misiones',
                '9' => 'Paraguarí',
                '15' => 'Presidente Hayes',
                '2' => 'San Pedro',
                '12' => 'Ñeembucú',
            ),
            'PE' => Array
            (
                'AMA' => 'Amazonas',
                'ANC' => 'Ancash',
                'APU' => 'Apurímac',
                'ARE' => 'Arequipa',
                'AYA' => 'Ayacucho',
                'CAJ' => 'Cajamarca',
                'CUS' => 'Cusco',
                'CAL' => 'El Callao',
                'HUV' => 'Huancavelica',
                'HUC' => 'Huánuco',
                'ICA' => 'Ica',
                'JUN' => 'Junín',
                'LAL' => 'La Libertad',
                'LAM' => 'Lambayeque',
                'LIM' => 'Lima',
                'LOR' => 'Loreto',
                'MDD' => 'Madre de Dios',
                'MOQ' => 'Moquegua',
                'LMA' => 'Municipalidad Metropolitana de Lima',
                'PAS' => 'Pasco',
                'PIU' => 'Piura',
                'PUN' => 'Puno',
                'SAM' => 'San Martín',
                'TAC' => 'Tacna',
                'TUM' => 'Tumbes',
                'UCA' => 'Ucayali',
            ),
            'PH' => Array
            (
                '14' => 'Autonomous Region in Muslim Mindanao',
                '05' => 'Bicol',
                '02' => 'Cagayan Valley',
                '40' => 'Calabarzon',
                '13' => 'Caraga',
                '03' => 'Central Luzon',
                '07' => 'Central Visayas',
                '15' => 'Cordillera Administrative Region',
                '11' => 'Davao',
                '08' => 'Eastern Visayas',
                '01' => 'Ilocos',
                '41' => 'Mimaropa',
                '00' => 'National Capital Region',
                '10' => 'Northern Mindanao',
                '12' => 'Soccsksargen',
                '06' => 'Western Visayas',
                '09' => 'Zamboanga Peninsula',
            ),
            'PL' => Array
            (
                'DS' => 'Dolnośląskie',
                'KP' => 'Kujawsko - pomorskie',
                'LU' => 'Lubelskie',
                'LB' => 'Lubuskie',
                'MZ' => 'Mazowieckie',
                'MA' => 'Małopolskie',
                'OP' => 'Opolskie',
                'PK' => 'Podkarpackie',
                'PD' => 'Podlaskie',
                'PM' => 'Pomorskie',
                'WN' => 'Warmińsko - mazurskie',
                'WP' => 'Wielkopolskie',
                'ZP' => 'Zachodniopomorskie',
                'LD' => 'Łódzkie',
                'SL' => 'Śląskie',
                'SK' => 'Świętokrzyskie',
            ),
            'PT' => Array
            (
                '01' => 'Aveiro',
                '02' => 'Beja',
                '03' => 'Braga',
                '04' => 'Bragança',
                '05' => 'Castelo Branco',
                '06' => 'Coimbra',
                '08' => 'Faro',
                '09' => 'Guarda',
                '10' => 'Leiria',
                '11' => 'Lisboa',
                '12' => 'Portalegre',
                '13' => 'Porto',
                '30' => 'Região Autónoma da Madeira',
                '20' => 'Região Autónoma dos Açores',
                '14' => 'Santarém',
                '15' => 'Setúbal',
                '16' => 'Viana do Castelo',
                '17' => 'Vila Real',
                '18' => 'Viseu',
                '07' => 'Évora',
            ),
            'QA' => Array
            (
                'DA' => 'Ad Dawḩah',
                'KH' => 'Al Khawr wa adh Dhakhīrah',
                'WA' => 'Al Wakrah',
                'RA' => 'Ar Rayyān',
                'MS' => 'Ash Shamāl',
                'ZA' => 'Az̧ Za̧`āyin',
                'US' => 'Umm Şalāl',
            ),
            'RO' => Array
            (
                'AB' => 'Alba',
                'AR' => 'Arad',
                'AG' => 'Argeș',
                'BC' => 'Bacău',
                'BH' => 'Bihor',
                'BN' => 'Bistrița - Năsăud',
                'BT' => 'Botoșani',
                'BV' => 'Brașov',
                'BR' => 'Brăila',
                'B' => 'București',
                'BZ' => 'Buzău',
                'CS' => 'Caraș - Severin',
                'CJ' => 'Cluj',
                'CT' => 'Constanța',
                'CV' => 'Covasna',
                'CL' => 'Călărași',
                'DJ' => 'Dolj',
                'DB' => 'Dâmbovița',
                'GL' => 'Galați',
                'GR' => 'Giurgiu',
                'GJ' => 'Gorj',
                'HR' => 'Harghita',
                'HD' => 'Hunedoara',
                'IL' => 'Ialomița',
                'IS' => 'Iași',
                'IF' => 'Ilfov',
                'MM' => 'Maramureș',
                'MH' => 'Mehedinți',
                'MS' => 'Mureș',
                'NT' => 'Neamț',
                'OT' => 'Olt',
                'PH' => 'Prahova',
                'SM' => 'Satu Mare',
                'SB' => 'Sibiu',
                'SV' => 'Suceava',
                'SJ' => 'Sălaj',
                'TR' => 'Teleorman',
                'TM' => 'Timiș',
                'TL' => 'Tulcea',
                'VS' => 'Vaslui',
                'VN' => 'Vrancea',
                'VL' => 'Vâlcea',
            ),
            'RU' => Array
            (
                'AMU' => 'Amurskaya oblast',
                'ARK' => 'Arkhangel skaya oblast',
                'AST' => 'Astrakhanskaya oblast',
                'BEL' => 'Belgorodskaya oblast',
                'BRY' => 'Bryanskaya oblast',
                'CHE' => 'Chelyabinskaya oblast',
                'IRK' => 'Irkutskaya oblast',
                'IVA' => 'Ivanovskaya oblast',
                'KGD' => 'Kaliningradskaya oblast',
                'KLU' => 'Kaluzhskaya oblast',
                'KEM' => 'Kemerovskaya oblast',
                'KIR' => 'Kirovskaya oblast',
                'KOS' => 'Kostromskaya oblast',
                'KGN' => 'Kurganskaya oblast',
                'KRS' => 'Kurskaya oblast',
                'LEN' => 'Leningradskaya oblast',
                'LIP' => 'Lipetskaya oblast',
                'MAG' => 'Magadanskaya oblast',
                'MOS' => 'Moskovskaya oblast',
                'MUR' => 'Murmanskaya oblast',
                'NIZ' => 'Nizhegorodskaya oblast',
                'NGR' => 'Novgorodskaya oblast',
                'NVS' => 'Novosibirskaya oblast',
                'OMS' => 'Omskaya oblast',
                'ORE' => 'Orenburgskaya oblast',
                'ORL' => 'Orlovskaya oblast',
                'PNZ' => 'Penzenskaya oblast',
                'PSK' => 'Pskovskaya oblast',
                'ROS' => 'Rostovskaya oblast',
                'RYA' => 'Ryazanskaya oblast',
                'SAK' => 'Sakhalinskaya oblast',
                'SAM' => 'Samarskaya oblast',
                'SAR' => 'Saratovskaya oblast',
                'SMO' => 'Smolenskaya oblast',
                'SVE' => 'Sverdlovskaya oblast',
                'TAM' => 'Tambovskaya oblast',
                'TOM' => 'Tomskaya oblast',
                'TUL' => 'Tul"skaya oblast',
                'TVE' => 'Tverskaya oblast',
                'TYU' => 'Tyumenskaya oblast',
                'ULY' => 'Ul"yanovskaya oblast',
                'VLA' => 'Vladimirskaya oblast',
                'VGG' => 'Volgogradskaya oblast',
                'VLG' => 'Vologodskaya oblast',
                'VOR' => 'Voronezhskaya oblast',
                'YAR' => 'Yaroslavskaya oblast',
                'ALT' => 'Altayskiy kray',
                'KAM' => 'Kamchatskiy kray',
                'KHA' => 'Khabarovskiy kray',
                'KDA' => 'Krasnodarskiy kray',
                'KYA' => 'Krasnoyarskiy kray',
                'PER' => 'Permskiy kray',
                'PRI' => 'Primorskiy kray',
                'STA' => 'Stavropol"skiy kray',
                'ZAB' => 'Zabaykal"skiy kray',
                'MOW' => 'Moskva',
                'SPE' => 'Sankt - Peterburg',
                'CHU' => 'Chukotskiy avtonomnyy okrug',
                'KHM' => 'Khanty - Mansiyskiy avtonomnyy okrug - Yugra',
                'NEN' => 'Nenetskiy avtonomnyy okrug',
                'YAN' => 'Yamalo - Nenetskiy avtonomnyy okrug',
                'YEV' => 'Yevreyskaya avtonomnaya oblast',
                'AD' => 'Adygeya, Respublika',
                'AL' => 'Altay, Respublika',
                'BA' => 'Bashkortostan, Respublika',
                'BU' => 'Buryatiya, Respublika',
                'CE' => 'Chechenskaya Respublika',
                'CU' => 'Chuvashskaya Respublika',
                'DA' => 'Dagestan, Respublika',
                'IN' => 'Ingushetiya, Respublika',
                'KB' => 'Kabardino-Balkarskaya Respublika',
                'KL' => 'Kalmykiya, Respublika',
                'KC' => 'Karachayevo-Cherkesskaya Respublika',
                'KR' => 'Kareliya, Respublika',
                'KK' => 'Khakasiya, Respublika',
                'KO' => 'Komi, Respublika',
                'ME' => 'Mariy El, Respublika',
                'MO' => 'Mordoviya, Respublika',
                'SA' => 'Sakha, Respublika',
                'SE' => 'Severnaya Osetiya - Alaniya, Respublika',
                'TA' => 'Tatarstan, Respublika',
                'TY' => 'Tyva, Respublika',
                'UD' => 'Udmurtskaya Respublika',
            ),
            'RW' => Array
            (
                '02' => 'Est',
                '03' => 'Nord',
                '04' => 'Ouest',
                '05' => 'Sud',
                '01' => 'Ville de Kigali',
            ),
            'SH' => Array
            (
                'AC' => 'Ascension',
                'HL' => 'Saint Helena',
                'TA' => 'Tristan da Cunha',
            ),
            'KN' => Array
            (
                'N' => 'Nevis',
                'K' => 'Saint Kitts',
            ),
            'LC' => Array
            (
                '01' => 'Anse la Raye',
                '02' => 'Castries',
                '03' => 'Choiseul',
                '04' => 'Dauphin',
                '05' => 'Dennery',
                '06' => 'Gros Islet',
                '07' => 'Laborie',
                '08' => 'Micoud',
                '09' => 'Praslin',
                '10' => 'Soufrière',
                '11' => 'Vieux Fort',
            ),
            'VC' => Array
            (
                '01' => 'Charlotte',
                '06' => 'Grenadines',
                '02' => 'Saint Andrew',
                '03' => 'Saint David',
                '04' => 'Saint George',
                '05' => 'Saint Patrick',
            ),
            'WS' => Array
            (
                'AA' => 'A"ana',
                'AL' => 'Aiga-i-le-Tai',
                'AT' => 'Atua',
                'FA' => 'Fa"asaleleaga',
                'GE' => 'Gaga"emauga',
                'GI' => 'Gagaifomauga',
                'PA' => 'Palauli',
                'SA' => 'Satupa"itea',
                'TU' => 'Tuamasaga',
                'VF' => 'Va"a-o-Fonoti',
                'VS' => 'Vaisigano',
            ),
            'SM' => Array
            (
                '01' => 'Acquaviva',
                '06' => 'Borgo Maggiore',
                '02' => 'Chiesanuova',
                '03' => 'Domagnano',
                '04' => 'Faetano',
                '05' => 'Fiorentino',
                '08' => 'Montegiardino',
                '07' => 'San Marino',
                '09' => 'Serravalle',
            ),
            'ST' => Array
            (
                'P' => 'Príncipe',
                'S' => 'São Tomé',
            ),
            'SA' => Array
            (
                '11' => 'Al Bāḩah',
                '12' => 'Al Jawf',
                '03' => 'Al Madīnah',
                '05' => 'Al Qaşīm',
                '08' => 'Al Ḩudūd ash Shamālīyah',
                '01' => 'Ar Riyāḑ',
                '04' => 'Ash Sharqīyah',
                '09' => 'Jīzān',
                '02' => 'Makkah',
                '10' => 'Najrān',
                '07' => 'Tabūk',
                '14' => 'ٰĀsīr',
                '06' => 'Ḩā"il',
            ),
            'SN' => Array
            (
                'DK' => 'Dakar',
                'DB' => 'Diourbel',
                'FK' => 'Fatick',
                'KA' => 'Kaffrine',
                'KL' => 'Kaolack',
                'KD' => 'Kolda',
                'KE' => 'Kédougou',
                'LG' => 'Louga',
                'MT' => 'Matam',
                'SL' => 'Saint-Louis',
                'SE' => 'Sédhiou',
                'TC' => 'Tambacounda',
                'TH' => 'Thiès',
                'ZG' => 'Ziguinchor',
            ),
            'RS' => Array
            (
                'KM' => 'Kosovo - Metohija',
                'VO' => 'Vojvodina',
            ),
            'SC' => Array
            (
                '02' => 'Anse Boileau',
                '03' => 'Anse Etoile',
                '05' => 'Anse Royale',
                '01' => 'Anse aux Pins',
                '04' => 'Au Cap',
                '06' => 'Baie Lazare',
                '07' => 'Baie Sainte Anne',
                '08' => 'Beau Vallon',
                '09' => 'Bel Air',
                '10' => 'Bel Ombre',
                '11' => 'Cascade',
                '16' => 'English River',
                '12' => 'Glacis',
                '13' => 'Grand Anse Mahe',
                '14' => 'Grand Anse Praslin',
                '15' => 'La Digue',
                '24' => 'Les Mamelles',
                '17' => 'Mont Buxton',
                '18' => 'Mont Fleuri',
                '19' => 'Plaisance',
                '20' => 'Pointe Larue',
                '21' => 'Port Glaud',
                '25' => 'Roche Caiman',
                '22' => 'Saint Louis',
                '23' => 'Takamaka',
            ),
            'SL' => Array
            (
                'E' => 'Eastern',
                'N' => 'Northern',
                'S' => 'Southern',
                'W' => 'Western Area',
            ),
            'SG' => Array
            (
                '01' => 'Central Singapore',
                '02' => 'North East',
                '03' => 'North West',
                '04' => 'South East',
                '05' => 'South West',
            ),
            'SK' => Array
            (
                'BC' => 'Banskobystrický kraj',
                'BL' => 'Bratislavský kraj',
                'KI' => 'Košický kraj',
                'NI' => 'Nitriansky kraj',
                'PV' => 'Prešovský kraj',
                'TC' => 'Trenčiansky kraj',
                'TA' => 'Trnavský kraj',
                'ZI' => 'Žilinský kraj',
            ),
            'SI' => Array
            (
                '001' => 'Ajdovščina',
                '195' => 'Apače',
                '002' => 'Beltinci',
                '148' => 'Benedikt',
                '149' => 'Bistrica ob Sotli',
                '003' => 'Bled',
                '150' => 'Bloke',
                '004' => 'Bohinj',
                '005' => 'Borovnica',
                '006' => 'Bovec',
                '151' => 'Braslovče',
                '007' => 'Brda',
                '008' => 'Brezovica',
                '009' => 'Brežice',
                '152' => 'Cankova',
                '011' => 'Celje',
                '012' => 'Cerklje na Gorenjskem',
                '013' => 'Cerknica',
                '014' => 'Cerkno',
                '153' => 'Cerkvenjak',
                '196' => 'Cirkulane',
                '018' => 'Destrnik',
                '019' => 'Divača',
                '154' => 'Dobje',
                '020' => 'Dobrepolje',
                '155' => 'Dobrna',
                '021' => 'Dobrova–Polhov Gradec',
                '156' => 'Dobrovnik',
                '022' => 'Dol pri Ljubljani',
                '157' => 'Dolenjske Toplice',
                '023' => 'Domžale',
                '024' => 'Dornava',
                '025' => 'Dravograd',
                '026' => 'Duplek',
                '027' => 'Gorenja vas–Poljane',
                '028' => 'Gorišnica',
                '207' => 'Gorje',
                '029' => 'Gornja Radgona',
                '030' => 'Gornji Grad',
                '031' => 'Gornji Petrovci',
                '158' => 'Grad',
                '032' => 'Grosuplje',
                '159' => 'Hajdina',
                '161' => 'Hodoš',
                '162' => 'Horjul',
                '160' => 'Hoče–Slivnica',
                '034' => 'Hrastnik',
                '035' => 'Hrpelje-Kozina',
                '036' => 'Idrija',
                '037' => 'Ig',
                '038' => 'Ilirska Bistrica',
                '039' => 'Ivančna Gorica',
                '040' => 'Izola',
                '041' => 'Jesenice',
                '163' => 'Jezersko',
                '042' => 'Juršinci',
                '043' => 'Kamnik',
                '044' => 'Kanal',
                '045' => 'Kidričevo',
                '046' => 'Kobarid',
                '047' => 'Kobilje',
                '049' => 'Komen',
                '164' => 'Komenda',
                '050' => 'Koper',
                '197' => 'Kosanjevica na Krki',
                '165' => 'Kostel',
                '051' => 'Kozje',
                '048' => 'Kočevje',
                '052' => 'Kranj',
                '053' => 'Kranjska Gora',
                '166' => 'Križevci',
                '054' => 'Krško',
                '055' => 'Kungota',
                '056' => 'Kuzma',
                '057' => 'Laško',
                '058' => 'Lenart',
                '059' => 'Lendava',
                '060' => 'Litija',
                '061' => 'Ljubljana',
                '062' => 'Ljubno',
                '063' => 'Ljutomer',
                '208' => 'Log-Dragomer',
                '064' => 'Logatec',
                '167' => 'Lovrenc na Pohorju',
                '065' => 'Loška Dolina',
                '066' => 'Loški Potok',
                '068' => 'Lukovica',
                '067' => 'Luče',
                '069' => 'Majšperk',
                '198' => 'Makole',
                '070' => 'Maribor',
                '168' => 'Markovci',
                '071' => 'Medvode',
                '072' => 'Mengeš',
                '073' => 'Metlika',
                '074' => 'Mežica',
                '169' => 'Miklavž na Dravskem Polju',
                '075' => 'Miren–Kostanjevica',
                '170' => 'Mirna Peč',
                '076' => 'Mislinja',
                '199' => 'Mokronog–Trebelno',
                '078' => 'Moravske Toplice',
                '077' => 'Moravče',
                '079' => 'Mozirje',
                '080' => 'Murska Sobota',
                '081' => 'Muta',
                '082' => 'Naklo',
                '083' => 'Nazarje',
                '084' => 'Nova Gorica',
                '085' => 'Novo Mesto',
                '086' => 'Odranci',
                '171' => 'Oplotnica',
                '087' => 'Ormož',
                '088' => 'Osilnica',
                '089' => 'Pesnica',
                '090' => 'Piran',
                '091' => 'Pivka',
                '172' => 'Podlehnik',
                '093' => 'Podvelka',
                '092' => 'Podčetrtek',
                '200' => 'Poljčane',
                '173' => 'Polzela',
                '094' => 'Postojna',
                '174' => 'Prebold',
                '095' => 'Preddvor',
                '175' => 'Prevalje',
                '096' => 'Ptuj',
                '097' => 'Puconci',
                '100' => 'Radenci',
                '099' => 'Radeče',
                '101' => 'Radlje ob Dravi',
                '102' => 'Radovljica',
                '103' => 'Ravne na Koroškem',
                '176' => 'Razkrižje',
                '098' => 'Rače–Fram',
                '201' => 'Renče-Vogrsko',
                '209' => 'Rečica ob Savinji',
                '104' => 'Ribnica',
                '177' => 'Ribnica na Pohorju',
                '107' => 'Rogatec',
                '106' => 'Rogaška Slatina',
                '105' => 'Rogašovci',
                '108' => 'Ruše',
                '178' => 'Selnica ob Dravi',
                '109' => 'Semič',
                '110' => 'Sevnica',
                '111' => 'Sežana',
                '112' => 'Slovenj Gradec',
                '113' => 'Slovenska Bistrica',
                '114' => 'Slovenske Konjice',
                '179' => 'Sodražica',
                '180' => 'Solčava',
                '202' => 'Središče ob Dravi',
                '115' => 'Starše',
                '203' => 'Straža',
                '181' => 'Sveta Ana',
                '204' => 'Sveta Trojica v Slovenskih Goricah',
                '182' => 'Sveti Andraž v Slovenskih Goricah',
                '116' => 'Sveti Jurij',
                '210' => 'Sveti Jurij v Slovenskih Goricah',
                '205' => 'Sveti Tomaž',
                '184' => 'Tabor',
                '010' => 'Tišina',
                '128' => 'Tolmin',
                '129' => 'Trbovlje',
                '130' => 'Trebnje',
                '185' => 'Trnovska Vas',
                '186' => 'Trzin',
                '131' => 'Tržič',
                '132' => 'Turnišče',
                '133' => 'Velenje',
                '187' => 'Velika Polana',
                '134' => 'Velike Lašče',
                '188' => 'Veržej',
                '135' => 'Videm',
                '136' => 'Vipava',
                '137' => 'Vitanje',
                '138' => 'Vodice',
                '139' => 'Vojnik',
                '189' => 'Vransko',
                '140' => 'Vrhnika',
                '141' => 'Vuzenica',
                '142' => 'Zagorje ob Savi',
                '143' => 'Zavrč',
                '144' => 'Zreče',
                '015' => 'Črenšovci',
                '016' => 'Črna na Koroškem',
                '017' => 'Črnomelj',
                '033' => 'Šalovci',
                '183' => 'Šempeter–Vrtojba',
                '118' => 'Šentilj',
                '119' => 'Šentjernej',
                '120' => 'Šentjur',
                '211' => 'Šentrupert',
                '117' => 'Šenčur',
                '121' => 'Škocjan',
                '122' => 'Škofja Loka',
                '123' => 'Škofljica',
                '124' => 'Šmarje pri Jelšah',
                '206' => 'Šmarješke Toplice',
                '125' => 'Šmartno ob Paki',
                '194' => 'Šmartno pri Litiji',
                '126' => 'Šoštanj',
                '127' => 'Štore',
                '190' => 'Žalec',
                '146' => 'Železniki',
                '191' => 'Žetale',
                '147' => 'Žiri',
                '192' => 'Žirovnica',
                '193' => 'Žužemberk',
            ),
            'SB' => Array
            (
                'CT' => 'Capital Territory',
                'CE' => 'Central',
                'CH' => 'Choiseul',
                'GU' => 'Guadalcanal',
                'IS' => 'Isabel',
                'MK' => 'Makira - Ulawa',
                'ML' => 'Malaita',
                'RB' => 'Rennell and Bellona',
                'TE' => 'Temotu',
                'WE' => 'Western',
            ),
            'SO' => Array
            (
                'AW' => 'Awdal',
                'BK' => 'Bakool',
                'BN' => 'Banaadir',
                'BR' => 'Bari',
                'BY' => 'Bay',
                'GA' => 'Galguduud',
                'GE' => 'Gedo',
                'HI' => 'Hiiraan',
                'JD' => 'Jubbada Dhexe',
                'JH' => 'Jubbada Hoose',
                'MU' => 'Mudug',
                'NU' => 'Nugaal',
                'SA' => 'Sanaag',
                'SD' => 'Shabeellaha Dhexe',
                'SH' => 'Shabeellaha Hoose',
                'SO' => 'Sool',
                'TO' => 'Togdheer',
                'WO' => 'Woqooyi Galbeed',
            ),
            'ZA' => Array
            (
                'EC' => 'Eastern Cape',
                'FS' => 'Free State',
                'GT' => 'Gauteng',
                'NL' => 'KwaZulu - Natal',
                'LP' => 'Limpopo',
                'MP' => 'Mpumalanga',
                'NW' => 'North West',
                'NC' => 'Northern Cape',
                'WC' => 'Western Cape',
            ),
            'KR' => Array
            (
                '26' => 'Busan - gwangyeoksi',
                '43' => 'Chungcheongbuk -do',
                '44' => 'Chungcheongnam -do',
                '27' => 'Daegu - gwangyeoksi',
                '30' => 'Daejeon - gwangyeoksi',
                '42' => 'Gangwon -do',
                '29' => 'Gwangju - gwangyeoksi',
                '41' => 'Gyeonggi -do',
                '47' => 'Gyeongsangbuk -do',
                '48' => 'Gyeongsangnam -do',
                '28' => 'Incheon - gwangyeoksi',
                '49' => 'Jeju - teukbyeoljachido',
                '45' => 'Jeollabuk -do',
                '46' => 'Jeollanam -do',
                '50' => 'Sejong',
                '11' => 'Seoul - teukbyeolsi',
                '31' => 'Ulsan - gwangyeoksi',
            ),
            'SS' => Array
            (
                'EC' => 'Central Equatoria',
                'EE' => 'Eastern Equatoria',
                'JG' => 'Jonglei',
                'LK' => 'Lakes',
                'BN' => 'Northern Bahr el Ghazal',
                'UY' => 'Unity',
                'NU' => 'Upper Nile',
                'WR' => 'Warrap',
                'BW' => 'Western Bahr el Ghazal',
                'EW' => 'Western Equatoria',
            ),
            'ES' => Array
            (
                'C' => 'A Coruña',
                'AB' => 'Albacete',
                'A' => 'Alicante',
                'AL' => 'Almería',
                'O' => 'Asturias',
                'BA' => 'Badajoz',
                'PM' => 'Balears',
                'B' => 'Barcelona',
                'BU' => 'Burgos',
                'S' => 'Cantabria',
                'CS' => 'Castellón',
                'CR' => 'Ciudad Real',
                'CU' => 'Cuenca',
                'CC' => 'Cáceres',
                'CA' => 'Cádiz',
                'CO' => 'Córdoba',
                'GI' => 'Girona',
                'GR' => 'Granada',
                'GU' => 'Guadalajara',
                'SS' => 'Guipúzcoa',
                'H' => 'Huelva',
                'HU' => 'Huesca',
                'J' => 'Jaén',
                'LO' => 'La Rioja',
                'GC' => 'Las Palmas',
                'LE' => 'León',
                'L' => 'Lleida',
                'LU' => 'Lugo',
                'M' => 'Madrid',
                'MU' => 'Murcia',
                'MA' => 'Málaga',
                'NA' => 'Navarra',
                ' OR ' => 'Ourense',
                'P' => 'Palencia',
                'PO' => 'Pontevedra',
                'SA' => 'Salamanca',
                'TF' => 'Santa Cruz de Tenerife',
                'SG' => 'Segovia',
                'SE' => 'Sevilla',
                'SO' => 'Soria',
                'T' => 'Tarragona',
                'TE' => 'Teruel',
                'TO' => 'Toledo',
                'V' => 'Valencia',
                'VA' => 'Valladolid',
                'BI' => 'Vizcaya',
                'ZA' => 'Zamora',
                'Z' => 'Zaragoza',
                'VI' => 'Álava',
                'AV' => 'Ávila',
                'CE' => 'Ceuta',
                'ML' => 'Melilla',
                'AN' => 'Andalucía',
                'AR' => 'Aragón',
                'AS' => 'Asturias, Principado de',
                'CN' => 'Canarias',
                'CB' => 'Cantabria',
                'CL' => 'Castilla y León',
                'CM' => 'Castilla - La Mancha',
                'CT' => 'Catalunya',
                'EX' => 'Extremadura',
                'GA' => 'Galicia',
                'IB' => 'Illes Balears',
                'RI' => 'La Rioja',
                'MD' => 'Madrid, Comunidad de',
                'MC' => 'Murcia, Región de',
                'NC' => 'Navarra, Comunidad Foral de',
                'PV' => 'País Vasco',
                'VC' => 'Valenciana, Comunidad',
            ),
            'LK' => Array
            (
                '2' => 'Central Province',
                '5' => 'Eastern Province',
                '7' => 'North Central Province',
                '6' => 'North Western Province',
                '4' => 'Northern Province',
                '9' => 'Sabaragamuwa Province',
                '3' => 'Southern Province',
                '8' => 'Uva Province',
                '1' => 'Western Province',
            ),
            'SD' => Array
            (
                'RS' => 'Al Baḩr al Aḩmar',
                'GZ' => 'Al Jazīrah',
                'KH' => 'Al Kharţūm',
                'GD' => 'Al Qaḑārif',
                'NR' => 'An Nīl',
                'NW' => 'An Nīl al Abyaḑ',
                'NB' => 'An Nīl al Azraq',
                'NO' => 'Ash Shamālīyah',
                'DW' => 'Gharb Dārfūr',
                'DS' => 'Janūb Dārfūr',
                'KS' => 'Janūb Kurdufān',
                'KA' => 'Kassalā',
                'DN' => 'Shamāl Dārfūr',
                'KN' => 'Shamāl Kurdufān',
                'DE' => 'Sharq Dārfūr',
                'SI' => 'Sinnār',
                'DC' => 'Zalingei',
            ),
            'SR' => Array
            (
                'BR' => 'Brokopondo',
                'CM' => 'Commewijne',
                'CR' => 'Coronie',
                'MA' => 'Marowijne',
                'NI' => 'Nickerie',
                'PR' => 'Para',
                'PM' => 'Paramaribo',
                'SA' => 'Saramacca',
                'SI' => 'Sipaliwini',
                'WA' => 'Wanica',
            ),
            'SZ' => Array
            (
                'HH' => 'Hhohho',
                'LU' => 'Lubombo',
                'MA' => 'Manzini',
                'SH' => 'Shiselweni',
            ),
            'SE' => Array
            (
                'K' => 'Blekinge län',
                'W' => 'Dalarnas län',
                'I' => 'Gotlands län',
                'X' => 'Gävleborgs län',
                'N' => 'Hallands län',
                'Z' => 'Jämtlands län',
                'F' => 'Jönköpings län',
                'H' => 'Kalmar län',
                'G' => 'Kronobergs län',
                'BD' => 'Norrbottens län',
                'M' => 'Skåne län',
                'AB' => 'Stockholms län',
                'D' => 'Södermanlands län',
                'C' => 'Uppsala län',
                'S' => 'Värmlands län',
                'AC' => 'Västerbottens län',
                'Y' => 'Västernorrlands län',
                'U' => 'Västmanlands län',
                'O' => 'Västra Götalands län',
                'T' => 'Örebro län',
                'E' => 'Östergötlands län',
            ),
            'CH' => Array
            (
                'AG' => 'Aargau',
                'AR' => 'Appenzell Ausserrhoden',
                'AI' => 'Appenzell Innerrhoden',
                'BL' => 'Basel - Landschaft',
                'BS' => 'Basel - Stadt',
                'BE' => 'Bern',
                'FR' => 'Fribourg',
                'GE' => 'Genève',
                'GL' => 'Glarus',
                'GR' => 'Graubünden',
                'JU' => 'Jura',
                'LU' => 'Luzern',
                'NE' => 'Neuchâtel',
                'NW' => 'Nidwalden',
                'OW' => 'Obwalden',
                'SG' => 'Sankt Gallen',
                'SH' => 'Schaffhausen',
                'SZ' => 'Schwyz',
                'SO' => 'Solothurn',
                'TG' => 'Thurgau',
                'TI' => 'Ticino',
                'UR' => 'Uri',
                'VS' => 'Valais',
                'VD' => 'Vaud',
                'ZG' => 'Zug',
                'ZH' => 'Zürich',
            ),
            'SY' => Array
            (
                'LA' => 'Al Lādhiqīyah',
                'QU' => 'Al Qunayţirah',
                'HA' => 'Al Ḩasakah',
                'RA' => 'Ar Raqqah',
                'SU' => 'As Suwaydā',
                'DR' => 'Darٰā',
                'DY' => 'Dayr az Zawr',
                'DI' => 'Dimashq',
                'ID' => 'Idlib',
                'RD' => 'Rīf Dimashq',
                'TA' => 'Ţarţūs',
                'HL' => 'Ḩalab',
                'HM' => 'Ḩamāh',
                'HI' => 'Ḩimş',
            ),
            'TW' => Array
            (
                'CHA' => 'Changhua',
                'CYQ' => 'Chiayi',
                'CYI' => 'Chiayi',
                'HSZ' => 'Hsinchu',
                'HSQ' => 'Hsinchu',
                'HUA' => 'Hualien',
                'ILA' => 'Ilan',
                'KHQ' => 'Kaohsiung',
                'KHH' => 'Kaohsiung',
                'KEE' => 'Keelung',
                'MIA' => 'Miaoli',
                'NAN' => 'Nantou',
                'PEN' => 'Penghu',
                'PIF' => 'Pingtung',
                'TXG' => 'Taichung',
                'TXQ' => 'Taichung',
                'TNN' => 'Tainan',
                'TNQ' => 'Tainan',
                'TPE' => 'Taipei',
                'TPQ' => 'Taipei',
                'TTT' => 'Taitung',
                'TAO' => 'Taoyuan',
                'YUN' => 'Yunlin',
            ),
            'TJ' => Array
            (
                'DU' => 'Dushanbe',
                'KT' => 'Khatlon',
                'GB' => 'Kŭhistoni Badakhshon',
                'SU' => 'Sughd',
            ),
            'TZ' => Array
            (
                '01' => 'Arusha',
                '02' => 'Dar es Salaam',
                '03' => 'Dodoma',
                '04' => 'Iringa',
                '05' => 'Kagera',
                '06' => 'Kaskazini Pemba',
                '07' => 'Kaskazini Unguja',
                '08' => 'Kigoma',
                '09' => 'Kilimanjaro',
                '10' => 'Kusini Pemba',
                '11' => 'Kusini Unguja',
                '12' => 'Lindi',
                '26' => 'Manyara',
                '13' => 'Mara',
                '14' => 'Mbeya',
                '15' => 'Mjini Magharibi',
                '16' => 'Morogoro',
                '17' => 'Mtwara',
                '18' => 'Mwanza',
                '19' => 'Pwani',
                '20' => 'Rukwa',
                '21' => 'Ruvuma',
                '22' => 'Shinyanga',
                '23' => 'Singida',
                '24' => 'Tabora',
                '25' => 'Tanga',
            ),
            'TH' => Array
            (
                '37' => 'Amnat Charoen',
                '15' => 'Ang Thong',
                '38' => 'Bueng Kan',
                '31' => 'Buri Ram',
                '24' => 'Chachoengsao',
                '18' => 'Chai Nat',
                '36' => 'Chaiyaphum',
                '22' => 'Chanthaburi',
                '50' => 'Chiang Mai',
                '57' => 'Chiang Rai',
                '20' => 'Chon Buri',
                '86' => 'Chumphon',
                '46' => 'Kalasin',
                '62' => 'Kamphaeng Phet',
                '71' => 'Kanchanaburi',
                '40' => 'Khon Kaen',
                '81' => 'Krabi',
                '10' => 'Krung Thep Maha Nakhon',
                '52' => 'Lampang',
                '51' => 'Lamphun',
                '42' => 'Loei',
                '16' => 'Lop Buri',
                '58' => 'Mae Hong Son',
                '44' => 'Maha Sarakham',
                '49' => 'Mukdahan',
                '26' => 'Nakhon Nayok',
                '73' => 'Nakhon Pathom',
                '48' => 'Nakhon Phanom',
                '30' => 'Nakhon Ratchasima',
                '60' => 'Nakhon Sawan',
                '80' => 'Nakhon Si Thammarat',
                '55' => 'Nan',
                '96' => 'Narathiwat',
                '39' => 'Nong Bua Lam Phu',
                '43' => 'Nong Khai',
                '12' => 'Nonthaburi',
                '13' => 'Pathum Thani',
                '94' => 'Pattani',
                '82' => 'Phangnga',
                '93' => 'Phatthalung',
                'S' => 'Phatthaya',
                '56' => 'Phayao',
                '67' => 'Phetchabun',
                '76' => 'Phetchaburi',
                '66' => 'Phichit',
                '65' => 'Phitsanulok',
                '14' => 'Phra Nakhon Si Ayutthaya',
                '54' => 'Phrae',
                '83' => 'Phuket',
                '25' => 'Prachin Buri',
                '77' => 'Prachuap Khiri Khan',
                '85' => 'Ranong',
                '70' => 'Ratchaburi',
                '21' => 'Rayong',
                '45' => 'Roi Et',
                '27' => 'Sa Kaeo',
                '47' => 'Sakon Nakhon',
                '11' => 'Samut Prakan',
                '74' => 'Samut Sakhon',
                '75' => 'Samut Songkhram',
                '19' => 'Saraburi',
                '91' => 'Satun',
                '33' => 'Si Sa Ket',
                '17' => 'Sing Buri',
                '90' => 'Songkhla',
                '64' => 'Sukhothai',
                '72' => 'Suphan Buri',
                '84' => 'Surat Thani',
                '32' => 'Surin',
                '63' => 'Tak',
                '92' => 'Trang',
                '23' => 'Trat',
                '34' => 'Ubon Ratchathani',
                '41' => 'Udon Thani',
                '61' => 'Uthai Thani',
                '53' => 'Uttaradit',
                '95' => 'Yala',
                '35' => 'Yasothon',
            ),
            'TL' => Array
            (
                'AL' => 'Aileu',
                'AN' => 'Ainaro',
                'BA' => 'Baucau',
                'BO' => 'Bobonaro',
                'CO' => 'Cova Lima',
                'DI' => 'Díli',
                'ER' => 'Ermera',
                'LA' => 'Lautem',
                'LI' => 'Liquiça',
                'MT' => 'Manatuto',
                'MF' => 'Manufahi',
                'OE' => 'Oecussi',
                'VI' => 'Viqueque',
            ),
            'TG' => Array
            (
                'C' => 'Centre',
                'K' => 'Kara',
                'M' => 'Maritime',
                'P' => 'Plateaux',
                'S' => 'Savannes',
            ),
            'TO' => Array
            (
                '01' => 'Eua',
                '02' => 'Ha"apai',
                '03' => 'Niuas',
                '04' => 'Tongatapu',
                '05' => 'Vava"u',
            ),
            'TT' => Array
            (
                'ARI' => 'Arima',
                'CHA' => 'Chaguanas',
                'CTT' => 'Couva - Tabaquite - Talparo',
                'DMN' => 'Diego Martin',
                'ETO' => 'Eastern Tobago',
                'PED' => 'Penal - Debe',
                'PTF' => 'Point Fortin',
                'POS' => 'Port of Spain',
                'PRT' => 'Princes Town',
                'RCM' => 'Rio Claro - Mayaro',
                'SFO' => 'San Fernando',
                'SJL' => 'San Juan - Laventille',
                'SGE' => 'Sangre Grande',
                'SIP' => 'Siparia',
                'TUP' => 'Tunapuna - Piarco',
                'WTO' => 'Western Tobago',
            ),
            'TN' => Array
            (
                '12' => 'Ariana',
                '13' => 'Ben Arous',
                '23' => 'Bizerte',
                '31' => 'Béja',
                '81' => 'Gabès',
                '71' => 'Gafsa',
                '32' => 'Jendouba',
                '41' => 'Kairouan',
                '42' => 'Kasserine',
                '73' => 'Kebili',
                '14' => 'La ,Manouba',
                '33' => 'Le ,Kef',
                '53' => 'Mahdia',
                '82' => 'Medenine',
                '52' => 'Monastir',
                '21' => 'Nabeul',
                '61' => 'Sfax',
                '43' => 'Sidi Bouzid',
                '34' => 'Siliana',
                '51' => 'Sousse',
                '83' => 'Tataouine',
                '72' => 'Tozeur',
                '11' => 'Tunis',
                '22' => 'Zaghouan',
            ),
            'TR' => Array
            (
                '01' => 'Adana',
                '02' => 'Adıyaman',
                '03' => 'Afyonkarahisar',
                '68' => 'Aksaray',
                '05' => 'Amasya',
                '06' => 'Ankara',
                '07' => 'Antalya',
                '75' => 'Ardahan',
                '08' => 'Artvin',
                '09' => 'Aydın',
                '04' => 'Ağrı',
                '10' => 'Balıkesir',
                '74' => 'Bartın',
                '72' => 'Batman',
                '69' => 'Bayburt',
                '11' => 'Bilecik',
                '12' => 'Bingöl',
                '13' => 'Bitlis',
                '14' => 'Bolu',
                '15' => 'Burdur',
                '16' => 'Bursa',
                '20' => 'Denizli',
                '21' => 'Diyarbakır',
                '81' => 'Düzce',
                '22' => 'Edirne',
                '23' => 'Elazığ',
                '24' => 'Erzincan',
                '25' => 'Erzurum',
                '26' => 'Eskişehir',
                '27' => 'Gaziantep',
                '28' => 'Giresun',
                '29' => 'Gümüşhane',
                '30' => 'Hakkâri',
                '31' => 'Hatay',
                '32' => 'Isparta',
                '76' => 'Iğdır',
                '46' => 'Kahramanmaraş',
                '78' => 'Karabük',
                '70' => 'Karaman',
                '36' => 'Kars',
                '37' => 'Kastamonu',
                '38' => 'Kayseri',
                '79' => 'Kilis',
                '41' => 'Kocaeli',
                '42' => 'Konya',
                '43' => 'Kütahya',
                '39' => 'Kırklareli',
                '71' => 'Kırıkkale',
                '40' => 'Kırşehir',
                '44' => 'Malatya',
                '45' => 'Manisa',
                '47' => 'Mardin',
                '33' => 'Mersin',
                '48' => 'Muğla',
                '49' => 'Muş',
                '50' => 'Nevşehir',
                '51' => 'Niğde',
                '52' => 'Ordu',
                '80' => 'Osmaniye',
                '53' => 'Rize',
                '54' => 'Sakarya',
                '55' => 'Samsun',
                '56' => 'Siirt',
                '57' => 'Sinop',
                '58' => 'Sivas',
                '59' => 'Tekirdağ',
                '60' => 'Tokat',
                '61' => 'Trabzon',
                '62' => 'Tunceli',
                '64' => 'Uşak',
                '65' => 'Van',
                '77' => 'Yalova',
                '66' => 'Yozgat',
                '67' => 'Zonguldak',
                '17' => 'Çanakkale',
                '18' => 'Çankırı',
                '19' => 'Çorum',
                '34' => 'İstanbul',
                '35' => 'İzmir',
                '63' => 'Şanlıurfa',
                '73' => 'Şırnak',
            ),
            'TM' => Array
            (
                'A' => 'Ahal',
                'S' => 'Aşgabat',
                'B' => 'Balkan',
                'D' => 'Daşoguz',
                'L' => 'Lebap',
                'M' => 'Mary',
            ),
            'TV' => Array
            (
                'FUN' => 'Funafuti',
                'NMG' => 'Nanumanga',
                'NMA' => 'Nanumea',
                'NIT' => 'Niutao',
                'NUI' => 'Nui',
                'NKF' => 'Nukufetau',
                'NKL' => 'Nukulaelae',
                'VAI' => 'Vaitupu',
            ),
            'UG' => Array
            (
                'C' => 'Central',
                'E' => 'Eastern',
                'N' => 'Northern',
                'W' => 'Western',
            ),
            'UA' => Array
            (
                '43' => 'Avtonomna Respublika Krym',
                '71' => 'Cherkas"ka Oblast',
                '74' => 'Chernihivs"ka Oblast',
                '77' => 'Chernivets"ka Oblast',
                '12' => 'Dnipropetrovs"ka Oblast',
                '14' => 'Donets"ka Oblast',
                '26' => 'Ivano - Frankivs"ka Oblast',
                '63' => 'Kharkivs"ka Oblast',
                '65' => 'Khersons"ka Oblast',
                '68' => 'Khmel"nyts"ka Oblast',
                '35' => 'Kirovohrads"ka Oblast',
                '30' => 'Kyïv',
                '32' => 'Kyïvs"ka Oblast',
                '46' => 'L"vivs"ka Oblast',
                '09' => 'Luhans"ka Oblast',
                '48' => 'Mykolaïvs"ka Oblast',
                '51' => 'Odes"ka Oblast',
                '53' => 'Poltavs"ka Oblast',
                '56' => 'Rivnens"ka Oblast',
                '40' => 'Sevastopol',
                '59' => 'Sums"ka Oblast',
                '61' => 'Ternopil"s"ka Oblast',
                '05' => 'Vinnyts"ka Oblast',
                '07' => 'Volyns"ka Oblast',
                '21' => 'Zakarpats"ka Oblast',
                '23' => 'Zaporiz"ka Oblast',
                '18' => 'Zhytomyrs"ka Oblast',
            ),
            'AE' => Array
            (
                'AJ' => 'Ajmān',
                'AZ' => 'Abū Z̧aby',
                'FU' => 'Al Fujayrah',
                'SH' => 'Ash Shāriqah',
                'DU' => 'Dubayy',
                'RK' => 'Ra"s al Khaymah',
                'UQ' => 'Umm al Qaywayn',
            ),
            'GB' => Array
            (
                'BDG' => 'Barking and Dagenham',
                'BNE' => 'Barnet',
                'BEX' => 'Bexley',
                'BEN' => 'Brent',
                'BRY' => 'Bromley',
                'CMD' => 'Camden',
                'CRY' => 'Croydon',
                'EAL' => 'Ealing',
                'ENF' => 'Enfield',
                'GRE' => 'Greenwich',
                'HCK' => 'Hackney',
                'HMF' => 'Hammersmith and Fulham',
                'HRY' => 'Haringey',
                'HRW' => 'Harrow',
                'HAV' => 'Havering',
                'HIL' => 'Hillingdon',
                'HNS' => 'Hounslow',
                'ISL' => 'Islington',
                'KEC' => 'Kensington and Chelsea',
                'KTT' => 'Kingston upon Thames',
                'LBH' => 'Lambeth',
                'LEW' => 'Lewisham',
                'MRT' => 'Merton',
                'NWM' => 'Newham',
                'RDB' => 'Redbridge',
                'RIC' => 'Richmond upon Thames',
                'SWK' => 'Southwark',
                'STN' => 'Sutton',
                'TWH' => 'Tower Hamlets',
                'WFT' => 'Waltham Forest',
                'WND' => 'Wandsworth',
                'WSM' => 'Westminster',
                'EAW' => 'England and Wales',
                'GBN' => 'Great Britain',
                'UKM' => 'United Kingdom',
                'LND' => 'London, City of',
                'ABE' => 'Aberdeen City',
                'ABD' => 'Aberdeenshire',
                'ANS' => 'Angus',
                'AGB' => 'Argyll and Bute',
                'CLK' => 'Clackmannanshire',
                'DGY' => 'Dumfries and Galloway',
                'DND' => 'Dundee City',
                'EAY' => 'East Ayrshire',
                'EDU' => 'East Dunbartonshire',
                'ELN' => 'East Lothian',
                'ERW' => 'East Renfrewshire',
                'EDH' => 'Edinburgh, City of',
                'ELS' => 'Eilean Siar',
                'FAL' => 'Falkirk',
                'FIF' => 'Fife',
                'GLG' => 'Glasgow City',
                'HLD' => 'Highland',
                'IVC' => 'Inverclyde',
                'MLN' => 'Midlothian',
                'MRY' => 'Moray',
                'NAY' => 'North Ayrshire',
                'NLK' => 'North Lanarkshire',
                'ORK' => 'Orkney Islands',
                'PKN' => 'Perth and Kinross',
                'RFW' => 'Renfrewshire',
                'SCB' => 'Scottish Borders, The',
                'ZET' => 'Shetland Islands',
                'SAY' => 'South Ayrshire',
                'SLK' => 'South Lanarkshire',
                'STG' => 'Stirling',
                'WDU' => 'West Dunbartonshire',
                'WLN' => 'West Lothian',
                'ENG' => 'England',
                'SCT' => 'Scotland',
                'WLS' => 'Wales',
                'ANT' => 'Antrim',
                'ARD' => 'Ards',
                'ARM' => 'Armagh',
                'BLA' => 'Ballymena',
                'BLY' => 'Ballymoney',
                'BNB' => 'Banbridge',
                'BFS' => 'Belfast',
                'CKF' => 'Carrickfergus',
                'CSR' => 'Castlereagh',
                'CLR' => 'Coleraine',
                'CKT' => 'Cookstown',
                'CGV' => 'Craigavon',
                'DRY' => 'Derry',
                'DOW' => 'Down',
                'DGN' => 'Dungannon and South Tyrone',
                'FER' => 'Fermanagh',
                'LRN' => 'Larne',
                'LMV' => 'Limavady',
                'LSB' => 'Lisburn',
                'MFT' => 'Magherafelt',
                'MYL' => 'Moyle',
                'NYM' => 'Newry and Mourne District',
                'NTA' => 'Newtownabbey',
                'NDN' => 'North Down',
                'OMH' => 'Omagh',
                'STB' => 'Strabane',
                'BNS' => 'Barnsley',
                'BIR' => 'Birmingham',
                'BOL' => 'Bolton',
                'BRD' => 'Bradford',
                'BUR' => 'Bury',
                'CLD' => 'Calderdale',
                'COV' => 'Coventry',
                'DNC' => 'Doncaster',
                'DUD' => 'Dudley',
                'GAT' => 'Gateshead',
                'KIR' => 'Kirklees',
                'KWL' => 'Knowsley',
                'LDS' => 'Leeds',
                'LIV' => 'Liverpool',
                'MAN' => 'Manchester',
                'NET' => 'Newcastle upon Tyne',
                'NTY' => 'North Tyneside',
                'OLD' => 'Oldham',
                'RCH' => 'Rochdale',
                'ROT' => 'Rotherham',
                'SLF' => 'Salford',
                'SAW' => 'Sandwell',
                'SFT' => 'Sefton',
                'SHF' => 'Sheffield',
                'SOL' => 'Solihull',
                'STY' => 'South Tyneside',
                'SHN' => 'St . Helens',
                'SKP' => 'Stockport',
                'SND' => 'Sunderland',
                'TAM' => 'Tameside',
                'TRF' => 'Trafford',
                'WKF' => 'Wakefield',
                'WLL' => 'Walsall',
                'WGN' => 'Wigan',
                'WRL' => 'Wirral',
                'WLV' => 'Wolverhampton',
                'NIR' => 'Northern Ireland',
                'BKM' => 'Buckinghamshire',
                'CAM' => 'Cambridgeshire',
                'CMA' => 'Cumbria',
                'DBY' => 'Derbyshire',
                'DEV' => 'Devon',
                'DOR' => 'Dorset',
                'ESX' => 'East Sussex',
                'ESS' => 'Essex',
                'GLS' => 'Gloucestershire',
                'HAM' => 'Hampshire',
                'HRT' => 'Hertfordshire',
                'KEN' => 'Kent',
                'LAN' => 'Lancashire',
                'LEC' => 'Leicestershire',
                'LIN' => 'Lincolnshire',
                'NFK' => 'Norfolk',
                'NYK' => 'North Yorkshire',
                'NTH' => 'Northamptonshire',
                'NTT' => 'Nottinghamshire',
                'OXF' => 'Oxfordshire',
                'SOM' => 'Somerset',
                'STS' => 'Staffordshire',
                'SFK' => 'Suffolk',
                'SRY' => 'Surrey',
                'WAR' => 'Warwickshire',
                'WSX' => 'West Sussex',
                'WOR' => 'Worcestershire',
                'BAS' => 'Bath and North East Somerset',
                'BDF' => 'Bedford',
                'BBD' => 'Blackburn with Darwen',
                'BPL' => 'Blackpool',
                'BGW' => 'Blaenau Gwent',
                'BMH' => 'Bournemouth',
                'BRC' => 'Bracknell Forest',
                'BGE' => 'Bridgend',
                'BNH' => 'Brighton and Hove',
                'BST' => 'Bristol, City of',
                'CAY' => 'Caerphilly',
                'CRF' => 'Cardiff',
                'CMN' => 'Carmarthenshire',
                'CBF' => 'Central Bedfordshire',
                'CGN' => 'Ceredigion',
                'CHE' => 'Cheshire East',
                'CHW' => 'Cheshire West and Chester',
                'CWY' => 'Conwy',
                'CON' => 'Cornwall',
                'DAL' => 'Darlington',
                'DEN' => 'Denbighshire',
                'DER' => 'Derby',
                'DUR' => 'Durham, County',
                'ERY' => 'East Riding of Yorkshire',
                'FLN' => 'Flintshire',
                'GWN' => 'Gwynedd',
                'HAL' => 'Halton',
                'HPL' => 'Hartlepool',
                'HEF' => 'Herefordshire',
                'AGY' => 'Isle of Anglesey',
                'IOW' => 'Isle of Wight',
                'IOS' => 'Isles of Scilly',
                'KHL' => 'Kingston upon Hull',
                'LCE' => 'Leicester',
                'LUT' => 'Luton',
                'MDW' => 'Medway',
                'MTY' => 'Merthyr Tydfil',
                'MDB' => 'Middlesbrough',
                'MIK' => 'Milton Keynes',
                'MON' => 'Monmouthshire',
                'NTL' => 'Neath Port Talbot',
                'NWP' => 'Newport',
                'NEL' => 'North East Lincolnshire',
                'NLN' => 'North Lincolnshire',
                'NSM' => 'North Somerset',
                'NBL' => 'Northumberland',
                'NGM' => 'Nottingham',
                'PEM' => 'Pembrokeshire',
                'PTE' => 'Peterborough',
                'PLY' => 'Plymouth',
                'POL' => 'Poole',
                'POR' => 'Portsmouth',
                'POW' => 'Powys',
                'RDG' => 'Reading',
                'RCC' => 'Redcar and Cleveland',
                'RCT' => 'Rhondda, Cynon, Taff',
                'RUT' => 'Rutland',
                'SHR' => 'Shropshire',
                'SLG' => 'Slough',
                'SGC' => 'South Gloucestershire',
                'STH' => 'Southampton',
                'SOS' => 'Southend - on - Sea',
                'STT' => 'Stockton - on - Tees',
                'STE' => 'Stoke - on - Trent',
                'SWA' => 'Swansea',
                'SWD' => 'Swindon',
                'TFW' => 'Telford and Wrekin',
                'THR' => 'Thurrock',
                'TOB' => 'Torbay',
                'TOF' => 'Torfaen',
                'VGL' => 'Vale of Glamorgan, The',
                'WRT' => 'Warrington',
                'WBK' => 'West Berkshire',
                'WIL' => 'Wiltshire',
                'WNM' => 'Windsor and Maidenhead',
                'WOK' => 'Wokingham',
                'WRX' => 'Wrexham',
                'YOR' => 'York',
            ),
            'US' => Array
            (
                'DC' => 'District of Columbia',
                'AS' => 'American Samoa',
                'GU' => 'Guam',
                'MP' => 'Northern Mariana Islands',
                'PR' => 'Puerto Rico',
                'UM' => 'United States Minor Outlying Islands',
                'VI' => 'Virgin Islands, U . S .',
                'AL' => 'Alabama',
                'AK' => 'Alaska',
                'AZ' => 'Arizona',
                'AR' => 'Arkansas',
                'CA' => 'California',
                'CO' => 'Colorado',
                'CT' => 'Connecticut',
                'DE' => 'Delaware',
                'FL' => 'Florida',
                'GA' => 'Georgia',
                'HI' => 'Hawaii',
                'ID' => 'Idaho',
                'IL' => 'Illinois',
                'IN' => 'Indiana',
                'IA' => 'Iowa',
                'KS' => 'Kansas',
                'KY' => 'Kentucky',
                'LA' => 'Louisiana',
                'ME' => 'Maine',
                'MD' => 'Maryland',
                'MA' => 'Massachusetts',
                'MI' => 'Michigan',
                'MN' => 'Minnesota',
                'MS' => 'Mississippi',
                'MO' => 'Missouri',
                'MT' => 'Montana',
                'NE' => 'Nebraska',
                'NV' => 'Nevada',
                'NH' => 'New Hampshire',
                'NJ' => 'New Jersey',
                'NM' => 'New Mexico',
                'NY' => 'New York',
                'NC' => 'North Carolina',
                'ND' => 'North Dakota',
                'OH' => 'Ohio',
                'OK' => 'Oklahoma',
                ' OR ' => 'Oregon',
                'PA' => 'Pennsylvania',
                'RI' => 'Rhode Island',
                'SC' => 'South Carolina',
                'SD' => 'South Dakota',
                'TN' => 'Tennessee',
                'TX' => 'Texas',
                'UT' => 'Utah',
                'VT' => 'Vermont',
                'VA' => 'Virginia',
                'WA' => 'Washington',
                'WV' => 'West Virginia',
                'WI' => 'Wisconsin',
                'WY' => 'Wyoming',
            ),
            'UY' => Array
            (
                'AR' => 'Artigas',
                'CA' => 'Canelones',
                'CL' => 'Cerro Largo',
                'CO' => 'Colonia',
                'DU' => 'Durazno',
                'FS' => 'Flores',
                'FD' => 'Florida',
                'LA' => 'Lavalleja',
                'MA' => 'Maldonado',
                'MO' => 'Montevideo',
                'PA' => 'Paysandú',
                'RV' => 'Rivera',
                'RO' => 'Rocha',
                'RN' => 'Río Negro',
                'SA' => 'Salto',
                'SJ' => 'San José',
                'SO' => 'Soriano',
                'TA' => 'Tacuarembó',
                'TT' => 'Treinta y Tres',
            ),
            'UZ' => Array
            (
                'AN' => 'Andijon',
                'BU' => 'Buxoro',
                'FA' => 'Farg‘ona',
                'JI' => 'Jizzax',
                'NG' => 'Namangan',
                'NW' => 'Navoiy',
                'QA' => 'Qashqadaryo',
                'QR' => 'Qoraqalpog‘iston Respublikasi',
                'SA' => 'Samarqand',
                'SI' => 'Sirdaryo',
                'SU' => 'Surxondaryo',
                'TO' => 'Toshkent',
                'TK' => 'Toshkent',
                'XO' => 'Xorazm',
            ),
            'VU' => Array
            (
                'MAP' => 'Malampa',
                'PAM' => 'Pénama',
                'SAM' => 'Sanma',
                'SEE' => 'Shéfa',
                'TAE' => 'Taféa',
                'TOB' => 'Torba',
            ),
            'VE' => Array
            (
                'Z' => 'Amazonas',
                'B' => 'Anzoátegui',
                'C' => 'Apure',
                'D' => 'Aragua',
                'E' => 'Barinas',
                'F' => 'Bolívar',
                'G' => 'Carabobo',
                'H' => 'Cojedes',
                'Y' => 'Delta Amacuro',
                'W' => 'Dependencias Federales',
                'A' => 'Distrito Capital',
                'I' => 'Falcón',
                'J' => 'Guárico',
                'K' => 'Lara',
                'M' => 'Miranda',
                'N' => 'Monagas',
                'L' => 'Mérida',
                'O' => 'Nueva Esparta',
                'P' => 'Portuguesa',
                'R' => 'Sucre',
                'T' => 'Trujillo',
                'S' => 'Táchira',
                'X' => 'Vargas',
                'U' => 'Yaracuy',
                'V' => 'Zulia',
            ),
            'VN' => Array
            (
                '44' => 'An Giang',
                '43' => 'Bà Rịa–Vũng Tàu',
                '57' => 'Bình Dương',
                '58' => 'Bình Phước',
                '40' => 'Bình Thuận',
                '31' => 'Bình Định',
                '55' => 'Bạc Liêu',
                '54' => 'Bắc Giang',
                '53' => 'Bắc Kạn',
                '56' => 'Bắc Ninh',
                '50' => 'Bến Tre',
                '04' => 'Cao Bằng',
                '59' => 'Cà Mau',
                'CT' => 'Cần Thơ',
                '30' => 'Gia Lai',
                '03' => 'Hà Giang',
                '63' => 'Hà Nam',
                'HN' => 'Hà Nội',
                '15' => 'Hà Tây',
                '23' => 'Hà Tĩnh',
                '14' => 'Hòa Bình',
                '66' => 'Hưng Yên',
                '61' => 'Hải Dương',
                'HP' => 'Hải Phòng',
                '73' => 'Hậu Giang',
                'SG' => 'Hồ Chí Minh',
                '34' => 'Khánh Hòa',
                '47' => 'Kiên Giang',
                '28' => 'Kon Tum',
                '01' => 'Lai Châu',
                '41' => 'Long An',
                '02' => 'Lào Cai',
                '35' => 'Lâm Đồng',
                '09' => 'Lạng Sơn',
                '67' => 'Nam Định',
                '22' => 'Nghệ An',
                '18' => 'Ninh Bình',
                '36' => 'Ninh Thuận',
                '68' => 'Phú Thọ',
                '32' => 'Phú Yên',
                '24' => 'Quảng Bình',
                '27' => 'Quảng Nam',
                '29' => 'Quảng Ngãi',
                '13' => 'Quảng Ninh',
                '25' => 'Quảng Trị',
                '52' => 'Sóc Trăng',
                '05' => 'Sơn La',
                '21' => 'Thanh Hóa',
                '20' => 'Thái Bình',
                '69' => 'Thái Nguyên',
                '26' => 'Thừa Thiên–Huế',
                '46' => 'Tiền Giang',
                '51' => 'Trà Vinh',
                '07' => 'Tuyên Quang',
                '37' => 'Tây Ninh',
                '49' => 'Vĩnh Long',
                '70' => 'Vĩnh Phúc',
                '06' => 'Yên Bái',
                '71' => 'Điện Biên',
                'DN' => 'Đà Nẵng',
                '33' => 'Đắk Lắk',
                '72' => 'Đắk Nông',
                '39' => 'Đồng Nai',
                '45' => 'Đồng Tháp',
            ),
            'YE' => Array
            (
                'AD' => 'Adan',
                'AM' => 'Amrān',
                'AB' => 'Abyān',
                'BA' => 'Al Bayḑā',
                'JA' => 'Al Jawf',
                'MR' => 'Al Mahrah',
                'MW' => 'Al Maḩwīt',
                'HU' => 'Al Ḩudaydah',
                'DA' => 'Aḑ Ḑāli',
                'DH' => 'Dhamār',
                'IB' => 'Ibb',
                'LA' => 'Laḩij',
                'MA' => 'Ma"rib',
                'RA' => 'Raymah',
                'SH' => 'Shabwah',
                'TA' => 'Tā‘izz',
                'SA' => 'Şan‘ā',
                'SN' => 'Şan‘ā',
                'SD' => 'Şā‘dah',
                'HJ' => 'Ḩajjah',
                'HD' => 'Ḩaḑramawt',
            ),
            'ZM' => Array
            (
                '02' => 'Central',
                '08' => 'Copperbelt',
                '03' => 'Eastern',
                '04' => 'Luapula',
                '09' => 'Lusaka',
                '06' => 'North - Western',
                '05' => 'Northern',
                '07' => 'Southern',
                '01' => 'Western',
            ),
            'ZW' => Array
            (
                'BU' => 'Bulawayo',
                'HA' => 'Harare',
                'MA' => 'Manicaland',
                'MC' => 'Mashonaland Central',
                'ME' => 'Mashonaland East',
                'MW' => 'Mashonaland West',
                'MV' => 'Masvingo',
                'MN' => 'Matabeleland North',
                'MS' => 'Matabeleland South',
                'MI' => 'Midlands',
            )
        );

        $returnData = array();
        if ($country_code != null) {
            if (array_key_exists($country_code, $data)) {
                $returnData = $data[$country_code];
                return $returnData;
            }
        }

    }
}

/**
 * get city list on state code
 * @param  $state_code
 * @return \Illuminate\Http\Response
 */

if (!function_exists('get_city_list')) {
    function get_city_list($state_code = null)
    {
        $data = array(
            "TX" => array(
                "HOU" => "Houston",
                "DA" => "DALLAS",
            ),
            "CA" => array(
                "SFO" => "San Francisco",
                "SAN" => "San Diego",
            ),
            "IL" => array(
                "CHI" => "chicago",
            ),
            "MA" => array(
                "BOS" => "Boston",
            ),
            "MI" => array(
                "DDT" => "Detroit",
            ),
            "GA" => array(
                "ATL" => "Atlanta",
            ),
            "CO" => array(
                "DEN" => "Denver",
            ),
            "DC" => array(
                "WAS" => "Washington",
            ),
            "NY" => array(
                "SYR" => "Syracuse",
            ),
            "WA" => array(
                "SEA" => "SEATTLE",
            ),
        );

        $returnData = array();
        if ($state_code != null) {
            if (array_key_exists($state_code, $data)) {
                $returnData = $data[$state_code];
                return $returnData;
            }
        }
    }
}
/*-- get pagination left side--*/
if(!function_exists('get_pagination_view')){
    function get_pagination_view($pagination) {
        $option = '';
        $option .= '<option value="1-20">01-20</option>';
        $option .= '<option value="1-40">01-40</option>';
        $option .= '<option value="1-60">01-60</option>';
        $option .= '<option value="1-80">01-80</option>';
        $option .= '<option value="1-100">01-100</option>';

//        if(!empty($pagination)){
//            foreach($pagination as $key => $value){
//                $offsetValue = explode('-', $value);
//                if($key == 1)$offset = $offsetValue[0];
//                else $offset = $offsetValue[0] + 1;
//
//                $offset2 = $offsetValue[1];
//                $mainoffset = $offset.'-'.$offset2;
//
//                $option .= '<option value="'.$mainoffset.'">'.$value.'</option>';
//            }
//        }
       return $option;
    }
}

if(!function_exists('get_pagination_right_view')){
    function get_pagination_right_view($pagination, $data_count,$pagination_slot,$pagi) {
        $option = '';
        if(!empty($pagination)) {
            $numberRecordsTotal = ceil($data_count/$pagination_slot);
            $option .= '<div class="pagination">';
//            $option .= '<button class="previous btn btn-sm btn-light1 text-dark py-0 px-1 mx-1" data-toggle="tooltip" data-placement="right" title="Prev Page">
//                            <i class="mdi mdi-chevron-left "></i>
//
            $option .= '<span class="previous mx-1" id="pagination-prev" data-toggle="tooltip" data-placement="right" title="" data-original-title="Prev Page" aria-describedby="tooltip450295">
                            <i class="mdi mdi-chevron-left "></i>
                        </span>';

            foreach ($pagination as $key => $value) {
                if ($key == 1 && empty($pagi)) {
                    $active = 'btn-primary';
                } elseif($pagi == $key) {
                    $active = 'btn-primary';
                } else{
                    $active = '';
                }
                if($key == 1 || $key== $numberRecordsTotal || $pagi == $key)
//                    $option .= '<button class="btn btn-sm '.$active.' py-0 px-1 mx-1 page-no get_range_'.$value.' page_changed change_page'.$key.'" data-val="'.$key.'">'.$key.'</button>';
                    $option .= '<span class="mx-1 '.$active.' get_range_'.$value.' page_changed change_page'.$key.'" data-val="'.$key.'">'.$key.'</span>';
            }
//            $option .= '<button class="next btn btn-sm btn-light1 text-dark py-0 px-1 mx-1" data-toggle="tooltip" data-placement="right" title="Next Page">
//                                        <i class="mdi mdi-chevron-right"></i>
//                                    </button>';
            $option .= '<span class="mx-1 next" id="pagination-next" data-toggle="tooltip" data-placement="right" title="" data-original-title="Next Page">
                                        <i class="mdi mdi-chevron-right"></i>
                                    </span>';
            $option .= '<input type="hidden" id="total_records" value="' . $numberRecordsTotal .'">';
            $option .= '</div>';
        }
        return $option;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lead_time_check  extends Model
{
    //
    protected $table = 'lead_time_check';

    protected $fillable = [
        'lead_time_type'
    ];

}

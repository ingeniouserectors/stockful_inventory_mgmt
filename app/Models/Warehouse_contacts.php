<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Warehouse_contacts extends Model
{
    //
    use SoftDeletes;
    protected $table = 'warehouse_contacts';

    protected $fillable = [
        'warehouse_id',
        'first_name',
        'last_name',
        'title',
        'email',
        'mobile',
        'primary',
    ];

    /**
     * Get the user marketplace that owns the warhouse.
     *
     * @param  no-params
     *
     */


}

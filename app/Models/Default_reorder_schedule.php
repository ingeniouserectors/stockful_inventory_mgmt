<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Default_reorder_schedule extends Model
{
    //
    protected $table = 'default_reorder_schedule';

    protected $fillable = [
        'reorder_schedule_type_id',
        'schedule_days'
    ];
    /**
     * Get the user marketplace that owns the cron log..
     *
     * @param  no-params
     *
     */
}

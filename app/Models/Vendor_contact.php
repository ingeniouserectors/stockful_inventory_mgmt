<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vendor_contact extends Model
{
    use SoftDeletes;
    protected $table = 'vendor_contacts';

    protected $fillable = [
        'vendor_id',
        'first_name',
        'last_name',
        'email',
        'phone_number',
        'title',
        'primary'
    ];

    /**
     * Get the vendor that owns the contact.
     *
     * @param  no-params
     *
     */
    public function vendor(){
        return $this->belongsTo('App\Models\Vendor', 'vendor_id');
    }
}

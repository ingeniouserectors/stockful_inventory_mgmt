<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Warehouse_wise_default_setting extends Model
{
    //
    protected $table = 'warehouse_wise_default_settings';

    protected $fillable = [
        'warehouse_id',
        'lead_time'
    ];
    /**
     * Get the warehouse that owns the warhouse wise default setting.
     *
     * @param  no-params
     *
     */
    public function warehouse(){
        return $this->belongsTo('App\Models\Warehouse','warehouse_id');
    }
    /**
     * Get the warehouse default setting that owns the warhouse wise default setting.
     *
     * @param  no-params
     *
     */
    public function warehouse_default_setting(){
        return $this->belongsTo('App\Models\Warehouse_default_setting','warehouse_id');
    }
}

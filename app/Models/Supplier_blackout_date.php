<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Supplier_blackout_date extends Model
{
    //
    protected $table = 'supplier_blackout_dates';

    protected $fillable = [
        'supplier_id',
        'event_name',
        'start_date',
        'end_date',
        'number_of_days',
        'type',
    ];

    /**
     * Get the supplier that owns the blackout dates.
     *
     * @param  no-params
     *
     */
    public function supplier(){
        return $this->belongsTo('App\Models\Supplier', 'supplier_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vendor_default_setting extends Model
{
    //
    protected $table = 'vendor_default_settings';

    protected $fillable = [
        'lead_time',
        'order_volume',
        'quantity_discount',
        'moq',
        'shipping',
        'ship_to_warehouse',
        'cbm_per_container'
    ];
    /**
     * Get the vendor wise default setting for the vendor default setting.
     *
     * @param  no-params
     *
     */
    public function vendor_wise_default_setting(){
        return $this->hasOne('App\Models\Vendor_wise_default_setting','vendor_id');
    }
}

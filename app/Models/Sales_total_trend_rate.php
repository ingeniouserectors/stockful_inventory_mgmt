<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sales_total_trend_rate extends Model
{
    protected $fillable = ['user_marketplace_id','prod_id','month_year','sales_total',
        'day_rate','trend_percentage'];

    protected $table = 'sales_total_trend_rate';

    /**
     * Get the user marketplace that owns the sales total trend rate.
     *
     * @param  no-params
     *
     */
    public function usermarketplace()
    {
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
    /**
     * Get the product that owns the sales total trend rate.
     *
     * @param  no-params
     *
     */
    public function mws_product()
    {
        return $this->belongsTo('App\Models\Mws_product', 'product_id');
    }

}
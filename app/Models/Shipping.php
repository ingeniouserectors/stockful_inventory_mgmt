<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shipping extends Model
{
    use SoftDeletes;
    protected $table = 'shipping';

    protected $fillable = [
        'supplier_vendor',
        'supplier_vendor_id',
        'shipping_type',
        'logistic_type_id',
        'shipping_agent_id',
        'lcl_cost_settings',
        'lcl_cost_per',
        'lcl_cost',
        'duties_cost',
        'direct_to_amazon',
        'shipping_carrier_id',
        'tracking_provided'
    ];

    /**
     * Get the user marketplace that owns the shipping agent.
     *
     * @param  no-params
     *
     */
    public function supplier(){
        return $this->belongsTo('App\Models\supplier', 'id');
    }

    public function vendor(){
        return $this->belongsTo('App\Models\vendor', 'id');
    }

    public function shipping_agent(){
        return $this->hasOne('App\Models\ShippingAgent','id','shipping_agent_id');
    }

    public function shipping_carrier(){
        return $this->hasOne('App\Models\Shippingcarrier','id','shipping_carrier_id');
    }

    public function shippingair(){
        return $this->hasMany('App\Models\shippingair','shipping_id');
    }

    public function shippingcontainersettings(){
        return $this->hasMany('App\Models\shippingcontainersettings','shipping_id');
    }

    public function shiptowarehouse(){
        return $this->hasMany('App\Models\shiptowarehouse','shipping_id');
    }

    public function shipdeliverydetail(){
        return $this->hasMany('App\Models\shippingdeliverydetails','shipping_id');
    }

    public function shippingcutoffdeliverytime(){
        return $this->hasMany('App\Models\shippingcutoffdeliverytime','shipping_id');
    }
}

<?php

namespace App\Models;

use App\Classes\helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use ClouSale\AmazonSellingPartnerAPI\Configuration;
use App\Jobs\Jobspapi;
use ClouSale\AmazonSellingPartnerAPI\Helpers\SellingPartnerApiRequest;
use ClouSale\AmazonSellingPartnerAPI\Models\Catalog\GetCatalogItemResponse;

class Mws_product extends Model
{
    //
    protected $table = 'mws_products';
    protected $fillable = [
        'user_marketplace_id',
        'sku',
        'fnsku',
        'asin',
        'upc',
        'prod_name',
        'prod_image',
        'prod_group',
        'brand',
        'fulfilledby',
        'haslocalinventory',
        'your_price',
        'sales_price',
        'longestside',
        'medianside',
        'shortestside',
        'lengthandgridth',
        'unitofdimensions',
        'itempackweight',
        'unitofweight',
        'productsizeweightband',
        'currency',
    ];
    /**
     * Get the user marketplace that owns the product..
     *
     * @param  no-params
     *
     */


    public function usermarketplace()
    {
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
    /**
     * Get the afn inventory for the product.
     *
     * @param  no-params
     *
     */
    public function mws_afn_inventory(){
        return $this->hasOne('App\Models\Mws_afn_inventory_data','product_id');
    }
    /**
     * Get the vendor for the product.
     *
     * @param  no-params
     *
     */
    public function vendor_product()
    {
        return $this->hasMany('App\Models\Vendor_product', 'product_id');
    }
    /**
     * Get the supplier for the product.
     *
     * @param  no-params
     *
     */
    public function supplier_product()
    {
        return $this->hasMany('App\Models\Supplier_product', 'product_id');
    }
    /**
     * Get the calculated sales quantity for the product.
     *
     * @param  no-params
     *
     */
    public function calculated_sales_qty()
    {
        return $this->hasMany('App\Models\Calculated_sales_qty', 'product_id');
    }
    /**
     * Get the daily logic calculations for the product.
     *
     * @param  no-params
     *
     */
    public function daily_logic_calculations()
    {
        return $this->hasMany('App\Models\Daily_logic_Calculations', 'prod_id');
    }
    /**
     * Get the inbound shipment items for the product.
     *
     * @param  no-params
     *
     */
    public function inbound_shipment_items()
    {
        return $this->hasMany('App\Models\Inbound_shipment_items', 'product_id');
    }
    /**
     * Get the excess inventory for the product.
     *
     * @param  no-params
     *
     */
    public function mws_excess_inventory_data()
    {
        return $this->hasMany('App\Models\Mws_excess_inventory_data', 'product_id');
    }
    /**
     * Get the adjustment inventory for the product.
     *
     * @param  no-params
     *
     */
    public function mws_adjustment_inventory_datas()
    {
        return $this->hasMany('App\Models\Mws_adjustment_inventory_data', 'product_id');
    }
    /**
     * Get the reserved inventory for the product.
     *
     * @param  no-params
     *
     */
    public function mws_reserved_inventory()
    {
        return $this->hasOne('App\Models\Mws_reserved_inventory', 'product_id');
    }
    /**
     * Get the inventory details for the product.
     *
     * @param  no-params
     *
     */
    public function mws_inventory_details()
    {
        return $this->hasMany('App\Models\Mws_inventory_details', 'product_id');
    }
    /**
     * Get the orders for the product.
     *
     * @param  no-params
     *
     */
    public function mws_orders()
    {
        return $this->hasMany('App\Models\Mws_order', 'product_id');
    }
    /**
     * Get the order returns for the product.
     *
     * @param  no-params
     *
     */
    public function mws_order_returns()
    {
        return $this->hasMany('App\Models\Mws_order_returns', 'product_id')->orderBy('return_date','DESC');
    }
    /**
     * Get the out of stock details for the product.
     *
     * @param  no-params
     *
     */
    public function mws_out_of_stock_detail()
    {
        return $this->hasMany('App\Models\Mws_out_of_stock_detail', 'product_id');
    }
    /**
     * Get the calculated inventory daily for the product.
     *
     * @param  no-params
     *
     */
    public function mws_calculated_inventory_daily()
    {
        return $this->hasMany('App\Models\Mws_calculated_inventory_daily', 'product_id');
    }
    /**
     * Get the warehouse wise inventory for the product.
     *
     * @param  no-params
     *
     */
    public function mws_warehouse_wise_inventory()
    {
        return $this->hasMany('App\Models\Mws_warehouse_wise_inventory', 'product_id');
    }
    /**
     * Get the warehouse for the product.
     *
     * @param  no-params
     *
     */
    public function warehouse_product()
    {
        return $this->hasMany('App\Models\Warehouse_product', 'product_id');
    }
    /**
     * Get the sales total trend rate for the product.
     *
     * @param  no-params
     *
     */
    public function sales_total_trend_rate(){
        return $this->hasMany('App\Models\Sales_total_trend_rate','user_marketplace_id');
    }
    /**
     * Get the purchase order detail for the product.
     *
     * @param  no-params
     *
     */
    public function purchase_order_details(){
        $this->belongsTo('App\Models\Purchase_order_details','product_id');
    }
    /**
     * Get the unsuppressed inventory for the product.
     *
     * @param  no-params
     *
     */
    public function mws_unsuppressed_inventory_data(){
        return $this->hasOne('App\Models\Mws_unsuppressed_inventory_data','product_id');
    }
    /**
     * Get the supply logic for the product.
     *
     * @param  no-params
     *
     */
    public function product_assign_supply_logic()
    {
        return $this->hasMany('App\Models\Product_assign_supply_logic', 'user_marketplace_id');
    }
    /**
     * Logic for process reports..
     *
     * @param  amazon_report
     *         report_stream
     *         marketplace
     *
     */
    protected $markatplace;
    public static function getConfiguration($options)
    {
        $config = \ClouSale\AmazonSellingPartnerAPI\Configuration::getDefaultConfiguration();
        $config->setHost(\ClouSale\AmazonSellingPartnerAPI\SellingPartnerEndpoint::$NORTH_AMERICA);
        $config->setRegion(\ClouSale\AmazonSellingPartnerAPI\SellingPartnerRegion::$NORTH_AMERICA);
        Mws_product::initRequest($config, $options);
        return $config;
    }

    public static function initRequest($config, $options){
        $assumedRole = Mws_product::getAssumeRole($options);
        $accessToken = Mws_product::getAccessKeys($options);
        $config->setAccessToken($accessToken);
        $config->setAccessKey($assumedRole->getAccessKeyId());
        $config->setSecretKey($assumedRole->getSecretAccessKey());
        $config->setSecurityToken($assumedRole->getSessionToken());
    }

    public  function getSPAPIReport($config){
        return new ReportsApi($config);
    }

    public   function getReportSepc(){
        return new CreateReportSpecification();
    }

    public static function getAccessKeys($options){
        $accessToken = $options->access_token;
        try {
            if (empty($options->access_token)) {
                $accessToken = \ClouSale\AmazonSellingPartnerAPI\SellingPartnerOAuth::getAccessTokenFromRefreshToken(
                    $options->refresh_token,
                    $options->client_id,
                    $options->client_secret
                );
                if (!empty($accessToken)) {
                    $options->access_token = $accessToken;
                    $options->expire_time = date('Y-m-d H:i:s');
                    $options->save();
                }
            }
            if (!empty($options->expire_time)) {
                $diff = strtotime(date('Y-m-d H:i:s')) - strtotime($options->expire_time);
                if ($diff >= 3500) {
                    $accessToken = \ClouSale\AmazonSellingPartnerAPI\SellingPartnerOAuth::getAccessTokenFromRefreshToken(
                        $options->refresh_token,
                        $options->client_id,
                        $options->client_secret
                    );
                    if (!empty($accessToken)) {
                        $options->access_token = $accessToken;
                        $options->expire_time = date('Y-m-d H:i:s');
                        $options->save();
                    }
                }
            }
            return $accessToken;
        } catch (\Exception $ex) {
            Log::error("Can't Generate Access Token : " . $ex->getMessage());
//            $this->log_it($ex->getTraceAsString());
            throw new \Exception($ex->getMessage());
        }
    }

    public static function getAssumeRole($options){
        try {
            return \ClouSale\AmazonSellingPartnerAPI\AssumeRole::assume(
                \ClouSale\AmazonSellingPartnerAPI\SellingPartnerRegion:: $NORTH_AMERICA,
                $options['access_key'],
                $options['secret_key'],
                $options['role_arn']
            );
        } catch (\Exception $ex) {
            Log::error("Can't Generate Assume role : " . $ex->getMessage());
//            $this->log_it($ex->getTraceAsString());
            throw new \Exception($ex->getMessage());
        }
    }

    public static function processReport(Amazon_requestlog &$amazon_report, $report_stream, Usermarketplace $markatplace)
    {
        $first = false;
        $numHeaders = 0;

        Log::debug("Started Reading MWS Product report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        while (!feof($report_stream)) {
//            Log::debug("Reading a row from report {$amazon_report->id}");

            $row = fgetcsv($report_stream, 10000, "\t");
            if (!$first) {
                $first = true;
                $headers = $row;
                $numHeaders = count($headers);
                continue;
            }
            $mailForward = 0;
            $storageStatus = 'status unknown';
            $mwsProduct = '';
            if (!empty($row)) {
                // truncate row data if item count exceeds number of headers
                if (count($row) > $numHeaders) {
                    $row = array_slice($row, 0, $numHeaders);
                }

                $row = array_combine($headers, $row);
                $prodName = utf8_encode($row['product-name']);
                $mwsProduct = Mws_product::where('sku', $row['sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                if ($mwsProduct) {
                    $storageStatus = 'updated';
                    $fetchImage = new helper();
                    $productData = $fetchImage->getProductImage($row['asin'], $markatplace);
                    if($mwsProduct->prod_name=='' || $mwsProduct->prod_name=='null' || $mwsProduct->prod_name==NULL){
                        $prodName = utf8_encode($productData['title']);
                        Log::debug("MWS Products $prodName: {$mwsProduct->id}");
                    }
                } else {
                    $storageStatus = 'created';
                    $fetchImage = new helper();
                    $productData = $fetchImage->getProductImage($row['asin'], $markatplace);
                    $imageURL = str_replace('SL75','UL1448',$productData['imageURL']);
                    $mwsProduct = new Mws_product();
                    $mwsProduct->prod_image = ($imageURL ? $imageURL[0] : '');
                    if($mwsProduct->prod_name=='' || $mwsProduct->prod_name=='null' || $mwsProduct->prod_name==NULL){
                        $prodName = utf8_encode($productData['title']);
                        Log::debug("MWS Products $prodName: ");
                    }
                }
                $mwsProduct->user_marketplace_id = $amazon_report->user_marketplace_id;
                $mwsProduct->sku = $row['sku'];
                $mwsProduct->fnsku = $row['fnsku'];
                $mwsProduct->asin = $row['asin'];
                $mwsProduct->prod_name = $prodName;
                $mwsProduct->prod_group = $row['product-group'];
                $mwsProduct->brand = $row['brand'];
                $mwsProduct->fulfilledby = $row['fulfilled-by'];
                $mwsProduct->haslocalinventory = (isset($row['has-local-inventory']) ? $row['has-local-inventory'] : '');
                $mwsProduct->your_price = $row['your-price'];
                $mwsProduct->sales_price = $row['sales-price'];
                $mwsProduct->longestside = $row['longest-side'];
                $mwsProduct->medianside = $row['median-side'];
                $mwsProduct->shortestside = $row['shortest-side'];
                $mwsProduct->lengthandgridth = $row['length-and-girth'];
                $mwsProduct->unitofdimensions = $row['unit-of-dimension'];
                $mwsProduct->itempackweight = $row['item-package-weight'];
                $mwsProduct->unitofweight = $row['unit-of-weight'];
                $mwsProduct->productsizeweightband = (isset($row['product-size-weight-band']) ? $row['product-size-weight-band'] : '');
                $mwsProduct->save();
            }
        }
        Log::debug("Ended Reading MWS Product report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        $amazon_report->processed = 1;
        $amazon_report->save();
    }

    public static function processSPAPIReport(Amazon_requestlog &$amazon_report, $report_stream, SPApiUserMarketplace $markatplace)
    {
        $requestConfiguration = Mws_product::getConfiguration($markatplace);

        //dd($markatplace->market_place_developer->marketplace_id);
        $finance = new \ClouSale\AmazonSellingPartnerAPI\Api\CatalogApi($requestConfiguration);
        // $shipmentData = $finance->getCatalogItem($markatplace->market_place_developer->marketplace_id,'B077ZKDQLV');
        // $payload = $shipmentData->getPayload();
        // $get_attribute = $payload->getAttributeSets();
        // dd($get_attribute[0]["small_image"]['url']);

        $numHeaders = 0;
        Log::debug("Started Reading MWS Product report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        if (!empty($report_stream)) {
            $headers = array();
            $tmp = explode("\n", $report_stream);
            foreach ($tmp as $lineCount => $finaldata) {
                if (!empty($finaldata)) {
                    $row = explode("\t", $finaldata);
                   // print_r($row);
                    if ($lineCount == 0) {
                        $headers = $row;
                        $numHeaders = count($headers);
                    }
                    if ($lineCount > 0) {
                        if (count($row) > $numHeaders) {
                            $row = array_slice($row, 0, $numHeaders);
                        }
                        $row = array_combine($headers, $row);
                        echo "<pre />";
                        print_r($row);

                        $shipmentData = $finance->getCatalogItem($markatplace->market_place_developer->marketplace_id,$row['asin']);
                        $payload = $shipmentData->getPayload();
                        $get_attribute = $payload->getAttributeSets();
                        $product_title = @$get_attribute[0]["title"];
                        $product_image = @$get_attribute[0]["small_image"]['url'];

                        $prodName = utf8_encode($row['product-name']);
                        $mwsProduct = Mws_product::where('sku', $row['sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                        if ($mwsProduct) {
                            $shipmentData = $finance->getCatalogItem($markatplace->market_place_developer->marketplace_id,$row['asin']);
                            $storageStatus = 'updated';
                            if($mwsProduct->prod_name=='' || $mwsProduct->prod_name=='null' || $mwsProduct->prod_name==NULL){
                                $prodName = utf8_encode($product_title);
                                Log::debug("MWS Products $prodName: {$mwsProduct->id}");
                            }
                        } else {
                            $storageStatus = 'created';
                             $mwsProduct = new Mws_product();
                             $mwsProduct->prod_image = ($product_image ? $product_image : '');
                                if($mwsProduct->prod_name=='' || $mwsProduct->prod_name=='null' || $mwsProduct->prod_name==NULL){
                                    $prodName = utf8_encode($product_title);
                                    Log::debug("MWS Products $prodName: ");
                                }
                        }
                        $mwsProduct->user_marketplace_id = $amazon_report->user_marketplace_id;
                        $mwsProduct->sku = $row['sku'];
                        $mwsProduct->fnsku = $row['fnsku'];
                        $mwsProduct->asin = $row['asin'];
                        $mwsProduct->prod_name = $prodName;
                        $mwsProduct->prod_group = $row['product-group'];
                        $mwsProduct->brand = $row['brand'];
                        $mwsProduct->fulfilledby = $row['fulfilled-by'];
                        $mwsProduct->haslocalinventory = (isset($row['has-local-inventory']) ? $row['has-local-inventory'] : '');
                        $mwsProduct->your_price = $row['your-price'];
                        $mwsProduct->sales_price = $row['sales-price'];
                        $mwsProduct->longestside = $row['longest-side'];
                        $mwsProduct->medianside = $row['median-side'];
                        $mwsProduct->shortestside = $row['shortest-side'];
                        $mwsProduct->lengthandgridth = $row['length-and-girth'];
                        $mwsProduct->unitofdimensions = $row['unit-of-dimension'];
                        $mwsProduct->itempackweight = $row['item-package-weight'];
                        $mwsProduct->unitofweight = $row['unit-of-weight'];
                        $mwsProduct->productsizeweightband = (isset($row['product-size-weight-band']) ? $row['product-size-weight-band'] : '');
                        $mwsProduct->save();
                    }
                }
            }
        }
        Log::debug("Ended Reading MWS Product report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        $amazon_report->processed = 1;
        $amazon_report->save();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shippingcarrier extends Model
{
//    use SoftDeletes;
    protected $table = 'shipping_carrier';

    protected $fillable = [
        'user_marketplace_id',
        'carrier_name'
    ];

    /**
     * Get the user marketplace that owns the shipping agent.
     *
     * @param  no-params
     *
     */
    public function usermarketplace(){
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
}

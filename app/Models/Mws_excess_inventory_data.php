<?php

namespace App\Models;

use App\Classes\helper;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class Mws_excess_inventory_data extends Model
{
    //
    protected $table = 'mws_excess_inventory_data';

    protected $fillable = [
        'user_marketplace_id', 'product_id', 'request_date', 'msku', 'fnsku', 'asin',
        'product_name', 'condition', 'fulfilled_by', 'total_qty_sellable', 'estimated_excess',
        'days_of_supply', 'excess_threshold', 'units_sold_last_7_days', 'units_sold_last_30_days', 'units_sold_last_60_days',
        'units_sold_last_90_days', 'alert', 'your_price', 'lowest_price_including_shipping', 'buybox_price',
        'recommended_action', 'healty_inventory_level', 'recommended_sales_price', 'recommended_sales_duration_days', 'estimated_total_storage_cost',
        'recommended_removal_qty', 'estimated_cost_saving_of_removal'
    ];
    /**
     * Get the user marketplace that owns the excess inventory..
     *
     * @param  no-params
     *
     */
    public function usermarketplace()
    {
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
    /**
     * Get the product that owns the excess inventory..
     *
     * @param  no-params
     *
     */
    public function mws_product()
    {
        return $this->belongsTo('App\Models\Mws_product', 'product_id');
    }
    /**
     * Logic for process reports..
     *
     * @param  amazon_report
     *         report_stream
     *         marketplace
     *
     */
    public static function processReport(Amazon_requestlog &$amazon_report, $report_stream, Usermarketplace $markatplace)
    {
        $first = false;
        $numHeaders = 0;

        while (!feof($report_stream)) {

            Log::debug("Started Reading MWS Excess Inventory report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
            $row = fgetcsv($report_stream, 10000, "\t");
            if (!$first) {
                $first = true;
                $headers = $row;
                $numHeaders = count($headers);
                continue;
            }
            if (!empty($row)) {
                // truncate row data if item count exceeds number of headers
                if (count($row) > $numHeaders) {
                    $row = array_slice($row, 0, $numHeaders);
                }

                $row = array_combine($headers, $row);
                $productId = Mws_product::where('sku', $row['MSKU'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                if (empty($productId)) {
                    $fetchImage = new helper();
                    $productData =  $fetchImage->getProductImage($row['asin'], $markatplace);
                    $imageURL = str_replace('SL75','UL1448',$productData['imageURL']);
                    $productId = new Mws_product();
                    $productId->user_marketplace_id = $amazon_report->user_marketplace_id;
                    $productId->sku = $row['sku'];
                    $productId->fnsku = $row['fnsku'];
                    $productId->asin = $row['asin'];
                    $productId->prod_name = utf8_encode($row['Product Name']);
                    $productId->prod_image = ($imageURL ? $imageURL[0] : '');
                    $productId->fulfilledby = $row['Fulfilled By'];
                    $productId->your_price = (!empty($row['Your Price']) ? $row['Your Price'] : '0');
                    $productId->sales_price = (!empty($row['Your Price']) ? $row['Your Price'] : '0');
                    $productId->save();
                }
//                else{
//                    $productId->prod_name = utf8_encode($row['product-name']);
//                    $productId->your_price = (!empty($row['item-price']) ? $row['item-price'] : '0');
//                    $productId->sales_price = (!empty($row['item-price']) ? $row['item-price'] : '0');
//                    $productId->currency = $row['currency'];
//                    $productId->save();
//                }
                if (isset($row['Asin'])) {
                    $mwsExcessInventoryData = Mws_excess_inventory_data::where('asin', $row['Asin'])->where('fnsku', $row['FNSKU'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)
                        ->first();
                    if ($mwsExcessInventoryData) {
                        $storageStatus = 'updated';
                    } else {
                        $storageStatus = 'created';
                        $mwsExcessInventoryData = new Mws_excess_inventory_data();
                    }
                    $mwsExcessInventoryData->user_marketplace_id = $amazon_report->user_marketplace_id;
                    $mwsExcessInventoryData->product_id = $productId->id;
                    $mwsExcessInventoryData->request_date = $row['Date'];
                    $mwsExcessInventoryData->msku = $row['MSKU'];
                    $mwsExcessInventoryData->fnsku = $row['FNSKU'];
                    $mwsExcessInventoryData->asin = $row['Asin'];
                    $mwsExcessInventoryData->product_name = utf8_encode($row['Product Name']);
                    $mwsExcessInventoryData->condition = $row['Condition'];
                    $mwsExcessInventoryData->fulfilled_by = $row['Fulfilled By'];
                    $mwsExcessInventoryData->total_qty_sellable = $row['Total Quantity (Sellable)'];
                    $mwsExcessInventoryData->estimated_excess = $row['Estimated Excess'];
                    $mwsExcessInventoryData->days_of_supply = $row['Days Of Supply'];
                    $mwsExcessInventoryData->excess_threshold = (!empty($row['Excess Threshold']) ? $row['Excess Threshold'] : '0');
                    $mwsExcessInventoryData->units_sold_last_7_days = (!empty($row['Units Sold - Last 7 Days']) ? $row['Units Sold - Last 7 Days'] : '0');
                    $mwsExcessInventoryData->units_sold_last_30_days = (!empty($row['Units Sold - Last 30 Days']) ? $row['Units Sold - Last 30 Days'] : '0');
                    $mwsExcessInventoryData->units_sold_last_60_days = (!empty($row['Units Sold - Last 60 Days']) ? $row['Units Sold - Last 60 Days'] : '0');
                    $mwsExcessInventoryData->units_sold_last_90_days = (!empty($row['Units Sold - Last 90 Days']) ? $row['Units Sold - Last 90 Days'] : '0');
                    $mwsExcessInventoryData->alert = $row['Alert'];
                    $mwsExcessInventoryData->your_price = (!empty($row['Your Price']) ? $row['Your Price'] : '0');
                    $mwsExcessInventoryData->lowest_price_including_shipping = (!empty($row['Lowest Price (Includes Shipping)']) ? $row['Lowest Price (Includes Shipping)'] : '0');
                    $mwsExcessInventoryData->buybox_price = (!empty($row['Buybox Price']) ? $row['Buybox Price'] : '0');
                    $mwsExcessInventoryData->recommended_action = $row['Recommended action'];
                    $mwsExcessInventoryData->healty_inventory_level = (!empty($row['Healthy Inventory Level']) ? $row['Healthy Inventory Level'] : '0');
                    $mwsExcessInventoryData->recommended_sales_price = (!empty($row['Recommended sales price']) ? $row['Recommended sales price'] : '0');
                    $mwsExcessInventoryData->recommended_sales_duration_days = (!empty($row['Recommended sale duration (days)']) ? $row['Recommended sale duration (days)'] : '0');
                    $mwsExcessInventoryData->estimated_total_storage_cost = $row['Estimated total storage cost'];
                    $mwsExcessInventoryData->recommended_removal_qty = $row['Recommended Removal Quantity'];
                    $mwsExcessInventoryData->estimated_cost_saving_of_removal = $row['Estimated cost savings of removal'];
                    $mwsExcessInventoryData->save();
                }
            }
        }
        Log::debug("Ended Reading MWS Excess Inventory report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");

        $amazon_report->processed = 1;
        $amazon_report->save();
    }

    public static function processSPAPIReport(Amazon_requestlog &$amazon_report, $report_stream, SPApiUserMarketplace $markatplace)
    {
        $numHeaders = 0;
        Log::debug("Started Reading MWS Excess Inventory report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        if (!empty($report_stream)) {
            $headers = array();
            $tmp = explode("\n", $report_stream);
            foreach ($tmp as $lineCount => $finaldata) {
                if (!empty($finaldata)) {
                    $row = explode("\t", $finaldata);
                    if ($lineCount == 0) {
                        $headers = $row;
                        $numHeaders = count($headers);
                    }
                    if ($lineCount > 0) {
                        if (count($row) > $numHeaders) {
                            $row = array_slice($row, 0, $numHeaders);
                        }
                        $row = array_combine($headers, $row);
                        $productId = Mws_product::where('sku', $row['MSKU'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                        if (empty($productId)) {
                        } else {
                            if (isset($row['Asin'])) {
                                $mwsExcessInventoryData = Mws_excess_inventory_data::where('asin', $row['Asin'])->where('fnsku', $row['FNSKU'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)
                                    ->first();
                                if ($mwsExcessInventoryData) {
                                    $storageStatus = 'updated';
                                } else {
                                    $storageStatus = 'created';
                                    $mwsExcessInventoryData = new Mws_excess_inventory_data();
                                }
                                $mwsExcessInventoryData->user_marketplace_id = $amazon_report->user_marketplace_id;
                                $mwsExcessInventoryData->product_id = $productId->id;
                                $mwsExcessInventoryData->request_date = $row['Date'];
                                $mwsExcessInventoryData->msku = $row['MSKU'];
                                $mwsExcessInventoryData->fnsku = $row['FNSKU'];
                                $mwsExcessInventoryData->asin = $row['Asin'];
                                $mwsExcessInventoryData->product_name = utf8_encode($row['Product Name']);
                                $mwsExcessInventoryData->condition = $row['Condition'];
                                $mwsExcessInventoryData->fulfilled_by = $row['Fulfilled By'];
                                $mwsExcessInventoryData->total_qty_sellable = $row['Total Quantity (Sellable)'];
                                $mwsExcessInventoryData->estimated_excess = $row['Estimated Excess'];
                                $mwsExcessInventoryData->days_of_supply = $row['Days Of Supply'];
                                $mwsExcessInventoryData->excess_threshold = (!empty($row['Excess Threshold']) ? $row['Excess Threshold'] : '0');
                                $mwsExcessInventoryData->units_sold_last_7_days = (!empty($row['Units Sold - Last 7 Days']) ? $row['Units Sold - Last 7 Days'] : '0');
                                $mwsExcessInventoryData->units_sold_last_30_days = (!empty($row['Units Sold - Last 30 Days']) ? $row['Units Sold - Last 30 Days'] : '0');
                                $mwsExcessInventoryData->units_sold_last_60_days = (!empty($row['Units Sold - Last 60 Days']) ? $row['Units Sold - Last 60 Days'] : '0');
                                $mwsExcessInventoryData->units_sold_last_90_days = (!empty($row['Units Sold - Last 90 Days']) ? $row['Units Sold - Last 90 Days'] : '0');
                                $mwsExcessInventoryData->alert = $row['Alert'];
                                $mwsExcessInventoryData->your_price = (!empty($row['Your Price']) ? $row['Your Price'] : '0');
                                $mwsExcessInventoryData->lowest_price_including_shipping = (!empty($row['Lowest Price (Includes Shipping)']) ? $row['Lowest Price (Includes Shipping)'] : '0');
                                $mwsExcessInventoryData->buybox_price = (!empty($row['Buybox Price']) ? $row['Buybox Price'] : '0');
                                $mwsExcessInventoryData->recommended_action = $row['Recommended action'];
                                $mwsExcessInventoryData->healty_inventory_level = (!empty($row['Healthy Inventory Level']) ? $row['Healthy Inventory Level'] : '0');
                                $mwsExcessInventoryData->recommended_sales_price = (!empty($row['Recommended sales price']) ? $row['Recommended sales price'] : '0');
                                $mwsExcessInventoryData->recommended_sales_duration_days = (!empty($row['Recommended sale duration (days)']) ? $row['Recommended sale duration (days)'] : '0');
                                $mwsExcessInventoryData->estimated_total_storage_cost = $row['Estimated total storage cost'];
                                $mwsExcessInventoryData->recommended_removal_qty = $row['Recommended Removal Quantity'];
                                $mwsExcessInventoryData->estimated_cost_saving_of_removal = $row['Estimated cost savings of removal'];
                                $mwsExcessInventoryData->save();
                            }
                        }
                    }
                }
            }
        }
        Log::debug("Ended Reading MWS Excess Inventory report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");

        $amazon_report->processed = 1;
        $amazon_report->save();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShippingContainerSettings extends Model
{
//    use SoftDeletes;
    protected $table = 'shipping_container_settings';

    protected $fillable = [
        'shipping_id',
        'container_size',
        'select_container',
        'cbm_per_container',
        'container_cost',
    ];

    public function shipping(){
        return $this->belongsTo('App\Models\shipping', 'id');
    }
}

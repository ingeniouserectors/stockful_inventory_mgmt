<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fba_default_setting extends Model
{
    //
    protected $table = 'fba_default_setting';

    protected $fillable = [
        'supply_days',
        'ship_boxes',
        'ship_pallets'
    ];

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cronlog extends Model
{
    //
    protected $table = 'cronlog';

    protected $fillable = [
        'user_marketplace_id',
        'cron_name',
        'message',
        'code'
    ];
    /**
     * Get the user marketplace that owns the cron log..
     *
     * @param  no-params
     *
     */
    public function usermarketplace(){
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
}

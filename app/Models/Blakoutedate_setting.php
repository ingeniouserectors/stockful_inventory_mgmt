<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blakoutedate_setting extends Model
{
    

     protected $table = 'blackoutdates_setting';

    protected $fillable = [
        'blackout_date',
        'reason',
        'country_id'
    ];

      public function blakoutedates_setting(){
        return $this->hasOne('App\Models\Blakoutedate_setting','id');
    }

}

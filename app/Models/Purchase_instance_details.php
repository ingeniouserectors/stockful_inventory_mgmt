<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;


class Purchase_instance_details  extends Model
{
    use SoftDeletes;
    protected $table = 'purchase_instance_details';

    protected $fillable = [
        'purchesinstances_id',
        'product_id',
        'supplier',
        'vendor',
        'warehouse',
        'projected_units',
        'items_per_cartoons',
        'cbm',
        'containers',
        'subtotal'
       
    ];
    /**
     * Get the purchase instance that owns the purchase instance details..
     *
     * @param  no-params
     *
     */
     public function puraches_instances(){
        return $this->belongsTo('App\Models\Purchase_instances','purchesinstances_id');
    }
    /**
     * Get the supplier that owns the purchase instance..
     *
     * @param  no-params
     *
     */
    public function suppliers(){
        return $this->hasOne('App\Models\Supplier','supplier');
    }

}

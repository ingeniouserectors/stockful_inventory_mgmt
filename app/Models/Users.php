<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Users extends Model
{
    use SoftDeletes;
    protected $table = 'users';

    protected $fillable = [
        'name',
        'email',
        'email_verified_at',
        'password',
        'activation_code',
        'active',
        'parent_id',
        'created_by',
    ];
    /**
     * Get the user detail for the user.
     *
     * @param  no-params
     *
     */
    public function userdetails(){
        return $this->hasOne('App\Models\Userdetails','user_id');
    }
    /**
     * Get the user assign role for the user.
     *
     * @param  no-params
     *
     */
    public function user_assigned_role(){
        return $this->hasOne('App\Models\User_assigned_role', 'user_id');
    }
    /**
     * Get the user access module for the user.
     *
     * @param  no-params
     *
     */
    public function user_access_modules(){
        return $this->hasMany('App\Models\User_access_modules', 'user_id');
    }
    /**
     * Get the user role that owns the user.
     *
     * @param  no-params
     *
     */
    public function user_roles(){
        return $this->belongsToMany('App\Models\Userrole', 'user_assigned_role', 'user_id','user_role_id');
    }
    /**
     * Get the user marketplace for the user.
     *
     * @param  no-params
     *
     */
    public function usermarketplace(){
        return $this->hasMany('App\Models\Usermarketplace','user_id');
    }

    public function spAPIUserMarketplace(){
        return $this->hasMany('App\Models\SPApiUserMarketplace','user_id');
    }

}

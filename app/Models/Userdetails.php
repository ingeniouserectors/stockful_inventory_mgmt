<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Userdetails extends Model
{
    use SoftDeletes;
    protected $table = 'users_details';

    protected $fillable = [
        'user_id',
        'company',
        'address',
        'address2',
        'city',
        'state',
        'country',
        'zipcode',
        'phone',
        'created_by',
        'updated_by',
        'deleted_at'
    ];
    /**
     * Get the user  that owns the user details.
     *
     * @param  no-params
     *
     */
    public function users(){
        return $this->belongsTo('App\Models\Users','user_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Warehouse_wise_users extends Model
{
    //
    protected $table = 'warehouse_wise_users';

    protected $fillable = [
        'warehouse_id',
        'warehouse_user_id'
    ];
    /**
     * Get the warehouse wise default setting for the warhouse default setting.
     *
     * @param  no-params
     *
     */
    public function warehouse_user(){
        return $this->hasOne('App\Models\Warehouse_users','id','warehouse_user_id');
    }
//    public function warehouse_wise_default_setting(){
//        return $this->hasOne('App\Models\Warehouse_wise_default_setting','warehouse_id');
//    }
}

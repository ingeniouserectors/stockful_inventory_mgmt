<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_label extends Model
{
    //
    protected $table = 'product_label';

    protected $fillable = [
        'user_marketplace_id',
        'label_name',
    ];
    /**
     * Get the user marketplace that owns the cron log..
     *
     * @param  no-params
     *
     */
}

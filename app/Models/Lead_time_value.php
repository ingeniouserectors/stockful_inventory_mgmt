<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lead_time_value extends Model
{
    //
    protected $table = 'lead_time_value';

    protected $fillable = [
        'lead_time_id',
        'lead_time_detail_id',
        'send_mail',
        'no_of_days',
        'contact_detail'
    ];

}

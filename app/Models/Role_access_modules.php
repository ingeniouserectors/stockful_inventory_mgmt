<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role_access_modules extends Model
{
    //
    protected $table = 'role_access_modules';

    protected $fillable = [
        'role_id',
        'application_module_id',
        'create',
        'edit',
        'view',
        'delete',
        'access'
    ];

    /**
     * Get the user role that owns the access module.
     *
     * @param  no-params
     *
     */

    public function userrole(){
        return $this->belongsTo('App\Models\Userrole', 'role_id');
    }
    /**
     * Get the application module that owns the access module.
     *
     * @param  no-params
     *
     */
    public function application_modules(){
        return $this->belongsTo('App\Models\Application_modules','application_module_id');
    }
}

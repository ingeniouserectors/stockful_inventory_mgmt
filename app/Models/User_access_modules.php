<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_access_modules extends Model
{
    //
    protected $table = 'user_access_modules';

    protected $fillable = [
        'user_id',
        'application_module_id',
        'create',
        'edit',
        'view',
        'delete',
        'access'
    ];
    /**
     * Get the user  that owns the access module.
     *
     * @param  no-params
     *
     */
    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    /**
     * Get the application module  that owns the access module.
     *
     * @param  no-params
     *
     */
    public function application_modules(){
        return $this->belongsTo('App\Models\Application_modules','application_module_id');
    }
}

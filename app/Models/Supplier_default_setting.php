<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Supplier_default_setting extends Model
{
    //
    protected $table = 'supplier_default_settings';

    protected $fillable = [
        'lead_time',
'order_volume',
'quantity_discount',
'moq',
'CBM_Per_Container',
'Production_Time',
'Boat_To_Port',
'Port_To_Warehouse',
'Warehouse_Receipt',
'Ship_To_Specific_Warehouse'
    ];
    /**
     * Get the default setting for the supplier wise setting.
     *
     * @param  no-params
     *
     */
    public function supplier_wise_default_setting(){
        return $this->hasOne('App\Models\Supplier_wise_default_setting','supplier_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Warehouse_default_setting extends Model
{
    //
    protected $table = 'warehouse_default_settings';

    protected $fillable = [
        'lead_time'
    ];
    /**
     * Get the warehouse wise default setting for the warhouse default setting.
     *
     * @param  no-params
     *
     */
    public function warehouse_wise_default_setting(){
        return $this->hasOne('App\Models\Warehouse_wise_default_setting','warehouse_id');
    }
}

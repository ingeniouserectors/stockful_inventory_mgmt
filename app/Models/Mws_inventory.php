<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mws_inventory extends Model
{
    //
    protected $table="mws_inventories";
    protected $fillable = [
        'user_marketplace_id',
        'product_id',
        'condition',
        'total_supply_quantity',
        'fnsku',
        'instock_supply_quantity',
        'asin',
        'sellersku'
    ];
    /**
     * Get the user marketplace that owns the inventory..
     *
     * @param  no-params
     *
     */
    public function usermarketplace(){
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
}

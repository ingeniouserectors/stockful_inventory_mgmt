<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vendor_wise_default_setting extends Model
{
    //
    protected $table = 'vendor_wise_default_settings';

    protected $fillable = [
        'vendor_id',
        'lead_time',
        'order_volume',
        'quantity_discount',
        'moq',
        'shipping',
        'ship_to_warehouse',
        'cbm_per_container'
    ];
    /**
     * Get the vendor that owns the vendor wise default setting.
     *
     * @param  no-params
     *
     */
    public function vendor(){
        return $this->belongsTo('App\Models\Vendor','vendor_id');
    }
    /**
     * Get the vendor default setting that owns the vendor wise default setting.
     *
     * @param  no-params
     *
     */
    public function vendor_default_setting(){
        return $this->belongsTo('App\Models\Vendor_default_setting','vendor_id');
    }
}

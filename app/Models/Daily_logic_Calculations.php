<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Daily_logic_Calculations extends Model
{
    protected $fillable = [
        'user_marketplace_id',
        'prod_id',
        'total_inventory',
        'lead_time_amazon',
        'order_frequency',
        'day_rate',
        'days_supply',
        'blackout_days',
        'projected_reorder_date',
        'projected_reorder_qty',
        'days_left_to_order',
        'projected_order_date_range',
        'sevendays_sales',
        'sevenday_day_rate',
        'fourteendays_sales',
        'fourteendays_day_rate',
        'thirtydays_sales',
        'thirtyday_day_rate',
        'sevendays_previous',
        'sevendays_previous_day_rate',
        'thirtydays_previous',
        'thirtydays_previous_day_rate',
        'thirtydays_last_year',
        'thirtydays_last_year_day_rate'
    ];
    //
    protected $table = 'daily_logic_calculations';
    /**
     * Get the user marketplace that owns the Daily calculations..
     *
     * @param  no-params
     *
     */
    public function usermarketplace()
    {
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
    /**
     * Get the product that owns the Daily alculations..
     *
     * @param  no-params
     *
     */
    public function mws_product()
    {
        return $this->belongsTo('App\Models\Mws_product', 'prod_id');
    }
}

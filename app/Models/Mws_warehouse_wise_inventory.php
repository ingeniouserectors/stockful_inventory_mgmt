<?php

namespace App\Models;

use App\Classes\helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Mws_warehouse_wise_inventory extends Model
{
    //
    protected $table = 'mws_warehouse_wise_inventory';

    protected $fillable = [
        'user_marketplace_id', 'product_id', 'snapshot_date', 'fnsku', 'sku',
        'qty', 'fulfilment_center_id', 'detailed_disposition','country'
    ];
    /**
     * Get the user marketplace that owns the warehouse wise inventory..
     *
     * @param  no-params
     *
     */
    public function usermarketplace()
    {
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
    /**
     * Get the product that owns the warehouse wise inventory..
     *
     * @param  no-params
     *
     */
    public function mws_product()
    {
        return $this->belongsTo('App\Models\Mws_product', 'product_id');
    }
    /**
     * Logic for process reports..
     *
     * @param  amazon_report
     *         report_stream
     *         marketplace
     *
     */
    public static function processReport(Amazon_requestlog &$amazon_report, $report_stream, Usermarketplace $markatplace)
    {
        $first = false;
        $numHeaders = 0;

        Log::debug("Started Reading MWS Warehouse Wise Inventory report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        while (!feof($report_stream)) {

            $row = fgetcsv($report_stream, 10000, "\t");
            if (!$first) {
                $first = true;
                $headers = $row;
                $numHeaders = count($headers);
                continue;
            }
            if (!empty($row)) {
                // truncate row data if item count exceeds number of headers
                if (count($row) > $numHeaders) {
                    $row = array_slice($row, 0, $numHeaders);
                }
                $row = array_combine($headers, $row);
                $productId = Mws_product::where('sku', $row['sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                if (empty($productId)) {
                    $fetchImage = new helper();
                    $productData  = $fetchImage->getProductImage($row['asin'], $markatplace);
                    $imageURL = str_replace('SL75','UL1448',$productData['imageURL']);
                    $productId = new Mws_product();
                    $productId->user_marketplace_id = $amazon_report->user_marketplace_id;
                    $productId->sku = $row['sku'];
                    $productId->prod_name = utf8_encode($row['product-name']);
                    $productId->fnsku = $row['fnsku'];
                    $productId->prod_image = ($imageURL ? $imageURL[0] : '');
                    $productId->save();
                }
//                else{
//                    $productId->prod_name = utf8_encode($row['product-name']);
//                    $productId->save();
//                }
                if (isset($row['sku'])) {
                    $mwsWarehouseWiseInventoryData = Mws_warehouse_wise_inventory::where('sku', $row['sku'])->where('fnsku', $row['fnsku'])
                        ->where('user_marketplace_id', $amazon_report->user_marketplace_id)
                        ->where('fulfilment_center_id',$row['fulfillment-center-id'])
                        ->where('detailed_disposition',$row['detailed-disposition'])->first();
                    if ($mwsWarehouseWiseInventoryData) {
                        $storageStatus = 'updated';
                    } else {
                        $storageStatus = 'created';
                        $mwsWarehouseWiseInventoryData = new Mws_warehouse_wise_inventory();
                    }
                    $mwsWarehouseWiseInventoryData->user_marketplace_id = $amazon_report->user_marketplace_id;
                    $mwsWarehouseWiseInventoryData->product_id = $productId->id;
                    $mwsWarehouseWiseInventoryData->snapshot_date = date('Y-m-d h:m:s', strtotime($row['snapshot-date']));
                    $mwsWarehouseWiseInventoryData->sku = $row['sku'];
                    $mwsWarehouseWiseInventoryData->fnsku = $row['fnsku'];
                    $mwsWarehouseWiseInventoryData->qty = $row['quantity'];
                    $mwsWarehouseWiseInventoryData->fulfilment_center_id = $row['fulfillment-center-id'];
                    $mwsWarehouseWiseInventoryData->detailed_disposition = $row['detailed-disposition'];
                    $mwsWarehouseWiseInventoryData->country = $row['country'];
                    $mwsWarehouseWiseInventoryData->save();
                }
            }
        }
        Log::debug("Ended Reading MWS Warehouse Wise Inventory report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");

        $amazon_report->processed = 1;
        $amazon_report->save();
    }
}

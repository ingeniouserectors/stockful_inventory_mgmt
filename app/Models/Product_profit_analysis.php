<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_profit_analysis extends Model
{
    //
    protected $table = 'product_profit_analysis';

    protected $fillable = [
        'product_id',
        'unit_cost',
        'shipping',
        'customs',
        'other_landed_cost',
        'total_landed',
        'warehouse_storage',
        'inbound_shipping',
        'other_inland_cost',
        'total_inland',
        'fba_fee',
        'referal_fee',
        'storage_fee',
        'total_amazon_fee',
        'total_costs',
        'price_profit',
        'total_profit',
        'profit_margin',
        'roi'
    ];
    /**
     * Get the user marketplace that owns the cron log..
     *
     * @param  no-params
     *
     */
}

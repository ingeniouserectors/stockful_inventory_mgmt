<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_assign_to_label extends Model
{
    //
    protected $table = 'product_assign_to_label';

    protected $fillable = [
        'user_marketplace_id',
        'label_id',
        'product_id'
    ];

    /**
     * Get the user marketplace that owns the cron log..
     *
     * @param  no-params
     *
     */

    public function labels(){
        return $this->hasOne('App\Models\Product_label','id');
    }
}

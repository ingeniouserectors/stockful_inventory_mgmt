<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_dimensions extends Model
{
    //
    protected $table = 'product_dimensions';

    protected $fillable = [
        'product_id',
        'item_height',
        'item_width',
        'item_length',
        'size_in',
        'item_weight',
        'weigth_in',
        'box_qty',
        'box_height',
        'box_width',
        'box_length',
        'box_size_in',
        'box_weight',
        'box_volume',
        'box_volumn_in',
        'box_weight_unit',
        'box_par_carton_qty',
        'box_par_carton_height',
        'box_par_carton_width',
        'box_par_carton_length',
        'box_par_carton_size_in',
        'box_par_carton_weight',
        'box_par_carton_volume',
        'box_par_carton_volumn_in',
        'cartons_par_pallet_qty',
        'cartons_par_pallet_height',
        'cartons_par_pallet_width',
        'cartons_par_pallet_length',
        'cartons_par_pallet_size_in',
        'cartons_par_pallet_weight',
        'cartons_par_pallet_volume',
        'cartons_par_pallet_volumn_in',
    ];
    /**
     * Get the user marketplace that owns the cron log..
     *
     * @param  no-params
     *
     */
}

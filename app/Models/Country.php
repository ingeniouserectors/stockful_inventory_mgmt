<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Country extends Model
{
    protected $table = 'country';

    protected $fillable = [
        'country_code',
        'country_name',
        
    ];


    public function member_blackoutdate_setting()
    {
        return $this->hasMany('App\Models\Member_blackoutdate_setting', 'country_id');
    }    
}
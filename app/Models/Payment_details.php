<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment_details extends Model
{
    //
    protected $table = 'payment_details';

    protected $fillable = [
        'payment_id',
        'payment_terms',
        'percentage',
        'dues',
        'days_before_after',
        'payment_term_type_id',
    ];

    public function term_type(){
        return $this->hasOne('App\Models\Payment_terms_type','id','payment_term_type_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_custom_reorder_schedule_detail extends Model
{
    protected $table = 'product_custom_reorder_schedule_detail';

    protected $fillable = [
        'product_id',
        'product_assign_id',
        'reorder_schedule_type_id',
        'schedule_days',
    ];
}

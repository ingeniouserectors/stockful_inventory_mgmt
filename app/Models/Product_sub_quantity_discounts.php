<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_sub_quantity_discounts extends Model
{
    //
    protected $table = 'product_sub_quantity_discounts';

    protected $fillable = [
        'product_id',
        'product_quantity_id',
        'unit',
        'price'
    ];
    /**
     * Get the user marketplace that owns the cron log..
     *
     * @param  no-params
     *
     */
}

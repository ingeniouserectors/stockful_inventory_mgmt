<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reorder_schedule_detail  extends Model
{
    //
    protected $table = 'reorder_schedule_detail';

    protected $fillable = [
        'supplier_vendor',
        'supplier_vendor_id',
        'reorder_schedule_type_id',
		'schedule_days'
		
    ];

}

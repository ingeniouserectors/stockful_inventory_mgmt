<?php

namespace App\Models;

use App\Classes\helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
class Mws_restock_inventory_recommendations extends Model
{
    //
    protected $table = 'mws_restock_inventory_recommendations';

    protected $fillable = ['user_marketplace_id', 'product_id','country','product_name','fnsku','sku','asin', 'condition', 'supplier','supplier_part_no','currency_code', 'price', 'sales_last_30_days','units_sold_last_30_days','total_units','inbound_inventory','available_inventory','fc_transfer','fc_processing','customer_order','unfulfillable','fulfilled_by','days_of_supply','alert', 'recommended_replenishment_quantity', 'recommended_ship_date','inventory_level_threshold_published_current_month','current_month_very_low_inventory_threshold','current_month_minimum_inventory_threshold','current_month_maximum_inventory_threshold','current_month_very_high_inventory_threshold','inventory_level_threshold_published_next_month','next_month_very_low_inventory_threshold','next_month_minimum_inventory_threshold','next_month_maximum_inventory_threshold','next_month_very_high_inventory_threshold','utilization','maximum_shipment_quantity'
        ];
    /**
     * Get the user marketplace that owns the restock inventory recommendations..
     *
     * @param  no-params
     *
     */
    public function usermarketplace()
    {
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
    /**
     * Get the product that owns the restock inventory recommendations..
     *
     * @param  no-params
     *
     */
    public function mws_product()
    {
        return $this->belongsTo('App\Models\Mws_product', 'product_id');
    }
    /**
     * Logic for process reports..
     *
     * @param  amazon_report
     *         report_stream
     *         marketplace
     *
     */
    public static function processReport(Amazon_requestlog &$amazon_report, $report_stream, Usermarketplace $markatplace)
    {
        $first = false;
        $numHeaders = 0;

        Log::debug("Started Reading MWS restock inventory recommendations of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        while (!feof($report_stream)) {

            $row = fgetcsv($report_stream, 10000, "\t");
            if (!$first) {
                $first = true;
                $headers = $row;
                $numHeaders = count($headers);
                continue;
            }
            if (!empty($row)) {
                // truncate row data if item count exceeds number of headers
                if (count($row) > $numHeaders) {
                    $row = array_slice($row, 0, $numHeaders);
                }
                $originalArray = $row;
                $row = array_combine($headers, $row);
                $productId = Mws_product::where('sku', $row['Merchant SKU'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                if (empty($productId)) {
                    $fetchImage = new helper();
                    $productData =  $fetchImage->getProductImage($row['ASIN'], $markatplace);
                    $imageURL = str_replace('SL75','UL1448',$productData['imageURL']);
                    $productId = new Mws_product();
                    $productId->user_marketplace_id = $amazon_report->user_marketplace_id;
                    $productId->sku = $row['Merchant SKU'];
                    $productId->fnsku = $row['FNSKU'];
                    $productId->prod_name = utf8_encode($row['Product Name']);
                    $productId->prod_image = ($imageURL ? $imageURL[0] : '');
                    $productId->save();
                }
//                else{
//                    $productId->prod_name = utf8_encode($row['product-name']);
//                    $productId->save();
//                }
                if (isset($row['Merchant SKU'])) {
                    $mwsRestockInventoryRecommandationsData = Mws_restock_inventory_recommendations::where('sku', $row['Merchant SKU'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                    if ($mwsRestockInventoryRecommandationsData) {
                        $storageStatus = 'updated';
                    } else {
                        $storageStatus = 'created';
                        $mwsRestockInventoryRecommandationsData = new Mws_restock_inventory_recommendations();
                    }
                    if(empty($row['Recommended ship date']) || $row['Recommended ship date']=='') {
                        $ship_date = NULL;
                    }
                    else{
                        $ship_date = date('Y-m-d', strtotime($row['Recommended ship date']));

                    }
                    if(isset($row['Inventory level threshold\'s published  Current Month'])) {
                        $current_month_thres = $row['Inventory level threshold\'s published  Current Month'];
                    }
                    else {
                        $current_month_thres = '';
                    }
                    if(isset($row['Inventory level threshold\'s published  Next Month'])) {
                        $next_month_thres = $row['Inventory level threshold\'s published  Next Month'];
                    }
                    else{
                       $next_month_thres='';
                    }
                    $mwsRestockInventoryRecommandationsData->user_marketplace_id = $amazon_report->user_marketplace_id;
                    $mwsRestockInventoryRecommandationsData->product_id = $productId->id;
                    $mwsRestockInventoryRecommandationsData->country = $row['Country'];
                    $mwsRestockInventoryRecommandationsData->product_name=utf8_encode($row['Product Name']);
                    $mwsRestockInventoryRecommandationsData->fnsku = $row['FNSKU'];
                    $mwsRestockInventoryRecommandationsData->sku = $row['Merchant SKU'];
                    $mwsRestockInventoryRecommandationsData->asin = $row['ASIN'];
                    $mwsRestockInventoryRecommandationsData->condition = $row['Condition'];
                    $mwsRestockInventoryRecommandationsData->supplier = $row['Supplier'];
                    $mwsRestockInventoryRecommandationsData->supplier_part_no = $row['Supplier part no.'];
                    $mwsRestockInventoryRecommandationsData->currency_code=$row['Currency code'];
                    $mwsRestockInventoryRecommandationsData->price = $row['Price'];
                    $mwsRestockInventoryRecommandationsData->sales_last_30_days = $row['Sales last 30 days'];
                    $mwsRestockInventoryRecommandationsData->units_sold_last_30_days = $row['Units Sold Last 30 Days'];
                    $mwsRestockInventoryRecommandationsData->total_units = $row['Total Units'];
                    $mwsRestockInventoryRecommandationsData->inbound_inventory = $row['Inbound'];
                    $mwsRestockInventoryRecommandationsData->available_inventory = $row['Available'];
                    $mwsRestockInventoryRecommandationsData->fc_transfer = $row['FC transfer'];
                    $mwsRestockInventoryRecommandationsData->fc_processing = $row['FC Processing'];
                    $mwsRestockInventoryRecommandationsData->customer_order = $row['Customer Order'];
                    $mwsRestockInventoryRecommandationsData->unfulfillable = $row['Unfulfillable'];
                    $mwsRestockInventoryRecommandationsData->fulfilled_by = $row['Fulfilled by'];
                    $mwsRestockInventoryRecommandationsData->days_of_supply = (!empty($row['Days of Supply']) ? $row['Days of Supply'] : '');
                    $mwsRestockInventoryRecommandationsData->alert = $row['Alert'];
                    $mwsRestockInventoryRecommandationsData->recommended_replenishment_quantity = $row['Recommended replenishment qty'];
                    $mwsRestockInventoryRecommandationsData->recommended_ship_date = $ship_date;
                    $mwsRestockInventoryRecommandationsData->inventory_level_threshold_published_current_month = (!empty($current_month_thres) ? $current_month_thres : (!empty($originalArray[24]) ? $originalArray[24] : 0));
                    $mwsRestockInventoryRecommandationsData->current_month_very_low_inventory_threshold = (!empty($row['Current month - Very low inventory threshold']) ? $row['Current month - Very low inventory threshold'] : '');
                    $mwsRestockInventoryRecommandationsData->current_month_minimum_inventory_threshold = (!empty($row['Current month - Minimum inventory threshold']) ? $row['Current month - Minimum inventory threshold'] : '');
                    $mwsRestockInventoryRecommandationsData->current_month_maximum_inventory_threshold = (!empty($row['Current month - Maximum inventory threshold']) ? $row['Current month - Maximum inventory threshold'] : '');
                    $mwsRestockInventoryRecommandationsData->current_month_very_high_inventory_threshold = (!empty($row['Current month - Very high inventory threshold']) ? $row['Current month - Very high inventory threshold'] : '');
                    $mwsRestockInventoryRecommandationsData->inventory_level_threshold_published_next_month = (!empty($next_month_thres) ? $next_month_thres : (!empty($originalArray[29]) ? $originalArray[29] : 0));
                    $mwsRestockInventoryRecommandationsData->next_month_very_low_inventory_threshold = (!empty($row['Next month - Very low inventory threshold']) ? $row['Next month - Very low inventory threshold'] : '');
                    $mwsRestockInventoryRecommandationsData->next_month_minimum_inventory_threshold = (!empty($row['Next month - Minimum inventory threshold']) ? $row['Next month - Minimum inventory threshold'] : '');
                    $mwsRestockInventoryRecommandationsData->next_month_maximum_inventory_threshold = (!empty($row['Next month - Maximum inventory threshold']) ? $row['Next month - Maximum inventory threshold'] : '');
                    $mwsRestockInventoryRecommandationsData->next_month_very_high_inventory_threshold = (!empty($row['Next month - Very high inventory threshold']) ? $row['Next month - Very high inventory threshold'] : '');
                    $mwsRestockInventoryRecommandationsData->utilization = (!empty($row['Utilization']) ? $row['Utilization'] : '');
                    $mwsRestockInventoryRecommandationsData->maximum_shipment_quantity = (!empty($row['Maximum shipment quantity']) ? $row['Maximum shipment quantity'] : '');
                    $mwsRestockInventoryRecommandationsData->save();
                }
            }
        }
        Log::debug("Ended Reading MWS Restock Inventory Recommendation Report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");

        $amazon_report->processed = 1;
        $amazon_report->save();
    }
    public static function processSPAPIReport(Amazon_requestlog &$amazon_report, $report_stream, SPApiUserMarketplace $markatplace)
    {
        $requestConfiguration = Mws_product::getConfiguration($markatplace);
        $finance = new \ClouSale\AmazonSellingPartnerAPI\Api\CatalogApi($requestConfiguration);
        $numHeaders = 0;
        Log::debug("Started Reading MWS Restock Inventory Recommendation Report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        if (!empty($report_stream)) {
            $headers = array();
            $tmp = explode("\n", $report_stream);
            foreach ($tmp as $lineCount => $finaldata) {
                if (!empty($finaldata)) {
                    $row = explode("\t", $finaldata);
                    print_r($row);
                    if ($lineCount == 0) {
                        $headers = $row;
                        $numHeaders = count($headers);
                    }
                    if ($lineCount > 0) {
                        if (count($row) > $numHeaders) {
                            $row = array_slice($row, 0, $numHeaders);
                        }
                        $row = array_combine($headers, $row);
                        echo "<pre />";
                        print_r($row);
                        $productId = Mws_product::where('sku', $row['Merchant SKU'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                        if (empty($productId)) {
                            $shipmentData = $finance->getCatalogItem($markatplace->market_place_developer->marketplace_id,$row['ASIN']);
                                $payload = $shipmentData->getPayload();
                                $get_attribute = $payload->getAttributeSets();
                                $product_title = @$get_attribute[0]["title"];
                                $product_image = @$get_attribute[0]["small_image"]['url'];
                                $product_brand = @$get_attribute[0]["brand"];
                                $product_prod_group = @$get_attribute[0]["product_group"];
                                //dd($get_attribute);
    
                                $productId = new Mws_product();
                                $productId->user_marketplace_id = $amazon_report->user_marketplace_id;
                                $productId->sku = $row['Merchant SKU'];
                                $productId->fnsku = $row['FNSKU'];
                                $productId->asin = $row['ASIN'];
                                $productId->prod_image = $product_image;
                                $productId->prod_name = $product_title;
                                $productId->brand = $product_brand;
                                $productId->prod_group = $product_prod_group;
                                $productId->save();
                        } else {
                            if (isset($row['Merchant SKU'])) {
                                $mwsRestockInventoryRecommandationsData = Mws_restock_inventory_recommendations::where('sku', $row['Merchant SKU'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                                if ($mwsRestockInventoryRecommandationsData) {
                                    $storageStatus = 'updated';
                                } else {
                                    $storageStatus = 'created';
                                    $mwsRestockInventoryRecommandationsData = new Mws_restock_inventory_recommendations();
                                }
                                if (empty($row['Recommended ship date']) || $row['Recommended ship date'] == '') {
                                    $ship_date = NULL;
                                } else {
                                    $ship_date = date('Y-m-d', strtotime($row['Recommended ship date']));

                                }
                                if (isset($row['Inventory level threshold\'s published  Current Month'])) {
                                    $current_month_thres = $row['Inventory level threshold\'s published  Current Month'];
                                } else {
                                    $current_month_thres = '';
                                }
                                if (isset($row['Inventory level threshold\'s published  Next Month'])) {
                                    $next_month_thres = $row['Inventory level threshold\'s published  Next Month'];
                                } else {
                                    $next_month_thres = '';
                                }
                                $mwsRestockInventoryRecommandationsData->user_marketplace_id = $amazon_report->user_marketplace_id;
                                $mwsRestockInventoryRecommandationsData->product_id = $productId->id;
                                $mwsRestockInventoryRecommandationsData->country = $row['Country'];
                                $mwsRestockInventoryRecommandationsData->product_name = utf8_encode($row['Product Name']);
                                $mwsRestockInventoryRecommandationsData->fnsku = $row['FNSKU'];
                                $mwsRestockInventoryRecommandationsData->sku = $row['Merchant SKU'];
                                $mwsRestockInventoryRecommandationsData->asin = $row['ASIN'];
                                $mwsRestockInventoryRecommandationsData->condition = $row['Condition'];
                                $mwsRestockInventoryRecommandationsData->supplier = $row['Supplier'];
                                $mwsRestockInventoryRecommandationsData->supplier_part_no = $row['Supplier part no.'];
                                $mwsRestockInventoryRecommandationsData->currency_code = $row['Currency code'];
                                $mwsRestockInventoryRecommandationsData->price = $row['Price'];
                                $mwsRestockInventoryRecommandationsData->sales_last_30_days = $row['Sales last 30 days'];
                                $mwsRestockInventoryRecommandationsData->units_sold_last_30_days = $row['Units Sold Last 30 Days'];
                                $mwsRestockInventoryRecommandationsData->total_units = $row['Total Units'];
                                $mwsRestockInventoryRecommandationsData->inbound_inventory = $row['Inbound'];
                                $mwsRestockInventoryRecommandationsData->available_inventory = $row['Available'];
                                $mwsRestockInventoryRecommandationsData->fc_transfer = $row['FC transfer'];
                                $mwsRestockInventoryRecommandationsData->fc_processing = $row['FC Processing'];
                                $mwsRestockInventoryRecommandationsData->customer_order = $row['Customer Order'];
                                $mwsRestockInventoryRecommandationsData->unfulfillable = $row['Unfulfillable'];
                                $mwsRestockInventoryRecommandationsData->fulfilled_by = $row['Fulfilled by'];
                                $mwsRestockInventoryRecommandationsData->days_of_supply = $row['Days of Supply'];
                                $mwsRestockInventoryRecommandationsData->alert = $row['Alert'];
                                $mwsRestockInventoryRecommandationsData->recommended_replenishment_quantity = $row['Recommended replenishment qty'];
                                $mwsRestockInventoryRecommandationsData->recommended_ship_date = $ship_date;
                                $mwsRestockInventoryRecommandationsData->inventory_level_threshold_published_current_month = (!empty($current_month_thres) ? $current_month_thres : (!empty($originalArray[24]) ? $originalArray[24] : 0));
                                $mwsRestockInventoryRecommandationsData->current_month_very_low_inventory_threshold = (!empty($row['Current month - Very low inventory threshold']) ? $row['Current month - Very low inventory threshold'] : '');
                                $mwsRestockInventoryRecommandationsData->current_month_minimum_inventory_threshold = (!empty($row['Current month - Minimum inventory threshold']) ? $row['Current month - Minimum inventory threshold'] : '');
                                $mwsRestockInventoryRecommandationsData->current_month_maximum_inventory_threshold = (!empty($row['Current month - Maximum inventory threshold']) ? $row['Current month - Maximum inventory threshold'] : '');
                                $mwsRestockInventoryRecommandationsData->current_month_very_high_inventory_threshold = (!empty($row['Current month - Very high inventory threshold']) ? $row['Current month - Very high inventory threshold'] : '');
                                $mwsRestockInventoryRecommandationsData->inventory_level_threshold_published_next_month = (!empty($next_month_thres) ? $next_month_thres : (!empty($originalArray[29]) ? $originalArray[29] : 0));
                                $mwsRestockInventoryRecommandationsData->next_month_very_low_inventory_threshold = (!empty($row['Next month - Very low inventory threshold']) ? $row['Next month - Very low inventory threshold'] : '');
                                $mwsRestockInventoryRecommandationsData->next_month_minimum_inventory_threshold = (!empty($row['Next month - Minimum inventory threshold']) ? $row['Next month - Minimum inventory threshold'] : '');
                                $mwsRestockInventoryRecommandationsData->next_month_maximum_inventory_threshold = (!empty($row['Next month - Maximum inventory threshold']) ? $row['Next month - Maximum inventory threshold'] : '');
                                $mwsRestockInventoryRecommandationsData->next_month_very_high_inventory_threshold = (!empty($row['Next month - Very high inventory threshold']) ? $row['Next month - Very high inventory threshold'] : '');
                                $mwsRestockInventoryRecommandationsData->utilization = (!empty($row['Utilization']) ? $row['Utilization'] : '');
                                $mwsRestockInventoryRecommandationsData->maximum_shipment_quantity = (!empty($row['Maximum shipment quantity']) ? $row['Maximum shipment quantity'] : '');
                                $mwsRestockInventoryRecommandationsData->save();
                            }
                        }
                    }
                }
            }
        }
        Log::debug("Ended Reading MWS Restock Inventory Recommendation Report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");

        $amazon_report->processed = 1;
        $amazon_report->save();
    }
}

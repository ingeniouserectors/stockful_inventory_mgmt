<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vendor_blackout_date extends Model
{
    //
    protected $table = 'vendor_blackout_dates';

    protected $fillable = [
        'vendor_id',
        'event_name',
        'start_date',
        'end_date',
        'number_of_days',
        'type',
    ];

    /**
     * Get the vendor that owns the blackout date.
     *
     * @param  no-params
     *
     */
    public function vendor(){
        return $this->belongsTo('App\Models\Vendor', 'vendor_id');
    }
}

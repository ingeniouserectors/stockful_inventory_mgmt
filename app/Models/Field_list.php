<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Field_list extends Model
{
    //
    protected $table = 'field_list';

    protected $fillable = [
        'module_id',
        'original_name',
        'display_name',
        'field_type',
        'referance_table',
        'referance_table_field',
        'active',
        'table_name',
        'display_field',
        'join_with',
        'join_type',
    ];

}

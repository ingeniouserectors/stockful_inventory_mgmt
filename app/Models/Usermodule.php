<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;



class Usermodule extends Model
{

	protected $table='application_modules';

	protected $fillable=[
		'module',
        'table_name'
	];
}

?>
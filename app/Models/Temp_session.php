<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Temp_session extends Model
{
    protected $table = 'temp_session';

    protected $fillable = [
        'user_id',
        'marketplace_id',
        'product_id',
        'recalulated_qty'
    ];

}

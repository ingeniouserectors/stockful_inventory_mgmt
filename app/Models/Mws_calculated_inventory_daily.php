<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mws_calculated_inventory_daily extends Model
{
    //
    protected $table="mws_calculated_inventory_daily";
    protected $fillable = [
        'user_marketplace_id',
        'product_id',
        'fnsku',
        'sku',
        'qty',
        'inventory_date',
        'count_status'
    ];
    /**
     * Get the product that owns the calculated inventory daily..
     *
     * @param  no-params
     *
     */
    public function usermarketplace(){
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
    /**
     * Get the product that owns the calculated inventory daily..
     *
     * @param  no-params
     *
     */
    public function mws_product(){
        return $this->belongsTo('App\Models\Mws_product', 'product_id');
    }
}

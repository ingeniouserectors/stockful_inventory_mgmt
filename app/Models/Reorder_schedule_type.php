<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reorder_schedule_type  extends Model
{
    //
    protected $table = 'reorder_schedule_type';

    protected $fillable = [
        'schedule_type'
    ];

}

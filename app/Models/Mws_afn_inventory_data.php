<?php

namespace App\Models;

use App\Classes\helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Mws_afn_inventory_data extends Model
{
    //
    protected $table = 'mws_afn_inventory_data';

    protected $fillable = [
        'user_marketplace_id', 'product_id', 'seller_sku', 'fulfilment_channel_sku', 'asin',
        'condition_type', 'warehouse_condition_code', 'quantity_available'
    ];
    /**
     * Get the user marketplace that owns the afn inventory..
     *
     * @param  no-params
     *
     */
    public function usermarketplace()
    {
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
    /**
     * Get the product that owns the afn inventory..
     *
     * @param  no-params
     *
     */
    public function mws_product()
    {
        return $this->belongsTo('App\Models\Mws_product', 'product_id');
    }
    /**
     * Logic for process reports..
     *
     * @param  amazon_report
     *         report_stream
     *         marketplace
     *
     */
    public static function processReport(Amazon_requestlog &$amazon_report, $report_stream, Usermarketplace $markatplace)
    {
        $first = false;
        $numHeaders = 0;

        Log::debug("Started Reading MWS AFN Inventory report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        while (!feof($report_stream)) {

            $row = fgetcsv($report_stream, 10000, "\t");
            if (!$first) {
                $first = true;
                $headers = $row;
                $numHeaders = count($headers);
                continue;
            }
            if (!empty($row)) {
                // truncate row data if item count exceeds number of headers
                if (count($row) > $numHeaders) {
                    $row = array_slice($row, 0, $numHeaders);
                }
                $row = array_combine($headers, $row);
                $productId = Mws_product::where('sku', $row['seller-sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                if (empty($productId)) {
                    $fetchImage = new helper();
                    $productData  = $fetchImage->getProductImage($row['asin'], $markatplace);
                    $imageURL = str_replace('SL75','UL1448',$productData['imageURL']);
                    $productId = new Mws_product();
                    $productId->user_marketplace_id = $amazon_report->user_marketplace_id;
                    $productId->sku = $row['seller-sku'];
                    $productId->fnsku = $row['fulfillment-channel-sku'];
                    $productId->asin = $row['asin'];
                    $productId->prod_image = ($imageURL ? $imageURL[0] : '');
                    $productId->save();
                }
                if (isset($row['asin'])) {
                    $mwsAfnInventoryData = Mws_afn_inventory_data::where('asin', $row['asin'])->where('seller_sku', $row['seller-sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)
                        ->where('warehouse_condition_code',$row['Warehouse-Condition-code'])->first();
                    if ($mwsAfnInventoryData) {
                        $storageStatus = 'updated';
                    } else {
                        $storageStatus = 'created';
                        $mwsAfnInventoryData = new Mws_afn_inventory_data();
                    }
                    $mwsAfnInventoryData->user_marketplace_id = $amazon_report->user_marketplace_id;
                    $mwsAfnInventoryData->product_id = $productId->id;
                    $mwsAfnInventoryData->seller_sku = $row['seller-sku'];
                    $mwsAfnInventoryData->fulfilment_channel_sku = $row['fulfillment-channel-sku'];
                    $mwsAfnInventoryData->asin = $row['asin'];
                    $mwsAfnInventoryData->condition_type = $row['condition-type'];
                    $mwsAfnInventoryData->warehouse_condition_code = $row['Warehouse-Condition-code'];
                    $mwsAfnInventoryData->quantity_available = $row['Quantity Available'];
                    $mwsAfnInventoryData->save();
                }
            }
        }
        Log::debug("Ended Reading MWS AFN Inventory report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");

        $amazon_report->processed = 1;
        $amazon_report->save();
    }

    public static function processSPAPIReport(Amazon_requestlog &$amazon_report, $report_stream, SPApiUserMarketplace $markatplace)
    {
        $requestConfiguration = Mws_product::getConfiguration($markatplace);
        $finance = new \ClouSale\AmazonSellingPartnerAPI\Api\CatalogApi($requestConfiguration);
        $numHeaders = 0;
        Log::debug("Started Reading MWS AFN Inventory report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        if (!empty($report_stream)) {
            $headers = array();
            $tmp = explode("\n", $report_stream);
            foreach ($tmp as $lineCount => $finaldata) {
                if (!empty($finaldata)) {
                    $row = explode("\t", $finaldata);
                    if ($lineCount == 0) {
                        $headers = $row;
                        $numHeaders = count($headers);
                    }
                    if ($lineCount > 0) {
                        if (count($row) > $numHeaders) {
                            $row = array_slice($row, 0, $numHeaders);
                        }
                        $row = array_combine($headers, $row);
                        $productId = Mws_product::where('sku', $row['seller-sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                         //echo '<pre>';
                         //print_r($row);
                        if (empty($productId)) {
                            // echo $row['asin'];
                            // $checkasin = Mws_product::where('asin',$row['asin'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                            // if(empty($checkasin)){
                                $shipmentData = $finance->getCatalogItem($markatplace->market_place_developer->marketplace_id,$row['asin']);
                                $payload = $shipmentData->getPayload();
                                $get_attribute = $payload->getAttributeSets();
                                $product_title = @$get_attribute[0]["title"];
                                $product_image = @$get_attribute[0]["small_image"]['url'];
                                $product_brand = @$get_attribute[0]["brand"];
                                $product_prod_group = @$get_attribute[0]["product_group"];
                                //dd($get_attribute);
    
                                $productId = new Mws_product();
                                $productId->user_marketplace_id = $amazon_report->user_marketplace_id;
                                $productId->sku = $row['seller-sku'];
                                $productId->fnsku = $row['fulfillment-channel-sku'];
                                $productId->asin = $row['asin'];
                                $productId->prod_image = $product_image;
                                $productId->prod_name = $product_title;
                                $productId->brand = $product_brand;
                                $productId->prod_group = $product_prod_group;
                                $productId->save();
                            //}
                        } 
                        
                            if (isset($row['asin'])) {
                                $mwsAfnInventoryData = Mws_afn_inventory_data::where('asin', $row['asin'])->where('seller_sku', $row['seller-sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)
                                    ->where('warehouse_condition_code', $row['Warehouse-Condition-code'])->first();
                                if ($mwsAfnInventoryData) {
                                    $storageStatus = 'updated';
                                } else {
                                    $storageStatus = 'created';
                                    $mwsAfnInventoryData = new Mws_afn_inventory_data();
                                }
                                $mwsAfnInventoryData->user_marketplace_id = $amazon_report->user_marketplace_id;
                                $mwsAfnInventoryData->product_id = @$productId->id;
                                $mwsAfnInventoryData->seller_sku = $row['seller-sku'];
                                $mwsAfnInventoryData->fulfilment_channel_sku = $row['fulfillment-channel-sku'];
                                $mwsAfnInventoryData->asin = $row['asin'];
                                $mwsAfnInventoryData->condition_type = $row['condition-type'];
                                $mwsAfnInventoryData->warehouse_condition_code = $row['Warehouse-Condition-code'];
                                $mwsAfnInventoryData->quantity_available = @$row['Quantity Available'];
                                $mwsAfnInventoryData->save();
                            }
                        
                    }
                }
            }
        }
        Log::debug("Ended Reading MWS AFN Inventory report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");

        $amazon_report->processed = 1;
        $amazon_report->save();
    }
}

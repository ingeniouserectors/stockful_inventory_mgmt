<?php

namespace App\Models;

use App\Classes\helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Mws_reserved_inventory extends Model
{
    //
    protected $table = 'mws_reserved_inventory';

    protected $fillable = [
        'user_marketplace_id', 'product_id', 'asin', 'fnsku', 'sku',
        'reserved_qty', 'reserved_customer_orders', 'reserved_fc_transfer','reserved_fc_processing'
    ];

    /**
     * Get the user marketplace that owns the reserved inventory..
     *
     * @param  no-params
     *
     */
    public function usermarketplace()
    {
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
    /**
     * Get the product that owns the reserved inventory..
     *
     * @param  no-params
     *
     */
    public function mws_product()
    {
        return $this->belongsTo('App\Models\Mws_product', 'product_id');
    }
    /**
     * Logic for process reports..
     *
     * @param  amazon_report
     *         report_stream
     *         marketplace
     *
     */
    public static function processReport(Amazon_requestlog &$amazon_report, $report_stream, Usermarketplace $markatplace)
    {
        $first = false;
        $numHeaders = 0;

        Log::debug("Started Reading MWS Reserved Inventory report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        while (!feof($report_stream)) {

            $row = fgetcsv($report_stream, 10000, "\t");
            if (!$first) {
                $first = true;
                $headers = $row;
                $numHeaders = count($headers);
                continue;
            }
            if (!empty($row)) {
                // truncate row data if item count exceeds number of headers
                if (count($row) > $numHeaders) {
                    $row = array_slice($row, 0, $numHeaders);
                }
                $row = array_combine($headers, $row);
                $productId = Mws_product::where('sku', $row['sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                if (empty($productId)) {
                    $fetchImage = new helper();
                    $productData = $fetchImage->getProductImage($row['asin'], $markatplace);
                    $imageURL = str_replace('SL75','UL1448',$productData['imageURL']);
                    $productId = new Mws_product();
                    $productId->user_marketplace_id = $amazon_report->user_marketplace_id;
                    $productId->sku = $row['sku'];
                    $productId->prod_name = utf8_encode($row['product-name']);
                    $productId->fnsku = $row['fnsku'];
                    $productId->asin = $row['asin'];
                    $productId->prod_image = ($imageURL ? $imageURL[0] : '');
                    $productId->save();
                }
//                else{
//                    $productId->prod_name = utf8_encode($row['product-name']);
//                    $productId->save();
//                }
                if (isset($row['sku'])) {
                    $mwsReservedInventoryData = Mws_reserved_inventory::where('sku', $row['sku'])->where('fnsku', $row['fnsku'])
                        ->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                    if ($mwsReservedInventoryData) {
                        $storageStatus = 'updated';
                    } else {
                        $storageStatus = 'created';
                        $mwsReservedInventoryData = new Mws_reserved_inventory();
                    }
                    $mwsReservedInventoryData->user_marketplace_id = $amazon_report->user_marketplace_id;
                    $mwsReservedInventoryData->product_id = $productId->id;
                    $mwsReservedInventoryData->sku = $row['sku'];
                    $mwsReservedInventoryData->fnsku = $row['fnsku'];
                    $mwsReservedInventoryData->asin = $row['asin'];
                    $mwsReservedInventoryData->reserved_qty = $row['reserved_qty'];
                    $mwsReservedInventoryData->reserved_customer_orders = $row['reserved_customerorders'];
                    $mwsReservedInventoryData->reserved_fc_transfer = $row['reserved_fc-transfers'];
                    $mwsReservedInventoryData->reserved_fc_processing = $row['reserved_fc-processing'];
                    $mwsReservedInventoryData->save();
                }
            }
        }
        Log::debug("Ended Reading MWS Reserved Inventory report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");

        $amazon_report->processed = 1;
        $amazon_report->save();
    }

    public static function processSPAPIReport(Amazon_requestlog &$amazon_report, $report_stream, SPApiUserMarketplace $markatplace)
    {
        $numHeaders = 0;
        Log::debug("Started Reading MWS Reserved Inventory report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        if (!empty($report_stream)) {
            $headers = array();
            $tmp = explode("\n", $report_stream);
            foreach ($tmp as $lineCount => $finaldata) {
                if (!empty($finaldata)) {
                    $row = explode("\t", $finaldata);
                    print_r($row);
                    if ($lineCount == 0) {
                        $headers = $row;
                        $numHeaders = count($headers);
                    }
                    if ($lineCount > 0) {
                        if (count($row) > $numHeaders) {
                            $row = array_slice($row, 0, $numHeaders);
                        }
                        $row = array_combine($headers, $row);
                        echo "<pre />";
                        print_r($row);
                        $productId = Mws_product::where('sku', $row['sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                        if (empty($productId)) {

                        }
                        else {
                            if (isset($row['sku'])) {
                                $mwsReservedInventoryData = Mws_reserved_inventory::where('sku', $row['sku'])->where('fnsku', $row['fnsku'])
                                    ->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                                if ($mwsReservedInventoryData) {
                                    $storageStatus = 'updated';
                                } else {
                                    $storageStatus = 'created';
                                    $mwsReservedInventoryData = new Mws_reserved_inventory();
                                }
                                $mwsReservedInventoryData->user_marketplace_id = $amazon_report->user_marketplace_id;
                                $mwsReservedInventoryData->product_id = $productId->id;
                                $mwsReservedInventoryData->sku = $row['sku'];
                                $mwsReservedInventoryData->fnsku = $row['fnsku'];
                                $mwsReservedInventoryData->asin = $row['asin'];
                                $mwsReservedInventoryData->reserved_qty = $row['reserved_qty'];
                                $mwsReservedInventoryData->reserved_customer_orders = $row['reserved_customerorders'];
                                $mwsReservedInventoryData->reserved_fc_transfer = $row['reserved_fc-transfers'];
                                $mwsReservedInventoryData->reserved_fc_processing = $row['reserved_fc-processing'];
                                $mwsReservedInventoryData->save();
                            }
                        }
                    }
                }
            }
        }
        Log::debug("Ended Reading MWS Reserved Inventory report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");

        $amazon_report->processed = 1;
        $amazon_report->save();
    }
}

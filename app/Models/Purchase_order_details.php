<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Purchase_order_details  extends Model
{
    
    protected $table = 'purchase_order_details';

    protected $fillable = [
        'purchase_orders_id',
        'product_id',
        'supplier',
        'vendor',
        'warehouse',
        'projected_units',
        'items_per_cartoons',
        'cbm',
        'containers',
        'subtotal'
       
    ];
    /**
     * Get the purchase instance that owns the purchase order details.
     *
     * @param  no-params
     *
     */
     public function puraches_instances(){
        return $this->belongsTo('App\Models\Purchase_instances','purchesinstances_id');
    }
    /**
     * Get the purchase order that owns the purchase order details.
     *
     * @param  no-params
     *
     */
    public function purchase_orders(){
        return $this->belongsTo('App\Models\Purchase_orders','purchase_orders_id');
    }
    /**
     * Get the product for the purchase order details.
     *
     * @param  no-params
     *
     */
    public function product(){
        return $this->hasOne('App\Models\Mws_product','id');
    }



}

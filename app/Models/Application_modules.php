<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Application_modules extends Model
{
    //
    protected $table = 'application_modules';

    protected $fillable = [
        'module',
        'table_name'
    ];
    /**
     * Get the role wise access for the application modules.
     *
     * @param  no-params
     *
     */
    public function role_access_modules(){
        return $this->hasMany('App\Models\Role_access_modules', 'application_module_id');
    }
    /**
     * Get the user wise access for the application modules.
     * 
     * @param  no-params
     *
     */
    public function user_access_modules(){
        return $this->hasMany('App\Models\User_access_modules', 'application_module_id');
    }
}

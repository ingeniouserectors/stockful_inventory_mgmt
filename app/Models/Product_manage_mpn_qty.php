<?php

namespace App\Models;

use App\Classes\helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Product_manage_mpn_qty extends Model
{
    //
    protected $table = 'product_manage_mpn_qty';
    protected $fillable = [
        'product_import_id',
        'product_assign_id',
        'qty',
        'shipping_option'
    ];
}

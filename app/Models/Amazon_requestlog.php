<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Amazon_requestlog extends Model
{
    //
    protected $table = 'amazon_reportrequestlog';
    public static $MERCHANT_LISTINGS = "_GET_FBA_ESTIMATED_FBA_FEES_TXT_DATA_";
    public static $ORDER_MERCHANGT_LISTINGS = "_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_LAST_UPDATE_";
    public static $MWS_EXCESS_INVENTORY_DATA_LISTINGS = "_GET_EXCESS_INVENTORY_DATA_";
    public static $MWS_AFN_INVENTORY_DATA = "_GET_AFN_INVENTORY_DATA_";
    public static $MWS_WAREHOUSE_WISE_INVENTORY_DATA = "_GET_FBA_FULFILLMENT_CURRENT_INVENTORY_DATA_";
    public static $MWS_RESERVED_INVENTORY_DATA = "_GET_RESERVED_INVENTORY_DATA_";
    public static $MWS_UNSUPPRESSED_INVENTORY_DATA = "_GET_FBA_MYI_UNSUPPRESSED_INVENTORY_DATA_";
    public static $ADJUSTMENT_DATA ="_GET_FBA_FULFILLMENT_INVENTORY_ADJUSTMENTS_DATA_";
    public static $MWS_DAILY_INVENTORY_REPORT ="_GET_FBA_FULFILLMENT_CURRENT_INVENTORY_DATA_";
    public static $MWS_ORDER_RETURNS_LISTINGS="_GET_FBA_FULFILLMENT_CUSTOMER_RETURNS_DATA_";
    public static $MWS_RESTOCK_INVENTORY_RECOMMENDATIONS="_GET_RESTOCK_INVENTORY_RECOMMENDATIONS_REPORT_";
    public static $DONE = "_DONE_";
    public static $SUBMITTED = "_SUBMITTED_";
    public static $IN_PROGRESS = "_IN_PROGRESS_";
    public static $CANCELLED = "_CANCELLED_";
    public static $NO_DATA = "_DONE_NO_DATA_";

    //SP API Report Types
    public static $SP_API_MERCHANT_LISTINGS = "GET_FBA_ESTIMATED_FBA_FEES_TXT_DATA";
    public static $SP_API_EXCESS_INVENTORY_DATA_LISTINGS = "GET_EXCESS_INVENTORY_DATA";
    public static $SP_API_AFN_INVENTORY_DATA = "GET_AFN_INVENTORY_DATA";
    public static $SP_API_WAREHOUSE_WISE_INVENTORY_DATA = "GET_FBA_FULFILLMENT_CURRENT_INVENTORY_DATA";
    public static $SP_API_RESERVED_INVENTORY_DATA = "GET_RESERVED_INVENTORY_DATA";
    public static $SP_API_UNSUPPRESSED_INVENTORY_DATA = "GET_FBA_MYI_UNSUPPRESSED_INVENTORY_DATA";
    public static $SP_API_ADJUSTMENT_DATA ="GET_FBA_FULFILLMENT_INVENTORY_ADJUSTMENTS_DATA";
    public static $SP_API_DAILY_INVENTORY_REPORT ="GET_FBA_FULFILLMENT_CURRENT_INVENTORY_DATA";
    public static $SP_API_ORDER_RETURNS_LISTINGS="GET_FBA_FULFILLMENT_CUSTOMER_RETURNS_DATA";
    public static $SP_API_RESTOCK_INVENTORY_RECOMMENDATIONS="GET_RESTOCK_INVENTORY_RECOMMENDATIONS_REPORT";
    
    public static $ORDER_DATA_LAST_UPDATE_GENERAL = "GET_FLAT_FILE_ALL_ORDERS_DATA_BY_LAST_UPDATE_GENERAL";
    public static $RETURNS_DATA_BY_RETURN_DATE = "GET_FLAT_FILE_RETURNS_DATA_BY_RETURN_DATE";

    public static $DONESPAPI = "DONE";
    public static $FATALSPAPI = "FATAL";
    public static $IN_PROGRESSSPAPI = "IN_PROGRESS";
    public static $IN_QUEUESPAPI = "IN_QUEUE";
    public static $CANCELLEDSPAPI = "CANCELLED";
    public static $NO_DATASPAPI = "DONE_NO_DATA";

    protected $fillable = [
        'user_marketplace_id', 'marketplace_id', 'request_id', 'report_id', 'status', 'available_at',
        'last_checked', 'report_type', 'request_response','processed', 'amz_dateitetration_id'
    ];

    /**
     * Get the user marketplace that owns the Reports Log..
     *
     * @param  no-params
     *
     */
    public function usermarketplace(){
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
    /**
     * Get the user marketplace that owns the Pending Reports Log with low priority..
     *
     * @param  usermarketplaceid
     *
     */
    public static function getPendingReports($usermarketplaceid){
        return self::where('last_checked', "<" , Carbon::parse('1 minutes ago'))
            ->whereNotIn("report_type",["_GET_FBA_FULFILLMENT_CURRENT_INVENTORY_DATA_","_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_LAST_UPDATE_"])
            ->whereUserMarketplaceId($usermarketplaceid)
            ->whereProcessed(0)->get();
    }
    /**
     * Get the user marketplace that owns the Pending Reports Log with high priority..
     *
     * @param  usermarketplaceid
     *
     */
    public static function getPendingReportsHigh($usermarketplaceid){
        return self::where('last_checked', "<" , Carbon::parse('1 minutes ago'))
            ->whereUserMarketplaceId($usermarketplaceid)
            ->whereReportType('_GET_FBA_FULFILLMENT_CURRENT_INVENTORY_DATA_')
            ->whereProcessed(0)->get();
    }
    /**
     * Get the user marketplace that owns the Pending Order Reports Log..
     *
     * @param  usermarketplaceid
     *
     */
    public static function getPendingReportsOrder($usermarketplaceid){
        return self::where('last_checked', "<" , Carbon::parse('1 minutes ago'))
            ->whereUserMarketplaceId($usermarketplaceid)
            ->whereReportType('_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_LAST_UPDATE_')
            ->whereProcessed(0)->get();
    }
    /**
     * Get the user marketplace that owns the Pending SPAPI Reports Log..
     *
     * @param  usermarketplaceid
     *
     */
    public static function getPendingSPAPIReports($usermarketplaceid){
        return self::where('last_checked', "<" , Carbon::parse('1 minutes ago'))
            ->whereUserMarketplaceId($usermarketplaceid)
            ->whereProcessed(0)->get();
    }


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vendor extends Model
{
    use SoftDeletes;
    protected $table = 'vendors';

    protected $fillable = [
        'user_marketplace_id',
        'vendor_type',
        'vendor_name',
        'address_line_1',
        'address_line_2',
        'city',
        'state',
        'zipcode',
        'country_id'
    ];

    /**
     * Get the marketplace country that owns the vendor.
     *
     * @param  no-params
     *
     */
    public function usermarketplace(){
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
    /**
     * Get the contacts for the vendor.
     *
     * @param  no-params
     *
     */
    public function vendor_contact(){
        return $this->hasMany('App\Models\Vendor_contact','vendor_id');
    }
    /**
     * Get the products for the vendor.
     *
     * @param  no-params
     *
     */
    public function vendor_product(){
        return $this->hasMany('App\Models\Vendor_product','vendor_id');
    }
    /**
     * Get the vendor wise default setting for the vendor.
     *
     * @param  no-params
     *
     */
    public function vendor_wise_setting(){
        return $this->hasMany('App\Models\Vendor_wise_default_setting','vendor_id');
    }
    /**
     * Get the blackout date for the vendor.
     *
     * @param  no-params
     *
     */
    public function vendor_blackout_date(){
        return $this->hasMany('App\Models\Vendor_blackout_date','vendor_id');
    }
    /**
     * Get the country that owns the vendor.
     *
     * @param  no-params
     *
     */
    public function country(){
        return $this->belongsTo('App\Models\Country', 'country_id');
    }

    public function shipping(){
        return $this->hasOne('App\Models\Shipping');
    }
}

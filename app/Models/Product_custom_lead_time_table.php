<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_custom_lead_time_table extends Model
{
    protected $table = 'product_custom_lead_time_table';

    protected $fillable = [
        'product_id',
        'product_assign_id',
        'type',
        'product_manuf_days',
		'to_port_days',
        'transit_time_days',
        'to_warehouse_days',
        'to_amazon',
        'po_to_production_days',
        'safety_days',
        'total_lead_time_days',
        'order_prep_days',
        'po_to_prep_days',
        'order_volume_value',
        'order_volume_id'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SPApiMarketplaceDetails extends Model
{
    protected $table = 'sp_api_marketplace_details';

    protected $fillable = [
        'developer_account_number',
        'marketplace_region',
        'marketplace_slug',
        'marketplace_id',
        'marketplace_url',
        'marketplace_country',
        'app_id',
        'access_key',
        'secret_key',
        'client_id',
        'client_secret',
        'role_arn',
        'marketplace_status'
    ];

    /**
     * Get the user  that owns the user marketplace.
     *
     * @param  no-params
     *
     */
    public function user(){
        return $this->belongsTo('App\Models\Users');
    }

    public function market_place(){
        return $this->belongsTo('App\Models\SPApiUserMarketplace','marketplace_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShippingDeliveryDetails extends Model
{
    use SoftDeletes;
    protected $table = 'shipping_delivery_details';

    protected $fillable = [
        'shipping_id',
        'delivery_schedule_type',
        'schedule_day'
    ];

    /**
     * Get the user marketplace that owns the shipping agent.
     *
     * @param  no-params
     *
     */
    public function usermarketplace(){
        return $this->belongsTo('App\Models\Shipping', 'id');
    }
}

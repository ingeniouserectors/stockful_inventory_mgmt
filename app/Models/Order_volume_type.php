<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order_volume_type  extends Model
{
    //
    protected $table = 'order_volume_type';

    protected $fillable = [
        'volume_type',
        
    ];

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Member_blackoutdate_setting extends Model
{


    protected $table = 'member_blackoutdate_setting';

    protected $fillable = [
        'user_marketplace_id',
        'blackout_date',
        'reason',
        'country_id'
    ];
    /**
     * Get the user marketplace that owns the blackout dates..
     *
     * @param  no-params
     *
     */
    public function usermarketplace(){
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
    public function country(){
        return $this->belongsTo('App\Models\Country','country_id');
    }
}

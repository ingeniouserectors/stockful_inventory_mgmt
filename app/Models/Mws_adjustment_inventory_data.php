<?php

namespace App\Models;

use App\Classes\helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Mws_adjustment_inventory_data extends Model
{
    //
    protected $table = 'mws_adjustment_inventory_datas';

    protected $fillable = [
        'user_marketplace_id', 'product_id', 'adjusted_date', 'transaction_item_id', 'fnsku',
        'sku', 'product_name', 'fulfillment_center_id', 'quantity', 'reason',
        'disposition', 'reconciled', 'unreconciled'
    ];
    /**
     * Get the user marketplace that owns the adjustment inventory..
     *
     * @param  no-params
     *
     */
    public function usermarketplace()
    {
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
    /**
     * Get the product that owns the adjustment inventory..
     *
     * @param  no-params
     *
     */
    public function mws_product()
    {
        return $this->belongsTo('App\Models\Mws_product', 'product_id');
    }
    /**
     * Logic for process reports..
     *
     * @param  amazon_report
     *         report_stream
     *         marketplace
     *
     */
    public static function processReport(Amazon_requestlog &$amazon_report, $report_stream, Usermarketplace $markatplace)
    {
        $first = false;
        $numHeaders = 0;

        Log::debug("Started Reading MWS Adjustment Inventory report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        while (!feof($report_stream)) {

            $row = fgetcsv($report_stream, 10000, "\t");
            if (!$first) {
                $first = true;
                $headers = $row;
                $numHeaders = count($headers);
                continue;
            }
            if (!empty($row)) {
                // truncate row data if item count exceeds number of headers
                if (count($row) > $numHeaders) {
                    $row = array_slice($row, 0, $numHeaders);
                }

                $row = array_combine($headers, $row);
                $productId = Mws_product::where('sku', $row['sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                if (empty($productId)) {
                    $fetchImage = new helper();
                    $productData =  $fetchImage->getProductImage($row['asin'], $markatplace);
                    $imageURL = str_replace('SL75','UL1448',$productData['imageURL']);
                    $productId = new Mws_product();
                    $productId->user_marketplace_id = $amazon_report->user_marketplace_id;
                    $productId->sku = $row['sku'];
                    $productId->fnsku = $row['fnsku'];
                    $productId->prod_name = utf8_encode($row['product_name']);
                    $productId->prod_image = ($imageURL ? $imageURL[0] : '');
                    $productId->save();
                }
//                else{
//                    $productId->prod_name = utf8_encode($row['product-name']);
//                    $productId->save();
//                }
                if (isset($row['sku'])) {
                    $mwsAdjustmentInventoryData = Mws_adjustment_inventory_data::where('sku', $row['sku'])->where('fnsku', $row['fnsku'])->where('adjusted_date', $row['adjusted-date'])->where('transaction_item_id', $row['transaction-item-id'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)
                        ->first();
                    if ($mwsAdjustmentInventoryData) {
                        $storageStatus = 'updated';
                    } else {
                        $storageStatus = 'created';
                        $mwsAdjustmentInventoryData = new Mws_adjustment_inventory_data();
                    }
                    $mwsAdjustmentInventoryData->user_marketplace_id = $amazon_report->user_marketplace_id;
                    $mwsAdjustmentInventoryData->product_id = $productId->id;
                    $mwsAdjustmentInventoryData->adjusted_date = date('Y-m-d h:m;s',strtotime($row['adjusted-date']));
                    $mwsAdjustmentInventoryData->transaction_item_id = $row['transaction-item-id'];
                    $mwsAdjustmentInventoryData->fnsku = $row['fnsku'];
                    $mwsAdjustmentInventoryData->sku = $row['sku'];
                    $mwsAdjustmentInventoryData->fulfillment_center_id = $row['fulfillment-center-id'];
                    $mwsAdjustmentInventoryData->quantity = $row['quantity'];
                    $mwsAdjustmentInventoryData->reason = $row['reason'];
                    $mwsAdjustmentInventoryData->disposition = $row['disposition'];
                    $mwsAdjustmentInventoryData->reconciled = $row['reconciled'];
                    $mwsAdjustmentInventoryData->unreconciled = $row['unreconciled'];
                    $mwsAdjustmentInventoryData->save();
                }
            }
        }
        Log::debug("Ended Reading MWS Adjustment Inventory report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");

        $amazon_report->processed = 1;
        $amazon_report->save();
    }

    public static function processSPAPIReport(Amazon_requestlog &$amazon_report, $report_stream, SPApiUserMarketplace $markatplace)
    {
        $numHeaders = 0;
        Log::debug("Started Reading MWS Adjustment Inventory report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        if (!empty($report_stream)) {
            $headers = array();
            $tmp = explode("\n", $report_stream);
            foreach ($tmp as $lineCount => $finaldata) {
                if (!empty($finaldata)) {
                    $row = explode("\t", $finaldata);
                    print_r($row);
                    if ($lineCount == 0) {
                        $headers = $row;
                        $numHeaders = count($headers);
                    }
                    if ($lineCount > 0) {
                        if (count($row) > $numHeaders) {
                            $row = array_slice($row, 0, $numHeaders);
                        }
                        $row = array_combine($headers, $row);
                        echo "<pre />";
                        print_r($row);
                        $productId = Mws_product::where('sku', $row['sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                        if (empty($productId)) {

                        } else {
                            if (isset($row['sku'])) {
                                $mwsAdjustmentInventoryData = Mws_adjustment_inventory_data::where('sku', $row['sku'])->where('fnsku', $row['fnsku'])->where('adjusted_date', $row['adjusted-date'])->where('transaction_item_id', $row['transaction-item-id'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)
                                    ->first();
                                if ($mwsAdjustmentInventoryData) {
                                    $storageStatus = 'updated';
                                } else {
                                    $storageStatus = 'created';
                                    $mwsAdjustmentInventoryData = new Mws_adjustment_inventory_data();
                                }
                                $mwsAdjustmentInventoryData->user_marketplace_id = $amazon_report->user_marketplace_id;
                                $mwsAdjustmentInventoryData->product_id = $productId->id;
                                $mwsAdjustmentInventoryData->adjusted_date = date('Y-m-d h:m;s', strtotime($row['adjusted-date']));
                                $mwsAdjustmentInventoryData->transaction_item_id = $row['transaction-item-id'];
                                $mwsAdjustmentInventoryData->fnsku = $row['fnsku'];
                                $mwsAdjustmentInventoryData->sku = $row['sku'];
                                $mwsAdjustmentInventoryData->fulfillment_center_id = $row['fulfillment-center-id'];
                                $mwsAdjustmentInventoryData->quantity = $row['quantity'];
                                $mwsAdjustmentInventoryData->reason = $row['reason'];
                                $mwsAdjustmentInventoryData->disposition = $row['disposition'];
                                $mwsAdjustmentInventoryData->reconciled = @$row['reconciled'];
                                $mwsAdjustmentInventoryData->unreconciled = @$row['unreconciled'];
                                $mwsAdjustmentInventoryData->save();
                            }
                        }
                    }
                }
            }
        }
        Log::debug("Ended Reading MWS Adjustment Inventory report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        $amazon_report->processed = 1;
        $amazon_report->save();
    }
}

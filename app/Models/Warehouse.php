<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Warehouse extends Model
{
    //
    use SoftDeletes;
    protected $table = 'warehouses';

    protected $fillable = [
        'user_marketplace_id',
        'country_id',
        'type',
        'primary_first_name',
        'primary_last_name',
        'primary_email',
        'address_line_1',
        'address_line_2',
        'city',
        'state',
        'zipcode',
        'warehouse_name',
    ];

    /**
     * Get the user marketplace that owns the warhouse.
     *
     * @param  no-params
     *
     */
    public function usermarketplace(){
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
    /**
     * Get the warehouse product for the warhouse.
     *
     * @param  no-params
     *
     */
    public function warehouse_product(){
        return $this->hasMany('App\Models\Warehouse_product');
    }
    /**
     * Get the warehouse wise setting for the warhouse.
     *
     * @param  no-params
     *
     */
    public function warehouse_wise_setting(){
        return $this->hasMany('App\Models\Warehouse_wise_default_setting','warehouse_id');
    }
    

}

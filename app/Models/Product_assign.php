<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_assign extends Model
{
    //
    protected $table = 'product_assign';

    protected $fillable = [
        'product_id',
        'supplier_vendor',
        'supplier_vendor_id',
        'primary_user',
        'leadtime_setting_type',
        'order_setting_type'
    ];
    /**
     * Get the user marketplace that owns the cron log..
     *
     * @param  no-params
     *
     */
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Amazon_shipment extends Model
{
    //
    protected $table="Amazon_shipment";
    protected $fillable = [
        'shipment_id',
        'product_id',
        'destination_name',
        'api_shipment_id',
        'seller_sku',
        'fulfillment_network_sku',
        'qty',
        'ship_to_address_name',
        'ship_to_address_line1',
        'ship_to_address_line2',
        'ship_to_city',
        'ship_to_state_code',
        'ship_to_country_code',
        'ship_to_postal_code',
        'label_prep_type',
        'total_units',
        'fee_per_unit_currency_code',
        'fee_per_unit_value',
        'total_fee_value',
        'status'
        
    ];

    
}

<?php

namespace App\Models;

use App\Classes\helper;
use App\Jobs\CalculateSalesDaily;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use App\Models\Mws_import_order;

class Mws_order extends Model
{
    protected $fillable = [
        'user_marketplace_id',
        'product_id',
        'amazon_order_id',
        'merchant_order_id',
        'purchase_date',
        'last_updated_date',
        'order_status',
        'fulfillment_channel',
        'sales_channel',
        'order_channel',
        'ship_service_level',
        'product_name',
        'sku',
        'asin',
        'item_status',
        'quantity',
        'currency',
        'item_price',
        'item_tax',
        'shipping_price',
        'shipping_tax',
        'gift_wrap_price',
        'gift_wrap_tax',
        'item_promotion_discount',
        'ship_promotion_discount',
        'ship_city',
        'ship_state',
        'ship_postal_code',
        'ship_country',
        'promotion_ids',
        'calculation_status',
        'converted_date'
    ];
    //
    protected $table = 'mws_orders';

    /**
     * Get the user marketplace that owns the orders..
     *
     * @param no-params
     *
     */
    public function usermarketplace()
    {
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }

    /**
     * Get the product that owns the orders..
     *
     * @param no-params
     *
     */
    public function mws_product()
    {
        return $this->belongsTo('App\Models\Mws_product', 'product_id');
    }

    /**
     * Logic for process reports..
     *
     * @param amazon_report
     *         report_stream
     *         marketplace
     *
     */
    public static function processReport(Amazon_requestlog &$amazon_report, $report_stream, Usermarketplace $markatplace)
    {
        $first = false;
        $numHeaders = 0;

        Log::debug("Started Reading MWS Order report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        while (!feof($report_stream)) {

            $row = fgetcsv($report_stream, 10000, "\t");
            if (!$first) {
                $first = true;
                $headers = $row;
                $numHeaders = count($headers);
                continue;
            }
            $mailForward = 0;
            $storageStatus = 'status unknown';

            if (!empty($row)) {
                // truncate row data if item count exceeds number of headers
                if (count($row) > $numHeaders) {
                    $row = array_slice($row, 0, $numHeaders);
                }

                $row = array_combine($headers, $row);
                if (strtolower($row['order-status']) == 'cancelled')
                    continue;
                $productId = Mws_product::where('sku', $row['sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                if (empty($productId)) {
                    $fetchImage = new helper();
                    $productData = $fetchImage->getProductImage($row['asin'], $markatplace);
                    $imageURL = str_replace('SL75', 'UL1448', $productData['imageURL']);
                    $productId = new Mws_product();
                    $productId->user_marketplace_id = $amazon_report->user_marketplace_id;
                    $productId->sku = $row['sku'];
                    $productId->asin = $row['asin'];
                    $productId->prod_name = utf8_encode($row['product-name']);
                    $productId->prod_image = ($imageURL ? $imageURL[0] : '');
                    $productId->fulfilledby = $row['fulfillment-channel'];
                    $productId->your_price = (!empty($row['item-price']) ? $row['item-price'] : '0');
                    $productId->sales_price = (!empty($row['item-price']) ? $row['item-price'] : '0');
                    $productId->currency = $row['currency'];
                    $productId->save();
                }
//                else{
//                    $productId->prod_name = utf8_encode($row['product-name']);
//                    $productId->your_price = (!empty($row['item-price']) ? $row['item-price'] : '0');
//                    $productId->sales_price = (!empty($row['item-price']) ? $row['item-price'] : '0');
//                    $productId->currency = $row['currency'];
//                    $productId->save();
//                }
//                $productCount = Product::where('account_id', $amazon_report->account->id)->count();
//                $syncProductFlag = Account::where('id', $amazon_report->account->id)->first();

//                if ($syncProductFlag['queue_url'] == 1) {
//                    $mailForward = 1;
//                }
//
//                if ($productCount == 0) {
//                    $mailForward = 1;
//                }
                if (isset($row['amazon-order-id'])) {
                    $amazone_order_id_unique = Mws_import_order::where(array('amazon_order_id' => $row['amazon-order-id']))->get()->toArray();
                    if (empty($amazone_order_id_unique)) {
                        $mwsOrder = Mws_order::where('amazon_order_id', $row['amazon-order-id'])->where('sku', $row['sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)
                            ->first();
                        if ($mwsOrder) {
                            $storageStatus = 'updated';
                        } else {
                            $calculatedSalesQty = Calculated_sales_qty::where('user_marketplace_id', $amazon_report->user_marketplace_id)->where('product_id', $productId->id)->where('order_date', date('Y-m-d', strtotime($row['purchase-date'])))->first();
                            if ($calculatedSalesQty)
                                $calculatedSalesQty->total_qty = $calculatedSalesQty->total_qty + $row['quantity'];
                            else {
                                $calculatedSalesQty = new Calculated_sales_qty();
                                $calculatedSalesQty->user_marketplace_id = $amazon_report->user_marketplace_id;
                                $calculatedSalesQty->product_id = $productId->id;
                                $calculatedSalesQty->order_date = date('Y-m-d', strtotime($row['purchase-date']));
                                $calculatedSalesQty->total_qty = $row['quantity'];
                            }
                            $calculatedSalesQty->save();
                            $maxDays = date('t', strtotime($row['purchase-date']));
                            $monthlyCalculatedSalesQty = Sales_total_trend_rate::where('user_marketplace_id', $amazon_report->user_marketplace_id)->where('prod_id', $productId->id)->where('month_year', date('Y-m', strtotime($row['purchase-date'])))->first();
                            if ($monthlyCalculatedSalesQty) {
                                $salesTotal = $monthlyCalculatedSalesQty->sales_total + $row['quantity'];
                                // Calculations for day rate
                                $monthlyCalculatedSalesQty->day_rate = round(($salesTotal / $maxDays), 6);
                                $monthlyCalculatedSalesQty->sales_total = $salesTotal;
                            } else {
                                $monthlyCalculatedSalesQty = new Sales_total_trend_rate();
                                $monthlyCalculatedSalesQty->user_marketplace_id = $amazon_report->user_marketplace_id;
                                $monthlyCalculatedSalesQty->prod_id = $productId->id;
                                $monthlyCalculatedSalesQty->month_year = date('Y-m', strtotime($row['purchase-date']));
                                $monthlyCalculatedSalesQty->sales_total = $row['quantity'];
                                $monthlyCalculatedSalesQty->day_rate = round((($row['quantity']) / $maxDays), 6);
                            }
                            $monthlyCalculatedSalesQty->save();
                            $storageStatus = 'created';
                            $mwsOrder = new Mws_order();
                        }
                        $mwsOrder->user_marketplace_id = $amazon_report->user_marketplace_id;
                        $mwsOrder->product_id = $productId->id;
                        $mwsOrder->amazon_order_id = $row['amazon-order-id'];
                        $mwsOrder->merchant_order_id = $row['merchant-order-id'];
                        $mwsOrder->purchase_date = $row['purchase-date'];
                        $mwsOrder->last_updated_date = $row['last-updated-date'];
                        $mwsOrder->order_status = $row['order-status'];
                        $mwsOrder->fulfillment_channel = $row['fulfillment-channel'];
                        $mwsOrder->sales_channel = $row['sales-channel'];
                        $mwsOrder->order_channel = $row['order-channel'];
                        $mwsOrder->ship_service_level = $row['ship-service-level'];
                        $mwsOrder->product_name = utf8_encode($row['product-name']);
                        $mwsOrder->sku = $row['sku'];
                        $mwsOrder->asin = $row['asin'];
                        $mwsOrder->item_status = $row['item-status'];
                        $mwsOrder->quantity = $row['quantity'];
                        $mwsOrder->currency = $row['currency'];
                        $mwsOrder->item_price = (!empty($row['item-price']) ? $row['item-price'] : '0');
                        $mwsOrder->item_tax = (!empty($row['item-tax']) ? $row['item-tax'] : '0');
                        $mwsOrder->shipping_price = (!empty($row['shipping-price']) ? $row['shipping-price'] : '0');
                        $mwsOrder->shipping_tax = (!empty($row['shipping-tax']) ? $row['shipping-tax'] : '0');
                        $mwsOrder->gift_wrap_price = (!empty($row['gift-wrap-price']) ? $row['gift-wrap-price'] : '0');
                        $mwsOrder->gift_wrap_tax = (!empty($row['gift-wrap-tax']) ? $row['gift-wrap-tax'] : '0');
                        $mwsOrder->item_promotion_discount = (!empty($row['item-promotion-discount']) ? $row['item-promotion-discount'] : '0');
                        $mwsOrder->ship_promotion_discount = (!empty($row['ship-promotion-discount']) ? $row['ship-promotion-discount'] : '0');
                        $mwsOrder->ship_city = $row['ship-city'];
                        $mwsOrder->ship_state = $row['ship-state'];
                        $mwsOrder->ship_postal_code = $row['ship-postal-code'];
                        $mwsOrder->ship_country = $row['ship-country'];
                        $mwsOrder->promotion_ids = $row['promotion-ids'];
                        $mwsOrder->save();
                    }
                }
            }
        }
        Log::debug("Ended Reading MWS Order report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");

//            if ($mailForward == 1) {
//                $account = Account::where('id', $amazon_report->account->id)->first();
//                $account->queue_url = null;
//                $account->save();
//                $user = User::where('id', $account['user_id'])->first();
//
//                $data['message'] = 'Your product list has been updated successfully. Please login and check your products.';
//                $data['subject'] = 'Your products have been updated - AMZSTARS';
//                $data['user_email'] = $user->email;
//
//                Mail::raw($data['message'], function ($m) use ($data) {
//                    $m->to($data['user_email'])->subject($data['subject']);
//                });
//            }

//            console("Saved a product!!" . $product->id);

        $amazon_report->processed = 1;
        $amazon_report->save();

        if ($amazon_report->amz_dateitetration_id > 0) {
            $amzDateitetration = Amzdateiteration::where('id', $amazon_report->amz_dateitetration_id)->first();
            if (!empty($amzDateitetration->id)) {
                $amzDateitetration->OrderStatus = '1';
                $amzDateitetration->save();
            }
        } else {
            $cronStatus = 0;
            $queueArray = \DB::table(config('queue.connections.database.table'))->get()->toArray();
            if (count($queueArray) > 0) {
                foreach ($queueArray as $queue) {
                    $payload = json_decode($queue->payload, true);
                    $payloadCommandData = unserialize($payload['data']['command']);
                    if ($payload['displayName'] == 'App\Jobs\CalculateSalesDaily' && $payloadCommandData->marketplace->user_id == $amazon_report->user_marketplace_id) {
                        $cronStatus = 1;
                        break;
                    }
                }
            }
            if ($cronStatus == 0) {
                $requestJob = new CalculateSalesDaily($markatplace);
                dispatch($requestJob)->onQueue('custom');

            }
        }
    }

    public static function processSPAPIReport(Amazon_requestlog &$amazon_report, $report_stream, SPApiUserMarketplace $markatplace)
    {
        $numHeaders = 0;
        Log::debug("Started Reading MWS Order report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        if (!empty($report_stream)) {
            $headers = array();
            $tmp = explode("\n", $report_stream);
            foreach ($tmp as $lineCount => $finaldata) {
                if (!empty($finaldata)) {
                    $row = explode("\t", $finaldata);
                    if ($lineCount == 0) {
                        $headers = $row;
                        $numHeaders = count($headers);
                    }
                    if ($lineCount > 0) {
                        if (count($row) > $numHeaders) {
                            $row = array_slice($row, 0, $numHeaders);
                        }
                        $row = array_combine($headers, $row);
                        if (strtolower($row['order-status']) == 'cancelled')
                            continue;
                        $productId = Mws_product::where('sku', $row['sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                        // print_r($productId);
                        if (empty($productId)) {          
                        } 
                        else {
                            if (isset($row['amazon-order-id'])) {
                                $amazone_order_id_unique = Mws_import_order::where(array('amazon_order_id' => $row['amazon-order-id']))->get()->toArray();
                                if (empty($amazone_order_id_unique)) {
                                    $mwsOrder = Mws_order::where('amazon_order_id', $row['amazon-order-id'])->where('sku', $row['sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)
                                        ->first();
                                    if ($mwsOrder) {
                                        $storageStatus = 'updated';
                                    } else {
                                        $calculatedSalesQty = Calculated_sales_qty::where('user_marketplace_id', $amazon_report->user_marketplace_id)->where('product_id', $productId->id)->where('order_date', date('Y-m-d', strtotime($row['purchase-date'])))->first();
                                        if ($calculatedSalesQty)
                                            $calculatedSalesQty->total_qty = $calculatedSalesQty->total_qty + $row['quantity'];
                                        else {
                                            $calculatedSalesQty = new Calculated_sales_qty();
                                            $calculatedSalesQty->user_marketplace_id = $amazon_report->user_marketplace_id;
                                            $calculatedSalesQty->product_id = $productId->id;
                                            $calculatedSalesQty->order_date = date('Y-m-d', strtotime($row['purchase-date']));
                                            $calculatedSalesQty->total_qty = $row['quantity'];
                                        }
                                        $calculatedSalesQty->save();
                                        $maxDays = date('t', strtotime($row['purchase-date']));
                                        $monthlyCalculatedSalesQty = Sales_total_trend_rate::where('user_marketplace_id', $amazon_report->user_marketplace_id)->where('prod_id', $productId->id)->where('month_year', date('Y-m', strtotime($row['purchase-date'])))->first();
                                        if ($monthlyCalculatedSalesQty) {
                                            $salesTotal = $monthlyCalculatedSalesQty->sales_total + $row['quantity'];
                                            // Calculations for day rate
                                            $monthlyCalculatedSalesQty->day_rate = round(($salesTotal / $maxDays), 6);
                                            $monthlyCalculatedSalesQty->sales_total = $salesTotal;
                                        } else {
                                            $monthlyCalculatedSalesQty = new Sales_total_trend_rate();
                                            $monthlyCalculatedSalesQty->user_marketplace_id = $amazon_report->user_marketplace_id;
                                            $monthlyCalculatedSalesQty->prod_id = $productId->id;
                                            $monthlyCalculatedSalesQty->month_year = date('Y-m', strtotime($row['purchase-date']));
                                            $monthlyCalculatedSalesQty->sales_total = $row['quantity'];
                                            $monthlyCalculatedSalesQty->day_rate = round((($row['quantity']) / $maxDays), 6);
                                        }
                                        $monthlyCalculatedSalesQty->save();
                                        $storageStatus = 'created';
                                        $mwsOrder = new Mws_order();
                                    }
                                    $mwsOrder->user_marketplace_id = $amazon_report->user_marketplace_id;
                                    $mwsOrder->product_id = $productId->id;
                                    $mwsOrder->amazon_order_id = $row['amazon-order-id'];
                                    $mwsOrder->merchant_order_id = $row['merchant-order-id'];
                                    $mwsOrder->purchase_date = $row['purchase-date'];
                                    $mwsOrder->last_updated_date = $row['last-updated-date'];
                                    $mwsOrder->order_status = $row['order-status'];
                                    $mwsOrder->fulfillment_channel = $row['fulfillment-channel'];
                                    $mwsOrder->sales_channel = $row['sales-channel'];
                                    $mwsOrder->order_channel = $row['order-channel'];
                                    $mwsOrder->ship_service_level = $row['ship-service-level'];
                                    $mwsOrder->product_name = utf8_encode($row['product-name']);
                                    $mwsOrder->sku = $row['sku'];
                                    $mwsOrder->asin = $row['asin'];
                                    $mwsOrder->item_status = $row['item-status'];
                                    $mwsOrder->quantity = $row['quantity'];
                                    $mwsOrder->currency = $row['currency'];
                                    $mwsOrder->item_price = (!empty($row['item-price']) ? $row['item-price'] : '0');
                                    $mwsOrder->item_tax = (!empty($row['item-tax']) ? $row['item-tax'] : '0');
                                    $mwsOrder->shipping_price = (!empty($row['shipping-price']) ? $row['shipping-price'] : '0');
                                    $mwsOrder->shipping_tax = (!empty($row['shipping-tax']) ? $row['shipping-tax'] : '0');
                                    $mwsOrder->gift_wrap_price = (!empty($row['gift-wrap-price']) ? $row['gift-wrap-price'] : '0');
                                    $mwsOrder->gift_wrap_tax = (!empty($row['gift-wrap-tax']) ? $row['gift-wrap-tax'] : '0');
                                    $mwsOrder->item_promotion_discount = (!empty($row['item-promotion-discount']) ? $row['item-promotion-discount'] : '0');
                                    $mwsOrder->ship_promotion_discount = (!empty($row['ship-promotion-discount']) ? $row['ship-promotion-discount'] : '0');
                                    $mwsOrder->ship_city = $row['ship-city'];
                                    $mwsOrder->ship_state = $row['ship-state'];
                                    $mwsOrder->ship_postal_code = $row['ship-postal-code'];
                                    $mwsOrder->ship_country = $row['ship-country'];
                                    $mwsOrder->promotion_ids = $row['promotion-ids'];
                                    $mwsOrder->save();
                                }
                            }
                        }
                    }
                }
            }
        }
        Log::debug("Ended Reading MWS Order report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        $amazon_report->processed = 1;
        $amazon_report->save();

        }
}

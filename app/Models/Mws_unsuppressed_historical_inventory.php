<?php

namespace App\Models;

use App\Classes\helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Mws_unsuppressed_historical_inventory extends Model
{
    //
    protected $table = 'mws_unsuppressed_historical_inventories';

    protected $fillable = [
        'user_marketplace_id', 'product_id', 'sku', 'fnsku', 'asin',
        'condition', 'yourprice', 'mfn_listing_exists','mfn_fulfilment_qty','afn_listing_exists','afn_warehouse_qty',
        'afn_fulfillable_qty','afn_unsellable_qty','afn_reserved_qty','afn_total_qty','per_unit_volume',
        'afn_inbound_working_qty','afn_inbound_shipment_qty','afn_inbound_receving_qty','afn_research_qty','afn_reserved_future_supply',
        'afn_future_supply_buyable','inventory_for_date'
    ];
    /**
     * Get the user marketplace that owns the unsuppressed historical inventory..
     *
     * @param  no-params
     *
     */
    public function usermarketplace()
    {
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
    /**
     * Get the product that owns the unsuppressed historical inventory..
     *
     * @param  no-params
     *
     */
    public function mws_product()
    {
        return $this->belongsTo('App\Models\Mws_product', 'product_id');
    }
    /**
     * Logic for process reports..
     *
     * @param  amazon_report
     *         report_stream
     *         marketplace
     *
     */
    public static function processReport($amazon_report, $report_stream, Usermarketplace $markatplace)
    {
        $first = false;
        $numHeaders = 0;
        Log::debug("Started Reading MWS Unsuppressed Historical Inventory report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        while (!feof($report_stream)) {

            $row = fgetcsv($report_stream, 10000, "\t");
            if (!$first) {
                $first = true;
                $headers = $row;
                $numHeaders = count($headers);
                continue;
            }
            if (!empty($row)) {
                // truncate row data if item count exceeds number of headers
                if (count($row) > $numHeaders) {
                    $row = array_slice($row, 0, $numHeaders);
                }
                $row = array_combine($headers, $row);
                $productId = Mws_product::where('sku', $row['sku'])->where('user_marketplace_id', $amazon_report['user_marketplace_id'])->first();
                if (isset($row['asin'])) {
                        $storageStatus = 'created';
                        $mwsUnsuppressedInventoryData = new Mws_unsuppressed_historical_inventory();
                    $mwsUnsuppressedInventoryData->user_marketplace_id = $amazon_report['user_marketplace_id'];
                    $mwsUnsuppressedInventoryData->product_id = $productId->id;
                    $mwsUnsuppressedInventoryData->sku = $row['sku'];
                    $mwsUnsuppressedInventoryData->fnsku = $row['fnsku'];
                    $mwsUnsuppressedInventoryData->asin = $row['asin'];
                    $mwsUnsuppressedInventoryData->condition = $row['condition'];
                    $mwsUnsuppressedInventoryData->yourprice = ($row['your-price'] ? $row['your-price'] : 0);
                    $mwsUnsuppressedInventoryData->mfn_listing_exists = $row['mfn-listing-exists'];
                    $mwsUnsuppressedInventoryData->mfn_fulfilment_qty = ($row['mfn-fulfillable-quantity'] ? $row['mfn-fulfillable-quantity'] : 0);
                    $mwsUnsuppressedInventoryData->afn_listing_exists = $row['afn-listing-exists'];
                    $mwsUnsuppressedInventoryData->afn_warehouse_qty = ($row['afn-warehouse-quantity'] ? $row['afn-warehouse-quantity'] : 0);
                    $mwsUnsuppressedInventoryData->afn_fulfillable_qty = ($row['afn-fulfillable-quantity'] ? $row['afn-fulfillable-quantity'] : 0);
                    $mwsUnsuppressedInventoryData->afn_unsellable_qty = ($row['afn-unsellable-quantity'] ? $row['afn-unsellable-quantity'] : 0);
                    $mwsUnsuppressedInventoryData->afn_reserved_qty = ($row['afn-reserved-quantity'] ? $row['afn-reserved-quantity'] : 0);
                    $mwsUnsuppressedInventoryData->afn_total_qty = ($row['afn-total-quantity'] ? $row['afn-total-quantity'] : 0);
                    $mwsUnsuppressedInventoryData->per_unit_volume = ($row['per-unit-volume'] ? $row['per-unit-volume'] : 0);
                    $mwsUnsuppressedInventoryData->afn_inbound_working_qty = ($row['afn-inbound-working-quantity'] ? $row['afn-inbound-working-quantity'] : 0);
                    $mwsUnsuppressedInventoryData->afn_inbound_shipment_qty = ($row['afn-inbound-shipped-quantity'] ? $row['afn-inbound-shipped-quantity'] : 0);
                    $mwsUnsuppressedInventoryData->afn_inbound_receving_qty = ($row['afn-inbound-receiving-quantity'] ? $row['afn-inbound-receiving-quantity'] : 0);
                    $mwsUnsuppressedInventoryData->afn_research_qty = ($row['afn-researching-quantity'] ? $row['afn-researching-quantity'] : 0);
                    $mwsUnsuppressedInventoryData->afn_reserved_future_supply = ($row['afn-reserved-future-supply'] ? $row['afn-reserved-future-supply'] : 0);
                    $mwsUnsuppressedInventoryData->afn_future_supply_buyable = ($row['afn-future-supply-buyable'] ? $row['afn-future-supply-buyable'] : 0);
                    $mwsUnsuppressedInventoryData->inventory_for_date = date('Y-m-d',strtotime($amazon_report['previousDate']));
                    $mwsUnsuppressedInventoryData->save();
                }
            }
        }
        Amazon_requestlog::where('id',$amazon_report['id'])->update(array('processed'=>2));
        Log::debug("Ended Reading MWS Unsuppressed Historical Inventory report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
    }
}

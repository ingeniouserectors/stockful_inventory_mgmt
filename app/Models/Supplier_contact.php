<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Supplier_contact extends Model
{
    //
    protected $table = 'supplier_contacts';

    protected $fillable = [
        'supplier_id',
        'first_name',
        'last_name',
        'email',
        'phone_number',
        'title',
        'primary'
    ];

    /**
     * Get the supplier that owns the contact.
     *
     * @param  no-params
     *
     */
    public function supplier(){
        return $this->belongsTo('App\Models\Supplier');
    }
}

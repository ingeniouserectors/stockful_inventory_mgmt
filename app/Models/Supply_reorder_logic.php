<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Supply_reorder_logic extends Model
{
    use SoftDeletes;
    protected $table = 'supply_reorder_logics';

    protected $fillable = [
        'user_marketplace_id',
        'logic_label_name',
        'supply_last_year_sales_percentage',
        'supply_recent_last_7_day_sales_percentage',
        'supply_recent_last_14_day_sales_percentage',
        'supply_recent_last_30_day_sales_percentage',
        'supply_trends_year_over_year_historical_percentage',
        'supply_trends_year_over_year_current_percentage',
        'supply_trends_multi_month_trend_percentage',
        'supply_trends_30_over_30_days_percentage',
        'supply_trends_multi_week_trend_percentage',
        'supply_trends_7_over_7_days_percentage',
        'reorder_last_year_sales_percentage',
        'reorder_recent_last_7_day_sales_percentage',
        'reorder_recent_last_14_day_sales_percentage',
        'reorder_recent_last_30_day_sales_percentage',
        'reorder_trends_year_over_year_historical_percentage',
        'reorder_trends_year_over_year_current_percentage',
        'reorder_trends_multi_month_trend_percentage',
        'reorder_trends_30_over_30_days_percentage',
        'reorder_trends_multi_week_trend_percentage',
        'reorder_trends_7_over_7_days_percentage',
        'reorder_safety_stock_percentage'
    ];
    /**
     * Get the user marketplace that owns the supply reorder logic.
     *
     * @param  no-params
     *
     */
    public function usermarketplace()
    {
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
    /**
     * Get the product assign supply logic for the supply reorder logic.
     *
     * @param  no-params
     *
     */
    public function product_assing_supply_logic(){
        return $this->hasMany('App\Models\Product_assign_supply_logic','label_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Usermarketplace extends Model
{
    protected $table = 'mws_user_marketplace';

    protected $fillable = [
        'user_id',
        'mws_sellerid',
        'mws_marketplaceid',
        'mws_authtoken',
        'country',
        'mws_report_url',
        'amazonstorename',
        'apiactive',
        'created_at',
        'updated_at',
        'deleted_at',
        'api_token'
    ];
    /**
     * Get the user  that owns the user marketplace.
     *
     * @param  no-params
     *
     */
    public function user(){
        return $this->belongsTo('App\Models\Users');
    }
    /**
     * Get the marketplace country for the user marketplace.
     *
     * @param  no-params
     *
     */
    public function mws_market_place_country(){
        return $this->hasOne('App\Models\Mwsdeveloperaccount','marketplace','country');
    }
    /**
     * Get the amazon request log for the user marketplace.
     *
     * @param  no-params
     *
     */
    public function amazon_requestlog(){
        return $this->hasMany('App\Models\Amazon_requestlog','user_marketplace_id');
    }
    /**
     * Get the date iteration for the user marketplace.
     *
     * @param  no-params
     *
     */
    public function amzdateiteration(){
        return $this->hasMany('App\Models\Amzdateiteration','user_marketplace_id');
    }
    /**
     * Get the cronlog for the user marketplace.
     *
     * @param  no-params
     *
     */
    public function cronlog(){
        return $this->hasMany('App\Models\Cronlog','user_marketplace_id');
    }
    /**
     * Get the daily logic calculations for the user marketplace.
     *
     * @param  no-params
     *
     */
    public function daily_logic_calculations(){
        return $this->hasMany('App\Models\Daily_logic_Calculations','user_marketplace_id');
    }
    /**
     * Get the vendor for the user marketplace.
     *
     * @param  no-params
     *
     */
    public function vendor(){
        return $this->hasMany('App\Models\Vendor','user_marketplace_id');
    }
    /**
     * Get the supplier for the user marketplace.
     *
     * @param  no-params
     *
     */
    public function supplier(){
        return $this->hasMany('App\Models\Supplier','user_marketplace_id');
    }
    /**
     * Get the warehouse for the user marketplace.
     *
     * @param  no-params
     *
     */
    public function warehouse(){
        return $this->hasMany('App\Models\Warehouse','user_marketplace_id'); 
    }
    /**
     * Get the order returns for the user marketplace.
     *
     * @param  no-params
     *
     */
    public function mws_order_returns(){
        return $this->hasMany('App\Models\Mws_order_returns','user_marketplace_id');
    }
    /**
     * Get the calculated sale quantity for the user marketplace.
     *
     * @param  no-params
     *
     */
    public function calculated_sale_qty(){
        return $this->hasMany('App\Models\Calculated_sales_qty','user_marketplace_id');
    }
    /**
     * Get the sales total trend rate for the user marketplace.
     *
     * @param  no-params
     *
     */
    public function sales_total_trend_rate(){
        return $this->hasMany('App\Models\Sales_total_trend_rate','user_marketplace_id');
    }
    /**
     * Get the purchase instance for the user marketplace.
     *
     * @param  no-params
     *
     */
    public function purchase_instance(){
        return $this->hasMany('App\Models\Purchase_instances','user_marketplace_id');
    }
    /**
     * Get the purchase order for the user marketplace.
     *
     * @param  no-params
     *
     */
    public function purchase_order(){
        return $this->hasMany('App\Models\Purchase_orders','user_marketplace_id');
    }
    /**
     * Get the supply reorder logic for the user marketplace.
     *
     * @param  no-params
     *
     */
    public function Supply_reorder_logic(){
        return $this->hasMany('App\Models\Supply_reorder_logic','user_marketplace_id');
    }
    /**
     * Get the reserved inventory for the user marketplace.
     *
     * @param  no-params
     *
     */
    public function mws_reserved_inventory()
    {
        return $this->hasMany('App\Models\Mws_reserved_inventory', 'user_marketplace_id');
    }
    /**
     * Get the warehouse wise inventory for the user marketplace.
     *
     * @param  no-params
     *
     */
    public function mws_warehouse_wise_inventory()
    {
        return $this->hasMany('App\Models\Mws_warehouse_wise_inventory', 'user_marketplace_id');
    }
    /**
     * Get the member blackout setting for the user marketplace.
     *
     * @param  no-params
     *
     */
    public function member_blackoutdate_setting()
    {
        return $this->hasMany('App\Models\Member_blackoutdate_setting', 'user_marketplace_id');
    }
    /**
     * Get the unsuppressed inventory for the user marketplace.
     *
     * @param  no-params
     *
     */
    public function mws_unsuppressed_inventory_data()
    {
        return $this->hasMany('App\Models\Mws_unsuppressed_inventory_data', 'user_marketplace_id');
    }
}

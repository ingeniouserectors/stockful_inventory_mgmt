<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mws_out_of_stock_detail extends Model
{
    //
    protected $table="mws_out_of_stock_detail";
    protected $fillable = [
        'user_marketplace_id',
        'product_id',
        'stock_date',
        'fnsku',
        'sku',
        'qty',
        'count_status'
    ];
    /**
     * Get the user marketplace that owns the out of stock..
     *
     * @param  no-params
     *
     */
    public function usermarketplace(){
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
    /**
     * Get the product that owns the out of stock..
     *
     * @param  no-params
     *
     */
    public function mws_product(){
        return $this->belongsTo('App\Models\Mws_product', 'product_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inbound_shipment_items extends Model
{
    //
    protected $table = 'inbound_shipment_items';

    protected $fillable = [
        'inbound_shipment_id',
        'product_id',
        'QuantityShipped',
        'QuantityReceived',
        'QuantityInCase',
        'PrepDetailsList',
        'ReleaseDate',
    ];
    /**
     * Get the inbound shipment that owns the inbound shipment items..
     *
     * @param  no-params
     *
     */
    public function inbound_shipment(){
        return $this->belongsTo('App\Models\Inbound_shipments','inbound_shipment_id');
    }
    /**
     * Get the product that owns the inbound shipment items..
     *
     * @param  no-params
     *
     */
    public function mws_product()
    {
        return $this->belongsTo('App\Models\Mws_product', 'product_id');
    }

}

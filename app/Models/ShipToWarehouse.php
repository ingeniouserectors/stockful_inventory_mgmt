<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShipToWarehouse extends Model
{
    use SoftDeletes;
    protected $table = 'ship_to_warehouse';

    protected $fillable = [
        'shipping_id',
        'warehouse_value',
    ];

    public function shipping(){
        return $this->belongsTo('App\Models\shipping', 'id');
    }
}

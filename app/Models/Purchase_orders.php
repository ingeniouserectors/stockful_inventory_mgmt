<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Purchase_orders  extends Model
{
    
    protected $table = 'purchase_orders';

    protected $fillable = [
        'user_marketplace_id',
        'purchesinstances_id',
        'track_id',
        'date_of_ship',
        'date_arrived',
        'reorder_date',
        'day_rate',
        'days_of_supply',
        'blackout_days',
        'status',
        'referance_id'
    ];
    /**
     * Get the purchase order details for the purchase order.
     *
     * @param  no-params
     *
     */
    public function purachse_order_details(){
        return $this->hasOne('App\Models\Purchase_order_details','purchase_orders_id');
    }
    /**
     * Get the product for the purchase order.
     *
     * @param  no-params
     *
     */
    public function product(){
        return $this->hasOne('App\Models\Mws_product','id');
    }
}

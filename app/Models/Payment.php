<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //
    protected $table = 'payment';

    protected $fillable = [
        'supplier_vendor_id',
        'supplier_vendor',
    ];

}

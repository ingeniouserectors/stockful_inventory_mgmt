<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Amzdateiteration extends Model
{
    //
    protected $table = 'amzdateiteration';

    protected $fillable = [
        'user_marketplace_id',
        'startDate',
        'endDate',
        'OrderStatus',
        'InventoryStatus',
        'ReimburseStatus',
        'ReturnStatus',
        'AdjustedCalculations',
        'FeedbackStatus',
        'CustomersalesStatus',
        'InventoryshipmentStatus',
        'ShipmentOrderRemovalStatus'
    ];

    /**
     * Get the user marketplace that owns the date iterations..
     *
     * @param  no-params
     *
     */
    public function usermarketplace(){
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_lead_time_value extends Model
{
    protected $table = 'product_lead_time_value';

    protected $fillable = [
        'lead_time_id',
        'lead_time_detail_id',
        'send_mail',
        'no_of_days',
        'contact_detail'
    ];
}

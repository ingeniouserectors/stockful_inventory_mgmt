<?php

namespace App\Models;

use App\Classes\helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Mws_inventory_details extends Model
{
    //
    protected $table = "mws_inventory_details";
    protected $fillable = [
        'user_marketplace_id',
        'product_id',
        'snapshot_date',
        'fnsku',
        'sku',
        'prod_name',
        'qty',
        'fulfilment_center_id',
        'detailed_disposition',
        'country',
        'count_status'
    ];
    /**
     * Get the user marketplace that owns the inventory details..
     *
     * @param  no-params
     *
     */
    public function usermarketplace()
    {
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
    /**
     * Get the product that owns the inventory details..
     *
     * @param  no-params
     *
     */
    public function mws_product()
    {
        return $this->belongsTo('App\Models\Mws_product', 'product_id');
    }
    /**
     * Logic for process reports..
     *
     * @param  amazon_report
     *         report_stream
     *         marketplace
     *
     */
    public static function processReport(Amazon_requestlog &$amazon_report, $report_stream, Usermarketplace $markatplace)
    {
        $first = false;
        $numHeaders = 0;
//        $requestConfiguration = Mws_product::getConfiguration($markatplace);
//        $finance = new \ClouSale\AmazonSellingPartnerAPI\Api\CatalogApi($requestConfiguration);

        Log::debug("Started Reading MWS Inventory Details report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        while (!feof($report_stream)) {

            $row = fgetcsv($report_stream, 10000, "\t");
            if (!$first) {
                $first = true;
                $headers = $row;
                $numHeaders = count($headers);
                continue;
            }
            if (!empty($row)) {
                // truncate row data if item count exceeds number of headers
                if (count($row) > $numHeaders) {
                    $row = array_slice($row, 0, $numHeaders);
                }
                // echo '<pre>';
                // print_r($row);
                $row = array_combine($headers, $row);
                $productId = Mws_product::where('sku', $row['sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                if (empty($productId)) {
//                    $shipmentData = $finance->getCatalogItem($markatplace->market_place_developer->marketplace_id,$row['asin']);
//                    $payload = $shipmentData->getPayload();
//                    $get_attribute = $payload->getAttributeSets();
//                    $product_title = @$get_attribute[0]["title"];
//                    $product_image = @$get_attribute[0]["small_image"]['url'];
//                    $product_brand = @$get_attribute[0]["brand"];
//                    $product_prod_group = @$get_attribute[0]["product_group"];
//                    dd($get_attribute);
//
                    $productId = new Mws_product();
//                    $productId->user_marketplace_id = $amazon_report->user_marketplace_id;
//                    $productId->sku = $row['sku'];
//                    $productId->fnsku = $row['fnsku'];
//                    $productId->asin = $row['asin'];
//                    $productId->prod_image = $product_image;
//                    $productId->prod_name = $product_title;
//                    $productId->brand = $product_brand;
//                    $productId->prod_group = $product_prod_group;
//                    $productId->save();

                     $productId->user_marketplace_id = $amazon_report->user_marketplace_id;
                     $productId->sku = $row['sku'];
                     $productId->prod_name = utf8_encode($row['product-name']);
                     $productId->save();
                }
                else {
                    $productId->prod_name = utf8_encode($row['product-name']);
                    $productId->save();
                }
                if (isset($row['sku'])) {
                    $mwsInventoryDetails = Mws_inventory_details::where('snapshot_date', date('Y-m-d',strtotime($row['snapshot-date'])))->where('sku', $row['sku'])->where('fulfilment_center_id', $row['fulfillment-center-id'])
                        ->where('user_marketplace_id', $amazon_report->user_marketplace_id)
                        ->first();
                    if ($mwsInventoryDetails) {
                        $storageStatus = 'updated';
                    } else {
                        $mwsInventoryCalculation = Mws_calculated_inventory_daily::where('fnsku', $row['fnsku'])->where('inventory_date', date('Y-m-d',strtotime($row['snapshot-date'])))
                            ->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                        if ($row['detailed-disposition'] == 'SELLABLE') {
                            if ($mwsInventoryCalculation) {
                                $mwsInventoryCalculation->qty = $mwsInventoryCalculation->qty + $row['quantity'];
                            } else {
                                $mwsInventoryCalculation = new Mws_calculated_inventory_daily();
                                $mwsInventoryCalculation->user_marketplace_id = $amazon_report->user_marketplace_id;
                                $mwsInventoryCalculation->product_id = $productId->id;
                                $mwsInventoryCalculation->fnsku = $row['fnsku'];
                                $mwsInventoryCalculation->qty = $row['quantity'];
                                $mwsInventoryCalculation->inventory_date = date('Y-m-d',strtotime($row['snapshot-date']));
                            }
                            $mwsInventoryCalculation->save();
                        }
                        $storageStatus = 'created';
                        $mwsInventoryDetails = new Mws_inventory_details();
                    }
                    $mwsInventoryDetails->user_marketplace_id = $amazon_report->user_marketplace_id;
                    $mwsInventoryDetails->product_id = $productId->id;
                    $mwsInventoryDetails->snapshot_date = date('Y-m-d',strtotime($row['snapshot-date']));
                    $mwsInventoryDetails->fnsku = $row['fnsku'];
                    $mwsInventoryDetails->sku = $row['sku'];
                    $mwsInventoryDetails->prod_name = utf8_encode($row['product-name']);
                    $mwsInventoryDetails->qty = $row['quantity'];
                    $mwsInventoryDetails->fulfilment_center_id = $row['fulfillment-center-id'];
                    $mwsInventoryDetails->detailed_disposition = $row['detailed-disposition'];
                    $mwsInventoryDetails->country = $row['country'];
                    $mwsInventoryDetails->count_status = 1;
                    $mwsInventoryDetails->save();
                }

            }
        }
        $amazon_report->processed = 1;
        $amazon_report->save();
        if ($amazon_report->amz_dateitetration_id > 0) {
            $amzDateitetration = Amzdateiteration::where('id', $amazon_report->amz_dateitetration_id)->first();
            if (!empty($amzDateitetration->id)) {
                $amzDateitetration->InventoryStatus = '1';
                $amzDateitetration->save();
            }
        }
        Log::debug("Ended Reading MWS Inventory Details report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
    }

    public static function processSPAPIReport(Amazon_requestlog &$amazon_report, $report_stream, SPApiUserMarketplace $markatplace)
    {
       $numHeaders = 0;
        Log::debug("Started Reading MWS Inventory Details report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        if (!empty($report_stream)) {
            $headers = array();
            $tmp = explode("\n", $report_stream);
            foreach ($tmp as $lineCount => $finaldata) {
                if (!empty($finaldata)) {
                    $row = explode("\t", $finaldata);
                    if ($lineCount == 0) {
                        $headers = $row;
                        $numHeaders = count($headers);
                    }
                    if ($lineCount > 0) {
                        if (count($row) > $numHeaders) {
                            $row = array_slice($row, 0, $numHeaders);
                        }
                        $row = array_combine($headers, $row);
                        $productId = Mws_product::where('sku', $row['sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                        if (empty($productId)) {
                            $productId = new Mws_product();
                            $productId->user_marketplace_id = $amazon_report->user_marketplace_id;
                            $productId->sku = $row['sku'];
                            $productId->prod_name = utf8_encode($row['product-name']);
                            $productId->save();
                        }
                //                else {
                //                    $productId->prod_name = utf8_encode($row['product-name']);
                //                    $productId->save();
                //                }
                        if (isset($row['sku'])) {
                            $mwsInventoryDetails = Mws_inventory_details::where('snapshot_date', date('Y-m-d',strtotime($row['snapshot-date'])))->where('sku', $row['sku'])->where('fulfilment_center_id', $row['fulfillment-center-id'])
                                ->where('user_marketplace_id', $amazon_report->user_marketplace_id)
                                ->first();
                            if ($mwsInventoryDetails) {
                                $storageStatus = 'updated';
                            } else {
                                $mwsInventoryCalculation = Mws_calculated_inventory_daily::where('fnsku', $row['fnsku'])->where('inventory_date', date('Y-m-d',strtotime($row['snapshot-date'])))
                                    ->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                                if ($row['detailed-disposition'] == 'SELLABLE') {
                                    if ($mwsInventoryCalculation) {
                                        $mwsInventoryCalculation->qty = $mwsInventoryCalculation->qty + $row['quantity'];
                                    } else {
                                        $mwsInventoryCalculation = new Mws_calculated_inventory_daily();
                                        $mwsInventoryCalculation->user_marketplace_id = $amazon_report->user_marketplace_id;
                                        $mwsInventoryCalculation->product_id = $productId->id;
                                        $mwsInventoryCalculation->fnsku = $row['fnsku'];
                                        $mwsInventoryCalculation->qty = $row['quantity'];
                                        $mwsInventoryCalculation->inventory_date = date('Y-m-d',strtotime($row['snapshot-date']));
                                    }
                                    $mwsInventoryCalculation->save();
                                }
                                $storageStatus = 'created';
                                $mwsInventoryDetails = new Mws_inventory_details();
                            }
                            $mwsInventoryDetails->user_marketplace_id = $amazon_report->user_marketplace_id;
                            $mwsInventoryDetails->product_id = $productId->id;
                            $mwsInventoryDetails->snapshot_date = date('Y-m-d',strtotime($row['snapshot-date']));
                            $mwsInventoryDetails->fnsku = $row['fnsku'];
                            $mwsInventoryDetails->sku = $row['sku'];
                            $mwsInventoryDetails->prod_name = utf8_encode($row['product-name']);
                            $mwsInventoryDetails->qty = $row['quantity'];
                            $mwsInventoryDetails->fulfilment_center_id = $row['fulfillment-center-id'];
                            $mwsInventoryDetails->detailed_disposition = $row['detailed-disposition'];
                            $mwsInventoryDetails->country = @$row['country'];
                            $mwsInventoryDetails->count_status = 1;
                            $mwsInventoryDetails->save();
                        }
                    }
                }
            }
        }

        $amazon_report->processed = 1;
        $amazon_report->save();
        if ($amazon_report->amz_dateitetration_id > 0) {
            $amzDateitetration = Amzdateiteration::where('id', $amazon_report->amz_dateitetration_id)->first();
            if (!empty($amzDateitetration->id)) {
                $amzDateitetration->InventoryStatus = '1';
                $amzDateitetration->save();
            }
        }
        Log::debug("Ended Reading MWS Inventory Details report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
    }
}

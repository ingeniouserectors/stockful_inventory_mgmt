<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
class Supplier_product extends Model
{
    //
    use SoftDeletes;
    protected $table = 'supplier_products';

    protected $fillable = [
        'supplier_id',
        'product_id',
        'lead_time',
        'order_volume',
        'quantity_discount',
        'moq',
        'CBM_Per_Container',
        'Production_Time',
        'Boat_To_Port',
        'Port_To_Warehouse',
        'Warehouse_Receipt',
        'Ship_To_Specific_Warehouse',
        'Cost',
        'Cost_Expenses',
        'Box_Domensions_Height',
        'Box_Domensions_Width',
        'Box_Domensions_Weight',
        'Qty_per_Carton',
        'Cartons_per_pallet',
        'Inventory_level',
    ];

    /**
     * Get the supplier that owns the supplier wise product.
     *
     * @param  no-params
     *
     */
    public function supplier(){
        return $this->belongsTo('App\Models\Supplier', 'supplier_id');
    }
    /**
     * Get the product that owns the supplier wise product.
     *
     * @param  no-params
     *
     */
    public function mws_product(){
        return $this->belongsTo('App\Models\Mws_product', 'product_id');
    }
}

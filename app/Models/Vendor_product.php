<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
class Vendor_product extends Model
{
    use SoftDeletes;
    protected $table = 'vendor_products';

    protected $fillable = [
        'vendor_id',
        'product_id',
        'lead_time',
        'order_volume',
        'quantity_discount',
        'moq',
        'shipping',
        'ship_to_warehouse',
        'CBM_Per_Container',
        'Cost',
        'Cost_Expenses',
        'Box_Domensions_Height',
        'Box_Domensions_Width',
        'Box_Domensions_Weight',
        'Qty_per_Carton',
        'Cartons_per_pallet',
        'Inventory_level',
    ];

    /**
     * Get the vendor that owns the vendor product.
     *
     * @param  no-params
     *
     */
    public function vendor(){
        return $this->belongsTo('App\Models\Vendor', 'vendor_id');
    }
    /**
     * Get the product that owns the vendor product.
     *
     * @param  no-params
     *
     */
    public function mws_product(){
        return $this->belongsTo('App\Models\Mws_product', 'product_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Member_fba_setting extends Model
{
    //
    protected $table = 'members_fba_setting';

    protected $fillable = [
        'user_marketplace_id',
        'supply_days',
        'ship_boxes',
        'ship_pallets'
    ];

}

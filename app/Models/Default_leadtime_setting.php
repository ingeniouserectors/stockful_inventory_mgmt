<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Default_leadtime_setting extends Model
{
    //
    protected $table = 'default_leadtime_setting';

    protected $fillable = [
        'product_manuf_days',
        'to_port_days',
        'transit_time_days',
        'to_warehouse_days',
        'to_amazon',
        'po_to_production_days',
        'safety_days',
        'total_lead_time_days',
        'order_prep_days',
        'po_to_prep_days',
        'order_volume_value',
        'order_volume_id',
        'type',
    ];
    /**
     * Get the user marketplace that owns the cron log..
     *
     * @param  no-params
     *
     */
}

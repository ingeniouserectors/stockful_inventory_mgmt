<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;



class Purchase_instances  extends Model
{
    use SoftDeletes;
    protected $table = 'purchase_instances';

    protected $fillable = [
        'user_marketplace_id',
        'track_id',
        'status'
       
    ];
    /**
     * Get the purchase instance details for the purchase instance.
     *
     * @param  no-params
     *
     */
    public function purchase_instances_details(){
        return $this->hasOne('App\Models\Purchase_instance_details','purchesinstances_id');
    }
    /**
     * Get the purchase order details for the purchase instance.
     *
     * @param  no-params
     *
     */
     public function purchase_order_details(){
        return $this->hasMany('App\Models\Purchase_order_details','purchesinstances_id');
    }
    /**
     * Get the product for the purchase instance.
     *
     * @param  no-params
     *
     */
     public function product(){
        return $this->hasOne('App\Models\Mws_product','id');
    }
    /**
     * Get the supplier for the purchase instance.
     *
     * @param  no-params
     *
     */
     public function suppliers(){
        return $this->hasOne('App\Models\Supplier','id');
    }
    /**
     * Get the user marketplace that owns the purchase instance..
     *
     * @param  no-params
     *
     */
    public function usermarketplace(){
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
    

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    //
    protected $table = 'settings';

    protected $fillable = [
        'registration_setting',
        'email_settings',
        'forget_password_settings',
        'mws_settings',
        'maintenance_mode',
        'affiliate_settings',
        'support_ticket_settings',
        'historical_fetch_number_months'
    ];

}

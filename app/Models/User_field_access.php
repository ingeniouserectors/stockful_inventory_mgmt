<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_field_access extends Model
{
    //
    protected $table = 'user_field_access';

    protected $fillable = [
        'field_list_id',
        'marketplace_id',
        'active',
        'type'
    ];

}

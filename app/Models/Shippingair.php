<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shippingair extends Model
{
//    use SoftDeletes;
    protected $table = 'shipping_air';

    protected $fillable = [
        'shipping_id',
        'ship_type',
        'price',
        'messuare_in'
    ];

    public function shipping(){
        return $this->belongsTo('App\Models\shipping', 'id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Default_logic_setting extends Model
{
    //
    protected $table = 'default_logic_setting';

    protected $fillable = [
        'user_marketplace_id',
        'logic_label_name',
        'supply_last_year_sales_percentage',
        'supply_recent_last_7_day_sales_percentage',
        'supply_recent_last_14_day_sales_percentage',
        'supply_recent_last_30_day_sales_percentage',
        'supply_trends_year_over_year_historical_percentage',
        'supply_trends_year_over_year_current_percentage',
        'supply_trends_multi_month_trend_percentage',
        'supply_trends_30_over_30_days_percentage',
        'supply_trends_multi_week_trend_percentage',
        'supply_trends_7_over_7_days_percentage',
        'reorder_last_year_sales_percentage',
        'reorder_recent_last_7_day_sales_percentage',
        'reorder_recent_last_14_day_sales_percentage',
        'reorder_recent_last_30_day_sales_percentage',
        'reorder_trends_year_over_year_historical_percentage',
        'reorder_trends_year_over_year_current_percentage',
        'reorder_trends_multi_month_trend_percentage',
        'reorder_trends_30_over_30_days_percentage',
        'reorder_trends_multi_week_trend_percentage',
        'reorder_trends_7_over_7_days_percentage',
        'reorder_safety_stock_percentage'
    ];
    /**
     * Get the user marketplace that owns the default logic..
     *
     * @param  no-params
     *
     */
    public function usermarketplace()
    {
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lead_time_type  extends Model
{
    //
    protected $table = 'lead_time_type';

    protected $fillable = [
        'type'
    ];

}

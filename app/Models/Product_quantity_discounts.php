<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_quantity_discounts extends Model
{
    //
    protected $table = 'product_quantity_discounts';

    protected $fillable = [
        'product_id',
        'moq_quantity_unit',
        'moq_quantity_price'
    ];
    /**
     * Get the user marketplace that owns the cron log..
     *
     * @param  no-params
     *
     */
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment_terms_type extends Model
{
    //
    protected $table = 'payment_term_type';

    protected $fillable = [
        'payment_type',
    ];

}

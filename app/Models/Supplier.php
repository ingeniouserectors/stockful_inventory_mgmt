<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    use SoftDeletes;
    protected $table = 'suppliers';

    public function country(){
        return get_country_fullname($this->country_id);
    }

    protected $fillable = [
        'user_marketplace_id',
        'supplier_name',
        'address_line_1',
        'address_line_2',
        'city',
        'state',
        'zipcode',
        'country_id'
    ];

    /**
     * Get the user marketplace that owns the supplier.
     *
     * @param  no-params
     *
     */
    public function usermarketplace(){
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
    /**
     * Get the contacts for the supplier.
     *
     * @param  no-params
     *
     */
    public function supplier_contact(){
        return $this->hasMany('App\Models\Supplier_contact');
    }
    /**
     * Get the product for the supplier.
     *
     * @param  no-params
     *
     */
    public function supplier_product(){
        return $this->hasMany('App\Models\Supplier_product','supplier_id');
    }
    /**
     * Get the settings for the supplier.
     *
     * @param  no-params
     *
     */
    public function supplier_wise_setting(){
        return $this->hasMany('App\Models\Supplier_wise_default_setting');
    }
    /**
     * Get the blackout dates for the supplier.
     *
     * @param  no-params
     *
     */
    public function supplier_blackout_date(){
        return $this->hasMany('App\Models\Supplier_blackout_date');
    }

    public function shipping(){
        return $this->hasOne('App\Models\Shipping','supplier_vendor_id');
    }

}

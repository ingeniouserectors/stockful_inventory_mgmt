<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Calculated_sales_qty extends Model
{
    protected $fillable = [
        'user_marketplace_id',
        'product_id',
        'order_date',
        'total_qty',
    ];
    //
    protected $table = 'calculated_sales_qty';
    /**
     * Get the user marketplace that owns the Calculation..
     *
     * @param  no-params
     *
     */
    public function usermarketplace()
    {
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
    /**
     * Get the product that owns the Calculation..
     *
     * @param  no-params
     *
     */
    public function mws_product()
    {
        return $this->belongsTo('App\Models\Mws_product', 'product_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Warehouse_users extends Model
{
    //
    protected $table = 'warehouse_users';

    protected $fillable = [
        'user_marketplace_id',
        'full_name',
        'designation'
    ];
    /**
     * Get the warehouse wise default setting for the warhouse default setting.
     *
     * @param  no-params
     *
     */
//    public function warehouse_wise_default_setting(){
//        return $this->hasOne('App\Models\Warehouse_wise_default_setting','warehouse_id');
//    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mws_adjusted_inventory extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'mws_adjusted_inventories';

    protected $fillable = [
        'user_marketplace_id', 'product_id', 'sku', 'quantity', 'date',
        'total_sales', 'total_return', 'seven_day_sales', 'adjusted_inventory', 'average_sales',
        'mod_sales','modified_sales_flag'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShippingAgent extends Model
{
    use SoftDeletes;
    protected $table = 'shipping_agents';

    protected $fillable = [
        'user_marketplace_id',
        'country_id',
        'company_name',
        'first_name',
        'last_name',
        'email',
        'phone'
    ];

    /**
     * Get the user marketplace that owns the shipping agent.
     *
     * @param  no-params
     *
     */
    public function usermarketplace(){
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
}

<?php

namespace App\Models;

use App\Classes\helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Mws_unsuppressed_inventory_data extends Model
{
    //
    protected $table = 'mws_unsuppressed_inventory_data';

    protected $fillable = [
        'user_marketplace_id', 'product_id', 'sku', 'fnsku', 'asin',
        'condition', 'yourprice', 'mfn_listing_exists', 'mfn_fulfilment_qty', 'afn_listing_exists', 'afn_warehouse_qty',
        'afn_fulfillable_qty', 'afn_unsellable_qty', 'afn_reserved_qty', 'afn_total_qty', 'per_unit_volume',
        'afn_inbound_working_qty', 'afn_inbound_shipment_qty', 'afn_inbound_receving_qty', 'afn_research_qty', 'afn_reserved_future_supply',
        'afn_future_supply_buyable'
    ];

    /**
     * Get the user marketplace that owns the unsuppressed inventory..
     *
     * @param no-params
     *
     */
    public function usermarketplace()
    {
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }

    /**
     * Get the product that owns the unsuppressed inventory..
     *
     * @param no-params
     *
     */
    public function mws_product()
    {
        return $this->belongsTo('App\Models\Mws_product', 'product_id');
    }

    /**
     * Logic for process reports..
     *
     * @param amazon_report
     *         report_stream
     *         marketplace
     *
     */
    public static function processReport(Amazon_requestlog &$amazon_report, $report_stream, Usermarketplace $markatplace)
    {
        $first = false;
        $numHeaders = 0;
        Log::debug("Started Reading MWS Unsuppressed Inventory report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        while (!feof($report_stream)) {

            $row = fgetcsv($report_stream, 10000, "\t");
            if (!$first) {
                $first = true;
                $headers = $row;
                $numHeaders = count($headers);
                continue;
            }
            if (!empty($row)) {
                // truncate row data if item count exceeds number of headers
                if (count($row) > $numHeaders) {
                    $row = array_slice($row, 0, $numHeaders);
                }
                $row = array_combine($headers, $row);
                if (isset($row['sku'])) {
                    $productId = Mws_product::where('sku', $row['sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                    if (empty($productId)) {
                        $fetchImage = new helper();
                        $productData = $fetchImage->getProductImage($row['asin'], $markatplace);
                        $imageURL = str_replace('SL75', 'UL1448', $productData['imageURL']);
                        $productId = new Mws_product();
                        $productId->user_marketplace_id = $amazon_report->user_marketplace_id;
                        $productId->sku = $row['sku'];
                        $productId->fnsku = $row['fnsku'];
                        $productId->asin = $row['asin'];
                        $productId->prod_name = utf8_encode($row['product-name']);
                        $productId->prod_image = ($imageURL ? $imageURL[0] : '');
                        $productId->save();
                    }
                    if (isset($row['asin'])) {
                        //Insert or Update Historical Unsuppressed Data
                        $compareDate = date('Y-m-d 00:00:00', strtotime($amazon_report->created_at->format('Y-m-d')));
                        $checkUnsuppressedHistorical = Mws_unsuppressed_historical_inventory::where('sku', $row['sku'])->where('inventory_for_date', $compareDate)->first();
                        if (empty($checkUnsuppressedHistorical)) {
                            $checkUnsuppressedHistorical = new Mws_unsuppressed_historical_inventory();
                        }
                        $checkUnsuppressedHistorical->user_marketplace_id = $amazon_report['user_marketplace_id'];
                        $checkUnsuppressedHistorical->product_id = $productId->id;
                        $checkUnsuppressedHistorical->sku = $row['sku'];
                        $checkUnsuppressedHistorical->fnsku = $row['fnsku'];
                        $checkUnsuppressedHistorical->asin = $row['asin'];
                        $checkUnsuppressedHistorical->condition = $row['condition'];
                        $checkUnsuppressedHistorical->yourprice = ($row['your-price'] ? $row['your-price'] : 0);
                        $checkUnsuppressedHistorical->mfn_listing_exists = $row['mfn-listing-exists'];
                        $checkUnsuppressedHistorical->mfn_fulfilment_qty = ($row['mfn-fulfillable-quantity'] ? $row['mfn-fulfillable-quantity'] : 0);
                        $checkUnsuppressedHistorical->afn_listing_exists = $row['afn-listing-exists'];
                        $checkUnsuppressedHistorical->afn_warehouse_qty = ($row['afn-warehouse-quantity'] ? $row['afn-warehouse-quantity'] : 0);
                        $checkUnsuppressedHistorical->afn_fulfillable_qty = ($row['afn-fulfillable-quantity'] ? $row['afn-fulfillable-quantity'] : 0);
                        $checkUnsuppressedHistorical->afn_unsellable_qty = ($row['afn-unsellable-quantity'] ? $row['afn-unsellable-quantity'] : 0);
                        $checkUnsuppressedHistorical->afn_reserved_qty = ($row['afn-reserved-quantity'] ? $row['afn-reserved-quantity'] : 0);
                        $checkUnsuppressedHistorical->afn_total_qty = ($row['afn-total-quantity'] ? $row['afn-total-quantity'] : 0);
                        $checkUnsuppressedHistorical->per_unit_volume = ($row['per-unit-volume'] ? $row['per-unit-volume'] : 0);
                        $checkUnsuppressedHistorical->afn_inbound_working_qty = ($row['afn-inbound-working-quantity'] ? $row['afn-inbound-working-quantity'] : 0);
                        $checkUnsuppressedHistorical->afn_inbound_shipment_qty = ($row['afn-inbound-shipped-quantity'] ? $row['afn-inbound-shipped-quantity'] : 0);
                        $checkUnsuppressedHistorical->afn_inbound_receving_qty = ($row['afn-inbound-receiving-quantity'] ? $row['afn-inbound-receiving-quantity'] : 0);
                        $checkUnsuppressedHistorical->afn_research_qty = ($row['afn-researching-quantity'] ? $row['afn-researching-quantity'] : 0);
                        $checkUnsuppressedHistorical->afn_reserved_future_supply = ($row['afn-reserved-future-supply'] ? $row['afn-reserved-future-supply'] : 0);
                        $checkUnsuppressedHistorical->afn_future_supply_buyable = ($row['afn-future-supply-buyable'] ? $row['afn-future-supply-buyable'] : 0);
                        $checkUnsuppressedHistorical->inventory_for_date = $compareDate;
                        $checkUnsuppressedHistorical->save();

                        $mwsUnsuppressedInventoryData = Mws_unsuppressed_inventory_data::where('sku', $row['sku'])->where('condition', $row['condition'])
                            ->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                        if (isset($mwsUnsuppressedInventoryData->id)) {
                            $storageStatus = 'updated';
                        } else {
                            $storageStatus = 'created';
                            $mwsUnsuppressedInventoryData = new Mws_unsuppressed_inventory_data();
                        }
                        $mwsUnsuppressedInventoryData->user_marketplace_id = $amazon_report->user_marketplace_id;
                        $mwsUnsuppressedInventoryData->product_id = $productId->id;
                        $mwsUnsuppressedInventoryData->sku = $row['sku'];
                        $mwsUnsuppressedInventoryData->fnsku = $row['fnsku'];
                        $mwsUnsuppressedInventoryData->asin = $row['asin'];
                        $mwsUnsuppressedInventoryData->condition = $row['condition'];
                        $mwsUnsuppressedInventoryData->yourprice = ($row['your-price'] ? $row['your-price'] : 0);
                        $mwsUnsuppressedInventoryData->mfn_listing_exists = $row['mfn-listing-exists'];
                        $mwsUnsuppressedInventoryData->mfn_fulfilment_qty = ($row['mfn-fulfillable-quantity'] ? $row['mfn-fulfillable-quantity'] : 0);
                        $mwsUnsuppressedInventoryData->afn_listing_exists = $row['afn-listing-exists'];
                        $mwsUnsuppressedInventoryData->afn_warehouse_qty = ($row['afn-warehouse-quantity'] ? $row['afn-warehouse-quantity'] : 0);
                        $mwsUnsuppressedInventoryData->afn_fulfillable_qty = ($row['afn-fulfillable-quantity'] ? $row['afn-fulfillable-quantity'] : 0);
                        $mwsUnsuppressedInventoryData->afn_unsellable_qty = ($row['afn-unsellable-quantity'] ? $row['afn-unsellable-quantity'] : 0);
                        $mwsUnsuppressedInventoryData->afn_reserved_qty = ($row['afn-reserved-quantity'] ? $row['afn-reserved-quantity'] : 0);
                        $mwsUnsuppressedInventoryData->afn_total_qty = ($row['afn-total-quantity'] ? $row['afn-total-quantity'] : 0);
                        $mwsUnsuppressedInventoryData->per_unit_volume = ($row['per-unit-volume'] ? $row['per-unit-volume'] : 0);
                        $mwsUnsuppressedInventoryData->afn_inbound_working_qty = ($row['afn-inbound-working-quantity'] ? $row['afn-inbound-working-quantity'] : 0);
                        $mwsUnsuppressedInventoryData->afn_inbound_shipment_qty = ($row['afn-inbound-shipped-quantity'] ? $row['afn-inbound-shipped-quantity'] : 0);
                        $mwsUnsuppressedInventoryData->afn_inbound_receving_qty = ($row['afn-inbound-receiving-quantity'] ? $row['afn-inbound-receiving-quantity'] : 0);
                        $mwsUnsuppressedInventoryData->afn_research_qty = ($row['afn-researching-quantity'] ? $row['afn-researching-quantity'] : 0);
                        $mwsUnsuppressedInventoryData->afn_reserved_future_supply = ($row['afn-reserved-future-supply'] ? $row['afn-reserved-future-supply'] : 0);
                        $mwsUnsuppressedInventoryData->afn_future_supply_buyable = ($row['afn-future-supply-buyable'] ? $row['afn-future-supply-buyable'] : 0);
                        $mwsUnsuppressedInventoryData->save();
                    }
                }
            }
            Log::debug("Ended Reading MWS Unsuppressed Inventory report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        }
        $amazon_report->processed = 1;
        $amazon_report->save();
    }
    public static function processSPAPIReport(Amazon_requestlog &$amazon_report, $report_stream, SPApiUserMarketplace $markatplace)
    {
        $requestConfiguration = Mws_product::getConfiguration($markatplace);
        $finance = new \ClouSale\AmazonSellingPartnerAPI\Api\CatalogApi($requestConfiguration);
        $numHeaders = 0;
        Log::debug("Started Reading MWS Unsuppressed Inventory Report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");
        if (!empty($report_stream)) {

            $headers = array();
            $tmp = explode("\n", $report_stream);
            foreach ($tmp as $lineCount => $finaldata) {
                if (!empty($finaldata)) {

                    $row = explode("\t", $finaldata);
                    print_r($row);
                    if ($lineCount == 0) {

                        $headers = $row;
                        $numHeaders = count($headers);
                    }
                    if ($lineCount > 0) {
                        if (count($row) > $numHeaders) {
                            $row = array_slice($row, 0, $numHeaders);
                        }
                        $row = array_combine($headers, $row);
                        $productId = Mws_product::where('sku', $row['sku'])->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();

                        if (empty($productId)) {
                            $shipmentData = $finance->getCatalogItem($markatplace->market_place_developer->marketplace_id, $row['asin']);
                            $payload = $shipmentData->getPayload();
                            $get_attribute = $payload->getAttributeSets();
                            $product_title = @$get_attribute[0]["title"];
                            $product_image = @$get_attribute[0]["small_image"]['url'];
                            $product_brand = @$get_attribute[0]["brand"];
                            $product_prod_group = @$get_attribute[0]["product_group"];
                            //dd($get_attribute);

                            $productId = new Mws_product();
                            $productId->user_marketplace_id = $amazon_report->user_marketplace_id;
                            $productId->sku = $row['sku'];
                            $productId->fnsku = $row['fnsku'];
                            $productId->asin = $row['asin'];
                            $productId->prod_image = $product_image;
                            $productId->prod_name = $product_title;
                            $productId->brand = $product_brand;
                            $productId->prod_group = $product_prod_group;
                            $productId->save();
                        }

                        //echo 'test';
                        if (isset($row['asin'])) {
                            //Insert or Update Historical Unsuppressed Data
                            $compareDate = date('Y-m-d 00:00:00', strtotime($amazon_report->created_at->format('Y-m-d')));
                            $checkUnsuppressedHistorical = Mws_unsuppressed_historical_inventory::where('sku', $row['sku'])->where('inventory_for_date', $compareDate)->first();
                            if (empty($checkUnsuppressedHistorical)) {
                                $checkUnsuppressedHistorical = new Mws_unsuppressed_historical_inventory();
                            }
                            $checkUnsuppressedHistorical->user_marketplace_id = $amazon_report['user_marketplace_id'];
                            $checkUnsuppressedHistorical->product_id = @$productId->id;
                            $checkUnsuppressedHistorical->sku = $row['sku'];
                            $checkUnsuppressedHistorical->fnsku = $row['fnsku'];
                            $checkUnsuppressedHistorical->asin = $row['asin'];
                            $checkUnsuppressedHistorical->condition = $row['condition'];
                            $checkUnsuppressedHistorical->yourprice = ($row['your-price'] ? $row['your-price'] : 0);
                            $checkUnsuppressedHistorical->mfn_listing_exists = $row['mfn-listing-exists'];
                            $checkUnsuppressedHistorical->mfn_fulfilment_qty = ($row['mfn-fulfillable-quantity'] ? $row['mfn-fulfillable-quantity'] : 0);
                            $checkUnsuppressedHistorical->afn_listing_exists = $row['afn-listing-exists'];
                            $checkUnsuppressedHistorical->afn_warehouse_qty = ($row['afn-warehouse-quantity'] ? $row['afn-warehouse-quantity'] : 0);
                            $checkUnsuppressedHistorical->afn_fulfillable_qty = ($row['afn-fulfillable-quantity'] ? $row['afn-fulfillable-quantity'] : 0);
                            $checkUnsuppressedHistorical->afn_unsellable_qty = ($row['afn-unsellable-quantity'] ? $row['afn-unsellable-quantity'] : 0);
                            $checkUnsuppressedHistorical->afn_reserved_qty = ($row['afn-reserved-quantity'] ? $row['afn-reserved-quantity'] : 0);
                            $checkUnsuppressedHistorical->afn_total_qty = ($row['afn-total-quantity'] ? $row['afn-total-quantity'] : 0);
                            $checkUnsuppressedHistorical->per_unit_volume = ($row['per-unit-volume'] ? $row['per-unit-volume'] : 0);
                            $checkUnsuppressedHistorical->afn_inbound_working_qty = ($row['afn-inbound-working-quantity'] ? $row['afn-inbound-working-quantity'] : 0);
                            $checkUnsuppressedHistorical->afn_inbound_shipment_qty = ($row['afn-inbound-shipped-quantity'] ? $row['afn-inbound-shipped-quantity'] : 0);
                            $checkUnsuppressedHistorical->afn_inbound_receving_qty = ($row['afn-inbound-receiving-quantity'] ? $row['afn-inbound-receiving-quantity'] : 0);
                            $checkUnsuppressedHistorical->afn_research_qty = (@$row['afn-researching-quantity'] ? @$row['afn-researching-quantity'] : 0);
                            $checkUnsuppressedHistorical->afn_reserved_future_supply = (@$row['afn-reserved-future-supply'] ? @$row['afn-reserved-future-supply'] : 0);
                            $checkUnsuppressedHistorical->afn_future_supply_buyable = (@$row['afn-future-supply-buyable'] ? @$row['afn-future-supply-buyable'] : 0);
                            $checkUnsuppressedHistorical->inventory_for_date = $compareDate;
                            $checkUnsuppressedHistorical->save();

                            $mwsUnsuppressedInventoryData = Mws_unsuppressed_inventory_data::where('sku', $row['sku'])->where('condition', $row['condition'])
                                ->where('user_marketplace_id', $amazon_report->user_marketplace_id)->first();
                            if (isset($mwsUnsuppressedInventoryData->id)) {
                                echo 1;
                                $storageStatus = 'updated';
                            } else {
                                echo 2;
                                $storageStatus = 'created';
                                $mwsUnsuppressedInventoryData = new Mws_unsuppressed_inventory_data();
                            }
                            $mwsUnsuppressedInventoryData->user_marketplace_id = $amazon_report->user_marketplace_id;
                            $mwsUnsuppressedInventoryData->product_id = $productId->id;
                            $mwsUnsuppressedInventoryData->sku = $row['sku'];
                            $mwsUnsuppressedInventoryData->fnsku = $row['fnsku'];
                            $mwsUnsuppressedInventoryData->asin = $row['asin'];
                            $mwsUnsuppressedInventoryData->condition = $row['condition'];
                            $mwsUnsuppressedInventoryData->yourprice = ($row['your-price'] ? $row['your-price'] : 0);
                            $mwsUnsuppressedInventoryData->mfn_listing_exists = $row['mfn-listing-exists'];
                            $mwsUnsuppressedInventoryData->mfn_fulfilment_qty = ($row['mfn-fulfillable-quantity'] ? $row['mfn-fulfillable-quantity'] : 0);
                            $mwsUnsuppressedInventoryData->afn_listing_exists = $row['afn-listing-exists'];
                            $mwsUnsuppressedInventoryData->afn_warehouse_qty = ($row['afn-warehouse-quantity'] ? $row['afn-warehouse-quantity'] : 0);
                            $mwsUnsuppressedInventoryData->afn_fulfillable_qty = ($row['afn-fulfillable-quantity'] ? $row['afn-fulfillable-quantity'] : 0);
                            $mwsUnsuppressedInventoryData->afn_unsellable_qty = ($row['afn-unsellable-quantity'] ? $row['afn-unsellable-quantity'] : 0);
                            $mwsUnsuppressedInventoryData->afn_reserved_qty = ($row['afn-reserved-quantity'] ? $row['afn-reserved-quantity'] : 0);
                            $mwsUnsuppressedInventoryData->afn_total_qty = ($row['afn-total-quantity'] ? $row['afn-total-quantity'] : 0);
                            $mwsUnsuppressedInventoryData->per_unit_volume = ($row['per-unit-volume'] ? $row['per-unit-volume'] : 0);
                            $mwsUnsuppressedInventoryData->afn_inbound_working_qty = ($row['afn-inbound-working-quantity'] ? $row['afn-inbound-working-quantity'] : 0);
                            $mwsUnsuppressedInventoryData->afn_inbound_shipment_qty = ($row['afn-inbound-shipped-quantity'] ? $row['afn-inbound-shipped-quantity'] : 0);
                            $mwsUnsuppressedInventoryData->afn_inbound_receving_qty = ($row['afn-inbound-receiving-quantity'] ? $row['afn-inbound-receiving-quantity'] : 0);
                            $mwsUnsuppressedInventoryData->afn_research_qty = (@$row['afn-researching-quantity'] ? @$row['afn-researching-quantity'] : 0);
                            $mwsUnsuppressedInventoryData->afn_reserved_future_supply = (@$row['afn-reserved-future-supply'] ? @$row['afn-reserved-future-supply'] : 0);
                            $mwsUnsuppressedInventoryData->afn_future_supply_buyable = (@$row['afn-future-supply-buyable'] ? @$row['afn-future-supply-buyable'] : 0);
                            $mwsUnsuppressedInventoryData->save();
                        }

                    }
                }
            }
        }
        Log::debug("Ended Reading MWS Unsuppressed Inventory report of report id : {$amazon_report->id} And MarketplaceID : {$amazon_report->user_marketplace_id}");

        $amazon_report->processed = 1;
        $amazon_report->save();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mws_import_order extends Model
{
    //
    protected $table="mws_import_order";
    protected $fillable = [
        'user_marketplace_id',
        'product_id',
        'product_name',
        'sku',
        'asin',
        'amazon_order_id',
        'purchase_date',
        'quantity',
        'currency',
        'item_price',
        'item_tax',
        'shipping_price',
        'shipping_tax',
        'ship_city',
        'ship_state',
        'ship_country',
        'ship_postal_code',
        'order_status'
    ];

}

<?php

namespace App\Models;

use App\Classes\helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Mws_import_products extends Model
{
    //
    protected $table = 'mws_import_products';
    protected $fillable = [
        'product_id',
        'sku',
        'fnsku',
        'asin',
        'upc',
        'prod_name',
        'nick_name',
        'categoty',
        'prod_image',
        'price',
        'prod_group',
        'brand',
        'fulfilledby',
        'haslocalinventory',
        'currency',
        'your_price',
        'sales_price',
        'longestside',
        'medianside',
        'shortestside',
        'lengthandgridth',
        'unitofdimensions',
        'itempackweight',
        'unitofweight',
        'productsizeweightband'
    ];
    /**
     * Get the user marketplace that owns the product..
     *
     * @param  no-params
     *
     */
    public function products()
    {
        return $this->belongsTo('App\Models\Mws_products', 'id');
    }
}

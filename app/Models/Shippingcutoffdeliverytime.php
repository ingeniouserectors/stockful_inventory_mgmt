<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shippingcutoffdeliverytime extends Model
{
    use SoftDeletes;
    protected $table = 'shipping_cutoff_delivery_time';

    protected $fillable = [
        'shipping_id',
        'shipping_delivery_id',
        'cut_off_day',
        'cut_off_time',
    ];

    /**
     * Get the user marketplace that owns the shipping agent.
     *
     * @param  no-params
     *
     */
    public function shipping(){
        return $this->belongsTo('App\Models\Shipping', 'id');
    }
}

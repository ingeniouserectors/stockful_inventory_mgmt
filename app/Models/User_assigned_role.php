<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_assigned_role extends Model
{
    //
    protected $table = 'user_assigned_role';

    protected $fillable = [
        'user_id',
        'user_role_id'
    ];
    /**
     * Get the user  that owns the assign role.
     *
     * @param  no-params
     *
     */
    public function users(){
        return $this->belongsTo('App\Models\Users','user_id');
    }
    /**
     * Get the user role  that owns the assign role.
     *
     * @param  no-params
     *
     */
    public function userrole(){
        return $this->belongsTo('App\Models\Userrole', 'user_role_id');
    }

}

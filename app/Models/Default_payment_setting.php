<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Default_payment_setting extends Model
{
    //
    protected $table = 'default_payment_setting';

    protected $fillable = [
        'payment_terms',
        'percentage',
        'dues',
        'days_before_after',
        'payment_term_type_id',
    ];
    /**
     * Get the user marketplace that owns the cron log..
     *
     * @param  no-params
     *
     */
}

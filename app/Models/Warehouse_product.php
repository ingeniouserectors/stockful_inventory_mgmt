<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Warehouse_product extends Model
{
    //
    protected $table = 'warehouse_products';

    protected $fillable = [
        'warehouse_id',
        'product_id',
        'qty',
        'lead_time'
    ];

    /**
     * Get the warehouse that owns the warhouse product.
     *
     * @param  no-params
     *
     */
    public function warehouse(){
        return $this->belongsTo('App\Models\Warehouse');
    }
    /**
     * Get the product that owns the warhouse product.
     *
     * @param  no-params
     *
     */
    public function mws_product(){
        return $this->belongsTo('App\Models\Mws_product', 'product_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SPApiUserMarketplace extends Model
{
    protected $table = 'sp_api_user_marketplaces';

    protected $fillable = [
        'user_id',
        'marketplace_id',
        'selling_partner_id',
        'client_id',
        'client_secret',
        'access_key',
        'secret_key',
        'role_arn',
        'end_point',
        'region',
        'refresh_token',
        'access_token',
        'expire_time',
        'rdt_access_token',
        'rdt_token_expire_time'
    ];

    /**
     * Get the user  that owns the user marketplace.
     *
     * @param  no-params
     *
     */
    public function user(){
        return $this->belongsTo('App\Models\Users');
    }

    /**
     * Get the marketplace country for the user marketplace.
     *
     * @param  no-params
     *
     */
    public function market_place_developer(){
        return $this->hasOne('App\Models\SPApiMarketplaceDetails','id');
    }

}

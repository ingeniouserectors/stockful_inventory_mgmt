<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inbound_shipments extends Model
{
    //
    protected $table = 'inbound_shipments';

    protected $fillable = [
        'user_marketplace_id',
        'ShipmentId',
        'ShipmentName',
        'ShipFromAddress',
        'DestinationFulfillmentCenterId',
        'LabelPrepType',
        'ShipmentStatus'
    ];
    /**
     * Get the inbound shipment items for the inbound shipment.
     *
     * @param  no-params
     *
     */
    public function inbound_shipment_items(){
        return $this->hasMany('App\Models\Inbound_shipment_items','inbound_shipment_id');
    }

}

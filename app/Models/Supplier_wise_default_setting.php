<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Supplier_wise_default_setting extends Model
{
    //

    protected $table = 'supplier_wise_default_settings';

    protected $fillable = [
        'supplier_id',
        'lead_time',
        'order_volume',
        'quantity_discount',
        'moq',
        'CBM_Per_Container',
        'Production_Time',
        'Boat_To_Port',
        'Port_To_Warehouse',
        'Warehouse_Receipt',
        'Ship_To_Specific_Warehouse'
    ];
    /**
     * Get the supplier that owns the supplier wise default setting.
     *
     * @param  no-params
     *
     */
    public function supplier(){
        return $this->belongsTo('App\Models\Supplier');
    }
}

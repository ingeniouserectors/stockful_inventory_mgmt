<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_assign_supply_logic extends Model
{
    protected $fillable = [
       
        'product_id',
        'label_id',
        'status',
    ];
    //
    protected $table = 'product_assign_supply_logic';
    /**
     * Get the user marketplace that owns the supply logic..
     *
     * @param  no-params
     *
     */
    public function usermarketplace()
    {
        return $this->belongsTo('App\Models\Usermarketplace', 'user_marketplace_id');
    }
    /**
     * Get the product that owns the supply logic..
     *
     * @param  no-params
     *
     */
    public function mws_product()
    {
        return $this->belongsTo('App\Models\Mws_product', 'product_id');
    }
    /**
     * Get the supply reorder logic that owns the supply logic..
     *
     * @param  no-params
     *
     */
    public function supply_reoder_logic(){
        return $this->belongsTo('App\Models\Supply_reorder_logic', 'label_id');
    }
}
 
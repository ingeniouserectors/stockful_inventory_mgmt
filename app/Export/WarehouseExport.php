<?php
namespace App\Export;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\BeforeWriting;

class WarehouseExport implements WithMultipleSheets, WithEvents 
{
    use Exportable, RegistersEventListeners;

    public function registerEvents(): array
    {
        return [
          // Handle by a closure.
          BeforeExport::class => function(BeforeExport $event) {
              $event->writer->getProperties()->setCreator('You')->setTitle("Title");
          },
          BeforeWriting::class => function(BeforeWriting $event) {
              $event->writer->setActiveSheetIndex(1);
          },
        ];
    }

    public function sheets(): array
    {
        return [
            'Structure' => new WarehouseFirstSheetExport(),
            'Instruction' => new WarehouseSecondSheetExport(),
        ];
    }
}
?>
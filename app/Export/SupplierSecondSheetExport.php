<?php
namespace App\Export;

use App\Page;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class SupplierSecondSheetExport implements FromCollection,WithHeadings,WithTitle
{
    public function title(): string
    {
        return "Instruction";
    }
    public function headings(): array {
        return [
           "Compulsory Fields list :"
        ];
    }
    public function collection() {
        return collect([
            [
                'Compulsory Fields list :',
            ],
            [
                '',
            ],
            [
                'Basic Details requirement:',
            ],
            [
                'Supplier Name',
            ],
            [
                'Country',
            ],
            [
                'Address',
            ],
            [
                'State',
            ],
            [
                'Postal Code',
            ],
            [
                '',
            ],
            [
                'Leadtime requirement:',
            ],
            [
                "'Notify_Confirm_Production_Started = 1'  then  'Contact_Confirm_Production_Started'  is required",
            ],
            [
                "'Notify_Confirm_Production_End = 1' then 'Contact_Confirm_Production_End' is requierd",
            ],
            [
                "'Notify_To_Port = 1' then 'Contact_To_Port' is requierd",
            ],
            [
                "'Notify_In_Transit = 1' then 'Contact_In_Transit' is requierd",
            ],
            [
                "'Notify_Port_Arrival = 1' then 'Contact_Port_Arrival' is requierd",
            ],
            [
                "'Notify_To_Warehouse = 1' then 'Contact_To_Warehouse' is requierd",
            ],
            [
                "'Notify_Warehouse_Receipt = 1' then 'Contact_Warehouse_Receipt' is requierd",
            ],
            [
                '',
            ],
            [
                'Order setting requirement: ',
            ],
            [
                "'Reorder_Schedule' is 'monthly' then 'Reorder_Schedule_Calendar' is require with only one month date",
            ],
            [
                "'Reorder_Schedule' is 'bi_monthly' then 'Reorder_Schedule_Calendar' is require with two month date ",
            ],
            [
                "'Reorder_Schedule' is 'weekly' then 'Reorder_Schedule_Calendar' is require with only one week day",
            ],
            [
                "'Reorder_Schedule' is 'bi_weekly' then 'Reorder_Schedule_Calendar' is require with two week day",
            ],
        ]);
    }
}
?>
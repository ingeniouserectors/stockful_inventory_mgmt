<?php
namespace App\Export;

use App\Page;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class WarehouseSecondSheetExport implements FromCollection,WithHeadings,WithTitle
{
    public function title(): string
    {
        return "Instruction";
    }
    public function headings(): array {
        return [
           "Compulsory Fields list :"
        ];
    }
    public function collection() {
        return collect([
            [
                'Type',
            ],
            [
                'Warehouse Name',
            ],
            [
                'Country',
            ],
            [
                'Address',
            ],
            [
                'State',
            ],
            [
                'Postal Code',
            ],
            [
                'Leadtime',
            ],
            [
                '',
            ],
            [
                'If select type "3PL" then contact details required compulsory',
            ],
            [
                'If select type  "self"  then usermanager details required compulsory',
            ],
        ]);
    }
}
?>
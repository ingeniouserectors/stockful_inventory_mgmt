<?php
namespace App\Export;

use App\Page;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class VendorSecondSheetExport implements FromCollection,WithHeadings,WithTitle
{
    public function title(): string
    {
        return "Instruction";
    }
    public function headings(): array {
        return [
           "Compulsory Fields list :"
        ];
    }
    public function collection() {
        return collect([
            [
                'Compulsory Fields list :',
            ],
            [
                '',
            ],
            [
                'Basic Details requirement:',
            ],
            [
                'Vendor Name',
            ],
            [
                'Country',
            ],
            [
                'Address',
            ],
            [
                'State',
            ],
            [
                'Postal Code',
            ],
            [
                '',
            ],
            [
                'Leadtime requirement:',
            ],
            [
                "'Notify_Confirm_Production_Started = 1'  then  'Contact_Confirm_Production_Started'  is required",
            ],
            [
                "'Notify_Confirm_Production_End = 1' then 'Contact_Confirm_Production_End' is requierd",
            ],
            [
                "'Notify_To_Port = 1' then 'Contact_To_Port' is requierd",
            ],
            [
                "'Notify_In_Transit = 1' then 'Contact_In_Transit' is requierd",
            ],
            [
                "'Notify_Port_Arrival = 1' then 'Contact_Port_Arrival' is requierd",
            ],
            [
                "'Notify_To_Warehouse = 1' then 'Contact_To_Warehouse' is requierd",
            ],
            [
                "'Notify_Warehouse_Receipt = 1' then 'Contact_Warehouse_Receipt' is requierd",
            ],
            [
                "''Notify_Domestic_Order_Prep = 1' then 'Contact_Domestic_Order_Prep' is requierd",
            ],
            [
                "''Notify_Domestic_Ship_Confirmation = 1' then 'Contact_Domestic_Ship_Confirmation' is requierd",
            ],
            [
                "''Notify_Domestic_To_Warehouse = 1' then 'Contact_Domestic_To_Warehouse' is requierd",
            ],
            [
                '',
            ],
            [
                'Order setting requirement: ',
            ],
            [
                "'Reorder_Schedule' is 'monthly' then 'Reorder_Schedule_Calendar' is require with only one month date",
            ],
            [
                "'Reorder_Schedule' is 'bi_monthly' then 'Reorder_Schedule_Calendar' is require with two month date ",
            ],
            [
                "'Reorder_Schedule' is 'weekly' then 'Reorder_Schedule_Calendar' is require with only one week day",
            ],
            [
                "'Reorder_Schedule' is 'bi_weekly' then 'Reorder_Schedule_Calendar' is require with two week day",
            ],
            [
                '',
            ],
            [
                'Shipping requirement: ',
            ],
            [
                'Vendor type : Domestic ',
            ],
            [
                'if you are select  : vendor ',
            ],
            [
                'Delivery_Schedule ',
            ],
            [
                'Days (with comma seperated) ',
            ],
            [
                'cut_off_delivery_time_sun_time ',
            ],
            [
                'cut_off_delivery_time_mon_time ',
            ],
            [
                'cut_off_delivery_time_tue_time ',
            ],
            [
                'cut_off_delivery_time_wed_time ',
            ],
            [
                'cut_off_delivery_time_thu_time ',
            ],
            [
                'cut_off_delivery_time_wed_time ',
            ],
            [
                'cut_off_delivery_time_fri_time ',
            ],
            [
                'cut_off_delivery_time_sat_time ',
            ],
            [
                'Ship_To_Warehouse ',
            ],
            [
                '',
            ],
            [
                'if you are select  : shipping-carrier',
            ],
            [
                'shipping_carrier ',
            ],
            [
                'provided_by ',
            ],
            [
                'Ship_To_Warehouse ',
            ],
            [
                '',
            ],
            [
                'Vendor type :  International ',
            ],
            [
                'if you are select  : vendor ',
            ],
            [
                'LCL_Cost ',
            ],
            [
                'LCL_Metric ',
            ],
            [
                'Container_20ft ',
            ],
            [
                'Container_20ft_CBM ',
            ],
            [
                'Container_20ft_Cost ',
            ],
            [
                'Container_40ft ',
            ],
            [
                'Container_40ft_CBM ',
            ],
            [
                'Container_40ft_Cost ',
            ],
            [
                'Container_40HQ ',
            ],
            [
                'Container_40HQ_CBM ',
            ],
            [
                'Container_40HQ_Cost ',
            ],
            [
                'Container_45HQ ',
            ],
            [
                'Container_45HQ_CBM ',
            ],
            [
                'Container_45HQ_Cost ',
            ],
            [
                'By_Air_Standard ',
            ],
            [
                'By_Air_Standard_Cost ',
            ],
            [
                'By_Air_Standard_Metric ',
            ],
            [
                'By_Air_Express ',
            ],
            [
                'By_Air_Express_Cost ',
            ],
            [
                'By_Air_Express_Metric ',
            ],
            [
                'Duties_Cost ',
            ],
            [
                'Ship_To_Warehouse ',
            ],
            [
                '',
            ],
            [
                'if you are select  : shipping-agent ',
            ],
            [
                'Shipping_Agent',
            ],
            [
                'LCL_Cost ',
            ],
            [
                'LCL_Metric ',
            ],
            [
                'Container_20ft ',
            ],
            [
                'Container_20ft_CBM ',
            ],
            [
                'Container_20ft_Cost ',
            ],
            [
                'Container_40ft ',
            ],
            [
                'Container_40ft_CBM ',
            ],
            [
                'Container_40ft_Cost ',
            ],
            [
                'Container_40HQ ',
            ],
            [
                'Container_40HQ_CBM ',
            ],
            [
                'Container_40HQ_Cost ',
            ],
            [
                'Container_45HQ ',
            ],
            [
                'Container_45HQ_CBM ',
            ],
            [
                'Container_45HQ_Cost ',
            ],
            [
                'By_Air_Standard ',
            ],
            [
                'By_Air_Standard_Cost ',
            ],
            [
                'By_Air_Standard_Metric ',
            ],
            [
                'By_Air_Express ',
            ],
            [
                'By_Air_Express_Cost ',
            ],
            [
                'By_Air_Express_Metric ',
            ],
            [
                'Duties_Cost ',
            ],
            [
                'Ship_To_Warehouse ',
            ],
        ]);
    }
}
?>
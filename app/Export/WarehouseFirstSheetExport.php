<?php
namespace App\Export;

use App\Page;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;

class WarehouseFirstSheetExport implements FromCollection,WithHeadings,WithTitle,WithEvents
{
    public function title(): string
    {
        return "Structure";
    }
    public function headings(): array {
        return [
           "Type","Warehouse Name","Country","Address","State","Postal Code","Contact_Fname","Contact_Lname","Contact_Title","Contact_Email","Contact_Phone","Primary","User manage","Leadtime",
        ];
    }
    public function collection() {
        return collect([
            [
                'Type' => '3PL',
                'Warehouse Name' => 'Test Warehouse',
                'Country' => 'India',
                'Address' => 'Ahmedabad',
                'State' => 'Gujarat',
                'Postal Code' => '380005',
                'Contact_Fname' => 'u1',
                'Contact_Lname' => 'l1',
                'Contact_Title' => 'u1',
                'Contact_Email' => 'u1@gmail.com',
                'Contact_Phone' => '1234567890',
                'Primary' => '1',
                'User manage' => '',
                'Leadtime' => '10',
            ],
            [
                'Type' => '',
                'Warehouse Name' => '',
                'Country' => '',
                'Address' => '',
                'State' => '',
                'Postal Code' => '',
                'Contact_Fname' => 'u2',
                'Contact_Lname' => 'l2',
                'Contact_Title' => 'u2',
                'Contact_Email' => 'u2@gmail.com',
                'Contact_Phone' => '1234567890',
                'Primary' => '0',
                'User manage' => '',
                'Leadtime' => '',
            ],
            [
                'Type' => 'self',
                'Warehouse Name' => 'Test self warehouse',
                'Country' => 'India',
                'Address' => 'Ahmedabad',
                'State' => 'Gujarat',
                'Postal Code' => '380005',
                'Contact_Fname' => '',
                'Contact_Lname' => '',
                'Contact_Title' => '',
                'Contact_Email' => '',
                'Contact_Phone' => '',
                'Primary' => '',
                'User manage' => 'Test,Test2',
                'Leadtime' => '10',
            ]
        ]);
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $styleArray1 = [
                    /*'font' => [
                        'bold' => true,
                    ],*/
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'startColor' => [
                            'argb' => '6600CC',
                        ],
                        'endColor' => [
                            'argb' => '6600CC',
                        ]
                    ]
                ];
                $event->getSheet()->getDelegate()->getStyle('A1:D1')->applyFromArray($styleArray1);
            },
        ];
    }
}
?>
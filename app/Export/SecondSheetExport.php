<?php
namespace App\Export;

use App\Page;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class SecondSheetExport implements FromCollection,WithHeadings,WithTitle
{
    public function title(): string
    {
        return "Instruction";
    }
    public function headings(): array {
        return [
           "Compulsory Fields list :"
        ];
    }
    public function collection() {
        return collect([
            [
                'CompanyName',
            ],
            [
                'FirstName',
            ],
            [
                'LastName',
            ],
            [
                'Email',
            ],
            [
                'Phone',
            ],
        ]);
    }
}
?>
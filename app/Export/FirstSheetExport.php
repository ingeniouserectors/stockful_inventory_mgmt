<?php
namespace App\Export;

use App\Page;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class FirstSheetExport implements FromCollection,WithHeadings,WithTitle
{
    public function title(): string
    {
        return "Structure";
    }
    public function headings(): array {
        return [
           "CompanyName","FirstName","LastName","Email","Phone"
        ];
    }
    public function collection() {
        return collect([
            [
                'CompanyName' => 'Test',
                'FirstName' => 'firstuser',
                'LastName' => 'lastuser',
                'Email' => 'user@gmail.com',
                'Phone' => '1234567890'
            ]
        ]);
    }
}
?>
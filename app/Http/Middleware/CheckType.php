<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CheckType
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session()->has('MARKETPLACE_ID')) {
            $global_marketplace = session('MARKETPLACE_ID');
            $userRoleId = get_user_role(Auth::user()->id);
            if($userRoleId['user_assigned_role']['user_role_id']=='3' || $userRoleId['user_assigned_role']['user_role_id']=='5'){
                if($global_marketplace != ''){
                }else{
                    $message = get_messages('You will be able to access everything after you need to  add the marketplace',0);
                    Session::flash('message', $message);
                    return redirect()->route('amazonsettings.index');
                }
            }else{
            }

        }
        return $next($request);
    }
}

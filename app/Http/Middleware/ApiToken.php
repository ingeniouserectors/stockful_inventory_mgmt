<?php

namespace App\Http\Middleware;

use Closure;
class ApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!isset($request->userid)){
            return response()->json('userid complusary', 401);
        }
        if(!isset($request->api_token)){
            return response()->json('api token complusary', 401);
        }
        $authenticate = get_authenticate_user($request->userid,$request->api_token);
        if (empty($authenticate)) {
            return response()->json('Unauthorized', 401);
        }
        return $next($request);
    }
}

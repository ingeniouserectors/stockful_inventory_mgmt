<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Mws_order;
use App\Models\Mws_import_order;
use Response;
use DB;

class MemberorderController extends Controller
{
    /**
     * Display a listing of the all Orders with marketplace wise.
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get_checks = get_access('order_module','view');
        $get_user_access = get_user_check_access('order_module','view');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }
        $global_marketplace = session('MARKETPLACE_ID');
        $marketplace = '';
        if($global_marketplace != ''){
            $marketplace = $global_marketplace;
        }
        return view('memberorders.index',[
            'marketplace'=>$marketplace
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * There is 1 function added in store method :
     *      @method POST
     *
     *  Request Types:
     *
     * 1. upload_csv : Upload csv for orders.
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     */
    public function store(Request $request)
    {
        if($request->insert_type == 'upload_csv'){

            if ($request->hasFile('uploadFile')) {
                $size = $request->file('uploadFile')->getSize();
                $extension = $request->uploadFile->getClientOriginalExtension();
                if ($extension == "csv" && $size <= 100000) {
                    $filePath = $request->file('uploadFile')->getRealPath();
                    $file = fopen($filePath, "r");
                    $escapedHeader = [];
                    $detail = [];
                    $flag = 0;

                    while ($columns = fgetcsv($file)) {
                        $columnValue = [];
                        if ($flag == 0) {
                            if (trim($columns[0]) != 'User marketplace id' || trim($columns[1]) != 'Product id' || trim($columns[2]) != 'Amazone order id' || trim($columns[3]) != 'Purchase date(Y-M-D)' || trim($columns[4]) != 'Order status'  || trim($columns[5]) != 'Product name' || trim($columns[6]) != 'Sku' || trim($columns[7]) != 'Asin'  || trim($columns[8]) != 'Quantity' || trim($columns[9]) != 'Currency' || trim($columns[10]) != 'Item price'  || trim($columns[11]) != 'Item tax' || trim($columns[12]) != 'Shipping price' || trim($columns[13]) != 'Shipping tax' || trim($columns[14]) != 'Ship city' || trim($columns[15]) != 'Ship state' || trim($columns[16]) != 'Ship postal code' || trim($columns[17]) != 'Ship country') {
                                $message = get_messages('Please upload proper file format as provide in sample!', 0);
                                Session::flash('message', $message);
                                return redirect()->route('memberorder.index');
                            }
                        }
                        foreach ($columns as $key => &$value) {
                            if ($flag == 0) {
                                $lowerHeader = strtolower($value);
                                $escapedItems = preg_replace("/[^a-z]/", "", $lowerHeader);
                                array_push($escapedHeader, $escapedItems);
                            } else {
                                array_push($columnValue, $value);
                            }
                        }
                        $flag++;
                        if (!empty($columnValue))
                            $detail[] = array_combine($escapedHeader, $columnValue);
                    }

                    if(!empty($detail)){
                        $uploaded_file = $request->file('uploadFile');
                        $name = 'csv_' . time() . $uploaded_file->getClientOriginalName();
                        $uploaded_file->move(public_path('assets/images/csv/suppliers'), $name);
                        $images[] = $name;
                        $storefolder = 'assets/images/csv/suppliers/csv_' . time() . $uploaded_file->getClientOriginalName();
                    }
                    $i=0;
                    foreach ($detail as $data) {
                        $product = [];
                       $quantity=$data['quantity'];
                        if(strlen($data['amazoneorderid']) <= 0){
                            $message = get_messages('Amazone order id is empty.', 0);
                            Session::flash('message', $message);
                        }
                         else if (strlen($data['quantity']) <= 0) {
                            $message = get_messages('quantity is empty.', 0);
                            Session::flash('message', $message);
                        }
                        else if ($quantity <=  -1 || $quantity <= 0 ) {
                            $message = get_messages('Please enter valid Quantity', 0);
                            Session::flash('message', $message);
                        }

                        else {

                            $amazone_order_id_unique = Mws_import_order::where(array('amazon_order_id'=>$data['amazoneorderid']))->get()->toArray();
                             $mws_amazone_order_id_unique = Mws_order::where(array('amazon_order_id'=>$data['amazoneorderid']))->get()->toArray();

                                if(empty($amazone_order_id_unique ||  $mws_amazone_order_id_unique) ) {
                                    $req_data = [
                                        'user_marketplace_id' => session('MARKETPLACE_ID'),
                                        'amazon_order_id' => $data['amazoneorderid'],
                                        'purchase_date' => $data['purchasedateymd'],
                                        'quantity'=>$data['quantity'], 
                                        'sku' => $data['sku'],
                                        'asin'=> $data['asin'],
                                        'product_id' => $data['asin'],
                                        'product_name' => $data['productname'],
                                        'currency'=>$data['currency'],
                                        'item_price'=>$data['itemprice'],
                                        'item_tax'=>$data['itemtax'],
                                        'shipping_price'=>$data['shippingprice'],
                                        'shipping_tax'=>$data['shippingtax'],
                                        'ship_city'=>$data['shipcity'],
                                        'ship_state'=>$data['shipstate'],
                                        'ship_country'=>$data['shipcountry'],
                                        'ship_postal_code'=>$data['shippostalcode'],
                                        'order_status'=>$data['orderstatus'],

                                    ];
                                    $datas_of = Mws_import_order::create($req_data);
                                    $message = get_messages("csv uploaded successfully !",1);
                                    Session::flash('message', $message);
                                }else{
                                    
                                     $message = get_messages(' Amazon order id already exists',0);
                                     Session::flash('message', $message);
                                      return redirect()->route('memberorder.index');
                                }
                        }
                    }
                    return redirect()->route('memberorder.index');
                }
            }else{
                $message = get_messages('CSV file must be required', 0);
                Session::flash('message', $message);
                return redirect()->route('memberorder.index');
            }
        }else{
            ini_set('memory_limit', '-1');
            $purchase_raw = \DB::raw("STR_TO_DATE(`purchase_date`, '%Y-%m-%d')");
            $data = Mws_order::with(['mws_product'])->where(array('user_marketplace_id'=>$request->id))->where($purchase_raw,'>=', date('Y-m-d',strtotime('-1 month')))->orderBy($purchase_raw,'desc')->get()->toArray();
            echo json_encode($data);
            exit;
        }
    }

    /**
     * Download structure for import mws_orders.
     *
     * @method POST
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         if($id == 'import') {
            $filename = 'mwsorder.csv';
            $headers = array(
                "Content-type" => "text/csv",
                "Content-Disposition" => "attachment; filename=".$filename." ",
                "Pragma" => "no-cache",
                "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                "Expires" => "0"
            );

            $columns = array('User marketplace id','Product id', 'Amazone order id', 'Purchase date(Y-M-D)', 'Order status','Product name','Sku','Asin','Quantity','Currency','Item price','Item tax','Shipping price','Shipping tax','Ship city','Ship state','Ship postal code','Ship country'); $fileputcsv = array('1', '1', 'S01-2659649-1714212', date('Y-m-d'), 'Shipped','dewe','Standard','test', '1','USD','9.99','1.50','1.84','2','GRAY','LA','12345','US');

            $callback = function() use ( $columns,$fileputcsv)
            {
                ob_clean();
                $file = fopen('php://output', 'w+');
                fputcsv($file, $columns);
                fputcsv($file,$fileputcsv);
                fclose($file);
            };
            return Response::stream($callback, 200, $headers);
        }
            }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

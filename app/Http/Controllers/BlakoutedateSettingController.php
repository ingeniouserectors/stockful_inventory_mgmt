<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use App\Models\Blakoutedate_setting;
use Illuminate\Support\Facades\Auth;
use App\Models\Users;
use App\Models\Supplier;
use App\Models\Vendor;
use App\Models\Usermarketplace;
use App\Models\Country;
use App\Models\Vendor_blackout_date;
use App\Models\Supplier_blackout_date;
use App\Models\Member_blackoutdate_setting;
use Response;
use Illuminate\Support\Facades\Gate;
use App\Models\Application_modules;
use App\Models\Role_access_modules;


class BlakoutedateSettingController extends Controller
{
    Protected $userRolePermissionId;

    function __construct()
    {
        $this->middleware(function ($request, $next) {

            if (!Auth::user()) {
                return redirect('login');
            }
            $moduleId = Application_modules::where('module', 'blackoute_setting_module')->first()->id;
            $userRoleID = Users::with('user_roles')->whereIn('id', [Auth::id()])->first()->user_roles[0]->id;
            $this->userRolePermissionId = Role_access_modules::where('role_id', $userRoleID)->where('application_module_id', $moduleId)->first();
            return $next($request);
        });
    }

    /**
     * Display a listing of the backoutdates.
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $get_checks = get_access('blackoute_setting_module','view');
        $get_user_access = get_user_check_access('blackoute_setting_module','view');
        if ($get_checks == 1) {
            if($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }
        
        $global_marketplace = session('MARKETPLACE_ID'); 
        $data["user"] = Users::with(['userdetails', 'user_assigned_role', 'user_roles'])->where('id', Auth::user()->id)->first()->toArray();

        $data['countryDetails'] = Country::get()->toArray();
        if ($data["user"]['user_roles'][0]['id'] == 1 || $data["user"]['user_roles'][0]['id'] == 2)  {
            $marketplace = country::get()->toArray();
            return view('blakoutedates_setting.index',[
                'marketplace' =>$marketplace,
                'marketplaces' =>$global_marketplace,
                'users'=>$data
            ]);
        }
        elseif ($data["user"]['user_roles'][0]['id'] == 3 || $data["user"]['user_roles'][0]['id']==5) {
            $marketplace = country::get()->toArray();
            foreach ($marketplace as $key => $value) {
                $vendors = array();
                $suppliers = array();
                $vendors = Vendor::where(array('country_id' =>$value['country_code'],'user_marketplace_id'=>$global_marketplace))->get();
                $suppliers = Supplier::where(array('country_id'=>$value['country_code'],'user_marketplace_id'=>$global_marketplace))->get();
            }      
            return view('blakoutedates_setting.index',[
                'marketplace' =>$marketplace,
                'marketplaces' =>$global_marketplace,
                'vendors'=>$vendors,
                'suppliers'=>$suppliers,
                'users'=>$data
            ]);
        }
        else {
            $message = get_messages("Don't have Access Rights for this Functionality", 0);
            Session::flash('message', $message);
            return redirect('/index');
        }
    }

    /**
     * Show the form for creating a new backoutdate.
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $data["user"] = Users::with(['userdetails', 'user_assigned_role', 'user_roles'])->where('id', Auth::user()->id)->first()->toArray();
       $data['countryDetails'] = Country::get()->toArray();
  
    if ($data["user"]['user_roles'][0]['id'] == 1 || $data["user"]['user_roles'][0]['id'] == 2) {
        $data['countryDetails'] = Country::all();
        return view('blakoutedates_setting.create', $data);
    } else if ($data["user"]['user_roles'][0]['id'] == 3 || $data["user"]['user_roles'][0]['id'] == 5) {
        
    }
    else {
            $message = get_messages("Don't have Access Rights for this Functionality", 0);
            Session::flash('message', $message);
            return redirect('/index');
        }
         
         return view('blakoutedates_setting.create',$data);
    }

    /**
     * There are 5 function added in store method :
     *      @method POST
     *
     *  Request Types:
     *
     * 1. blackout_date_list : show all backoutdate listing
     *      @param  $requesttype
     *      @return \Illuminate\Http\Response
     *
     * 2. create_form: create from with single date & daterange dates
     *      @param  $requesttype,$request->all()
     *      @return \Illuminate\Http\Response
     *
     * 3. upload_csv: upload direct csv for backoutdate
     *      @param  $country,$backoutdate,$reason,$requesttype
     *      @return \Illuminate\Http\Response
     *
     * 4. show_marketplace_wise_data: Show the records with marketplace wise
     *      @param  $requesttype,$countryid
     *      @return \Illuminate\Http\Response
     *
     * * 5. Assign_vendors_suppliers: Assign vendors & suppliers in products
     *      @param  $requesttype,$requestedall()
     *      @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
       
        $inputs = $request->all();
        $global_marketplace = session('MARKETPLACE_ID');
         $data["user"] = Users::with(['userdetails', 'user_assigned_role', 'user_roles'])->where('id', Auth::user()->id)->first()->toArray();
        if($request->request_type == 'blackout_date_list'){
           
          if ($data["user"]['user_roles'][0]['id'] == 1 || $data["user"]['user_roles'][0]['id'] == 2) {
             $data=Blakoutedate_setting::with('blakoutedates_setting')->where('country_id',1)->get()->toArray();
          
           }
          else {
               $data=Member_blackoutdate_setting::with('usermarketplace','country')->where(array('user_marketplace_id'=> $request->id))->get()->toArray();

               }
          $blackoute_dates_data = array();
          if(!empty($data))
            {
                 foreach ($data as $key => $post)
                {
                    $action = '' ;
                    $nestedData['checkboxes'] =  '<input type="checkbox" name="checkalls[]" class="checkboxes" value="'.$post['id'].'" id="checkalls"  />';
                    $nestedData['blackout_date'] =  $post['blackout_date'];
                    $nestedData['reason'] =  $post['reason'];
                    $action .= '<a href="'.route('blakoutedatesetting.edit',$post['id']).'"  class=" btn btn-info btn-sm btn-rounded waves-effect waves-light" role="button"><i class="fas fa-edit" ></i></a>';
                    $action .=  '<button id="button" type="submit"  class=" btn btn-danger btn-sm btn-rounded waves-effect  waves-light sa-remove" data-id='.$post['id'].'><i class="fas fa-trash-alt"></i></button>';
                    $nestedData['action'] =  $action ;
                    $blackoute_dates_data[] = $nestedData;               
                }    
             }
                
           echo json_encode($blackoute_dates_data); exit;
         }

         
        if($request->insert_type == 'create_form')
        {
            if($request->customRadio == 'single'){
                if($inputs['blackout_date_single'] >= date('Y-m-d')){
                    $get_check_date = Blakoutedate_setting::where(array('blackout_date'=>$inputs['blackout_date_single'],'country_id'=>$inputs['country_id']))->get()->toArray();
                     $get_check_member_blackoute_date= Member_blackoutdate_setting::where(array('blackout_date'=>$inputs['blackout_date_single'],'country_id'=>$inputs['country_id']))->get()->toArray();
               
            if ($data["user"]['user_roles'][0]['id'] == 1 || $data["user"]['user_roles'][0]['id'] == 2) {       
                if(empty($get_check_date)){
                  $backoute_date = [
                    'blackout_date'=> $inputs['blackout_date_single'],
                    'reason'=> $inputs['reason_single'],
                    'country_id'=> $inputs['country_id'],
                   ];
                   $get_usermarket_place_id=Usermarketplace::get()->all(); 

                    $datas_of = Blakoutedate_setting::create($backoute_date);
                     foreach ($get_usermarket_place_id as $key => $value)
                    {

                         $member_backoute_date = [
                                'blackout_date'=> $inputs['blackout_date_single'],
                                'reason'=> $inputs['reason_single'],
                                'country_id'=> $inputs['country_id'],
                                'user_marketplace_id'=>$value['id']
                            ];
                        $datas_of = Member_blackoutdate_setting::create($member_backoute_date);    
                        $message = get_messages(' Blackoute date added successfully', 1);
                        Session::flash('message', $message);
                    }
            }
                else{
                        $message = get_messages(' Blackoute date is already exists!', 0);
                        Session::flash('message', $message);
                    }
            }            
       
           if ($data["user"]['user_roles'][0]['id'] == 3 || $data["user"]['user_roles'][0]['id'] == 5) {

                     if(empty($get_check_member_blackoute_date)){
                         $backoute_date = [
                            'blackout_date'=> $inputs['blackout_date_single'],
                            'reason'=> $inputs['reason_single'],
                            'country_id'=> $inputs['country_id'],
                            'user_marketplace_id'=> $global_marketplace
                        ];
                        $datas_of = Member_blackoutdate_setting::create($backoute_date);
                        $message = get_messages(' Blackoute date added successfully', 1);
                        Session::flash('message', $message); 
                    }
                    else
                    {
                        $message = get_messages(' Blackoute date is already exists!', 0);
                        Session::flash('message', $message);
                    }
            }        

                }else{
                    $message = get_messages(' Invalid backoute date', 0);
                    Session::flash('message', $message);
                }
                return redirect()->route('blakoutedatesetting.index');
            }else if($request->customRadio == 'daterange'){
                if($request->blackout_date_multiple_start > $request->blackout_date_multiple_end ){
                    $message = get_messages(' To date Must be greater than From date', 0);
                    Session::flash('message', $message);
                }else if($request->blackout_date_multiple_start < date('Y-m-d') || $request->blackout_date_multiple_end < date('Y-m-d')){
                    $message = get_messages('Invalid backoute date', 0);
                    Session::flash('message', $message);
                }else{
                    $date = $this->displayDates($request['blackout_date_multiple_start'], $request['blackout_date_multiple_end']);

                  
                    foreach($date as $backoutdate){
                        $get_check_date = Blakoutedate_setting::where(array('blackout_date'=>$backoutdate,'country_id'=>$inputs['country_id']))->get()->toArray();

                          $get_check_member_blackoute_date= Member_blackoutdate_setting::where(array('blackout_date'=>$backoutdate,'country_id'=>$inputs['country_id']))->get()->toArray();
                    if ($data["user"]['user_roles'][0]['id'] == 1 || $data["user"]['user_roles'][0]['id'] == 2) {
                        if(empty($get_check_date)) {
                            $backoute_date = [
                                'blackout_date' => $backoutdate,
                                'reason' => $inputs['reason_multiple'],
                                'country_id' => $inputs['country_id'],
                            ];             
                            $datas_of = Blakoutedate_setting::create($backoute_date);
                              $get_usermarket_place_id=Usermarketplace::get()->all();
                             foreach ($get_usermarket_place_id as $key => $value)
                            {

                                 $member_backoute_date = [
                                        'blackout_date'=> $backoutdate,
                                        'reason'=> $inputs['reason_multiple'],
                                        'country_id'=> $inputs['country_id'],
                                        'user_marketplace_id'=>$value['id']
                                    ];
                                $datas_of = Member_blackoutdate_setting::create($member_backoute_date);    
                                $message = get_messages(' Blackoute date added successfully', 1);
                                Session::flash('message', $message);
                            }
                          }
                           else{
                            $message = get_messages($backoutdate.' Blackoute date is already exists!', 0);
                            Session::flash('message', $message);
                             return redirect()->route('blakoutedatesetting.index');
                            }

                    } 
                      
                       if ($data["user"]['user_roles'][0]['id'] == 3 || $data["user"]['user_roles'][0]['id'] == 5) {
                            if(empty($get_check_member_blackoute_date)) {
                                $backoute_date = [
                                    'blackout_date' => $backoutdate,
                                    'reason' => $inputs['reason_multiple'],
                                    'country_id' => $inputs['country_id'],
                                    'user_marketplace_id'=>$global_marketplace
                                ];

                                 $datas_of = Member_blackoutdate_setting::create($backoute_date);
                                 $message = get_messages(' Blackoute date added successfully', 1);
                                 Session::flash('message', $message);
                            }
                             else{
                                $message = get_messages($backoutdate.' Blackoute date is already exists!', 0);
                                Session::flash('message', $message);
                                 return redirect()->route('blakoutedatesetting.index');
                                }   
                        }

                }      
            }
                return redirect()->route('blakoutedatesetting.index');
            }else{
                $message = get_messages('Something went wrong!', 0);
                Session::flash('message', $message);
                return redirect()->route('blakoutedatesetting.index');
            }
        }
        else if($request->insert_type == 'upload_csv'){
            if ($request->hasFile('uploadFile')) {
                $size = $request->file('uploadFile')->getSize();
                $extension = $request->uploadFile->getClientOriginalExtension();
                if ($extension == "csv" && $size <= 100000) {
                    $filePath = $request->file('uploadFile')->getRealPath();
                    $file = fopen($filePath, "r");
                    $escapedHeader = [];
                    $detail = [];
                    $flag = 0;
                    while ($columns = fgetcsv($file)) {
                        $columnValue = [];
                        if ($flag == 0) {
                            if (($columns[0]) != 'Country [must be 2 character]'||  ($columns[1]) != 'Blackoutdate(Y-M-D)'|| ($columns[2])!='Reason') {
                                $message = get_messages('Please upload proper file format as provide in sample!', 0);
                                Session::flash('message', $message);
                                return redirect()->route('blakoutedatesetting.index');
                            }
                        }
                        foreach ($columns as $key => &$value) {
                            if ($flag == 0) {
                                $lowerHeader = strtolower($value);
                                $escapedItems = preg_replace("/[^a-z]/", "", $lowerHeader);
                                array_push($escapedHeader, $escapedItems);
                            } else {
                                array_push($columnValue, $value);
                            }
                        }
                        $flag++;
                        if (!empty($columnValue))
                            $detail[] = array_combine($escapedHeader, $columnValue);
                    }

                    if(!empty($detail)) {
                        $uploaded_file = $request->file('uploadFile');
                        $name = 'csv_' . time() . $uploaded_file->getClientOriginalName();
                        $uploaded_file->move(public_path('assets/images/csv'), $name);
                        $images[] = $name;
                        $storefolder = 'assets/images/csv/csv_' . time() . $uploaded_file->getClientOriginalName();
                    }

                    foreach ($detail as $data) {
                        $product = [];
                        if (strlen($data['reason']) <= 0) {
                            $message = get_messages('reason is empty.', 0);
                            Session::flash('message', $message);
                        }elseif (strlen($data['countrymustbecharacter']) <= 0){
                            $message = get_messages('Country is empty.', 0);
                            Session::flash('message', $message);
                        }elseif (date('Y-m-d',strtotime($data['blackoutdateymd'])) == '1970-01-01') {
                            $message = get_messages('Enter Valid Blackoute Date.', 0);
                            Session::flash('message', $message);
                        }elseif (date('Y-m-d',strtotime($data['blackoutdateymd'])) < date('Y-m-d')){
                            $message = get_messages('Please enter a date greater than or equal to '.date('Y-m-d').'', 0);
                            Session::flash('message', $message);
                        } else {
                            $get_market_place_id = Country::where(array('country_code'=>$data['countrymustbecharacter']))->first();
                            if(!empty($get_market_place_id)){
                                $check_date = Blakoutedate_setting::where(array('blackout_date'=>date('Y-m-d',strtotime($data['blackoutdateymd'])),'country_id'=>$get_market_place_id->id))->get()->toArray();
                                $check_member_date =Member_blackoutdate_setting::where(array('blackout_date'=>date('Y-m-d',strtotime($data['blackoutdateymd'])),'country_id'=>$get_market_place_id->id))->get()->toArray();
                                  $data["user"] = Users::with(['userdetails', 'user_assigned_role', 'user_roles'])->where('id', Auth::user()->id)->first()->toArray();
                                if($data['user']['user_roles'][0]['id'] == 1 || $data['user']['user_roles'][0]['id'] == 2) { 
                                 $get_usermarket_place_id=Usermarketplace::get()->all();
                      
                                if(empty($check_date)) {
                                    $req_datas = [
                                        'country_id' =>$get_market_place_id->id,
                                        'reason'=>$data['reason'],
                                        'blackout_date' => date('Y-m-d',strtotime($data['blackoutdateymd']))
                                    ];

                                    $datas_of = Blakoutedate_setting::create($req_datas);
                                     foreach ($get_usermarket_place_id as $key => $value)
                                   {

                                    $req_datas = [
                                        'country_id' =>$get_market_place_id->id,
                                        'reason'=>$data['reason'],
                                        'blackout_date' => date('Y-m-d',strtotime($data['blackoutdateymd'])),
                                        'user_marketplace_id'=> $value['id']
                                    ];

                                  
                                    $req_datas = Member_blackoutdate_setting::create($req_datas);
                                   
                                    $message = get_messages('csv uploaded successfully !', 1);
                                    Session::flash('message', $message);

                                  }  
                                }else{
                                    $message = get_messages(date('Y-m-d',strtotime($data['blackoutdateymd'])).' date already exist!', 0);
                                    Session::flash('message', $message);
                                }
                            }
                          }

                           else
                            {
                                $message = get_messages($data['countrymustbecharacter'].' country is not valid!', 0);
                                Session::flash('message', $message);
                            }
                          if ($data["user"]['user_roles'][0]['id'] == 3 || $data["user"]['user_roles'][0]['id'] == 5) { 

                            $get_usermarket_place_id=Usermarketplace::get()->all();   

                          if(empty($check_member_date)) {
                             
                                    $req_datas = [
                                        'country_id' =>$get_market_place_id->id,
                                        'reason'=>$data['reason'],
                                        'blackout_date' => date('Y-m-d',strtotime($data['blackoutdateymd'])),
                                        'user_marketplace_id'=>$global_marketplace
                                    ];
                           
                                    $datas_of = Member_blackoutdate_setting::create($req_datas);
                                    $message = get_messages('csv uploaded successfully !', 1);
                                    Session::flash('message', $message);
                                   

                                }else{
                                    $message = get_messages(date('Y-m-d',strtotime($data['blackoutdateymd'])).' date already exist!', 0);
                                    Session::flash('message', $message);
                            }     }         
                        }
                    }
                    return redirect()->route('blakoutedatesetting.index');
                }
            }else{
                $message = get_messages('CSV file must be required', 0);
                Session::flash('message', $message);
                return redirect()->route('blakoutedatesetting.index');
            }
        }
        else if($request->insert_type == 'show_marketplace_wise_data'){
            $inputs = $request->all();

            $country_code=Country::where('id',$inputs['id'])->first();



             $data["user"] = Users::with(['userdetails', 'user_assigned_role', 'user_roles'])->where('id', Auth::user()->id)->first()->toArray();

              if ($data["user"]['user_roles'][0]['id'] == 3 || $data["user"]['user_roles'][0]['id'] == 5) {

              $datas=Member_blackoutdate_setting::with('usermarketplace','country')->where(array('country_id'=>$inputs['id']))->get()->toArray();
              $vendors=Vendor::where(array('country_id' =>$country_code['country_code']))->get()->toArray();
              $Suppliers=Supplier::where(array('country_id' =>$country_code['country_code']))->get()->toArray();

             }
            else if ($data["user"]['user_roles'][0]['id'] == 1 || $data["user"]['user_roles'][0]['id'] == 2) {
              $vendors=Vendor::where(array('country_id' =>$inputs['id']))->get()->toArray();
              $Suppliers=Supplier::where(array('country_id' =>$inputs['id']))->get()->toArray();
            $datas=Blakoutedate_setting::where(array('country_id'=>$inputs['id']))->get()->toArray();
            }
            
            $view = View::make('blakoutedates_setting.get_marketplace_wise_data', [
                'blackout_date_Details' => $datas,
                'suppliers_list'=>$Suppliers,
                'vendors_list'=>$vendors,
                'users_role_id'=>$data
            ]);
            $html = $view->render();
            $res['error'] = 0;
            $res['view'] = $html;
            return response()->json($res);
        }
          if($request->insert_type == 'Assign_vendors_suppliers'){
            $data=$request->all();
           

            $checkboxesDate = explode(',',$data['checkboxes'][0]);
            $supplier_id = explode(',',$data['supplier_id']);
            $vendor_id = explode(',',$data['vendor_id']);
               
            foreach ($checkboxesDate as $key => $value) {
                $date=Member_blackoutdate_setting::where(array('id'=>$value))->get()->toArray();   
            if(!empty($data['checkboxes'][0])) 
            {   
                     if(!empty($data['vendor_id']))
                        {
                     foreach ($vendor_id as $key => $value) {
                        $backoute_date = [
                            'blackout_date'=> $date[0]['blackout_date'],
                            'reason'=> $date[0]['reason'],
                            'vendor_id'=>$value,
                        ]; 


                        $datas_of = Vendor_blackout_date::create($backoute_date);
                    }
                   }
                     if(!empty($data['supplier_id']))
                        {   
                        foreach ($supplier_id as $key => $value) { 
                        $backoute_date = [
                            'blackout_date'=> $date[0]['blackout_date'],
                            'reason'=> $date[0]['reason'],
                            'supplier_id'=>$value,
                        ]; 


                        $datas_of = Supplier_blackout_date::create($backoute_date);

                      }
                }
              }
              else
              {
                $message = get_messages('Please select any one checkbox', 0);
                Session::flash('message', $message);
                return redirect()->route('blakoutedatesetting.index');
              }  
                
            }
            $message = get_messages(' blackoute date added successfully', 1);
            Session::flash('message', $message);
            return redirect()->route('blakoutedatesetting.index');
         }
                
    }

    /**
     * Display dates with proper formating
     *
     * @param  $date1,$date2,$format
     * @return \Illuminate\Http\Response
     */

    function displayDates($date1, $date2, $format = 'Y-m-d' ) {
        $dates = array();
        $current = strtotime($date1);
        $date2 = strtotime($date2);
        $stepVal = '+1 day';
        while( $current <= $date2 ) {
            $dates[] = date($format, $current);
            $current = strtotime($stepVal, $current);
        }
        return $dates;
    }

    /**
     * Doewnload the csv structure to upload backoutdate.
     *
     * @method POST
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $filename = 'blackoutesetting.csv';
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=" . $filename . " ",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );
        $columns = array('Country [must be 2 character]','Blackoutdate(Y-M-D)','Reason');
        $fileputcsv = array('US',date('Y-m-d'),'Test');

        $callback = function () use ($columns, $fileputcsv) {
            ob_clean();
            $file = fopen('php://output', 'w+');
            fputcsv($file, $columns);
            fputcsv($file, $fileputcsv);
            fclose($file);
        };
        return Response::stream($callback, 200, $headers);
    }

    /**
     * Show the form for editing the specified backoutdate.
     *
     * @method POST
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data["user"] = Users::with(['userdetails', 'user_assigned_role', 'user_roles'])->where('id', Auth::user()->id)->first()->toArray();
       if ($data["user"]['user_roles'][0]['id'] == 1 || $data["user"]['user_roles'][0]['id'] == 2) { 
          $blackout_date_Details = Blakoutedate_setting::where(array('id'=>$id))->get()->first();
          $get_market_palce = Country::where(array('id'=>$blackout_date_Details['country_id']))->get()->first();
        }
        elseif($data["user"]['user_roles'][0]['id'] == 3 || $data["user"]['user_roles'][0]['id'] == 5)  {
            $blackout_date_Details = Member_blackoutdate_setting::where(array('id'=>$id))->get()->first();
            $get_market_palce = Country::where(array('id'=>$blackout_date_Details['country_id']))->get()->first(); 
        }
        else {
            $message = get_messages("Don't have Access Rights for this Functionality", 0);
            Session::flash('message', $message);
            return redirect('/index');
        }

        return view('blakoutedates_setting.edit', [
                'blackout_date_Details' => $blackout_date_Details,
                'country'=> $get_market_palce['country_name'],
                'marketplace' => $get_market_palce['country_code'],
                'country_id' => $get_market_palce['id'],
            ]
        );
    }

    /**
     * Update the specified backoutdate  in storage.
     * @method POST
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id,$request_all()
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

     $input = $request->all();
     $data["user"] = Users::with(['userdetails', 'user_assigned_role', 'user_roles'])->where('id', Auth::user()->id)->first()->toArray();
        if($request->insert_type == 'edit_form')
        {
         
         $blackout_date = $request->blackout_date;
         $reason = $request->reason;
         $check_blackout_date = Blakoutedate_setting::where(array(['blackout_date','=',trim($input['blackout_date'])],['id','!=',$id]))->get()->toArray();    
            if ($data["user"]['user_roles'][0]['id'] == 1 || $data["user"]['user_roles'][0]['id'] == 2) { 

                if(empty($check_blackout_date)) { 

                    $sub_data = [
                                        'blackout_date' => $blackout_date,
                                        'reason'=>$reason
                                    ];
                 $blakoute_date_setting = Blakoutedate_setting::findOrFail($id);
                 $blakoute_date_setting ->update($sub_data);
                 $message = get_messages(' Blackoute date Updated successfully', 1);
                 Session::flash('message', $message);
                
                }
                else
                {
                   $message = get_messages('Blackoute date is already exists!', 0);
                   Session::flash('message', $message);
                   
                }
                 return redirect()->route('blakoutedatesetting.index');
            }    
            
             if ($data["user"]['user_roles'][0]['id'] == 3 || $data["user"]['user_roles'][0]['id'] == 5) { 

                  $check_member_blackout_date = Member_blackoutdate_setting::where(array(['blackout_date','=',trim($input['blackout_date'])],['id','!=',$id]))->get()->toArray();


                 if(empty($check_member_blackout_date)) {

                     $sub_data = [
                                            'blackout_date' => $blackout_date,
                                            'reason'=>$reason
                                        ];
                     $blakoute_date_setting = Member_blackoutdate_setting::findOrFail($id);
                     $blakoute_date_setting ->update($sub_data);
                     $message = get_messages(' blackoute date Updated successfully', 1);
                     Session::flash('message', $message);
                }
                else
                {
                  $message = get_messages('Blackoute date is already exists!', 0);
                   Session::flash('message', $message);
                    
                }     

                return redirect()->route('blakoutedatesetting.index');
            }   
                            
        }
    }

    /**
     * soft delete the specified backoutdate from storage.
     * @method POST
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data["user"] = Users::with(['userdetails', 'user_assigned_role', 'user_roles'])->where('id', Auth::user()->id)->first()->toArray();
        if ($data["user"]['user_roles'][0]['id'] == 1 || $data["user"]['user_roles'][0]['id'] == 2) 
        {
             $blackoute_date = Blakoutedate_setting::findOrFail($id);
            $blackoute_date->delete();
        }
        else
        {
            $blackoute_date = Member_blackoutdate_setting::findOrFail($id);
            $blackoute_date->delete();
        }
        return json_encode(array('statusCode'=>200));
    }     
}

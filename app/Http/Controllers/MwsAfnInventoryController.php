<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mws_afn_inventory_data;
use Illuminate\Support\Facades\Session;


class MwsAfnInventoryController extends Controller
{
    /**
     * Display a listing of the mws_product with mws_afn_inventories.
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get_checks = get_access('afn_inventory_module','view');
        $get_user_access = get_user_check_access('afn_inventory_module','view');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }
        $global_marketplace = session('MARKETPLACE_ID');
        $marketplace = '';
        if($global_marketplace != ''){
            $marketplace = $global_marketplace;
        }
        return view('mws_afn_inventories.index',[
            'marketplace'=>$marketplace
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     *
     * Get all Mws_afn_inventory_data & mws_product with particular marketplace id  .
     * @method POST
     * @param  $marketplaceid
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $afninventory= Mws_afn_inventory_data::with('usermarketplace','mws_product')->where(array('user_marketplace_id'=>$request->id))->orderBy('quantity_available','desc')->get()->toArray();
        echo json_encode($afninventory); exit;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\Session;
use App\Models\Supply_reorder_logic;
use App\Models\Mws_product;
use App\Models\Product_assign_supply_logic;

class SupplyreorderlogicController extends Controller
{
    /**
     * Display a listing of the supply order logic with mws pproduct.
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get_checks = get_access('supply_order_logic_module','view');
        $get_user_access = get_user_check_access('supply_order_logic_module','view');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }
        $global_marketplace = session('MARKETPLACE_ID');
        $data = Supply_reorder_logic::where('user_marketplace_id',$global_marketplace)->get()->toArray();
        $product_list=array();
        $product_list=Mws_product::all();
       // echo "<pre>"; print_r($product_list); exit;
        return view('supply_order_logic.index',[
            'logic_data'=>$data,
            'product_list'=>$product_list
        ]);
    }

    /**
     * Show the form for supply order logic
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $get_checks = get_access('supply_order_logic_module','create');
        $get_user_access = get_user_check_access('supply_order_logic_module','create');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }

        return view('supply_order_logic.create');
    }

    /**
     * There are 2 function added in store method :
     *      @method POST
     *
     *  Request Types:
     *
     * 1. supply_order_logic_list : listing of supply order logic
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     * * 2. Creates_form: assign product supply logic
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->request_type == 'supply_order_logic_list'){
            $global_marketplace = session('MARKETPLACE_ID');
            $data = Supply_reorder_logic::where('user_marketplace_id',$global_marketplace)->get()->toArray();
            $get_supply_order_logic_data = array();
         if(!empty($data))
            {
                 foreach ($data as $key => $post)
                {
                    $action = '' ;
                    $nestedData['logic_label_name'] =  $post['logic_label_name'];
                    $action .= '<a href="'.route('supplyorderlogic.edit',$post['id']).'" class=" btn btn-info btn-sm btn-rounded waves-effect waves-light" ><i class="fas fa-edit" title="edit"></i></a>';
                    $action .=  '<button id="button" type="submit"  class=" btn btn-danger btn-sm btn-rounded waves-effect  waves-light sa-remove " data-id='.$post['id'].'><i class="fas fa-trash-alt"></i></button>';
                    $action .= '<button type="button" class=" get_lableid btn btn-info btn-sm btn-rounded waves-effect waves-light" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false" data-product_id='.$post['id'].' title="Apply Order logic"><i class="fa fa-list"></i></button>';
                    $nestedData['action'] =  $action ;
                    $get_supply_order_logic_data[] = $nestedData;               
                }    
             }
                
           echo json_encode($get_supply_order_logic_data); exit;


       }
       if($request->input_type == 'Creates_form'){
          $inputs=$request->all();    
          $prod_id = $inputs['pro_id'];    
          $check_already_assign_supply_logic = Product_assign_supply_logic::where(array('product_id'=> $prod_id))->get()->toArray();     
            if(empty($check_already_assign_supply_logic)) {
                foreach ($prod_id as $key => $value) {

                $datas = [
                       'product_id' => $value,
                       'label_id'=>$inputs['logic_label_id'],
                       'status'=>0
                   ];     
                
                $datas_of = Product_assign_supply_logic::create($datas);
             } 
            }
            
            else {
                $check_assign_data = Product_assign_supply_logic::where(array('product_id'=>$prod_id,'status' => 0,'label_id'=>$inputs['logic_label_id']))->get()->toArray();
                if(empty($check_assign_data)) {
                    $update_status['status'] = 1;
                    foreach ($prod_id as $key => $value) {
                    $datas_of = Product_assign_supply_logic::where(array('product_id' => $value))->update($update_status);                
                    $data = [
                        'product_id' =>$value,
                        'label_id' => $inputs['logic_label_id'],
                        'status' => 0
                    ];
                    $datas_of = Product_assign_supply_logic::create($data);
                }
                }else{
                    $datas_of = 1;
                }
            }
        
            if($datas_of){
                $res['error'] = 0;
                $res['message'] = get_messages('Product supply logic assigned ', 1);
            }else{
                $res['error'] = 1;
                $res['message'] = get_messages('Failed to assign product supply logic ',0);
            }
            return response()->json($res);
         }
        $inputs =  $request->all();
        $supply_history=$inputs['supply_last_year_sales_percentage'];
        $supply_order = 
            $inputs['supply_recent_last_7_day_sales_percentage'] +
            $inputs['supply_recent_last_14_day_sales_percentage'] +
            $inputs['supply_recent_last_30_day_sales_percentage'];
        $supply_trend_order =
            $inputs['supply_trends_year_over_year_historical_percentage'] +
            $inputs['supply_trends_year_over_year_current_percentage'] +
            $inputs['supply_trends_multi_month_trend_percentage'] +
            $inputs['supply_trends_30_over_30_days_percentage'] +
            $inputs['supply_trends_multi_week_trend_percentage'] +
            $inputs['supply_trends_7_over_7_days_percentage'] ;
        $reorder_history=$inputs['reorder_last_year_sales_percentage'];    
        $reorder = 
            $inputs['reorder_recent_last_7_day_sales_percentage'] +
            $inputs['reorder_recent_last_14_day_sales_percentage'] +
            $inputs['reorder_recent_last_30_day_sales_percentage'];
        $reorder_trend =
            $inputs['reorder_trends_year_over_year_historical_percentage'] +
            $inputs['reorder_trends_year_over_year_current_percentage'] +
            $inputs['reorder_trends_multi_month_trend_percentage'] +
            $inputs['reorder_trends_30_over_30_days_percentage'] +
            $inputs['reorder_trends_multi_week_trend_percentage'] +
            $inputs['reorder_trends_7_over_7_days_percentage']; 
            $stock=$inputs['reorder_safety_stock_percentage'] ;
        if(($supply_order == 100.00 || $supply_history <= 100.00 ) && ($reorder == 100.00 ||  $reorder_history <=100) && $supply_trend_order == 100.00 && $reorder_trend == 100.00  && $stock <=100 ) {
             $check_lable_name = Supply_reorder_logic::where(array(['logic_label_name','=',trim($inputs['logic_label_name'])]))->get()->toArray();        
            
             if(empty($check_lable_name))
              {
                $insert_data = Supply_reorder_logic::create($request->all());
                 $message = get_messages('Supply order logic added successfully', 1);
                Session::flash('message', $message);
                return redirect()->route('supplyorderlogic.index');
              
             }      
           
           else {
                $message = get_messages('Label name already exists!', 0);
                Session::flash('message', $message);
                return redirect()->route('supplyorderlogic.index');
            }
        }else{
            $message = get_messages('Supply order or reorder percentage must be 100 and at present the total of all the fields exceeds 100', 0);
            Session::flash('message', $message);
            return redirect()->route('supplyorderlogic.index');
        }

       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified supply order logic.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $get_checks = get_access('supply_order_logic_module', 'edit');
        $get_user_access = get_user_check_access('supply_order_logic_module','edit');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }

        $data=Supply_reorder_logic::with('product_assing_supply_logic','usermarketplace')->where('id',$id)->first()->toArray();
        
        return view('supply_order_logic.edit',[

            'supply_rorder_logic_details'=>$data

        ]);
    }

    /**
     * Update the specified supply reorder logic.
     * @method POST
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs =  $request->all();
        $supply_history=$inputs['supply_last_year_sales_percentage'];
        $supply_order = 
            $inputs['supply_recent_last_7_day_sales_percentage'] +
            $inputs['supply_recent_last_14_day_sales_percentage'] +
            $inputs['supply_recent_last_30_day_sales_percentage'];
        $supply_trend_order =
            $inputs['supply_trends_year_over_year_historical_percentage'] +
            $inputs['supply_trends_year_over_year_current_percentage'] +
            $inputs['supply_trends_multi_month_trend_percentage'] +
            $inputs['supply_trends_30_over_30_days_percentage'] +
            $inputs['supply_trends_multi_week_trend_percentage'] +
            $inputs['supply_trends_7_over_7_days_percentage'] ;
        $reorder_history=$inputs['reorder_last_year_sales_percentage'];    
        $reorder = 
            $inputs['reorder_recent_last_7_day_sales_percentage'] +
            $inputs['reorder_recent_last_14_day_sales_percentage'] +
            $inputs['reorder_recent_last_30_day_sales_percentage'];
        $reorder_trend =
            $inputs['reorder_trends_year_over_year_historical_percentage'] +
            $inputs['reorder_trends_year_over_year_current_percentage'] +
            $inputs['reorder_trends_multi_month_trend_percentage'] +
            $inputs['reorder_trends_30_over_30_days_percentage'] +
            $inputs['reorder_trends_multi_week_trend_percentage'] +
            $inputs['reorder_trends_7_over_7_days_percentage']; 
            $stock=$inputs['reorder_safety_stock_percentage'] ;
        if(($supply_order == 100.00 || $supply_history <= 100.00 ) && ($reorder == 100.00 ||  $reorder_history <=100) && $supply_trend_order == 100.00 && $reorder_trend == 100.00  && $stock <=100 ) {
             $check_lable_name = Supply_reorder_logic::where(array(['logic_label_name','=',trim($inputs['logic_label_name'])],['id','!=',$id]))->get()->toArray();        
            
             if(empty($check_lable_name))
              {

                 $get_data=Supply_reorder_logic::findorFail($id);
                 $get_data->update($request->all());

                $message = get_messages('Supply order logic data updated successfully', 1);
                Session::flash('message', $message);
                return redirect()->route('supplyorderlogic.index');

              }

             else
             {
                $message = get_messages('Label name already exists!', 0);
                Session::flash('message', $message);
                return redirect()->route('supplyorderlogic.index');
             }


        }else{
            $message = get_messages('Supply order or reorder percentage must be 100 and at present the total of all the fields exceeds 100', 0);
            Session::flash('message', $message);
            return redirect()->route('supplyorderlogic.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supply_order_logic_Details = Supply_reorder_logic::findorFail($id);
        $supply_order_logic_Details->delete();
        return json_encode(array('statusCode'=>200));
    }

    
}

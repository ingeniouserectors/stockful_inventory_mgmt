<?php

namespace App\Http\Controllers;

use App\Models\Application_modules;
use App\Models\Role_access_modules;
use App\Models\User_access_modules;
use App\Models\Users;
use Illuminate\Http\Request;
use App\Models\Userrole;
use App\Models\Usermodule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
class RoleController extends Controller
{

    Protected $userRolePermissionId;

    function __construct()
    {
        $this->middleware(function ($request, $next) {

            $moduleId = Application_modules::where('module', 'role_module')->first()->id;
            $userRoleID = Users::with('user_roles')->whereIn('id', [Auth::id()])->first()->user_roles[0]->id;
            $this->userRolePermissionId = Role_access_modules::where('role_id', $userRoleID)->where('application_module_id', $moduleId)->first();
            return $next($request);
        });
    }

    /**
     * Display a listing of the roles & permissions.
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $get_checks = get_access('role_module','view');
        $get_user_access = get_user_check_access('role_module','view');
        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
                if ($get_user_access == 0) {
                    $message = get_messages("Don't have Access Rights for this Functionality", 0);
                    Session::flash('message', $message);
                    return redirect('/index');
                }
        }
        $resultRolesDetails = Userrole::get()->toArray();
        return view('roles.index', ['roles_details' => $resultRolesDetails,'userpermission' => $this->userRolePermissionId]);
    }

    /**
     * Show the form for creating a new role.
     * @method GET
     * @params no-params
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $get_user_access = get_user_check_access('role_module','create');
        if (Gate::allows('create', $this->userRolePermissionId)) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }
        return view('roles.create-role');
    }

    /**
     * There are 3 function added in store method :
     *      @method POST
     *
     *  Request Types:
     *
     * 1. user_role_list : listing of all roles and get the paermission lists
     *      @param  $request
     *      @return \Illuminate\Http\Response
     *
     * 2. rolewiseuseraccessmodules: Get the permission with particular role
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     * 3. useraccessrolewisemodules: Get the permission with particular role & particular user wise
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     */


    public function store(Request $request)
    {
     if($request->request_type == 'user_role_list'){

         $resultRolesDetails = Userrole::get()->toArray();
    
          $roles_data = array();
         if(!empty($resultRolesDetails))
            {
                foreach ($resultRolesDetails as $key => $post)
                {
                    $action = '' ;
                    $get_user_access = get_user_check_access('role_module','edit');
                    if(Gate::allows('edit', $this->userRolePermissionId) || $get_user_access != 0){
                        $nestedData['role'] ='<a href="'.route('roles.show',$post['id']).'"> '.$post['role'].' </a>';
                    }
                    else
                    {
                    $get_user_access1 = get_user_check_access('role_module','view');
                        if(Gate::allows('view', $this->userRolePermissionId) || $get_user_access1 != 0)
                        {
                            $nestedData['role']=$post['role'];
                        }

                    }    
                 if(Gate::allows('edit', $this->userRolePermissionId) || $get_user_access != 0){   
                    $action .= '<a href="'.route('roles.edit',$post['id']).'" class=" btn btn-info btn-sm btn-rounded waves-effect waves-light" ><i class="fas fa-edit" title="edit"></i></a>';
                  }     
                    $nestedData['action'] =  $action ;
                    $roles_data[] = $nestedData;               
                }    
             }
                
           echo json_encode($roles_data); exit;


       }   

        if(isset($request->dataroletype) && $request->dataroletype == 'rolewiseuseraccessmodules'){
            $input = $request->all();
            $get_role_wise_user = Users::with('user_roles')->whereHas(
                'user_roles', function($q) use($input) {$q->where(array('user_role_id'=>$input['role_id']));})->get()->toArray();;
            if($input['role_id'] == 3){
                $add_old_members_permission = add_old_members_permission();
            }
             if(!empty($get_role_wise_user)){
                 foreach($get_role_wise_user as $total_user){
                     $get_user_access = User_access_modules::where(array('user_id'=>$total_user['id'],'application_module_id'=>$input['application_module_id']))->get()->first();
                     if(!empty($get_user_access)){
                         if ($input['type_value'] == 0) {
                             $update[$input['type']] = 1;
                         } else if ($input['type_value'] == 1) {
                             $update[$input['type']] = 0;
                         }
                         $get_user_access->update($update);
                     }else{
                        $user_assign_role['user_id'] = $total_user['id'];
                        $user_assign_role['application_module_id'] = $input['application_module_id'];
                         if ($input['type_value'] == 0) {
                             $user_assign_role[$input['type']] = 1;
                         } else if ($input['type_value'] == 1) {
                             $user_assign_role[$input['type']] = 0;
                         }
                         
                         User_access_modules::create($user_assign_role);
                     }
                 }
             }
            $accessModulesDetails = Role_access_modules::where(['id' => $input['id'], 'role_id' => $input['role_id']])->get()->first();
            if ($input['type_value'] == 0) {
                $update[$input['type']] = 1;
            } else if ($input['type_value'] == 1) {
                $update[$input['type']] = 0;
            }
            $accessModulesDetails->update($update);
            return response()->json(array('sucess' => 'sucess')
            );
        }else if(isset($request->dataroletype) && $request->dataroletype == 'useraccessrolewisemodules'){
            $input = $request->all();
            if($input['role_id'] == 3){
                $add_old_members_permission = add_old_members_permission();
            }
            $accessModulesDetails = User_access_modules::where(['id' => $input['id']])->get()->first();
            if ($input['type_value'] == 0) {
                $update[$input['type']] = 1;
            } else if ($input['type_value'] == 1) {
                $update[$input['type']] = 0;
            }
            $accessModulesDetails->update($update);
            return response()->json(array('sucess' => 'sucess')
            );
        }else{
            $get_user_access = get_user_check_access('role_module','create');
            if (Gate::allows('create', $this->userRolePermissionId)) {
                if ($get_user_access == 0) {
                    $message = get_messages("Don't have Access Rights for this Functionality", 0);
                    Session::flash('message', $message);
                    return redirect('/index');
                }
            } else {
                if ($get_user_access == 0) {
                    $message = get_messages("Don't have Access Rights for this Functionality", 0);
                    Session::flash('message', $message);
                    return redirect('/index');
                }
            }
            $request->validate([
                'role' => ['required']
            ]);
            $input = $request->all();
            $check_userrole = Userrole::where(array(['role','=',trim($input['role'])]))->get()->toArray(); 
           
             if(empty($check_userrole))
             {
                Userrole::create($input);
                $message = get_messages('Role created successfully!',1);
                Session::flash('message', $message);
                return redirect()->route('roles.index');

               $message = get_messages("Duplicate role should not be allowed!", 0);
                    Session::flash('message', $message);
                    return redirect()->route('roles.index');
              }
              else
              {

               $message = get_messages("Role already exists!", 0);
                    Session::flash('message', $message);
                    return redirect()->route('roles.index');
              } 
           
        }

    }

    /**
     * Display the specified roles wise module list.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $get_user_access = get_user_check_access('role_module','edit');
        if (Gate::allows('edit', $this->userRolePermissionId)) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }
        $resultRolesDetails = Userrole::get()->toArray();
        $resultModulesDetails = Usermodule::get()->toArray();
        foreach ($resultModulesDetails as $modules) {
            foreach ($resultRolesDetails as $roles) {
                $accessModulesDetails = Role_access_modules::where(['role_id' => $roles['id'], 'application_module_id' => $modules['id']])->get()->toArray();
                if (empty($accessModulesDetails)) {
                    if ($roles['id'] == 1) {
                        $create = [
                            'role_id' => $roles['id'],
                            'application_module_id' => $modules['id'],
                            'create' => 1,
                            'edit' => 1,
                            'view' => 1,
                            'delete' => 1,
                            'access' => 1,
                        ];
                    } else {
                        $create = [
                            'role_id' => $roles['id'],
                            'application_module_id' => $modules['id'],
                        ];
                    }
                    Role_access_modules::create($create);

                }
            }
        }
        $application_module = Role_access_modules::with(['application_modules'])->where(array('role_id' => $id))->get()->toArray();
        $resultRolesName = Userrole::where(['id' => $id])->first()->toArray();
        return view('roles.modules-list', [
                'module_details' => $application_module,
                'rolesName' => $resultRolesName['role'],
                'userpermission' => $this->userRolePermissionId
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     * @method GET
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $get_user_access = get_user_check_access('role_module','edit');
        if (Gate::allows('edit', $this->userRolePermissionId)) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }
        $roleDetails = Userrole::findOrFail($id);
        return view('roles.edit-role', [
                'roleDetails' => $roleDetails
            ]
        );
    }

    /**
     * Update the specified role.
     * @method PUT
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $get_user_access = get_user_check_access('role_module','edit');
        if (Gate::allows('edit', $this->userRolePermissionId)) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }

        $input = $request->all();

       $check_userrole = Userrole::where(array(['role','=',trim($input['role'])],['id','!=',$id]))->get()->toArray(); 
           
             if(empty($check_userrole))
             {
                $roleDetails = Userrole::findOrFail($id);
                $roleDetails->update($input);
                $message = get_messages('Role updated successfully!',1);
                Session::flash('message', $message);
                return redirect()->route('roles.index');

              }
              else
              {

               $message = get_messages("Role already exists!", 0);
                    Session::flash('message', $message);
                    return redirect()->route('roles.index');
              } 
        
    }
    /**
     * Remove the specified role from storage.
     * @method POST
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $get_user_access = get_user_check_access('role_module','delete');
        if (Gate::allows('delete', $this->userRolePermissionId)) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }

        $roleDetails = Userrole::findOrFail($id);
        $roleDetails->delete();
        $message = get_messages('Role deleted successfully!',1);
        Session::flash('message', $message);
        return redirect()->route('roles.index');
    }
}

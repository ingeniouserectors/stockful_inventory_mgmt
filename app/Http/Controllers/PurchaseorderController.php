<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Models\Warehouse;
Use App\Models\Mws_product;
Use App\Models\Purchase_instance_details;
Use App\Models\Purchase_instances;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Models\Purchase_orders;
use App\Models\Purchase_order_details;  
use App\Models\Daily_logic_Calculations;
use Response;
use Illuminate\Support\Facades\Gate;
use App\Models\Application_modules;
use App\Models\Role_access_modules;
use App\Models\Users;


class PurchaseorderController extends Controller
{
    Protected $userRolePermissionId;

    function __construct()
    {
        $this->middleware(function ($request, $next) {

            if (!Auth::user()) {
                return redirect('login');
            }
            $moduleId = Application_modules::where('module', 'po_module')->first()->id;
            $userRoleID = Users::with('user_roles')->whereIn('id', [Auth::id()])->first()->user_roles[0]->id;
            $this->userRolePermissionId = Role_access_modules::where('role_id', $userRoleID)->where('application_module_id', $moduleId)->first();
            return $next($request);
        });
    }

    /**
     * Display a listing of the pending & completed purchase orders.
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $get_checks = get_access('po_module','view');
        $get_user_access = get_user_check_access('po_module','view');
        if ($get_checks == 1) {
            if($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }

        $global_marketplace = session('MARKETPLACE_ID');
        $marketplace = '';
            $Completed_Purchase_order=Purchase_orders::with('purachse_order_details','product')->where('status', 2)->where(array('user_marketplace_id'=>$global_marketplace))->get()->ToArray();
             $pending_purchase_order=Purchase_orders::with('purachse_order_details','product')->where('status', 1)->where(array('user_marketplace_id'=>$global_marketplace))->get()->ToArray();
            if($global_marketplace != ''){
            $marketplace = $global_marketplace;
        }

        return view('purchase_order.index',[
            'marketplace'=> $marketplace,
             'Completed_Purchase_order'=>$Completed_Purchase_order,
             'pending_purchase_order'=>$pending_purchase_order
        ]);
    }


    /**
     * Get daily logic calculation display with purchase order
     * @method GET
     * @param  no-params
     * @return \Illuminate\Http\Response
     */
    public  function daily_logic_calculations($id=null)
    {
        if($id != null){

            $details=Daily_logic_Calculations::with('usermarketplace','mws_product')->where('prod_id',$id)->get()->first();
            $product_data = mws_product::where(array('id'=>$id))->get()->first();

            return view('purchase_order.reorder_details_view', [
                'reorder_details'=>$details,
                'product_data'=>$product_data
                
            ]);

        }else{

            $get_user_access = get_user_check_access('reorder_module','view');
            if (Gate::allows('view', $this->userRolePermissionId)) {
                if($get_user_access == 0) {
                    $message = get_messages("Don't have Access Rights for this Functionality", 0);
                    Session::flash('message', $message);
                    return redirect('/index');
                }
            } else {
                if($get_user_access == 0) {
                    $message = get_messages("Don't have Access Rights for this Functionality", 0);
                    Session::flash('message', $message);
                    return redirect('/index');
                }
            }

            $global_marketplace = session('MARKETPLACE_ID');

            if($global_marketplace != ''){
                $marketplace = $global_marketplace;
            }else{
                    $marketplace = '';
                }

            return view('purchase_order.daily_logic_calculation_list', [
                'marketplace'=> $marketplace
            ]);

        }
    }
    

    /**    
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * There are 5 function added in store method :
     *      @method POST
     *
     *  Request Types:
     *
     * 1. purchase_order_list : listing of pending purchase orders with marketplace wise
     *      @param  $requesttype
     *      @return \Illuminate\Http\Response
     *
     * 2. comleted_purchase_order_list: listing of completed purchase orders with marketplace wise
     *      @param  $requesttype
     *      @return \Illuminate\Http\Response
     *
     * 3. reorder_list: listing of reorder lists with marketplace wise
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     * 4. confirm_type: update the status of the purchase order
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     * 5. upload_csv: upload purchase orders with csv files
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     */
    public function store(Request $request)
    {

        if($request->request_type == 'purchase_order_list'){    
             $global_marketplace = session('MARKETPLACE_ID');
            $data=Purchase_orders::with('purachse_order_details','product')->where('status', 1)->where(array('user_marketplace_id'=>$global_marketplace))->get()->ToArray();        
          $pendding_data = array();
         if(!empty($data))
            {
                 foreach ($data as $key => $post)
                {
                    $action = '' ;
                    
                    $nestedData['created_at'] =  date('Y-m-d',strtotime($post['created_at']));
                    $nestedData['track_id'] =  $post['track_id'];
                    $nestedData['date_of_ship'] =  date('Y-m-d',strtotime($post['date_of_ship']));
                    $nestedData['date_arrived'] =  date('Y-m-d',strtotime($post['date_arrived']));
                    $nestedData['reorder_date'] =  date('Y-m-d',strtotime($post['reorder_date']));
                    $nestedData['days_of_supply'] =  date('Y-m-d',strtotime($post['days_of_supply']));
                    $action .= '<a href="'.route('purchaseorder.edit',$post['id']).'" class=" btn btn-primary btn-sm btn-rounded waves-effect waves-light" role="button" title="Contact"><i class="fas fa-eye"></i></a>';
                    if($post['date_arrived'] >= date('Y-m-d'))
                    {
                       $action .= '<button  class=" btn btn-primary btn-sm btn-rounded waves-effect  waves-light sa-remove confirm_PO" data-id='.$post['id'].' title="Confirm PO"><i class="fas fa-check-circle"></i></button>';
                    }                    
                    $nestedData['action'] =  $action ;
                    $pendding_data[] = $nestedData;
                }    
             }
                
           echo json_encode($pendding_data); exit;

        }
        if($request->request_type == 'comleted_purchase_order_list'){    
            $global_marketplace = session('MARKETPLACE_ID');
            $data=Purchase_orders::with('purachse_order_details','product')->where('status', 2)->where(array('user_marketplace_id'=>$global_marketplace))->get()->ToArray();        
          $received_data = array();
         if(!empty($data))
            {
                 foreach ($data as $key => $post)
                {
                    $action = '' ;
                    
                    $nestedData['created_at'] =  date('Y-m-d',strtotime($post['created_at']));
                    $nestedData['track_id'] =  $post['track_id'];
                    $nestedData['date_of_ship'] =  date('Y-m-d',strtotime($post['date_of_ship']));
                    $nestedData['date_arrived'] =  date('Y-m-d',strtotime($post['date_arrived']));
                    $nestedData['reorder_date'] =  date('Y-m-d',strtotime($post['reorder_date']));
                    $nestedData['days_of_supply'] =  date('Y-m-d',strtotime($post['days_of_supply']));
                    $action .= '<a href="'.route('purchaseorder.edit',$post['id']).'" class=" btn btn-primary btn-sm btn-rounded waves-effect waves-light" role="button" title="Contact"><i class="fas fa-eye"></i></a>';
                    $nestedData['action'] =  $action ;
                    $received_data[] = $nestedData;
                }    
             }
                
           echo json_encode($received_data); exit;

        }

       else if($request->request_type == 'reorder_list'){
         $reorder_listing=Daily_logic_Calculations::with('usermarketplace','mws_product')->where(array('user_marketplace_id'=>$request->id))->where(array(['projected_reorder_qty','!=',0]))->get()->toArray();
          $rorder_data = array();
         if(!empty($reorder_listing))
            {
                 foreach ($reorder_listing as $key => $post)
                {
                    $action = '' ;
                    $nestedData['prod_name'] = '<span title="'.@$post['mws_product']['prod_name'].'">'.substr(strip_tags(@$post['mws_product']['prod_name']),0,25)."...".'</span>';
                    $nestedData['prod_image'] = '<img width="100px" src="'.$post['mws_product']['prod_image'].'" title="'.@$post['mws_product']['prod_image'].'">';
                    $nestedData['sku'] =  $post['mws_product']['sku'];
                    $nestedData['price'] =  $post['mws_product']['your_price'];
                    $nestedData['total_inventory'] =  $post['total_inventory'];
                    $nestedData['days_supply'] =  $post['days_supply'];
                    $nestedData['projected_reorder_date'] =  date('Y-m-d',strtotime($post['projected_reorder_date']));
                    $nestedData['projected_reorder_qty'] =  $post['projected_reorder_qty'];
                    $nestedData['sales_price'] = $post['mws_product']['sales_price'];

                    $action .= '<a href="'.url('reorder',$post['mws_product']['id']).'"  class="btn btn-primary btn-sm btn-rounded waves-effect waves-light"  role="button" title="Detail"><i class="fas fa-eye"></i></a>';
                    $action .= '<a href="'.url('products_sales_rate/'.$post['mws_product']['id']).'" data-product_id='.$post['mws_product']['id'].' type="submit" class=" btn btn-info btn-sm btn-rounded waves-effect waves-light" title="Projected Reorder for product"><i class="fa fa-percent"></i></a>';
                    $action .= '<a href="javascript:void(0);" data-product_id='.$post['mws_product']['id'].' type="submit" class="addtocart btn btn-info btn-sm btn-rounded waves-effect waves-light" title="Add to Purchase Instance"><i class="fas fa-shopping-cart"></i></a>';

                    $nestedData['action'] =  $action ;
                    $rorder_data[] = $nestedData;
                }    
             }
                
           echo json_encode($rorder_data); exit;


       }

       else if($request->request_type == 'confirm_type'){
           $update = Purchase_orders::where(array('id'=>$request->id))->update(['status' =>2]);
            echo json_encode($update); exit;

       }

         $inputs = $request->all();
         if($request->insert_type == 'upload_csv'){
            if ($request->hasFile('uploadFile')) {
                $size = $request->file('uploadFile')->getSize();
                $extension = $request->uploadFile->getClientOriginalExtension();
                if ($extension == "csv" && $size <= 100000) {
                    $filePath = $request->file('uploadFile')->getRealPath();
                    $file = fopen($filePath, "r");
                    $escapedHeader = [];
                    $detail = [];
                    $flag = 0;

                    while ($columns = fgetcsv($file)) {
                        $columnValue = [];
                        if ($flag == 0) {
                            if (($columns[0]) != 'POID' || ($columns[1]) != 'SKU' || ($columns[2]) != 'Date_of_ship' || ($columns[3]) != 'Date_arrived'  || ($columns[4]) != 'Reorder_date' || ($columns[5]) != 'Day_rate' || ($columns[6]) != 'Days_of_supply' || ($columns[7]) != 'Blackout_days' || ($columns[8]) != 'Projected_units' || ($columns[9]) != 'Items_per_cartoons' || ($columns[10]) != 'Containers' || ($columns[11]) != 'CBM') {
                                $message = get_messages('Please upload proper file format as provide in sample!', 0);
                                Session::flash('message', $message);
                                return redirect()->route('purchaseorder.index');
                            }
                        }
                        foreach ($columns as $key => &$value) {
                            if ($flag == 0) {
                                $lowerHeader = strtolower($value);
                                $escapedItems = preg_replace("/[^a-z]/", "", $lowerHeader);
                                array_push($escapedHeader, $escapedItems);
                            } else {
                                array_push($columnValue, $value);
                            }
                        }
                        $flag++;
                        if (!empty($columnValue))
                            $detail[] = array_combine($escapedHeader, $columnValue);
                    }

                    if(!empty($detail)){
                        $uploaded_file = $request->file('uploadFile');
                        $name = 'csv_' . time() . $uploaded_file->getClientOriginalName();
                        $uploaded_file->move(public_path('assets/images/csv/purchaseorder'), $name);
                        $images[] = $name;
                        $storefolder = 'assets/images/csv/purchaseorder/csv_' . time() . $uploaded_file->getClientOriginalName();
                    }

                    foreach ($detail as $data) {
                        $product = [];
                        if(($data['poid']) <= 0){
                            $message = get_messages('poid is empty.', 0);
                            Session::flash('message', $message);
                        }else if (($data['sku']) == '') {
                            $message = get_messages('sku is empty.', 0);
                            Session::flash('message', $message);
                        }else if (($data['dateofship']) ==  '') {
                            $message = get_messages('date of ship is empty.', 0);
                            Session::flash('message', $message);
                        }elseif (date('Y-m-d',strtotime($data['dateofship'])) == '1970-01-01') {
                            $message = get_messages('Enter Valid Ship Date.', 0);
                            Session::flash('message', $message);
                        }else if (($data['datearrived']) == '') {
                            $message = get_messages('date arrived is empty.', 0);
                            Session::flash('message', $message);
                        }elseif (date('Y-m-d',strtotime($data['datearrived'])) == '1970-01-01') {
                            $message = get_messages('Enter Valid Arrived Date.', 0);
                            Session::flash('message', $message);
                        }else if (($data['reorderdate']) == '') {
                            $message = get_messages('reorder date is empty.', 0);
                            Session::flash('message', $message);
                        }elseif (date('Y-m-d',strtotime($data['reorderdate'])) == '1970-01-01') {
                            $message = get_messages('Enter Valid Reorder Date.', 0);
                            Session::flash('message', $message);
                        }else if (($data['dayrate']) == '' ) {
                            $message = get_messages('day rate is empty.', 0);
                            Session::flash('message', $message);
                        }else if (($data['daysofsupply']) <= 0) {
                            $message = get_messages('days of supply is empty.', 0);
                            Session::flash('message', $message);
                        }else if (($data['blackoutdays']) <= 0) {
                            $message = get_messages('blackout days is empty.', 0);
                            Session::flash('message', $message);
                        }else if (($data['projectedunits']) <= 0) {
                            $message = get_messages('projected unit is empty.', 0);
                            Session::flash('message', $message);
                        }else if (($data['itemspercartoons']) <= 0) {
                            $message = get_messages('items per cartoons is empty.', 0);
                            Session::flash('message', $message);
                        }else if (($data['containers']) <= 0) {
                            $message = get_messages('containers is empty.', 0);
                            Session::flash('message', $message);
                        }else if (($data['cbm']) <= 0) {
                            $message = get_messages('cbm is empty.', 0);
                            Session::flash('message', $message);
                        } else {
                            $get_product_id = Mws_product::where(array('sku'=>$data['sku'],'user_marketplace_id'=>session('MARKETPLACE_ID')))->get()->first();
                            if(!empty($get_product_id)){
                                $get_referance_id = Purchase_orders::where(array('referance_id'=>$data['poid']))->get()->first();
                                if(!empty($get_referance_id)){
                                    $purchase_order_id = $get_referance_id->id;
                                }else{
                                    $req_data = [
                                        'user_marketplace_id' => session('MARKETPLACE_ID'),
                                        'track_id' => $this->gerate_track_id(),
                                        'date_of_ship' => date('Y-m-d',strtotime($data['dateofship'])),
                                        'date_arrived' => date('Y-m-d',strtotime($data['datearrived'])),
                                        'reorder_date' => date('Y-m-d',strtotime($data['reorderdate'])),
                                        'day_rate'=>$data['dayrate'],
                                        'days_of_supply'=>$data['daysofsupply'],
                                        'blackout_days'=>$data['blackoutdays'],
                                        'status'=>1,
                                        'referance_id'=>$data['poid'],
                                    ];
                                    $datas_of = Purchase_orders::create($req_data);
                                    $userLastInsertId = $datas_of->toArray();
                                    $purchase_order_id = $userLastInsertId['id'];
                                }
                                if($purchase_order_id != '' ){
                                    $check_vendors = get_product_vendors($get_product_id->id);
                                    $check_suppliers = get_product_suppliers($get_product_id->id);
                                    $check_warehouse = get_product_warehouse($get_product_id->id);
                                    $product_data = [
                                        'purchase_orders_id' => $purchase_order_id,
                                        'product_id' => $get_product_id->id,
                                        'supplier' => !empty($check_suppliers) && $check_suppliers[0]['supplier_id'] != '' ? $check_suppliers[0]['supplier_id'] : 0,
                                        'vendor' => !empty($check_vendors) && $check_vendors[0]['vendor_id'] != '' ? $check_vendors[0]['vendor_id'] : 0,
                                        'warehouse' => !empty($check_warehouse) && $check_warehouse[0]['warehouse_id'] != '' ? $check_warehouse[0]['warehouse_id'] : 0,
                                        'projected_units'=>$data['projectedunits'],
                                        'items_per_cartoons'=>$data['itemspercartoons'],
                                        'cbm'=>$data['cbm'],
                                        'containers'=>$data['containers'],
                                    ];

                                    Purchase_order_details::create($product_data);
                                    $message = get_messages('CSV file uploaded successfully', 1);
                                    Session::flash('message', $message);

                                }else{
                                    $message = get_messages('Something went wrong', 0);
                                    Session::flash('message', $message);
                                }
                            }else{
                                $message = get_messages('Product sku is not found', 0);
                                Session::flash('message', $message);
                            }
                        }
                    }
                    return redirect()->route('purchaseorder.index');
                }
            }
             else{
                $message = get_messages('CSV file must be required', 0);
                Session::flash('message', $message);
                return redirect()->route('purchaseorder.index');
            }
        } 
         else{
            $message = get_messages('Something wrong', 0);
            Session::flash('message', $message);
            return redirect()->route('purchaseorder.index');
        }   
    }

    /**
     * download the structure of the upload purchase order data.
     * @method POST
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

     if($id == 'import') {
            $filename = 'purchasesaorder.csv';
            $headers = array(
                "Content-type" => "text/csv",
                "Content-Disposition" => "attachment; filename=" . $filename . " ",
                "Pragma" => "no-cache",
                "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                "Expires" => "0"
            );

            $columns = array('POID','SKU','Date_of_ship','Date_arrived','Reorder_date','Day_rate','Days_of_supply','Blackout_days','Projected_units','Items_per_cartoons','Containers','CBM');
            $fileputcsv = array('201','Q1-S6RF-V6XP','2020-05-24', '2020-05-28', '2020-05-23','5','2','5','2','2','2','3');

            $callback = function () use ($columns, $fileputcsv) {
                ob_clean();
                $file = fopen('php://output', 'w+');
                fputcsv($file, $columns);
                fputcsv($file, $fileputcsv);
                fclose($file);
            };
            return Response::stream($callback, 200, $headers);
        }

        $products = array();
        $warehouse = array();
        $global_marketplace = session('MARKETPLACE_ID');
        if($global_marketplace != ''){
            $warehouse = Warehouse::where(array('user_marketplace_id'=>$global_marketplace))->get();
            $products = Mws_product::where(array('user_marketplace_id'=>$global_marketplace))->get();
        }
        $purchases = Purchase_instances::findOrFail($id);
        $purchases_details = Purchase_instance_details::where(array('purchesinstances_id'=>$id))->get()->toArray();
        return view('purchase_order.confirm', [
                'purchase' => $purchases,
                'purchase_details'=>$purchases_details,
                'warehouse'=>$warehouse,
                'products'=>$products,
                'id'=>$id
            ]
        );
    }

    /**
     * Show the form for editing the purchase order view.
     * @Method GET
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $global_marketplace = session('MARKETPLACE_ID');
        $order_listing = array();
        if($global_marketplace != '') {
            $order_listing = Purchase_orders::with('purachse_order_details', 'product')->where(array('user_marketplace_id' => $global_marketplace,'id'=>$id))->get()->first();
        }
        $get_order_listing = Purchase_order_details::where(array('purchase_orders_id'=>$id))->get()->toArray();
        return view('purchase_order.view',[
            'view'=> $order_listing,
            'order_details'=>$get_order_listing
        ]);
    }

    /**
     * Update the particular purchase order
     * @method PUT
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = $request->all();
        $global_marketplace = session('MARKETPLACE_ID');
        if($id != ''){
            $orders = [
                'user_marketplace_id'=>$global_marketplace,
                'track_id'=> $this->gerate_track_id(),
                'purchesinstances_id' => $id,
                'date_of_ship' => $inputs['date_of_ship'],
                'date_arrived' => $inputs['date_arrieved'],
                'reorder_date' => $inputs['reorder_date'],
                'day_rate' => $inputs['day_rate'],
                'days_of_supply' => $inputs['days_of_supply'],
                'blackout_days' => $inputs['blakoute_days'],
                'status'=>1
            ];
            $perchase = Purchase_orders::create($orders);
            $purchase_order_id = $perchase->toArray();

            $get_all_purchase_details_data = Purchase_instance_details::where(array('purchesinstances_id'=>$id))->get()->toArray();
            if(!empty($get_all_purchase_details_data)){
                foreach($get_all_purchase_details_data as $purchases){
                    $order_details = [
                        'purchase_orders_id' =>$purchase_order_id['id'],
                        'product_id' => $purchases['product_id'],
                        'supplier' =>$purchases['supplier'],
                        'vendor' => $purchases['vendor'],
                        'warehouse' => $purchases['warehouse'],
                        'projected_units' => $purchases['projected_units'],
                        'items_per_cartoons' =>$purchases['items_per_cartoons'],
                        'containers' =>$purchases['containers'],
                        'cbm' =>$purchases['cbm'],
                        'subtotal' =>$purchases['subtotal'],
                    ];
                    $order_detail = Purchase_order_details::create($order_details);
                }
            }

            if($purchase_order_id['id'] != ''){
                $order_status['status'] = 2;
                $update = Purchase_instances::where(array('id'=>$id))->update($order_status);
            }

            if($purchase_order_id['id'] != ''){
                $message = get_messages('Purchase confirm successfully', 1);
                Session::flash('message', $message);
                return redirect()->route('purchaseinstances.index');
            }else{
                $message = get_messages('Failed to purchase confirm', 0);
                Session::flash('message', $message);
                return redirect()->route('purchaseinstances.index');
            }
        }else{
            $message = get_messages('Failed to purchase confirm', 0);
            Session::flash('message', $message);
            return redirect()->route('purchaseinstances.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    /**
     * get & check the track id for purchase order.
     * @method GET
     * @param  no-params
     * @return \Illuminate\Http\Response
     */

    public function gerate_track_id(){
        $generate_trake = rand(100,999);
        $track_id = 'PO'.$generate_trake;
        if(!empty($track_id)){
            $check_tranck_number = Purchase_orders::where(array('track_id'=>$track_id))->get()->toArray();
            if(!empty($check_tranck_number)){
                return $this->gerate_track_id();
            }else{
                return $track_id;
            }
        }
    }
}

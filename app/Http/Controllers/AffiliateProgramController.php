<?php

namespace App\Http\Controllers;

use App\Models\Users;
use App\Models\User_assigned_role;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class AffiliateProgramController extends Controller
{
    /**
     * Display a listing of the affiliate users with role wise.
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get_checks = get_access('affiliate_module','view');
        $get_user_access = get_user_check_access('affiliate_module','view');
        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }
        $id = Auth::id();
        $userRoleID = Users::with('user_roles')->whereIn('id', [Auth::id()])->first()->user_roles[0]->id;
            if ($userRoleID == 3 || $userRoleID == 4) {
                return view('affiliate.index', [
                        'id' => $id,
                        'roleId'=>$userRoleID
                    ]
                );
            }
            elseif ($userRoleID == 1 || $userRoleID == 2){
                return view('affiliate.index', [
                        'roleId'=>$userRoleID
                    ]
                );
            }
            else {
                return redirect('/index');
            }
    }

    /**
     * Show the form for invitation send to another users.
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $get_checks = get_access('affiliate_module','create');
        $get_user_access = get_user_check_access('affiliate_module','create');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }

        $user = auth()->user();
        $id = $user->id;
        $name = $user->name;

        $userDetails = Users::with(['user_assigned_role', 'user_roles'])->where(array('id' => $id))->first()->toArray();
        if (!empty($userDetails)) {   
            $resultUserDetails = array();
            return view('affiliate.invite', [
                    'users_details' => $resultUserDetails,
                    'id' => $id,
                    'name' => $name,
                ]
            );        
        }
    }

    /**
 * There are 2 function added in store method :
 *      @method POST
 *
 *  Request Types:
 *
 * 1. Invite mail : send multiple mail for invitation to make affiliate
 *      @param  multiple emails,request type
 *      @return \Illuminate\Http\Response
 *
 * * 2. Affiliate_list: list of affiliate users & it's commisions
 *      @param  request type
 *      @return \Illuminate\Http\Response
 */

    public function store(Request $request)
    {

    $input = $request->all();

    $data["user"] = Users::with(['userdetails', 'user_assigned_role', 'user_roles'])->where('id', Auth::user()->id)->first()->toArray();

    if($request->request_type == 'invite_mail'){
        if(!empty($input['emails']) && isset($input['emails'])){

            foreach ($input['emails'] as $email){
                $userData = [
                    'name' => $input['name'],
                    'email' => $email,
                ];
                $userData['link'] = $input['link'];
                $settings = get_settings();
                if(!empty($settings['email_settings']) && $settings['email_settings'] == 1) {
                    Mail::send('emailtemplate.inviteuser', $userData, function ($message) use ($userData) {
                        $message->to($userData['email'], $userData['name'])
                            ->subject('Invitation');
                    });
                }else{
                    $message = get_messages('Mail funcationlity is not working.', 0);
                    Session::flash('message', $message);
                    return redirect('affiliate_programs');
                }
            }

            $message = get_messages('invitation send successfully!',1);
            Session::flash('message', $message);
            return redirect('/affiliate_programs');
        }else{
            $request->validate([
                'email' => ['required', 'string', 'email'],
            ]);
        }
    }
        if($request->request_type == 'affiliate_list'){

        if ($data["user"]['user_roles'][0]['id'] == 1 || $data["user"]['user_roles'][0]['id'] == 2 ) {

          $data = User_assigned_role::with(['users'])->where(array('user_role_id' => '3'))->get()->toArray();



          $affiliate_data = array();
          if(!empty($data))
            {
                 foreach ($data as $key => $post)
                {
                    $action = '' ;
                    $nestedData['name'] =  $post['users']['name'];
                    $nestedData['email'] =  $post['users']['email'];
                    $nestedData['total Commission'] =  '$0';
                    $nestedData['total paid Commission'] =  '$0';
                    $nestedData['Outstanding Commission'] =  '$0';
                    $affiliate_data[] = $nestedData;
                }
             }

           echo json_encode($affiliate_data); exit;
         }
        if ($data["user"]['user_roles'][0]['id'] == 3 || $data["user"]['user_roles'][0]['id'] == 4 ) {
            $user = auth()->user();
            $id = $user->id;
            $data = Users::with(['userdetails', 'user_roles'])->where(array('refer_id' => $id))->get()->toArray();
            $affiliate_data = array();
                if(!empty($data))
                {
                    foreach ($data as $key => $post)
                    {
                        $action = '' ;
                        $nestedData['name'] =  $post['name'];
                        $nestedData['email'] =  $post['email'];
                        $nestedData['total Commission'] =  '$0';
                        $affiliate_data[] = $nestedData;
                    }
                }else{

                }
                echo json_encode($affiliate_data); exit;
            }
        }
    }
    /**
 * show the list of all childs affiliate users
 *
 * @method POST
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */

    public function show($id)
    {
        $get_checks = get_access('affiliate_module', 'view');
        $get_user_access = get_user_check_access('affiliate_module','access');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }

        $auth_Id = Auth::id();
        $data = Users::where('refer_id', $id)->whereNotIn('id', [$auth_Id])->get()->toArray();
        return view('affiliate.member-child-user', [
                'users_details' => $data
            ]
        );
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
    /**
     * check email exists or not records
     * @method POST
     * @param  string  $email
     * @return \Illuminate\Http\Response
     */

    public function check_email_exists(Request $request){
        if($request->email != ''){
            $check_email = User::where(array('email'=>$request->email))->get()->toArray();
            if(!empty($check_email)){
                $res['error'] = 1;
                $res['message'] = get_messages('Email is already exist',0);
            }else{
                $res['error'] = 0;
                $res['message'] = '';
            }
        }else{
            $res['message'] = get_messages('Something wrong',0);
            $res['error'] = 1;
        }
        return response()->json($res);
    }

    /**
     * Affiliate user register form
     * @method POST
     * @param  string  $request->all(),$refer_id
     * @return \Illuminate\Http\Response
     */
    /* -- Affiliate user register form  --*/
    public function create_user(Request $request, $refer_id=0)
    {
        if($refer_id >0 ) {
            return view('auth.register', [
                    'refer_id' => $refer_id,
                ]
            );
        }
        else{
            return view('auth.register',[
                    'role' => '4',
                ]
            );
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Vendor;
use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\Session;
use App\Models\Warehouse;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\Warehouse_wise_default_setting;
use App\Models\Warehouse_default_setting;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use App\Models\Field_list;
use App\Models\Warehouse_users;
use App\Models\Warehouse_wise_users;
use App\Models\Warehouse_contacts;
use App\Models\User_field_access;
use App\Imports\WarehouseImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Export\WarehouseExport;

class WarehousesController extends Controller
{
    /**
     * Display a listing view of the warehouse with marketplace wise
     *
     * @param no -params
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get_checks = get_access('warehouse_module', 'view');
        $get_user_access = get_user_check_access('warehouse_module', 'view');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }
        $global_marketplace = session('MARKETPLACE_ID');
        $marketplace = '';
        if ($global_marketplace != '') {
            $marketplace = $global_marketplace;
        }

        $get_all_fileds = array();
        $get_module_id = get_module_id('warehouse_module');
        if ($get_module_id > 0) {
            $get_all_fileds = Field_list::where(array('active' => '1', ['display_name', '!=', NULL], 'module_id' => $get_module_id))->orderBy('id', 'asc')->get()->toArray();
        }

        $data = Warehouse::with(['usermarketplace', 'warehouse_product'])->where(array('type' => '1'))->whereHas('usermarketplace', function ($query) {
            $query->where(array('id' => session('MARKETPLACE_ID')));
        })->get()->toArray();
//        echo "<pre />"; print_r($data);exit;
        return view('warehousesfinal.index', [
            'marketplace' => $marketplace,
            'warehouses' => $data, 'get_all_fields' => $get_all_fileds,
            'cols' => DB::table('cols')->where('user_id', auth()->id())
                ->where('table_name', 'warehouses')->pluck('cols')->first()
        ]);
//        return view('warehouses.index',[]);
    }

    /**
     * Show the form for creating a new warehouse
     *
     * @param no -params
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Warehouse::with('usermarketplace')->get()->toArray();
        $get_checks = get_access('warehouse_module', 'create');
        $get_user_access = get_user_check_access('warehouse_module', 'create');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect()->back();
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect()->back();
            }
        }
        $global_marketplace = session('MARKETPLACE_ID');
        $warehouse_users = Warehouse_users::where(array('user_marketplace_id' => $global_marketplace))->get()->toArray();
        $country = get_country_list();
        return view('warehousesfinal.create', [
            'details' => $data,
            'country' => $country,
            'warehouse_users' => $warehouse_users
        ]);
    }

    /**
     * Show the all state with country wise.
     * @method GET
     * @param  $country_id
     * @return \Illuminate\Http\Response
     */

    public function getStateList(Request $request)
    {
        $inputs = $request->all();

        $list = get_state_list($inputs['country']);
        //print_r($list); exit;
        return response()->json($list);
    }

    /**
     * Show the all city with state wise.
     * @method GET
     * @param state_id
     * @return \Illuminate\Http\Response
     */

    public function getCityList(Request $request)
    {
        $inputs = $request->all();
        $list = get_city_list($inputs['state']);
        return response()->json($list);
    }

    /**
     * There are 6 function added in store method :
     * @method POST
     *
     *  Request Types:
     *
     * 1. Get_all_state : Get all state on country wise
     * @param  $requesttype
     * @param \Illuminate\Http\Response $request
     * @param \Illuminate\Http\Response $request
     * @param \Illuminate\Http\Response $request
     * @param \Illuminate\Http\Response $request
     * @param \Illuminate\Http\Response $request
     * @return \Illuminate\Http\Response
     *
     * 2. Get_all_city: Get all city on state wise
     * @return \Illuminate\Http\Response
     *
     * 3. warehouse_list: get all warehouse with marketplace wise
     * @return \Illuminate\Http\Response
     *
     * 4. show_type_wise_data: Show type wise data
     * @return \Illuminate\Http\Response
     *
     * 5. create_form: Create & store the data in warehouse
     * @return \Illuminate\Http\Response
     *
     * 6. upload_csv: upload csv file import & insert warehouse data
     * @return \Illuminate\Http\Response
     *
     *
     */
    public function store(Request $request)
    {
        $get_checks = get_access('warehouse_module', 'create');
        $get_user_access = get_user_check_access('warehouse_module', 'create');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect()->back();
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect()->back();
            }
        }
        $res['error'] = 1;
        $res['message'] = '';


        if ($request->request_type == 'Get_all_state') {
            $inputs = $request->all();
            $list = get_state_list($inputs['country']);
            return response()->json($list);
            exit;
        }

        if ($request->request_type == 'Get_all_city') {
            $inputs = $request->all();
            $list = get_city_list($inputs['state']);
            return response()->json($list);
            exit;
        }

        if ($request->request_type == 'get_all_warehouse_data') {
            $offsetValue = $request->offsetValue;
            if (!empty($offsetValue)) {
                $offsetValue = explode('-', $offsetValue);
                $offset = $offsetValue[0];
                $limit = $request->pagination_slot;
                $output = '';
                if ($offset % 2 == 0) {
                    $offset = 0;
                } else {
                    $offset = $offset - 1;
                }
            } else {
                $offset = 0;
                $limit = $request->pagination_slot;
                $output = '';
            }
            $global_marketplace = session('MARKETPLACE_ID');

                $no_display_fileds = User_field_access::
                join('field_list','field_list.id','=','field_list_id')
                ->select(DB::raw("GROUP_CONCAT((CASE WHEN original_name IS NULL THEN CONCAT(table_name,'.',display_field) ELSE original_name END)) AS disply_fileds, GROUP_CONCAT((CASE WHEN original_name IS NULL THEN display_field ELSE original_name END)) AS display_all"))
                ->where(array('marketplace_id'=>$global_marketplace,'type'=>3))
              ->first();



          
            $unchecked_arr = explode(',', $no_display_fileds->disply_fileds);
            $total_display_fileds = explode(',',$no_display_fileds->display_all);

            $selected_fields = array();
            $join_table = array();
            $join_first = array();
            $join_last = array();

            $get_module_id = get_module_id('warehouse_module');
            if ($get_module_id > 0) {
                $get_all_fileds = Field_list::where(array('active' => '1', ['display_name', '!=', NULL], 'module_id' => $get_module_id))->orderBy('id', 'asc')->get()->toArray();

                if (!empty($get_all_fileds)) {
                    $selected_fields[] = 'warehouses.*';
                    //$get_fields[] = 'id';
                    foreach ($get_all_fileds as $key => $val) {
                        $data_of = $val['original_name'] != '' ? $val['original_name'] : $val['display_field'];
                        $display_name[$data_of] = $val['original_name'] != '' ? $val['original_name'] : $val['display_field'];
                        if ($val['referance_table'] != '' && $val['referance_table_field'] != '') {
                            $selected_fields[] = $val['referance_table'] . '.' . $val['referance_table_field'];
                            //$get_fileds[] = $val['referance_table_field'];
                            $join_table[] = $val['referance_table'];
                            $join_first[] = 'warehouses.' . $val['original_name'];
                            $join_last[] = $val['referance_table'] . "." . $val['field_type'];
                        } else if ($val['table_name'] != '' && $val['display_field'] != '' && $val['join_with'] != '') {
                            $selected_fields[] = $val['table_name'] . '.' . $val['display_field'];
                            //$get_fields[] = $val['display_field'];
                            $join_table[] = $val['table_name'];
                            $join_first[] = 'warehouses.id';
                            $join_last[] = $val['table_name'] . "." . $val['join_with'];
                        } else {
                            $selected_fields[] = $val['original_name'];
                            //$get_fields[] = $val['original_name'];
                        }
                    }
                }

               

                $whereArray = [
                    "user_marketplace_id" => $global_marketplace
                ];
                if ($request->get_country) {
                    $whereArray = [
                        "user_marketplace_id" => $global_marketplace,
                        "country_id" => $request->get_country
                    ];
                }
                if($request->search != ''){
                    if($request->search == 'self'){
                        $whereArray[] = array([
                            'type'=> 1
                        ]);
                    }else if($request->search == '3pl'){
                        $whereArray[] = array([
                            'type'=>2
                        ]);
                    }else{
                        $whereArray[] = [
                            'warehouse_name', 'LIKE', "%{$request->search}%"
                        ];
                    }
                }


                $join_table = array_unique($join_table);
                $selected_raw = array_diff($selected_fields,$unchecked_arr);

                $selected_field = "'" . implode("', '", $selected_fields) . "'";

              

                $userRole = Warehouse::select($selected_raw)->where($whereArray);
                if (!empty($join_table)) {
                    foreach ($join_table as $key => $jon) {
                        $userRole->leftjoin("$jon", "$join_first[$key]", '=', "$join_last[$key]");
                    }
                }
                if($offset > 0)
                    $listing_data = $userRole->offset($offset)->limit($limit)->groupBy('id')->orderBy('id', 'desc')->get()->toArray();
                else
                    $listing_data = $userRole->limit($limit)->groupBy('id')->orderBy('id', 'desc')->get()->toArray();



                $listing_data_count = Warehouse::with(['usermarketplace', 'warehouse_product'])->where($whereArray)->count();

//                $listing_data_count = $userRole->groupBy('id')->orderBy('id', 'desc')->get()->count();
            }
            $total_count = 0;
            //$shipping_agents = ShippingAgent::where(array('user_marketplace_id' => $global_marketplace,'country_id'=>$country))->offset($offset)->limit($limit)->get()->toArray();
            if (!empty($listing_data)) {
                $output = '';
                foreach ($listing_data as $k => $data) {
                    if ($data['type'] == 1) {
                        $warehouse_contact = Warehouse_wise_users::where(array('warehouse_id' => $data['id']))->first();
                    } else {
                        $warehouse_contact = Warehouse_contacts::where(array('warehouse_id' => $data['id']))->first();
                    }
                    $warehouse_setting = Warehouse_wise_default_setting::where(array('warehouse_id' => $data['id']))->first();
                    if (empty($warehouse_contact) || empty($warehouse_setting)) {
                        $check_return = 0;
                    } else {
                        $check_return = 1;
                    }
                    if ($request->toggle_b == 1) {
                        if ($check_return == 0) {
                            $i = 1;
                            $counter = $k + 1;
                            $newtext = 0;
                            $count = count($display_name);
                            //$vendors_name = @$data['vendor_contact'][0]['first_name'] ? @$data['vendor_contact'][0]['first_name'] : "" . " " . @$data['vendor_contact'][0]['last_name'] ? @$data['vendor_contact'][0]['last_name'] : "";
                            $output .= '<tr id="' . $data['id'] . '">';
                            $output .= '<td class="p-2">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input row-checkbox" name="delete_all" data-val="' . $data['id'] . '" value="' . $data['id'] . '" id="customCheck' . $counter . '">
                                        <label class="custom-control-label" for="customCheck' . $counter . '">&nbsp;</label>
                                    </div>
                                </td>
                                 <td class="p-2">';
                            if ($check_return == 0) {
                                $output .= '<i class="fas fa-exclamation-triangle" style="color:#F1B44C;" data-toggle="tooltip" title="Missing required settings"></i>';
                            }
                            $output .= '</td>';
                            $newtext = 0;
                            $count = count($display_name);
                            foreach ($display_name as $key => $dispay) {
                                if(!in_array($dispay,$total_display_fileds)){
                                $t = $i + $newtext;
                                $newtext++;
                                if ($dispay == 'type') {
                                    if ($data[$key] == '1') {
                                        $dats = 'Self';
                                    } else {
                                        $dats = "3PL";
                                    }
                                } else if ($dispay == 'country_id') {
                                    $dats = get_country_fullname($data[$key]);
                                } else if ($dispay == 'state') {
                                    $dats = get_state_fullname($data['country_id'], $data[$key]);
                                } else if ($dispay == 'product_id') {
                                    $dats = get_vendors_total_product($data['id']);
                                } else if ($dispay == 'first_name') {
                                    $option = '';
                                    if ($data['type'] == 1) {
                                        $contact = Warehouse_wise_users::with(['warehouse_user'])->where(array('warehouse_id' => $data['id']))->get()->toArray();
                                        if (!empty($contact)) {
                                            $option .= ' <select name="warehouse_contact_' . $data['id'] . '" id="warehouse_contact_' . $data['id'] . '" class="form-control border-0 pl-0 bg-transparent">';
                                            foreach ($contact as $opt) {
                                                $option .= '<option value="">' . $opt['warehouse_user']['full_name'] . '</option>';
                                            }
                                            $option .= '</select>';
                                        }
                                    } else {
                                        $contact = Warehouse_contacts::where(array('warehouse_id' => $data['id']))->orderBy("primary","asc")->get()->toArray();
                                        if (!empty($contact)) {
                                            $option .= ' <select name="warehouse_contact_' . $data['id'] . '" id="warehouse_contact_' . $data['id'] . '" class="form-control border-0 pl-0 bg-transparent" onchange="changeContact(' . $data['id'] . ')">';
                                            foreach ($contact as $opt) {
                                                $option .= '<option value="' . $opt['id'] . '">' . $opt['first_name'] . '</option>';
                                            }
                                            $option .= '</select>';
                                        }
                                    }
                                    $dats = $option;

                                } else if($dispay == 'email'){
                                    if ($data['type'] != 1) {
                                        $dats = $data[$key];
                                    }else{
                                        $dats = '';
                                    }
                                } else if(@$dispay == 'mobile'){
                                    if($data['type'] != 1){
                                        $dats = $data[$key];
                                    }else{
                                        $dats = '';
                                    }
                                } else {
                                    $dats = $data[$key];
                                }
                                $output .= '<td class="col' . $t . '">' . ucfirst(strtolower($dats)) . '</td>';
                            }
                            }
                            $output .= '<td>
                                   <a href="' . url('/warehouses/' . $data['id'] . '/edit') . '" class="mr-3 text-secondary row-hover-action" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="mdi mdi-square-edit-outline font-size-18"></i></a>
                                    <a href="javascript:void(0);" class="text-secondary row-hover-action delete_single" data-id="' . $data['id'] . '" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="mdi mdi-trash-can font-size-18"></i></a>
                                </td>';
                            $output .= '</tr>';
                            $total_count++;
                        }
                    } else {
                        $i = 1;
                        $counter = $k + 1;
                        $newtext = 0;
                        $count = count($display_name);
                        //$vendors_name = @$data['vendor_contact'][0]['first_name'] ? @$data['vendor_contact'][0]['first_name'] : "" . " " . @$data['vendor_contact'][0]['last_name'] ? @$data['vendor_contact'][0]['last_name'] : "";
                        $output .= '<tr id="' . $data['id'] . '">';
                        $output .= '<td class="p-2">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input row-checkbox" name="delete_all" data-val="' . $data['id'] . '" value="' . $data['id'] . '" id="customCheck' . $counter . '">
                                        <label class="custom-control-label" for="customCheck' . $counter . '">&nbsp;</label>
                                    </div>
                                </td>
                                 <td class="p-2">';
                        if ($check_return == 0) {
                            $output .= '<i class="fas fa-exclamation-triangle" style="color:#F1B44C;" data-toggle="tooltip" title="Missing required settings"></i>';
                        }
                        $output .= '</td>';
                        $newtext = 0;
                        $count = count($display_name);
                        foreach ($display_name as $key => $dispay) {
                            if(!in_array($dispay,$total_display_fileds)){
                            //  print_r($display_name);
                            $country_code = '';
                            $t = $i + $newtext;
                            $newtext++;
                            if ($dispay == 'type') {
                                if ($data[$key] == '1') {
                                    $dats = 'Self';
                                } else {
                                    $dats = "3PL";
                                }
                            } else if ($dispay == 'country_id') {
                                $dats = get_country_fullname($data[$key]);
                            } else if ($dispay == 'state') {
                                $dats = get_state_fullname($data['country_id'], $data[$key]);
                            } else if ($dispay == 'product_id') {
                                $dats = get_vendors_total_product($data['id']);
                            } else if ($dispay == 'first_name') {
                                $option = '';
                                if ($data['type'] == 1) {
                                    $contact = Warehouse_wise_users::with(['warehouse_user'])->where(array('warehouse_id' => $data['id']))->get()->toArray();
                                    if (!empty($contact)) {
                                        $option .= ' <select name="warehouse_contact_' . $data['id'] . '" id="warehouse_contact_' . $data['id'] . '" class="form-control border-0 pl-0 bg-transparent">';
                                        foreach ($contact as $opt) {
                                            $option .= '<option value="">' . $opt['warehouse_user']['full_name'] . '</option>';
                                        }
                                        $option .= '</select>';
                                    }
                                } else {
                                    $contact = Warehouse_contacts::where(array('warehouse_id' => $data['id']))->orderBy("primary","desc")->get()->toArray();
                                    if (!empty($contact)) {
                                        $option .= ' <select name="warehouse_contact_' . $data['id'] . '" id="warehouse_contact_' . $data['id'] . '" class="form-control border-0 pl-0 bg-transparent" onchange="changeContact(' . $data['id'] . ')">';
                                        foreach ($contact as $opt) {
                                            $option .= '<option value="' . $opt['id'] . '">' . $opt['first_name'] . '</option>';
                                        }
                                        $option .= '</select>';
                                    }
                                }
                                $dats = $option;

                            } else if($dispay == 'email'){
                                if ($data['type'] != 1) {
                                    $dats = $data[$key];
                                }else{
                                    $dats = '';
                                }
                            } else if(@$dispay == 'mobile'){
                                if($data['type'] != 1){
                                    $dats = $data[$key];
                                }else{
                                    $dats = '';
                                }
                            } else {
                                $dats = $data[$key];
                            }
                            $output .= '<td class="col' . $t . '">' . ucfirst(strtolower($dats)) . '</td>';
                        }
                        }
                        $output .= '<td>
                                   <a href="' . url('/warehouses/' . $data['id'] . '/edit') . '" class="mr-3 text-secondary row-hover-action" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="mdi mdi-square-edit-outline font-size-18"></i></a>
                                    <a href="javascript:void(0);" class="text-secondary row-hover-action delete_single" data-id="' . $data['id'] . '" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="mdi mdi-trash-can font-size-18"></i></a>
                                </td>';
                        $output .= '</tr>';
                        $total_count++;
                    }
                }
                if ($request->toggle_b == 0) {
                    if ($listing_data_count > $limit) {
                        $output_first = '<input type="hidden" value="0" class="checkdeleteall"><input type="hidden" value="' . $listing_data_count . '" class="total_listing_count"><tr class="selected-row-number" style="display:none;"><td colspan="12" class="text-center">Selected <span id="number-of-selected"></span> out of ' . $listing_data_count . '. <a class="text-primary select-all" id="select-all-link" style="cursor: pointer;">Select All</a></td></tr>';
                    } else {
                        $output_first = '<input type="hidden" value="0" class="checkdeleteall"><input type="hidden" value="' . $listing_data_count . '" class="total_listing_count"><tr class="selected-row-number" style="display:none;"><td colspan="12" class="text-center">Selected <span id="number-of-selected"></span> out of ' . $listing_data_count . '. </td></tr>';
                    }
                } else {
                    if ($listing_data_count > $limit) {
                        $output_first = '<input type="hidden" value="0" class="checkdeleteall"><input type="hidden" value="' . $total_count . '" class="total_listing_count"><tr class="selected-row-number" style="display:none;"><td colspan="12" class="text-center">Selected <span id="number-of-selected"></span> out of ' . $total_count . '. <a class="text-primary select-all" id="select-all-link" style="cursor: pointer;">Select All</a></td></tr>';
                    } else {
                        $output_first = '<input type="hidden" value="0" class="checkdeleteall"><input type="hidden" value="' . $total_count . '" class="total_listing_count"><tr class="selected-row-number" style="display:none;"><td colspan="12" class="text-center">Selected <span id="number-of-selected"></span> out of ' . $total_count . '. </td></tr>';
                    }
                }
                $output_final = $output_first . $output;
            } else {
                $output_final = '<td align="center" colspan="10" class="alert alert-danger">No record found. </td>';
            }
            $res['output'] = $output_final;
            $res['total_count'] = $total_count;
            return response()->json($res);
        }
        if($request->request_type == 'fileds_manage'){
            $global_marketplace = session('MARKETPLACE_ID');
            $total_unchecked_fileds = $request->checked_arr;
            User_field_access::where(array('marketplace_id'=>$global_marketplace,'type'=>3))->delete();
            if(!empty($total_unchecked_fileds)){
                foreach($total_unchecked_fileds as $val){
                    $req_data = [
                        'field_list_id'=>$val,
                        'marketplace_id'=>$global_marketplace,
                        'type'=>3
                    ];
                    User_field_access::create($req_data);
                }
            }
            $res['success'] = 1;
            return response()->json($res);
        }
        if ($request->request_type == 'get_pagi_country') {
            $global_marketplace = session('MARKETPLACE_ID');
            $pagination_left = '';
            $pagination_right = '';
            $pagination = array();
            $success = 0;
            $pagination_slot = $request->pagination_slot;


            if ($global_marketplace != '') {
//                $get_country = ($request->get_country ? $request->get_country : 'US');
//                $data_count = Warehouse::with(['usermarketplace', 'warehouse_product'])->where(array('country_id' => $get_country))->whereHas('usermarketplace', function ($query) {
//                    $query->where(array('id' => session('MARKETPLACE_ID')));
//                })->count();
//
                $whereArray = [
                    "user_marketplace_id" => $global_marketplace
                ];
                if ($request->get_country) {
                    $whereArray = [
                        "user_marketplace_id" => $global_marketplace,
                        "country_id" => $request->get_country
                    ];
                }

                if($request->search != ''){
                    $whereArray[] = [
                        'warehouse_name', 'LIKE', "%{$request->search}%"
                    ];
                }


                $data_count = Warehouse::with(['usermarketplace', 'warehouse_product'])->where($whereArray)->count();
                $keyj = 1;
                if ($data_count > 0) {
                    for ($i = 1; $i <= $data_count; $i = $i + $pagination_slot) {
                        //for ($i = 1; $i <= $data_count; $i = $i + (env('PAGINATION'))) {
                        if ($i == 1) {
                            $newi = $i;
                        } else {
                            $newi = $i - 1;
                        }
                        $n = $i + ($pagination_slot - 1);
                        //$pagination[$keyj]= $newi.'-'.$n;
                        $pagination[$keyj] = ($newi) . '-' . $n;
                        $keyj++;
                    }
                } else {
                    $newi = 1;
                    $n = $newi + ($pagination_slot - 1);
                    $pagination[$newi] = ($newi) . '-' . $n;

                }
                $pagi = ($request->pagecount ? $request->pagecount : '');
                $success = 1;
                //$pagination_left = get_pagination_view($pagination);
                $pagination_right = get_pagination_right_view($pagination, $data_count,$pagination_slot, $pagi);
            }
            $res['success'] = $success;
            //$res['select_pagi'] = $pagination_left;
            $res['paginations'] = $pagination_right;
            $res['count'] = $data_count;
            return response()->json($res);
            exit;
            // echo json_encode($res); exit;
        }

        if ($request->request_type == 'warehouse_list') {

            $data = Warehouse::with('usermarketplace')->where(array('type' => '1'))->whereHas('usermarketplace', function ($query) {
                $query->where(array('id' => session('MARKETPLACE_ID')));
            })->get()->toArray();

            $warehouse_data = array();
            if (!empty($data)) {
                foreach ($data as $key => $post) {
                    $cnname = "";
                    $state_name = "";
                    $city_name = "";
                    if (isset($post['country_id']) && $post['country_id'] != "") {
                        $country_list = get_country_list();
                        $cnname = array_search($post['country_id'], $country_list);
                    }
                    if (isset($post['state']) && $post['state'] != "" && (isset($post['country_id'])) && $post['country_id'] != "") {
                        $state_name = get_state_fullname($post['country_id'], $post['state']);
                    }

                    if (isset($post['city']) && $post['city'] != "" && (isset($post['state'])) && $post['state'] != "" && (isset($post['country_id'])) && $post['country_id'] != "") {
                        $city_name = get_city_fullname($post['country_id'], $post['state'], $post['city']);
                    }
                    $action = '';
                    $nestedData['warehouse_name'] = '<a href="' . route('warehouses.show', $post['id']) . '">' . $post['warehouse_name'] . '</a>';
                    $nestedData['address_line_1'] = $post['address_line_1'];
                    $nestedData['address_line_2'] = $post['address_line_2'];
                    $nestedData['city'] = $city_name;
                    $nestedData['state'] = $state_name;
                    $nestedData['zipcode'] = $post['zipcode'];
                    $nestedData['country'] = '';
                    $action .= '<a href="' . route('warehouses.edit', $post['id']) . '"  class=" btn btn-info btn-sm btn-rounded waves-effect waves-light" role="button"><i class="fas fa-edit" ></i></a>';
                    $action .= '<button id="button" type="submit" class=" btn btn-danger btn-sm btn-rounded waves-effect  waves-light sa-remove" data-id=' . $post['id'] . '><i class="fas fa-trash-alt"></i></button>';
                    $nestedData['action'] = $action;
                    $warehouse_data[] = $nestedData;
                }
            }

            echo json_encode($warehouse_data);
            exit;


        }
        if ($request->insert_type == 'show_type_wise_data') {
            $data = Warehouse::with('usermarketplace')->where(array('type' => $request->id))->whereHas('usermarketplace', function ($query) {
                $query->where(array('id' => session('MARKETPLACE_ID')));
            })->orderBy('id', 'DESC')->get()->toArray();
            $view = View::make('warehouses.get_warehouse_type_data', [
                'warhouse_details' => $data,
                'type' => $request->id,
            ]);
            $html = $view->render();
            $res['error'] = 0;
            $res['view'] = $html;
            return response()->json($res);
        }

        if ($request->insert_type == 'insert_update_basic_details') {
         
            $req_data = [
                'type' => $request->warehouse_type,
                'warehouse_name' => $request->Warehouse_name,
                'user_marketplace_id' => $request->marketplace_id,
                'country_id' => $request->countryId,
                'address_line_1' => $request->address_line1,
                'address_line_2' => $request->address_line2,
                'zipcode' => $request->postal_code,
                'state' => $request->stateId
            ];
            if ($request->warehouse_id == '') {
                $datas_of = Warehouse::create($req_data);
                $userLastInsertId = $datas_of->toArray();
                if ($userLastInsertId != '') {
                    $res['warehouse_id'] = $userLastInsertId['id'];
                }
                $message = 'Warehouse created successfully';
                $res['error'] = 0;
                $res['message'] = $message;
            } else {
                $warhouse = Warehouse::findOrFail($request->warehouse_id);
                $warhouse->update($req_data);
                $message = 'Warehouse updated successfully';
                $res['warehouse_id'] = $request->warehouse_id;
                $res['error'] = 0;
                $res['message'] = $message;
            }
            return response()->json($res);
        }

        if ($request->insert_type == 'insert_update_users') {
            $user_manager = $request->user_manager;
            if (!empty($user_manager)) {
                Warehouse_wise_users::where(array('warehouse_id' => $request->warehouse_id))->delete();
                foreach ($user_manager as $val) {
                    $req_data = [
                        'warehouse_id' => $request->warehouse_id,
                        'warehouse_user_id' => $val,
                    ];
                    Warehouse_wise_users::create($req_data);
                }
                $message = 'Warehouses users created successfully';
                $res['error'] = 0;
                $res['message'] = $message;
            } else {
                $message = 'Warehouses users updated successfully';
                $res['error'] = 0;
                $res['message'] = $message;
            }
            return response()->json($res);

        }

        if ($request->insert_type == 'insert_contacts') {
            $primary = $request->primary;
            $first_name = $request->first_names;
            $last_name = $request->last_names;
            $emails = $request->emails;
            $title = $request->titles;
            $mobile = $request->phone_numbers;
            $res['error'] = 0;
            $res['message'] = '';
            $warehouse_contact_id = $request->warehouse_contact_id;
            if (!empty($first_name)) {
            // Warehouse_contacts::where(array('warehouse_id' => $request->warehouse_id))->delete();
                foreach ($first_name as $key => $val) {
                    if ($val != '' && $last_name[$key] != '' && $emails[$key] != '') {
                        if(is_array($primary) && array_search("1",$primary)){
                            $primary = $primary[$key];
                        }else{
                            // $check_email = Vendor_contact::where(array('email' => $emails[$key]))->first();
                            $check_primary = Warehouse_contacts::where(array('primary' => 1, 'warehouse_id' => $request->warehouse_id))->first();
                            if (!empty($check_primary)) {
                                $primary = 0;
                            } else {
                                $primary = 1;
                            }
                        }
                        $check_email = Warehouse_contacts::where(array('email' => $emails[$key],'warehouse_id' => $request->warehouse_id))->first();
                        if(empty($check_email)){
                            $req_data = [
                                'warehouse_id' => $request->warehouse_id,
                                'first_name' => $val,
                                'last_name' => $last_name[$key],
                                'title' => $title[$key],
                                'email' => $emails[$key],
                                'mobile' => $mobile[$key],
                                'primary' => $primary
                            ];
                            if ($warehouse_contact_id[$key] == '') {
                                $datas_of = Warehouse_contacts::create($req_data);
                                $userLastInsertId = $datas_of->toArray();
                                if ($userLastInsertId != '') {
                                    $res['warehouse_contact_id'] = $userLastInsertId['id'];
                                }
                                $message = 'warehouse contact created successfully';
                                $res['error'] = 0;
                                $res['message'] = $message;
                            } else {
                                $warehouseContact = Warehouse_contacts::where('id', $warehouse_contact_id[$key])->get()->first();
                                $warehouseContact->update($req_data);
                                $message = 'warehouse contact updated successfully';
                                $res['warehouse_contact_id'] = $warehouse_contact_id[$key];
                                $res['error'] = 0;
                                $res['message'] = $message;
                            }
                        }

                        /*$datas_of = Warehouse_contacts::create($req_data);
                        $message = get_messages('warehouse contact created successfully', 1);
                        //$res['vendors_contact_id'] = '';
                        $res['error'] = 0;
                        $res['message'] = $message;*/
                        // }
                    }
                }
            }
            return response()->json($res);
        }

        if ($request->insert_type == 'insert_update_setting') {
            $req_data = [
                'warehouse_id' => $request->warehouse_id,
                'lead_time' => $request->lead_time,
            ];
            $check_data = Warehouse_wise_default_setting::where(array('warehouse_id' => $request->warehouse_id))->first();
            if (empty($check_data)) {
                $datas_of = Warehouse_wise_default_setting::create($req_data);
                $message = 'Warehouse default setting created successfully';
                $res['error'] = 0;
                $res['message'] = $message;
            } else {
                $datas_of = Warehouse_wise_default_setting::where(array('warehouse_id' => $request->warehouse_id))->update($req_data);
                $message = 'Warehouse default setting updated successfully';
                $res['warehouse_id'] = $request->warehouse_id;
                $res['error'] = 0;
                $res['message'] = $message;
            }
            return response()->json($res);
        }

        if ($request->insert_type == 'delete_multiple_records') {
            Warehouse::whereIn('id', explode(",", $request->delete_array))->delete();
            return json_encode(array('statusCode' => 200));
        }

        if ($request->insert_type == 'create_form') {

            $inputs = $request->all();
            $email = array_filter([$request->primary_email]);

            if (!empty($email)) {
                $emailList = implode(',', $email);
                $check_emails = Warehouse::where('primary_email', [$emailList])->get()->toArray();

                if (!empty($check_emails)) {
                    $message = get_messages('Email must be unique!', 0);
                    Session::flash('message', $message);
                    return redirect()->route('warehouses.create');
                }
            }

            $req_data = [
                'type' => $request->type,
                'warehouse_name' => $request->warehouse_name,
                'user_marketplace_id' => $request->marketplace_id,
                'country_id' => $request->country,
                'address_line_1' => $request->address_line_1,
                'address_line_2' => $request->address_line_2,
                'city' => $request->city,
                'state' => $request->state,
                'zipcode' => $request->zipcode,
                'primary_first_name' => $request->primary_first_name,
                'primary_last_name' => $request->primary_last_name,
                'primary_email' => $request->primary_email,
            ];

            $datas_of = Warehouse::create($req_data);
            $userLastInsertId = $datas_of->toArray();
            if ($userLastInsertId != '') {
                $get_default_warehouse_setting = get_default_warehouse_setting();
                if (!empty($get_default_warehouse_setting)) {
                    $settings = [
                        'warehouse_id' => $userLastInsertId['id'],
                        'lead_time' => $get_default_warehouse_setting->lead_time,
                    ];
                    $data_settings = Warehouse_wise_default_setting::create($settings);
                }
                $message = get_messages('Warhouse  created successfully', 1);
                Session::flash('message', $message);
                return redirect()->route('warehouses.index');
            }

        } else if ($request->insert_type == 'upload_csv') {
            if ($request->hasFile('uploadFile')) {
                $size = $request->file('uploadFile')->getSize();
                $extension = $request->uploadFile->getClientOriginalExtension();
                if ($extension == "csv" && $size <= 100000) {
                    $filePath = $request->file('uploadFile')->getRealPath();
                    $file = fopen($filePath, "r");
                    $escapedHeader = [];
                    $detail = [];
                    $flag = 0;

                    while ($columns = fgetcsv($file)) {
                        $columnValue = [];
                        if ($flag == 0) {
                            if (trim($columns[0]) != 'Type' || trim($columns[1]) != 'Warehouse Name' || trim($columns[2]) != 'Firstname' || trim($columns[3]) != 'Lastname' || trim($columns[4]) != 'Email' || trim($columns[5]) != 'Address line 1' || trim($columns[6]) != 'Address line 2 [Optional]' || trim($columns[7]) != 'City' || trim($columns[8]) != 'State' || trim($columns[9]) != 'Zipcode' || trim($columns[10]) != 'Country') {
                                $message = get_messages('Please upload proper file format as provide in sample!', 0);
                                Session::flash('message', $message);
                                return redirect()->route('warehouses.index');
                            }
                        }
                        foreach ($columns as $key => &$value) {
                            if ($flag == 0) {
                                $lowerHeader = strtolower($value);
                                $escapedItems = preg_replace("/[^a-z]/", "", $lowerHeader);
                                array_push($escapedHeader, $escapedItems);
                            } else {
                                array_push($columnValue, $value);
                            }
                        }
                        $flag++;
                        if (!empty($columnValue))
                            $detail[] = array_combine($escapedHeader, $columnValue);
                    }

                    if (!empty($detail)) {
                        $uploaded_file = $request->file('uploadFile');
                        $name = 'csv_' . time() . $uploaded_file->getClientOriginalName();
                        $uploaded_file->move(public_path('assets/images/csv/warehouses'), $name);
                        $images[] = $name;
                        $storefolder = 'assets/images/csv/warehouses/csv_' . time() . $uploaded_file->getClientOriginalName();
                    }
                    $i = 0;
                    foreach ($detail as $data) {
                        $product = [];
                        $zipcode = $data['zipcode'];
                        if (strlen($data['type']) <= 0) {
                            $message = get_messages('type is empty.', 0);
                            Session::flash('message', $message);
                        } else if (strlen($data['warehousename']) <= 0) {
                            $message = get_messages('warehouse name is empty.', 0);
                            Session::flash('message', $message);
                        } else if (strlen($data['firstname']) <= 0) {
                            $message = get_messages('firstname is empty.', 0);
                            Session::flash('message', $message);
                        } else if (strlen($data['lastname']) <= 0) {
                            $message = get_messages('lastname is empty.', 0);
                            Session::flash('message', $message);
                        } else if (strlen($data['email']) <= 0) {
                            $message = get_messages('email is empty.', 0);
                            Session::flash('message', $message);
                        } else if (strlen($data['addressline']) <= 0) {
                            $message = get_messages('address is empty.', 0);
                            Session::flash('message', $message);
                        } else if (strlen($data['city']) <= 0) {
                            $message = get_messages('city is empty.', 0);
                            Session::flash('message', $message);
                        } else if (strlen($data['state']) <= 0) {
                            $message = get_messages('state is invalid', 0);
                            Session::flash('message', $message);
                        } else if (strlen($data['zipcode']) <= 0) {
                            $message = get_messages('zipcode name is empty', 0);
                            Session::flash('message', $message);
                        } else if (strlen($data['zipcode']) != 0 && !is_numeric($zipcode)) {
                            $message4 = 'only number allowed for zipcode!';
                            Session::flash('message4', $message4);
                        } else if (strlen($data['country']) <= 0) {
                            $message = get_messages('country code is empty', 0);
                            Session::flash('message', $message);
                        } else {
                            $global_marketplace = session('MARKETPLACE_ID');
                            if (strtolower($data['type']) == 'self' || strtolower($data['type']) == 'third party') {
                                $email_check = Warehouse::where(array('primary_email' => $data['email']))->get()->toArray();
                                if (empty($email_check)) {
                                    $req_data = [
                                        'user_marketplace_id' => $global_marketplace,
                                        'warehouse_name' => $data['warehousename'],
                                        'type' => $data['type'] == 'Self' ? 1 : 2,
                                        'primary_first_name' => $data['firstname'],
                                        'primary_last_name' => $data['lastname'],
                                        'primary_email' => $data['email'],
                                        'address_line_1' => $data['addressline'],
                                        'address_line_2' => $data['addresslineoptional'],
                                        'city' => $data['city'],
                                        'state' => $data['state'],
                                        'zipcode' => $data['zipcode'],
                                        'country_id' => $data['country']
                                    ];
                                    $datas_of = Warehouse::create($req_data);
                                    $userLastInsertId = $datas_of->toArray();

                                    if ($userLastInsertId != '') {
                                        $get_warehouse_setting = get_default_warehouse_setting();
                                        if (!empty($get_warehouse_setting)) {
                                            $settings = [
                                                'warehouse_id' => $userLastInsertId['id'],
                                                'lead_time' => $get_warehouse_setting->lead_time,
                                            ];
                                            $data_settings = Warehouse_wise_default_setting::create($settings);
                                        }
                                    }
                                    $message2 = "csv uploaded successfully !";
                                    Session::flash('message2', $message2);
                                } else {
                                    $i++;
                                    $message3 = $i . ' email already exists';
                                    Session::flash('message3', $message3);
                                }
                            } else {
                                $message = get_messages($data['type'] . ' type is not valid', 0);
                                Session::flash('message', $message);
                                return redirect()->route('warehouses.index');
                            }
                        }
                    }
                    return redirect()->route('warehouses.index');
                }
            } else {
                $message = get_messages('CSV file must be required', 0);
                Session::flash('message', $message);
                return redirect()->route('warehouses.index');
            }
        } else {
            $message = get_messages('Something wrong', 0);
            Session::flash('message', $message);
            return redirect()->route('warehouses.index');
        }

        //}
    }

    /**
     * if id is not black then download structure of upload the warehouse otherwise warehouse view details show
     *
     * @method POST
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        if ($id == 'import') {

            $import = new WarehouseExport();
            return Excel::download($import, 'warehouse.xlsx');

            // $file_name = 'warehouse.xlsx';
            // $file = public_path().'/sheets/'.$file_name;
            // //$file= public_path(). "/download/info.pdf";

            // $headers = array(
            //         'Content-Type: application/csv',
            //         );

            // return Response::download($file, $file_name, $headers);
        } else {
            $warehouse_setting = Warehouse_wise_default_setting::where('warehouse_id', $id)->first();
            if (empty($warehouse_setting)) {
                $warehouse_setting = Warehouse_default_setting::first();
            }
            return view('warehouses.setting', ['id' => $id, 'warehouse_setting' => $warehouse_setting]);
        }

    }

    /**
     * Show the form for editing the specified warehouse
     * @method GET
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $global_marketplace = session('MARKETPLACE_ID');
        $get_checks = get_access('warehouse_module', 'edit');
        $get_user_access = get_user_check_access('warehouse_module', 'edit');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect()->back();
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect()->back();
            }
        }
        $data = Warehouse::with('usermarketplace')->where('id', $id)->get()->first();
        $warehouse_default = Warehouse_wise_default_setting::where(array('warehouse_id' => $id))->first();
        $country = get_country_list();
        $warehouse_contact = Warehouse_contacts::where(array('warehouse_id' => $id))->get()->toArray();
        $cid = "";
        $state_id = "";
        if (isset($data->country_id) && $data->country_id != "") {
            $cid = $data->country_id;
        }
        if (isset($data->state) && $data->state != "") {
            $state_id = $data->state;
        }
        $state_list = get_state_list($cid);
        $city_list = get_city_list($state_id);
        $warehouse_users = Warehouse_users::where(array('user_marketplace_id' => $global_marketplace))->get()->toArray();
        $warehouse_wise_users = Warehouse_wise_users::with(['warehouse_user'])->where(array('warehouse_id' => $id))->get()->toArray();
        return view('warehousesfinal.edit-warehouse', [
            'warhouse_details' => $data,
            'country' => $country,
            'state_list' => $state_list,
            'city_list' => $city_list,
            'setting' => $warehouse_default,
            'warehouse_contact' => $warehouse_contact,
            'warehouse_users' => $warehouse_users,
            'warehouse_wise_users' => $warehouse_wise_users

        ]);
    }

    /**
     *
     * There are 2 function added in update method :
     * @method PUT
     *
     *  Request Types:
     *
     * 1. edit_form : Update the specified warehouse in storage
     * @param \Illuminate\Http\Response $request
     * @param \Illuminate\Http\Response $request
     * @return \Illuminate\Http\Response
     *
     * 2. setting_form: update the default warehouse setting
     * @return \Illuminate\Http\Response
     *
     */
    public function update(Request $request, $id)
    {
        $get_checks = get_access('warehouse_module', 'edit');
        $get_user_access = get_user_check_access('warehouse_module', 'edit');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect()->back();
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect()->back();
            }
        }

        if ($request->insert_type == 'create_form') {
            $email = $request->primary_email;

            if ($email != "") {
                $check_emails = Warehouse::where('primary_email', $email)->where('id', "!=", $id)->count();
                if ($check_emails != "0") {
                    $message = get_messages('Email already exists!', 0);
                    Session::flash('message', $message);
                    return redirect()->back();
                }
            }

            if ($request->type == 1) {
                $first_name = '';
                $last_name = '';
                $user_email = '';
            } else {
                $first_name = $request->primary_first_name;
                $last_name = $request->primary_last_name;
                $user_email = $request->primary_email;
            }

            $req_data = [
                'type' => $request->type,
                'warehouse_name' => $request->warehouse_name,
                'user_marketplace_id' => $request->marketplace_id,
                'country_id' => $request->country,
                'address_line_1' => $request->address_line_1,
                'address_line_2' => $request->address_line_2,
                'city' => $request->city,
                'state' => $request->state,
                'zipcode' => $request->zipcode,
                'primary_first_name' => $first_name,
                'primary_last_name' => $last_name,
                'primary_email' => $user_email,
            ];

            $warhouse = Warehouse::findOrFail($id);
            $warhouse->update($req_data);
            $message = get_messages('Warhouse  Updated successfully', 1);
            Session::flash('message', $message);
            return redirect()->route('warehouses.index');
        } else if (isset($request->insert_type) && $request->insert_type == 'setting_form') {
            if ($id != '') {
                $req_data = [
                    'warehouse_id' => $id,
                    'lead_time' => $request->lead_time
                ];
                $warehouse_setting = Warehouse_wise_default_setting::where('warehouse_id', $id)->first();
                if (!empty($warehouse_setting)) {
                    $warehouse_setting->update($req_data);
                    $message = get_messages('Warehouse setting updated successfully', 1);
                    Session::flash('message', $message);
                    return redirect()->route('warehouses.index');
                } else {
                    Warehouse_wise_default_setting::create($req_data);
                    $message = get_messages('Warehouse setting created successfully', 1);
                    Session::flash('message', $message);
                    return redirect()->route('warehouses.index');
                }
            }
        }
    }

    /**
     * Remove the specified warehouse & it's contact from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $get_checks = get_access('warehouse_module', 'delete');
        $get_user_access = get_user_check_access('warehouse_module', 'delete');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
            }
        }
        $warehouse = Warehouse::findOrFail($id);
        $warehouse->delete();
        return json_encode(array('statusCode' => 200));


    }

    public function changeContact(Request $request)
    {
        $contact_id = $request->contact_id;
        $warehouse_contact = Warehouse_contacts::where('id', $contact_id)->first();
        if (!empty($warehouse_contact)) {
            $res['email'] = $warehouse_contact['email'];
            $res['phone'] = $warehouse_contact['mobile'];
        } else {
            $res['email'] = '';
            $res['phone'] = '';
        }
        return response()->json($res);
    }

    public function ImportWarehouse()
    {
        \Session::forget('error_msg_array');
        \Session::forget('success_message');

        $fileContent = file_get_contents(request()->file('file'));
        $enc = mb_detect_encoding($fileContent, mb_list_encodings(), true);
        \Config::set('excel.imports.csv.input_encoding', $enc);
        $WarehouseImport = new WarehouseImport();
        $collection= Excel::import($WarehouseImport ,request()->file('file'),'UTF-8');
         return redirect('/warehouse_import_message');
    }

    public function warehouse_import_message()
    {
        $error_msg=((\Session::get('error_msg_array')) ? \Session::get('error_msg_array') : '');
        $success_msg=((\Session::get('success_message')) ? \Session::get('success_message') : '');
        
        $error_count=((\Session::get('error_count')) ? \Session::get('error_count') : '');
        $success_count=((\Session::get('success_count')) ? \Session::get('success_count') : '');
        $total_count=((\Session::get('total_count')) ? \Session::get('total_count') : '');

        if(!empty($error_msg)){
            return view('warehousesfinal.import_messages',compact('error_msg','success_msg','total_count','error_count','success_count'));
        }else{
            $message = get_messages("Total $total_count rows uploaded successfully", 1);
            Session::flash('message', $message);
            return redirect('/warehouses');
        }
        // $error_msg=((\Session::get('error_msg_array')) ? \Session::get('error_msg_array') : '');
        // $success_msg=((\Session::get('success_message')) ? \Session::get('success_message') : '');
        // return view('warehousesfinal.import_messages',compact('error_msg','success_msg'));
    }

    public function getData(){
        $warehouses = Warehouse::orderBy('id','desc')->get();
        $warehouses->each(function($warehouse){
            if ($warehouse['type'] == 1) {
                $warehouse['contacts'] = Warehouse_wise_users::where(array('warehouse_id' => $warehouse['id']))->get();
            } else {
                $warehouse['contacts'] = Warehouse_contacts::where(array('warehouse_id' => $warehouse['id']))->get();
            }
            $warehouse_setting = Warehouse_wise_default_setting::where(array('warehouse_id' => $warehouse['id']))->first();
            if (empty($warehouse_contact) || empty($warehouse_setting)) {
                $check_return = 0;
            } else {
                $check_return = 1;
            }
            $warehouses['completeFlag'] = $check_return;
        });

        return $warehouses;
    }

    public function bulkDelete(Request $request){
        $result = Warehouse::whereKey($request->selected)->delete();
        return ['success' => $result];
    }

    public function bulkExport(){
        return response()->streamDownload(function(){
            echo Warehouse::toCsv();
        }, 'warehouses'. date("Ymdhis") .'.csv');
    }

    public function updateCols(){
        if(DB::table('cols')->where('user_id', auth()->id())->where('table_name', 'warehouses')->get()->count()){
            DB::table('cols')
                ->where('user_id', auth()->id())
                ->where('table_name', 'warehouses')
                ->update(['cols' => request()->cols]);
        }else{
            DB::table('cols')
                ->insert([
                    'user_id' => auth()->id(),
                    'table_name' => 'warehouses',
                    'cols' => json_encode(request()->cols)
                ]);
        }
    }
}

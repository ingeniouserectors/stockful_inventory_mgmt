<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Vendor_blackout_date;
use App\Models\Vendor;
use Response;



class VendorBlackouteDateController extends Controller
{
    /**
     * download the structure of the vendor backout date.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            $filename = 'vendor-blackout-date.csv';
            $headers = array(
                "Content-type" => "text/csv",
                "Content-Disposition" => "attachment; filename=" . $filename . " ",
                "Pragma" => "no-cache",
                "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                "Expires" => "0"
            );
            $columns = array('Blackoutdate(Y-M-D)','Reason');
            $fileputcsv = array(date('Y-m-d'),'Test');

            $callback = function () use ($columns, $fileputcsv) {
                ob_clean();
                $file = fopen('php://output', 'w+');
                fputcsv($file, $columns);
                fputcsv($file, $fileputcsv);
                fclose($file);
            };
            return Response::stream($callback, 200, $headers);
        
    }

    /**
     * Show the form for creating a new vendors blackout dates.
     * @method GET
     * @params no-params
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $vendor_id = $_GET['vendor_id'];
        return view('vendors.vendors_blackoute_dates.create', [
            'vendors_id' => $vendor_id,
        ]);
        
    }

    /* There are 4 function added in store method :
     *      @method POST
     *
     *  Request Types:
     *
     * 1. vendors_blackoutedate_list : listing of vendors blackout date
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     * 2. single: create a single vendors blackout date
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     * 3. daterange: create a daterange vendors blackout date
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     * 4. upload_csv: upload csv file for vendors blackout date
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $user_Id = $request->vendor_id;
         $inputs = $request->all();
         if($request->request_type == 'vendors_blackoutedate_list'){

         $get_vendors_blackoute_date = Vendor_blackout_date::where(array('vendor_id' => 
            $request->id))->get()->toArray();
          $get_vendors_blackoute_date_data = array();
         if(!empty($get_vendors_blackoute_date))
            {
                 foreach ($get_vendors_blackoute_date as $key => $post)
                {
                    $action = '' ;
                    $nestedData['blackout_date'] =  $post['blackout_date'];
                    $nestedData['reason'] =  $post['reason'];
                    $action .= '<a href="'.route('vendor_blackoute_date.edit',$post['id']).'" class=" btn btn-info btn-sm btn-rounded waves-effect waves-light" ><i class="fas fa-edit" title="edit"></i></a>';
                    $action .=  '<button id="button" type="submit" class=" btn btn-danger btn-sm btn-rounded waves-effect  waves-light sa-remove " data-id='.$post['id'].'><i class="fas fa-trash-alt"></i></button>';
                    $nestedData['action'] =  $action ;
                    $get_vendors_blackoute_date_data[] = $nestedData;               
                }    
             }
                
           echo json_encode($get_vendors_blackoute_date_data); exit;


       }
        if($request->customRadio == 'single'){
                if($inputs['blackout_date_single'] >= date('Y-m-d')){
                    $get_check_date = Vendor_blackout_date::where(array('blackout_date'=>$inputs['blackout_date_single']))->get()->toArray();
                    if(empty($get_check_date)){
                        $backoute_date = [
                            'blackout_date'=> $inputs['blackout_date_single'],
                            'reason'=> $inputs['reason_single'],
                            'vendor_id'=> $inputs['vendor_id'],
                        ];
                        $datas_of = Vendor_blackout_date::create($backoute_date);
                        $message = get_messages('Vendors blackoute date added successfully', 1);
                        Session::flash('message', $message);
                    }else{
                        $message = get_messages(' blackoute date is already exists!', 0);
                        Session::flash('message', $message);
                    }
                }else{
                    $message = get_messages(' Invalid backoute date', 0);
                    Session::flash('message', $message);
                }
                 return redirect('vendor_blackoute_date/'.$request->vendor_id);
            }else if($request->customRadio == 'daterange'){
                if($request->blackout_date_multiple_start > $request->blackout_date_multiple_end ){
                    $message = get_messages(' To date Must be greater than From date', 0);
                    Session::flash('message', $message);
                }else if($request->blackout_date_multiple_start < date('Y-m-d') || $request->blackout_date_multiple_end < date('Y-m-d')){
                    $message = get_messages('Invalid backoute date', 0);
                    Session::flash('message', $message);
                }else{
                    $date = $this->displayDates($request['blackout_date_multiple_start'], $request['blackout_date_multiple_end']);

                    foreach($date as $backoutdate){
                        $get_check_date = Vendor_blackout_date::where(array('blackout_date'=>$backoutdate))->get()->toArray();

                        if(empty($get_check_date)) {
                            $backoute_date = [
                                'blackout_date' => $backoutdate,
                                'reason' => $inputs['reason_multiple'],
                                'vendor_id' => $inputs['vendor_id'],
                            ];
                           
                           
                            $datas_of = Vendor_blackout_date::create($backoute_date);
                        }else{
                            $message = get_messages($backoutdate.' blackoute date is already exists!', 0);
                            Session::flash('message', $message);
                            return redirect('vendor_blackoute_date/'.$request->vendor_id);
                        }
                    }
                    $message = get_messages('Vendors blackoute date added successfully', 1);
                    Session::flash('message', $message);
                }
                 return redirect('vendor_blackoute_date/'.$request->vendor_id);
            }

         if($request->insert_type == 'upload_csv'){
            if ($request->hasFile('uploadFile')) {
                $size = $request->file('uploadFile')->getSize();
                $extension = $request->uploadFile->getClientOriginalExtension();
                if ($extension == "csv" && $size <= 100000) {
                    $filePath = $request->file('uploadFile')->getRealPath();
                    $file = fopen($filePath, "r");
                    $escapedHeader = [];
                    $detail = [];
                    $flag = 0;

                    while ($columns = fgetcsv($file)) {
                        $columnValue = [];
                        if ($flag == 0) {
                            if (($columns[0]) != 'Blackoutdate(Y-M-D)'|| ($columns[1])!='Reason') {
                                $message = get_messages('Please upload proper file format as provide in sample!', 0);
                                Session::flash('message', $message);
                                return redirect('vendor_blackoute_date/'.$request->vendor_id);
                            }
                        }
                        foreach ($columns as $key => &$value) {
                            if ($flag == 0) {
                                $lowerHeader = strtolower($value);
                                $escapedItems = preg_replace("/[^a-z]/", "", $lowerHeader);
                                array_push($escapedHeader, $escapedItems);
                            } else {
                                array_push($columnValue, $value);
                            }
                        }
                        $flag++;
                        if (!empty($columnValue))
                            $detail[] = array_combine($escapedHeader, $columnValue);
                    }

                    if(!empty($detail)) {
                        $uploaded_file = $request->file('uploadFile');
                        $name = 'csv_' . time() . $uploaded_file->getClientOriginalName();
                        $uploaded_file->move(public_path('assets/images/csv/vendors'), $name);
                        $images[] = $name;
                        $storefolder = 'assets/images/csv/vendors/csv_' . time() . $uploaded_file->getClientOriginalName();
                    }

                    foreach ($detail as $data) {
                        $product = [];
                        if (strlen($data['reason']) <= 0) {
                            $message = get_messages('reason is empty.', 0);
                            Session::flash('message', $message);
                         } 
                        elseif (date('Y-m-d',strtotime($data['blackoutdateymd'])) == '1970-01-01') {
                            $message = get_messages('Enter Valid Blackoute Date.', 0);
                            Session::flash('message', $message);
                        }elseif (date('Y-m-d',strtotime($data['blackoutdateymd'])) < date('Y-m-d')){
                            $message = get_messages('Please enter a date greater than or equal to '.date('Y-m-d').'', 0);
                            Session::flash('message', $message);
                        } else {

                             $check_date = Vendor_blackout_date::where(array('blackout_date'=>date('Y-m-d',strtotime($data['blackoutdateymd']))))->get()->toArray();
                            if(empty($check_date)) {
                            $check_date_already = Vendor_blackout_date::where(array('blackout_date'=>date('Y-m-d',strtotime($data['blackoutdateymd'])),'vendor_id'=>$request->vendor_id,'reason'=>$request->reason))->get()->toArray();
                                $req_datas = [
                                    'vendor_id' =>$request->vendor_id,
                                     'reason'=>$data['reason'],
                                    'blackout_date' => date('Y-m-d',strtotime($data['blackoutdateymd']))
                                ];

                                  $datas_of = Vendor_blackout_date::create($req_datas);

                                $message = get_messages('csv uploaded successfully !', 1);
                                Session::flash('message', $message);
                            }else{
                                $message = get_messages(date('Y-m-d',strtotime($data['blackoutdateymd'])).' date already exist!', 0);
                                Session::flash('message', $message);
                            }
                        }
                    }
                    return redirect('vendor_blackoute_date/'.$request->vendor_id);
                }
            }else{
                $message = get_messages('CSV file must be required', 0);
                Session::flash('message', $message);
                return redirect('vendor_blackoute_date/'.$request->vendor_id);
            }
        }
    }

    /**
     * Display the specified vendors blackout date.
     * @method POST
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id != '') {
            $get_vendors = Vendor::where(array('id' => $id))->first();
            return view('vendors.vendors_blackoute_dates.index', [
                'vendors_id' => $id,
                'vendors'=>$get_vendors,
            ]);
        }else{
            $message = get_messages('Failed to get vendors contact details', 0);
            Session::flash('message', $message);
            return redirect()->route('vendors.index');
        }
       
        
    }

    /**
     * get the date with proper format
     * @method GET
     * @param  $$date1, $date2,$format
     * @return \Illuminate\Http\Response
     */

    function displayDates($date1, $date2, $format = 'Y-m-d' ) {
        $dates = array();
        $current = strtotime($date1);
        $date2 = strtotime($date2);
        $stepVal = '+1 day';
        while( $current <= $date2 ) {
            $dates[] = date($format, $current);
            $current = strtotime($stepVal, $current);
        }
        return $dates;
    }

    /**
     * Show the form for editing the specified blackout date.
     * @method GET
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $vendors_blackoute_date_data = Vendor_blackout_date::where(array('id'=>$id))->get()->first();
        return view('vendors.vendors_blackoute_dates.edit', [
                'vendors_blackoute_dates_details' => $vendors_blackoute_date_data,
            ]
        );
    }

    /**
     * Update the specified blackout date in storage.
     * @method POST
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {    
         $user_Id = $request->vendor_id;
         $blackout_date = $request->blackout_date;
         $reason = $request->reason;

           if(!empty($blackout_date)) {
                $check_blackoutdate = Vendor_blackout_date::where(array('vendor_id'=>$request->vendor_id))->whereNotIn('id',[$id])->get()->toArray();

                foreach ($check_blackoutdate as $key => $value) {
                    if ($blackout_date===$value['blackout_date']) {
                    $message = get_messages('Duplicate date should not be allowed!', 0);
                    Session::flash('message', $message);
                      return redirect('vendor_blackoute_date/'.$request->vendor_id);
                }
                    
                }                           
            }
            $sub_data = [
                        'vendor_id' => $user_Id,
                        'blackout_date' => $blackout_date,
                        'reason'=>$reason
                        ];
        $vendors = Vendor_blackout_date::findOrFail($id);
        $vendors->update($sub_data);
        $message = get_messages('Vendors blackoute date Updated successfully', 1);
                Session::flash('message', $message);
        return redirect('vendor_blackoute_date/'.$request->vendor_id);
                            
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vendors_blackoute_date = Vendor_blackout_date::findOrFail($id);
        $vendors_blackoute_date->delete();
        return json_encode(array('statusCode'=>200));
    }

    
}

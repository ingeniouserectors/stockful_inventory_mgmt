<?php

namespace App\Http\Controllers;

use App\Models\Amazon_shipment;
use App\Models\Country;
use App\Models\Daily_logic_Calculations;
use App\Models\Mws_product;
use Illuminate\Support\Facades\Session;
use App\Models\Purchase_order_details;
use App\Models\Purchase_orders;
use App\Models\Usermarketplace;
use Illuminate\Http\Request;
use DB;
use App\Models\Warehouse_product;
use App\Models\Mwsdeveloperaccount;


class ReplenishController extends Controller
{
    /**
     * Display a listing replenish product data view .
     * @param no-params
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get_checks = get_access('replenish_module','view');
        $get_user_access = get_user_check_access('replenish_module','view');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }
        $global_marketplace = session('MARKETPLACE_ID');
        //$replenishment = Daily_logic_Calculations::with(['mws_product'])->where(array('user_marketplace_id'=>$global_marketplace))->get()->toArray();
        return view('replenish.index',[
            'marketplace'=> $global_marketplace,
            //'replenishment'=>$replenishment
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * There are 2 function added in store method :
     *      @method POST
     *
     *  Request Types:
     *
     * 1. replenish_data : get the listing replanish data with marketplace wise using ajax
     *      @param  $request
     *      @return \Illuminate\Http\Response
     *
     * * 2. create_shipment: create a amazon shipment
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request->insert_type == 'replenish_data'){
            $inputs = $request->all();
            $datas = array();
            $nestedData = array();
            $action = '';
            $global_marketplace = session('MARKETPLACE_ID');
            //$posts = Daily_logic_Calculations::with(['mws_product'])->where(array('user_marketplace_id'=>$global_marketplace))->get()->toArray();
            //$posts = Purchase_order_details::with(['product'])->where(array(''));
            $posts = DB::table('purchase_order_details')->join('purchase_orders', 'purchase_orders.id', '=', 'purchase_order_details.purchase_orders_id')
                ->join('mws_products','purchase_order_details.product_id','=','mws_products.id')
                ->selectRaw('purchase_order_details.id as id,
                            purchase_order_details.product_id as product_id,
                            mws_products.prod_image as prod_image,
                            mws_products.prod_name as prod_name,
                            mws_products.your_price as your_price,
                            mws_products.sku as sku,
                            mws_products.asin as asin')
                ->where(array('purchase_orders.user_marketplace_id'=>$global_marketplace,'purchase_orders.status'=>2))
                ->groupBy('purchase_order_details.product_id')
                ->get()->toArray();


            if(!empty($posts))
            {
                foreach ($posts as $key => $post)
                {
                    $get_product_details = Purchase_order_details::where(array('product_id'=>$post->product_id))->get()->toArray();
                    $sum_product_units = Purchase_order_details::where(array('product_id'=>$post->product_id))->sum('projected_units');

                    $nestedData['id'] = $key + 1;
                    $nestedData['prod_image'] = '<img width="100px" src="'.$post->prod_image.'" title="'.$post->prod_name.'">';
                    $nestedData['prod_name'] = '<span title="'.$post->prod_name.'">'.substr(strip_tags($post->prod_name),0,25)."...".'</span>';
                    $nestedData['sku'] =  $post->sku;
                    $nestedData['asin'] =  '<a href=" https://www.amazon.com/gp/product/'.$post->asin.'" target="_blank" >'.$post->asin.'</a>';
                    $nestedData['price'] = $post->your_price;
                    /*$nestedData['inventory'] =  '';
                    $nestedData['days_of_supply'] = '';
                    $nestedData['replenish_date'] = '';*/
                    $nestedData['projected_units'] = $sum_product_units;
                    /*$nestedData['current_inventory'] =  '';
                    $nestedData['pos'] = '';
                    $nestedData['warehouses'] = '';
                    $nestedData['fba'] = '';*/
                    $action = '<a href="'.route('replenish.show',$post->product_id).'"  class=" btn btn-info btn-sm btn-rounded waves-effect waves-light" title="view"><i class="fas fa-eye" ></i></a>';
                    $nestedData['action'] =  $action ;
                    $datas[] = $nestedData;
                }
            }

            echo json_encode($datas);
            exit;
        }
         if($request->insert_type == 'create_shipment'){
              $product_id= $request->product_id;
              $quantity=$request->quantity;
              $sellerSKU = $request->sku;
             /*$address = Warehouse_product::with('warehouse')->where('product_id',$product_id)->first();
             $country = Country::where('id',$address->warehouse['country_id'])->first();
             $userMarketplace = Usermarketplace::where('id', session('MARKETPLACE_ID'))->first();
             $fromaddress = new \FBAInboundServiceMWS_Model_Address();
             $fromaddress->setName($address->warehouse['warehouse_name']);
             $fromaddress->setAddressLine1($address->warehouse['address_line_1']);
             $fromaddress->setAddressLine2($address->warehouse['address_line_2']);
             $fromaddress->setCountryCode($country['country_code']);
             $fromaddress->setStateOrProvinceCode($address->warehouse['state']);
             $fromaddress->setCity($address->warehouse['city']);
             $fromaddress->setPostalCode($address->warehouse['zipcode']);
             $service = $this->getReportsClient();
             $ship_request = new \FBAInboundServiceMWS_Model_CreateInboundShipmentPlanRequest();
             $ship_request->setSellerId(base64_decode($userMarketplace['mws_sellerid']));
             $ship_request->setMWSAuthToken(base64_decode($userMarketplace['mws_authtoken']));
             $ship_request->setShipFromAddress($fromaddress);
             $item = array();
             if($quantity > 0) {
                 $data = array('SellerSKU' => $sellerSKU, 'Quantity' => $quantity);
                 $item[] = new \FBAInboundServiceMWS_Model_InboundShipmentPlanItem($data);
             }
             if(!empty($item)) {
                 $itemlist = new \FBAInboundServiceMWS_Model_InboundShipmentPlanRequestItemList();
                 $itemlist->setmember($item);
                 $ship_request->setInboundShipmentPlanRequestItems($itemlist);
                 $arr_response = $this->invokeCreateInboundShipmentPlan($service, $ship_request);
                 //create shipments api of particular shipmentplan
                 $shipment_service = $this->getReportsClient();
                 $shipment_request = new \FBAInboundServiceMWS_Model_CreateInboundShipmentRequest();
                 $shipment_request->setSellerId(base64_decode($userMarketplace['mws_sellerid']));
                 $shipment_request->setMWSAuthToken(base64_decode($userMarketplace['mws_authtoken']));
                 $shipment_header = new \FBAInboundServiceMWS_Model_InboundShipmentHeader();
                 $shipment_header->setShipmentName("SHIPMENT_NAME");
                 $shipment_header->setShipFromAddress($fromaddress);
                 //response of shipment plan and insert data in amazon shipment
                 $shipment_id = $this->generate_shipment_id();
                 foreach ($arr_response as $new_response) {
                     foreach ($new_response->InboundShipmentPlans as $planresult) {
                         foreach ($planresult->member as $member) {
                             $api_shipment_id = $member->ShipmentId;
                             $destination_name = $member->DestinationFulfillmentCenterId;
                             foreach ($member->ShipToAddress as $address) {
                                 $address_name = $address->Name;
                                 $addressline1 = $address->AddressLine1;
                                 $city = $address->City;
                                 $state = $address->StateOrProvinceCode;
                                 $country = $address->CountryCode;
                                 $postal = $address->PostalCode;
                             }
                             $preptype = $member->LabelPrepType;
                             foreach ($member->EstimatedBoxContentsFee as $fee) {
                                 $total_unit = $fee->TotalUnits;
                                 foreach ($fee->FeePerUnit as $unit) {
                                     $unit_currency = $unit->CurrencyCode;
                                     $unit_value = $unit->Value;
                                 }
                                 foreach ($fee->TotalFee as $total) {
                                     $total_currency = $total->CurrencyCode;
                                     $total_value = $total->Value;
                                 }
                             }
                             $shipment_header->setDestinationFulfillmentCenterId($destination_name);
                             $shipment_request->setInboundShipmentHeader($shipment_header);
                             $shipment_request->setShipmentId($api_shipment_id);
                             $shipment_item = array();
                             foreach ($member->Items as $item) {
                                 foreach ($item->member as $sub_member) {
                                     $amazon_destination = array('destination_name' => $destination_name,
                                         'shipment_id' => $shipment_id,
                                         'product_id'=>$product_id,
                                         'api_shipment_id' => $api_shipment_id,
                                         'sellerSKU' => $sub_member->SellerSKU,
                                         'fulfillment_network_SKU' => $sub_member->FulfillmentNetworkSKU,
                                         'qty' => $sub_member->Quantity,
                                         'ship_to_address_name' => $address_name,
                                         'ship_to_address_line1' => $addressline1,
                                         'ship_to_city' => $city,
                                         'ship_to_state_code' => $state,
                                         'ship_to_country_code' => $country,
                                         'ship_to_postal_code' => $postal,
                                         'label_prep_type' => $preptype,
                                         'total_units' => $total_unit,
                                         'fee_per_unit_currency_code' => $unit_currency,
                                         'fee_per_unit_value' => $unit_value,
                                         'total_fee_value' => $total_value
                                     );
                                     Amazon_shipment::create($amazon_destination);
                                     $item_array = array('SellerSKU' => $sub_member->SellerSKU, 'QuantityShipped' => $sub_member->Quantity, 'FulfillmentNetworkSKU' => $sub_member->FulfillmentNetworkSKU);
                                     $shipment_item[] = new \FBAInboundServiceMWS_Model_InboundShipmentItem($item_array);
                                 }
                             }
                             $api_shipment_detail = new \FBAInboundServiceMWS_Model_InboundShipmentItemList();
                             $api_shipment_detail->setmember($shipment_item);
                             $shipment_request->setInboundShipmentItems($api_shipment_detail);
                             $this->invokeCreateInboundShipment($shipment_service, $shipment_request);
                         }
                     }
                 }
             }*/

             $message = get_messages('Amazon shipment created successfully!', 1);
             Session::flash('message', $message);
             return redirect()->back();
         }

    }

    /**
     * get & check the shipment id
     *
     * @method GET
     * @param  no-params
     * @return \Illuminate\Http\Response
     */


    public function generate_shipment_id(){
        $generate_trake = rand(100,999);
        $track_id = 'SHIP'.$generate_trake;
        if(!empty($track_id)){
            $check_tranck_number = Amazon_shipment::where(array('shipment_id'=>$track_id))->get()->toArray();
            if(!empty($check_tranck_number)){
                return $this->generate_shipment_id();
            }else{
                return $track_id;
            }
        }
    }

    /**
     * These all methods are get amazon data with requirements
     *
     * @method GET
     * @param  no-params
     * @return \Illuminate\Http\Response
     */

    protected function getReportsClient(){
        list($access_key, $secret_key, $config) = $this->getKeys();
        return new \FBAInboundServiceMWS_Client(
            $access_key,
            $secret_key,
            env('APPLICATION_NAME'),
            env('APPLICATION_VERSION'),
            $config
        );
    }
    private function getKeys(){
        add_to_path('Libraries');
        $marketplace = Usermarketplace::where('id',session('MARKETPLACE_ID'))->first();
        $devAccount = Mwsdeveloperaccount::where('marketplace_id',$marketplace['mws_marketplaceid'])->first();
        return [
            base64_decode($devAccount['access_key']),
            base64_decode($devAccount['secret_key']),
            self::getMWSConfig()
        ];
    }
    public static function getMWSConfig(){
        return [
            'ServiceURL' => "https://mws.amazonservices.com/FulfillmentInboundShipment/2010-10-01",
            'ProxyHost' => null,
            'ProxyPort' => -1,
            'ProxyUsername' => null,
            'ProxyPassword' => null,
            'MaxErrorRetry' => 3,
        ];
    }
    function invokeCreateInboundShipmentPlan(\FBAInboundServiceMWS_Interface $service, $request){
        try {

            $response = $service->CreateInboundShipmentPlan($request);
            $dom = new \DOMDocument();
            $dom->loadXML($response->toXML());
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $dom->saveXML();
            return $arr_response = new \SimpleXMLElement($dom->saveXML());
        } catch (\FBAInboundServiceMWS_Exception $ex) {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
        }
    }
    function invokeCreateInboundShipment(\FBAInboundServiceMWS_Interface $service, $request){
        try {
            $response = $service->CreateInboundShipment($request);
            $dom = new \DOMDocument();
            $dom->loadXML($response->toXML());
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $dom->saveXML();
            return 1;
        } catch (\FBAInboundServiceMWS_Exception $ex) {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
        }
    }
    public function arrToQueryString($param){
        $strURL = "";
        $url = array();
        foreach ($param as $key => $val) {
            $key = str_replace("%7E", "~", rawurlencode($key));
            $val = str_replace("%7E", "~", rawurlencode($val));
            $url[] = "{$key}={$val}";
        }
        sort($url);
        $strURL = implode('&', $url);
        return $strURL;
    }
    public function sendQuery($strUrl, $amazon_feed, $param){
        $devAccount = Mwsdeveloperaccount::where('access_key',base64_encode($param['AWSAccessKeyId']))->first();
        $secret_key = base64_decode($devAccount['secret_key']);
        $strServieURL = preg_replace('#^https?://#', '',$devAccount['marketplace_url'] );
        $strServieURL = str_ireplace("/", "", $strServieURL);
        $sign = 'POST' . "\n";
        $sign .= $strServieURL . "\n";
        $sign .= '/Feeds/' . $param['Version'] . '' . "\n";
        $sign .= $strUrl;
        $signature = hash_hmac("sha256", $sign, $secret_key, true);
        $signature = urlencode(base64_encode($signature));
        $httpHeader = array();
        $httpHeader[] = 'Transfer-Encoding: chunked';
        $httpHeader[] = 'Content-Type: application/xml';
        $httpHeader[] = 'Content-MD5: ' . base64_encode(md5($amazon_feed, true));
        $httpHeader[] = 'Expect:';
        $httpHeader[] = 'Accept:';
        $link = "https://mws.amazonservices.com/Feeds/" . $param['Version'] . "?";
        $link .= $strUrl . "&Signature=" . $signature;
        $ch = curl_init($link);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeader);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $amazon_feed);
        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        return $response;
    }
    /**
     * Display the specified replenish order details.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product_data = Mws_product::where(array('id'=>$id))->first();
        $get_product_details = Purchase_order_details::where(array('product_id'=>$id))->get()->toArray();
        return view('replenish.view',[
            'product_details_data'=> $get_product_details,
            'product_data' =>$product_data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mws_product;
use Response;

class SalesHistoryController extends Controller
{
    /**
     * Display a listing of daily logic calulations & mws_product with particular marketplace id .
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        ini_set('memory_limit','-1');
        $global_marketplace = session('MARKETPLACE_ID');
        $get_all_product_sku = Mws_product::with(['daily_logic_calculations'])->where(array('user_marketplace_id'=>$global_marketplace))->selectRaw('sku,id')->get()->toArray();
        return view('saleshistory.index', [
            'Daily_logic_calculations'=>$get_all_product_sku
       ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Download all mws_product & daily_logic_calulations with particular marketplace wise
     *
     * @method POST
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id == 'import') {

            ini_set('memory_limit','-1');
            $data_arrays = array('SKU','7 Days Sales','7 Days Rate','14 Days Sales','14 Days Rate','30 Days Sales','30 Days Rate','7 Over 7','30 Over 30', '30 Days Sales - Last Year');
            for ($count=0;$count<=24;$count++) {
                $result = date('m-Y', strtotime("-$count month"));
                array_push($data_arrays,$result);
            }

            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename=Sales History.csv');
            $output = fopen("php://output", "w");

            fputcsv($output, $data_arrays);
            $global_marketplace = session('MARKETPLACE_ID');
            $get_all_product_sku = Mws_product::with(['daily_logic_calculations'])->where(array('user_marketplace_id'=>$global_marketplace))->selectRaw('sku,id')->get()->toArray();
            foreach($get_all_product_sku as $record)
            {
                $fileputcsv['SKU'] = $record['sku'];
                $fileputcsv['7 Days Sales'] = isset($record['daily_logic_calculations'][0]['sevendays_sales']) ? $record['daily_logic_calculations'][0]['sevendays_sales'] : '';
                $fileputcsv['7 Days Rate'] = isset($record['daily_logic_calculations'][0]['sevenday_day_rate']) ? $record['daily_logic_calculations'][0]['sevenday_day_rate'] : '';
                $fileputcsv['14 Days Sales'] = isset($record['daily_logic_calculations'][0]['fourteendays_sales']) ? $record['daily_logic_calculations'][0]['fourteendays_sales'] : '';
                $fileputcsv['14 Days Rate'] = isset($record['daily_logic_calculations'][0]['fourteendays_day_rate']) ? $record['daily_logic_calculations'][0]['fourteendays_day_rate'] : '';
                $fileputcsv['30 Days Sales'] = isset($record['daily_logic_calculations'][0]['thirtydays_sales']) ? $record['daily_logic_calculations'][0]['thirtydays_sales'] : '';
                $fileputcsv['30 Days Rate'] = isset($record['daily_logic_calculations'][0]['thirtyday_day_rate']) ? $record['daily_logic_calculations'][0]['thirtyday_day_rate'] : '';
                $fileputcsv['7 Over 7'] = isset($record['daily_logic_calculations'][0]['sevendays_previous']) ? $record['daily_logic_calculations'][0]['sevendays_previous'] : '';
                $fileputcsv['30 Over 30'] = isset($record['daily_logic_calculations'][0]['thirtydays_previous'])  ? $record['daily_logic_calculations'][0]['thirtydays_previous'] : '';
                $fileputcsv['30 Days Sales - Last Year'] = isset($record['daily_logic_calculations'][0]['thirtydays_last_year']) ? $record['daily_logic_calculations'][0]['thirtydays_last_year'] : '';

                for ($count=0;$count<=24;$count++){
                    $get_datemonth =  date('Y-m',strtotime("-$count month"));
                    $get_supply_total_trands = get_supply_total_trands($get_datemonth,$record['id']);
                    if(!empty($get_supply_total_trands)){
                        $fileputcsv[$get_datemonth] = $get_supply_total_trands->sales_total;
                    }else{
                        $fileputcsv[$get_datemonth] = '';
                    }
                 }
                fputcsv($output, $fileputcsv);

                for ($count=0;$count<=24;$count++){
                    $get_datemonth =  date('Y-m',strtotime("-$count month"));
                    $get_supply_total_trands = get_supply_total_trands($get_datemonth,$record['id']);
                    if(!empty($get_supply_total_trands)){
                        $fileputcsv['SKU'] = '';
                        $fileputcsv[$get_datemonth] = $get_supply_total_trands->day_rate;
                    }else{
                        $fileputcsv['SKU'] = '';
                        $fileputcsv[$get_datemonth] = '';
                    }
                }
                fputcsv($output, $fileputcsv);
            }
            fclose($output);
            exit;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;
use App\Helpers\CollectionHelper;
use App\Models\Lead_time;
use App\Models\Lead_time_check;
use App\Models\Lead_time_type;
use App\Models\Lead_time_value;
use App\Models\Order_volume_type;
use App\Models\Reorder_schedule_detail;
use App\Models\Reorder_schedule_type;
use App\Models\Shipping;
use App\Models\ShippingAgent;
use App\Models\Shippingair;
use App\Models\ShippingContainerSettings;
use App\Models\ShipToWarehouse;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Response;
use Illuminate\Support\Facades\Session;
use App\Models\Supplier;
use App\Models\Supplier_contact;
use App\Models\Supplier_default_setting;
use App\Models\Supplier_wise_default_setting;
use App\Models\Supplier_blackout_date;
use App\Models\Country;
use App\Models\Payment_details;
use App\Models\Payment;
use App\Models\Payment_terms_type;
use Validator;
use App\Imports\SupplierImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Export\SupplierExport;
use App\Models\Field_list;
use App\Models\User_field_access;

class SuppliersController extends Controller
{
    /**
     * Display a listing view of the suppliers with marketplace wise
     *
     * @param  no -params
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get_checks = get_access('supplier_module', 'view');
        $get_user_access = get_user_check_access('supplier_module', 'view');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }

        $get_all_fileds = array();
        $get_module_id = get_module_id('supplier_module');
        if ($get_module_id > 0) {
            $get_all_fileds = Field_list::where(array('active' => '1', ['display_name', '!=', NULL], 'module_id' => $get_module_id))->orderBy('id', 'asc')->get()->toArray();
        }

        $global_marketplace = session('MARKETPLACE_ID');
        $marketplace = '';
        $data = array();
        if ($global_marketplace != '') {
            $marketplace = $global_marketplace;
            //$data_count = Supplier::with(['supplier_contact','supplier_product'])->where(array('user_marketplace_id'=>$global_marketplace))->count();
        }

//        $keyj = 1;
//        for($i = 1 ; $i <= $data_count ; $i=$i+(env('PAGINATION'))){
//            if($i == 1){
//                $newi = $i;
//            }else{
//                $newi = $i-1;
//            }
//            $n = $i + (env('PAGINATION') - 1);
//            //$pagination[$keyj]= $newi.'-'.$n;
//            $pagination[$keyj] = ($newi).'-'.$n;
//
//            $keyj ++;
//        }
     
        //$data=Supplier::with(['supplier_contact','supplier_product'])->where(array('user_marketplace_id'=>$global_marketplace))->get()->toArray();
//        return view('suppliersnew.index',['marketplace' =>$marketplace, 'suppliers' => $data]);

        return view('suppliersfinal.index', [
            'marketplace' => $marketplace,
            'suppliers' => $data,
            'get_all_fields' => $get_all_fileds,
            'countries' => get_country_list(),
            'cols' => DB::table('cols')->where('user_id', auth()->id())
                ->where('table_name', 'suppliers')->pluck('cols')->first()
        ]);
    }


    /**
     * Show the form for creating a new supplier
     *
     * @param  no -params
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $get_checks = get_access('supplier_module', 'create');
        $get_user_access = get_user_check_access('supplier_module', 'create');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }
        $global_marketplace = session('MARKETPLACE_ID');
        $warehouse = Warehouse::where(array('user_marketplace_id' => $global_marketplace))->get()->toArray();
        $country = get_country_list();
        $cid = null;
        $lead_time_type = Lead_time_type::get()->toArray();
        $lead_time_check = Lead_time_check::get()->toArray();
        $order_volume_type= Order_volume_type::get()->toArray();
        $reorder_schedule_type = Reorder_schedule_type::get()->toArray();
        $payment_term = Payment_terms_type::get()->toArray();
        $state_list = get_state_list($cid = 'US');
        $shippingAgents = ShippingAgent::where(array('user_marketplace_id' => $global_marketplace))->get()->toArray();
//        Return view('suppliersnew.create', array('country_list' => $country, 'warehouse' => $warehouse));
        return view('suppliersfinal.create', array(
            'country_list' => $country,
            'warehouse' => $warehouse,
            'state_list' => $state_list,
            'payment_term' => $payment_term,
            'lead_time_type'=>$lead_time_type,
            'lead_time_check'=>$lead_time_check,
            'order_volume_type'=>$order_volume_type,
            'shipping_agents' => $shippingAgents,
            'reorder_schedule_type'=>$reorder_schedule_type
        ));
        return view('suppliersfinal.create', array('country_list' => $country, 'warehouse' => $warehouse, 'state_list' => $state_list));
    }

    /**
     * Show the all state with country wise.
     * @method GET
     * @param  $country_id
     * @return \Illuminate\Http\Response
     */

    public function getStateList(Request $request)
    {
        $inputs = $request->all();
        $list = get_state_list($inputs['country']);
        return response()->json($list);
    }

    /**
     * Show the all city with state wise.
     * @method GET
     * @param  state_id
     * @return \Illuminate\Http\Response
     */

    public function getCityList(Request $request)
    {
        $inputs = $request->all();
        $list = get_city_list($inputs['state']);
        return response()->json($list);
    }


    /**
     * There are 3 function added in store method :
     * @method POST
     *
     *  Request Types:
     *
     * 1. Get_all_state : Get all state on country wise
     * @param  $requesttype
     * @return \Illuminate\Http\Response
     *
     * 2. Get_all_city: Get all city on state wise
     * @param  $requesttype
     * @return \Illuminate\Http\Response
     *
     * 3. suppliers_list: get all suppliers with marketplace wise
     * @param  $requesttype , $request
     * @return \Illuminate\Http\Response
     *
     * 4. suppliers_pagination: get all pagination for suppliers
     * @param  $requesttype , $request
     * @return \Illuminate\Http\Response
     *
     *
     */

    public function store(Request $request)
    {
        if ($request->request_type == 'get_all_suppliers_data') {
            $supplier = env('SUPPLIER');
            $offsetValue = $request->offsetValue;
            if (!empty($offsetValue)) {
                $offsetValue = explode('-', $offsetValue);
                $offset = $offsetValue[0];
                $limit = $request->pagination_slot;
                if ($offset % 2 == 0) {
                    $offset = 0;
                } else {
                    $offset = $offset - 1;
                }
            } else {
                $offset = 0;
                $limit = $request->pagination_slot;
            }

           
            $global_marketplace = session('MARKETPLACE_ID');

                $no_display_fileds = User_field_access::
                join('field_list','field_list.id','=','field_list_id')
                ->select(\DB::raw("GROUP_CONCAT((CASE WHEN original_name IS NULL THEN CONCAT(table_name,'.',display_field) ELSE original_name END)) AS disply_fileds, GROUP_CONCAT((CASE WHEN original_name IS NULL THEN display_field ELSE original_name END)) AS display_all"))
                ->where(array('marketplace_id'=>$global_marketplace,'type'=>2))
              ->first();



         
            $unchecked_arr = explode(',', $no_display_fileds->disply_fileds);
            $total_display_fileds = explode(',',$no_display_fileds->display_all);

            $selected_fields = array();
            $join_table = array();
            $join_first = array();
            $join_last = array();

            $get_module_id = get_module_id('supplier_module');
            if ($get_module_id > 0) {
                $get_all_fileds = Field_list::where(array('active' => '1', ['display_name', '!=', NULL], 'module_id' => $get_module_id))->orderBy('id', 'asc')->get()->toArray();

                if (!empty($get_all_fileds)) {
                    $selected_fields[] = 'suppliers.*';
                    //$get_fields[] = 'id';
                    foreach ($get_all_fileds as $key => $val) {
                        $data_of = $val['original_name'] != '' ? $val['original_name'] : $val['display_field'];
                        $display_name[$data_of] = $val['original_name'] != '' ? $val['original_name'] : $val['display_field'];
                        if ($val['referance_table'] != '' && $val['referance_table_field'] != '') {
                            $selected_fields[] = $val['referance_table'] . '.' . $val['referance_table_field'];
                            //$get_fileds[] = $val['referance_table_field'];
                            $join_table[] = $val['referance_table'];
                            $join_first[] = 'suppliers.' . $val['original_name'];
                            $join_last[] = $val['referance_table'] . "." . $val['field_type'];
                        } else if ($val['table_name'] != '' && $val['display_field'] != '' && $val['join_with'] != '') {
                            $selected_fields[] = $val['table_name'] . '.' . $val['display_field'];
                            //$get_fields[] = $val['display_field'];
                            $join_table[] = $val['table_name'];
                            $join_first[] = 'suppliers.id';
                            $join_last[] = $val['table_name'] . "." . $val['join_with'];
                        } else {
                            $selected_fields[] = $val['original_name'];
                            //$get_fields[] = $val['original_name'];
                        }
                    }
                }

            $whereArray = [
                "user_marketplace_id" => $global_marketplace
            ];
            if($request->get_country){
                $whereArray = [
                    "user_marketplace_id" => $global_marketplace,
                    "country_id" => $request->get_country
                ];
            }
            if($request->search != ''){
                $whereArray[] = [
                    'supplier_name', 'LIKE', "%{$request->search}%"
                ];
            }
            $join_table = array_unique($join_table);
            $selected_raw = array_diff($selected_fields,$unchecked_arr);

            $selected_field = "'" . implode("', '", $selected_fields) . "'";

            

            $userRole =Supplier::with(['supplier_contact' => function ($query) {
                $query->orderBy('primary', 'desc');
            }, 'supplier_product'])->select($selected_raw)->where($whereArray);
            if (!empty($join_table)) {
                foreach ($join_table as $key => $jon) {
                    $userRole->leftjoin("$jon", "$join_first[$key]", '=', "$join_last[$key]");
                }
            }
            if($offset > 0)
                $listing_data = $userRole->offset($offset)->limit($limit)->groupBy('id')->orderBy('id', 'desc')->get()->toArray();
            else
                $listing_data = $userRole->limit($limit)->groupBy('id')->orderBy('id', 'desc')->get()->toArray();



            $listing_data_count = Supplier::with(['supplier_contact', 'supplier_product'])->where($whereArray)->get()->count();
            }
            $total_count = 0;
            if (!empty($listing_data)) {
                $output = '';

                foreach ($listing_data as $index => $data) {
                    $suppliers_contact = Supplier_contact::where(array('supplier_id'=>$data['id']))->first();
                    $suppliers_blackoutdate = Supplier_blackout_date::where(array('supplier_id'=>$data['id']))->first();
                    $lead_time = Lead_time::where(array('supplier_vendor'=>$supplier,'supplier_vendor_id'=>$data['id']))->first();
                    $order_setting = Reorder_schedule_detail::where(array('supplier_vendor' => $supplier, 'supplier_vendor_id' =>$data['id']))->first();
                    $payment = Payment::where(array('supplier_vendor'=>$supplier,'supplier_vendor_id'=>$data['id']))->first();
                    $shipping = Shipping::where(array('supplier_vendor'=>$supplier,'supplier_vendor_id'=>$data['id']))->first();

                    

                    if(empty($suppliers_contact) || empty($suppliers_blackoutdate) || empty($lead_time) || empty($order_setting) || empty($payment) || empty($shipping)){
                        $check_return = 0;
                    }else{
                        $check_return = 1;
                    }
                    if($request->toggle_b == 1){
                        if($check_return == 0) {
                            $i = 1;
                            $counter = $index + 1;
                            $output .=
                            '<tr id="' . $data['id'] . '">
                                <td class="p-2">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input row-checkbox" name="delete_all" data-val="' . $data['id'] . '" value="' . $data['id'] . '" id="customCheck' . $counter . '">
                                        <label class="custom-control-label" for="customCheck' . $counter . '">&nbsp;</label>
                                    </div>
                                </td>

                                <td class="p-2">';
                        if ($check_return == 0) {
                            $output .= '<i class="fas fa-exclamation-triangle" style="color:#F1B44C;" data-toggle="tooltip" title="Missing required settings"></i>';
                        }
                        $output .= '</td>';
                        $newtext = 0;
                        $count = count($display_name);
                        foreach ($display_name as $key => $dispay) {
                            if(!in_array($dispay,$total_display_fileds)){
                            $t = $i + $newtext;
                            $newtext++;
                            if ($dispay == 'country_id') {
                                $dats = get_country_fullname($data[$key]);
                            } else if ($dispay == 'first_name') {
                                 if (!empty($data['supplier_contact'])) {
                                    $options = '';
                                         $options = '<select name="supplier_contact_' . $data['id'] . '" id="supplier_contact_' . $data['id'] . '" class="form-control border-0 pl-0 bg-transparent" onchange="changeContact(' . $data['id'] . ')">';
                                         foreach ($data['supplier_contact'] as $contactDetail) {
                                            $options .= '<option value="' . $contactDetail["id"] . '">' . $contactDetail["first_name"] . ' ' . $contactDetail['last_name'] . '</option>';
                                         }
                                         $options .= '</select>';
                                      }
                                  $dats = $options;

                            }else if($dispay == 'email'){
                                    $dats = @$data['supplier_contact'][0]['email'] ? @$data['supplier_contact'][0]['email'] : '';
                            } else if(@$dispay == 'phone_number'){
                                    $dats = @$data['supplier_contact'][0]['phone_number'] ? @$data['supplier_contact'][0]['phone_number'] : '';
                            } else if ($dispay == 'product_id') {
                                $dats = get_vendors_total_product($data['id']);
                             }else {
                                $dats = $data[$key];
                            }
                           
                            $output .= '<td class="col' . $t . '">' . ucfirst(strtolower($dats)) . '</td>';
                        }
                        }
                        $output .= '<td>
                               <a href="' . url('/suppliers/' . $data['id'] . '/edit') . '" class="mr-3 text-secondary row-hover-action" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="mdi mdi-square-edit-outline font-size-18"></i></a>
                                <a href="javascript:void(0);" class="text-secondary row-hover-action delete_single" data-id="' . $data['id'] . '" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="mdi mdi-trash-can font-size-18"></i></a>
                            </td>';
                        $output .= '</tr>';
                        $total_count++;
                        }
                    }else {
                        $i = 1;
                        $counter = $index + 1;
                        $output .=
                            '<tr id="' . $data['id'] . '">
                                <td class="p-2">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input row-checkbox" name="delete_all" data-val="' . $data['id'] . '" value="' . $data['id'] . '" id="customCheck' . $counter . '">
                                        <label class="custom-control-label" for="customCheck' . $counter . '">&nbsp;</label>
                                    </div>
                                </td>

                                <td class="p-2">';
                        if ($check_return == 0) {
                            $output .= '<i class="fas fa-exclamation-triangle" style="color:#F1B44C;" data-toggle="tooltip" title="Missing required settings"></i>';
                        }
                         $output .= '</td>';
                            $newtext = 0;
                            
                            $count = count($display_name);
                            foreach ($display_name as $key => $dispay) {
                                if(!in_array($dispay,$total_display_fileds)){
                                $t = $i + $newtext;
                                $newtext++;
                                if ($dispay == 'country_id') {
                                    $dats = get_country_fullname($data[$key]);
                                } else if ($dispay == 'first_name') {
                                    $options = '';
                                     if (!empty($data['supplier_contact'])) {
                                             $options = '<select name="supplier_contact_' . $data['id'] . '" id="supplier_contact_' . $data['id'] . '" class="form-control border-0 pl-0 bg-transparent" onchange="changeContact(' . $data['id'] . ')">';
                                             foreach ($data['supplier_contact'] as $contactDetail) {
                                                $options .= '<option value="' . $contactDetail["id"] . '">' . $contactDetail["first_name"] . ' ' . $contactDetail['last_name'] . '</option>';
                                             }
                                             $options .= '</select>';
                                          }
                                      $dats = $options;

                                }else if($dispay == 'email'){
                                        $dats = @$data['supplier_contact'][0]['email'] ? @$data['supplier_contact'][0]['email'] : '';
                                } else if(@$dispay == 'phone_number'){
                                        $dats = @$data['supplier_contact'][0]['phone_number'] ? @$data['supplier_contact'][0]['phone_number'] : '';
                                } else if ($dispay == 'product_id') {
                                    $dats = get_vendors_total_product($data['id']);
                                 }else {
                                    $dats = $data[$key];
                                }
                                
                                $output .= '<td class="col' . $t . '">' . ucfirst(strtolower($dats)) . '</td>';
                            }
                            }
                            $output .= '<td>
                                   <a href="' . url('/suppliers/' . $data['id'] . '/edit') . '" class="mr-3 text-secondary row-hover-action" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="mdi mdi-square-edit-outline font-size-18"></i></a>
                                    <a href="javascript:void(0);" class="text-secondary row-hover-action delete_single" data-id="' . $data['id'] . '" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="mdi mdi-trash-can font-size-18"></i></a>
                                </td>';
                            $output .= '</tr>';
                            $total_count++;
                    }
                }
                if($request->toggle_b == 0) {
                    if ($listing_data_count > $limit) {
                        $output_first = '<input type="hidden" value="0" class="checkdeleteall"><input type="hidden" value="' . $listing_data_count . '" class="total_listing_count"><tr class="selected-row-number" style="display:none;"><td colspan="9" class="text-center">Selected <span id="number-of-selected"></span> out of ' . $listing_data_count . '. <a class="text-primary select-all" id="select-all-link" style="cursor: pointer;">Select All</a></td></tr>';
                    } else {
                        $output_first = '<input type="hidden" value="0" class="checkdeleteall"><input type="hidden" value="' . $listing_data_count . '" class="total_listing_count"><tr class="selected-row-number" style="display:none;"><td colspan="9" class="text-center">Selected <span id="number-of-selected"></span> out of ' . $listing_data_count . '. </td></tr>';
                    }
                }else {
                    if($total_count  > $limit){
                        $output_first = '<input type="hidden" value="0" class="checkdeleteall"><input type="hidden" value="' . $total_count . '" class="total_listing_count"><tr class="selected-row-number" style="display:none;"><td colspan="9" class="text-center">Selected <span id="number-of-selected"></span> out of ' . $total_count . '. <a class="text-primary select-all" id="select-all-link" style="cursor: pointer;">Select All</a></td></tr>';
                    }else{
                        $output_first = '<input type="hidden" value="0" class="checkdeleteall"><input type="hidden" value="' . $total_count . '" class="total_listing_count"><tr class="selected-row-number" style="display:none;"><td colspan="9" class="text-center">Selected <span id="number-of-selected"></span> out of ' . $total_count . '. </td></tr>';
                    }
                }
                $output_final = $output_first . $output;
            } else {
                //                $output = '<div align="center" class="alert alert-danger">No record found. </div>';
                $output_final = '<tr class="add-row-tr">
                                    <td colspan="6">
                                        <a href="'. url("suppliers/create") .'" <span class="text-primary1 add-row">
                                            <i class="bx bx-plus-circle font-size-16 align-middle"></i>
                                            Add Contacts
                                        </span>
                                    </td>
                                </tr>';
            }

           
            $res['output'] = $output_final;
            $res['total_count'] = $total_count;
            return response()->json($res);
        }
        if($request->request_type == 'fileds_manage'){
            $global_marketplace = session('MARKETPLACE_ID');
            $total_unchecked_fileds = $request->checked_arr;
            User_field_access::where(array('marketplace_id'=>$global_marketplace,'type'=>2))->delete();
            if(!empty($total_unchecked_fileds)){
                foreach($total_unchecked_fileds as $val){
                    $req_data = [
                        'field_list_id'=>$val,
                        'marketplace_id'=>$global_marketplace,
                        'type'=>2
                    ];
                    User_field_access::create($req_data);
                }
            }
            $res['success'] = 1;
            return response()->json($res);
        }

        if ($request->request_type == 'Get_all_state') {
            $inputs = $request->all();
            $list = get_state_list($inputs['country']);
            return response()->json($list);
            exit;
        }
        if ($request->request_type == 'Get_all_city') {
            $inputs = $request->all();
            $list = get_city_list($inputs['state']);
            return response()->json($list);
            exit;
        }
        if ($request->insert_type == 'insert_update_basic_details') {
            $validator = Validator::make($request->all(), [
                'supplier_name' => 'required',
                'marketplace_id'=>'required',
                'country'=>'required',
                'address_line_1'=>'required',
                'state'=>'required',
                'zipcode'=>'required'
            ]);
            if ($validator->fails()) {
                $res['success'] = false;
                $res['error'] = 1;
                $res['message'] = 'Please provide required field data';
            }
            else {
                $req_data = [
                    'supplier_name' => $request->supplier_name,
                    'user_marketplace_id' => $request->marketplace_id,
                    'country_id' => $request->country,
                    'address_line_1' => $request->address_line_1,
                    'address_line_2' => $request->address_line_2,
                    'city' => ($request->city ? $request->city : ''),
                    'state' => $request->state,
                    'zipcode' => $request->zipcode,
                ];
                
                if ($request->supplier_id == '') {
                   
                    $datas_of = Supplier::create($req_data);
                    $userLastInsertId = $datas_of->toArray();
                    if ($userLastInsertId != '') {
                        $res['supplier_id'] = $userLastInsertId['id'];
                    }
                    //$message = get_messages('supplier created successfully', 1);
                    $res['supplier_id'] = $res['supplier_id'];
                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = "Supplier created successfully";
                } else {
                   
                    $supplier = Supplier::findOrFail($request->supplier_id);
                    $supplier->update($req_data);
                    //$message = get_messages('supplier updated successfully', 1);
                    $res['supplier_id'] = $request->supplier_id;
                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = "Supplier updated successfully";
                    // $res['success'] = true;
                    // $res['message'] = $request->vendor_id;
                }
            }
            return response()->json($res);
        }
        if ($request->insert_type == 'insert_contacts') {
            $res['error'] = 1;
            $res['message'] = '';
            if(empty($request->supplier_id)){
                $res_arr['success'] = false;
                $res_arr['error'] = 1;
                $res_arr['message'] = 'Please fill up basic details';
                return response()->json($res_arr);
            }
            $primary_contact = $request->primary_contacts;
            $res_arr = array();
            $success_count = 0;
            $fail_count = 0;
            $error_msg = array();
            $message = '';
            $res_arr['supplier_id'] = $request->supplier_id;
            $res_arr['success'] = true;
            $res_arr['error'] = 0;
            $mandatory=0;
            // if(!empty($request->contacts)) {
            //     foreach ($request->contacts as $key => $value) {
            //         if ($value['first_name'] != '' && $value['last_name'] != '' && $value['email'] != '' && $value['phone_number'] ) {}
            //         // }
            //         // else{
            //         //     $mandatory=1;
            //         // }
            //     }
            // }
            // if($mandatory==1){
            //     $res_arr['success'] = false;
            //     $res_arr['error'] = 1;
            //     $res_arr['message'] = 'Please provide required field data';
            //     return response()->json($res_arr);
            // }
            // else {
                if (!empty($request->contacts)) {
                    foreach ($request->contacts as $key => $value) {
                        if ($value['first_name'] != '' & $value['last_name'] != '' && $value['email'] != '' && $value['phone_number']) {
                            if ($primary_contact >= 0) {
                                if ($primary_contact == $key)
                                    $primary = 1;
                                else
                                    $primary = 0;
                            } else {
                                if ($key == 0)
                                    $primary = 1;
                                else
                                    $primary = 0;
                            }
                            $req_data = [
                                'first_name' => $value['first_name'],
                                'last_name' => $value['last_name'],
                                'email' => $value['email'],
                                'supplier_id' => $request->supplier_id,
                                'phone_number' => $value['phone_number'],
                                'title' => $value['title'],
                                'primary' => $primary,
                            ];
                            $supplierContactID = '';
                            $check_email = Supplier_contact::where(array('email' => $value['email'], 'supplier_id' => $request->supplier_id))->first();
                            if (empty($check_email)) {
                                $datas_of = Supplier_contact::create($req_data);
                                $userLastInsertId = $datas_of->toArray();
                                if ($userLastInsertId != '') {
                                    $supplierContactID = $userLastInsertId['id'];
                                }
                                $message = 'supplier contact created successfully';
                                $success_count++;
                            } else {
                                $vendorContact = Supplier_contact::findOrFail($check_email['id']);
                                $vendorContact->update($req_data);
                                $message = 'supplier contact updated successfully';
                                $supplierContactID = $check_email['id'];
                                $success_count++;
                            }
                        }
                    }
                    $res_arr['success_count'] = $success_count;
                    $res_arr['failer_count'] = $fail_count;
                    $res_arr['success'] = true;
                    $res_arr['error'] = 0;
                    if ($fail_count == 0) {
                        $res_arr['message'] = $message;
                    } else {
                        $res_arr['message'] = $error_msg;
                    }
                } else {
                    $res_arr['success'] = true;
                    $res_arr['error'] = 0;
                    $res_arr['message'] = '';
                }
           // }
            return response()->json($res_arr);
        }
        if ($request->insert_type == 'insert_update_setting') {
            if(empty($request->supplier_id)){
                $res_arr['success'] = false;
                $res_arr['error'] = 1;
                $res_arr['message'] = 'Please fill up basic details';
                return response()->json($res_arr);
            }
            $req_data = [
                'supplier_id' => $request->supplier_id,
                'lead_time' => $request->lead_time,
                'moq' => $request->moqs,
                'order_volume' => $request->order_volume,
                'Production_Time' => $request->Production_Time,
                'Boat_To_Port' => $request->Boat_To_Port,
                'Port_To_Warehouse' => $request->Port_To_Warehouse,
                'Warehouse_Receipt' => $request->Warehouse_Receipt,
                'CBM_Per_Container' => $request->cbm_per_container,
                'quantity_discount' => $request->quantity_discount,
                'Ship_To_Specific_Warehouse' => $request->warehouse_id,
            ];

            $check_data = Supplier_wise_default_setting::where(array('supplier_id' => $request->supplier_id))->first();
            if (empty($check_data)) {
                $datas_of = Supplier_wise_default_setting::create($req_data);
                $message = get_messages('Supplier default setting created successfully', 1);
                $res['error'] = 0;
                $res['message'] = $message;
            } else {
                $datas_of = Supplier_wise_default_setting::where(array('supplier_id' => $request->supplier_id))->update($req_data);
                $message = get_messages('Supplier default setting updated successfully', 1);
                $res['supplier_id'] = $request->supplier_id;
                $res['error'] = 0;
                $res['message'] = $message;
            }
            return response()->json($res);
        }
        if ($request->insert_type == 'delete_vendors_contact') {
            $id = $request->supplier_contact_id;
            $vendor = Supplier_contact::where(array('id' => $id))->delete();
            //$vendors = Vendor_contact::findOrFail($id);
            //$vendors->delete();
            return json_encode(array('statusCode' => 200));
            
        }
        if ($request->insert_type == 'delete_blackout_date') {
            $id = $request->black_out_id;
            $ids = explode(',', $id);
            $check_date = Supplier_blackout_date::whereIn('id', $ids)->delete();
            return json_encode(array('statusCode' => 200));
            
        }
        if ($request->insert_type == "insert_update_payment") {
            if(empty($request->supplier_id)){
                $res_arr['success'] = false;
                $res_arr['error'] = 1;
                $res_arr['message'] = 'Please fill up basic details';
                return response()->json($res_arr);
            }
            $validator = Validator::make($request->all(), [
                'deposit_per' => 'required',
                'deposit_due' => 'required',
                'deposit_day'=>'required',
                'desposit_term_id'=>'required',
                'balance_per'=>'required',
                'balance_due'=>'required',
                'balance_day'=>'required',
                'balance_term_id'=>'required'
            ]);
            if ($validator->fails()) {
                $res['success'] = false;
                $res['error'] = 1;
                $res['message'] = 'Please provide required field data';
            }
            else {
                $vendors_supplier_type = env('SUPPLIER');
                if ($request->supplier_id != '' && $vendors_supplier_type != '') {

                    $deposit = [
                        'payment_terms' => '1',
                        'percentage' => $request->deposit_per,
                        'dues' => $request->deposit_due,
                        'days_before_after' => $request->deposit_day,
                        'payment_term_type_id' => $request->desposit_term_id,
                    ];

                    $balance = [
                        'payment_terms' => '2',
                        'percentage' => $request->balance_per,
                        'dues' => $request->balance_due,
                        'days_before_after' => $request->balance_day,
                        'payment_term_type_id' => $request->balance_term_id,
                    ];

                    $check_exists = Payment::where(array('supplier_vendor' => $vendors_supplier_type, 'supplier_vendor_id' => $request->supplier_id))->first();
                    if (!empty($check_exists)) {
                        $payment_id = $check_exists->id;
                        if ($payment_id != '') {
                            $get_desosit_data = Payment_details::where(array('payment_terms' => '1', 'payment_id' => $payment_id))->first();
                            $get_balance_data = Payment_details::where(array('payment_terms' => '2', 'payment_id' => $payment_id))->first();
                            if (!empty($get_desosit_data)) {
                                Payment_details::where(array('id' => $get_desosit_data->id))->update($deposit);
                            } else {
                                $deposit['payment_id'] = $payment_id;
                                Payment_details::create($deposit);
                            }

                            if (!empty($get_balance_data)) {
                                Payment_details::where(array('id' => $get_balance_data->id))->update($balance);
                            } else {
                                $balance['payment_id'] = $payment_id;
                                Payment_details::create($balance);
                            }
                        }
                    } else {
                        $payment = [
                            'supplier_vendor_id' => $request->supplier_id,
                            'supplier_vendor' => $vendors_supplier_type,
                        ];
                        $payment_data = Payment::create($payment);
                        $payment_id = $payment_data->id;
                        $deposit['payment_id'] = $payment_id;
                        $balance['payment_id'] = $payment_id;
                        Payment_details::create($deposit);
                        Payment_details::create($balance);
                    }
                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = "Payment details updated successfully";
                    $res['supplier_id'] = $request->vendors_supplier_id;

                } else {
                    $res['success'] = false;
                    $res['error'] = 1;
                    $res['message'] = 'Supplier not found';
                }
            }
            return response()->json($res);
        }
        if ($request->insert_type == 'remove_vendors_blackout_date') {
            $id = $request->id;
            $vendor = Supplier_blackout_date::where(array('id' => $id))->delete();
            return json_encode(array('statusCode' => 200));
        }
        if ($request->insert_type == 'insert_update_backout_date') {
            if(empty($request->supplier_id)){
                $res_arr['success'] = false;
                $res_arr['error'] = 1;
                $res_arr['message'] = 'Please fill up basic details';
                return response()->json($res_arr);
            }
            $events = $request->events;
            $supplier_id = $request->supplier_id;
            $mandatory=0;
            if(!empty($events)) {
                foreach ($events as $key => $val) {
                    if ($val['name'] != '' && $val['start_date'] != '' && $val['end_date'] != '') {
                        if ((strtotime($val['start_date'])) > (strtotime($val['end_date'])))
                        {
                            $mandatory=2;
                        }
                    }
                    else{
                        $mandatory=1;
                    }
                }
            }
            if($mandatory==1){
                $res['success'] = false;
                $res['error'] = 1;
                $res['message'] = 'Please provide required field data';
            }
            else if($mandatory==2){
                $res['success'] = false;
                $res['error'] = 1;
                $res['message'] = 'Start date should be less than end date';
            }
            else {
                if (!empty($events)) {
                    if ($supplier_id != '') {
                        $delete = Supplier_blackout_date::where(array('supplier_id' => $supplier_id))->delete();
                    }
                    foreach ($events as $key => $val) {
                        if ($val['name'] != '' && $val['start_date'] != '' && $val['end_date'] != '') {

                            $date1 = date_create(date("Y-m-d", strtotime($val['start_date'])));
                            $date2 = date_create(date("Y-m-d", strtotime($val['end_date'])));
                            $diff = date_diff($date1, $date2);
                            $days_diff = $diff->format("%a");

                            $req_data = [
                                'supplier_id' => $supplier_id,
                                'event_name' => $val['name'],
                                'start_date' => date("Y-m-d", strtotime($val['start_date'])),
                                'end_date' => date("Y-m-d", strtotime($val['end_date'])),
                                'number_of_days' => $days_diff,
                                'type' => $val['type'],
                            ];
                            $create = Supplier_blackout_date::create($req_data);
                            $res['success'] = true;
                            $res['error'] = 0;
                            $res['message'] = "Supplier blackout date created successfully";
                            $res['supplier_id'] = $supplier_id;
                        } else {
                            $res['success'] = false;
                            $res['error'] = 0;
                            $res['message'] = "All fields must be required";
                            $res['$supplier_id'] = $supplier_id;
                        }
                    }
                }
            }
            return response()->json($res);
        }
        if ($request->request_type == 'suppliers_list') {
            $data = Supplier::where(array('user_marketplace_id' => $request->id))->get()->toArray();
            $suppliers_data = array();
            if (!empty($data)) {
                foreach ($data as $key => $post) {
                    $cnname = "";
                    if (isset($post['country_id']) && $post['country_id'] != "") {
                        $cnname = get_country_fullname($post['country_id']);
                    }

                    $action = '';
                    $nestedData['supplier_name'] = $post['supplier_name'];
                    $nestedData['zipcode'] = $post['zipcode'];
                    $nestedData['country'] = $cnname;
                    $nestedData['user_marketplace_id'] = $post['user_marketplace_id'];
                    $action .= '<ul class="d-flex flex-row align-items-center table-right-actions">
                    <li>
                        <a href="' . route('suppliers.edit', $post['id']) . '" ><img src="asset/images/svg/edit.svg" alt=""></a>
                    </li>
                    <li class="px-2">
                        <a href="#" ><img src="asset/images/svg/basket.svg" alt="" height="15.13"></a>
                    </li>
                    <li>
                        <img src="asset/images/svg/polygon.svg" alt="">
                    </li>
                </ul>';
                    $nestedData['action'] = $action;
                    $suppliers_data[] = $nestedData;
                }
            }
            echo json_encode($suppliers_data);
            exit;
        }
        if ($request->request_type == 'get_pagi_country') {
            $global_marketplace = session('MARKETPLACE_ID');
            $pagination_left = '';
            $pagination_right = '';
            $pagination = array();
            $success = 0;
            $pagination_slot = $request->pagination_slot;
            if ($global_marketplace != '') {
                $whereArray = [
                    "user_marketplace_id" => $global_marketplace
                ];
                if($request->get_country){
                    $whereArray = [
                        "user_marketplace_id" => $global_marketplace,
                        "country_id" => $request->get_country
                    ];
                }
                if($request->search != ''){
                    $whereArray[] = [
                        'supplier_name', 'LIKE', "%{$request->search}%"
                    ];
                }
                if($request->toggle_b == 0) {
                    $data_count = Supplier::with(['supplier_contact', 'supplier_product'])->where($whereArray)->count();
                }else{
                    $listing_data = Supplier::with(['supplier_contact'=> function ($query) {
                        $query->orderBy('primary', 'desc');
                    }, 'supplier_product'])->where($whereArray)->get()->toArray();
                    $data_count = 0;
                    foreach ($listing_data as $index => $data) {
                        $suppliers_contact = Supplier_contact::where(array('supplier_id' => $data['id']))->first();
                        $suppliers_blackoutdate = Supplier_blackout_date::where(array('supplier_id' => $data['id']))->first();
                        $lead_time = Lead_time::where(array('supplier_vendor' => env('SUPPLIER'), 'supplier_vendor_id' => $data['id']))->first();
                        $order_setting = Reorder_schedule_detail::where(array('supplier_vendor' => env('SUPPLIER'), 'supplier_vendor_id' => $data['id']))->first();
                        $payment = Payment::where(array('supplier_vendor' => '2', 'supplier_vendor_id' => $data['id']))->first();
                        $shipping = Shipping::where(array('supplier_vendor' => env('SUPPLIER'), 'supplier_vendor_id' => $data['id']))->first();
                        if (empty($suppliers_contact) || empty($suppliers_blackoutdate) || empty($lead_time) || empty($order_setting) || empty($payment) || empty($shipping)) {
                            $data_count++;
                        }
                    }
                }
                $keyj = 1;
                if ($data_count > 0) {
                    for ($i = 1; $i <= $data_count; $i = $i + $pagination_slot) {
                        if ($i == 1) {
                            $newi = $i;
                        } else {
                            $newi = $i - 1;
                        }
                        $n = $i + ($pagination_slot - 1);
                        //$pagination[$keyj]= $newi.'-'.$n;
                        $pagination[$keyj] = ($newi) . '-' . $n;
                        $keyj++;
                    }
                } else {
                    $newi = 1;
                    $n = $newi + ($pagination_slot - 1);
                    $pagination[$newi] = ($newi) . '-' . $n;

                }
                $pagi = ($request->pagecount ? $request->pagecount : '');
                $success = 1;
              //  $pagination_left = get_pagination_view($pagination);
                $pagination_right = get_pagination_right_view($pagination, $data_count,$pagination_slot, $pagi);
            }
            $res['success'] = $success;
            //$res['select_pagi'] = $pagination_left;
            $res['paginations'] = $pagination_right;
            $res['count'] = $data_count;
            return response()->json($res);
            exit;
            // echo json_encode($res); exit;
        }
        if ($request->insert_type == 'delete_multiple_records') {
            Supplier::whereIn('id', explode(",", $request->delete_array))->delete();
            return json_encode(array('statusCode' => 200));
        }
        if ($request->insert_type == 'create_shipping_agent') {
            $req_data = [
                'user_marketplace_id' => $request->marketplace_id,
                'company_name' => $request->company_name,
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'phone' => $request->phone,
                'country_id' => ($request->country ? $request->country : 1)
            ];

            $datas_of = ShippingAgent::create($req_data);
            $userLastInsertId = $datas_of->toArray();
            if ($userLastInsertId != '') {
                $res['shipping_agent_id'] = $userLastInsertId['id'];
            }
            $res['shipping_agent_array'] = ShippingAgent::where('user_marketplace_id',$request->marketplace_id)->get()->toArray();
            $message = 'Shipping Agent created successfully';
            $res['error'] = 0;
            $res['message'] = $message;
            return response()->json($res);
        }
        if ($request->insert_type == 'insert_update_shipping_details') {
           
            if(empty($request->supplier_id)){
                $res_arr['success'] = false;
                $res_arr['error'] = 1;
                $res_arr['message'] = 'Please fill up basic details';
                return response()->json($res_arr);
            }
            $req_data_shipping = [
                'supplier_vendor' => env('SUPPLIER'),
                'supplier_vendor_id' => $request->supplier_id,
                'logistic_type_id' => $request->logistic_type,
                'shipping_agent_id' => $request->shipping_agent,
                'lcl_cost_settings' => ($request->lcl_cost_for == 'on' ? 1 : 0),
                'lcl_cost_per' => $request->lcl_cost_per, //kg/cbm
                'lcl_cost' => $request->lcl_cost,
                'duties_cost' => $request->duties_cost_percentage,
                'direct_to_amazon' => $request->direct_to_amazon
            ];
            $shipping_id = Shipping::where('supplier_vendor', env('SUPPLIER'))->where('supplier_vendor_id',$request->supplier_id)->get()->first();
            if(!empty($shipping_id))
                $shipping_id = $shipping_id->id;
            if (empty($shipping_id)) {
                $datas_of = Shipping::create($req_data_shipping);
                $userLastInsertId = $datas_of->toArray();
                $shipping_id = $userLastInsertId['id'];
            } else {
                $shippingData = Shipping::findOrFail($shipping_id);
                $shippingData->update($req_data_shipping);
            }
            if (!empty($shipping_id)) {
                $req_data_container_20ft = [
                    'shipping_id' => $shipping_id,
                    'container_size' => '20ft',
                    'select_container' => $request->twentyft_checkbox,
                    'cbm_per_container' => $request->twentyft_cost_per,
                    'container_cost' => $request->twentyft_cost,
                ];
                $check20ftContainer = ShippingContainerSettings::where('shipping_id', $shipping_id)->where('container_size', '20ft')->get()->first();
                if ($check20ftContainer == '') {
                    ShippingContainerSettings::create($req_data_container_20ft);
                } else {
                    $check20ftContainer->update($req_data_container_20ft);
                }
                $req_data_container_40ft = [
                    'shipping_id' => $shipping_id,
                    'container_size' => '40ft',
                    'select_container' => $request->fourtyft_checkbox,
                    'cbm_per_container' => $request->fourtyft_cost_per,
                    'container_cost' => $request->fourtyft_cost,
                ];
                $check40ftContainer = ShippingContainerSettings::where('shipping_id', $shipping_id)->where('container_size', '40ft')->get()->first();
                if ($check20ftContainer == '') {
                    ShippingContainerSettings::create($req_data_container_40ft);
                } else {
                    $check40ftContainer->update($req_data_container_40ft);
                }
                $req_data_container_40hq = [
                    'shipping_id' => $shipping_id,
                    'container_size' => '40hq',
                    'select_container' => $request->fourtyhq_checkbox,
                    'cbm_per_container' => $request->fourtyhq_cost_per,
                    'container_cost' => $request->fourtyhq_cost,
                ];
                $check40hqContainer = ShippingContainerSettings::where('shipping_id', $shipping_id)->where('container_size', '40hq')->get()->first();
                if ($check20ftContainer == '') {
                    ShippingContainerSettings::create($req_data_container_40hq);
                } else {
                    $check40hqContainer->update($req_data_container_40hq);
                }
                $req_data_container_45hq = [
                    'shipping_id' => $shipping_id,
                    'container_size' => '45hq',
                    'select_container' => $request->fourtyfivehq_checkbox,
                    'cbm_per_container' => $request->fourtyfivehq_cost_per,
                    'container_cost' => $request->fourtyfivehq_cost,
                ];
                $check45hqContainer = ShippingContainerSettings::where('shipping_id', $shipping_id)->where('container_size', '45hq')->get()->first();
                if ($check20ftContainer == '') {
                    ShippingContainerSettings::create($req_data_container_45hq);
                } else {
                    $check45hqContainer->update($req_data_container_45hq);
                }
                $shippingAirPrice = $request->shipping_air;
                //                        $shippingAirMessureIn = $request->shipping_air_messure_ins;
                //                        $shippingAirTypes = $request->shipping_air_type;
                if (!empty($shippingAirPrice)) {
                    foreach ($shippingAirPrice as $key => $shippingAir) {
                        $req_data_shipping = [
                            'shipping_id' => $shipping_id,
                            'ship_type' => $shippingAir['type'],
                            'price' => $shippingAir['price'],
                            'messuare_in' => $shippingAir['messure_in']
                        ];
                        $shippingAirCheck = Shippingair::where('shipping_id', $shipping_id)->where('ship_type', $shippingAir['type'])->where('messuare_in', $shippingAir['messure_in'])->get()->first();
                        if ($shippingAirCheck == '') {
                            Shippingair::create($req_data_shipping);
                        } else {
                            $shippingAirCheck->update($req_data_shipping);
                        }
                    }
                }
                $warehouses = $request->warehouses;
                if (!empty($warehouses)) {
                    ShipToWarehouse::where('shipping_id', $shipping_id)->delete();
                    foreach ($warehouses as $warehouse) {
                        $req_data_warehouse = [
                            'shipping_id' => $shipping_id,
                            'warehouse_value' => $warehouse
                        ];
                        ShipToWarehouse::create($req_data_warehouse);
                    }
                }
            }
            if (!empty($shipping_id)) {
                $message = 'Shipping created successfully';
                $res['success'] = true;
                $res['error'] = 0;
                $res['message'] = $message;
            } else {
                $message = 'Shipping updated successfully';
                $res['shipping_id'] = $shipping_id;
                $res['success'] = true;
                $res['error'] = 0;
                $res['message'] = $message;
            }
            return response()->json($res);
        }
        if ($request->insert_type == 'insert_update_lead_time') {
           
            if(empty($request->supplier_id)){
                $res_arr['success'] = false;
                $res_arr['error'] = 1;
                $res_arr['message'] = 'Please fill up basic details';
                return response()->json($res_arr);
            }
            if(isset($request->total_lead_time_days_boat)){
                $validator = Validator::make($request->all(), [
                    'product_manuf_days_boat' => 'required',
                    'to_port_days_boat' => 'required',
                    'transit_time_days_boat'=>'required',
                    'to_warehouse_days_boat'=>'required',
                    'to_amazon_boat'=>'required',
                    'po_to_production_days_boat'=>'required',
                    'safety_days_boat'=>'required',
                    'total_lead_time_days_boat'=>'required'
                ]);
                if ($validator->fails()) {
                    $res['success'] = false;
                    $res['error'] = 1;
                    $res['message'] = 'Please provide required field data';
                    return response()->json($res);
                }
                else {
                    $product_manuf_days = isset($request->product_manuf_days_boat) ? $request->product_manuf_days_boat : 0;
                    $to_port_days = isset($request->to_port_days_boat) ? $request->to_port_days_boat : 0;
                    $transit_time_days = isset($request->transit_time_days_boat) ? $request->transit_time_days_boat : 0;
                    $to_warehouse_days = isset($request->to_warehouse_days_boat) ? $request->to_warehouse_days_boat : 0;
                    $to_amazon = isset($request->to_amazon_boat) ? $request->to_amazon_boat : 0;
                    $po_to_production_days = isset($request->po_to_production_days_boat) ? $request->po_to_production_days_boat : 0;
                    $safety_days = isset($request->safety_days_boat) ? $request->safety_days_boat : 0;
                    $total_lead_time_days = isset($request->total_lead_time_days_boat) ? $request->total_lead_time_days_boat : 0;
                    $order_prep_days = isset($request->order_prep_days_boat) ? $request->order_prep_days_boat : 0;
                    $po_to_prep_days = isset($request->po_to_prep_days_boat) ? $request->po_to_prep_days_boat : 0;
                    $type = "1";
                }
            }else if(isset($request->total_lead_time_days_plane)){
                $validator = Validator::make($request->all(), [
                    'product_manuf_days_plane' => 'required',
                    'to_port_days_plane' => 'required',
                    'transit_time_days_plane'=>'required',
                    'to_warehouse_days_plane'=>'required',
                    'to_amazon_plane'=>'required',
                    'po_to_production_days_plane'=>'required',
                    'safety_days_plane'=>'required',
                    'total_lead_time_days_plane'=>'required'
                ]);
                if ($validator->fails()) {
                    $res['success'] = false;
                    $res['error'] = 1;
                    $res['message'] = 'Please provide required field data';
                    return response()->json($res);
                }
                else {
                    $product_manuf_days = isset($request->product_manuf_days_plane) ? $request->product_manuf_days_plane : 0;
                    $to_port_days = isset($request->to_port_days_plane) ? $request->to_port_days_plane : 0;
                    $transit_time_days = isset($request->transit_time_days_plane) ? $request->transit_time_days_plane : 0;
                    $to_warehouse_days = isset($request->to_warehouse_days_plane) ? $request->to_warehouse_days_plane : 0;
                    $to_amazon = isset($request->to_amazon_plane) ? $request->to_amazon_plane : 0;
                    $po_to_production_days = isset($request->po_to_production_days_plane) ? $request->po_to_production_days_plane : 0;
                    $safety_days = isset($request->safety_days_plane) ? $request->safety_days_plane : 0;
                    $total_lead_time_days = isset($request->total_lead_time_days_plane) ? $request->total_lead_time_days_plane : 0;
                    $order_prep_days = isset($request->order_prep_days_plane) ? $request->order_prep_days_plane : 0;
                    $po_to_prep_days = isset($request->po_to_prep_days_plane) ? $request->po_to_prep_days_plane : 0;
                    $type = "2";
                }
            }
            $req_data = [
                'supplier_vendor' => env('SUPPLIER'),
                'supplier_vendor_id' => $request->supplier_id,
                'product_manuf_days' => $product_manuf_days,
                'to_port_days' => $to_port_days,
                'transit_time_days' => $transit_time_days,
                'to_warehouse_days' => $to_warehouse_days,
                'to_amazon' => $to_amazon,
                'po_to_production_days' => $po_to_production_days,
                'safety_days' => $safety_days,
                'total_lead_time_days' => $total_lead_time_days,
                'order_prep_days' => $order_prep_days,
                'po_to_prep_days' => $po_to_prep_days,
                'type' => $type
            ];

            $check_exists = Lead_time::where(array('supplier_vendor' => env('SUPPLIER'), 'supplier_vendor_id' => $request->supplier_id))->first();
            if (!empty($check_exists)) {
                Lead_time::where(array('id' => $check_exists->id))->update($req_data);
                //$message = get_messages('vendor lead time updated successfully', 1);
                $res['supplier_id'] = $request->supplier_id;
                $res['error'] = 0;
                $res['success'] = true;
                $res['message'] = "supplier lead time updated successfully";
                $res['lead_time_id'] = $check_exists->id;
            } else {
                $lead_data = Lead_time::create($req_data);
                //$message = get_messages('vendor lead time created successfully', 1);
                $res['supplier_id'] = $request->supplier_id;
                $res['error'] = 0;
                $res['success'] = true;
                $res['message'] = "supplier lead time created successfully";
                $res['lead_time_id'] = $lead_data->id;
            }
            if ($res['lead_time_id'] > 0) {
                Lead_time_value::where(array('lead_time_id' => $res['lead_time_id']))->delete();
                    if($type==1 || $type==2){
                        if(isset($request->emailing['production'])){

                            foreach ($request->emailing['production'] as $key=>$production){
                                $lead_time_value_data = [
                                    'lead_time_id' => $res['lead_time_id'],
                                    'lead_time_detail_id' => $key+1,
                                    'send_mail' => '1',
                                    'no_of_days' => @$production['days'],
                                    'contact_detail' => (isset($production['email'])) && is_array($production['email']) ? implode(",", array_unique(@$production['email'])) : '',
                                ];
                              
                                if(is_array(@$production['email'])){
                                    Lead_time_value::create($lead_time_value_data);
                                }

                            }
                        }
                        if(isset($request->emailing['order_prep'])){
                            foreach ($request->emailing['order_prep'] as $key=>$production){
                                $lead_time_value_data = [
                                    'lead_time_id' => $res['lead_time_id'],
                                    'lead_time_detail_id' => $key+1,
                                    'send_mail' => '1',
                                    'no_of_days' => @$production['days'],
                                    'contact_detail' => (isset($production['email'])) && is_array($production['email']) ? implode(",", array_unique(@$production['email'])) : '',
                                ];
                                
                                if(is_array(@$production['email'])){
                                    Lead_time_value::create($lead_time_value_data);
                                }
                                //Lead_time_value::create($lead_time_value_data);
                            }
                        }
                        if(isset($request->emailing['port_departure'])){
                            foreach ($request->emailing['port_departure'] as $key=>$port_departure){
                                $lead_time_value_data = [
                                    'lead_time_id' => $res['lead_time_id'],
                                    'lead_time_detail_id' => $key+3,
                                    'send_mail' => '1',
                                    'no_of_days' => @$port_departure['days'],
                                    'contact_detail' => (isset($port_departure['email'])) && is_array($port_departure['email']) ? implode(",", array_unique(@$port_departure['email'])) : '',
                                ];
                                
                                if(is_array(@$port_departure['email'])){
                                    Lead_time_value::create($lead_time_value_data);
                                }
                                //Lead_time_value::create($lead_time_value_data);
                            }
                        }
                        if(isset($request->emailing['in_transit'])){
                            foreach ($request->emailing['in_transit'] as $key=>$in_transit){
                                $lead_time_value_data = [
                                    'lead_time_id' => $res['lead_time_id'],
                                    'lead_time_detail_id' => $key+4,
                                    'send_mail' => '1',
                                    'no_of_days' => @$in_transit['days'],
                                    'contact_detail' => (isset($in_transit['email'])) && is_array($in_transit['email']) ? implode(",", array_unique(@$in_transit['email'])) : '',
                                ];
                                if(is_array(@$in_transit['email'])){
                                    Lead_time_value::create($lead_time_value_data);
                                }
                                //Lead_time_value::create($lead_time_value_data);
                            }
                        }
                        if(isset($request->emailing['to_warehouse'])){
                            foreach ($request->emailing['to_warehouse'] as $key=>$to_warehouse){
                                $lead_time_value_data = [
                                    'lead_time_id' => $res['lead_time_id'],
                                    'lead_time_detail_id' => $key+5,
                                    'send_mail' => '1',
                                    'no_of_days' => @$to_warehouse['days'],
                                    'contact_detail' => (isset($to_warehouse['email'])) && is_array($to_warehouse['email']) ? implode(",", array_unique(@$to_warehouse['email'])) : '',
                                ];
                                if(is_array(@$to_warehouse['email'])){
                                    Lead_time_value::create($lead_time_value_data);
                                }
                                //Lead_time_value::create($lead_time_value_data);
                            }
                        }
                    }
                    else{
                        if(isset($request->emailing['order_prep'])) {
                            foreach ($request->emailing['order_prep'] as $key => $order_prep) {
                                $lead_time_value_data = [
                                    'lead_time_id' => $res['lead_time_id'],
                                    'lead_time_detail_id' => $key + 8,
                                    'send_mail' => '1',
                                    'no_of_days' => @$order_prep['days'],
                                    'contact_detail' => (isset($order_prep['email'])) && is_array($order_prep['email']) ?  implode(",", array_unique(@$order_prep['email'])) : '',
                                ];
                                if(is_array(@$order_prep['email'])){
                                    Lead_time_value::create($lead_time_value_data);
                                }
                                //Lead_time_value::create($lead_time_value_data);
                            }
                        }
                        if(isset($request->emailing['ship_confirmation'])){
                            foreach ($request->emailing['ship_confirmation'] as $key=>$ship_confirm){
                                $lead_time_value_data = [
                                    'lead_time_id' => $res['lead_time_id'],
                                    'lead_time_detail_id' => $key+9,
                                    'send_mail' => '1',
                                    'no_of_days' => @$ship_confirm['days'],
                                    'contact_detail' => (isset($ship_confirm['email'])) && is_array($ship_confirm['email']) ?  implode(",", array_unique(@$ship_confirm['email'])) : '',
                                ];
                                if(is_array(@$ship_confirm['email'])){
                                    Lead_time_value::create($lead_time_value_data);
                                }
                               // Lead_time_value::create($lead_time_value_data);
                            }
                        }
                        if(isset($request->emailing['to_warehouse'])){
                            foreach ($request->emailing['to_warehouse'] as $key=>$to_warehouse){
                                $lead_time_value_data = [
                                    'lead_time_id' => $res['lead_time_id'],
                                    'lead_time_detail_id' => $key+10,
                                    'send_mail' => '1',
                                    'no_of_days' => @$to_warehouse['days'],
                                    'contact_detail' => (isset($to_warehouse['email'])) && is_array($to_warehouse['email']) ? implode(",", array_unique(@$to_warehouse['email'])) : '',
                                ];
                                if(is_array(@$to_warehouse['to_warehouse'])){
                                    Lead_time_value::create($lead_time_value_data);
                                }
                                //Lead_time_value::create($lead_time_value_data);
                            }
                        }
                    }
                /*if (!empty($request->arr_test)) {
                    foreach ($request->arr_test as $key => $data) {
                        if ($data['class'] == 'boat_production_mail_1') $class_id = '1';
                        if ($data['class'] == 'boat_production_mail_2') $class_id = '2';
                        if ($data['class'] == 'boat_port_departure') $class_id = '3';
                        if ($data['class'] == 'boat_in_transist') $class_id = '4';
                        if ($data['class'] == 'boat_to_warehouse_1') $class_id = '5';
                        if ($data['class'] == 'boat_to_warehouse_2') $class_id = '6';
                        if ($data['class'] == 'boat_to_warehouse_3') $class_id = '7';

                        if ($data['class'] == 'plane_production_mail_1') $class_id = '1';
                        if ($data['class'] == 'plane_production_mail_2') $class_id = '2';
                        if ($data['class'] == 'plane_port_departure') $class_id = '3';
                        if ($data['class'] == 'plane_in_transist') $class_id = '4';
                        if ($data['class'] == 'plane_to_warehouse_1') $class_id = '5';
                        if ($data['class'] == 'plane_to_warehouse_2') $class_id = '6';
                        if ($data['class'] == 'plane_to_warehouse_3') $class_id = '7';

                        if ($data['class'] == 'demostic_order_prep') $class_id = '8';
                        if ($data['class'] == 'domestic_ship_confirmation') $class_id = '9';
                        if ($data['class'] == 'domestic_to_warehouse') $class_id = '10';

                        if(isset($data['contact']) && !empty($data['contact'])){
                            $req_data = [
                                'lead_time_id' => $res['lead_time_id'],
                                'lead_time_detail_id' => $class_id,
                                'send_mail' => '1',
                                'no_of_days' => @$data['days'],
                                'contact_detail' => implode(",", $data['contact']),
                            ];
                        }
                        Lead_time_value::create($req_data);
                    }
                }*/

            }
            return response()->json($res);

          

            // if(empty($request->supplier_id)){
            //     $res_arr['success'] = false;
            //     $res_arr['error'] = 1;
            //     $res_arr['message'] = 'Please fill up basic details';
            //     return response()->json($res_arr);
            // }
            // $req_data = [
            //     'supplier_vendor'=>env('SUPPLIER'),
            //     'supplier_vendor_id' => $request->supplier_id,
            //     'product_manuf_days' => isset($request->product_manuf_days) ? $request->product_manuf_days :0,
            //     'to_port_days' => isset($request->to_port_days) ? $request->to_port_days : 0,
            //     'transit_time_days' => isset($request->transit_time_days) ? $request->transit_time_days : 0,
            //     'to_warehouse_days' => isset($request->to_warehouse_days) ? $request->to_warehouse_days : 0,
            //     'to_amazon' => isset($request->to_amazon) ? $request->to_amazon : '',
            //     'po_to_production_days' => isset($request->po_to_production_days) ? $request->po_to_production_days :0,
            //     'safety_days' => isset($request->safety_days) ? $request->safety_days : 0,
            //     'total_lead_time_days' =>isset($request->total_lead_time_days) ? $request->total_lead_time_days :0,
            //     'order_prep_days' => isset($request->order_prep_days) ? $request->order_prep_days :0,
            //     'po_to_prep_days' => isset($request->po_to_prep_days) ? $request->po_to_prep_days : 0,
            //     'type'=>isset($request->type) ? $request->type : 0
            // ];
            // $check_exists = Lead_time::where(array('supplier_vendor'=>env('SUPPLIER'),'supplier_vendor_id'=>$request->supplier_id))->first();
            // if(!empty($check_exists)){
            //     Lead_time::where(array('id'=>$check_exists->id))->update($req_data);
            //     $message = get_messages('supplier lead time updated successfully', 1);
            //     $res['vendor_id'] = $request->supplier_id;
            //     $res['error'] = 0;
            //     $res['message'] = $message;
            //     $res['lead_time_id'] = $check_exists->id;
            // }else{
            //     $lead_data = Lead_time::create($req_data);
            //     $message = get_messages('supplier lead time created successfully', 1);
            //     $res['error'] = 0;
            //     $res['message'] = $message;
            //     $res['lead_time_id']= $lead_data->id;
            // }
            // if($res['lead_time_id'] >0 ){
            //     Lead_time_value::where(array('lead_time_id'=>$res['lead_time_id']))->delete();
            //     if(!empty($request->arr_test)){
            //         foreach($request->arr_test as $key=>$data){
            //             if($data['class'] == 'boat_production_mail_1') $class_id = '1';
            //             if($data['class'] == 'boat_production_mail_2') $class_id = '2';
            //             if($data['class'] == 'boat_port_departure') $class_id = '3';
            //             if($data['class'] == 'boat_in_transist') $class_id = '4';
            //             if($data['class'] == 'boat_to_warehouse_1') $class_id = '5';
            //             if($data['class'] == 'boat_to_warehouse_2') $class_id = '6';
            //             if($data['class'] == 'boat_to_warehouse_3') $class_id = '7';

            //             if($data['class'] == 'plane_production_mail_1') $class_id = '1';
            //             if($data['class'] == 'plane_production_mail_2') $class_id = '2';
            //             if($data['class'] == 'plane_port_departure') $class_id = '3';
            //             if($data['class'] == 'plane_in_transist') $class_id = '4';
            //             if($data['class'] == 'plane_to_warehouse_1') $class_id = '5';
            //             if($data['class'] == 'plane_to_warehouse_2') $class_id = '6';
            //             if($data['class'] == 'plane_to_warehouse_3') $class_id = '7';

            //             if($data['class'] == 'demostic_order_prep') $class_id = '8';
            //             if($data['class'] == 'domestic_ship_confirmation') $class_id = '9';
            //             if($data['class'] == 'domestic_to_warehouse') $class_id = '10';

            //             if(isset($data['contact']) && !empty($data['contact'])){
            //                 $req_data = [
            //                     'lead_time_id' => $res['lead_time_id'],
            //                     'lead_time_detail_id'=>$class_id,
            //                     'send_mail' => '1',
            //                     'no_of_days' => @$data['days'],
            //                     'contact_detail' => implode(",",$data['contact']),
            //                 ];
            //             }
            //             Lead_time_value::create($req_data);
            //         }
            //     }

            // }
            // return response()->json($res);
        }
        if ($request->insert_type == 'insert_update_order_setting') {
            $message = get_messages('Something wrong', 0);
            $res['success'] = false;
            $res['message'] = $message;
            if(empty($request->supplier_id)){
                $res_arr['success'] = false;
                $res_arr['error'] = 1;
                $res_arr['message'] = 'Please fill up basic details';
                return response()->json($res_arr);
            }
            $res['supplier_id'] = $request->supplier_id;

            if($request->reorder_schedule == 'on_demand'){
                $validator = Validator::make($request->all(), [
                    'order_volume' => 'required',
                    'order_volume_type'=>'required'
                ]);
            }else{
                $validator = Validator::make($request->all(), [
                    'order_volume' => 'required',
                    'order_volume_type'=>'required',
                    'days'=>'required'
                ]);
            }
            if ($validator->fails()) {
                $res['success'] = false;
                $res['error'] = 1;
                $res['message'] = 'Please provide required field data';
                return response()->json($res);
            }
            else {
                
                $typeArray = ['on_demand'=>0 ,'weekly' => 1, 'bi_weekly' => 2, 'monthly' => 3, 'bi_monthly' => 4];
                $check_exists = Lead_time::where(array('supplier_vendor' => env('SUPPLIER'), 'supplier_vendor_id' => $request->supplier_id))->first();
                if (!empty($check_exists)) {
                    $req_data = [
                        'order_volume_value' => $request->order_volume,
                        'order_volume_id' => $request->order_volume_type
                    ];
                    Lead_time::where(array('id' => $check_exists->id))->update($req_data);
                    //$message = get_messages('vendor order setting updated successfully', 1);
                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = "Supplier order setting updated successfully";
                    $res['lead_time_id'] = $check_exists->id;
                    $res['order_volume'] = $request->order_volume;
                    $res['supplier_id'] = $request->supplier_id;
                } else {
                    $res['success'] = false;
                    $res['error'] = 1;
                    $res['message'] = 'Please first complete lead time details';
                    return response()->json($res);

                    $supplier = Supplier::where('id', $request->supplier_id)->first();
                    $req_data = [
                        'supplier_vendor' => env('SUPPLIER'),
                        'supplier_vendor_id' => $request->supplier_id,
                        'type' =>  1,
                        'order_volume_value' => $request->order_volume,
                        'order_volume_id' => $request->order_volume_type
                    ];
                    $lead_data = Lead_time::create($req_data);
                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = "supplier order setting created successfully";
                    $res['lead_time_id'] = $lead_data->id;
                    $res['supplier_id'] = $request->supplier_id;
                }
                
                if (!empty($request->days)) {
                    Reorder_schedule_detail::where(array('supplier_vendor' => env('SUPPLIER'), 'supplier_vendor_id' => $request->supplier_id))->delete();
                    foreach ($request->days as $schedule_type) {
                        if (is_numeric($schedule_type)) {
                            $values_d = $schedule_type;
                        } else {
                            if ($schedule_type == 'Sun') $values_d = 1;
                            if ($schedule_type == 'Mon') $values_d = 2;
                            if ($schedule_type == 'Tue') $values_d = 3;
                            if ($schedule_type == 'Wed') $values_d = 4;
                            if ($schedule_type == 'Thu') $values_d = 5;
                            if ($schedule_type == 'Fri') $values_d = 6;
                            if ($schedule_type == 'Sat') $values_d = 7;
                        }
                        $reorder_schedule = ['supplier_vendor' => env('SUPPLIER'), 'supplier_vendor_id' => $request->supplier_id, 'reorder_schedule_type_id' => $typeArray[$request->reorder_schedule], 'schedule_days' => $values_d];
                        $reorder_schedule_data = Reorder_schedule_detail::create($reorder_schedule);
                    }
                }else{
                    
                    Reorder_schedule_detail::where(array('supplier_vendor' => env('SUPPLIER'), 'supplier_vendor_id' => $request->supplier_id))->delete();
                    $reorder_schedule = ['supplier_vendor' => env('SUPPLIER'), 'supplier_vendor_id' => $request->supplier_id, 'reorder_schedule_type_id' => $typeArray[$request->reorder_schedule]];
                                       $reorder_schedule_data = Reorder_schedule_detail::create($reorder_schedule);
                }
            }
            return response()->json($res);
        }
    }

    function displayDates($date1, $date2, $format = 'Y-m-d')
    {
        $dates = array();
        $current = strtotime($date1);
        $date2 = strtotime($date2);
        $stepVal = '+1 day';
        while ($current <= $date2) {
            $dates[] = date($format, $current);
            $current = strtotime($stepVal, $current);
        }
        return $dates;
    }

    /**
     * if id is not black then download structure of upload the suppliers otherwise suppliers view details show
     *
     * @method POST
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        if ($id == 'import') {
            
            $import = new SupplierExport();
            return Excel::download($import, 'supplier.xlsx');

            // $file_name = 'supplier.xlsx';
            // $file = public_path().'/sheets/'.$file_name;
            // //$file= public_path(). "/download/info.pdf";

            // $headers = array(
            //         'Content-Type: application/csv',
            //         );

            // return Response::download($file, $file_name, $headers);

            // $filename = 'suppliers.csv';
            // $headers = array(
            //     "Content-type" => "text/csv",
            //     "Content-Disposition" => "attachment; filename=" . $filename . " ",
            //     "Pragma" => "no-cache",
            //     "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            //     "Expires" => "0"
            // );

            // $columns = array('Supplier Name', 'Country', 'Address', 'State', 'Postal Code', 'Contact_Fname', 'Contact_Lname', 'Contact_Title', 'Contact_Email', 'Contact_Phone','Primary','Default_Transit','Transit_Type','PO_to_Production','Production','To_Port','Transit_Time','To_Warehouse','To_Amazon','Safety_days','Total_Lead_Time','Notify_Confirm_Production_Started','X_Days_Confirm_Production_Started','Contact_Confirm_Production_Started','Notify_Confirm_Production_End','X_Days_Confirm_Production_End','Contact_Confirm_Production_End','Notify_To_Port','Contact_To_Port','Notify_In_Transit','Contact_In_Transit','Notify_Port_Arrival','Contact_Port_Arrival','Notify_To_Warehouse','Contact_To_Warehouse','Notify_Warehouse_Receipt','Contact_Warehouse_Receipt','Order_Volume','Order_Volume_Type','Reorder_Schedule','Reorder_Schedule_Calendar','Blackout_Event_Name','Blackout_Start_Date','Blackout_End_Date','Blackout_Type','Logistics_Handler','Shipping_Agent','LCL_Cost','LCL_Metric','Container_20ft','Container_20ft_CBM','Container_20ft_Cost','Container_40ft','Container_40ft_CBM','Container_40ft_Cost','Container_40HQ','Container_40HQ_CBM','Container_40HQ_Cost','Container_45HQ','Container_45HQ_CBM','Container_45HQ_Cost','By_Air_Standard','By_Air_Standard_Cost','By_Air_Standard_Metric','By_Air_Express','By_Air_Express_Cost','By_Air_Express_Metric','Duties_Cost','Ship_To_Warehouse','Payment_Deposit','Payment_Deposit_Due_Days','Payment_Deposit_Event','Payment_Balance','Payment_Balance_Due_Days','Payment_Balance_Event');


            // $fileputcsv = array('Supplier test', 'India', 'ahmedabad', 'Gujarat', '385001', 'f1', '11', 't1', 'f1@gmail.com', '1234567890','1','Boat/Plane','Boat/Plane','10','3','1','2','2','2','10','30','1','3','f1@gmail.com,f2@gmail.com','1','1','f1@gmail.com,f2@gmail.com','1','f1@gmail.com,f2@gmail.com','','','1','f1@gmail.com','','','','','1','Days','bi-weekly','sun,mon','event1','1/6/2021','5/6/2021','1','supplier/shipping-agent','','10','kg','1','1','10','1','1','10','1','1','10','1','1','10','1','10','kg','1','10','cbm','100','warehouse1,warehouse2','20','10 before','Production Start Date','10','10 after','Production End Date');

            // $fileputcsv1 = array('', '', '', '', '', 'f2', '12', 't2', 'f2@gmail.com', '1234567890','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','event2','10/6/2021','15/6/2021','1','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','');

            // $callback = function () use ($columns, $fileputcsv,$fileputcsv1) {
            //     ob_clean();
            //     $file = fopen('php://output', 'w+');
            //     fputcsv($file, $columns);
            //     fputcsv($file, $fileputcsv);
            //     fputcsv($file, $fileputcsv1);
            //     fclose($file);
            // };
            // return Response::stream($callback, 200, $headers);
        } else {
//            $vendor_setting = Supplier_wise_default_setting::where('supplier_id',$id)->first();
//            if(empty($vendor_setting)){
//                $vendor_setting = Supplier_default_setting::first();
//            }
//            $warehouse = Warehouse::where('user_marketplace_id',session('MARKETPLACE_ID'))->get()->toArray();
//            return view('suppliers.setting',['id'=>$id,'supplier_setting'=>$vendor_setting,'warehouse'=>$warehouse]);
            $data = Supplier::with(['supplier_contact', 'supplier_product', 'supplier_blackout_date', 'supplier_wise_setting'])->where(array('id' => $_GET['id']))->get()->toArray();
            return view('suppliersnew.detail', [
                'supplierDetail' => $data
            ]);
        }
    }

    /**
     * Show the form for editing the specified supplier
     * @method GET
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $get_checks = get_access('supplier_module', 'edit');
        $get_user_access = get_user_check_access('supplier_module', 'edit');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }
        $global_marketplace = session('MARKETPLACE_ID');
        $suppliers_data = Supplier::where(array('id' => $id))->get()->first();
        $primary_suppliers = Supplier_contact::where(array('supplier_id' => $id, 'primary' => 1))->get()->first();
        $suppliers_contact = Supplier_contact::where(array('supplier_id' => $id))->get()->toArray();
        $setting = Supplier_wise_default_setting::where(array('supplier_id' => $id))->first();
        $shippingAgents = ShippingAgent::where(array('user_marketplace_id' => $global_marketplace))->get()->toArray();
        $shippingDetails = Shipping::where('supplier_vendor', env('SUPPLIER'))->where('supplier_vendor_id', $id)->get()->first();
        $containerDetails = $shiptoAirDetails = $shiptoWarehouseDetails = '';
        if(!empty($shippingDetails)){
            $containerDetails = ShippingContainerSettings::where('shipping_id',$shippingDetails->id)->get()->toArray();
            $shiptoAirDetails = Shippingair::where('shipping_id',$shippingDetails->id)->get()->toArray();
            $shiptoWarehouseDetails = ShipToWarehouse::where('shipping_id',$shippingDetails->id)->get()->toArray();
        }
        

        $get_lead_time = Lead_time::where('supplier_vendor', env('SUPPLIER'))->where('supplier_vendor_id', $id)->first();
        $lead_time_value = array();
        if(!empty($get_lead_time)){
            $lead_time_value = Lead_time_value::where(array('lead_time_id'=>$get_lead_time->id))->get()->toArray();
        }

        $country = get_country_list();
        $cid = null;
        $state_id = null;

        if (isset($suppliers_data->country_id) && $suppliers_data->country_id != null) {
            $cid = $suppliers_data->country_id;
        }
        if (isset($suppliers_data->state) && $suppliers_data->state != "" && (isset($suppliers_data->country_id)) && $suppliers_data->country_id != "") {
            $state_id = $suppliers_data->state;
        }

        $state_list = get_state_list($cid);
        $city_list = get_city_list($state_id);
        $warehouse = Warehouse::where(array('user_marketplace_id' => $global_marketplace))->get()->toArray();
        $blackout_dates = Supplier_blackout_date::where(array('supplier_id' => $id))->get()->toArray();
        $lead_time_type = Lead_time_type::get()->toArray();
        $lead_time_check = Lead_time_check::get()->toArray();
        $order_volume_type= Order_volume_type::get()->toArray();
        $reorder_schedule_type = Reorder_schedule_type::get()->toArray();
        $schedule_details = Reorder_schedule_detail::where('supplier_vendor', env('SUPPLIER'))->where('supplier_vendor_id', $id)->selectRaw('supplier_vendor,reorder_schedule_type_id,supplier_vendor_id,GROUP_CONCAT(schedule_days) as days')->first();
        $payment_term = Payment_terms_type::get()->toArray();
        $get_payment_id = Payment::where(array('supplier_vendor'=>env('SUPPLIER'),'supplier_vendor_id'=>$id))->first();
        $get_deposit = array();
        $get_balance = array();
        if(!empty($get_payment_id)){
            $get_deposit = Payment_details::where(array('payment_id'=>$get_payment_id->id,'payment_terms'=>'1'))->first();
            $get_balance = Payment_details::where(array('payment_id'=>$get_payment_id->id,'payment_terms'=>'2'))->first();
        }
        return view('suppliersfinal.edit-suppliers', [
                'shiptoWarehouseDetails' => ($shiptoWarehouseDetails ? $shiptoWarehouseDetails : ''),
                'container_details' => ($containerDetails ? $containerDetails : ''),
                'ship_to_air_details' => ($shiptoAirDetails ? $shiptoAirDetails : ''),
                'suppliers' => $suppliers_data,
                'primary_suppliers' => $primary_suppliers,
                'suppliers_contact' => $suppliers_contact,
                'country_list' => $country,
                'state_list' => $state_list,
                'city_list' => $city_list,
                'setting' => $setting,
                'warehouse' => $warehouse,
                'blackout_dates' => $blackout_dates,
                'shipping_agents' => $shippingAgents,
                'shippingDetails' => $shippingDetails,
                'payment_term' =>$payment_term,
                'deposit' =>$get_deposit,
                'balance' => $get_balance,
                'leadtime'=>$get_lead_time,
                'lead_time_type'=>$lead_time_type,
                'lead_time_check'=>$lead_time_check,
                'order_volume_type'=>$order_volume_type,
                'reorder_schedule_type'=>$reorder_schedule_type,
                'schedule_details' =>$schedule_details,
                'lead_time_value'=>$lead_time_value
            ]
        );
    }

    /**
     *
     * There are 2 function added in update method :
     * @method PUT
     *
     *  Request Types:
     *
     * 1. edit_form : Update the specified supplier in storage
     * @param  \Illuminate\Http\Response $request
     * @return \Illuminate\Http\Response
     *
     * 2. setting_form: update the default supplier setting
     * @param  \Illuminate\Http\Response $request
     * @return \Illuminate\Http\Response
     *
     */


    public function update(Request $request, $id)
    {
        $inputs = $request->all();
        $get_checks = get_access('supplier_module', 'edit');
        $get_user_access = get_user_check_access('supplier_module', 'edit');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }

        if (isset($request->insert_type) && $request->insert_type == 'edit_form') {
            $firstname = !empty($request->firstname) ? array_filter($request->firstname) : array();
            $lastname = !empty($request->lastname) ? array_filter($request->lastname) : array();
            $email = !empty($request->email) ? array_filter($request->email) : array();

            if (!empty($check_emails)) {
                $message = get_messages('Email already exists!', 0);
                Session::flash('message', $message);
                return redirect()->route('suppliers.index');
            }

            $req_data = [
                'user_marketplace_id' => $request->marketplace_id,
                'supplier_name' => $request->supplier_name,
                'address_line_1' => $request->address_line1,
                'address_line_2' => $request->address_line2,
                'city' => $request->city,
                'state' => $request->states,
                'zipcode' => $request->zipcode,
                'country_id' => $request->country
            ];

            if ($id != '') {

                $suppliers = Supplier::findOrFail($id);
                $suppliers->update($req_data);

                if (!empty($firstname) && !empty($lastname) && !empty($email)) {

                    $check_email = Supplier_contact::whereIn('email', $request['email'])->get()->toArray();

                    if (!empty($check_email)) {
                        $message = get_messages('Email already exists!', 0);
                        Session::flash('message', $message);
                        return redirect()->route('suppliers.index');
                    }
                    foreach ($firstname as $key => $value) {
                        if ($value == '' && $lastname[$key] == '' && $email[$key] == '') {
                        } else {
                            $sub_data = [
                                'supplier_id' => $id,
                                'first_name' => $value,
                                'last_name' => @$lastname[$key],
                                'email' => @$email[$key],
                                'primary' => 0,
                            ];
                            $datas_of = Supplier_contact::create($sub_data);
                        }
                    }
                }
                $message = get_messages('Suppliers updated successfully', 1);
                Session::flash('message', $message);
                return redirect()->route('suppliers.index');
            } else {
                $message = get_messages('Failed to update supplier data', 0);
                Session::flash('message', $message);
                return redirect()->route('suppliers.index');
            }
        } else if (isset($request->insert_type) && $request->insert_type == 'setting_form') {
            if ($id != '') {
                $req_data = [
                    'supplier_id' => $id,
                    'lead_time' => $request->lead_time,
                    'order_volume' => $request->order_volume,
                    'quantity_discount' => $request->quantity_discount,
                    'moq' => $request->moq,
                    'CBM_Per_Container' => $request->CBM_Per_Container,
                    'Production_Time' => $request->Production_Time,
                    'Boat_To_Port' => $request->Boat_To_Port,
                    'Port_To_Warehouse' => $request->Port_To_Warehouse,
                    'Warehouse_Receipt' => $request->Warehouse_Receipt != '' ? $request->Warehouse_Receipt : 0,
                    'Ship_To_Specific_Warehouse' => $request->Ship_To_Specific_Warehouse
                ];
                $vendor_setting = Supplier_wise_default_setting::where('supplier_id', $id)->first();
                if (!empty($vendor_setting)) {
                    $vendor_setting->update($req_data);
                    $message = get_messages('Supplier setting updated successfully', 1);
                    Session::flash('message', $message);
                    return redirect()->route('suppliers.index');
                } else {
                    Supplier_wise_default_setting::create($req_data);
                    $message = get_messages('Supplier setting created successfully', 1);
                    Session::flash('message', $message);
                    return redirect()->route('suppliers.index');
                }
            }
        } else {
            $message = get_messages('Something wrong', 0);
            Session::flash('message', $message);
            return redirect()->route('suppliers.index');
        }
    }

    /**
     * Remove the specified supplier & it's contact from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $get_checks = get_access('supplier_module', 'delete');
        $get_user_access = get_user_check_access('supplier_module', 'delete');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }

        $suppliers = Supplier::findOrFail($id);
        $suppliers->delete();

//        $suppliers_primary = Supplier_contact::where('supplier_id', $id)->firstOrFail();
//        $suppliers_primary->delete();
        return json_encode(array('statusCode' => 200));

    }

    public function changeContact(Request $request){
        $contact_id= $request->contact_id;
        $supplier_contact = Supplier_contact::where('id', $contact_id)->first();
        if(!empty($supplier_contact)){
            $res['email'] = $supplier_contact['email'];
            $res['phone'] = $supplier_contact['phone_number'];
        }
        else{
            $res['email'] = '';
            $res['phone'] = '';
        }
        return response()->json($res);
    }

    public function ImportSupplier()
    {
        $fileContent = file_get_contents(request()->file('file'));
        $enc = mb_detect_encoding($fileContent, mb_list_encodings(), true);
        \Config::set('excel.imports.csv.input_encoding', $enc);

        $collection= Excel::import(new SupplierImport(),request()->file('file'),'UTF-8');
        // return redirect()->route('suppliers.index');
         return redirect('/supplier_import_message');
    }

    public function supplier_import_message()
    {
        $error_msg=((\Session::get('error_msg_array')) ? \Session::get('error_msg_array') : '');
        $success_msg=((\Session::get('success_message')) ? \Session::get('success_message') : '');
        
        $error_count=((\Session::get('error_count')) ? \Session::get('error_count') : '');
        $success_count=((\Session::get('success_count')) ? \Session::get('success_count') : '');
        $total_count=((\Session::get('total_count')) ? \Session::get('total_count') : '');

        if(!empty($error_msg)){
            return view('suppliersfinal.import_messages',compact('error_msg','success_msg','total_count','error_count','success_count'));
        }else{
            $message = get_messages("Total $total_count rows uploaded successfully", 1);
            Session::flash('message', $message);
            return redirect('/suppliers');
        }

    }

    public function getData(){
        $global_marketplace = session('MARKETPLACE_ID');
        $suppliers = Supplier::with('supplier_contact')->where(array('user_marketplace_id'=>$global_marketplace))->orderBy('id','desc')->get();
        $suppliers->each(function($supplier){
            $suppliers_contact = Supplier_contact::where(array('supplier_id'=>$supplier['id']))->first();
            $suppliers_blackoutdate = Supplier_blackout_date::where(array('supplier_id'=>$supplier['id']))->first();
            $lead_time = Lead_time::where(array('supplier_vendor'=>$supplier,'supplier_vendor_id'=>$supplier['id']))->first();
            $order_setting = Reorder_schedule_detail::where(array('supplier_vendor' => $supplier, 'supplier_vendor_id' =>$supplier['id']))->first();
            $payment = Payment::where(array('supplier_vendor'=>$supplier,'supplier_vendor_id'=>$supplier['id']))->first();
            $shipping = Shipping::where(array('supplier_vendor'=>$supplier,'supplier_vendor_id'=>$supplier['id']))->first();
            if(empty($suppliers_contact) || empty($suppliers_blackoutdate) || empty($lead_time) || empty($order_setting) || empty($payment) || empty($shipping)){
                $check_return = 0;
            }else{
                $check_return = 1;
            }
            $supplier['completeFlag'] = $check_return;
            $supplier['country'] = $supplier->country();
           $supplier['sku'] = $supplier->supplier_product->count();
        });

        return $suppliers;
    }

    public function bulkDelete(Request $request){
        Supplier::whereKey($request->selected)->delete();
        return ['success' => true];
    }

    public function bulkExport(){
        return response()->streamDownload(function(){
            echo Supplier::toCsv();
        }, 'suppliers'. date("Ymdhis") .'.csv');
    }

    public function updateCols(){
        if(DB::table('cols')->where('user_id', auth()->id())->where('table_name', 'suppliers')->get()->count()){
            DB::table('cols')
                ->where('user_id', auth()->id())
                ->where('table_name', 'suppliers')
                ->update(['cols' => request()->cols]);
        }else{
            DB::table('cols')
                ->insert([
                    'user_id' => auth()->id(),
                    'table_name' => 'suppliers',
                    'cols' => json_encode(request()->cols)
                ]);
        }
    }
}

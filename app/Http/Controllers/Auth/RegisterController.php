<?php

namespace App\Http\Controllers\Auth;
use App\Models\Role_access_modules;
use App\Models\User_access_modules;
use App\Models\Userdetails;
use App\Models\User_assigned_role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Application_modules;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255','regex:/^[a-z. \s]+$/i'],
            'company' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $activate_code = md5( rand( 1, 10000000000 ) . "skote" . rand( 1, 100000 ) );
        $settings = get_settings();
        $referId = (!empty($data['refer_id']) ? $data['refer_id'] : '0');
        $role = (!empty($data['role']) ? $data['role'] : '3');
        if(!empty($settings['email_settings']) && $settings['email_settings'] == 1){
            $user =  User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'parent_id'=>'0',
                'refer_id'=> $referId,
                'activation_code' => $activate_code,
                'active'=>0,
                'password' => Hash::make($data['password']),
            ]);
        } else {
            $user =  User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'parent_id'=>'0',
                'refer_id'=> $referId,
                'active'=>1,
                'password' => Hash::make($data['password']),
            ]);
        }
        // add user detail
        $userLastInsertId = $user->toArray();
        $input['user_id'] = $userLastInsertId['id'];
        $input['company'] = $data['company'];
        Userdetails::create($input);
        // add assigned role
        $assigned_role['user_id'] = $userLastInsertId['id'];
        $assigned_role['user_role_id'] = $role;
        User_assigned_role::create($assigned_role);

        $get_application_module = Application_modules::get()->toArray();
        if(!empty($get_application_module)){
            foreach($get_application_module as $module){
                $check_application_check = Role_access_modules::where(array('role_id'=>$assigned_role['user_role_id'],'application_module_id'=>$module['id']))->first();
                $modue['role_id'] = $assigned_role['user_role_id'];
                $modue['user_id'] = $userLastInsertId['id'];
                $modue['application_module_id'] = $module['id'];
                if(!empty($check_application_check)){
                    $modue['create'] = $check_application_check->create;
                    $modue['edit'] = $check_application_check->edit;
                    $modue['delete'] = $check_application_check->delete;
                    $modue['view'] = $check_application_check->view;
                    $modue['access'] = $check_application_check->access;
                }else{
                    $modue['create'] = 0;
                    $modue['edit'] = 0;
                    $modue['delete'] = 0;
                    $modue['view'] = 0;
                    $modue['access'] = 0;
                }
                User_access_modules::create($modue);
            }
        }

        if(!empty($settings['email_settings']) && $settings['email_settings'] == 1){
            $data['confirmation_code'] = $activate_code;
            Mail::send('emailtemplate.registereduser', $data, function($message) use ($data){
                $message->to($data['email'], $data['name'])
                    ->subject('Verify your email address');
            });
            if (Mail::failures()) {
                $message = get_messages('Something wrong to sending Activation Mail Please Contact System Admin.', 0);
                Session::flash('message', $message);
            }else{
                $message = get_messages('Check your mail and Verify Account.', 1);
                Session::flash('message', $message);
            }
        }
    return $user;
    }
    protected function registered(Request $request, $user)
    {
        $this->guard()->logout();
        $settings = get_settings();
        if(!empty($settings['email_settings']) && $settings['email_settings'] == 1){
            $message = get_messages('We sent you an activation code. Check your email and click on the link to verify.', 1);
        } else {
            $message = get_messages('You have registered successfully.', 1);
        }
        Session::flash('message', $message);
        return redirect('/login');
    }
    public function verifyUser($token)
    {
        $verifyUser = User::where('activation_code', $token)->first();
        if(!empty($verifyUser) ){
            if($verifyUser->active==0) {
                $verifyUser->active = 1;
                $verifyUser->activation_code='';
                $verifyUser->save();
                $message = get_messages('Your e-mail is verified. You can now login.', 1);
                Session::flash('message', $message);
            }else{
                $message = get_messages('Your e-mail is already verified. You can now login.', 1);
                Session::flash('message', $message);
            }
        }else{
            $message = get_messages('Sorry your email cannot be identified.', 0);
            Session::flash('message', $message);
            return redirect('/login');
        }
        return redirect('/login');
    }
}

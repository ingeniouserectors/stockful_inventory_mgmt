<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User_assigned_role;
use App\Models\Users;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Usermarketplace;
use App\Models\Temp_session;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function authenticated(Request $request, $user)
    {
        if($user->id != ''){
            Temp_session::where(array('user_id'=>$user->id))->delete();
        }
        $get_role = User_assigned_role::where(array('id'=>$user->id))->first();
        if($get_role->user_role_id == 3){
            $total_marketplace = Usermarketplace::where(array('user_id'=>$user->id))->get();
            if(!empty($total_marketplace) && @$total_marketplace[0]->id != '') {
                $product_id = 1;
                $request->session()->put('MARKETPLACE_ID', @$total_marketplace[0]->id);
            }else{
                $request->session()->put('MARKETPLACE_ID', '');
            }
        }else if($get_role->user_role_id == 5){
            $get_parent_user = Users::where(array('id'=>$user->id))->first();
            if(!empty($get_parent_user) && $get_parent_user->parent_id != ''){
                $total_marketplace = Usermarketplace::where(array('user_id'=>$get_parent_user->parent_id))->get();
                if(!empty($total_marketplace) && @$total_marketplace[0]->id != '') {
                    $request->session()->put('MARKETPLACE_ID', @$total_marketplace[0]->id);
                }else{
                    $request->session()->put('MARKETPLACE_ID', '');
                }
            }else{
                $request->session()->put('MARKETPLACE_ID', '');
            }
        }else{
            $request->session()->put('MARKETPLACE_ID', '');
        }
        if ($user->active==0) {
            $this->guard()->logout();
            $message = get_messages('You need to confirm your account. We have sent you an activation code, please check your email.', 0);
            Session::flash('message', $message);
            return back();
        }
        return redirect()->intended($this->redirectPath());
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vendor_contact;
use App\Models\Vendor;
use Illuminate\Support\Facades\Session;
use Response;

class VendorsContactController extends Controller
{
    /**
     * download the structure of the vendors contact.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filename = 'vendorscontacts.csv';
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=".$filename." ",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $columns = array('Firstname', 'Lastname', 'Email');
        $fileputcsv =  array('Test','Test','demo@gmail.com');

        $callback = function() use ( $columns,$fileputcsv)
        {
            ob_clean();
            $file = fopen('php://output', 'w+');
            fputcsv($file, $columns);
            fputcsv($file,$fileputcsv);
            fclose($file);
        };
        return Response::stream($callback, 200, $headers);
    }

    /**
     * Show the form for creating a new vendor contact.
     * @method GET
     * @params no-params
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $vendor_id = $_GET['vendor_id'];
        return view('vendors.contacts.create', [
            'vendors_id' => $vendor_id,
        ]);
    }

    /**
     * There are 3 function added in store method :
     *      @method POST
     *
     *  Request Types:
     *
     * 1. vendors_contact_list : listing of vendors contact
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     * 2. create_form: create a new contact for vendors in the form data
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     * 3. upload_csv: upload csv file for vendors contacts
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request->request_type == 'vendors_contact_list'){
         $data = Vendor_contact::where(array('vendor_id' =>$request->id))->get()->toArray();
          $vendors_contact_data = array();
         if(!empty($data))
            {
                 foreach ($data as $key => $post)
                {
                    $action = '' ;
                    $nestedData['first_name'] =  $post['first_name'];
                    $nestedData['last_name'] =  $post['last_name'];
                    $nestedData['email'] =  $post['email'];
                    $nestedData['primary'] =  $post['primary'] == '1' ? 'Yes' : 'No';
                    
                    $action .= '<a href="'.route('vendorscontact.edit',$post['id']).'"  class=" btn btn-info btn-sm btn-rounded waves-effect waves-light" role="button"><i class="fas fa-edit" ></i></a>';
                    $action .=  '<button id="button" type="submit" class=" btn btn-danger btn-sm btn-rounded waves-effect  waves-light sa-remove" data-id='.$post['id'].'><i class="fas fa-trash-alt"></i></button>';
                    $nestedData['action'] =  $action ;
                    $vendors_contact_data[] = $nestedData;               
                }    
             }
                
           echo json_encode($vendors_contact_data); exit;


       }
        $userLastInsertId = $request->vendor_id;
        if($request->insert_type == 'create_form'){
            $userLastInsertId = $request->vendor_id;
            $firstname = array_filter($request->firstname);
            $lastname = array_filter($request->lastname);
            $email = array_filter($request->email);

            if(!empty($email)) {
                $emailList = implode(',', $email);
                $check_emails = Vendor_contact::whereIn('email', $request['email'])->get()->toArray();

                if (!empty($check_emails)) {
                    $message = get_messages('Email already exists!', 0);
                    Session::flash('message', $message);
                    return redirect('vendorscontact/'.$request->vendor_id);
                }
            }

            if($userLastInsertId != '') {
                if(!empty($firstname) && !empty($lastname) && !empty($email)) {
                    $get_vendors_contact = Vendor_contact::where(array('vendor_id' => $userLastInsertId,'primary'=>1))->get()->toArray();
                    $primary = !empty($get_vendors_contact) ? 0 : 1;
                    foreach ($firstname as $key => $value) {
                        if ($value == '' && $lastname[$key] == '' && $email[$key] == '') {
                        } else {
                            $sub_data = [
                                'vendor_id' =>$userLastInsertId,
                                'first_name' => $value,
                                'last_name' => @$lastname[$key],
                                'email' => @$email[$key],
                                'primary' => $primary,
                            ];
                            $datas_of = Vendor_contact::create($sub_data);
                        }
                    }
                }
                $message = get_messages('Vendors contact added successfully', 1);
                Session::flash('message', $message);
                return redirect('vendorscontact/'.$request->vendor_id);
            }else{
                $message = get_messages('Failed to add vendors contact  data', 0);
                Session::flash('message', $message);
                return redirect('vendorscontact/'.$request->vendor_id);
            }
        }else if($request->insert_type == 'upload_csv'){
            if ($request->hasFile('uploadFile')) {
                $emails_data = array();
                $size = $request->file('uploadFile')->getSize();
                $extension = $request->uploadFile->getClientOriginalExtension();
                if ($extension == "csv" && $size <= 100000) {
                    $filePath = $request->file('uploadFile')->getRealPath();
                    $file = fopen($filePath, "r");
                    $escapedHeader = [];
                    $detail = [];
                    $flag = 0;

                    while ($columns = fgetcsv($file)) {
                        $columnValue = [];
                        if ($flag == 0) {
                            if (trim($columns[0]) != 'Firstname' || trim($columns[1]) != 'Lastname' || trim($columns[2]) != 'Email' ) {
                                $message = get_messages('Please upload proper file format as provide in sample!', 0);
                                Session::flash('message', $message);
                                return redirect('vendorscontact/'.$request->vendor_id);
                            }
                        }
                        foreach ($columns as $key => &$value) {
                            if ($flag == 0) {
                                $lowerHeader = strtolower($value);
                                $escapedItems = preg_replace("/[^a-z]/", "", $lowerHeader);
                                array_push($escapedHeader, $escapedItems);
                            } else {
                                array_push($columnValue, $value);
                            }
                        }
                        $flag++;
                        if (!empty($columnValue))
                            $detail[] = array_combine($escapedHeader, $columnValue);
                    }

                    if(!empty($detail)){
                        $uploaded_file = $request->file('uploadFile');
                        $name = 'csv_' . time() . $uploaded_file->getClientOriginalName();
                        $uploaded_file->move(public_path('assets/images/csv/vendors'), $name);
                        $images[] = $name;
                        $storefolder = 'assets/images/csv/vendors/csv_' . time() . $uploaded_file->getClientOriginalName();
                    }
                    $i=0;
                    foreach ($detail as $data) {
                        $product = [];
                        if (strlen($data['firstname']) <= 0) {
                            $message = get_messages('firstname is empty.', 0);
                            Session::flash('message', $message);
                        } else if (strlen($data['lastname']) <= 0) {
                            $message = get_messages('lastname is empty.', 0);
                            Session::flash('message', $message);
                        } else if (strlen($data['email']) <= 0) {
                            $message = get_messages('email is empty.', 0);
                            Session::flash('message', $message);
                        } else {
                            $checkemail = Vendor_contact::where(array('email'=>$data['email']))->get()->toArray();
                            if(empty($checkemail)){
                                $get_primary = Vendor_contact::where(array('vendor_id'=>$request->vendor_id,'primary'=>1))->first();
                                $primary = !empty($get_primary) ? 0 : 1 ;
                                $req_datas = [
                                    'vendor_id' => $request->vendor_id,
                                    'first_name' => $data['firstname'],
                                    'last_name' => $data['lastname'],
                                    'email' => $data['email'],
                                    'primary' => $primary,
                                ];
                                $datas_of = Vendor_contact::create($req_datas);

                               
                                $message2 = "csv uploaded successfully !";
                                Session::flash('message2', $message2);
                            }else{
                                $i++;
                               
                                     $message3 = $i.' email already exists';
                                     Session::flash('message3', $message3);   
                            }
                        }
                    }
                   
                    return redirect('vendorscontact/'.$request->vendor_id);
                }
            }else{
                $message = get_messages('CSV file must be required', 0);
                Session::flash('message', $message);
                return redirect('vendorscontact/'.$request->vendor_id);
            }
        }else{
            $message = get_messages('Something wrong', 0);
            Session::flash('message', $message);
            return redirect('vendorscontact/'.$request->vendor_id);
        }

}

    /**
     * Display the specified vendors contacts details.
     * @method POST
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id != '') {
            $get_vendors = Vendor::where(array('id' => $id))->first();
            return view('vendors.contacts.index', [
                'vendors_id' => $id,
                'vendors'=>$get_vendors
            ]);
        }else{
            $message = get_messages('Failed to get vendors contact details', 0);
            Session::flash('message', $message);
            return redirect()->route('vendors.index');
        }
    }

    /**
     * Show the form for editing the specified vendors contact
     * @method GET
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vendors_data = Vendor_contact::where(array('id'=>$id))->get()->first();
        return view('vendors.contacts.edit-vendors', [
                'primary_vendors' => $vendors_data,
            ]
        );
    }

    /**
     * Update the specified contact in storage.
     * @method POST
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $inputs=$request->all();              
        $firstname = !empty($request->firstname) ?  array_filter($request->firstname) : array();
        $lastname = !empty($request->lastname) ?  array_filter($request->lastname) : array();
        $email = !empty($request->email) ?  array_filter($request->email) : array();

        
            $emailList = implode(',', $email);

            $check_emails = Vendor_contact::where(array(['email','=',trim($inputs['email_edit'])],['id','!=',$id]))->get()->toArray();
            if (!empty($check_emails)) {
                $message = get_messages('Email already exists!', 0);
                Session::flash('message', $message);
                return redirect('vendorscontact/'.$request->vendor_id);
        }

        if($id != '') {

            $primary_vendors = $request->vendor_id;
            if($primary_vendors != '') {
                $req_datas = [
                    'first_name' => $request->firstname_edit,
                    'last_name' => $request->lastname_edit,
                    'email' => $request->email_edit,
                ];

                $vendors = Vendor_contact::findOrFail($id);
                $vendors->update($req_datas);
            }

            if(!empty($firstname) && !empty($lastname) && !empty($email)) {

                $check_email = Vendor_contact::whereIn('email', $request['email'])->get()->toArray();
                if (!empty($check_email)) {
                $message = get_messages('Email already exists!', 0);
                Session::flash('message', $message);
                return redirect('vendorscontact/'.$request->vendor_id);
                }   
                foreach ($firstname as $key => $value) {
                    if ($value == '' && $lastname[$key] == '' && $email[$key] == '') {
                    } else {
                        $sub_data = [
                            'vendor_id' =>$request->vendor_id,
                            'first_name' => $value,
                            'last_name' => @$lastname[$key],
                            'email' => @$email[$key],
                            'primary' => 0,
                        ];
                        $datas_of = Vendor_contact::create($sub_data);
                    }
                }
            }
            $message = get_messages('Vendors contact updated successfully', 1);
            Session::flash('message', $message);
            return redirect('vendorscontact/'.$request->vendor_id);
        }else{
            $message = get_messages('Failed to update vendors contact data', 0);
            Session::flash('message', $message);
            return redirect('vendorscontact/'.$request->vendor_id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vendors = Vendor_contact::findOrFail($id);
        $vendors->delete();
        return json_encode(array('statusCode'=>200));
    }

    
}

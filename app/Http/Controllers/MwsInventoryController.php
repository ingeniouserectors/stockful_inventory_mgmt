<?php

namespace App\Http\Controllers;

use App\Models\Calculated_order_qty;
use App\Models\Calculated_sales_qty;
use App\Models\Mws_afn_inventory_data;
use App\Models\Mws_calculated_inventory_daily;
use App\Models\Mws_product;
use App\Models\Mws_unsuppressed_inventory_data;
use App\Models\Mws_unsuppressed_historical_inventory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Usermarketplace;
use App\Models\Mws_inventory;
use App\Models\Inbound_shipment_items;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Models\Mws_out_of_stock_detail;

class MwsInventoryController extends Controller
{
    /**
     * Display a listing of the mws_product with mws_afn_inventory,mws_reserved_inventory,mws_unsuppressed_inventory_data & daily_logic_calculations.
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get_checks = get_access('mws_inventory_module','view');
        $get_user_access = get_user_check_access('mws_inventory_module','view');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }
        $global_marketplace = session('MARKETPLACE_ID');
        ini_set('memory_limit', '-1');
        $marketplace = '';
        $get_inventory = '';
        if ($global_marketplace != '') {
            $marketplace = $global_marketplace;
            // $get_inventory = Mws_product::with(['mws_reserved_inventory','mws_afn_inventory','mws_unsuppressed_inventory_data','daily_logic_calculations'])->where(array('user_marketplace_id' => $global_marketplace))->selectRaw('sku,id')->get()->toArray();
        }
        return view('mws_inventories.index', [
            'marketplace' => $marketplace,
            //'get_inventory' => $get_inventory
        ]);


        /*$data = array();
        $marketplace = '';
        ini_set('memory_limit', '-1');
        $total_marketplace = Usermarketplace::where(array('user_id'=>Auth::id()))->get();
            if(!empty($total_marketplace) && @$total_marketplace[0]->id != '') {
                $marketplace = @$total_marketplace[0]->id;
            $data = Mws_inventory::with('usermarketplace','mws_product')->where(array('user_marketplace_id'=>$total_marketplace[0]->id))->get()->toArray();
        }
        return view('mws_inventories.index',[
            'marketplace'=>$marketplace,
            'mwsinventory_details'=>$data,
            'marketplace_list'=>$total_marketplace,

        ]);*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * If id is blank then get all inventories data with marketplace wise, otherwise get the listing of inventory data
     * @method POST
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //ini_set('memory_limit', '-1');
        if ($request->insert_type == 'get_inventory') {
            $inputs = $request->all();
            $datas = array();
            $global_marketplace = session('MARKETPLACE_ID');
            $details = Mws_product::join('mws_afn_inventory_data', 'mws_products.id', '=', 'mws_afn_inventory_data.product_id')->selectRaw('mws_products.id as id,
                            mws_products.sku as sku
                            '
            )->where(array('mws_afn_inventory_data.user_marketplace_id' => $request->id));

            $data = $details->orderBy('mws_afn_inventory_data.quantity_available', 'desc')->get();
            if (!empty($data)) {
                foreach ($data as $key => $post) {
                    $nestedData['id'] = $key + 1;
                    $nestedData['sku'] = $post['sku'];
                    $nestedData['qty_available'] = $post['mws_afn_inventory']['quantity_available'];
                    $nestedData['afn_qty'] = $post['mws_unsuppressed_inventory_data']['afn_fulfillable_qty'];
                    $nestedData['current_stock'] = '';
                    $nestedData['fc_transfer'] = $post['mws_reserved_inventory']['reserved_fc_transfer'];
                    $nestedData['fc_processing'] = $post['mws_reserved_inventory']['reserved_fc_processing'];
                    $nestedData['customer_order'] = $post['mws_reserved_inventory']['reserved_customer_orders'];
                    $nestedData['reserved_qty'] = $post['mws_reserved_inventory']['reserved_qty'];
                    $nestedData['afn_research_qty'] = $post['mws_unsuppressed_inventory_data']['afn_research_qty'];
                    $nestedData['afn_sellable_qty'] = $post['mws_unsuppressed_inventory_data']['afn_unsellable_qty'];
                    $nestedData['afn_warehouse_qty'] = $post['mws_unsuppressed_inventory_data']['afn_warehouse_qty'];
                    $nestedData['amz_rece_inbound_qty'] = $post['mws_unsuppressed_inventory_data']['afn_inbound_receving_qty'];
                    $nestedData['amz_ship_inbound_qty'] = $post['mws_unsuppressed_inventory_data']['afn_inbound_shipment_qty'];
                    $nestedData['amz_working_inbound_qty'] = $post['mws_unsuppressed_inventory_data']['afn_inbound_working_qty'];
                    $nestedData['afn_total_qty'] = $post['mws_unsuppressed_inventory_data']['afn_total_qty'];
                    $datas[] = $nestedData;
                }
            }
            echo json_encode($datas);
            exit;

        } else {
            $data = array();
            $inputs = $request->all();
            if ($inputs['type'] == 'show_marketplace_wise_data') {
                $data = Mws_inventory::with('usermarketplace', 'mws_product')->where(array('user_marketplace_id' => $inputs['id']))->get()->toArray();
                $view = View::make('mws_inventories.get_marketplace_wise_data', [
                    'inventory_details' => $data,
                ]);
                $html = $view->render();
                $res['error'] = 0;
                $res['view'] = $html;
                return response()->json($res);
            }
        }
    }

    /**
     * download stockful inventory .
     * @method GET
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');
        $global_marketplace = session('MARKETPLACE_ID');
        if (empty($global_marketplace)) {
            $global_marketplace = $id;
        }
        $file = base_path() . '/download/Stockful_' . $global_marketplace . '_' . date('Y-m-d') . '.xls';
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($file));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        } else {
            $objPHPExcel = new Spreadsheet();
            //$sheetsstitle=array("Inventory","Inbound BreakDown","Sales History-old","Sales History","Date Range");
            $sheetsstitle = array("Inventory", "Inbound BreakDown", "Sales History","Order Returns");
            $skuArray = [
                'CB4-GD-IS1-12-03',
                'CP3-GD1-IS1-12-07',
                'CB3-GD-IS1-12-03',
                'CP2-GD1-IS1-12-07',
                'CB2-GD-IS1-12-03',
                'CB9-GD-IS1-07-25',
                'CB10-GD-IS1-11-18',
                'CP4-GD-IS1-11-18',
                'WB1-GD-IS1-5-17',
                'WB2-GD-IS1-5-17',
                'WB3-GD-IS1-5-17',
                'LWB4-GD-IS1-05-20',
                'MW1-GD-IS1-08-16',
                'MW2-GD-IS1-08-16',
                'MW3-GD-IS1-08-14',
                'MW4-GD-IS1-05-20',
                'WB4-GD-IS1-9-13',
                'WB5-GD-IS1-9-17',
                'WB6-GD-IS1-9-17',
                'WB7-GD-IS1-05-20',
                'CB5-GD-IS1-05-03',
                'CB6-GD-IS1-05-03',
                'CB7-GD-IS1-04-08',
                'CB8-GD-IS1-05-08',
                'CB11-GD-IS1-12-19',
                'TT6-GD-IS1-12-19',
                'TT7-GD-IS1-12-19',
                '6G-OHEF-0LNU',
                'CR2-GD-IS1-08-09',
                'CR3-GD-IS1-04-11',
                'CR4-GD-IS1-04-11',
                'CR5-GD-IS1-03-20',
                'SCR1-GD-IS1-08-09',
                'SCR2-GD-IS1-08-09',
                'SCR3-GD-IS1-04-11',
                'SCR4-GD-IS1-04-11',
                'SCR5-GD-IS1-04-11',
                'CMR1- GD-IS1-08-10',
                'CMR2- GD-IS1-08-10',
                'CMR3- GD-IS1-11-18',
                'BCM16-GD-IS1',
                'BCM20-GD-IS1',
                'BCM24-GD-IS1',
                'BCM30-GD-IS1',
                'BCM36-GD-IS1',
                'GCM16-GD-IS1',
                'GCM20-GD-IS1',
                'GCM24-GD-IS1',
                'GCM30-GD-IS1',
                'GCM36-GD-IS1',
                'DOS-GD-IS1-11-18',
                'DOS2-GD-IS1-02-20',
                'DOS3-GD-IS1-04-20',
                'FC1-GD-IS1-11-18',
                'FC2-GD-IS1-11-18',
                'FC3-GD-IS1-02-20',
                'CCR1-GD-IS1-11-18',
                'PCB1-GD-IS1-11-18',
                'PCB2-GD-IS1-11-18',
                'BGCM27.5-GD-IS1-11-18',
                'BCM27.5-GD-IS1-05-09',
                'GUMM-GD-IS1-11-19',
                'BUMM-GD-IS1-11-19',
                'US1-GD-IS1-11-19',
                'US2-GD-IS1-08-19',
                'LCM1-GD1-12-05',
                'LCM2-GD1-12-05',
                'LCM3-GD1-12-05',
                'SS1-GD-IS1-03-15',
                'SS2-GD-IS1-11-12',
                'TT8-GD-IS1-08-20',
                'TT11-GD-IS1-08-20',
                'TT5-GD-IS1-08-20',
                'LWB5-GD-IS1-09-20',
                'MW5-GD-IS1-09-20',
                'WB8-GD-IS1-09-20',
                'XLWB2-GD-IS1-12-20',
                'XLWB3-GD-IS1-12-20',
                'XLWB4-GD-IS1-12-20',
                'CB12-GD-IS1-12-20',
                'CP12-GD-IS1-12-20'];
            $get_all_product_sku = Mws_product::with(['mws_reserved_inventory', 'mws_afn_inventory', 'mws_unsuppressed_inventory_data', 'daily_logic_calculations','mws_order_returns'])->whereIn('sku', $skuArray)->where(array('user_marketplace_id' => $global_marketplace))->selectRaw('sku,id')->get()->toArray();
            $i = 0;
            while ($i < 4) {

                // Add new sheet
                $objWorkSheet = $objPHPExcel->createSheet($i); //Setting index when creating
                if ($i == 0) {
                    $objWorkSheet->setCellValue('A1', 'SKU');
                    $objWorkSheet->setCellValue('B1', 'Quantity Available');
                    $objWorkSheet->setCellValue('C1', 'Afn-fulfilled-quantity');
                    $objWorkSheet->setCellValue('D1', 'Current Stock');
                    $objWorkSheet->setCellValue('E1', 'FC Transfer');
                    $objWorkSheet->setCellValue('F1', 'FC Processing');
                    $objWorkSheet->setCellValue('G1', 'Customer Order');
                    $objWorkSheet->setCellValue('H1', 'Reserved Qty');
                    $objWorkSheet->setCellValue('I1', 'Afn Research qty');
                    $objWorkSheet->setCellValue('J1', 'Afn Unsellable qty');
                    $objWorkSheet->setCellValue('K1', 'Afn-warehouse-qty');
                    $objWorkSheet->setCellValue('L1', 'Amazon Inbound Receving Qty');
                    $objWorkSheet->setCellValue('M1', 'Amazon Inbound Shipment Qty');
                    $objWorkSheet->setCellValue('N1', 'Amazon Inbound Working Qty');
                    $objWorkSheet->setCellValue('O1', 'AFN Total Qty');
                    $column = 2;
                    foreach ($get_all_product_sku as $key => $result) {
                        $objWorkSheet->setCellValue('A' . $column, $result['sku']);
                        $objWorkSheet->setCellValue('B' . $column, $result['mws_afn_inventory']['quantity_available']);
                        $objWorkSheet->setCellValue('C' . $column, $result['mws_unsuppressed_inventory_data']['afn_fulfillable_qty']);
                        $objWorkSheet->setCellValue('D' . $column, '');
                        $objWorkSheet->setCellValue('E' . $column, $result['mws_reserved_inventory']['reserved_fc_transfer']);
                        $objWorkSheet->setCellValue('F' . $column, $result['mws_reserved_inventory']['reserved_fc_processing']);
                        $objWorkSheet->setCellValue('G' . $column, $result['mws_reserved_inventory']['reserved_customer_orders']);
                        $objWorkSheet->setCellValue('H' . $column, $result['mws_reserved_inventory']['reserved_qty']);
                        $objWorkSheet->setCellValue('I' . $column, $result['mws_unsuppressed_inventory_data']['afn_research_qty']);
                        $objWorkSheet->setCellValue('J' . $column, $result['mws_unsuppressed_inventory_data']['afn_unsellable_qty']);
                        $objWorkSheet->setCellValue('K' . $column, $result['mws_unsuppressed_inventory_data']['afn_warehouse_qty']);
                        $objWorkSheet->setCellValue('L' . $column, $result['mws_unsuppressed_inventory_data']['afn_inbound_receving_qty']);
                        $objWorkSheet->setCellValue('M' . $column, $result['mws_unsuppressed_inventory_data']['afn_inbound_shipment_qty']);
                        $objWorkSheet->setCellValue('N' . $column, $result['mws_unsuppressed_inventory_data']['afn_inbound_working_qty']);
                        $objWorkSheet->setCellValue('O' . $column, $result['mws_unsuppressed_inventory_data']['afn_total_qty']);
                        $column++;
                    }
                }
                if ($i == 1) {
                    //SellerSettlement data
                    $objWorkSheet->setCellValue('A1', 'SKU');
                    $objWorkSheet->setCellValue('B1', 'Name  ');
                    $objWorkSheet->setCellValue('C1', 'Shipment ID ');
                    $objWorkSheet->setCellValue('D1', 'Shipment Status ');
                    $objWorkSheet->setCellValue('E1', 'Sent ');
                    $objWorkSheet->setCellValue('F1', 'Received ');
                    $column = 2;
                    foreach ($get_all_product_sku as $key => $result) {
                        $datas = Inbound_shipment_items::with(['inbound_shipment'])->where(array('product_id' => $result['id']))->get()->toArray();
                        $newData = array();
                        foreach ($datas as $inc => $inbound) {
//                        if($inbound['QuantityShipped'] != $inbound['QuantityReceived']) {
                            $differenceInventory = $inbound['QuantityShipped'] - $inbound['QuantityReceived'];
                            $newData[$inc]['inbound'] = $differenceInventory;
                            $newData[$inc]['ShipmentName'] = $inbound['inbound_shipment']['ShipmentName'];
                            $newData[$inc]['ShipmentId'] = $inbound['inbound_shipment']['ShipmentId'];
                            $newData[$inc]['ShipmentStatus'] = $inbound['inbound_shipment']['ShipmentStatus'];
                            $newData[$inc]['QuantityShipped'] = $inbound['QuantityShipped'];
                            $newData[$inc]['QuantityReceived'] = $inbound['QuantityReceived'];
//                        }
                        }
                        $objWorkSheet->setCellValue('A' . $column, $result['sku']);
                        if (empty($newData)) {
                            $objWorkSheet->setCellValue('D' . $column, '');
                            $objWorkSheet->setCellValue('E' . $column, '');
                            $objWorkSheet->setCellValue('F' . $column, '');
                            $column++;
                        } else {
                            foreach ($newData as $inbound) {
                                $objWorkSheet->setCellValue('B' . $column, $inbound['ShipmentName']);
                                $objWorkSheet->setCellValue('C' . $column, $inbound['ShipmentId']);
                                $objWorkSheet->setCellValue('D' . $column, $inbound['ShipmentStatus']);
                                $objWorkSheet->setCellValue('E' . $column, $inbound['QuantityShipped']);
                                $objWorkSheet->setCellValue('F' . $column, $inbound['QuantityReceived']);
                                $column++;
                            }
                        }
                    }
                }
                /*if ($i == 2) {
                    //SellerSettlement data
                    $objWorkSheet->setCellValue('A1', 'SKU');
                    $objWorkSheet->setCellValue('B1', '1 Day (Returns/Sellable)');
                    $objWorkSheet->setCellValue('C1', '1 Day (24 Hours Sales)');
                    $objWorkSheet->setCellValue('D1', '7 Days');
                    $objWorkSheet->setCellValue('E1', '14 Days');
                    $objWorkSheet->setCellValue('F1', '30 Days');
                    $objWorkSheet->setCellValue('G1', 'Previous 7 Days');
                    $objWorkSheet->setCellValue('H1', 'Previous 30 Days');
                    $objWorkSheet->setCellValue('I1', '30 Days Sales - Last Year');
                    $dynamicArray = ['J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH'];
                    for ($count=0;$count<=24;$count++) {
                        $result = date('m-Y', strtotime("-$count month"));
                        $objWorkSheet->setCellValue($dynamicArray[$count].'1', $result);
                    }
                    $get_all_product_sku = Mws_product::with(['daily_logic_calculations'])->whereIn('sku', $skuArray)->where(array('user_marketplace_id' => $global_marketplace))->selectRaw('sku,id')->get()->toArray();
                    $column = 2;
                    foreach ($get_all_product_sku as $record) {

                        $objWorkSheet->setCellValue('A' . $column, $record['sku']);
                        $objWorkSheet->setCellValue('B' . $column, '');
                        $twentyFourHoursBackDate = Carbon::yesterday()->format('Y-m-d');
                        $twentyFourHoursSales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '>=', $twentyFourHoursBackDate)->sum('total_qty');
                        if ($twentyFourHoursSales > 0) {
                            $twentyFourHoursyRate = round(($twentyFourHoursSales / 1), 6);
                        } else {
                            $twentyFourHoursSales = $twentyFourHoursyRate = 0;
                        }

                        $objWorkSheet->setCellValue('C' . $column, $twentyFourHoursSales);
                            $sevenDaysBackDate = Carbon::now()->subDays(7)->format('Y-m-d');
                            $sevenDaysSales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '>=', $sevenDaysBackDate)->sum('total_qty');
                            if ($sevenDaysSales > 0) {
                                $sevenDaysDayRate = round(($sevenDaysSales / 7), 6);
                            } else {
                                $sevenDaysSales = $sevenDaysDayRate = 0;
                            }

                            $fourteenDaysBackDate = Carbon::now()->subDays(14)->format('Y-m-d');
                            $fourteenDaysSales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '>=', $fourteenDaysBackDate)->sum('total_qty');
                            if ($fourteenDaysSales > 0) {
                                $fourteenDaysDayRate = round(($fourteenDaysSales / 14), 6);
                            } else {
                                $fourteenDaysSales = $fourteenDaysDayRate = $fourteenDaysDayOfSupply = 0;
                            }
                            $thirtyDaysBackDate = Carbon::now()->subDays(30)->format('Y-m-d');
                            $thirtyDaysSales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '>=', $thirtyDaysBackDate)->sum('total_qty');
                            if ($thirtyDaysSales > 0) {
                                $thirtyDaysDayRate = round(($thirtyDaysSales / 30), 6);
                            } else {
                                $thirtyDaysSales = $thirtyDaysDayRate = $thirtyDaysDayOfSupply = 0;
                            }
                            $fourteenDaysBackDate = Carbon::now()->subDays(14)->format('Y-m-d');
                            $previousSevenDaysSales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '<=', $sevenDaysBackDate)->where('order_date', '>=', $fourteenDaysBackDate)->sum('total_qty');
                            if ($previousSevenDaysSales > 0) {
                                $previousSevenDaysDayRate = round(($previousSevenDaysSales / 7), 6);
                            } else {
                                $previousSevenDaysDayRate = 0;
                            }
                            $thirtyDaysBackDate = Carbon::now()->subDays(30)->format('Y-m-d');
                            $sixtyDaysBackDate = Carbon::now()->subDays(60)->format('Y-m-d');
                            $previousThirtyDaysSales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '<=', $thirtyDaysBackDate)->where('order_date', '>=', $sixtyDaysBackDate)->sum('total_qty');
                            if ($previousThirtyDaysSales > 0) {
                                $previousThirtyDaysDayRate = round(($previousThirtyDaysSales / 30), 6);
                            } else {
                                $previousThirtyDaysDayRate = 0;
                            }
                            $thirtyDaysStartDateLastYear = Carbon::now()->subDays(395)->format('Y-m-d');
                            $thirtyDaysEndDateLastYear = Carbon::now()->subDays(365)->format('Y-m-d');
                            $thirtyDaysSalesLastyear = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '<=', $thirtyDaysEndDateLastYear)->where('order_date', '>=', $thirtyDaysStartDateLastYear)->sum('total_qty');
                            if ($thirtyDaysSalesLastyear > 0) {
                                $ThirtyDaysDayRateLastYear = round(($thirtyDaysSalesLastyear / 30), 6);
                            } else {
                                $ThirtyDaysDayRateLastYear = 0;
                            }

                        $objWorkSheet->setCellValue('D' . $column, $sevenDaysSales);
                        $objWorkSheet->setCellValue('E' . $column, $fourteenDaysSales);
                        $objWorkSheet->setCellValue('F' . $column, $thirtyDaysSales);
                        $objWorkSheet->setCellValue('G' . $column, $previousSevenDaysSales);
                        $objWorkSheet->setCellValue('H' . $column, $previousThirtyDaysSales);
                        $objWorkSheet->setCellValue('I' . $column, $thirtyDaysSalesLastyear);
                        for ($count=0;$count<=24;$count++){
                            $get_datemonth =  date('Y-m',strtotime("-$count month"));
                            $get_supply_total_trands = get_supply_total_trands($get_datemonth,$record['id']);
                            if(!empty($get_supply_total_trands)){
                                $objWorkSheet->setCellValue($dynamicArray[$count]. $column,$get_supply_total_trands->sales_total);
                            }else{
                                $objWorkSheet->setCellValue($dynamicArray[$count]. $column,'');
                            }
                        }
                        $column++;
                        $objWorkSheet->setCellValue('A' . $column, '');
                        $objWorkSheet->setCellValue('B' . $column, '');
                        $objWorkSheet->setCellValue('C' . $column, $twentyFourHoursyRate);
                        $objWorkSheet->setCellValue('D' . $column, $sevenDaysDayRate);
                        $objWorkSheet->setCellValue('E' . $column, $fourteenDaysDayRate);
                        $objWorkSheet->setCellValue('F' . $column, $thirtyDaysDayRate);
                        $objWorkSheet->setCellValue('G' . $column, $previousSevenDaysDayRate);
                        $objWorkSheet->setCellValue('H' . $column, $previousThirtyDaysDayRate);
                        $objWorkSheet->setCellValue('I' . $column, $ThirtyDaysDayRateLastYear);
                         for ($count=0;$count<=24;$count++){
                             $get_datemonth =  date('Y-m',strtotime("-$count month"));
                             $get_supply_total_trands = get_supply_total_trands($get_datemonth,$record['id']);
                             if(!empty($get_supply_total_trands)){
                                 $objWorkSheet->setCellValue($dynamicArray[$count]. $column,$get_supply_total_trands->day_rate);
                             }else{
                                 $objWorkSheet->setCellValue($dynamicArray[$count]. $column,'');
                             }
                         }
                         $column++;
                    }
                } */
                if ($i == 2)
                {
                    //SellerSettlement data
                    $objWorkSheet->setCellValue('A1', 'SKU');
                    $objWorkSheet->setCellValue('B1', '1 Day (Returns/Sellable)');
                    $objWorkSheet->setCellValue('C1', '1 Day (24 Hours Sales)');
                    $objWorkSheet->setCellValue('D1', '7 Days');
                    $objWorkSheet->setCellValue('E1', 'Start Date');
                    $objWorkSheet->setCellValue('F1', 'End Date');
                    $objWorkSheet->setCellValue('G1', '14 Days');
                    $objWorkSheet->setCellValue('H1', 'Start Date');
                    $objWorkSheet->setCellValue('I1', 'End Date');
                    $objWorkSheet->setCellValue('J1', '30 Days');
                    $objWorkSheet->setCellValue('K1', 'Start Date');
                    $objWorkSheet->setCellValue('L1', 'End Date');
                    $objWorkSheet->setCellValue('M1', 'Previous 7 Days');
                    $objWorkSheet->setCellValue('N1', 'Start Date');
                    $objWorkSheet->setCellValue('O1', 'End Date');
                    $objWorkSheet->setCellValue('P1', 'Previous 30 Days');
                    $objWorkSheet->setCellValue('Q1', 'Start Date');
                    $objWorkSheet->setCellValue('R1', 'End Date');
                    $objWorkSheet->setCellValue('S1', '30 Days Sales - Last Year');
                    $dynamicArray = ['T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS'];

                    for ($count = 0; $count <= 24; $count++) {
                        $result = date('m-Y', strtotime("-$count month"));
                        $objWorkSheet->setCellValue($dynamicArray[$count] . '1', $result);
                    }
                    $column = 2;
                    foreach ($get_all_product_sku as $record) {
                        $sevenDaysStartDate = $sevenDaysEndDate = '';
                        $sevenDaysSales = $fourteenDaysSales = $thirtyDaysSales = $previousSevenDaysSales = $previousThirtyDaysSales = 0;
                        $previoussevenDaysStartDate = $previoussevenDaysEndDate = '';
                        $fourteenDaysStartDate = $fourteenDaysEndDate = '';
                        $thirtyDaysStartDate = $thirtyDaysEndDate = '';
                        $previousthirtyDaysStartDate = $previousthirtyDaysEndDate = '';
                        $breakFlag = 0;
                        $sevenDayFlag = $fourteenDayFlag = $thirtyDayFlag = $previousSevenFlag = $previousThirtyFlag = 0;
                        $startDate = date('Y-m-d');
                        $endDate = date('Y-m-d', strtotime('2020-01-01'));
                        $dailySales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '>=', $endDate)->orderBy('order_date', 'desc')->pluck('total_qty', 'order_date')->toArray();
                        $dailyInventory = Mws_calculated_inventory_daily::where('product_id', $record['id'])->where('inventory_date', '>=', date('Y-m-d 00:00:00', strtotime($endDate)))->orderBy('inventory_date', 'desc')->pluck('qty', 'inventory_date')->toArray();
                        $sevendayCount = $previoussevendayCount = $fourteendayCount = $thirtydayCount = $previousthirtydayCount = 0;
                        $dateArray = $this->displayDates($startDate, $endDate);
                        foreach ($dateArray as $key => $result) {
                            $count = ($key + 1);
                            if ($count == 1 || $count == 2) {
                                $newInv = Mws_unsuppressed_historical_inventory::where('product_id', $record['id'])->where('inventory_for_date',date('Y-m-d 00:00:00', strtotime($result)))->pluck('afn_total_qty', 'inventory_for_date')->toArray();
                                $newInv_ref = 0;
                                if(!empty($newInv)){
                                    $newInv_ref = array_values($newInv)[0];
                                    $dailyInventory[$result . ' 00:00:00'] = $newInv_ref;
                                }
                            }
                            if (!isset($dailyInventory[$result . ' 00:00:00']))
                                $dailyInventory[$result . ' 00:00:00'] = 0;
                            if (!isset($dailySales[$result]))
                                $dailySales[$result] = 0;
                            if ($count == 1) {
                                $sevenDaysStartDate = $result;
                                $fourteenDaysStartDate = $result;
                                $thirtyDaysStartDate = $result;
                            }
                            if ($sevendayCount < 7) {

                                if ($dailySales[$result] == 0 || $dailyInventory[$result . ' 00:00:00'] == 0) {
                                    $sevendayCount = $fourteendayCount = $thirtydayCount = 0;
                                    $sevenDayFlag++;
                                    $fourteenDayFlag++;
                                    $thirtyDayFlag++;
                                    $previousSevenFlag++;
                                    $previousThirtyFlag++;
                                    $sevenDaysStartDate = date('Y-m-d', strtotime("-1 days", strtotime($result)));
                                    $fourteenDaysStartDate = date('Y-m-d', strtotime("-1 days", strtotime($result)));
                                    $thirtyDaysStartDate = date('Y-m-d', strtotime("-1 days", strtotime($result)));
                                } else {
                                    $sevendayCount++;
                                    $fourteendayCount++;
                                    $thirtydayCount++;
                                }
                            }
                            if ($sevendayCount == 7) {
                                if (empty($sevenDaysEndDate)) {
                                    $sevenDaysEndDate = $result;
                                    $previoussevenDaysStartDate = date('Y-m-d', strtotime("-1 days", strtotime($result)));
                                }
                                if ($fourteendayCount <= 14) {
                                    if ($dailySales[$result] == 0 || $dailyInventory[$result . ' 00:00:00'] == 0) {
                                        $fourteendayCount = $thirtydayCount = $previoussevendayCount = 0;
                                        $previoussevenDaysStartDate = date('Y-m-d', strtotime("-2 days", strtotime($result)));
                                        $fourteenDaysStartDate = date('Y-m-d', strtotime("-2 days", strtotime($result)));
                                        $thirtyDaysStartDate = date('Y-m-d', strtotime("-2 days", strtotime($result)));
                                    } else {
                                        if ($previoussevendayCount == 7) {
                                            $previoussevenDaysEndDate = $result;
                                        }
                                        $previoussevendayCount++;
                                        $fourteendayCount++;
                                        $thirtydayCount++;
                                    }
                                }
                                if ($fourteendayCount == 15) {
                                    if (empty($fourteenDaysEndDate)) {
                                        $fourteenDaysEndDate = $result;
//                                        $previoussevenDaysEndDate = $result;
                                    }
                                    if ($thirtydayCount <= 31) {
                                        if ($dailySales[$result] == 0 || $dailyInventory[$result . ' 00:00:00'] == 0) {
                                            $thirtydayCount = 0;
                                            $thirtyDaysStartDate = date('Y-m-d', strtotime("-3 days", strtotime($result)));
                                        } else {
                                            $thirtydayCount++;
                                        }
                                    }
                                }
                                if ($thirtydayCount == 32) {
                                    if (empty($thirtyDaysEndDate)) {
                                        $thirtyDaysEndDate = $result;
                                        $previousthirtyDaysStartDate = date('Y-m-d', strtotime("-1 days", strtotime($result)));
                                    }
                                    if ($previousthirtydayCount <= 31) {
                                        if ($dailySales[$result] == 0 || $dailyInventory[$result . ' 00:00:00'] == 0) {
                                            $previousthirtydayCount = 0;
                                            $previousthirtyDaysStartDate = date('Y-m-d', strtotime("-2 days", strtotime($result)));
                                        } else {
                                            if ($previousthirtydayCount == 30) {
                                                $previousthirtyDaysEndDate = $result;
                                            }
                                            $previousthirtydayCount++;
                                        }
                                    }
                                }
                                if ($previousthirtydayCount == 32 && empty($previousthirtyDaysEndDate)) {
                                    $previousthirtyDaysEndDate = $result;
                                    $breakFlag = 1;
                                }
                            }
                            if ($breakFlag == 1)
                                break;
                        }
                        if (empty($fourteenDaysEndDate)) {
                            $previoussevenDaysEndDate = '';
                        }
                        $objWorkSheet->setCellValue('A' . $column, $record['sku']);
                        $objWorkSheet->setCellValue('B' . $column, '');
                        $twentyFourHoursBackDate = Carbon::yesterday()->format('Y-m-d');
                        $twentyFourHoursSales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '>=', $twentyFourHoursBackDate)->sum('total_qty');
                        if ($twentyFourHoursSales > 0) {
                            $twentyFourHoursyRate = round(($twentyFourHoursSales / 1), 6);
                        } else {
                            $twentyFourHoursSales = $twentyFourHoursyRate = 0;
                        }

                        $objWorkSheet->setCellValue('C' . $column, $twentyFourHoursSales);
                        if (!empty($sevenDaysStartDate) && !empty($sevenDaysEndDate))
                            $sevenDaysSales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '>=', $sevenDaysEndDate)->where('order_date', '<=', $sevenDaysStartDate)->sum('total_qty');

                        if ($sevenDaysSales > 0) {
                            $sevenDaysDayRate = round(($sevenDaysSales / 7), 6);
                        } else {
                            $sevenDaysSales = $sevenDaysDayRate = 0;
                        }

                        if (!empty($fourteenDaysStartDate) && !empty($fourteenDaysEndDate))
                            $fourteenDaysSales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '>=', $fourteenDaysEndDate)->where('order_date', '<=', $fourteenDaysStartDate)->sum('total_qty');

                        if ($fourteenDaysSales > 0) {
                            $fourteenDaysDayRate = round(($fourteenDaysSales / 14), 6);
                        } else {
                            $fourteenDaysSales = $fourteenDaysDayRate = $fourteenDaysDayOfSupply = 0;
                        }

                        if (!empty($thirtyDaysStartDate) && !empty($thirtyDaysEndDate))
                            $thirtyDaysSales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '>=', $thirtyDaysEndDate)->where('order_date', '<=', $thirtyDaysStartDate)->sum('total_qty');
                        if ($thirtyDaysSales > 0) {
                            $thirtyDaysDayRate = round(($thirtyDaysSales / 30), 6);
                        } else {
                            $thirtyDaysSales = $thirtyDaysDayRate = $thirtyDaysDayOfSupply = 0;
                        }

                        if (!empty($previoussevenDaysStartDate) && !empty($previoussevenDaysEndDate))
                            $previousSevenDaysSales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '>=', $previoussevenDaysEndDate)->where('order_date', '<=', $previoussevenDaysStartDate)->sum('total_qty');
                        if ($previousSevenDaysSales > 0) {
                            $previousSevenDaysDayRate = round(($previousSevenDaysSales / 7), 6);
                        } else {
                            $previousSevenDaysSales = $previousSevenDaysDayRate = 0;
                        }
                        if (!empty($previousthirtyDaysStartDate) && !empty($previousthirtyDaysEndDate))
                            $previousThirtyDaysSales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '>=', $previousthirtyDaysEndDate)->where('order_date', '<=', $previousthirtyDaysStartDate)->sum('total_qty');

                        if ($previousThirtyDaysSales > 0) {
                            $previousThirtyDaysDayRate = round(($previousThirtyDaysSales / 30), 6);
                        } else {
                            $previousThirtyDaysSales = $previousThirtyDaysDayRate = 0;
                        }
                        $thirtyDaysStartDateLastYear = Carbon::now()->subDays(395)->format('Y-m-d');
                        $thirtyDaysEndDateLastYear = Carbon::now()->subDays(365)->format('Y-m-d');
                        $thirtyDaysSalesLastyear = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '<=', $thirtyDaysEndDateLastYear)->where('order_date', '>=', $thirtyDaysStartDateLastYear)->sum('total_qty');
                        if ($thirtyDaysSalesLastyear > 0) {
                            $ThirtyDaysDayRateLastYear = round(($thirtyDaysSalesLastyear / 30), 6);
                        } else {
                            $ThirtyDaysDayRateLastYear = 0;
                        }


                        $objWorkSheet->setCellValue('D' . $column, $sevenDaysSales);
                        $objWorkSheet->setCellValue('E' . $column, (($sevenDayFlag > 0 && isset($sevenDaysStartDate) && $sevenDaysStartDate != '01-01-1970' && $sevenDaysStartDate != '' && isset($sevenDaysEndDate) && $sevenDaysEndDate != '01-01-1970' && $sevenDaysEndDate != '') ? date('m-d-Y', strtotime($sevenDaysStartDate)) : ''));
                        $objWorkSheet->setCellValue('F' . $column, (($sevenDayFlag > 0 && isset($sevenDaysEndDate) && $sevenDaysEndDate != '01-01-1970' && $sevenDaysEndDate != '') ? date('m-d-Y', strtotime($sevenDaysEndDate)) : ''));
                        $objWorkSheet->setCellValue('G' . $column, $fourteenDaysSales);
                        $objWorkSheet->setCellValue('H' . $column, (($fourteenDayFlag > 0 && isset($fourteenDaysStartDate) && isset($fourteenDaysEndDate) && $fourteenDaysStartDate != '01-01-1970' && $fourteenDaysStartDate != '' && $fourteenDaysEndDate != '01-01-1970' && $fourteenDaysEndDate != '') ? date('m-d-Y', strtotime($fourteenDaysStartDate)) : ''));
                        $objWorkSheet->setCellValue('I' . $column, (($fourteenDayFlag > 0 && isset($fourteenDaysEndDate) && $fourteenDaysEndDate != '01-01-1970' && $fourteenDaysEndDate != '') ? date('m-d-Y', strtotime($fourteenDaysEndDate)) : ''));
                        $objWorkSheet->setCellValue('J' . $column, $thirtyDaysSales);
                        $objWorkSheet->setCellValue('K' . $column, (($thirtyDayFlag > 0 && isset($thirtyDaysStartDate) && $thirtyDaysStartDate != '01-01-1970' && $thirtyDaysStartDate != '' && isset($thirtyDaysEndDate) && $thirtyDaysEndDate != '01-01-1970' && $thirtyDaysEndDate != '') ? date('m-d-Y', strtotime($thirtyDaysStartDate)) : ''));
                        $objWorkSheet->setCellValue('L' . $column, (($thirtyDayFlag > 0 && isset($thirtyDaysEndDate) && $thirtyDaysEndDate != '01-01-1970' && $thirtyDaysEndDate != '') ? date('m-d-Y', strtotime($thirtyDaysEndDate)) : ''));
                        $objWorkSheet->setCellValue('M' . $column, $previousSevenDaysSales);
                        $objWorkSheet->setCellValue('N' . $column, (($previousSevenFlag > 0 && isset($previoussevenDaysStartDate) && $previoussevenDaysStartDate != '01-01-1970' && $previoussevenDaysStartDate != '' && isset($previoussevenDaysEndDate) && $previoussevenDaysEndDate != '01-01-1970' && $previoussevenDaysEndDate != '') ? date('m-d-Y', strtotime($previoussevenDaysStartDate)) : ''));
                        $objWorkSheet->setCellValue('O' . $column, (($previousSevenFlag > 0 && isset($previoussevenDaysEndDate) && $previoussevenDaysEndDate != '01-01-1970' && $previoussevenDaysEndDate != '') ? date('m-d-Y', strtotime($previoussevenDaysEndDate)) : ''));
                        $objWorkSheet->setCellValue('P' . $column, $previousThirtyDaysSales);
                        $objWorkSheet->setCellValue('Q' . $column, (($previousThirtyFlag > 0 && isset($previousthirtyDaysStartDate) && $previousthirtyDaysStartDate != '01-01-1970' && $previousthirtyDaysStartDate != '' && isset($previousthirtyDaysEndDate) && $previousthirtyDaysEndDate != '01-01-1970' && $previousthirtyDaysEndDate != '') ? date('m-d-Y', strtotime($previousthirtyDaysStartDate)) : ''));
                        $objWorkSheet->setCellValue('R' . $column, (($previousThirtyFlag > 0 && isset($previousthirtyDaysEndDate) && $previousthirtyDaysEndDate != '01-01-1970' && $previousthirtyDaysEndDate != '') ? date('m-d-Y', strtotime($previousthirtyDaysEndDate)) : ''));
                        $objWorkSheet->setCellValue('S' . $column, $thirtyDaysSalesLastyear);
                        for ($count = 0; $count <= 24; $count++) {
                            $get_datemonth = date('Y-m', strtotime("-$count month"));
                            $get_supply_total_trands = get_supply_total_trands($get_datemonth, $record['id']);
                            if (!empty($get_supply_total_trands)) {
                                $objWorkSheet->setCellValue($dynamicArray[$count] . $column, $get_supply_total_trands->sales_total);
                            } else {
                                $objWorkSheet->setCellValue($dynamicArray[$count] . $column, '');
                            }
                        }
                        $column++;
                        $objWorkSheet->setCellValue('A' . $column, '');
                        $objWorkSheet->setCellValue('B' . $column, '');
                        $objWorkSheet->setCellValue('C' . $column, $twentyFourHoursyRate);
                        $objWorkSheet->setCellValue('D' . $column, $sevenDaysDayRate);
                        $objWorkSheet->setCellValue('E' . $column, '');
                        $objWorkSheet->setCellValue('F' . $column, '');
                        $objWorkSheet->setCellValue('G' . $column, $fourteenDaysDayRate);
                        $objWorkSheet->setCellValue('H' . $column, '');
                        $objWorkSheet->setCellValue('I' . $column, '');
                        $objWorkSheet->setCellValue('J' . $column, $thirtyDaysDayRate);
                        $objWorkSheet->setCellValue('K' . $column, '');
                        $objWorkSheet->setCellValue('L' . $column, '');
                        $objWorkSheet->setCellValue('M' . $column, $previousSevenDaysDayRate);
                        $objWorkSheet->setCellValue('N' . $column, '');
                        $objWorkSheet->setCellValue('O' . $column, '');
                        $objWorkSheet->setCellValue('P' . $column, $previousThirtyDaysDayRate);
                        $objWorkSheet->setCellValue('Q' . $column, '');
                        $objWorkSheet->setCellValue('R' . $column, '');
                        $objWorkSheet->setCellValue('S' . $column, $ThirtyDaysDayRateLastYear);
                        for ($count = 0; $count <= 24; $count++) {
                            $get_datemonth = date('Y-m', strtotime("-$count month"));
                            $get_supply_total_trands = get_supply_total_trands($get_datemonth, $record['id']);
                            if (!empty($get_supply_total_trands)) {
                                $objWorkSheet->setCellValue($dynamicArray[$count] . $column, $get_supply_total_trands->day_rate);
                            } else {
                                $objWorkSheet->setCellValue($dynamicArray[$count] . $column, '');
                            }
                        }
                        $column++;
                    }
                }
                if ($i == 3)
                {
                    //order returns data
                    $objWorkSheet->setCellValue('A1', 'SKU');
                    $objWorkSheet->setCellValue('B1', 'Return Date');
                    $objWorkSheet->setCellValue('C1', 'Product Name');
                    $objWorkSheet->setCellValue('D1', 'ASIN');
                    $objWorkSheet->setCellValue('E1', 'FNSKU');
                    $objWorkSheet->setCellValue('F1', 'Quantity');
                    $objWorkSheet->setCellValue('G1', 'Fulfillment Center Id');
                    $objWorkSheet->setCellValue('H1', 'Detailed Disposition');
                    $objWorkSheet->setCellValue('I1', 'Reason');
                    $objWorkSheet->setCellValue('J1', 'Status');
                    $objWorkSheet->setCellValue('K1', 'License Plate Number');
                    $objWorkSheet->setCellValue('L1', 'Customer Comments');
                    $column = 2;

                    foreach ($get_all_product_sku as $key => $result1)
                    {
                        $objWorkSheet->setCellValue('A' . $column, $result1['sku']);
                        if(!empty($result1['mws_order_returns'])){
                            foreach ($result1['mws_order_returns'] as $key => $result) {
                                $date_rt = date('m/d/Y',strtotime($result['return_date']));
                                $cmt_rt = $result['customer_comments'] != null ? $result['customer_comments'] : "-";
                                $objWorkSheet->setCellValue('B' . $column, $date_rt);
                                $objWorkSheet->setCellValue('C' . $column, $result['product_name']);
                                $objWorkSheet->setCellValue('D' . $column, $result['asin']);
                                $objWorkSheet->setCellValue('E' . $column, $result['fnsku']);
                                $objWorkSheet->setCellValue('F' . $column, $result['quantity']);
                                $objWorkSheet->setCellValue('G' . $column, $result['fulfillment_center_id']);
                                $objWorkSheet->setCellValue('H' . $column, $result['detailed_disposition']);
                                $objWorkSheet->setCellValue('I' . $column, $result['reason']);
                                $objWorkSheet->setCellValue('J' . $column, $result['status']);
                                $objWorkSheet->setCellValue('K' . $column, $result['license_plate_number']);
                                $objWorkSheet->setCellValue('L' . $column, $cmt_rt);
                                $column++;
                            }
                        }else{
                            $column++;
                        }
                    }
                }
                /*if($i==4){
                    $dynamicArray = ['A','B','C','D','E','F','G','H','I','J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH','AI','AJ','AK','Al','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','Ay','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ'];
                    $get_all_product_sku = Mws_product::whereIn('sku', $skuArray)->where(array('user_marketplace_id' => $global_marketplace))->selectRaw('sku,id')->get()->toArray();
                    $column =2;
                    $objWorkSheet->setCellValue('A1', 'SKU');
                    for ($count=1;$count<=73;$count++) {
                        $result = date('Y-m-d', strtotime("-$count days"));
                        $objWorkSheet->setCellValue($dynamicArray[$count] . '1', $result);
                    }
                    foreach ($get_all_product_sku as $record){
                        $sevendayCount = $fourteendayCount = $thirtydayCount = 0;
                        $sevenDaysStartDate = $sevenDaysEndDate = '';
                        $fourteenDaysStartDate = $fourteenDaysEndDate = '';
                        $thirtyDaysStartDate = $thirtyDaysEndDate = '';
                        $breakFlag = 0;
                        $objWorkSheet->setCellValue('A'.$column, $record['sku']);
                        $objWorkSheet->setCellValue('A'.($column+1), 'Sales');
                        $objWorkSheet->setCellValue('A'.($column+2), 'Stock');
                        for ($count=1;$count<=73;$count++) {
                            $result = date('Y-m-d', strtotime("-$count days"));
                            $dailySales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '=', $result)->sum('total_qty');
                            $dailyInventory = Mws_calculated_inventory_daily::where('product_id', $record['id'])->where('inventory_date', '=', $result. ' 00:00:00')->sum('qty');
                            if($count==1) {
                                $sevenDaysStartDate = $result;
                                $fourteenDaysStartDate = $result;
                                $thirtyDaysStartDate = $result;
                            }
                            if($sevendayCount <7) {
                                if ($dailySales == 0 && $dailyInventory == 0) {
                                    $sevendayCount = $fourteendayCount = $thirtydayCount = 0;
                                    $sevenDaysStartDate = date('Y-m-d', strtotime("-1 days",strtotime($result)));
                                    $fourteenDaysStartDate = date('Y-m-d', strtotime("-1 days",strtotime($result)));
                                    $thirtyDaysStartDate = date('Y-m-d', strtotime("-1 days",strtotime($result)));
                                } else {
                                    $sevendayCount++;
                                    $fourteendayCount++;
                                    $thirtydayCount++;
                                }
                            }
                            if($sevendayCount==7){
                                if(empty($sevenDaysEndDate))
                                    $sevenDaysEndDate = $result;
                                if($fourteendayCount <=14) {
                                    if ($dailySales == 0 && $dailyInventory == 0) {
                                        $fourteendayCount = $thirtydayCount = 0;
                                        $fourteenDaysStartDate = date('Y-m-d', strtotime("-1 days",strtotime($result)));
                                        $thirtyDaysStartDate = date('Y-m-d', strtotime("-1 days",strtotime($result)));
                                    } else {
                                        $fourteendayCount++;
                                        $thirtydayCount++;
                                    }
                                }
                                if($fourteendayCount==15){
                                    if(empty($fourteenDaysEndDate))
                                        $fourteenDaysEndDate = $result;
                                    if($thirtydayCount <= 31) {
                                        if ($dailySales == 0 && $dailyInventory == 0) {
                                            $thirtydayCount = 0;
                                            $thirtyDaysStartDate = date('Y-m-d', strtotime("-1 days",strtotime($result)));
                                        } else {
                                            $thirtydayCount++;
                                        }
                                    }
                                }
                                if($thirtydayCount == 32) {
                                    $thirtyDaysEndDate = $result;
                                    $breakFlag = 1;
                                }
                            }
                            if($breakFlag == 1)
                                break;
                            $objWorkSheet->setCellValue($dynamicArray[$count] . ($column + 1), $dailySales);
                            $objWorkSheet->setCellValue($dynamicArray[$count] . ($column + 2), $dailyInventory);
                        }
                        $column = $column+3;
                        $objWorkSheet->setCellValue('D'.$column, '7 Days');
                        $objWorkSheet->setCellValue('E'.$column, $sevenDaysStartDate);
                        $objWorkSheet->setCellValue('F'.$column, '');
                        $objWorkSheet->setCellValue('G'.$column, $sevenDaysEndDate);
                        $column++;
                        $objWorkSheet->setCellValue('D'.$column, '14 Days');
                        $objWorkSheet->setCellValue('E'.$column, $fourteenDaysStartDate);
                        $objWorkSheet->setCellValue('F'.$column, '');
                        $objWorkSheet->setCellValue('G'.$column, $fourteenDaysEndDate);
                        $column++;
                        $objWorkSheet->setCellValue('D'.$column, '30 Days');
                        $objWorkSheet->setCellValue('E'.$column, $thirtyDaysStartDate);
                        $objWorkSheet->setCellValue('F'.$column, '');
                        $objWorkSheet->setCellValue('G'.$column, $thirtyDaysEndDate);
                        $column++;
                    }
                }*/
                $objWorkSheet->setTitle($sheetsstitle[$i]);
                $i++;
            }
            $objPHPExcel->setActiveSheetIndex($i - 4);
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="Stockful_' . $global_marketplace . '_' . date('Y-m-d') . '.xls"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            $writer = new Xlsx($objPHPExcel);
            $writer->save('php://output');
            exit();
        }
    }

    /**
     * Show & calulated with new inventory for download .
     * @method GET
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show_new_calculated($id)
    {
        //
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');
        $global_marketplace = session('MARKETPLACE_ID');
        if (empty($global_marketplace)) {
            $global_marketplace = $id;
        }
        $file = base_path() . '/download/Stockful_' . $global_marketplace . '_' . date('Y-m-d') . '.xls';
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($file));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        } else {
            $objPHPExcel = new Spreadsheet();
            //$sheetsstitle=array("Inventory","Inbound BreakDown","Sales History-old","Sales History","Date Range");
            $sheetsstitle = array("Inventory", "Inbound BreakDown", "Sales History","Order Returns");
            $skuArray = [
                'CB4-GD-IS1-12-03',
                'CP3-GD1-IS1-12-07',
                'CB3-GD-IS1-12-03',
                'CP2-GD1-IS1-12-07',
                'CB2-GD-IS1-12-03',
                'CB9-GD-IS1-07-25',
                'CB10-GD-IS1-11-18',
                'CP4-GD-IS1-11-18',
                'WB1-GD-IS1-5-17',
                'WB2-GD-IS1-5-17',
                'WB3-GD-IS1-5-17',
                'LWB4-GD-IS1-05-20',
                'MW1-GD-IS1-08-16',
                'MW2-GD-IS1-08-16',
                'MW3-GD-IS1-08-14',
                'MW4-GD-IS1-05-20',
                'WB4-GD-IS1-9-13',
                'WB5-GD-IS1-9-17',
                'WB6-GD-IS1-9-17',
                'WB7-GD-IS1-05-20',
                'CB5-GD-IS1-05-03',
                'CB6-GD-IS1-05-03',
                'CB7-GD-IS1-04-08',
                'CB8-GD-IS1-05-08',
                'CB11-GD-IS1-12-19',
                'TT6-GD-IS1-12-19',
                'TT7-GD-IS1-12-19',
                '6G-OHEF-0LNU',
                'CR2-GD-IS1-08-09',
                'CR3-GD-IS1-04-11',
                'CR4-GD-IS1-04-11',
                'CR5-GD-IS1-03-20',
                'SCR1-GD-IS1-08-09',
                'SCR2-GD-IS1-08-09',
                'SCR3-GD-IS1-04-11',
                'SCR4-GD-IS1-04-11',
                'SCR5-GD-IS1-04-11',
                'CMR1- GD-IS1-08-10',
                'CMR2- GD-IS1-08-10',
                'CMR3- GD-IS1-11-18',
                'BCM16-GD-IS1',
                'BCM20-GD-IS1',
                'BCM24-GD-IS1',
                'BCM30-GD-IS1',
                'BCM36-GD-IS1',
                'GCM16-GD-IS1',
                'GCM20-GD-IS1',
                'GCM24-GD-IS1',
                'GCM30-GD-IS1',
                'GCM36-GD-IS1',
                'DOS-GD-IS1-11-18',
                'DOS2-GD-IS1-02-20',
                'DOS3-GD-IS1-04-20',
                'FC1-GD-IS1-11-18',
                'FC2-GD-IS1-11-18',
                'FC3-GD-IS1-02-20',
                'CCR1-GD-IS1-11-18',
                'PCB1-GD-IS1-11-18',
                'PCB2-GD-IS1-11-18',
                'BGCM27.5-GD-IS1-11-18',
                'BCM27.5-GD-IS1-05-09',
                'GUMM-GD-IS1-11-19',
                'BUMM-GD-IS1-11-19',
                'US1-GD-IS1-11-19',
                'US2-GD-IS1-08-19',
                'LCM1-GD1-12-05',
                'LCM2-GD1-12-05',
                'LCM3-GD1-12-05',
                'SS1-GD-IS1-03-15',
                'SS2-GD-IS1-11-12',
                'TT8-GD-IS1-08-20',
                'TT11-GD-IS1-08-20',
                'TT5-GD-IS1-08-20',
                'LWB5-GD-IS1-09-20',
                'MW5-GD-IS1-09-20',
                'WB8-GD-IS1-09-20',
                'XLWB2-GD-IS1-12-20',
                'XLWB3-GD-IS1-12-20',
                'XLWB4-GD-IS1-12-20',
                'CB12-GD-IS1-12-20',
                'CP12-GD-IS1-12-20',
                'CCR2-GD-IS1-02-21',
                'CCR3-GD-IS1-02-21',
                'CCR2-GD-IS1-02-21',
                'CCR3-GD-IS1-02-21',
                'XLWB1-GD-IS1-03-21',
                'XLWB5-GD-IS1-03-21',
                'DOS4-GD-IS1-04-21'];
            $get_all_product_sku = Mws_product::with(['mws_reserved_inventory', 'mws_afn_inventory', 'mws_unsuppressed_inventory_data', 'daily_logic_calculations','mws_order_returns'])->whereIn('sku', $skuArray)->where(array('user_marketplace_id' => $global_marketplace))->selectRaw('sku,id')->get()->toArray();
            $i = 0;
            while ($i < 4) {

                // Add new sheet
                $objWorkSheet = $objPHPExcel->createSheet($i); //Setting index when creating
                if ($i == 0) {
                    $objWorkSheet->setCellValue('A1', 'SKU');
                    $objWorkSheet->setCellValue('B1', 'Quantity Available');
                    $objWorkSheet->setCellValue('C1', 'Afn-fulfilled-quantity');
                    $objWorkSheet->setCellValue('D1', 'Current Stock');
                    $objWorkSheet->setCellValue('E1', 'FC Transfer');
                    $objWorkSheet->setCellValue('F1', 'FC Processing');
                    $objWorkSheet->setCellValue('G1', 'Customer Order');
                    $objWorkSheet->setCellValue('H1', 'Reserved Qty');
                    $objWorkSheet->setCellValue('I1', 'Afn Research qty');
                    $objWorkSheet->setCellValue('J1', 'Afn Unsellable qty');
                    $objWorkSheet->setCellValue('K1', 'Afn-warehouse-qty');
                    $objWorkSheet->setCellValue('L1', 'Amazon Inbound Receving Qty');
                    $objWorkSheet->setCellValue('M1', 'Amazon Inbound Shipment Qty');
                    $objWorkSheet->setCellValue('N1', 'Amazon Inbound Working Qty');
                    $objWorkSheet->setCellValue('O1', 'AFN Total Qty');
                    $column = 2;
                    foreach ($get_all_product_sku as $key => $result) {
                        $objWorkSheet->setCellValue('A' . $column, $result['sku']);
                        $objWorkSheet->setCellValue('B' . $column, $result['mws_afn_inventory']['quantity_available']);
                        $objWorkSheet->setCellValue('C' . $column, $result['mws_unsuppressed_inventory_data']['afn_fulfillable_qty']);
                        $objWorkSheet->setCellValue('D' . $column, '');
                        $objWorkSheet->setCellValue('E' . $column, $result['mws_reserved_inventory']['reserved_fc_transfer']);
                        $objWorkSheet->setCellValue('F' . $column, $result['mws_reserved_inventory']['reserved_fc_processing']);
                        $objWorkSheet->setCellValue('G' . $column, $result['mws_reserved_inventory']['reserved_customer_orders']);
                        $objWorkSheet->setCellValue('H' . $column, $result['mws_reserved_inventory']['reserved_qty']);
                        $objWorkSheet->setCellValue('I' . $column, $result['mws_unsuppressed_inventory_data']['afn_research_qty']);
                        $objWorkSheet->setCellValue('J' . $column, $result['mws_unsuppressed_inventory_data']['afn_unsellable_qty']);
                        $objWorkSheet->setCellValue('K' . $column, $result['mws_unsuppressed_inventory_data']['afn_warehouse_qty']);
                        $objWorkSheet->setCellValue('L' . $column, $result['mws_unsuppressed_inventory_data']['afn_inbound_receving_qty']);
                        $objWorkSheet->setCellValue('M' . $column, $result['mws_unsuppressed_inventory_data']['afn_inbound_shipment_qty']);
                        $objWorkSheet->setCellValue('N' . $column, $result['mws_unsuppressed_inventory_data']['afn_inbound_working_qty']);
                        $objWorkSheet->setCellValue('O' . $column, $result['mws_unsuppressed_inventory_data']['afn_total_qty']);
                        $column++;
                    }
                }
                if ($i == 1) {
                    //SellerSettlement data
                    $objWorkSheet->setCellValue('A1', 'SKU');
                    $objWorkSheet->setCellValue('B1', 'Name  ');
                    $objWorkSheet->setCellValue('C1', 'Shipment ID ');
                    $objWorkSheet->setCellValue('D1', 'Shipment Status ');
                    $objWorkSheet->setCellValue('E1', 'Sent ');
                    $objWorkSheet->setCellValue('F1', 'Received ');
                    $objWorkSheet->setCellValue('G1', 'ShipFromAddress ');
                    $objWorkSheet->setCellValue('H1', 'DestinationFulfillmentCenterName ');
                    $objWorkSheet->setCellValue('I1', 'DestinationFulfillmentCenterId ');
                    $column = 2;
                    foreach ($get_all_product_sku as $key => $result) {
                        $datas = Inbound_shipment_items::with(['inbound_shipment'])->where(array('product_id' => $result['id']))->get()->toArray();
                        $newData = array();
                        foreach ($datas as $inc => $inbound) {
//                        if($inbound['QuantityShipped'] != $inbound['QuantityReceived']) {
                            $differenceInventory = $inbound['QuantityShipped'] - $inbound['QuantityReceived'];
                            $newData[$inc]['inbound'] = $differenceInventory;
                            $newData[$inc]['ShipmentName'] = $inbound['inbound_shipment']['ShipmentName'];
                            $newData[$inc]['ShipmentId'] = $inbound['inbound_shipment']['ShipmentId'];
                            $newData[$inc]['ShipmentStatus'] = $inbound['inbound_shipment']['ShipmentStatus'];
                            $newData[$inc]['QuantityShipped'] = $inbound['QuantityShipped'];
                            $newData[$inc]['QuantityReceived'] = $inbound['QuantityReceived'];
                            $newData[$inc]['ShipFromAddress'] = $inbound['inbound_shipment']['ShipFromAddress'];
                            $newData[$inc]['DestinationFulfillmentCenter'] = get_fulfillmentCenter_details($inbound['inbound_shipment']['DestinationFulfillmentCenterId']);
                            $newData[$inc]['DestinationFulfillmentCenterId'] = $inbound['inbound_shipment']['DestinationFulfillmentCenterId'];
//                        }
                        }
                        $objWorkSheet->setCellValue('A' . $column, $result['sku']);
                        if (empty($newData)) {
                            $objWorkSheet->setCellValue('D' . $column, '');
                            $objWorkSheet->setCellValue('E' . $column, '');
                            $objWorkSheet->setCellValue('F' . $column, '');
                            $objWorkSheet->setCellValue('G' . $column, '');
                            $objWorkSheet->setCellValue('H' . $column, '');
                            $objWorkSheet->setCellValue('I' . $column, '');
                            $column++;
                        } else {
                            foreach ($newData as $inbound) {
                                $objWorkSheet->setCellValue('B' . $column, $inbound['ShipmentName']);
                                $objWorkSheet->setCellValue('C' . $column, $inbound['ShipmentId']);
                                $objWorkSheet->setCellValue('D' . $column, $inbound['ShipmentStatus']);
                                $objWorkSheet->setCellValue('E' . $column, $inbound['QuantityShipped']);
                                $objWorkSheet->setCellValue('F' . $column, $inbound['QuantityReceived']);
                                $objWorkSheet->setCellValue('G' . $column, $inbound['ShipFromAddress']);
                                $objWorkSheet->setCellValue('H' . $column, $inbound['DestinationFulfillmentCenter']);
                                $objWorkSheet->setCellValue('I' . $column, $inbound['DestinationFulfillmentCenterId']);
                                $column++;
                            }
                        }
                    }
                }
                if ($i == 2)
                {
                    //SellerSettlement data
                    $objWorkSheet->setCellValue('A1', 'SKU');
                    $objWorkSheet->setCellValue('B1', '1 Day (Returns/Sellable)');
                    $objWorkSheet->setCellValue('C1', '1 Day (24 Hours Sales)');
                    $objWorkSheet->setCellValue('D1', '7 Days');
                    $objWorkSheet->setCellValue('E1', 'Start Date');
                    $objWorkSheet->setCellValue('F1', 'End Date');
                    $objWorkSheet->setCellValue('G1', '14 Days');
                    $objWorkSheet->setCellValue('H1', 'Start Date');
                    $objWorkSheet->setCellValue('I1', 'End Date');
                    $objWorkSheet->setCellValue('J1', '30 Days');
                    $objWorkSheet->setCellValue('K1', 'Start Date');
                    $objWorkSheet->setCellValue('L1', 'End Date');
                    $objWorkSheet->setCellValue('M1', 'Previous 7 Days');
                    $objWorkSheet->setCellValue('N1', 'Start Date');
                    $objWorkSheet->setCellValue('O1', 'End Date');
                    $objWorkSheet->setCellValue('P1', 'Previous 30 Days');
                    $objWorkSheet->setCellValue('Q1', 'Start Date');
                    $objWorkSheet->setCellValue('R1', 'End Date');
                    $objWorkSheet->setCellValue('S1', '30 Days Sales - Last Year');
                    $dynamicArray = ['T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS'];

                    for ($count = 0; $count <= 24; $count++) {
                        $result = date('m-Y', strtotime("-$count month"));
                        $objWorkSheet->setCellValue($dynamicArray[$count] . '1', $result);
                    }
                    $column = 2;
                    foreach ($get_all_product_sku as $record) {
                        $sevenDaysStartDate = $sevenDaysEndDate = '';
                        $sevenDaysSales = $fourteenDaysSales = $thirtyDaysSales = $previousSevenDaysSales = $previousThirtyDaysSales = 0;
                        $previoussevenDaysStartDate = $previoussevenDaysEndDate = '';
                        $fourteenDaysStartDate = $fourteenDaysEndDate = '';
                        $thirtyDaysStartDate = $thirtyDaysEndDate = '';
                        $previousthirtyDaysStartDate = $previousthirtyDaysEndDate = '';
                        $breakFlag = 0;
                        $sevenDayFlag = $fourteenDayFlag = $thirtyDayFlag = $previousSevenFlag = $previousThirtyFlag = 0;
                        $startDate = date('Y-m-d');
                        $endDate = date('Y-m-d', strtotime('2020-01-01'));
                        $dailySales = Calculated_order_qty::where('product_id', $record['id'])->where('order_date', '>=', $endDate)->orderBy('order_date', 'desc')->pluck('total_qty_shipping_shipped', 'order_date')->toArray();
                        $dailyInventory = Mws_calculated_inventory_daily::where('product_id', $record['id'])->where('inventory_date', '>=', date('Y-m-d 00:00:00', strtotime($endDate)))->orderBy('inventory_date', 'desc')->pluck('qty', 'inventory_date')->toArray();
                        $sevendayCount = $previoussevendayCount = $fourteendayCount = $thirtydayCount = $previousthirtydayCount = 0;
                        $dateArray = $this->displayDates($startDate, $endDate);
                        foreach ($dateArray as $key => $result) {
                            $count = ($key + 1);
                            if ($count == 1 || $count == 2) {
                                $newInv = Mws_unsuppressed_historical_inventory::where('product_id', $record['id'])->where('inventory_for_date',date('Y-m-d 00:00:00', strtotime($result)))->pluck('afn_total_qty', 'inventory_for_date')->toArray();
                                $newInv_ref = 0;
                                if(!empty($newInv)){
                                    $newInv_ref = array_values($newInv)[0];
                                    $dailyInventory[$result . ' 00:00:00'] = $newInv_ref;
                                }
                            }
                            if (!isset($dailyInventory[$result . ' 00:00:00']))
                                $dailyInventory[$result . ' 00:00:00'] = 0;
                            if (!isset($dailySales[$result]))
                                $dailySales[$result] = 0;
                            if ($count == 1) {
                                $sevenDaysStartDate = $result;
                                $fourteenDaysStartDate = $result;
                                $thirtyDaysStartDate = $result;
                            }
                            if ($sevendayCount < 7) {

                                if ($dailySales[$result] == 0 || $dailyInventory[$result . ' 00:00:00'] == 0) {
                                    $sevendayCount = $fourteendayCount = $thirtydayCount = 0;
                                    $sevenDayFlag++;
                                    $fourteenDayFlag++;
                                    $thirtyDayFlag++;
                                    $previousSevenFlag++;
                                    $previousThirtyFlag++;
                                    $sevenDaysStartDate = date('Y-m-d', strtotime("-1 days", strtotime($result)));
                                    $fourteenDaysStartDate = date('Y-m-d', strtotime("-1 days", strtotime($result)));
                                    $thirtyDaysStartDate = date('Y-m-d', strtotime("-1 days", strtotime($result)));
                                } else {
                                    $sevendayCount++;
                                    $fourteendayCount++;
                                    $thirtydayCount++;
                                }
                            }
                            if ($sevendayCount == 7) {
                                if (empty($sevenDaysEndDate)) {
                                    $sevenDaysEndDate = $result;
                                    $previoussevenDaysStartDate = date('Y-m-d', strtotime("-1 days", strtotime($result)));
                                }
                                if ($fourteendayCount <= 14) {
                                    if ($dailySales[$result] == 0 || $dailyInventory[$result . ' 00:00:00'] == 0) {
                                        $fourteendayCount = $thirtydayCount = $previoussevendayCount = 0;
                                        $previoussevenDaysStartDate = date('Y-m-d', strtotime("-2 days", strtotime($result)));
                                        $fourteenDaysStartDate = date('Y-m-d', strtotime("-2 days", strtotime($result)));
                                        $thirtyDaysStartDate = date('Y-m-d', strtotime("-2 days", strtotime($result)));
                                    } else {
                                        if ($previoussevendayCount == 7) {
                                            $previoussevenDaysEndDate = $result;
                                        }
                                        $previoussevendayCount++;
                                        $fourteendayCount++;
                                        $thirtydayCount++;
                                    }
                                }
                                if ($fourteendayCount == 15) {
                                    if (empty($fourteenDaysEndDate)) {
                                        $fourteenDaysEndDate = $result;
//                                        $previoussevenDaysEndDate = $result;
                                    }
                                    if ($thirtydayCount <= 31) {
                                        if ($dailySales[$result] == 0 || $dailyInventory[$result . ' 00:00:00'] == 0) {
                                            $thirtydayCount = 0;
                                            $thirtyDaysStartDate = date('Y-m-d', strtotime("-3 days", strtotime($result)));
                                        } else {
                                            $thirtydayCount++;
                                        }
                                    }
                                }
                                if ($thirtydayCount == 32) {
                                    if (empty($thirtyDaysEndDate)) {
                                        $thirtyDaysEndDate = $result;
                                        $previousthirtyDaysStartDate = date('Y-m-d', strtotime("-1 days", strtotime($result)));
                                    }
                                    if ($previousthirtydayCount <= 31) {
                                        if ($dailySales[$result] == 0 || $dailyInventory[$result . ' 00:00:00'] == 0) {
                                            $previousthirtydayCount = 0;
                                            $previousthirtyDaysStartDate = date('Y-m-d', strtotime("-2 days", strtotime($result)));
                                        } else {
                                            if ($previousthirtydayCount == 30) {
                                                $previousthirtyDaysEndDate = $result;
                                            }
                                            $previousthirtydayCount++;
                                        }
                                    }
                                }
                                if ($previousthirtydayCount == 32 && empty($previousthirtyDaysEndDate)) {
                                    $previousthirtyDaysEndDate = $result;
                                    $breakFlag = 1;
                                }
                            }
                            if ($breakFlag == 1)
                                break;
                        }
                        if (empty($fourteenDaysEndDate)) {
                            $previoussevenDaysEndDate = '';
                        }
                        $objWorkSheet->setCellValue('A' . $column, $record['sku']);
                        $objWorkSheet->setCellValue('B' . $column, '');
                        $twentyFourHoursBackDate = Carbon::yesterday()->format('Y-m-d');
//                        $twentyFourHoursSales = Calculated_order_qty::where('product_id', $record['id'])->where('order_date', '>=', $twentyFourHoursBackDate)->sum('total_qty_shipping_shipped');
                        $twentyFourHoursSales = Calculated_order_qty::where('product_id', $record['id'])->where('order_date', '>=', $twentyFourHoursBackDate)->sum('total_qty_pending');
                        $twentyFourHoursSales += Calculated_order_qty::where('product_id', $record['id'])->where('order_date', '>=', $twentyFourHoursBackDate)->sum('total_qty_shipped');
                        if ($twentyFourHoursSales > 0) {
                            $twentyFourHoursyRate = round(($twentyFourHoursSales / 1), 6);
                        } else {
                            $twentyFourHoursSales = $twentyFourHoursyRate = 0;
                        }

                        $objWorkSheet->setCellValue('C' . $column, $twentyFourHoursSales);
                        if (!empty($sevenDaysStartDate) && !empty($sevenDaysEndDate)) {
//                            $sevenDaysSales = Calculated_order_qty::where('product_id', $record['id'])->where('order_date', '>=', $sevenDaysEndDate)->where('order_date', '<=', $sevenDaysStartDate)->sum('total_qty_shipping_shipped');
                            $sevenDaysSales = Calculated_order_qty::where('product_id', $record['id'])->where('order_date', '>=', $sevenDaysEndDate)->where('order_date', '<=', $sevenDaysStartDate)->sum('total_qty_pending');
                            $sevenDaysSales += Calculated_order_qty::where('product_id', $record['id'])->where('order_date', '>=', $sevenDaysEndDate)->where('order_date', '<=', $sevenDaysStartDate)->sum('total_qty_shipped');
                        }
                        if ($sevenDaysSales > 0) {
                            $sevenDaysDayRate = round(($sevenDaysSales / 7), 6);
                        } else {
                            $sevenDaysSales = $sevenDaysDayRate = 0;
                        }

                        if (!empty($fourteenDaysStartDate) && !empty($fourteenDaysEndDate)) {
//                            $fourteenDaysSales = Calculated_order_qty::where('product_id', $record['id'])->where('order_date', '>=', $fourteenDaysEndDate)->where('order_date', '<=', $fourteenDaysStartDate)->sum('total_qty_shipping_shipped');
                            $fourteenDaysSales = Calculated_order_qty::where('product_id', $record['id'])->where('order_date', '>=', $fourteenDaysEndDate)->where('order_date', '<=', $fourteenDaysStartDate)->sum('total_qty_pending');
                            $fourteenDaysSales += Calculated_order_qty::where('product_id', $record['id'])->where('order_date', '>=', $fourteenDaysEndDate)->where('order_date', '<=', $fourteenDaysStartDate)->sum('total_qty_shipped');
                        }
                        if ($fourteenDaysSales > 0) {
                            $fourteenDaysDayRate = round(($fourteenDaysSales / 14), 6);
                        } else {
                            $fourteenDaysSales = $fourteenDaysDayRate = $fourteenDaysDayOfSupply = 0;
                        }

                        if (!empty($thirtyDaysStartDate) && !empty($thirtyDaysEndDate)) {
//                            $thirtyDaysSales = Calculated_order_qty::where('product_id', $record['id'])->where('order_date', '>=', $thirtyDaysEndDate)->where('order_date', '<=', $thirtyDaysStartDate)->sum('total_qty_shipping_shipped');
                            $thirtyDaysSales = Calculated_order_qty::where('product_id', $record['id'])->where('order_date', '>=', $thirtyDaysEndDate)->where('order_date', '<=', $thirtyDaysStartDate)->sum('total_qty_pending');
                            $thirtyDaysSales += Calculated_order_qty::where('product_id', $record['id'])->where('order_date', '>=', $thirtyDaysEndDate)->where('order_date', '<=', $thirtyDaysStartDate)->sum('total_qty_shipped');
                        }
                        if ($thirtyDaysSales > 0) {
                            $thirtyDaysDayRate = round(($thirtyDaysSales / 30), 6);
                        } else {
                            $thirtyDaysSales = $thirtyDaysDayRate = $thirtyDaysDayOfSupply = 0;
                        }

                        if (!empty($previoussevenDaysStartDate) && !empty($previoussevenDaysEndDate)) {
//                            $previousSevenDaysSales = Calculated_order_qty::where('product_id', $record['id'])->where('order_date', '>=', $previoussevenDaysEndDate)->where('order_date', '<=', $previoussevenDaysStartDate)->sum('total_qty_shipping_shipped');
                            $previousSevenDaysSales = Calculated_order_qty::where('product_id', $record['id'])->where('order_date', '>=', $previoussevenDaysEndDate)->where('order_date', '<=', $previoussevenDaysStartDate)->sum('total_qty_pending');
                            $previousSevenDaysSales += Calculated_order_qty::where('product_id', $record['id'])->where('order_date', '>=', $previoussevenDaysEndDate)->where('order_date', '<=', $previoussevenDaysStartDate)->sum('total_qty_shipped');
                        }
                        if ($previousSevenDaysSales > 0) {
                            $previousSevenDaysDayRate = round(($previousSevenDaysSales / 7), 6);
                        } else {
                            $previousSevenDaysSales = $previousSevenDaysDayRate = 0;
                        }
                        if (!empty($previousthirtyDaysStartDate) && !empty($previousthirtyDaysEndDate)) {
//                            $previousThirtyDaysSales = Calculated_order_qty::where('product_id', $record['id'])->where('order_date', '>=', $previousthirtyDaysEndDate)->where('order_date', '<=', $previousthirtyDaysStartDate)->sum('total_qty_shipping_shipped');
                            $previousThirtyDaysSales = Calculated_order_qty::where('product_id', $record['id'])->where('order_date', '>=', $previousthirtyDaysEndDate)->where('order_date', '<=', $previousthirtyDaysStartDate)->sum('total_qty_pending');
                            $previousThirtyDaysSales += Calculated_order_qty::where('product_id', $record['id'])->where('order_date', '>=', $previousthirtyDaysEndDate)->where('order_date', '<=', $previousthirtyDaysStartDate)->sum('total_qty_shipped');
                        }
                        if ($previousThirtyDaysSales > 0) {
                            $previousThirtyDaysDayRate = round(($previousThirtyDaysSales / 30), 6);
                        } else {
                            $previousThirtyDaysSales = $previousThirtyDaysDayRate = 0;
                        }
                        $thirtyDaysStartDateLastYear = Carbon::now()->subDays(395)->format('Y-m-d');
                        $thirtyDaysEndDateLastYear = Carbon::now()->subDays(365)->format('Y-m-d');
//                        $thirtyDaysSalesLastyear = Calculated_order_qty::where('product_id', $record['id'])->where('order_date', '<=', $thirtyDaysEndDateLastYear)->where('order_date', '>=', $thirtyDaysStartDateLastYear)->sum('total_qty_shipping_shipped');
                        $thirtyDaysSalesLastyear = Calculated_order_qty::where('product_id', $record['id'])->where('order_date', '<=', $thirtyDaysEndDateLastYear)->where('order_date', '>=', $thirtyDaysStartDateLastYear)->sum('total_qty_pending');
                        $thirtyDaysSalesLastyear += Calculated_order_qty::where('product_id', $record['id'])->where('order_date', '<=', $thirtyDaysEndDateLastYear)->where('order_date', '>=', $thirtyDaysStartDateLastYear)->sum('total_qty_shipped');
                        if ($thirtyDaysSalesLastyear > 0) {
                            $ThirtyDaysDayRateLastYear = round(($thirtyDaysSalesLastyear / 30), 6);
                        } else {
                            $ThirtyDaysDayRateLastYear = 0;
                        }


                        $objWorkSheet->setCellValue('D' . $column, $sevenDaysSales);
                        $objWorkSheet->setCellValue('E' . $column, (($sevenDayFlag > 0 && isset($sevenDaysStartDate) && $sevenDaysStartDate != '01-01-1970' && $sevenDaysStartDate != '' && isset($sevenDaysEndDate) && $sevenDaysEndDate != '01-01-1970' && $sevenDaysEndDate != '') ? date('m-d-Y', strtotime($sevenDaysStartDate)) : ''));
                        $objWorkSheet->setCellValue('F' . $column, (($sevenDayFlag > 0 && isset($sevenDaysEndDate) && $sevenDaysEndDate != '01-01-1970' && $sevenDaysEndDate != '') ? date('m-d-Y', strtotime($sevenDaysEndDate)) : ''));
                        $objWorkSheet->setCellValue('G' . $column, $fourteenDaysSales);
                        $objWorkSheet->setCellValue('H' . $column, (($fourteenDayFlag > 0 && isset($fourteenDaysStartDate) && isset($fourteenDaysEndDate) && $fourteenDaysStartDate != '01-01-1970' && $fourteenDaysStartDate != '' && $fourteenDaysEndDate != '01-01-1970' && $fourteenDaysEndDate != '') ? date('m-d-Y', strtotime($fourteenDaysStartDate)) : ''));
                        $objWorkSheet->setCellValue('I' . $column, (($fourteenDayFlag > 0 && isset($fourteenDaysEndDate) && $fourteenDaysEndDate != '01-01-1970' && $fourteenDaysEndDate != '') ? date('m-d-Y', strtotime($fourteenDaysEndDate)) : ''));
                        $objWorkSheet->setCellValue('J' . $column, $thirtyDaysSales);
                        $objWorkSheet->setCellValue('K' . $column, (($thirtyDayFlag > 0 && isset($thirtyDaysStartDate) && $thirtyDaysStartDate != '01-01-1970' && $thirtyDaysStartDate != '' && isset($thirtyDaysEndDate) && $thirtyDaysEndDate != '01-01-1970' && $thirtyDaysEndDate != '') ? date('m-d-Y', strtotime($thirtyDaysStartDate)) : ''));
                        $objWorkSheet->setCellValue('L' . $column, (($thirtyDayFlag > 0 && isset($thirtyDaysEndDate) && $thirtyDaysEndDate != '01-01-1970' && $thirtyDaysEndDate != '') ? date('m-d-Y', strtotime($thirtyDaysEndDate)) : ''));
                        $objWorkSheet->setCellValue('M' . $column, $previousSevenDaysSales);
                        $objWorkSheet->setCellValue('N' . $column, (($previousSevenFlag > 0 && isset($previoussevenDaysStartDate) && $previoussevenDaysStartDate != '01-01-1970' && $previoussevenDaysStartDate != '' && isset($previoussevenDaysEndDate) && $previoussevenDaysEndDate != '01-01-1970' && $previoussevenDaysEndDate != '') ? date('m-d-Y', strtotime($previoussevenDaysStartDate)) : ''));
                        $objWorkSheet->setCellValue('O' . $column, (($previousSevenFlag > 0 && isset($previoussevenDaysEndDate) && $previoussevenDaysEndDate != '01-01-1970' && $previoussevenDaysEndDate != '') ? date('m-d-Y', strtotime($previoussevenDaysEndDate)) : ''));
                        $objWorkSheet->setCellValue('P' . $column, $previousThirtyDaysSales);
                        $objWorkSheet->setCellValue('Q' . $column, (($previousThirtyFlag > 0 && isset($previousthirtyDaysStartDate) && $previousthirtyDaysStartDate != '01-01-1970' && $previousthirtyDaysStartDate != '' && isset($previousthirtyDaysEndDate) && $previousthirtyDaysEndDate != '01-01-1970' && $previousthirtyDaysEndDate != '') ? date('m-d-Y', strtotime($previousthirtyDaysStartDate)) : ''));
                        $objWorkSheet->setCellValue('R' . $column, (($previousThirtyFlag > 0 && isset($previousthirtyDaysEndDate) && $previousthirtyDaysEndDate != '01-01-1970' && $previousthirtyDaysEndDate != '') ? date('m-d-Y', strtotime($previousthirtyDaysEndDate)) : ''));
                        $objWorkSheet->setCellValue('S' . $column, $thirtyDaysSalesLastyear);
                        for ($count = 0; $count <= 24; $count++) {
                            $get_datemonth = date('Y-m', strtotime("-$count month"));
                            $get_supply_total_trands = get_supply_total_trands($get_datemonth, $record['id']);
                            if (!empty($get_supply_total_trands)) {
                                $objWorkSheet->setCellValue($dynamicArray[$count] . $column, $get_supply_total_trands->sales_total);
                            } else {
                                $objWorkSheet->setCellValue($dynamicArray[$count] . $column, '');
                            }
                        }
                        $column++;
                        $objWorkSheet->setCellValue('A' . $column, '');
                        $objWorkSheet->setCellValue('B' . $column, '');
                        $objWorkSheet->setCellValue('C' . $column, $twentyFourHoursyRate);
                        $objWorkSheet->setCellValue('D' . $column, $sevenDaysDayRate);
                        $objWorkSheet->setCellValue('E' . $column, '');
                        $objWorkSheet->setCellValue('F' . $column, '');
                        $objWorkSheet->setCellValue('G' . $column, $fourteenDaysDayRate);
                        $objWorkSheet->setCellValue('H' . $column, '');
                        $objWorkSheet->setCellValue('I' . $column, '');
                        $objWorkSheet->setCellValue('J' . $column, $thirtyDaysDayRate);
                        $objWorkSheet->setCellValue('K' . $column, '');
                        $objWorkSheet->setCellValue('L' . $column, '');
                        $objWorkSheet->setCellValue('M' . $column, $previousSevenDaysDayRate);
                        $objWorkSheet->setCellValue('N' . $column, '');
                        $objWorkSheet->setCellValue('O' . $column, '');
                        $objWorkSheet->setCellValue('P' . $column, $previousThirtyDaysDayRate);
                        $objWorkSheet->setCellValue('Q' . $column, '');
                        $objWorkSheet->setCellValue('R' . $column, '');
                        $objWorkSheet->setCellValue('S' . $column, $ThirtyDaysDayRateLastYear);
                        for ($count = 0; $count <= 24; $count++) {
                            $get_datemonth = date('Y-m', strtotime("-$count month"));
                            $get_supply_total_trands = get_supply_total_trands($get_datemonth, $record['id']);
                            if (!empty($get_supply_total_trands)) {
                                $objWorkSheet->setCellValue($dynamicArray[$count] . $column, $get_supply_total_trands->day_rate);
                            } else {
                                $objWorkSheet->setCellValue($dynamicArray[$count] . $column, '');
                            }
                        }
                        $column++;
                    }
                }
                if ($i == 3)
                {
                    //order returns data
                    $objWorkSheet->setCellValue('A1', 'SKU');
                    $objWorkSheet->setCellValue('B1', 'Return Date');
                    $objWorkSheet->setCellValue('C1', 'Product Name');
                    $objWorkSheet->setCellValue('D1', 'ASIN');
                    $objWorkSheet->setCellValue('E1', 'FNSKU');
                    $objWorkSheet->setCellValue('F1', 'Quantity');
                    $objWorkSheet->setCellValue('G1', 'Fulfillment Center Id');
                    $objWorkSheet->setCellValue('H1', 'Detailed Disposition');
                    $objWorkSheet->setCellValue('I1', 'Reason');
                    $objWorkSheet->setCellValue('J1', 'Status');
                    $objWorkSheet->setCellValue('K1', 'License Plate Number');
                    $objWorkSheet->setCellValue('L1', 'Customer Comments');
                    $column = 2;

                    foreach ($get_all_product_sku as $key => $result1)
                    {
                        $objWorkSheet->setCellValue('A' . $column, $result1['sku']);
                        if(!empty($result1['mws_order_returns'])){
                            foreach ($result1['mws_order_returns'] as $key => $result) {
                                $date_rt = date('m/d/Y',strtotime($result['return_date']));
                                $cmt_rt = $result['customer_comments'] != null ? $result['customer_comments'] : "-";
                                $objWorkSheet->setCellValue('B' . $column, $date_rt);
                                $objWorkSheet->setCellValue('C' . $column, $result['product_name']);
                                $objWorkSheet->setCellValue('D' . $column, $result['asin']);
                                $objWorkSheet->setCellValue('E' . $column, $result['fnsku']);
                                $objWorkSheet->setCellValue('F' . $column, $result['quantity']);
                                $objWorkSheet->setCellValue('G' . $column, $result['fulfillment_center_id']);
                                $objWorkSheet->setCellValue('H' . $column, $result['detailed_disposition']);
                                $objWorkSheet->setCellValue('I' . $column, $result['reason']);
                                $objWorkSheet->setCellValue('J' . $column, $result['status']);
                                $objWorkSheet->setCellValue('K' . $column, $result['license_plate_number']);
                                $objWorkSheet->setCellValue('L' . $column, $cmt_rt);
                                $column++;
                            }
                        }else{
                            $column++;
                        }
                    }
                }
                $objWorkSheet->setTitle($sheetsstitle[$i]);
                $i++;
            }
            $objPHPExcel->setActiveSheetIndex($i - 4);
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="Stockful_' . $global_marketplace . '_' . date('Y-m-d') . '.xls"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            $writer = new Xlsx($objPHPExcel);
            $writer->save('php://output');
            exit();
        }
    }

    /**
     * download sales inventory exports.
     * @method GET
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function SalesInventoryExport($id)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');
        $global_marketplace = session('MARKETPLACE_ID');
        if (empty($global_marketplace)) {
            $global_marketplace = $id;
        }
        $objPHPExcel = new Spreadsheet();
        $sheetsstitle = array("Inventory", "Sales");
        $skuArray = ['CB4-GD-IS1-12-03',
            'CP3-GD1-IS1-12-07',
            'CB3-GD-IS1-12-03',
            'CP2-GD1-IS1-12-07',
            'CB2-GD-IS1-12-03',
            'CB9-GD-IS1-07-25',
            'CB10-GD-IS1-11-18',
            'CP4-GD-IS1-11-18',
            'WB1-GD-IS1-5-17',
            'WB2-GD-IS1-5-17'];
        $get_all_product_sku = Mws_product::whereIn('sku', $skuArray)->where(array('user_marketplace_id' => $global_marketplace))->selectRaw('sku,id')->get()->toArray();
        $i = 0;
        $startDate = date('Y-m-d');
        $endDate = date('Y-m-d', strtotime('2020-05-01'));
        $dateArray = $this->displayDates($startDate, $endDate);
        while ($i < 2) {
            $objWorkSheet = $objPHPExcel->createSheet($i);
            if ($i == 0) {
                $column = 2;
                $objWorkSheet->setCellValue('A1', 'SKU');
                $objWorkSheet->setCellValue('B1', 'Dates');
                $objWorkSheet->setCellValue('c1', 'Quantity');
                foreach ($get_all_product_sku as $record) {
                    $dailyInventory = Mws_calculated_inventory_daily::where('product_id', $record['id'])->where('inventory_date', '>=', date('Y-m-d 00:00:00', strtotime($endDate)))->orderBy('inventory_date', 'desc')->pluck('qty', 'inventory_date')->toArray();
                    $objWorkSheet->setCellValue('A' . $column, $record['sku']);
                    foreach ($dateArray as $key => $dates) {
                        $objWorkSheet->setCellValue('B' . $column, $dateArray[$key]);
                        isset($dailyInventory[($dates . ' 00:00:00')]) ? $result = $dailyInventory[($dates . ' 00:00:00')] : $result = 0;
                        $objWorkSheet->setCellValue('c' . $column, $result);
                        $column++;
                    }
                }
            }
            if ($i == 1) {
                $column = 2;
                $objWorkSheet->setCellValue('A1', 'SKU');
                $objWorkSheet->setCellValue('B1', 'Dates');
                $objWorkSheet->setCellValue('c1', 'Quantity');
                foreach ($get_all_product_sku as $record) {
                    $dailySales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '>=', $endDate)->orderBy('order_date', 'desc')->pluck('total_qty', 'order_date')->toArray();
                    $objWorkSheet->setCellValue('A' . $column, $record['sku']);
                    foreach ($dateArray as $key => $dates) {
                        $objWorkSheet->setCellValue('B' . $column, $dateArray[$key]);
                        isset($dailySales[$dates]) ? $result = $dailySales[$dates] : $result = 0;
                        $objWorkSheet->setCellValue('C' . $column, $result);
                        $column++;
                    }
                }
            }
            $objWorkSheet->setTitle($sheetsstitle[$i]);
            $i++;
        }
        $objPHPExcel->setActiveSheetIndex($i - 2);
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="Stockful_salesinventory_' . $global_marketplace . '.xls"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $writer = new Xlsx($objPHPExcel);
        $writer->save('php://output');
        exit();
    }

    /**
     * Display date with proper format
     * @method GET
     * @Params $date1,$date2,$format
     * @return \Illuminate\Http\Response
     */

    function displayDates($date1, $date2, $format = 'Y-m-d')
    {
        $dates = array();
        $current = strtotime($date1);
        $date2 = strtotime($date2);
        $stepVal = '-1 day';
        while ($current >= $date2) {
            $dates[] = date($format, $current);
            $current = strtotime($stepVal, $current);
        }
        return $dates;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

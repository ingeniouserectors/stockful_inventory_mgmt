<?php

namespace App\Http\Controllers;

use App\Models\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class SettingController extends Controller
{

    public function __construct()
        {

        }

    /**
     * Display a setting data with form details.
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get_checks = get_access('common_setting_module','view');
        $get_user_access = get_user_check_access('common_setting_module','view');
        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
                if ($get_user_access == 0) {
                    $message = get_messages("Don't have Access Rights for this Functionality", 0);
                    Session::flash('message', $message);
                    return redirect('/index');
                }
        }
        $detail = Settings::first();
        return view('settings.index', ['detail' => $detail]);
    }


    public function create()
    {
        //
    }

    /**
     * Update setting changes using this method.
     * @method POST
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userRoleId = get_user_role(Auth::user()->id);
        if($userRoleId['user_assigned_role']['user_role_id'] != 1){
            $message = get_messages("Don't have Access Rights for this Functionality", 0);
            Session::flash('message', $message);
            return redirect('/index');
        }

        $setting = Settings::first();
        if (empty($setting)) {
            $setting = new Settings();
        }
        $setting->registration_setting = (isset($request->registration) ? $request->registration : 0);
        $setting->email_settings = (isset($request->email) ? $request->email : 0);
        $setting->forget_password_settings = (isset($request->forgot_password) ? $request->forgot_password : 0);
        $setting->mws_settings = (isset($request->mws) ? $request->mws : 0);
        $setting->maintenance_mode = (isset($request->maintenance) ? $request->maintenance : 0);
        $setting->affiliate_settings = (isset($request->affiliate) ? $request->affiliate : 0);
        $setting->support_ticket_settings = (isset($request->support_ticket) ? $request->support_ticket : 0);
        $setting->historical_fetch_number_months = (!empty($request->fetch_month) ? $request->fetch_month : 0);
        $setting->save();
        $message = get_messages('Settings updated successfully!',1);
        Session::flash('message', $message);
        return redirect()->back();
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Role_access_modules;
use App\Models\Userrole;
use Illuminate\Http\Request;
use App\Models\Usermodule;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Gate;
use App\Models\Application_modules;
use App\Models\Users;
use Illuminate\Support\Facades\Auth;


class UserModuleController extends Controller
{

    Protected $userRolePermissionId;

    function __construct()
    {
        $this->middleware(function ($request, $next) {

            if (!Auth::user()) {
                return redirect('login');
            }
            $moduleId = Application_modules::where('module', 'subscription_module')->first()->id;
            $userRoleID = Users::with('user_roles')->whereIn('id', [Auth::id()])->first()->user_roles[0]->id;
            $this->userRolePermissionId = Role_access_modules::where('role_id', $userRoleID)->where('application_module_id', $moduleId)->first();
            return $next($request);
        });
    }

    /**
     * Display a listing of moduless.
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $get_checks = get_access('subscription_module','view');
        $get_user_access = get_user_check_access('subscription_module','view');
        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
                if ($get_user_access == 0) {
                    $message = get_messages("Don't have Access Rights for this Functionality", 0);
                    Session::flash('message', $message);
                    return redirect('/index');
                }
        }
        return view('modules.index'
        );
    }

    /**
     * Show the form for creating a new modules.
     * @params no-params
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
         return view('modules.create-modules');
    }

    /**
     * If  request_type is blank then create a new modules other wise display the listing of all modules.
     * @method POST
     * @param  \Illuminate\Http\Response $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        if($request->request_type == 'modules_list'){

         $data = Usermodule::get()->toArray();

          $roles_data = array();
         if(!empty($data))
            {
                 foreach ($data as $key => $post)
                {
                    $action = '' ;
                    if($post['table_name'] != ''){
                        $data = '<a href="'.route('moduletable.show',$post['id']).'">'.$post['module'].'</a>';
                    }else{
                        $data = $post['module'];
                    }
                    $nestedData['module'] = $data;
                    $nestedData['table_name'] = $post['table_name'];
                    $action .= '<a href="'.route('modules.edit',$post['id']).'" class=" btn btn-info btn-sm btn-rounded waves-effect waves-light" ><i class="fas fa-edit" title="edit"></i></a>';
                    $nestedData['action'] =  $action ;
                    $roles_data[] = $nestedData;
                }
             }
                
           echo json_encode($roles_data); exit;


       }
        $request->validate([
            'module' => 'required'
            
        ]);
        $input  = $request->all();
        $check_module = Usermodule::where(array('module'=>trim($input['module'])))->get()->toArray();
        if(empty($check_module)){
            $appliation = Usermodule::create($input);
            $userLastInsertId = $appliation->toArray();
            $application_module_id = $userLastInsertId['id'];
            if($application_module_id != ''){
                $roles = Userrole::get()->toArray();
                if(!empty($roles)){
                    foreach($roles as $role){
                        $role_data['role_id'] = $role['id'];
                        $role_data['application_module_id'] = $application_module_id;
                        $role_data['create'] = 0;
                        $role_data['edit'] = 0;
                        $role_data['delete'] = 0;
                        $role_data['view'] = 0;
                        $role_data['access'] = 0;
                        Role_access_modules::create($role_data);
                    }
                }
            }
            $message = get_messages('Module created successfully!',1);
            Session::flash('message', $message);
            return redirect()->route('modules.index');
        }else{
            $message = get_messages('Module already exists!',0);
            Session::flash('message', $message);
            return redirect()->route('modules.index');
        }
    }


    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified modules.
     * @method GET
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
            $moduleDetails = Usermodule::findOrFail($id);
            return view('modules.edit-modules',[
            'moduleDetails' => $moduleDetails
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $input=$request->all();
        $check_module = Usermodule::where('table_name',trim($input['table_name']))->where('id',"!=",$id)->count();

        if($check_module == "0"){
            $moduleDetails = Usermodule::find($id);
            $moduleDetails->update(array('table_name' => $input['table_name']));
            $message = get_messages('Table name updated successfully!', 1);
            Session::flash('message', $message);
            return redirect()->route('modules.index');
        }else{
            $message = get_messages('Table name already exists!', 0);
            Session::flash('message', $message);
            return redirect()->route('modules.index');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $moduleDetails = Usermodule::findOrFail($id);
        $moduleDetails->delete();
        $message = get_messages('Module deleted successfully!',1);
        Session::flash('message', $message);
        return redirect()->route('modules.index');
    }
}

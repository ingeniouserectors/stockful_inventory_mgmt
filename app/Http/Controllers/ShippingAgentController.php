<?php

namespace App\Http\Controllers;

use App\Models\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use Illuminate\Support\Facades\Session;
use App\Models\ShippingAgent;
use App\Models\Country;
use App\Models\Field_list;
use App\Models\User_field_access;
use App\Imports\ShippingagentImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Export\UsersExport;
use File;

class ShippingAgentController extends Controller
{
    /**
     * Display a listing view of shipping agent .
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get_checks = get_access('shipping_agent_module','view');
        $get_user_access = get_user_check_access('shipping_agent_module','view');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }

        $global_marketplace = session('MARKETPLACE_ID');
        $marketplace = '';
        $shipping_agents = 0;
        if($global_marketplace != ''){
            $marketplace = $global_marketplace;
        }
        $get_all_fileds = array();
        $get_module_id =  get_module_id('shipping_agent_module');
        if($get_module_id > 0){
            $get_all_fileds = Field_list::where(array('active'=>'1',['display_name','!=',NULL],'module_id'=>$get_module_id))->orderBy('id','asc')->get()->toArray();
        }

        return view('shipping_agent.index',[
            'marketplace' =>$marketplace,
            'shipping_agents'=>$shipping_agents,
            'get_all_fields'=>$get_all_fileds,
            'cols' => DB::table('cols')->where('user_id', auth()->id())
                    ->where('table_name', 'shipping_agents')->pluck('cols')->first()
        ]);

    }


    /**
     * Show the form for creating a new shipping agent.
     * @method GET
     * @params no-params
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $get_checks = get_access('shipping_agent_module','create');
        $get_user_access = get_user_check_access('shipping_agent_module','create');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }

        $country = Country::get()->toArray();
        Return view('shipping_agent.create',array('country'=>$country));
    }

    /**
     * get the all state from country
     * @method POST
     * @params $cuntryid
     * @return \Illuminate\Http\Response
     */

    public function getStateList(Request $request)
    {
        $inputs=$request->all();
        $list= get_state_list($inputs['country']);
        return response()->json($list);
    }

    /**
     * get the all city from state
     * @method POST
     * @params $stateid
     * @return \Illuminate\Http\Response
     */

    public function getCityList(Request $request)
    {
        $inputs=$request->all();
        $list= get_city_list($inputs['state']);
        return response()->json($list);
    }


    /**
     * There are 1 function added in store method :
     *      @method POST
     *
     *  Request Types:
     *
     * 1. create_form : create a form & store the shipping agent
     *      @param  $request
     *      @return \Illuminate\Http\Response
     *
     */


    public function store(Request $request)
    {
        if($request->insert_type == 'create_form'){
             $req_data = [
                 'user_marketplace_id' => $request->marketplace_id,
                 'company_name' => $request->company_name,
                 'first_name' => $request->first_name,
                 'last_name' => $request->last_name,
                 'email' => $request->email,
                 'phone' => $request->phone,
                 'country_id' => $request->country
             ];

             $datas_of = ShippingAgent::create($req_data);
             $userLastInsertId = $datas_of->toArray();
             if($userLastInsertId > 0){
                 $message = get_messages('Shipping Agent added successfully', 1);
                 Session::flash('message', $message);
                 return redirect()->route('shippingagent.index');
             }else{
                 $message = get_messages('Failed to add shipping agent data', 0);
                 Session::flash('message', $message);
                 return redirect()->route('shippingagent.index');
             }
        }
        else if($request->request_type == 'get_all_shipping_data'){
            $offsetValue = $request->offsetValue;
            if(!empty($offsetValue)){
                $offsetValue = explode('-', $offsetValue);
                $offset = $offsetValue[0];
                $limit = $request->pagination_slot;
                $output = '';
                if ($offset % 2 == 0) {
                    $offset = 0;
                }else {
                    $offset = $offset -1;
                }
            }else{
                $offset = 0;
                $limit = $request->pagination_slot;
                $output = '';
            }

            $get_country_name = Country::where(array('country_code'=>$request->get_country))->first();
            if(!empty($get_country_name)){$country = $get_country_name->id; }
            else{ $country = '0';}
            $global_marketplace = session('MARKETPLACE_ID');

                $no_display_fileds = User_field_access::
                join('field_list','field_list.id','=','field_list_id')
                ->select(\DB::raw("GROUP_CONCAT((CASE WHEN original_name IS NULL THEN CONCAT(table_name,'.',display_field) ELSE original_name END)) AS disply_fileds, GROUP_CONCAT((CASE WHEN original_name IS NULL THEN display_field ELSE original_name END)) AS display_all"))
                ->where(array('marketplace_id'=>$global_marketplace,'type'=>4))
              ->first();



            $unchecked_arr = explode(',', $no_display_fileds->disply_fileds);
            $total_display_fileds = explode(',',$no_display_fileds->display_all);


            $selected_fields = array();
            $join_table = array();
            $join_first = array();
            $join_last = array();
            //$get_fields = array();

            $get_module_id =  get_module_id('shipping_agent_module');

            if($get_module_id > 0){
                $get_all_fileds = Field_list::where(array('active'=>'1',['display_name','!=',NULL],'module_id'=>$get_module_id))->get()->toArray();
                if(!empty($get_all_fileds)){
                    $selected_fields[] = 'shipping_agents.*';
                    //$get_fields[] = 'id';
                    foreach($get_all_fileds as $key=>$val){
                        $data_of = $val['original_name'] != '' ? $val['original_name'] : $val['display_field'];
                        $display_name[$data_of] = $val['original_name'] != '' ? $val['original_name'] : $val['display_field'];
                        if($val['referance_table'] != '' && $val['referance_table_field'] != ''){
                            $selected_fields[] = $val['referance_table'].'.'.$val['referance_table_field'] ;
                            //$get_fileds[] = $val['referance_table_field'];
                            $join_table[] = $val['referance_table'];
                            $join_first[] = 'shipping_agents.'.$val['original_name'];
                            $join_last[] = $val['referance_table'].".".$val['field_type'];
                        }else if($val['table_name'] != '' && $val['display_field'] != '' && $val['join_with'] != ''){
                            $selected_fields[] = $val['table_name'].'.'.$val['display_field'];
                            //$get_fields[] = $val['display_field'];
                            $join_table[] = $val['table_name'];
                            $join_first[] = 'shipping_agents.id';
                            $join_last[] = $val['table_name'].".".$val['join_with'];
                        }else{
                            $selected_fields[] = $val['original_name'];
                            //$get_fields[] = $val['original_name'];
                        }
                    }
                }

                $join_table = array_unique($join_table);
                $selected_raw = array_diff($selected_fields,$unchecked_arr);
                $selected_field = "'" . implode ( "', '", $selected_fields ) . "'";
                $orwhere_fname = '';
				$whereArray = [
                    "user_marketplace_id" => $global_marketplace
                ];
                if ($request->get_country) {
                    $whereArray = [
                        "user_marketplace_id" => $global_marketplace,
                        "country_id" => $request->get_country
                    ];
                }

                if($request->search != ''){
                    $whereArray[] = [
                        'company_name', 'LIKE', "%{$request->search}%"
                    ];

                    $orwhere_fname = array([
                        'first_name', 'LIKE', "%{$request->search}%"
                    ]);

                    $orwhere_lname = array([
                        'last_name', 'LIKE', "%{$request->search}%"
                    ]);
                    $orwhere_email = array([
                        'email', 'LIKE', "%{$request->search}%"
                    ]);
                    $orwhere_phone = array([
                        'phone', 'LIKE', "%{$request->search}%"
                    ]);

                }else{
                    $orwhere = [
                        '1'=>'1'
                    ];
                }

                if($request->search == ''){
                    $userRole = ShippingAgent::select($selected_raw)->where($whereArray);
                }else{
                    $userRole = ShippingAgent::select($selected_raw)->where($whereArray)->orwhere($orwhere_fname)->orwhere($orwhere_lname)->orwhere($orwhere_email)->orwhere($orwhere_phone);

                }

				if(!empty($join_table)){
                    foreach($join_table as $key=>$jon){
                        $userRole->leftjoin("$jon", "$join_first[$key]", '=', "$join_last[$key]");
                    }
                }
                if($offset != 0)
                    $listing_data =  $userRole->offset($offset)->limit($limit)->groupBy('id')->orderBy('id','desc')->get()->toArray();
                else
                    $listing_data =  $userRole->limit($limit)->groupBy('id')->orderBy('id','desc')->get()->toArray();


                $listing_data_count = ShippingAgent::where($whereArray)->count();
            }
            //$shipping_agents = ShippingAgent::where(array('user_marketplace_id' => $global_marketplace,'country_id'=>$country))->offset($offset)->limit($limit)->get()->toArray();
            if(!empty($listing_data)) {
                if($listing_data_count > env('PAGINATION')) {
                $output .= '<input type="hidden" value="0" class="checkdeleteall"><input type="hidden" value="' . $listing_data_count . '" class="total_listing_count"><tr class="selected-row-number" style="display:none;">
                                        <td colspan="9" class="text-center">Selected <span id="number-of-selected" class="total_selected_record"></span> out of ' . $listing_data_count . '. <a href="javascript:void(0)" class="text-primary select-all select_all" id="select-all-link" style="cursor: pointer;">Select All</a></td></tr>';
                } else {
                    $output .= '<input type="hidden" value="0" class="checkdeleteall"><input type="hidden" value="' . $listing_data_count . '" class="total_listing_count"><tr class="selected-row-number" style="display:none;">
                                        <td colspan="9" class="text-center">Selected <span id="number-of-selected" class="total_selected_record"></span> out of ' . $listing_data_count . '. </td></tr>';
                }
                foreach ($listing_data as $k => $data) {
                    $i = 1;
                    $counter = $k +1;
                    $newtext = 0;
                    $count  = count($display_name);
                    $output .= '<tr id="'.$data['id'].'">
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input row-checkbox" id="row-'.$counter.'" name="delete_all" data-val="'.$data['id'].'" value="'.$data['id'].'" >
                                            <label class="custom-control-label" for="row-'.$counter.'"></label>
                                        </div>
                                    </td>';
                    foreach($display_name as $key => $dispay) {
                        if(!in_array($dispay,$total_display_fileds)){
                        $t = $i +$newtext;
                        if($key =='country_id') {
                            $dats = get_country_fullname_code_name($data[$key]);
                           // $dats = $data[$key];
                        }else{
                            $dats = $data[$key];
                        }
                        $place_holder = $key == 'phone' ? "Phone Number" : ucfirst(str_replace('_',' ',$key));
                        $output .= ' <td class="align-middle">
                                        <input type="text" placeholder="'.$place_holder.'" value="'.$dats.'" data-id="'.$data['id'].'" data-key="'.$key.'"  class="add_edit_data" maxlength="16">
                                        <span class="col'.$t.'" style="display:none;">'.$dats.'</span>
                                    </td>';
                        $newtext ++;
                    }
                    }
                    $output .= ' <td class="align-middle">
                                        <i class="mdi mdi-trash-can-outline font-size-18 text-danger row-hover-action remove_button" data-id="'.$data['id'].'" data-toggle="tooltip" title="Delete"></i>
                                    </td>
                                  </tr>';
                    $i++;
                }
                $output .= '<tr class="add-row-tr">
                            <td colspan="7">
                                <span class="text-primary1 add-row">
                                    <i class="bx bx-plus-circle font-size-16 align-middle"></i>
                                    Add New Shipping Agent
                                </span>
                            </td>
                        </tr>';

            }else{
                $output .= '<tr class="selected-row-number" style="display:none;">
                                        <td colspan="9" class="text-center">
                                            Selected <span id="number-of-selected"></span> out of 100.
                                            <a class="text-primary select-all" id="select-all-link" style="cursor: pointer;">Select All</a>
                                        </td>
                                    </tr>
                                    <tr class="no-list">
                                        <td colspan="7" class="text-center">
                                            Click to add your first shipping agent
                                            <span class="text-primary1 add-row ml-3">
                                                            <i class="bx bx-plus-circle font-size-16 align-middle"></i>
                                                            Add Agent
                                                        </span>
                                        </td>
                                    </tr>';
            }
            return response()->json($output);
        }
        if($request->request_type == 'fileds_manage'){
           $global_marketplace = session('MARKETPLACE_ID');
            $total_unchecked_fileds = $request->checked_arr;
            User_field_access::where(array('marketplace_id'=>$global_marketplace,'type'=>4))->delete();
            if(!empty($total_unchecked_fileds)){
                foreach($total_unchecked_fileds as $val){
                    $req_data = [
                        'field_list_id'=>$val,
                        'marketplace_id'=>$global_marketplace,
                        'type'=>4
                    ];
                    User_field_access::create($req_data);
                }
            }
            $res['success'] = 1;
            return response()->json($res);
        }
        else if($request->request_type == 'get_pagi_country'){
            $global_marketplace = session('MARKETPLACE_ID');
            $pagination_left = '';
            $pagination_right = '';
            $pagination = array();
            $success = 0;
            $pagination_slot = $request->pagination_slot;
            if($global_marketplace != '') {
                $whereArray = [
                    "user_marketplace_id" => $global_marketplace
                ];
                if ($request->get_country) {
                    $whereArray = [
                        "user_marketplace_id" => $global_marketplace,
                        "country_id" => $request->get_country
                    ];
                }

                $data_count = ShippingAgent::where($whereArray)->count();

                $keyj = 1;
                if($data_count > 0){
                    for ($i = 1; $i <= $data_count; $i = $i + $pagination_slot) {
                        if ($i == 1) {
                            $newi = $i;
                        } else {
                            $newi = $i - 1;
                        }
                        $n = $i + ($pagination_slot - 1);
                        //$pagination[$keyj]= $newi.'-'.$n;
                        $pagination[$keyj] = ($newi) . '-' . $n;
                        $keyj++;
                    }
                }else{
                    $newi = 1;
                    $n = $newi + ($pagination_slot - 1);
                    $pagination[$newi] = ($newi) . '-' . $n;

                }
			$pagi = ($request->pagecount ? $request->pagecount : '');
                $success = 1;
                //$pagination_left = get_pagination_view($pagination);
                $pagination_right = get_pagination_right_view($pagination, $data_count,$pagination_slot,$pagi);
            }
            $res['success'] = $success;
           // $res['select_pagi'] = $pagination_left;
            $res['paginations'] = $pagination_right;
            $res['count'] = $data_count;
            return response()->json($res);
            exit;
            // echo json_encode($res); exit;
        }
        if($request->insert_type == 'delete_multiple_records'){
            ShippingAgent::whereIn('id',explode(",",$request->delete_array))->delete();
            return json_encode(array('statusCode'=>200));
        }
        if($request->insert_type =='create_update_data'){
            $check_exist = array();
            $global_marketplace = session('MARKETPLACE_ID');

            $get_country_id = Country::where(array('country_code'=>$request->country_id))->first();
            $req_data[$request->key] = $request->value;

            if($request->id != ''){
                if($request->key == 'email') {
                    $check_exist = ShippingAgent::where(array('email' => $request->value, ['id', '!=', $request->id]))->first();
                }
                    if(empty($check_exist)){
                        $return_data = ShippingAgent::where(array('id'=>$request->id))->update($req_data);
                        $res['message'] = get_messages('Shipping agent updated successfully.', 1);
                        $res['create'] = 0;
                        $res['error'] = 0;
                    }else{
                        $res['message'] = get_messages('Email already exist.', 0);
                        $res['create'] = 0;
                        $res['error'] = 1;
                    }
            }else{
                $option = '';
                if($request->key == 'email') {
                    $check_exist = ShippingAgent::where(array('email' => $request->value))->first();
                }
                if(empty($check_exist)) {
                    $req_data['user_marketplace_id'] = $global_marketplace;
                    $req_data['country_id'] = @$get_country_id->id;
                    $return_data = ShippingAgent::create($req_data);

//                    $get_all_fileds = array();
//                    $get_module_id =  get_module_id('shipping_agent_module');
//                    if($get_module_id > 0){
//                        $get_all_fileds = Field_list::where(array('active'=>'1',['display_name','!=',NULL],'module_id'=>$get_module_id))->orderBy('id','asc')->get()->toArray();
//                    }
//                    if($get_all_fileds){
//                        foreach($get_all_fileds as $key=>$data){
//                            $t = $key +1;
//                            if($request->key == $data['original_name']) {
//                                $datas = $request->value;
//                            }else{
//                                $datas = '';
//                            }
//                            $option .= '<tr id="'.$return_data->id.'">
//                                    <td>
//                                        <div class="custom-control custom-checkbox">
//                                            <input type="checkbox" class="custom-control-input row-checkbox" id="row-'.$t.'" name="delete_all" data-val="'.$return_data->id.'" value="'.$return_data->id.'" >
//                                            <label class="custom-control-label" for="row-'.$t.'"></label>
//                                        </div>
//                                    </td>
//                                     <td class="align-middle col'.$t.'">
//                                        <input type="text" value="'.$datas.'" data-id="'.$return_data->id.'" data-key="'.$t.'"  class="add_edit_data">
//                                    </td>
//                                    <td class="align-middle">
//                                        <i class="mdi mdi-trash-can-outline font-size-18 text-danger row-hover-action remove_button" data-id="'.$return_data->id.'" data-toggle="tooltip" title="Delete"></i>
//                                    </td>
//                                  </tr>';
//                        }
//                    }
//                    $res['option'] = $option;
                    $res['message'] = get_messages('Shipping agent created successfully.', 1);
                    $res['create'] = 1;
                    $res['error'] = 0;
					$res['id']=$return_data['id'];
                }else{
                   // $res['option'] = $option;
                    $res['message'] = get_messages('Email already exist.', 0);
                    $res['create'] = 0;
                    $res['error'] = 1;
                }
            }
            return response()->json($res);
        }
        else{
            $message = get_messages('Something wrong', 0);
            Session::flash('message', $message);
            return redirect()->route('shippingagent.index');
        }
    }

    /**
     * Show the form for editing the shipping agent.
     * @method POST
     * @params $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $get_checks = get_access('shipping_agent_module','edit');
        $get_user_access = get_user_check_access('shipping_agent_module','edit');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }

        $shipping_agent_data = ShippingAgent::where(array('id'=>$id))->get()->first();

        $country = Country::get()->toArray();

        return view('shipping_agent.edit-shipping_agent', [
                'shipping_agent' => $shipping_agent_data,
                'country'=>$country
            ]
        );
    }

    /**
     * Update the specified shipping agent.
     * @method PUT
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {

        $get_checks = get_access('shipping_agent_module','edit');
        $get_user_access = get_user_check_access('shipping_agent_module','edit');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }

        if (isset($request->insert_type) && $request->insert_type == 'edit_form') {


            $req_data = [
                'user_marketplace_id' => $request->marketplace_id,
                'company_name' => $request->company_name,
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'phone' => $request->phone,
                'country_id'=>$request->country
            ];
            if ($id != '') {

                $shipping_agent = ShippingAgent::findOrFail($id);
                $shipping_agent->update($req_data);

                $message = get_messages('Shipping Agent updated successfully', 1);
                Session::flash('message', $message);
                return redirect()->route('shippingagent.index');
            } else {
                $message = get_messages('Failed to update supplier data', 0);
                Session::flash('message', $message);
                return redirect()->route('shippingagent.index');
            }
        }else{
            $message = get_messages('Something wrong', 0);
            Session::flash('message', $message);
            return redirect()->route('shippingagent.index');
        }
    }

    /**
     * Remove the specified sipping agent.
     * @method POST
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        if ($id == 'import') {

            $import = new UsersExport();
            return Excel::download($import, 'shipping_agent.xlsx');

            // $file_name = 'shipping_agent.xlsx';
            // $file = public_path().'/sheets/'.$file_name;
            // //$file= public_path(). "/download/info.pdf";

            // $headers = array(
            //         'Content-Type: application/csv',
            //         );

            // return Response::download($file, $file_name, $headers);
        }
    }

    public function destroy($id)
    {

        $get_checks = get_access('shipping_agent_module','delete');
        $get_user_access = get_user_check_access('shipping_agent_module','delete');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }

        $shipping_agent = ShippingAgent::findOrFail($id);
        $shipping_agent->delete();

        return json_encode(array('statusCode'=>200));
    }

    /**
     * get shipping agent list
     * @method POST
     * @param  \Illuminate\Http\Response $request
     * @return \Illuminate\Http\Response
     */


    public function get_shipping_agent_list(Request $request)
    {
        $offsetValue = $request->offsetValue;
        $offsetValue = explode('-', $offsetValue);
        $offset = $offsetValue[0];
        //$limit = $offsetValue[1];
        $limit = env('PAGINATION');
        $output = '';
        if ($offset == 1) {
            $offset = 0;
        }else {
            $offset = $offset -1;
        }
        $global_marketplace = session('MARKETPLACE_ID');
        $shipping_agents = ShippingAgent::where(array('user_marketplace_id' => $global_marketplace))->offset($offset)
            ->limit($limit)->get()->toArray();
        if(!empty($shipping_agents)) {
            foreach ($shipping_agents as $shipping_agent) {
                $output .= ' <tr >' .
                    '<th scope = "row" width = "30%" >' .
                    '<ul class="d-flex flex-row align-items-center table-checkbox-main" >' .
                    '<li >' .
                    '<div class="form-group form-check" >' .
                    '<input type = "checkbox" class="form-check-input"
                                                   id = "exampleCheck1" >' .
                    '</div >' .
                    '</li >' .
                    '<li >' .
                    '<span >' . $shipping_agent['company_name'] . '</span >' .
                    '</li >' .
                    '</ul >' .
                    '</th >' .
                    '<td width = "15%" scope = "col" >' . $shipping_agent['first_name'] . '</td >' .
                    '<td width = "15%" scope = "col" >' . $shipping_agent['last_name'] . '</td >' .
                    '<td width = "20%" scope = "col" >' . $shipping_agent['email'] . '</td >' .
                    '<td scope = "col" class="d-flex flex-row justify-content-between align-items-center" >' .
                    '<span width = "30%" >' . $shipping_agent['phone'] . '</span >' .
                    '<ul class="d-flex flex-row align-items-center table-right-actions" >' .
                    '<li >' .
                    '<a href = "' . url('/shippingagent/' . $shipping_agent['id'] . '/edit') . '" >' .
                    '<svg xmlns = "http://www.w3.org/2000/svg" width = "13.208"
                                                 height = "13.208" viewBox = "0 0 13.208 13.208" >
                                                <g id = "Icon_feather-edit" data - name = "Icon feather-edit"
                                                   transform = "translate(1)" >
                                                    <path id = "Path_314" data - name = "Path 314"
                                                          d = "M8.46,6H4.213A1.213,1.213,0,0,0,3,7.213v8.494a1.213,1.213,0,0,0,1.213,1.213h8.494a1.213,1.213,0,0,0,1.213-1.213V11.46"
                                                          transform = "translate(-3 -4.713)" fill = "none"
                                                          stroke = "#a6b0cf" stroke - linecap = "round"
                                                          stroke - linejoin = "round" stroke - width = "2" />
                                                    <path id = "Path_315" data - name = "Path 315"
                                                          d = "M18.37,3.195a1.287,1.287,0,0,1,1.82,1.82l-5.764,5.764L12,11.386l.607-2.427Z"
                                                          transform = "translate(-8.36 -2.818)"
                                                          fill = "#a6b0cf" />
                                                </g >
                                            </svg >' . '</a >' .
                    '</li >' .
                    '<li class="px-2" ><svg xmlns = "http://www.w3.org/2000/svg"
                                                          width = "11.979" height = "15.13"
                                                          viewBox = "0 0 11.979 15.13" >
                                            <path id = "_25_Basket" data - name = "25 Basket"
                                                  d = "M8.91,15.13H3.07a1.577,1.577,0,0,1-1.576-1.491L.946,3.468a.946.946,0,1,1,0-1.891H4.413a1.576,1.576,0,1,1,3.152,0h3.468a.946.946,0,0,1,0,1.891l-.549,10.172A1.576,1.576,0,0,1,8.91,15.13ZM7.565,5.043a.631.631,0,0,0-.63.63v6.3a.63.63,0,0,0,1.261,0v-6.3A.631.631,0,0,0,7.565,5.043Zm-3.152,0a.631.631,0,0,0-.63.63v6.3a.63.63,0,1,0,1.26,0v-6.3A.631.631,0,0,0,4.413,5.043Z"
                                                  fill = "#a6b0cf" />
                                        </svg >
                                    </li >' .
                    '<li ><svg xmlns = "http://www.w3.org/2000/svg" width = "20"
                                             height = "19" viewBox = "0 0 20 19" >
                                            <g id = "Group_9340" data - name = "Group 9340"
                                               transform = "translate(-1177 -433)" >
                                                <path id = "Polygon_7" data - name = "Polygon 7"
                                                      d = "M7.414,4.4a3,3,0,0,1,5.172,0l4.755,8.083A3,3,0,0,1,14.755,17H5.245a3,3,0,0,1-2.586-4.521Z"
                                                      transform = "translate(1177 433)"
                                                      fill = "#f1b44c" />
                                                <text id = "_" data - name = "!"
                                                      transform = "translate(1185 447)" fill = "#fff"
                                                      stroke = "rgba(0,0,0,0)" stroke - width = "1"
                                                      font - size = "10"
                                                      font - family = "Poppins-SemiBold, Poppins"
                                                      font - weight = "600" >
                                                    <tspan x = "0" y = "0" > !</tspan >
                                                </text >
                                            </g >
                                        </svg >
                                    </li >' .
                    '</ul >' .
                    '</td >' .
                    '</tr >';
            }
        }
        return response()->json($output);
    }

    public function ImportShippingagent(Request $request)
    {
        \Session::forget('error_msg_array');
        \Session::forget('success_message');
        
        $fileContent = file_get_contents(request()->file('file'));
        $enc = mb_detect_encoding($fileContent, mb_list_encodings(), true);
        \Config::set('excel.imports.csv.input_encoding', $enc);
        $ShippingagentImport = new ShippingagentImport();
        $collection= Excel::import($ShippingagentImport ,request()->file('file'),'UTF-8');
         return redirect('/shippingagent_import_message');
    }

    public function shippingagent_import_message()
    {
       
        $error_msg=((\Session::get('error_msg_array')) ? \Session::get('error_msg_array') : '');
        $success_msg=((\Session::get('success_message')) ? \Session::get('success_message') : '');
        
        $error_count=((\Session::get('error_count')) ? \Session::get('error_count') : '');
        $success_count=((\Session::get('success_count')) ? \Session::get('success_count') : '');
        $total_count=((\Session::get('total_count')) ? \Session::get('total_count') : '');

        if(!empty($error_msg)){
            return view('shipping_agent.import_messages',compact('error_msg','success_msg','total_count','error_count','success_count'));
        }else{
            $message = get_messages("Total $total_count rows uploaded successfully", 1);
            Session::flash('message', $message);
            return redirect('/shippingagent');
        }
        
    }

    public function getData(){
        return ShippingAgent::orderBy('id','desc')->paginate(10);
    }

    public function bulkDelete(Request $request){
        $result = ShippingAgent::whereKey($request->selected)->delete();
        return ['success' => $result];
    }

    public function bulkExport(){
        return response()->streamDownload(function(){
            echo ShippingAgent::toCsv();
        }, 'vendors'. date("Ymdhis") .'.csv');
    }

    public function updateCols(){
        if(DB::table('cols')->where('user_id', auth()->id())->where('table_name', 'shipping_agents')->get()->count()){
            DB::table('cols')
                ->where('user_id', auth()->id())
                ->where('table_name', 'shipping_agents')
                ->update(['cols' => request()->cols]);
        }else{
            DB::table('cols')
                ->insert([
                    'user_id' => auth()->id(),
                    'table_name' => 'shipping_agents',
                    'cols' => json_encode(request()->cols)
                ]);
        }
    }

}

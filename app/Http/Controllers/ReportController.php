<?php

namespace App\Http\Controllers;

use App\Models\Amazon_requestlog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Jobs\RequestReport;
use App\Models\Usermarketplace;

class ReportController extends Controller
{
    /**
     * Display a listing view of the report.
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get_checks = get_access('report_request_module','view');
        $get_user_access = get_user_check_access('report_request_module','view');
        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
                if ($get_user_access == 0) {
                    $message = get_messages("Don't have Access Rights for this Functionality", 0);
                    Session::flash('message', $message);
                    return redirect('/index');
                }
        }
        return view('report.index');
    }

    /**
     * Show the form for creating a new report with particular marketplace wise.
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $get_checks = get_access('report_request_module','create');
        $get_user_access = get_user_check_access('report_request_module','create');
        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
                if ($get_user_access == 0) {
                    $message = get_messages("Don't have Access Rights for this Functionality", 0);
                    Session::flash('message', $message);
                    return redirect('/index');
                }
        }

        $userMarketplace = Usermarketplace::with('user')->get()->toArray();
        return view('report.create', ['userMarketplace' => $userMarketplace]);
    }

    /**
     * There are 2 function added in store method :
     *      @method POST
     *
     *  Request Types:
     *
     * 1. request_report_list : get all records of report to show in report listing
     *      @param  $requesttype
     *      @return \Illuminate\Http\Response
     *
     * * 2. insert_request_report: store a newly created report.
     *      @param $requesttype, $request
     *      @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {

        if($request->request_type == 'request_report_list'){
            
            $data = Amazon_requestlog::with('usermarketplace', 'usermarketplace.user')->orderBy('amazon_reportrequestlog.id','desc')->limit('5000')->get()->toArray();
           
            $report_data = array();

            if(!empty($data))
            {
                foreach ($data as $key => $post)
                {
                    $nestedData['user_name'] = $post['usermarketplace']['user']['name'];
                    $nestedData['request_id'] =  $post['request_id'];
                    $nestedData['status'] =  $post['status'];
                    $nestedData['report_type'] =  $post['report_type'];
                    $report_data[] = $nestedData;               
                }    
            }
                
            echo json_encode($report_data); exit;
        
        }else if($request->insert_type == 'insert_request_report'){

            $amzDateitetration['startDate'] = $request->start_date;
            $amzDateitetration['endDate'] = $request->end_date;

            $userMarketplace = Usermarketplace::where('id',$request->marketplace_id)->first();

            if(empty($userMarketplace)){
                $message = get_messages("Marketplace not found", 0);
                Session::flash('message', $message);
                return redirect()->back();
            }
            
            $orderRequestJob = new RequestReport($userMarketplace, $request->report_type, $amzDateitetration);
            dispatch($orderRequestJob)->onQueue('low');
            
            $message = get_messages('Request report inserted successfully!',1);
            Session::flash('message', $message);
            return redirect()->route('report.index');
        }
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Calculated_sales_qty;
use App\Models\Mws_afn_inventory_data;
use App\Models\Mws_product;
use App\Models\Mws_unsuppressed_inventory_data;
use App\Models\Inbound_shipment_items;
use Carbon\Carbon;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display the specified inventory data .
     * @method POST
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inventory(){
        $userid=$_POST['userid'];
        if(empty($_POST['sku'])){
            $get_all_product_sku = Mws_product::with(['mws_reserved_inventory'])->where(array('user_marketplace_id' => $userid))->selectRaw('sku,id')->get()->toArray();
        }
        else {
            $skuArray = explode(',',$_POST['sku']);
            $get_all_product_sku = Mws_product::with(['mws_reserved_inventory'])->whereIn('sku', $skuArray)->where(array('user_marketplace_id' => $userid))->selectRaw('sku,id')->get()->toArray();
        }

        $data = [];
        foreach ($get_all_product_sku as $key=>$result) {
            $mws_afn_inventory = Mws_afn_inventory_data::where('product_id', $result['id'])->where('warehouse_condition_code', 'SELLABLE')->first();
            $mws_unsuppressed_inventory = Mws_unsuppressed_inventory_data::where('product_id', $result['id'])->first();
            $data[$key]['sku']=$result['sku'];
            $data[$key]['quantity_available']=(isset($mws_afn_inventory->quantity_available) ? $mws_afn_inventory->quantity_available : '');
            $data[$key]['afn_fulfillable_qty']=$mws_unsuppressed_inventory['afn_fulfillable_qty'];
            $data[$key]['fc_transfer']= $result['mws_reserved_inventory']['reserved_fc_transfer'];
            $data[$key]['fc_process']= $result['mws_reserved_inventory']['reserved_fc_processing'];
            $data[$key]['customer_order']= $result['mws_reserved_inventory']['reserved_customer_orders'];
            $data[$key]['reserved_qty']= $result['mws_reserved_inventory']['reserved_qty'];
            $data[$key]['research_qty']= $mws_unsuppressed_inventory['afn_research_qty'];
            $data[$key]['unsellable_qty']= $mws_unsuppressed_inventory['afn_unsellable_qty'];
            $data[$key]['warehouse_qty']= $mws_unsuppressed_inventory['afn_warehouse_qty'];
            $data[$key]['afn_inbound_working_qty']= $mws_unsuppressed_inventory['afn_inbound_working_qty'];
            $data[$key]['inbound_receiving_qty']= $mws_unsuppressed_inventory['afn_inbound_receving_qty'];
            $data[$key]['inbound_shipment_qty']= $mws_unsuppressed_inventory['afn_inbound_shipment_qty'];
            $data[$key]['total_qty']= $mws_unsuppressed_inventory['afn_total_qty'];
        }
        return response()->json([
            'inventory' => $data,
        ]);
    }

    /**
     * Display the specified inbound shipment items.
     * @method POST
     * @param  \Illuminate\Http\Response $request
     * @return \Illuminate\Http\Response
     */

    public function inbound_breakdown(){
        $userid=$_POST['userid'];
        if(empty($_POST['sku'])){
            $get_all_product_sku = Mws_product::with(['mws_reserved_inventory'])->where(array('user_marketplace_id' => $userid))->selectRaw('sku,id')->get()->toArray();
        }
        else {
            $skuArray = explode(',',$_POST['sku']);
            $get_all_product_sku = Mws_product::with(['mws_reserved_inventory'])->whereIn('sku', $skuArray)->where(array('user_marketplace_id' => $userid))->selectRaw('sku,id')->get()->toArray();
        }
        $newData = array();
        foreach ($get_all_product_sku as $key=>$result) {
            //$mws_afn_inventory = Mws_afn_inventory_data::where('product_id',$result['id'])->where('warehouse_condition_code','SELLABLE')->first()->toArray();
            $datas = Inbound_shipment_items::with(['inbound_shipment'])->where(array('product_id' => $result['id']))->get()->toArray();
            foreach ($datas as $inc=>$inbound){
                $differenceInventory = $inbound['QuantityShipped'] - $inbound['QuantityReceived'];
                $newData[$result['sku']][$inc]['inbound'] = $differenceInventory;
                $newData[$result['sku']][$inc]['ShipmentName'] = $inbound['inbound_shipment']['ShipmentName'];
                $newData[$result['sku']][$inc]['ShipmentId'] = $inbound['inbound_shipment']['ShipmentId'];
                $newData[$result['sku']][$inc]['ShipmentStatus'] = $inbound['inbound_shipment']['ShipmentStatus'];
                $newData[$result['sku']][$inc]['QuantityShipped'] = $inbound['QuantityShipped'];
                $newData[$result['sku']][$inc]['QuantityReceived'] = $inbound['QuantityReceived'];
            }
        }
        return response()->json([
            'inbound_breakdown' => $newData,
        ]);
    }

    /**
     * Display the specified sales history
     * @method POST
     * @param  \Illuminate\Http\Response $request
     * @return \Illuminate\Http\Response
     */
    public function sales_history(){
        $userid=$_POST['userid'];
        if(empty($_POST['sku'])){
            $get_all_product_sku = Mws_product::with(['mws_reserved_inventory'])->where(array('user_marketplace_id' => $userid))->selectRaw('sku,id')->get()->toArray();
        }
        else {
            $skuArray = explode(',',$_POST['sku']);
            $get_all_product_sku = Mws_product::with(['mws_reserved_inventory'])->whereIn('sku', $skuArray)->where(array('user_marketplace_id' => $userid))->selectRaw('sku,id')->get()->toArray();
        }
        $newData = array();
        foreach ($get_all_product_sku as $key=>$record) {

            $twentyFourHoursBackDate = Carbon::yesterday()->format('Y-m-d');
            $twentyFourHoursSales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '>=', $twentyFourHoursBackDate)->sum('total_qty');
            if ($twentyFourHoursSales > 0) {
                $twentyFourHoursyRate = round(($twentyFourHoursSales / 1), 6);
            } else {
                $twentyFourHoursSales = $twentyFourHoursyRate = 0;
            }

            if(isset($record['daily_logic_calculations'][0]['sevendays_sales'])){
                $sevenDaysSales = $record['daily_logic_calculations'][0]['sevendays_sales'];
                $sevenDaysDayRate = $record['daily_logic_calculations'][0]['sevenday_day_rate'];
            }else{
                $sevenDaysBackDate = Carbon::now()->subDays(7)->format('Y-m-d');
                $sevenDaysSales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '>=', $sevenDaysBackDate)->sum('total_qty');
                if ($sevenDaysSales > 0) {
                    $sevenDaysDayRate = round(($sevenDaysSales / 7), 6);
                } else {
                    $sevenDaysSales = $sevenDaysDayRate = 0;
                }

            }
            if(isset($record['daily_logic_calculations'][0]['fourteendays_sales'])){
                $fourteenDaysSales = $record['daily_logic_calculations'][0]['fourteendays_sales'];
                $fourteenDaysDayRate = $record['daily_logic_calculations'][0]['fourteendays_day_rate'];
            }else{
                $fourteenDaysBackDate = Carbon::now()->subDays(14)->format('Y-m-d');
                $fourteenDaysSales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '>=', $fourteenDaysBackDate)->sum('total_qty');
                if ($fourteenDaysSales > 0) {
                    $fourteenDaysDayRate = round(($fourteenDaysSales / 14), 6);
                } else {
                    $fourteenDaysSales = $fourteenDaysDayRate = $fourteenDaysDayOfSupply = 0;
                }
            }
            if(isset($record['daily_logic_calculations'][0]['thirtydays_sales'])){
                $thirtyDaysSales = $record['daily_logic_calculations'][0]['thirtydays_sales'];
                $thirtyDaysDayRate = $record['daily_logic_calculations'][0]['thirtyday_day_rate'];
            }else{
                $thirtyDaysBackDate = Carbon::now()->subDays(30)->format('Y-m-d');
                $thirtyDaysSales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '>=', $thirtyDaysBackDate)->sum('total_qty');
                if ($thirtyDaysSales > 0) {
                    $thirtyDaysDayRate = round(($thirtyDaysSales / 30), 6);
                } else {
                    $thirtyDaysSales = $thirtyDaysDayRate = $thirtyDaysDayOfSupply = 0;
                }
            }
            if(isset($record['daily_logic_calculations'][0]['sevendays_previous'])){
                $previousSevenDaysSales = $record['daily_logic_calculations'][0]['sevendays_previous'];
                $previousSevenDaysDayRate = $record['daily_logic_calculations'][0]['sevendays_previous_day_rate'];
            }else{
                $fourteenDaysBackDate = Carbon::now()->subDays(14)->format('Y-m-d');
                $previousSevenDaysSales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '<=', $sevenDaysBackDate)->where('order_date', '>=', $fourteenDaysBackDate)->sum('total_qty');
                if ($previousSevenDaysSales > 0) {
                    $previousSevenDaysDayRate = round(($previousSevenDaysSales / 7), 6);
                } else {
                    $previousSevenDaysDayRate = 0;
                }
            }
            if(isset($record['daily_logic_calculations'][0]['thirtydays_previous'])){
                $previousThirtyDaysSales = $record['daily_logic_calculations'][0]['thirtydays_previous'];
                $previousThirtyDaysDayRate = $record['daily_logic_calculations'][0]['thirtydays_previous_day_rate'];
            }else{
                $thirtyDaysBackDate = Carbon::now()->subDays(30)->format('Y-m-d');
                $sixtyDaysBackDate = Carbon::now()->subDays(60)->format('Y-m-d');
                $previousThirtyDaysSales = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '<=', $thirtyDaysBackDate)->where('order_date', '>=', $sixtyDaysBackDate)->sum('total_qty');
                if ($previousThirtyDaysSales > 0) {
                    $previousThirtyDaysDayRate = round(($previousThirtyDaysSales / 30), 6);
                } else {
                    $previousThirtyDaysDayRate = 0;
                }
            }
            if(isset($record['daily_logic_calculations'][0]['thirtydays_last_year'])){
                $thirtyDaysSalesLastyear = $record['daily_logic_calculations'][0]['thirtydays_last_year'];
                $ThirtyDaysDayRateLastYear = $record['daily_logic_calculations'][0]['thirtydays_last_year_day_rate'];
            }else{
                $thirtyDaysStartDateLastYear = Carbon::now()->subDays(395)->format('Y-m-d');
                $thirtyDaysEndDateLastYear = Carbon::now()->subDays(365)->format('Y-m-d');
                $thirtyDaysSalesLastyear = Calculated_sales_qty::where('product_id', $record['id'])->where('order_date', '<=', $thirtyDaysEndDateLastYear)->where('order_date', '>=', $thirtyDaysStartDateLastYear)->sum('total_qty');
                if ($thirtyDaysSalesLastyear > 0) {
                    $ThirtyDaysDayRateLastYear = round(($thirtyDaysSalesLastyear / 30), 6);
                } else {
                    $ThirtyDaysDayRateLastYear = 0;
                }
            }
            $newData[$key]['sku'] = $record['sku'];
            $newData[$key]['twentyFourHoursSales']=$twentyFourHoursSales;
            $newData[$key]['sevenDaysSales']= $sevenDaysSales;
            $newData[$key]['fourteenDaysSales']= $fourteenDaysSales;
            $newData[$key]['thirtyDaysSales']=  $thirtyDaysSales;
            $newData[$key]['previousSevenDaysSales']=  $previousSevenDaysSales;
            $newData[$key]['previousThirtyDaysSales']=  $previousThirtyDaysSales;
            $newData[$key]['thirtyDaysSalesLastyear']=  $thirtyDaysSalesLastyear;
            $newData[$key]['twentyFourHoursyRate']=$twentyFourHoursyRate;
            $newData[$key]['sevenDaysDayRate']= $sevenDaysDayRate;
            $newData[$key]['fourteenDaysDayRate']= $fourteenDaysDayRate;
            $newData[$key]['$thirtyDaysDayRate']=  $thirtyDaysDayRate;
            $newData[$key]['previousSevenDaysDayRate']=  $previousSevenDaysDayRate;
            $newData[$key]['previousThirtyDaysDayRate']=  $previousThirtyDaysDayRate;
            $newData[$key]['ThirtyDaysDayRateLastYear']=  $ThirtyDaysDayRateLastYear;
            for ($count=0;$count<=24;$count++){
                $get_datemonth =  date('Y-m',strtotime("-$count month"));
                $get_supply_total_trands = get_supply_total_trands($get_datemonth,$record['id']);
                if(!empty($get_supply_total_trands)){
                    $newData[$key][$get_datemonth]=$get_supply_total_trands->sales_total;
                    $newData[$key][$get_datemonth."_day_rate"]=$get_supply_total_trands->day_rate;
                }
                else{
                    $newData[$key][$get_datemonth]='';
                    $newData[$key][$get_datemonth."_day_rate"]='';
                }
            }
        }
        return response()->json([
            'sales_history' => $newData,
        ]);
    }
}

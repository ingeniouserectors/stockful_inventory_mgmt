<?php

namespace App\Http\Controllers;
use App\Models\Daily_logic_Calculations;
use App\Models\Temp_session;
use Illuminate\Http\Request;
Use App\Models\Warehouse;
Use App\Models\Purchase_instance_details;
Use App\Models\Purchase_instances;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Models\Vendor_default_setting;
use App\Models\Vendor_wise_default_setting;  
use App\Models\Supplier_wise_default_setting;
use Illuminate\Support\Facades\Gate;
use App\Models\Application_modules;
use App\Models\Role_access_modules;
use App\Models\Users;

class PurchaseInstancesController extends Controller
{
    Protected $userRolePermissionId;

    function __construct()
    {
        $this->middleware(function ($request, $next) {

            if (!Auth::user()) {
                return redirect('login');
            }
            $moduleId = Application_modules::where('module', 'pi_module')->first()->id;
            $userRoleID = Users::with('user_roles')->whereIn('id', [Auth::id()])->first()->user_roles[0]->id;
            $this->userRolePermissionId = Role_access_modules::where('role_id', $userRoleID)->where('application_module_id', $moduleId)->first();
            return $next($request);
        });
    }

    /**
     * Display a listing of the purchase instance details.
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $get_checks = get_access('pi_module','view');
        $get_user_access = get_user_check_access('pi_module','view');
        if ($get_checks == 1) {
            if($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }

        $global_marketplace = session('MARKETPLACE_ID');
        $order_listing = array();
        $Purchase_instances = array();
        $Completed_Purchase_instances = array();
        if($global_marketplace != '') {
            $Completed_Purchase_instances = Purchase_instances::with('purchase_instances_details', 'usermarketplace', 'product')->where('status', 2)->where(array('user_marketplace_id'=>$global_marketplace))->get()->ToArray();
            $Purchase_instances = Purchase_instances::with('purchase_instances_details', 'usermarketplace', 'product')->where('status', 1)->where(array('user_marketplace_id'=>$global_marketplace))->get()->ToArray();
        }
       return view('purchaseinstances.index',[

         'purchaseinstances_details'=> $Purchase_instances,
          'Completed_Purchase_instances'=> $Completed_Purchase_instances

       ]);


    }

    /**
     * Show the form for creating a new purchase instance with particular marketplace wise.
     * @param  no-params
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = array();
        $warehouse = array();
        $global_marketplace = session('MARKETPLACE_ID');
        if($global_marketplace != ''){
            $warehouse = Warehouse::where(array('user_marketplace_id'=>$global_marketplace))->get();
            $products = Daily_logic_Calculations::with(['mws_product'])->where(array('user_marketplace_id'=>$global_marketplace))->get();
        }
        $Purchase_instances = Purchase_instances::with('purchase_instances_details', 'usermarketplace', 'product')->where('status', 1)->where(array('user_marketplace_id'=>$global_marketplace))->get()->ToArray();
        
            return view('purchaseinstances.create',['warehouse'=>$warehouse,'products'=>$products]);
    }

    /**
     * There are 8 function added in store method :
     *      @method POST
     *
     *  Request Types:
     *
     * 1. pendding_purchaseinstances_list : listing of pending purchase instance
     *      @param  $requesttype
     *      @return \Illuminate\Http\Response
     *
     * 2. comleted_purchaseinstances_list: listing of completed purchase instance
     *      @param  $requesttype
     *      @return \Illuminate\Http\Response
     *
     * 3. Create_form: Create a new purchase instance with these method
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     * 4. ajax_save_purchase_details: update the purchase details with ajax method
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     * 5. delete_purchase_details: delete the purchase details with ajax
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     * 6. vendors_suppliers_warehouse: get vendors,suppliers & warehouse name & id using these method
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     * 7. assign_model_data: assign calculated data to temp. selected product
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     * 8. product_assign_calculated_qty: assign to calculated qty to particular product
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        if($request->request_type == 'pendding_purchaseinstances_list'){    
             $global_marketplace = session('MARKETPLACE_ID');
            $data = Purchase_instances::with('purchase_instances_details', 'usermarketplace', 'product')->where('status', 1)->where(array('user_marketplace_id'=>$global_marketplace))->get()->ToArray();        
          $pendding_data = array();
         if(!empty($data))
            {
                 foreach ($data as $key => $post)
                {
                    $action = '' ;
                    
                    $nestedData['created_at'] =  date('Y-m-d',strtotime($post['created_at']));
                    $nestedData['track_id'] =  $post['track_id'];
                    
                    $action .= '<a href="'.route('purchaseinstances.edit',$post['id']).'" class=" btn btn-primary btn-sm btn-rounded waves-effect waves-light" role="button" ><i class="fas fa-edit"></i></a>';
                    $action .= '<a href="'.route('purchaseinstances.show',$post['id']).'" class=" btn btn-primary btn-sm btn-rounded waves-effect waves-light" role="button"><i class="fas fa-eye"></i></a>';
                    if(!empty($post['purchase_instances_details']))
                    {
                       $action .= '<a href="'.route('purchaseorder.show',$post['id']).'" class=" btn btn-primary btn-sm btn-rounded waves-effect waves-light" role="button" title="Confirm for PO"><i class="fas fa-check-circle"></i></a>';
                    }
                    $action .=  '<button id="button" type="submit" class=" btn btn-danger btn-sm btn-rounded waves-effect  waves-light sa-remove " data-id='.$post['id'].'><i class="fas fa-trash-alt"></i></button>';                    
                    $nestedData['action'] =  $action ;
                    $pendding_data[] = $nestedData;
                }    
             }
                
           echo json_encode($pendding_data); exit;

        }
        if($request->request_type == 'comleted_purchaseinstances_list'){    
            $global_marketplace = session('MARKETPLACE_ID');
            $data = Purchase_instances::with('purchase_instances_details', 'usermarketplace', 'product')->where('status', 2)->where(array('user_marketplace_id'=>$global_marketplace))->get()->ToArray();        
          $received_data = array();
         if(!empty($data))
            {
                 foreach ($data as $key => $post)
                {
                    $action = '' ;
                    
                    $nestedData['created_at'] =  date('Y-m-d',strtotime($post['created_at']));
                    $nestedData['track_id'] =  $post['track_id'];
                    $action .= '<a href="'.route('purchaseinstances.show',$post['id']).'" class=" btn btn-primary btn-sm btn-rounded waves-effect waves-light" role="button" title="Contact"><i class="fas fa-eye"></i></a>';
                    $nestedData['action'] =  $action ;
                    $received_data[] = $nestedData;
                }    
             }
                
           echo json_encode($received_data); exit;

        }
        $inputs = $request->all(); 
        $global_marketplace = session('MARKETPLACE_ID');
        if($inputs['input_type']== 'Create_form'){
        	
            $products = $inputs['product_id'];
            $vendors_select_id = $inputs['vendors_select_id'];
            $suppliers_select_id = $inputs['suppliers_select_id'];
            $warehouse_select_id = $inputs['warehouse_select_id'];
            $projected_units = $inputs['projected_units'];
          
          
            $items_per_cartoons = array_filter($inputs['items_per_cartoons']);
            $containers = array_filter($inputs['containers']);
            $cbm = array_filter($inputs['cbm']);
            $subtotals = array_filter($inputs['subtotal']);

            if($inputs['purchase_instance_id'] == ''){
                $product_instance = [
                    'user_marketplace_id'=>$global_marketplace,
                    'track_id'=> $this->gerate_track_id(),
                    'status'=>1
                ];
                $perchase = Purchase_instances::create($product_instance);
                $purchaseinstanceid = $perchase->toArray();
                $last_purchase_instace_id = $purchaseinstanceid['id'];

            }else{
                $last_purchase_instace_id = $inputs['purchase_instance_id'];
            }

            if(!empty($products)){
                foreach($products as $key=>$value){
                    $puchase_data[] = array(
                        'purchesinstances_id' =>$last_purchase_instace_id,
                        'product_id' => $value,
                        'supplier' => isset($suppliers_select_id[$key]) && $suppliers_select_id[$key] != '' ? $suppliers_select_id[$key] : 0,
                        'vendor' => isset($vendors_select_id[$key]) && $vendors_select_id[$key] != '' ? $vendors_select_id[$key] : 0,
                        'warehouse' => isset($warehouse_select_id[$key]) && $warehouse_select_id[$key] != '' ? $warehouse_select_id[$key] : 0,
                        'projected_units' => isset($projected_units[$key]) && $projected_units[$key] != '' ? $projected_units[$key] : 0,
                        'items_per_cartoons' =>isset($items_per_cartoons[$key]) && $items_per_cartoons[$key] != '' ? $items_per_cartoons[$key] : 0,
                        'containers' =>isset($containers[$key]) && $containers[$key] != '' ? $containers[$key] : 0,
                        'cbm' =>isset($cbm[$key]) && $cbm[$key] != '' ? $cbm[$key] : 0,
                        'subtotal' =>isset($subtotals[$key]) && $subtotals[$key] != '' ? $subtotals[$key] : 0,
                        'created_at' => date('Y-m-d h:i:s'),
                        'updated_at' => date('Y-m-d h:i:s'),
                    );
                }
                
                $puchase_detail = Purchase_instance_details::insert($puchase_data);
            }
            if($last_purchase_instace_id){
                $message = get_messages('Purchase instance created successfully', 1);
                Session::flash('message', $message);
                return redirect()->route('purchaseinstances.index');
            }else{
                $message = get_messages('Failed to create purchase instance', 0);
                Session::flash('message', $message);
                return redirect()->route('purchaseinstances.index');
            }

        }

        if($inputs['input_type'] == 'ajax_save_purchase_details'){
           $subtotal = $inputs['projected_units']*$inputs['items_per_cartoons']*$inputs['containers']*$inputs['cbm'];
            $update = [
               'projected_units'=>$inputs['projected_units'],
               'items_per_cartoons'=>$inputs['items_per_cartoons'],
               'containers'=>$inputs['containers'],
               'cbm' =>$inputs['cbm'],
                'subtotal'=>$subtotal
           ];
           $update = Purchase_instance_details::where(array('id'=>$inputs['id']))->update($update);
           if($update){
               $res['error'] = 0;
               $message = get_messages('Purchase instance details updated successfully', 1);
               Session::flash('message', $message);
           }
           else{
               $res['error'] = 1;
               $res['message'] = get_messages('Failed to update purchase instance details', 0);
           }
            return response()->json($res);
        }

        if($inputs['input_type'] == 'delete_purchase_details'){
            $get_data = Purchase_instance_details::where(array('id'=>$inputs['id']))->first();
            if(!empty($get_data)){
                $purchase_instance_id = $get_data->purchesinstances_id;
                $details = Purchase_instance_details::findOrFail($inputs['id']);
                $delete = $details->delete();

                $details_purchase = Purchase_instance_details::where(array('purchesinstances_id'=>$purchase_instance_id))->get()->toArray();
                if(empty($details_purchase)){
                    $delete = Purchase_instances::where(array('id'=>$purchase_instance_id))->delete();
                }

                if($delete){
                    $res['error'] = 0;
                    $message = get_messages('Purchase instance details deleted successfully', 1);
                    Session::flash('message', $message);
                }
                else{
                    $res['error'] = 1;
                    $res['message'] = get_messages('Failed to update purchase instance details', 0);
                }
            }else{
                $res['error'] = 1;
                $res['message'] = get_messages('No record found.', 0);
            }
            return response()->json($res);
        }

        if($inputs['input_type'] == 'vendors_suppliers_warehouse'){

            $vendors_id = 0;
            $suppliers_id = 0;
            $warehouse_id = 0;
            $cbm1 = "";

            $vendors_suppliers_name = ' - ';
            $warehouse_name = ' - ';
            $projected_units= ' - ';
            $cbm = ' - ';
            $items_per_cartoons = ' - ';
            $check_vendors = get_product_vendors($inputs['product_id']);
            $check_suppliers = get_product_suppliers($inputs['product_id']);
            $check_warehouse = get_product_warehouse($inputs['product_id']);

           
            $check_projected_units=Daily_logic_Calculations::where(array('prod_id' =>$inputs['product_id']))->get()->toArray();

            if(!empty($check_projected_units)){
                $projected_units= $check_projected_units[0]['projected_reorder_qty'];
            }
            else
            {
                return 0;
            }

            if(!empty($check_vendors)){
                $vendors_id = $check_vendors[0]['vendor_id'];
                $vendors_suppliers_name = $check_vendors[0]['vendor']['vendor_name'];
                $cbm1 = $check_vendors[0]['CBM_Per_Container'];
                $qty_per_carton = $check_vendors[0]['Qty_per_Carton'];
                if($projected_units != " - "){
                	$items_per_cartoons = $projected_units / $qty_per_carton;
                }

            }else if(!empty($check_suppliers)){
                $suppliers_id = $check_suppliers[0]['supplier_id'];
                $vendors_suppliers_name = $check_suppliers[0]['supplier']['supplier_name'];
                $cbm1 = $check_suppliers[0]['CBM_Per_Container'];
                $qty_per_carton = $check_suppliers[0]['Qty_per_Carton'];
                if($projected_units != " - "){
                	$items_per_cartoons = $projected_units / $qty_per_carton;
                }
            }
            if(!empty($check_warehouse)){
                $warehouse_id = $check_warehouse[0]['warehouse_id'];
                $warehouse_name = $check_warehouse[0]['warehouse']['warehouse_name'];
            }

            if((!empty($check_vendors)) || (!empty($check_suppliers)) ){
                if(!empty($check_vendors)){
                    $cbm = $cbm1;
                    if($cbm == "" && $cbm != null){
                        $getDefault = Vendor_wise_default_setting::where("vendor_id",$vendors_id)->first();
                        $cbm = $getDefault->cbm_per_container;
                    }
                }else{
                    $cbm = $cbm1;
                    if($cbm == "" && $cbm != null){
                        $getDefault = Supplier_wise_default_setting::where("supplier_id",$suppliers_id)->first();
                        $cbm = $getDefault->CBM_Per_Container;
                    }
                }
            }else{
                $getDefault = Vendor_default_setting::first();
                $cbm = $getDefault->cbm_per_container;
            }


            $res['vendors_id'] = $vendors_id;
            $res['suppliers_id'] = $suppliers_id;
            $res['warehouseid'] = $warehouse_id;

            $res['vendors_suppliers_name'] = $vendors_suppliers_name;
            $res['projected_units'] =  session($inputs['product_id']) != '' ? session($inputs['product_id']) :  $projected_units;
            $res['warehousename'] = $warehouse_name;
            $res['cbm'] =   $cbm;
            $res['items_per_cartoons'] = round($items_per_cartoons,2);

            return response()->json($res);
        }
        if($inputs['input_type'] == 'assign_model_data'){
          $id=$inputs['pi_id'];
            $projected_units = Purchase_instance_details::where('id',$id)->first();
            $daily_projected_units = Daily_logic_Calculations::where(array('prod_id'=>$inputs['pro_id']))->first();
            $session_projected_unit =Temp_session::where('product_id',$inputs['pro_id'])->first();
      
            $res['session_projected_units'] = isset($session_projected_unit['recalulated_qty']) ? $session_projected_unit['recalulated_qty'] : 0;
            $res['daily_projected_units'] = isset($daily_projected_units['projected_reorder_qty']) ? $daily_projected_units['projected_reorder_qty'] : 0;
            $res['projected_units'] = isset($projected_units['projected_units']) ? $projected_units['projected_units'] : 0;
            return response()->json($res);
        }

        if($inputs['input_type'] == 'product_assign_calculated_qty'){

            $products = explode(',',$inputs['product_id']);
            $projected_units = explode(',',$inputs['projected_units']);
            $product_name = explode(',',$inputs['product_name']);
            $id_arr = explode(',',$inputs['ids_arr']);

            $res = array();
            if(!empty($products)){$i = 0;
                foreach($products as $key => $product){
                    $get_check = Temp_session::where(array('product_id'=>$product,'marketplace_id'=>session('MARKETPLACE_ID')))->first();
                    if(!empty($get_check)){
                        $res[$i]['change_units'] = $get_check->recalulated_qty != '' ? $get_check->recalulated_qty : 0;
                        $res[$i]['projected_unitss'] = $projected_units[$key];
                        $res[$i]['product_namess'] = $product_name[$key];
                        $res[$i]['productid'] = $products[$key];
                        $res[$i]['uniqueid'] = trim($id_arr[$key]);
                        $i++;
                    }
                }
            }
            return response()->json($res);
        }

    }

    /**
     * Display the purchase instance view details
     * @method POST
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $purchase_instances=Purchase_instances::with('purchase_instances_details','usermarketplace','product')->where('id',$id)->get()->first();
        $purchases_details = Purchase_instance_details::where(array('purchesinstances_id'=>$id))->get()->toArray();

        return view('purchaseinstances.purchase_instanceview',[
            'view'=> $purchase_instances,
            'purchase_details'=>$purchases_details
        ]);
        
    }

    /**
     * Show the form for editing the specified purchase instance.
     * @method GET
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $products = array();
        $warehouse = array();
        $global_marketplace = session('MARKETPLACE_ID'); 
        if($global_marketplace != ''){
            $warehouse = Warehouse::where(array('user_marketplace_id'=>$global_marketplace))->get();
            $products = Daily_logic_Calculations::with(['mws_product'])->where(array('user_marketplace_id'=>$global_marketplace))->get();
        }

        $purchases = Purchase_instances::findOrFail($id);
        $purchases_details = Purchase_instance_details::where(array('purchesinstances_id'=>$id))->get()->toArray();
       // echo "<pre>"; print_r($purchases_details); exit;
        foreach ($purchases_details as $key => $value) {
                        
        $projected_units = Daily_logic_Calculations::with(['mws_product'])->where(array('prod_id'=>$value['product_id']))->get()->toArray();
        
        
    }
    //echo "<pre>";print_r($value['product_id']); exit;
    $session_projected_units =Temp_session::where(array('product_id'=>$value['product_id']))->get()->toArray();

     
///echo "<pre>"; print_r($session_projected_units); exit;   

        return view('purchaseinstances.editview', [
                'purchase' => $purchases,
                'purchase_details'=>$purchases_details,
                'warehouse'=>$warehouse,
                'products'=>$products,
                'id'=>$id,
                'projected_units_detais'=>$projected_units,
                'session_projected_units'=>$session_projected_units

            ]
        );
    }

    /**
     * Update the specified purchase instances.
     *
     * @param  \Illuminate\Http\Request  $request
     * @method PUT
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = $request->all();
        $global_marketplace = session('MARKETPLACE_ID');
        $product_instance = [
            'user_marketplace_id'=>$global_marketplace,
        ];
        $instance = Purchase_instances::findOrFail($id);
        $instance->update($product_instance);

        $puchase_data= [
            'purchesinstances_id' =>$id,
            'product_id' => $inputs['product_id'],
            'supplier' => $inputs['suppliers_select_id'],
            'vendor' => $inputs['vendors_select_id'],
            'warehouse' => $inputs['warehouse_select_id'],
            'projected_units' => $inputs['projected_units'],
            'items_per_cartoons' =>$inputs['items_per_cartoons'],
            'containers' =>$inputs['containers'],
            'cbm' =>$inputs['cbm'],
            'subtotal' =>$inputs['subtotal'],
        ];

        $purchase_datas = Purchase_instance_details::where(array('purchesinstances_id' => $id))->get()->first();
        if(!empty($purchase_datas)){
            $purchase_datas->update($puchase_data);
        }else{
            $purchase_datas = Purchase_instance_details::create($puchase_data);
        }

        if($purchase_datas){
            $message = get_messages('Purchase instance upadted successfully', 1);
            Session::flash('message', $message);
            return redirect()->route('purchaseinstances.index');
        }else{
            $message = get_messages('Failed to update purchase instance', 0);
            Session::flash('message', $message);
            return redirect()->route('purchaseinstances.index');
        }
    }

    /**
     * Remove the specified purchase instance.
     * @method POST
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $purchase_instances = Purchase_instances::findOrFail($id);
      $purchase_instances->delete();

        $purchase_instance_details = Purchase_instance_details::where('purchesinstances_id', $id);
     
        $purchase_instance_details->delete();
        return json_encode(array('statusCode'=>200));
    }

    /**
     * get & check track id for purchase instance.
     * @method GET
     * @param  no-params
     */

    public function gerate_track_id(){
        $generate_trake = rand(100,999);
        $track_id = 'PI'.$generate_trake;
        if(!empty($track_id)){
            $check_tranck_number = Purchase_instances::where(array('track_id'=>$track_id))->get()->toArray();
            if(!empty($check_tranck_number)){
                return $this->gerate_track_id();
            }else{
                return $track_id;
            }
        }
    }

    /**
     * create a new purchase instance form open using these method
     * @method POST
     * @param  \Illuminate\Http\Response $request
     * @return \Illuminate\Http\Response
     */

    public function createMore(Request $request){
        $id = $request->id;
        $products = array();
        $warehouse = array();
        $global_marketplace = session('MARKETPLACE_ID');
        if($global_marketplace != ''){
            $warehouse = Warehouse::where(array('user_marketplace_id'=>$global_marketplace))->get();
            $products = Daily_logic_Calculations::with(['mws_product'])->where(array('user_marketplace_id'=>$global_marketplace))->get();
        }
        return view('purchaseinstances.create',['purchase_instance_id'=>$id,'warehouse'=>$warehouse,'products'=>$products]);
    }
}

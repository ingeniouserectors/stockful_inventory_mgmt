<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Fba_default_setting;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Models\Member_fba_setting;
use Illuminate\Support\Facades\Gate;
use App\Models\Application_modules;
use App\Models\Role_access_modules;
use App\Models\Users;


class FbasettingController extends Controller
{
    Protected $userRolePermissionId;

    function __construct()
    {
        $this->middleware(function ($request, $next) {

            if (!Auth::user()) {
                return redirect('login');
            }
            $moduleId = Application_modules::where('module', 'fba_setting_module')->first()->id;
            $userRoleID = Users::with('user_roles')->whereIn('id', [Auth::id()])->first()->user_roles[0]->id;
            $this->userRolePermissionId = Role_access_modules::where('role_id', $userRoleID)->where('application_module_id', $moduleId)->first();
            return $next($request);
        });
    }
    /**
     * Display a listing of the fba settings.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get_checks = get_access('fba_setting_module','view');
        $get_user_access = get_user_check_access('fba_setting_module','view');
        if ($get_checks == 1) {
            if($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }

        $global_marketplace = session('MARKETPLACE_ID');
        $marketplaces = array();
        $total_marketplace = array();
        $marketplaces = '';

        $userRoleId = get_user_role(Auth::user()->id);
        if($userRoleId['user_assigned_role']['user_role_id'] != 1 && $userRoleId['user_assigned_role']['user_role_id'] != 2 && $userRoleId['user_assigned_role']['user_role_id'] != 3 && $userRoleId['user_assigned_role']['user_role_id'] != 5){
            $message = get_messages("Don't have Access Rights for this Functionality", 0);
            Session::flash('message', $message);
            return redirect('/index');
        }
        if($userRoleId['user_assigned_role']['user_role_id'] == 3 || $userRoleId['user_assigned_role']['user_role_id'] == 5){
            if($global_marketplace != ''){
                $marketplace = $global_marketplace;
                $fba_setting = Member_fba_setting::where(array('user_marketplace_id'=>$marketplace))->first();
            }
        }
        if(empty($fba_setting)){
            $fba_setting = Fba_default_setting::first();
        }
        return view('fba.setting.settings',[
            'fba_setting' => $fba_setting,
            'marketplaces' => $global_marketplace,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Create FBA setting, if record is already in our record then update it.
     *
     * @method POST
     * @param  $request->allformdata
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userRoleId = get_user_role(Auth::user()->id);
        if($userRoleId['user_assigned_role']['user_role_id'] != 1 && $userRoleId['user_assigned_role']['user_role_id'] != 2 && $userRoleId['user_assigned_role']['user_role_id'] != 3 && $userRoleId['user_assigned_role']['user_role_id'] != 5 ) {
            $message = get_messages("Don't have Access Rights for this Functionality", 0);
            Session::flash('message', $message);
            return redirect('/index');
        }

        if($userRoleId['user_assigned_role']['user_role_id'] == 1 || $userRoleId['user_assigned_role']['user_role_id'] == 2){
            $userData=[
                'supply_days'=>$request['supply_days'],
                'ship_boxes'=>@$request['ship_boxes'] != '' ? @$request['ship_boxes'] : 0 ,
                'ship_pallets'=>@$request['ship_pallets'] != '' ? @$request['ship_pallets'] : 0 ,
            ];

            $fba_setting = Fba_default_setting::first();
            if (!empty( $fba_setting)) {
                $fba_setting->update($userData);
                $message = get_messages('FBA setting updated successfully', 1);
                Session::flash('message', $message);
                return redirect()->back();
            }
            else
            {
                Fba_default_setting::create($userData);
                $message = get_messages('FBA setting created successfully', 1);
                Session::flash('message', $message);
                return redirect()->back();
            }
        }if($userRoleId['user_assigned_role']['user_role_id'] == 3 || $userRoleId['user_assigned_role']['user_role_id'] == 5){
            $userData=[
                'user_marketplace_id' =>$request['marketplace_id'],
                'supply_days'=>$request['supply_days'],
                'ship_boxes'=>@$request['ship_boxes'] != '' ? @$request['ship_boxes'] : 0 ,
                'ship_pallets'=>@$request['ship_pallets'] != '' ? @$request['ship_pallets'] : 0 ,
            ];
            $getdata = Member_fba_setting::where(array('user_marketplace_id' =>$request['marketplace_id']))->first();
            if(!empty($getdata)){
                $getdata->update($userData);
                $message = get_messages('FBA setting updated successfully', 1);
                Session::flash('message', $message);
                return redirect()->back();
            }else {
                Member_fba_setting::create($userData);
                $message = get_messages('FBA setting created successfully', 1);
                Session::flash('message', $message);
                return redirect()->back();
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

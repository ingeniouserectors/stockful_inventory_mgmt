<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Models\Mws_product;
Use App\Models\Inbound_shipment_items;
use Response;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Session;

class InboundController extends Controller
{
    /**
     * Display a listing of the inbound shipment with particular marketplace wise.
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get_checks = get_access('inbound_module','view');
        $get_user_access = get_user_check_access('inbound_module','view');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }
        $global_marketplace = session('MARKETPLACE_ID');
           
            if($global_marketplace != ''){
            $marketplace = $global_marketplace;
        }

        return view('inbound.index',[
            'marketplace'=> $marketplace
             
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * There are 2 function added in store method :
     *      @method POST
     *
     *  Request Types:
     *
     * 1. Product List : listing of product with particular marketplace wise
     *      @param  $requesttype, $marketplaceid
     *      @return \Illuminate\Http\Response
     *
     * * 2. Inbound shipment list : list of inbound shipment with product wise
     *      @param  $requesttype, $product_id
     *      @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        if($request->request_type == 'product_list'){
            $inputs = $request->all();
            $datas = array();
            $action='';
        $product_data = mws_product::where(array('user_marketplace_id'=>$request->id))->get()->ToArray();
            if(!empty($product_data))
            {
                foreach ($product_data as $key => $post)
                {
                    
                    $nestedData[''] = '<a href="javascript:void(0);" id="row_'.$post["id"].'" data-val="'.$post["id"].'" class="view_media view_medias_'.$post["id"].'btn btn-sm btn-icon btn-circle btn-default"><i class="success-icon" ></i></a>';
                    $nestedData['prod_image'] = '<img width="100px" src="'.$post['prod_image'].'" title="'.$post['prod_name'].'">';
                    $nestedData['prod_name'] = '<span title="'.$post['prod_name'].'">'.substr(strip_tags($post['prod_name']),0,25)."...".'</span>';
                    $nestedData['your_price'] =  $post['your_price'];
                    $nestedData['sku'] =  $post['sku'];
                    $nestedData['asin'] =  '<a href=" https://www.amazon.com/gp/product/'.$post['asin'].'" target="_blank" >'.$post['asin'].'</a>';
                    
                    $datas[] = $nestedData;
                }
            }
            echo json_encode($datas);
            exit;
        }
        else if( $request->request_type == 'inbound_shipment_list'){    

            $inbound_shipment_data = Inbound_shipment_items::with(['inbound_shipment'])->where(array('product_id' =>$request->prod_id))->get()->toArray();
            $view = View::make('inbound.sub-product-list', [
               'inbound_shipment_data'=>$inbound_shipment_data 
                            ]);
            $html = $view->render();
            $res['error'] = 0;
            $res['view'] = $html;
            return response()->json($res);
        
        }   
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Application_modules;
use App\Models\Field_list;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Session;


class ModuletableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(isset($request->insert_type) && $request->insert_type == 'get_table_fileds'){
            $columns = Schema::getColumnListing(trim($request->table));
            $option = '<option value="">Select fields</option>';
            if(!empty($columns)){
                foreach ($columns as $col){
                    if($col != 'id' && $col != 'user_marketplace_id' && $col != 'created_at' && $col != 'updated_at' && $col != 'deleted_at'){
                        $option .= '<option value="'.$col.'" >'.$col.'</option>';
                    }
                }
            }
            return response()->json($option);

        }else if(isset($request->insert_type) && $request->insert_type == 'fields_management'){
            $table_class = $request->table_class;
            $display_name = $request->display_name;
            $display_fields = $request->display_fields;
            $join_with_fields = $request->join_with_fields;

            $module_id = $request->module_id;

            if(!empty($table_class)){
                Field_list::where(array('module_id'=>$module_id,'join_type'=>'2'))->delete();
                foreach($table_class as $key => $value){
                    if($value != '' && $display_fields[$key] != '' && $display_name[$key] != '' && $join_with_fields[$key] != ''){
                        $data_req = [
                            'module_id' => $module_id,
                            'table_name' => $value,
                            'display_field' =>$display_fields[$key],
                            'display_name' => $display_name[$key],
                            'join_with' => $join_with_fields[$key],
                            'join_type' => '2',
                        ];
                        Field_list::create($data_req);
                    }
                }
                $message = get_messages("Fileds updated in list.", 1);
                Session::flash('message', $message);
            }
            return redirect('moduletable/'.$module_id);

        }else if(isset($request->insert_type) && $request->insert_type == 'remove_joining_update'){
            $remove_id = $request->data_id;
            Field_list::where(array('id'=>$remove_id))->delete();
            $res['success'] = 1;
            return response()->json($res);
            //dd($request->all());
        }else{
            $module_id = $request->module_id;
            $original_name = $request->original_name;
            $display_name = $request->display_name;
            $foreign_key = $request->foreign_key;
            $ref_table_name = $request->ref_table_name;
            $ref_table_field = $request->ref_table_field;

            if(!empty($original_name)){
                foreach($original_name as $key=>$value){
                    $check_already = Field_list::where(array('module_id'=>$module_id,'original_name'=>$value))->first();
                    $req_data = [
                        'module_id' =>$module_id,
                        'original_name' => $value,
                        'display_name' => $display_name[$key],
                        'field_type' => $foreign_key[$key],
                        'active' => '1',
                        'referance_table' => $ref_table_name[$key],
                        'referance_table_field' => $ref_table_field[$key],
                    ];
                    if(empty($check_already)){
                        Field_list::create($req_data);
                        $message = get_messages("Fileds added in list.", 1);
                        Session::flash('message', $message);
                    }else{
                        Field_list::where(array('id'=>$check_already->id))->update($req_data);
                        $message = get_messages("Fileds updated in list.", 1);
                        Session::flash('message', $message);
                    }
                }
            }
            return redirect('moduletable/'.$module_id);
        }

       // return redirect()->route('moduletable.show',$module_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //dd('1dfhdg');
        //echo '<pre>';
        $get_all_table = array();
        $columns = array();
        $table_name = '';
        $get_table_name = Application_modules::where(array('id'=>$id))->first();
        if(!empty($get_table_name)){
            $modulename = explode('_',$get_table_name->table_name);
            $tableNames = Schema::getConnection()->getDoctrineSchemaManager()->listTableNames();
            foreach($tableNames as $table){
                $tablename = explode('_',$table);
                $match_case =  similar_text(trim($modulename[0]),trim($tablename[0]));
                if($match_case > 5){
                    $get_all_table[] = $table;
                }
            }
            $columns = Schema::getColumnListing(trim($get_table_name->table_name));
            $table_name = ucfirst(trim($get_table_name->table_name));
        }
        $get_joins_table = Field_list::where(array('module_id'=>$id,'join_type'=>'2'))->get()->toArray();
        return view('roles.tabledata-list', [
                'table_column' => $columns,
                'table_name' => $table_name,
                'module_id' => $id,
                'match_table' => $get_all_table,
                'get_joins_table' =>$get_joins_table,
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        dd($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

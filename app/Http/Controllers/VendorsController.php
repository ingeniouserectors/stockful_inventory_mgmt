<?php

namespace App\Http\Controllers;

use App\Models\Lead_time;
use App\Models\Lead_time_check;
use App\Models\Lead_time_type;
use App\Models\Lead_time_value;
use App\Models\Order_volume_type;
use App\Models\Payment;
use App\Models\Payment_details;
use App\Models\Payment_terms_type;
use App\Models\Reorder_schedule_detail;
use App\Models\Reorder_schedule_type;
use App\Models\Shipping;
use App\Models\ShippingAgent;
use App\Models\Shippingair;
use App\Models\Shippingcarrier;
use App\Models\ShippingContainerSettings;
use App\Models\Shippingcutoffdeliverytime;
use App\Models\ShippingDeliveryDetails;
use App\Models\ShipToWarehouse;
use App\Models\Supplier;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use Illuminate\Support\Facades\Session;
use App\Models\Vendor;
use App\Models\Vendor_contact;
use App\Models\Vendor_default_setting;
use App\Models\Vendor_wise_default_setting;
use App\Models\Vendor_blackout_date;
use App\Models\Country;
use App\Models\Field_list;
use App\Models\User_field_access;
use Validator;
use App\Imports\VendorImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Export\VendorExport;


class VendorsController extends Controller
{
    /**
     * Display a listing view of the vendors with marketplace wise
     *
     * @param no -params
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get_checks = get_access('vendor_module', 'view');
        $get_user_access = get_user_check_access('vendor_module', 'view');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }

        $get_all_fileds = array();
        $get_module_id = get_module_id('vendor_module');
        if ($get_module_id > 0) {
            $get_all_fileds = Field_list::where(array('active' => '1', ['display_name', '!=', NULL], 'module_id' => $get_module_id))->orderBy('id', 'asc')->get()->toArray();
        }

        $global_marketplace = session('MARKETPLACE_ID');
        $marketplace = '';
        $data = array();
        if ($global_marketplace != '') {
            $marketplace = $global_marketplace;
        }

        return view('vendorsfinal.index', [
            'marketplace' => $marketplace,
            'vendors' => $data,
            'get_all_fields' => $get_all_fileds,
            'cols' => DB::table('cols')->where('user_id', auth()->id())
                ->where('table_name', 'vendors')->pluck('cols')->first()
        ]);

    }


    /**
     * Show the form for creating a new vendor
     *
     * @param no -params
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $get_checks = get_access('vendor_module', 'create');
        $get_user_access = get_user_check_access('vendor_module', 'create');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }
        $global_marketplace = session('MARKETPLACE_ID');
        $warehouse = Warehouse::where(array('user_marketplace_id' => $global_marketplace))->get()->toArray();
        $country = get_country_list();
        $cid = null;
        $lead_time_type = Lead_time_type::get()->toArray();
        $lead_time_check = Lead_time_check::get()->toArray();
        $order_volume_type = Order_volume_type::get()->toArray();
        $reorder_schedule_type = Reorder_schedule_type::get()->toArray();
        $payment_term = Payment_terms_type::get()->toArray();
        $state_list = get_state_list($cid = 'US');
        $shippingAgents = ShippingAgent::where(array('user_marketplace_id' => $global_marketplace))->get()->toArray();
        $shippingCarrier = Shippingcarrier::where(array('user_marketplace_id' => $global_marketplace))->get()->toArray();
//        Return view('suppliersnew.create', array('country_list' => $country, 'warehouse' => $warehouse));
        return view('vendorsfinal.create', array(
            'shipping_carrierss' => $shippingCarrier,
            'shipping_agents' => $shippingAgents,
            'country_list' => $country,
            'warehouse' => $warehouse,
            'state_list' => $state_list,
            'payment_term' => $payment_term,
            'lead_time_type' => $lead_time_type,
            'lead_time_check' => $lead_time_check,
            'order_volume_type' => $order_volume_type,
            'reorder_schedule_type' => $reorder_schedule_type
        ));

//        $get_checks = get_access('vendor_module', 'create');
//        $get_user_access = get_user_check_access('vendor_module', 'create');
//
//        if ($get_checks == 1) {
//            if ($get_user_access == 0) {
//                $message = get_messages("Don't have Access Rights for this Functionality", 0);
//                Session::flash('message', $message);
//                return redirect('/index');
//            }
//        } else {
//            if ($get_user_access == 0) {
//                $message = get_messages("Don't have Access Rights for this Functionality", 0);
//                Session::flash('message', $message);
//                return redirect('/index');
//            }
//        }
//        $global_marketplace = session('MARKETPLACE_ID');
//        $warehouse = Warehouse::where(array('user_marketplace_id' => $global_marketplace))->get()->toArray();
//        $country = get_country_list();
//        Return view('vendorsfinal.create', array('country_list' => $country, 'warehouse' => $warehouse));
    }

    /**
     * Show the all state with country wise.
     * @method GET
     * @param  $country_id
     * @return \Illuminate\Http\Response
     */

    public function getStateList(Request $request)
    {
        $inputs = $request->all();
        $list = get_state_list($inputs['country']);
        return response()->json($list);
    }

    /**
     * Show the all city with state wise.
     * @method GET
     * @param state_id
     * @return \Illuminate\Http\Response
     */

    public function getCityList(Request $request)
    {
        $inputs = $request->all();
        $list = get_city_list($inputs['state']);
        return response()->json($list);
    }

    /**
     * There are 3 function added in store method :
     * @method POST
     *
     *  Request Types:
     *
     * 1. get_all_vendors_data : Get all vendors data
     * @param  $requesttype
     * @param  $requesttype
     * @param  $requesttype , $request
     * @param  $requesttype , $request
     * @return \Illuminate\Http\Response
     *
     * 2. Get_all_state: Get all state on country wise
     * @return \Illuminate\Http\Response
     *
     * 3. Get_all_city: Get all city on state wise
     * @return \Illuminate\Http\Response
     *
     * 4. insert_update_basic_details: insert or update basic details
     * @param  $requesttype , $request
     * @return \Illuminate\Http\Response
     *
     * 5. insert_contacts: insert or update contact details
     * @param  $requesttype , $request
     * @return \Illuminate\Http\Response
     *
     * 6. insert_update_setting: insert or update order setting
     * @param  $requesttype , $request
     * @return \Illuminate\Http\Response
     *
     * 7. delete_vendors_contact: delete vendors contact
     * @param  $requesttype , $request
     * @return \Illuminate\Http\Response
     *
     * 8. delete_blackout_date: delete blackout date
     * @param  $requesttype , $request
     * @return \Illuminate\Http\Response
     *
     * 9. insert_update_payment: insert or update payment details
     * @param  $requesttype , $request
     * @return \Illuminate\Http\Response
     *
     * 10. remove_vendors_blackout_date: delete blackout date
     * @param  $requesttype , $request
     * @return \Illuminate\Http\Response
     *
     * 11. delete_vendors_contact: delete vendors contacts
     * @param  $requesttype , $request
     * @return \Illuminate\Http\Response
     *
     * 12. insert_update_backout_date: insert or update blackout date
     * @param  $requesttype , $request
     * @return \Illuminate\Http\Response
     *
     * 13. vendors_list: listing of all vendors
     * @param  $requesttype , $request
     * @return \Illuminate\Http\Response
     *
     * 14. get_pagi_country: get pagination listing
     * @param  $requesttype , $request
     * @return \Illuminate\Http\Response
     *
     * 15. delete_multiple_records: delete multiple records
     * @param  $requesttype , $request
     * @return \Illuminate\Http\Response
     *
     * 16. create_shipping_agent: create shipping agent
     * @param  $requesttype , $request
     * @return \Illuminate\Http\Response
     *
     * 17. create_shipping_carrier: create shipping carrier
     * @param  $requesttype , $request
     * @return \Illuminate\Http\Response
     *
     * 18. insert_update_shipping_details: insert & update shipping details
     * @param  $requesttype , $request
     * @return \Illuminate\Http\Response
     *
     * 19. insert_update_lead_time: insert or update lead time
     * @param  $requesttype , $request
     * @return \Illuminate\Http\Response
     *
     * 20. insert_update_order_setting: insert or update order settings
     * @param  $requesttype , $request
     * @return \Illuminate\Http\Response
     *
     */

    public function store(Request $request)
    {
        if ($request->request_type == 'get_all_vendors_data') {
            //ini_set('memory_limit', '-1');
            $vendor = env('VENDOR');
            $offsetValue = $request->offsetValue;
            if (!empty($offsetValue)) {
                $offsetValue = explode('-', $offsetValue);
                $offset = $offsetValue[0];
                $limit = $request->pagination_slot;
                $output = '';
                if ($offset % 2 == 0) {
                    $offset = 0;
                } else {
                    $offset = $offset - 1;
                }
            } else {
                $offset = 0;
                $limit = $request->pagination_slot;
            }

            
            $global_marketplace = session('MARKETPLACE_ID');

                $no_display_fileds = User_field_access::
                join('field_list','field_list.id','=','field_list_id')
                ->select(\DB::raw("GROUP_CONCAT((CASE WHEN original_name IS NULL THEN CONCAT(table_name,'.',display_field) ELSE original_name END)) AS disply_fileds, GROUP_CONCAT((CASE WHEN original_name IS NULL THEN display_field ELSE original_name END)) AS display_all"))
                ->where(array('marketplace_id'=>$global_marketplace,'type'=>1))
              ->first();


            //if(!empty())

            $unchecked_arr = explode(',', $no_display_fileds->disply_fileds);
            $total_display_fileds = explode(',',$no_display_fileds->display_all);

            $selected_fields = array();
            $join_table = array();
            $join_first = array();
            $join_last = array();

            $get_module_id = get_module_id('vendor_module');
            if ($get_module_id > 0) {
                $get_all_fileds = Field_list::where(array('active' => '1', ['display_name', '!=', NULL], 'module_id' => $get_module_id))->orderBy('id', 'asc')->get()->toArray();

                if (!empty($get_all_fileds)) {
                    $selected_fields[] = 'vendors.*';
                    //$get_fields[] = 'id';
                    foreach ($get_all_fileds as $key => $val) {
                        $data_of = $val['original_name'] != '' ? $val['original_name'] : $val['display_field'];
                        $display_name[$data_of] = $val['original_name'] != '' ? $val['original_name'] : $val['display_field'];
                        if ($val['referance_table'] != '' && $val['referance_table_field'] != '') {
                            $selected_fields[] = $val['referance_table'] . '.' . $val['referance_table_field'];
                            //$get_fileds[] = $val['referance_table_field'];
                            $join_table[] = $val['referance_table'];
                            $join_first[] = 'vendors.' . $val['original_name'];
                            $join_last[] = $val['referance_table'] . "." . $val['field_type'];
                        } else if ($val['table_name'] != '' && $val['display_field'] != '' && $val['join_with'] != '') {
                            $selected_fields[] = $val['table_name'] . '.' . $val['display_field'];
                            //$get_fields[] = $val['display_field'];
                            $join_table[] = $val['table_name'];
                            $join_first[] = 'vendors.id';
                            $join_last[] = $val['table_name'] . "." . $val['join_with'];
                        } else {
                            $selected_fields[] = $val['original_name'];
                            //$get_fields[] = $val['original_name'];
                        }
                    }
                }

            $whereArray = [
                "user_marketplace_id" => $global_marketplace
            ];
            if ($request->get_country) {
                $whereArray = [
                    "user_marketplace_id" => $global_marketplace,
                    "country_id" => $request->get_country
                ];
            }

            if($request->search != ''){
                $whereArray[] = [
                    'vendor_name', 'LIKE', "%{$request->search}%"
                ];
            }


            $join_table = array_unique($join_table);
            $selected_raw = array_diff($selected_fields,$unchecked_arr);

            $selected_field = "'" . implode("', '", $selected_fields) . "'";

           

            $userRole =Vendor::with(['vendor_contact' => function ($query) {
                $query->orderBy('primary', 'desc');
            }, 'vendor_product'])->select($selected_raw)->where($whereArray);
            if (!empty($join_table)) {
                foreach ($join_table as $key => $jon) {
                    $userRole->leftjoin("$jon", "$join_first[$key]", '=', "$join_last[$key]");
                }
            }
            if($offset > 0)
                $listing_data = $userRole->offset($offset)->limit($limit)->groupBy('id')->orderBy('id', 'desc')->get()->toArray();
            else
                $listing_data = $userRole->limit($limit)->groupBy('id')->orderBy('id', 'desc')->get()->toArray();



            $listing_data_count = Vendor::with(['vendor_contact', 'vendor_product'])->where($whereArray)->get()->count();


        }



            // if($offset > 0) {
            //     $listing_data = Vendor::with(['vendor_contact' => function ($query) {
            //         $query->orderBy('primary', 'desc');
            //     }, 'vendor_product'])->where($whereArray)->offset($offset)
            //         ->limit($limit)->orderBy('id','desc')->get()->toArray();
            // }else {
            //     $listing_data = Vendor::with(['vendor_contact' => function ($query) {
            //         $query->orderBy('primary', 'desc');
            //     }, 'vendor_product'])->where($whereArray)
            //         ->limit($limit)->orderBy('id','desc')->get()->toArray();
            // }



            //$listing_data_count =
            //$listing_data_count = Vendor::with(['vendor_contact', 'vendor_product'])->where($whereArray)->get()->count();
          
            $total_count = 0;
            if (!empty($listing_data)) {
                $output = '';
             
                foreach ($listing_data as $index => $data) {
                    $vendors_contact = Vendor_contact::where(array('vendor_id' => $data['id']))->first();
                    $vendors_blackoutdate = Vendor_blackout_date::where(array('vendor_id' => $data['id']))->first();
                    $lead_time = Lead_time::where(array('supplier_vendor' => $vendor, 'supplier_vendor_id' => $data['id']))->first();
                    $order_setting = Reorder_schedule_detail::where(array('supplier_vendor' => $vendor, 'supplier_vendor_id' => $data['id']))->first();
                    $payment = Payment::where(array('supplier_vendor' => $vendor, 'supplier_vendor_id' => $data['id']))->first();
                    $shipping = Shipping::where(array('supplier_vendor' => $vendor, 'supplier_vendor_id' => $data['id']))->first();

                    if (empty($vendors_contact) || empty($vendors_blackoutdate) || empty($lead_time) || empty($order_setting) || empty($payment) || empty($shipping)) {
                        $check_return = 0;
                    } else {
                        $check_return = 1;
                    }
                    if ($request->toggle_b == 1) {
                        if ($check_return == 0) {
                            $i = 1;
                            $counter = $index + 1;
                            $vendors_name = ( @$data['vendor_contact'][0]['first_name'] ? @$data['vendor_contact'][0]['first_name'] : "" . " " . @$data['vendor_contact'][0]['last_name'] ) ? @$data['vendor_contact'][0]['last_name'] : "";
                            $output .=
                                '<tr id="' . $data['id'] . '">
                                    <td class="p-2">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input row-checkbox" name="delete_all" data-val="' . $data['id'] . '" value="' . $data['id'] . '" id="customCheck' . $counter . '">
                                            <label class="custom-control-label" for="customCheck' . $counter . '">&nbsp;</label>
                                        </div>
                                    </td>

                                    <td class="p-2">';
                            if ($check_return == 0) {
                                $output .= '<i class="fas fa-exclamation-triangle" style="color:#F1B44C;" data-toggle="tooltip" title="Missing required settings"></i>';
                            }
                            $output .= '</td>';
                            $newtext = 0;
                           
                            $count = count($display_name);
                           
                            foreach ($display_name as $key => $dispay) {
                                if(!in_array($dispay,$total_display_fileds)){
                                $t = $i + $newtext;
                                $newtext++;
                                if ($dispay == 'country_id') {
                                    $dats = get_country_fullname($data[$key]);
                                }else if($dispay == 'state'){
                                    //$dats = get_state_fullname($data['country_id'], $data[$key]);
                                    $dats = $data[$key];
                                } else if ($dispay == 'first_name') {
                                    $options = '';
                                    if (!empty($data['vendor_contact'])) {
                                             $options = '<select name="vendor_contact_' . $data['id'] . '" id="vendor_contact_' . $data['id'] . '" class="form-control border-0 pl-0 bg-transparent" onchange="changeContact(' . $data['id'] . ')">';
                                             foreach ($data['vendor_contact'] as $contactDetail) {
                                                $options .= '<option value="' . $contactDetail["id"] . '">' . $contactDetail["first_name"] . ' ' . $contactDetail['last_name'] . '</option>';
                                             }
                                             $options .= '</select>';
                                          }
                                      $dats = $options;

                                }else if($dispay == 'email'){
                                        $dats = @$data['vendor_contact'][0]['email'] ? @$data['vendor_contact'][0]['email'] : '';
                                } else if(@$dispay == 'phone_number'){
                                        $dats = @$data['vendor_contact'][0]['phone_number'] ? @$data['vendor_contact'][0]['phone_number'] : '';
                                } else if ($dispay == 'product_id') {
                                    $dats = get_vendors_total_product($data['id']);
                                 }else {
                                    $dats = $data[$key];
                                }
                               
                                $output .= '<td class="col' . $t . '">' . ucfirst(strtolower($dats)) . '</td>';
                            }
                            }
                            $output .= '<td>
                                   <a href="' . url('/vendors/' . $data['id'] . '/edit') . '" class="mr-3 text-secondary row-hover-action" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="mdi mdi-square-edit-outline font-size-18"></i></a>
                                    <a href="javascript:void(0);" class="text-secondary row-hover-action delete_single" data-id="' . $data['id'] . '" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="mdi mdi-trash-can font-size-18"></i></a>
                                </td>';
                            $output .= '</tr>';
                            $total_count++;
                        }
                    } else {

                        $i = 1;
                        $counter = $index + 1;
                        $output .=
                            '<tr id="' . $data['id'] . '">
                                <td class="p-2">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input row-checkbox" name="delete_all" data-val="' . $data['id'] . '" value="' . $data['id'] . '" id="customCheck' . $counter . '">
                                        <label class="custom-control-label" for="customCheck' . $counter . '">&nbsp;</label>
                                    </div>
                                </td>

                                <td class="p-2">';
                        if ($check_return == 0) {
                            $output .= '<i class="fas fa-exclamation-triangle" style="color:#F1B44C;" data-toggle="tooltip" title="Missing required settings"></i>';
                        }
                         $output .= '</td>';
                            $newtext = 0;
                            
                            $count = count($display_name);
                           
                            foreach ($display_name as $key => $dispay) {
                                if(!in_array($dispay,$total_display_fileds)){
                                $t = $i + $newtext;
                                $newtext++;
                                if ($dispay == 'country_id') {
                                    $dats = get_country_fullname($data[$key]);
                                }else if($dispay == 'state'){
                                    $dats = $data[$key];
                                    // $state_name = get_state_fullname($data['country_id'] , $data['state']);
                                    // $dats = $state_name;
                                    // if($data[$key] != '' && $data['country_id'] != ''){
                                    //     $dats = get_state_fullname($data['country_id'], $data[$key]);
                                    // }
                                } else if ($dispay == 'first_name') {
                                    $options = '';
                                     if (!empty($data['vendor_contact'])) {
                                             $options = '<select name="vendor_contact_' . $data['id'] . '" id="vendor_contact_' . $data['id'] . '" class="form-control border-0 pl-0 bg-transparent" onchange="changeContact(' . $data['id'] . ')">';
                                             foreach ($data['vendor_contact'] as $contactDetail) {
                                                $options .= '<option value="' . $contactDetail["id"] . '">' . $contactDetail["first_name"] . ' ' . $contactDetail['last_name'] . '</option>';
                                             }
                                             $options .= '</select>';
                                          }
                                      $dats = $options;

                                }else if($dispay == 'email'){
                                        $dats = @$data['vendor_contact'][0]['email'] ? @$data['vendor_contact'][0]['email'] : '';
                                } else if(@$dispay == 'phone_number'){
                                        $dats = @$data['vendor_contact'][0]['phone_number'] ? @$data['vendor_contact'][0]['phone_number'] : '';
                                } else if ($dispay == 'product_id') {
                                    $dats = get_vendors_total_product($data['id']);
                                 }else {
                                    $dats = $data[$key];
                                }
                                // echo '<pre>';
                                // print_r($data[$key]);
                                $output .= '<td class="col' . $t . '">' . ucfirst(strtolower($dats)) . '</td>';
                            }
                            }
                            $output .= '<td>
                                   <a href="' . url('/vendors/' . $data['id'] . '/edit') . '" class="mr-3 text-secondary row-hover-action" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="mdi mdi-square-edit-outline font-size-18"></i></a>
                                    <a href="javascript:void(0);" class="text-secondary row-hover-action delete_single" data-id="' . $data['id'] . '" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="mdi mdi-trash-can font-size-18"></i></a>
                                </td>';
                            $output .= '</tr>';
                            $total_count++;
                    }
                }
                //exit;
                if ($request->toggle_b == 0) {
                    if ($listing_data_count > $limit) {
                        $output_first = '<input type="hidden" value="0" class="checkdeleteall"><input type="hidden" value="' . $listing_data_count . '" class="total_listing_count"><tr class="selected-row-number" style="display:none;"><td colspan="9" class="text-center">Selected <span id="number-of-selected"></span> out of ' . $listing_data_count . '. <a class="text-primary select-all" id="select-all-link" style="cursor: pointer;">Select All</a></td></tr>';
                    } else {
                        $output_first = '<input type="hidden" value="0" class="checkdeleteall"><input type="hidden" value="' . $listing_data_count . '" class="total_listing_count"><tr class="selected-row-number" style="display:none;"><td colspan="9" class="text-center">Selected <span id="number-of-selected"></span> out of ' . $listing_data_count . '. </td></tr>';
                    }
                } else {
                    if ($total_count > $limit) {
                        $output_first = '<input type="hidden" value="0" class="checkdeleteall"><input type="hidden" value="' . $total_count . '" class="total_listing_count"><tr class="selected-row-number" style="display:none;"><td colspan="9" class="text-center">Selected <span id="number-of-selected"></span> out of ' . $total_count . '. <a class="text-primary select-all" id="select-all-link" style="cursor: pointer;">Select All</a></td></tr>';
                    } else {
                        $output_first = '<input type="hidden" value="0" class="checkdeleteall"><input type="hidden" value="' . $total_count . '" class="total_listing_count"><tr class="selected-row-number" style="display:none;"><td colspan="9" class="text-center">Selected <span id="number-of-selected"></span> out of ' . $total_count . '. </td></tr>';
                    }
                }
                $output_final = $output_first . $output;
            } else {
                $output_final = '<td align="center" colspan="10" class="alert alert-danger">No record found. </td>';
            }

            $res['output'] = $output_final;
            $res['total_count'] = $total_count;
            return response()->json($res);

        }
        if($request->request_type == 'fileds_manage'){
            $global_marketplace = session('MARKETPLACE_ID');
            $total_unchecked_fileds = $request->checked_arr;
            User_field_access::where(array('marketplace_id'=>$global_marketplace,'type'=>1))->delete();
            if(!empty($total_unchecked_fileds)){
                foreach($total_unchecked_fileds as $val){
                    $req_data = [
                        'field_list_id'=>$val,
                        'marketplace_id'=>$global_marketplace,
                        'type'=>1
                    ];
                    User_field_access::create($req_data);
                }
            }
            $res['success'] = 1;
            return response()->json($res);
        }

        if ($request->request_type == 'Get_all_state') {
            $inputs = $request->all();
            $list = get_state_list($inputs['country']);
            return response()->json($list);
            exit;
        }
        if ($request->request_type == 'Get_all_city') {
            $inputs = $request->all();
            $list = get_city_list($inputs['state']);
            return response()->json($list);
            exit;
        }
        if ($request->insert_type == 'insert_update_basic_details') {
            $validator = Validator::make($request->all(), [
                'vendor_type' => 'required',
                'vendor_name' => 'required',
                'marketplace_id'=>'required',
                'country'=>'required',
                'address_line_1'=>'required',
                'state'=>'required',
                'zipcode'=>'required'
            ]);
            if ($validator->fails()) {
                $res['success'] = false;
                $res['error'] = 1;
                $res['message'] = 'Please provide required field data';
            }
            else {
                $req_data = [
                    'vendor_type' => $request->vendor_type,
                    'vendor_name' => $request->vendor_name,
                    'user_marketplace_id' => $request->marketplace_id,
                    'country_id' => $request->country,
                    'address_line_1' => $request->address_line_1,
                    'address_line_2' => $request->address_line_2,
                    'city' => ($request->city ? $request->city : ''),
                    'state' => $request->state,
                    'zipcode' => $request->zipcode,
                ];
                
                if ($request->vendor_id == '') {
                    $datas_of = Vendor::create($req_data);
                    $userLastInsertId = $datas_of->toArray();
                    if ($userLastInsertId != '') {
                        $res['vendor_id'] = $userLastInsertId['id'];
                    }
                    //$message = get_messages('vendor created successfully', 1);
                    $res['vendor_id'] = $res['vendor_id'];
                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = "vendor created successfully";
                } else {
                    $vendor = Vendor::findOrFail($request->vendor_id);
                    $vendor->update($req_data);
                    //$message = get_messages('vendor updated successfully', 1);
                    $res['vendor_id'] = $request->vendor_id;
                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = "vendor updated successfully";
                    // $res['success'] = true;
                    // $res['message'] = $request->vendor_id;
                }
            }
            return response()->json($res);
        }
        if ($request->insert_type == 'insert_contacts') {
            $res['error'] = 1;
            $res['message'] = '';
            $primary_contact = $request->primary_contacts;
            $res_arr = array();
            $success_count = 0;
            $fail_count = 0;
            $error_msg = array();
            $message = '';
            if(empty($request->vendor_id)){
                $res_arr['success'] = false;
                $res_arr['error'] = 1;
                $res_arr['message'] = 'Please fill up basic details';
                return response()->json($res_arr);
            }
            $res_arr['vendor_id'] = $request->vendor_id;
            $mandatory=0;
            // if(!empty($request->contacts)) {
            //     foreach ($request->contacts as $key => $value) {
            //         if ($value['first_name'] != '' && $value['last_name'] != '' && $value['email'] != '' && $value['phone_number'] ) {
            //         }
            //         else{
            //             $mandatory=1;
            //         }
            //     }
            // }
            // if($mandatory==1){
            //     $res_arr['success'] = false;
            //     $res_arr['error'] = 1;
            //     $res_arr['message'] = 'Please provide required field data';
            //     return response()->json($res_arr);
            // }
            // else {
                if (!empty($request->contacts)) {
                    foreach ($request->contacts as $key => $value) {
                        if ($value['first_name'] != '' & $value['last_name'] != '' && $value['email'] != '' && $value['phone_number']) {

                            if ($primary_contact >= 0) {
                                if ($primary_contact == $key)
                                    $primary = 1;
                                else
                                    $primary = 0;
                            } else {
                                if ($key == 0)
                                    $primary = 1;
                                else
                                    $primary = 0;
                            }

                            $req_data = [
                                'first_name' => $value['first_name'],
                                'last_name' => $value['last_name'],
                                'email' => $value['email'],
                                'vendor_id' => $request->vendor_id,
                                'phone_number' => $value['phone_number'],
                                'title' => $value['title'],
                                'primary' => $primary,
                            ];
                            $vendorContactID = '';
                            $check_email = Vendor_contact::where(array('email' => $value['email'], 'vendor_id' => $request->vendor_id))->first();
                            if (empty($check_email)) {
                                $datas_of = Vendor_contact::create($req_data);
                                $userLastInsertId = $datas_of->toArray();
                                if ($userLastInsertId != '') {
                                    $vendorContactID = $userLastInsertId['id'];
                                }
                                $message = 'vendor contact created successfully';
                                $success_count++;
                            } else {
                                $vendorContact = Vendor_contact::findOrFail($check_email['id']);
                                $vendorContact->update($req_data);
                                $message = 'vendor contact updated successfully';
                                //$res['message'] = $message;
                                $vendorContactID = $check_email['id'];
                                $success_count++;
                            }
                        }
                    }
                    $res_arr['success_count'] = $success_count;
                    $res_arr['failer_count'] = $fail_count;
                    $res_arr['success'] = true;
                    $res_arr['error'] = 0;
                    if ($fail_count == 0) {
                        $res_arr['message'] = $message;
                    } else {
                        $res_arr['message'] = $error_msg;
                    }
                } else {
                    $res_arr['success'] = true;
                    $res_arr['error'] = 0;
                    $res_arr['message'] = '';
                }
            //}
            return response()->json($res_arr);

            // if(!empty($first_name)) {

            //     foreach ($first_name as $key => $val) {
            //         if ($val != '' && $emails[$key] != '') {


            //         //    if(array_search("1",$primary_contact)){
            //         //        $primary = $primary_contact[$key];
            //         //    }else{
            //         //        $check_primary = Vendor_contact::where(array('primary' => 1, 'vendor_id' => $request->vendor_id))->first();
            //         //        if (!empty($check_primary)) {
            //         //            $primary = 0;
            //         //        } else {
            //         //            $primary = 1;
            //         //        }
            //         //    }
            //             if($primary_contact>=0){
            //                 if($primary_contact == $key)
            //                     $primary = 1;
            //                 else
            //                     $primary = 0;
            //             } else{
            //                 if($key == 0)
            //                     $primary = 1;
            //                 else
            //                     $primary = 0;
            //             }
            //             $req_data = [
            //                 'first_name' => $val,
            //                 'last_name' => $last_name[$key],
            //                 'email' => $emails[$key],
            //                 'vendor_id' => $request->vendor_id,
            //                 'phone_number' => $phone_number[$key],
            //                 'title' => $title[$key],
            //                 'primary' => $primary,
            //             ];
            //             $vendorContactID = '';
            //             if ($vendor_contact_id[$key] == '') {
            //                 $check_email = Vendor_contact::where(array('email' => $emails[$key],'vendor_id' => $request->vendor_id))->first();
            //                 if(empty($check_email)){
            //                     $datas_of = Vendor_contact::create($req_data);
            //                     $userLastInsertId = $datas_of->toArray();
            //                     if ($userLastInsertId != '') {
            //                         $vendorContactID = $userLastInsertId['id'];
            //                     }
            //                     // $message = get_messages('vendor contact updated successfully', 1);
            //                     // $res['message'] = $message;
            //                     $success_count ++;
            //                 }else{
            //                     $fail_count ++;
            //                 }
            //             } else {
            //                 $check_email = Vendor_contact::where(array('email' => $emails[$key],'vendor_id' => $request->vendor_id,['id','!=',$vendor_contact_id[$key]]))->first();
            //                 if(empty($check_email)){
            //                     $vendorContact = Vendor_contact::findOrFail($vendor_contact_id[$key]);
            //                     $vendorContact->update($req_data);
            //                     $message = get_messages('vendor contact updated successfully', 1);
            //                     $res['message'] = $message;
            //                     $vendorContactID = $vendor_contact_id[$key];
            //                     $success_count ++;
            //                 }
            //                 else{
            //                     $fail_count ++;
            //                 }
            //             }

            //         }
            //     }
            // }else{
            //     $message = get_messages('vendor contact updated successfully', 1);
            //     $res['message'] = $message;
            // }
            // $res_arr['success_count'] = $success_count;
            // $res_arr['failer_count'] = $fail_count;


            //     $res['error'] = 0;
            //     $total_contact = $check_email = Vendor_contact::where(array('vendor_id' => $request->vendor_id))->get()->toArray();
            //     foreach($total_contact as $value){
            //         $vendorcontactdetails .= '<tr '. (@$value['primary']==1 ? "class=active" : "").'>
            //         <td style="display: none;"><input class="primary_contact" type="hidden" value="'. @$value['primary'] .'">
            //         <input id="vendor_contact_id" type="hidden" value="'. (@$value['id'] ? @$value['id'] : "").'"></td>
            //         <td class="align-middle edit"><input id="first_name" type="text" placeholder="First Name" value="'. @$value['first_name'] .'"><span class="col1" style="display: none;">'.@$value['first_name'].'</span></td>
            //         <td class="align-middle edit"><input id="last_name" type="text" placeholder="Last Name" value="' . @$value['last_name'] . '"><span class="col2" style="display: none;">'.@$value['last_name'].'</span></td>
            //         <td class="align-middle edit"><input id="title" type="text" placeholder="Title" value="' . @$value['title'] . '"><span class="col3" style="display: none;">'.@$value['title'].'</span></td>
            //         <td class="align-middle edit"><input id="emails" type="email" placeholder="Email" value="' . @$value['email'] . '"><span class="col4" style="display: none;">'.@$value['email'].'</span></td>
            //         <td class="align-middle edit"><input id="phone_number" type="tel" placeholder="Phone number" value="' . @$value['phone_number']. '" maxlength="12"><span class="col5" style="display: none;">'.@$value['phone_number'].'</span></td>
            //         <td class="align-middle edit"><i class="mdi mdi-star font-size-18" data-toggle="tooltip" title="" data-original-title="Make Primary Contact"></i></td></tr>';
            //     }
            //     $boat_production_mail_1_contact = '';
            //     $boat_production_mail_2_contact = '';
            //     $boat_port_departure_contact = '';
            //     $boat_in_transist_contact = '';
            //     $boat_to_warehouse_1_contact = '';
            //     $boat_to_warehouse_2_contact = '';
            //     $boat_to_warehouse_3_contact = '';

            //     $plane_production_mail_1_contact = '';
            //     $plane_production_mail_2_contact = '';
            //     $plane_port_departure_contact = '';
            //     $plane_in_transist_contact = '';
            //     $plane_to_warehouse_1_contact = '';
            //     $plane_to_warehouse_2_contact = '';
            //     $plane_to_warehouse_3_contact = '';

            //     $domestic_order_prep_contact = '';
            //     $domestic_ship_confirmation_contact = '';
            //     $domestic_to_warehouse_contact = '';

            //     $v_contacts = Vendor_contact::where(array('vendor_id' => $request->vendor_id))->get()->toArray();
            //     if (!empty($v_contacts)) {
            //         $boat_production_mail_1_contact .= '<form class="select_contact_confirm_production">';
            //         $boat_production_mail_2_contact .= '<form class="select_contact_end_production">';
            //         $boat_port_departure_contact .= '<form class="select_contact_port_depature">';
            //         $boat_in_transist_contact .= '<form class="select_contact_in_transit boat_in_transist">';
            //         $boat_to_warehouse_1_contact .= '<form class="select_contact_confirm_product_at_port">';
            //         $boat_to_warehouse_2_contact .= '<form class="select_contact_confirm_product_left_port">';
            //         $boat_to_warehouse_3_contact .= '<form class="select_contact_confirm_warehouse_receipt">';

            //         $plane_production_mail_1_contact .= '<form class="plane_product_confirm">';
            //         $plane_production_mail_2_contact .= '<form class="plane_end_production">';
            //         $plane_port_departure_contact .= '<form class="plane_port_to_departure">';
            //         $plane_in_transist_contact .= '<form class="plane_in_transit">';
            //         $plane_to_warehouse_1_contact .= '<form class="plane_confirm_arrived_port">';
            //         $plane_to_warehouse_2_contact .= '<form class="plane_confirm_left_port">';
            //         $plane_to_warehouse_3_contact .= '<form class="plane_confirm_warehouse_receipt">';

            //         $domestic_order_prep_contact = '<form class="domestic_order_prep">';
            //         $domestic_ship_confirmation_contact = '<form class="domestic_ship_confirmation_select_contact">';
            //         $domestic_to_warehouse_contact = '<form class="domestic_warehouse">';

            //         $checked1 = '';
            //         $checked2 = '';
            //         $checked3 = '';
            //         $checked4 = '';
            //         $checked5 = '';
            //         $checked6 = '';
            //         $checked7 = '';
            //         $checked8 = '';
            //         $checked9 = '';
            //         $checked10 = '';
            //         $checked11 = '';
            //         $checked12 = '';
            //         $checked13 = '';
            //         $checked14 = '';
            //         $checked15 = '';
            //         $checked16 = '';
            //         $checked17 = '';

            //         foreach ($v_contacts as $key => $contact) {
            //             //$rand = rand(10000,99999);

            //             $get_lead_time = Lead_time::where('supplier_vendor', env('VENDOR'))->where('supplier_vendor_id', $request->vendor_id)->first();
            //             if (!empty($get_lead_time)) {
            //                 $get_lead_time_contact1 = get_lead_time($get_lead_time->id, 1, 1);
            //                 $get_array1 = explode(',', @$get_lead_time_contact1->contact_detail);
            //                 $checked1 = in_array($contact['id'], $get_array1) && @$get_lead_time->type == '1' ? 'checked' : '';

            //                 $get_lead_time_contact2 = get_lead_time($get_lead_time->id, 2, 1);
            //                 $get_array2 = explode(',', @$get_lead_time_contact2->contact_detail);
            //                 $checked2 = in_array($contact['id'], $get_array2) && @$get_lead_time->type == '1' ? 'checked' : '';

            //                 $get_lead_time_contact3 = get_lead_time($get_lead_time->id, 3, 1);
            //                 $get_array3 = explode(',', @$get_lead_time_contact3->contact_detail);
            //                 $checked3 = in_array($contact['id'], $get_array3) && @$get_lead_time->type == '1' ? 'checked' : '';

            //                 $get_lead_time_contact4 = get_lead_time($get_lead_time->id, 4, 1);
            //                 $get_array4 = explode(',', @$get_lead_time_contact4->contact_detail);
            //                 $checked4 = in_array($contact['id'], $get_array4) && @$get_lead_time->type == '1' ? 'checked' : '';

            //                 $get_lead_time_contact5 = get_lead_time($get_lead_time->id, 5, 1);
            //                 $get_array5 = explode(',', @$get_lead_time_contact5->contact_detail);
            //                 $checked5 = in_array($contact['id'], $get_array5) && @$get_lead_time->type == '1' ? 'checked' : '';

            //                 $get_lead_time_contact6 = get_lead_time($get_lead_time->id, 6, 1);
            //                 $get_array6 = explode(',', @$get_lead_time_contact6->contact_detail);
            //                 $checked6 = in_array($contact['id'], $get_array6) && @$get_lead_time->type == '1' ? 'checked' : '';

            //                 $get_lead_time_contact7 = get_lead_time($get_lead_time->id, 7, 1);
            //                 $get_array7 = explode(',', @$get_lead_time_contact7->contact_detail);
            //                 $checked7 = in_array($contact['id'], $get_array7) && @$get_lead_time->type == '1' ? 'checked' : '';


            //                 $get_lead_time_contact8 = get_lead_time($get_lead_time->id, 1, 2);
            //                 $get_array8 = explode(',', @$get_lead_time_contact8->contact_detail);
            //                 $checked8 = in_array($contact['id'], $get_array8) && @$get_lead_time->type == '2' ? 'checked' : '';

            //                 $get_lead_time_contact9 = get_lead_time($get_lead_time->id, 2, 2);
            //                 $get_array9 = explode(',', @$get_lead_time_contact9->contact_detail);
            //                 $checked9 = in_array($contact['id'], $get_array9) && @$get_lead_time->type == '2' ? 'checked' : '';

            //                 $get_lead_time_contact10 = get_lead_time($get_lead_time->id, 3, 2);
            //                 $get_array10 = explode(',', @$get_lead_time_contact10->contact_detail);
            //                 $checked10 = in_array($contact['id'], $get_array10) && @$get_lead_time->type == '2' ? 'checked' : '';

            //                 $get_lead_time_contact11 = get_lead_time($get_lead_time->id, 4, 2);
            //                 $get_array11 = explode(',', @$get_lead_time_contact11->contact_detail);
            //                 $checked11 = in_array($contact['id'], $get_array11) && @$get_lead_time->type == '2' ? 'checked' : '';

            //                 $get_lead_time_contact12 = get_lead_time($get_lead_time->id, 5, 2);
            //                 $get_array12 = explode(',', @$get_lead_time_contact12->contact_detail);
            //                 $checked12 = in_array($contact['id'], $get_array12) && @$get_lead_time->type == '2' ? 'checked' : '';

            //                 $get_lead_time_contact13 = get_lead_time($get_lead_time->id, 6, 2);
            //                 $get_array13 = explode(',', @$get_lead_time_contact13->contact_detail);
            //                 $checked13 = in_array($contact['id'], $get_array13) && @$get_lead_time->type == '2' ? 'checked' : '';

            //                 $get_lead_time_contact14 = get_lead_time($get_lead_time->id, 7, 2);
            //                 $get_array14 = explode(',', @$get_lead_time_contact14->contact_detail);
            //                 $checked14 = in_array($contact['id'], $get_array14) && @$get_lead_time->type == '2' ? 'checked' : '';


            //                 $get_lead_time_contact15 = get_lead_time($get_lead_time->id, 8, 2);
            //                 $get_array15 = explode(',', @$get_lead_time_contact15->contact_detail);
            //                 $checked15 = in_array($contact['id'], $get_array15) && @$get_lead_time->type == '0' ? 'checked' : '';

            //                 $get_lead_time_contact16 = get_lead_time($get_lead_time->id, 9, 2);
            //                 $get_array16 = explode(',', @$get_lead_time_contact16->contact_detail);
            //                 $checked16 = in_array($contact['id'], $get_array16) && @$get_lead_time->type == '0' ? 'checked' : '';

            //                 $get_lead_time_contact17 = get_lead_time($get_lead_time->id, 10, 2);
            //                 $get_array17 = explode(',', @$get_lead_time_contact17->contact_detail);
            //                 $checked17 = in_array($contact['id'], $get_array17) && @$get_lead_time->type == '0' ? 'checked' : '';


            //             }

            //             $boat_production_mail_1_contact .= '<div class="custom-control custom-checkbox custom-checkbox-primary pl-5 py-1">' .
            //                 '<input type="checkbox" class="custom-control-input" data-contactid="' . $contact['id'] . '" id="checkbox_contact_boat_product_1_' . $key . '" ' . $checked1 . '>' .
            //                 '<label class="custom-control-label" for="checkbox_contact_boat_product_1_' . $key . '">' . $contact['first_name'] . ' ' . $contact['last_name'] . '</label>' .
            //                 '</div>';

            //             $boat_production_mail_2_contact .= '<div class="custom-control custom-checkbox custom-checkbox-primary pl-5 py-1">' .
            //                 '<input type="checkbox" class="custom-control-input" data-contactid="' . $contact['id'] . '" id="checkbox_contact_boat_product_2_' . $key . '" ' . $checked2 . '>' .
            //                 '<label class="custom-control-label" for="checkbox_contact_boat_product_2_' . $key . '">' . $contact['first_name'] . ' ' . $contact['last_name'] . '</label>' .
            //                 '</div>';
            //             $boat_port_departure_contact .= '<div class="custom-control custom-checkbox custom-checkbox-primary pl-5 py-1">' .
            //                 '<input type="checkbox" class="custom-control-input" data-contactid="' . $contact['id'] . '" id="boat_port_departure_contact_' . $key . '" ' . $checked3 . '>' .
            //                 '<label class="custom-control-label" for="boat_port_departure_contact_' . $key . '">' . $contact['first_name'] . ' ' . $contact['last_name'] . '</label>' .
            //                 '</div>';
            //             $boat_in_transist_contact .= '<div class="custom-control custom-checkbox custom-checkbox-primary pl-5 py-1">' .
            //                 '<input type="checkbox" class="custom-control-input boat_in_transist_checkbox" data-contactid="' . $contact['id'] . '" id="boat_in_transist_contact_' . $key . '" ' . $checked4 . '>' .
            //                 '<label class="custom-control-label" for="boat_in_transist_contact_' . $key . '">' . $contact['first_name'] . ' ' . $contact['last_name'] . '</label>' .
            //                 '</div>';
            //             $boat_to_warehouse_1_contact .= '<div class="custom-control custom-checkbox custom-checkbox-primary pl-5 py-1">' .
            //                 '<input type="checkbox" class="custom-control-input" data-contactid="' . $contact['id'] . '" id="boat_to_warehouse_1_contact_' . $key . '" ' . $checked5 . '>' .
            //                 '<label class="custom-control-label" for="boat_to_warehouse_1_contact_' . $key . '">' . $contact['first_name'] . ' ' . $contact['last_name'] . '</label>' .
            //                 '</div>';
            //             $boat_to_warehouse_2_contact .= '<div class="custom-control custom-checkbox custom-checkbox-primary pl-5 py-1">' .
            //                 '<input type="checkbox" class="custom-control-input" data-contactid="' . $contact['id'] . '" id="boat_to_warehouse_2_contact_' . $key . '" ' . $checked6 . '>' .
            //                 '<label class="custom-control-label" for="boat_to_warehouse_2_contact_' . $key . '">' . $contact['first_name'] . ' ' . $contact['last_name'] . '</label>' .
            //                 '</div>';
            //             $boat_to_warehouse_3_contact .= '<div class="custom-control custom-checkbox custom-checkbox-primary pl-5 py-1">' .
            //                 '<input type="checkbox" class="custom-control-input" data-contactid="' . $contact['id'] . '" id="boat_to_warehouse_3_contact_' . $key . '" ' . $checked7 . '>' .
            //                 '<label class="custom-control-label" for="boat_to_warehouse_3_contact_' . $key . '">' . $contact['first_name'] . ' ' . $contact['last_name'] . '</label>' .
            //                 '</div>';


            //             $plane_production_mail_1_contact .= '<div class="custom-control custom-checkbox custom-checkbox-primary pl-5 py-1">' .
            //                 '<input type="checkbox" class="custom-control-input" data-contactid="' . $contact['id'] . '" id="checkbox_contact_plane_product_1_' . $key . '" ' . $checked8 . '>' .
            //                 '<label class="custom-control-label" for="checkbox_contact_plane_product_1_' . $key . '">' . $contact['first_name'] . ' ' . $contact['last_name'] . '</label>' .
            //                 '</div>';

            //             $plane_production_mail_2_contact .= '<div class="custom-control custom-checkbox custom-checkbox-primary pl-5 py-1">' .
            //                 '<input type="checkbox" class="custom-control-input" data-contactid="' . $contact['id'] . '" id="checkbox_contact_plane_product_2_' . $key . '" ' . $checked9 . '>' .
            //                 '<label class="custom-control-label" for="checkbox_contact_plane_product_2_' . $key . '">' . $contact['first_name'] . ' ' . $contact['last_name'] . '</label>' .
            //                 '</div>';
            //             $plane_port_departure_contact .= '<div class="custom-control custom-checkbox custom-checkbox-primary pl-5 py-1">' .
            //                 '<input type="checkbox" class="custom-control-input" data-contactid="' . $contact['id'] . '" id="plane_port_departure_contact_' . $key . '" ' . $checked10 . '>' .
            //                 '<label class="custom-control-label" for="plane_port_departure_contact_' . $key . '">' . $contact['first_name'] . ' ' . $contact['last_name'] . '</label>' .
            //                 '</div>';
            //             $plane_in_transist_contact .= '<div class="custom-control custom-checkbox custom-checkbox-primary pl-5 py-1">' .
            //                 '<input type="checkbox" class="custom-control-input" data-contactid="' . $contact['id'] . '" id="plane_in_transist_contact_' . $key . '" ' . $checked11 . '>' .
            //                 '<label class="custom-control-label" for="plane_in_transist_contact_' . $key . '">' . $contact['first_name'] . ' ' . $contact['last_name'] . '</label>' .
            //                 '</div>';
            //             $plane_to_warehouse_1_contact .= '<div class="custom-control custom-checkbox custom-checkbox-primary pl-5 py-1">' .
            //                 '<input type="checkbox" class="custom-control-input" data-contactid="' . $contact['id'] . '" id="plane_to_warehouse_1_contact_' . $key . '" ' . $checked12 . '>' .
            //                 '<label class="custom-control-label" for="plane_to_warehouse_1_contact_' . $key . '">' . $contact['first_name'] . ' ' . $contact['last_name'] . '</label>' .
            //                 '</div>';
            //             $plane_to_warehouse_2_contact .= '<div class="custom-control custom-checkbox custom-checkbox-primary pl-5 py-1">' .
            //                 '<input type="checkbox" class="custom-control-input" data-contactid="' . $contact['id'] . '" id="plane_to_warehouse_2_contact_' . $key . '" ' . $checked13 . '>' .
            //                 '<label class="custom-control-label" for="plane_to_warehouse_2_contact_' . $key . '">' . $contact['first_name'] . ' ' . $contact['last_name'] . '</label>' .
            //                 '</div>';
            //             $plane_to_warehouse_3_contact .= '<div class="custom-control custom-checkbox custom-checkbox-primary pl-5 py-1">' .
            //                 '<input type="checkbox" class="custom-control-input" data-contactid="' . $contact['id'] . '" id="plane_to_warehouse_3_contact_' . $key . '" ' . $checked14 . '>' .
            //                 '<label class="custom-control-label" for="plane_to_warehouse_3_contact_' . $key . '">' . $contact['first_name'] . ' ' . $contact['last_name'] . '</label>' .
            //                 '</div>';


            //             $domestic_order_prep_contact .= '<div class="custom-control custom-checkbox custom-checkbox-primary pl-5 py-1">' .
            //                 '<input type="checkbox" class="custom-control-input" data-contactid="' . $contact['id'] . '" id="domestic_order_prep_contact_' . $key . '" ' . $checked15 . '>' .
            //                 '<label class="custom-control-label" for="domestic_order_prep_contact_' . $key . '">' . $contact['first_name'] . ' ' . $contact['last_name'] . '</label>' .
            //                 '</div>';
            //             $domestic_ship_confirmation_contact .= '<div class="custom-control custom-checkbox custom-checkbox-primary pl-5 py-1">' .
            //                 '<input type="checkbox" class="custom-control-input" data-contactid="' . $contact['id'] . '" id="domestic_ship_confirmation_contact_' . $key . '" ' . $checked16 . '>' .
            //                 '<label class="custom-control-label" for="domestic_ship_confirmation_contact_' . $key . '">' . $contact['first_name'] . ' ' . $contact['last_name'] . '</label>' .
            //                 '</div>';
            //             $domestic_to_warehouse_contact .= '<div class="custom-control custom-checkbox custom-checkbox-primary pl-5 py-1">' .
            //                 '<input type="checkbox" class="custom-control-input" data-contactid="' . $contact['id'] . '" id="domestic_to_warehouse_contact_' . $key . '" ' . $checked17 . '>' .
            //                 '<label class="custom-control-label" for="domestic_to_warehouse_contact_' . $key . '">' . $contact['first_name'] . ' ' . $contact['last_name'] . '</label>' .
            //                 '</div>';
            //         }
            //         $boat_production_mail_1_contact .= '</form>';
            //         $boat_production_mail_2_contact .= '</form>';
            //         $boat_port_departure_contact .= '</form>';
            //         $boat_in_transist_contact .= '</form>';
            //         $boat_to_warehouse_1_contact .= '</form>';
            //         $boat_to_warehouse_2_contact .= '</form>';
            //         $boat_to_warehouse_3_contact .= '</form>';

            //         $plane_production_mail_1_contact .= '</form>';
            //         $plane_production_mail_2_contact .= '</form>';
            //         $plane_port_departure_contact .= '</form>';
            //         $plane_in_transist_contact .= '</form>';
            //         $plane_to_warehouse_1_contact .= '</form>';
            //         $plane_to_warehouse_2_contact .= '</form>';
            //         $plane_to_warehouse_3_contact .= '</form>';

            //         $domestic_order_prep_contact .= '</form>';
            //         $domestic_ship_confirmation_contact .= '</form>';
            //         $domestic_to_warehouse_contact .= '</form>';
            //     }
            // $vendorcontactdetails .= '<tr class="add-row-tr">
            //   <td colspan="6"><span class="text-primary1 add-row">
            //   <i class="bx bx-plus-circle font-size-16 align-middle"></i>Add Contacts</span></td></tr>';
            // $res['boat_production_mail_1_contact'] = $boat_production_mail_1_contact;
            // $res['boat_production_mail_2_contact'] = $boat_production_mail_2_contact;
            // $res['boat_port_departure_contact'] = $boat_port_departure_contact;
            // $res['boat_in_transist_contact'] = $boat_in_transist_contact;
            // $res['boat_to_warehouse_1_contact'] = $boat_to_warehouse_1_contact;
            // $res['boat_to_warehouse_2_contact'] = $boat_to_warehouse_2_contact;
            // $res['boat_to_warehouse_3_contact'] = $boat_to_warehouse_3_contact;

            // $res['plane_production_mail_1_contact'] = $plane_production_mail_1_contact;
            // $res['plane_production_mail_2_contact'] = $plane_production_mail_2_contact;
            // $res['plane_port_departure_contact'] = $plane_port_departure_contact;
            // $res['plane_in_transist_contact'] = $plane_in_transist_contact;
            // $res['plane_to_warehouse_1_contact'] = $plane_to_warehouse_1_contact;
            // $res['plane_to_warehouse_2_contact'] = $plane_to_warehouse_2_contact;
            // $res['plane_to_warehouse_3_contact'] = $plane_to_warehouse_3_contact;

            // $res['domestic_order_prep_contact'] = $domestic_order_prep_contact;
            // $res['domestic_ship_confirmation_contact'] = $domestic_ship_confirmation_contact;
            // $res['domestic_to_warehouse_contact'] = $domestic_to_warehouse_contact;
            // $res['vendorcontactdetails'] = $vendorcontactdetails;


        }
        if ($request->insert_type == 'insert_update_setting') {
            if(empty($request->vendor_id)){
                $res_arr['success'] = false;
                $res_arr['error'] = 1;
                $res_arr['message'] = 'Please fill up basic details';
                return response()->json($res_arr);
            }
            $validator = Validator::make($request->all(), [
                'lead_time' => 'required',
                'moqs'=>'required',
                'order_volume'=>'required',
                'Production_Time'=>'required',
                'Boat_To_Port'=>'required',
                'Port_To_Warehouse'=>'required',
                'Warehouse_Receipt'=>'required',
                'cbm_per_container'=>'required',
                'quantity_discount'=>'required',
                'warehouse_id'=>'required'
            ]);
            if ($validator->fails()) {
                $res['success'] = false;
                $res['error'] = 1;
                $res['message'] = 'Please provide required field data';
            }
            else {
                $req_data = [
                    'vendor_id' => $request->vendor_id,
                    'lead_time' => $request->lead_time,
                    'moq' => $request->moqs,
                    'order_volume' => $request->order_volume,
                    'Production_Time' => $request->Production_Time,
                    'Boat_To_Port' => $request->Boat_To_Port,
                    'Port_To_Warehouse' => $request->Port_To_Warehouse,
                    'Warehouse_Receipt' => $request->Warehouse_Receipt,
                    'CBM_Per_Container' => $request->cbm_per_container,
                    'quantity_discount' => $request->quantity_discount,
                    'Ship_To_Specific_Warehouse' => $request->warehouse_id,
                ];

                $check_data = vendor_wise_default_setting::where(array('vendor_id' => $request->vendor_id))->first();
                if (empty($check_data)) {
                    $datas_of = vendor_wise_default_setting::create($req_data);
                    //$message = get_messages('vendor default setting created successfully', 1);
                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = "vendor default setting created successfully";
                    $res['vendor_id'] = $request->vendor_id;
                } else {
                    $datas_of = vendor_wise_default_setting::where(array('vendor_id' => $request->vendor_id))->update($req_data);
                    //$message = get_messages('vendor default setting updated successfully', 1);
                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = "vendor default setting updated successfully";
                    $res['vendor_id'] = $request->vendor_id;
                }
            }
            return response()->json($res);
        }
        if ($request->insert_type == 'delete_vendors_contact') {
            $id = $request->vendor_contact_id;
            $vendor = Vendor_contact::where(array('id' => $id))->delete();
            return json_encode(array('statusCode' => 200));
        }
        if ($request->insert_type == 'delete_blackout_date') {
            $id = $request->black_out_id;
            $ids = explode(',', $id);
            $check_date = vendor_blackout_date::whereIn('id', $ids)->delete();
            return json_encode(array('statusCode' => 200));
        }
        if ($request->insert_type == "insert_update_payment") {
            
            if(empty($request->vendors_supplier_id)){
                $res_arr['success'] = false;
                $res_arr['error'] = 1;
                $res_arr['message'] = 'Please fill up basic details';
                return response()->json($res_arr);
            }
            $validator = Validator::make($request->all(), [
                'deposit_per' => 'required',
                'deposit_due' => 'required',
                'deposit_day'=>'required',
                'desposit_term_id'=>'required',
                'balance_per'=>'required',
                'balance_due'=>'required',
                'balance_day'=>'required',
                'balance_term_id'=>'required'
            ]);
            if ($validator->fails()) {
                $res['success'] = false;
                $res['error'] = 1;
                $res['message'] = 'Please provide required field data';
            }
            else {
                $vendors_supplier_type = env('VENDOR');
                if ($request->vendors_supplier_id != '' && $vendors_supplier_type != '') {

                    $deposit = [
                        'payment_terms' => '1',
                        'percentage' => $request->deposit_per,
                        'dues' => $request->deposit_due,
                        'days_before_after' => $request->deposit_day,
                        'payment_term_type_id' => $request->desposit_term_id,
                    ];

                    $balance = [
                        'payment_terms' => '2',
                        'percentage' => $request->balance_per,
                        'dues' => $request->balance_due,
                        'days_before_after' => $request->balance_day,
                        'payment_term_type_id' => $request->balance_term_id,
                    ];

                    $check_exists = Payment::where(array('supplier_vendor' => $vendors_supplier_type, 'supplier_vendor_id' => $request->vendors_supplier_id))->first();
                    if (!empty($check_exists)) {
                        $payment_id = $check_exists->id;
                        if ($payment_id != '') {
                            $get_desosit_data = Payment_details::where(array('payment_terms' => '1', 'payment_id' => $payment_id))->first();
                            $get_balance_data = Payment_details::where(array('payment_terms' => '2', 'payment_id' => $payment_id))->first();
                            if (!empty($get_desosit_data)) {
                                Payment_details::where(array('id' => $get_desosit_data->id))->update($deposit);
                            } else {
                                $deposit['payment_id'] = $payment_id;
                                Payment_details::create($deposit);
                            }

                            if (!empty($get_balance_data)) {
                                Payment_details::where(array('id' => $get_balance_data->id))->update($balance);
                            } else {
                                $balance['payment_id'] = $payment_id;
                                Payment_details::create($balance);
                            }
                        }
                    } else {
                        $payment = [
                            'supplier_vendor_id' => $request->vendors_supplier_id,
                            'supplier_vendor' => $vendors_supplier_type,
                        ];
                        $payment_data = Payment::create($payment);
                        $payment_id = $payment_data->id;
                        $deposit['payment_id'] = $payment_id;
                        $balance['payment_id'] = $payment_id;
                        Payment_details::create($deposit);
                        Payment_details::create($balance);
                    }
                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = "Payment details updated successfully";
                    $res['vendor_id'] = $request->vendors_supplier_id;

                } else {
                    $res['success'] = false;
                    $res['error'] = 1;
                    $res['message'] = 'Vendor not found';
                }
            }
            return response()->json($res);
        }
        if ($request->insert_type == 'remove_vendors_blackout_date') {
            $id = $request->id;
            $vendor = Vendor_blackout_date::where(array('id' => $id))->delete();
            return json_encode(array('statusCode' => 200));
        }
        if ($request->insert_type == 'insert_update_backout_date') {

           
            $res['success'] = true;
            $res['error'] = 0;
            $res['message'] = "Vendor blackout date updated successfully";
            $res['vendor_id'] = $request->vendors_id;

            $events = $request->events;
            // $start_dates = $request->start_dates;
            // $last_dates = $request->last_dates;
            // $number_of_days = $request->number_of_days;
            // $types = $request->types;
            $vendors_id = $request->vendors_id;

           
            if(empty($request->vendors_id)){
                $res_arr['success'] = false;
                $res_arr['error'] = 1;
                $res_arr['message'] = 'Please fill up basic details';
                return response()->json($res_arr);
            }
            $mandatory=0;
            if(!empty($events)) {
                foreach ($events as $key => $val) {
                    if ($val['name'] != '' && $val['start_date'] != '' && $val['end_date'] != '') {
                        if ((strtotime($val['start_date'])) > (strtotime($val['end_date'])))
                        {
                            $mandatory=2;
                        }
                    }
                    else{
                        $mandatory=1;
                    }
                }
            }
            if($mandatory==1){
                $res['success'] = false;
                $res['error'] = 1;
                $res['message'] = 'Please provide required field data';
            }
            else if($mandatory==2){
                $res['success'] = false;
                $res['error'] = 1;
                $res['message'] = 'Start date should be less than end date';
            }
            else {
               
                if (!empty($events)) {
                    if ($vendors_id != '') {
                       // $delete = Vendor_blackout_date::where(array('vendor_id' => $vendors_id))->delete();
                    }
                    foreach ($events as $key => $val) {
                        if ($val['name'] != '' && $val['start_date'] != '' && $val['end_date'] != '') {

                            $date1 = date_create(date("Y-m-d", strtotime($val['start_date'])));
                            $date2 = date_create(date("Y-m-d", strtotime($val['end_date'])));
                            $diff = date_diff($date1, $date2);
                            $days_diff = $diff->format("%a");

                            $req_data = [
                                'vendor_id' => $vendors_id,
                                'event_name' => $val['name'],
                                'start_date' => date("Y-m-d", strtotime($val['start_date'])),
                                'end_date' => date("Y-m-d", strtotime($val['end_date'])),
                                'number_of_days' => $days_diff,
                                'type' => $val['type'],
                            ];
                            $create = Vendor_blackout_date::create($req_data);
                            //$message = get_messages('Vendor blackout date created successfully', 1);
                            $res['success'] = true;
                            $res['error'] = 0;
                            $res['message'] = "Vendor blackout date created successfully";
                            $res['vendor_id'] = $vendors_id;
                        } else {
                            //$message = get_messages('All fields must be required', 1);
                            $res['success'] = false;
                            $res['error'] = 0;
                            $res['message'] = "All fields must be required";
                            $res['vendor_id'] = $vendors_id;
                        }

                    }

                }
            }
            return response()->json($res);
        }
        if ($request->request_type == 'vendors_list') {
            $data = Vendor::where(array('user_marketplace_id' => $request->id))->get()->toArray();
            $vendors_data = array();
            if (!empty($data)) {
                foreach ($data as $key => $post) {
                    $cnname = "";
                    if (isset($post['country_id']) && $post['country_id'] != "") {
                        $cnname = get_country_fullname($post['country_id']);
                    }

                    $action = '';
                    $nestedData['vendor_name'] = $post['vendor_name'];
                    $nestedData['zipcode'] = $post['zipcode'];
                    $nestedData['country'] = $cnname;
                    $nestedData['user_marketplace_id'] = $post['user_marketplace_id'];
                    $action .= '<ul class="d-flex flex-row align-items-center table-right-actions">
                        <li>
                            <a href="' . route('vendors.edit', $post['id']) . '" ><img src="asset/images/svg/edit.svg" alt=""></a>
                        </li>
                        <li class="px-2">
                            <a href="#" ><img src="asset/images/svg/basket.svg" alt="" height="15.13"></a>
                        </li>
                        <li>
                            <img src="asset/images/svg/polygon.svg" alt="">
                        </li>
                    </ul>';
                    $nestedData['action'] = $action;
                    $vendors_data[] = $nestedData;
                }
            }
            echo json_encode($vendors_data);
            exit;
        }
        if ($request->request_type == 'get_pagi_country') {
            $global_marketplace = session('MARKETPLACE_ID');
            $pagination_left = '';
            $pagination_right = '';
            $pagination = array();
            $success = 0;
            $pagination_slot = $request->pagination_slot;
            if ($global_marketplace != '') {
                $whereArray = [
                    "user_marketplace_id" => $global_marketplace
                ];
                if ($request->get_country) {
                    $whereArray = [
                        "user_marketplace_id" => $global_marketplace,
                        "country_id" => $request->get_country
                    ];
                }
                if($request->search != ''){
                    $whereArray[] = [
                        'vendor_name', 'LIKE', "%{$request->search}%"
                    ];
                }

                if ($request->toggle_b == 0) {
                    $data_count = Vendor::with(['vendor_contact', 'vendor_product'])->where($whereArray)->count();
                } else {
                    $listing_data = Vendor::with(['vendor_contact' => function ($query) {
                        $query->orderBy('primary', 'desc');
                    }, 'vendor_product'])->where($whereArray)->get()->toArray();
                    $data_count = 0;
                    foreach ($listing_data as $index => $data) {
                        $vendors_contact = Vendor_contact::where(array('vendor_id' => $data['id']))->first();
                        $vendors_blackoutdate = Vendor_blackout_date::where(array('vendor_id' => $data['id']))->first();
                        $lead_time = Lead_time::where(array('supplier_vendor' => env('VENDOR'), 'supplier_vendor_id' => $data['id']))->first();
                        $order_setting = Reorder_schedule_detail::where(array('supplier_vendor' => env('VENDOR'), 'supplier_vendor_id' => $data['id']))->first();
                        $payment = Payment::where(array('supplier_vendor' => '1', 'supplier_vendor_id' => $data['id']))->first();
                        $shipping = Shipping::where(array('supplier_vendor' => env('VENDOR'), 'supplier_vendor_id' => $data['id']))->first();
                        if (empty($vendors_contact) || empty($vendors_blackoutdate) || empty($lead_time) || empty($order_setting) || empty($payment) || empty($shipping)) {
                            $data_count++;
                        }
                    }
                }
                $keyj = 1;
                if ($data_count > 0) {
                    for ($i = 1; $i <= $data_count; $i = $i + $pagination_slot) {
                        if ($i == 1) {
                            $newi = $i;
                        } else {
                            $newi = $i - 1;
                        }
                        $n = $i + ($pagination_slot - 1);
                        //$pagination[$keyj]= $newi.'-'.$n;
                        $pagination[$keyj] = ($newi) . '-' . $n;
                        $keyj++;
                    }
                } else {
                    $newi = 1;
                    $n = $newi + ($pagination_slot - 1);
                    $pagination[$newi] = ($newi) . '-' . $n;

                }
                $pagi = ($request->pagecount ? $request->pagecount : '');
                $success = 1;
                //$pagination_left = get_pagination_view($pagination);
                $pagination_right = get_pagination_right_view($pagination, $data_count,$pagination_slot, $pagi);
            }
            $res['success'] = $success;
            //$res['select_pagi'] = $pagination_left;
            $res['paginations'] = $pagination_right;
            $res['count'] = $data_count;
            return response()->json($res);
            exit;
        }
        if ($request->insert_type == 'delete_multiple_records') {
            Vendor::whereIn('id', explode(",", $request->delete_array))->delete();
            return json_encode(array('statusCode' => 200));
        }
        if ($request->insert_type == 'delete_all_records') {
            $global_marketplace = session('MARKETPLACE_ID');
            Vendor::where('user_marketplace_id', $global_marketplace)->delete();
            return json_encode(array('statusCode' => 200));
        }
        if ($request->insert_type == 'create_shipping_agent') {
            $req_data = [
                'user_marketplace_id' => $request->marketplace_id,
                'company_name' => $request->company_name,
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'phone' => $request->phone,
                'country_id' => ($request->country ? $request->country : 1)
            ];

            $datas_of = ShippingAgent::create($req_data);
            $userLastInsertId = $datas_of->toArray();
            if ($userLastInsertId != '') {
                $res['shipping_agent_id'] = $userLastInsertId['id'];
            }
            $res['shipping_agent_array'] = ShippingAgent::where('user_marketplace_id', $request->marketplace_id)->get()->toArray();
            $message = 'Shipping Agent created successfully';
            $res['error'] = 0;
            $res['message'] = $message;
            return response()->json($res);
        }
        if ($request->insert_type == 'create_shipping_carrier') {
            $req_data = [
                'user_marketplace_id' => $request->marketplace_id,
                'carrier_name' => $request->carrier_name,
            ];
            $datas_of = Shippingcarrier::create($req_data);
            $shippingCarrierLastInsertId = $datas_of->toArray();
            if ($shippingCarrierLastInsertId != '') {
                $res['shipping_carrier_id'] = $shippingCarrierLastInsertId['id'];
            }
            $res['shipping_carrier_array'] = Shippingcarrier::where('user_marketplace_id', $request->marketplace_id)->get()->toArray();
            $message = get_messages('Shipping Carrier created successfully', 0);
            $res['error'] = 0;
            $res['success'] = true;
            $res['message'] = $message;
            return response()->json($res);
        }
        if ($request->insert_type == 'insert_update_shipping_details') {
            if(empty($request->vendor_id)){
                $res_arr['success'] = false;
                $res_arr['error'] = 1;
                $res_arr['message'] = 'Please fill up basic details';
                return response()->json($res_arr);
            }
            if ($request->vendor_type == 1) {
                $shipping_id = Shipping::where('supplier_vendor', env('VENDOR'))->where('supplier_vendor_id',$request->vendor_id)->get()->first();
                if(!empty($shipping_id))
                    $shipping_id = $shipping_id->id;
                $typeArray = ['on-demand'=>0,'weekly' => 1, 'monthly' => 2];
                if ($request->shipping_type == 1) {
                    $validator = Validator::make($request->all(), [
                        'vendor_id' => 'required',
                        'shipping_type'=>'required',
                        'days' => 'required'
                    ]);
                    if ($validator->fails()) {
                        $res['success'] = false;
                        $res['error'] = 1;
                        $res['message'] = 'Please provide required field data';
                        return response()->json($res);
                    }
                    else {
                        $req_data_shipping = [
                            'supplier_vendor' => env('VENDOR'),
                            'supplier_vendor_id' => $request->vendor_id,
                            'direct_to_amazon' => ($request->direct_to_amazon == 'on' ? 1 : 0),
                            'shipping_type' => $request->shipping_type
                        ];
                        if (empty($shipping_id)) {
                            $datas_of = Shipping::create($req_data_shipping);
                            $userLastInsertId = $datas_of->toArray();
                            $shipping_id = $userLastInsertId['id'];
                        } else {
                            $shippingData = Shipping::findOrFail($shipping_id);
                            $shippingData->update($req_data_shipping);
                        }
                        if (!empty($shipping_id)) {
                            $warehouses = $request->warehouses;
                            if (!empty($warehouses)) {
                                ShipToWarehouse::where('shipping_id', $shipping_id)->delete();
                                foreach ($warehouses as $warehouse) {
                                    $req_data_warehouse = [
                                        'shipping_id' => $shipping_id,
                                        'warehouse_value' => $warehouse
                                    ];
                                    ShipToWarehouse::create($req_data_warehouse);
                                }
                            }
                            ShippingDeliveryDetails::where('shipping_id', $shipping_id)->delete();
                            Shippingcutoffdeliverytime::where('shipping_id', $shipping_id)->delete();
                            $shipping_cut_off_delivery_time = $request->cut_off_deliver_time;
                            $shippDeliveryScheduleType = $request->delivery_schedule;
                            if($request->delivery_schedule != 'on-demand'){
                                foreach ($request->days as $key => $schedule_type) {
                                    $shipping_schedule = [
                                        'shipping_id' => $shipping_id,
                                        'delivery_schedule_type' => $typeArray[$request->delivery_schedule],
                                        'schedule_day' => $schedule_type
                                    ];
                                    $shipping_schedule_data = ShippingDeliveryDetails::create($shipping_schedule);
                                    $shipScheduleLastInsertId = $shipping_schedule_data->toArray();
                                    $shipScheduleLastInsert_id = $shipScheduleLastInsertId['id'];
                                    if (!empty($shipScheduleLastInsert_id)) {
                                        $cutOffDay = $cutOffTime = '';
                                        if($request->delivery_schedule == 'monthly'){
                                            $cutOffDay = $request->cutoff_delivery_time['monthly']['days'];
                                            $cutOffTime = $request->cutoff_delivery_time['monthly']['time'];
                                        } else{
                                            $cutOffDay = strtolower($schedule_type);
                                            $cutOffTime = $request->cutoff_delivery_time['weekly'][strtolower($schedule_type)]['time'];
                                        }
                                        $shippingCutoffDeliveryTimeArray = [
                                            'shipping_id' => $shipping_id,
                                            'shipping_delivery_id' => $shipScheduleLastInsert_id,
                                            'cut_off_day' => $cutOffDay,
                                            'cut_off_time' => $cutOffTime
                                        ];
                                        Shippingcutoffdeliverytime::create($shippingCutoffDeliveryTimeArray);
                                    }
                                }
                            } else {
                                $shipping_schedule = [
                                    'shipping_id' => $shipping_id,
                                    'delivery_schedule_type' => $typeArray[$request->delivery_schedule],
                                    'schedule_day' => ''
                                ];
                                $shipping_schedule_data = ShippingDeliveryDetails::create($shipping_schedule);
                            }
                        } else {
                            $res['success'] = false;
                            $res['error'] = 1;
                            $res['message'] = "There is some issue in creating or updating shipping details";
                            $res['vendor_id'] = $request->vendor_id;
                        }
                    }
                } else {
                    $validator = Validator::make($request->all(), [
                        'vendor_id' => 'required',
                        'shipping_carrier_id' => 'required',
                        'tracking_provided'=>'required',
//                        'direct_to_amazon'=>'required',
                        'shipping_type'=>'required'
                    ]);
                    if ($validator->fails()) {
                        $res['success'] = false;
                        $res['error'] = 1;
                        $res['message'] = 'Please provide required field data';
                        return response()->json($res);
                    }
                    else {
                        $req_data_shipping = [
                            'supplier_vendor' => env('VENDOR'),
                            'supplier_vendor_id' => $request->vendor_id,
                            'shipping_carrier_id' => ($request->shipping_carrier_id ? $request->shipping_carrier_id : ''),
                            'tracking_provided' => ($request->tracking_provided ? $request->tracking_provided : ''),
                            'direct_to_amazon' => ($request->direct_to_amazon == 'on' ? 1 : 0),
                            'shipping_type' => $request->shipping_type,
                        ];
                        if (empty($shipping_id)) {
                            $datas_of = Shipping::create($req_data_shipping);
                            $userLastInsertId = $datas_of->toArray();
                            $shipping_id = $userLastInsertId['id'];
                        } else {
                            $shippingData = Shipping::findOrFail($shipping_id);
                            $shippingData->update($req_data_shipping);
                        }
                        if (!empty($shipping_id)) {
                            $warehouses = $request->warehouses;
                            if (!empty($warehouses)) {
                                ShipToWarehouse::where('shipping_id', $shipping_id)->delete();
                                foreach ($warehouses as $warehouse) {
                                    $req_data_warehouse = [
                                        'shipping_id' => $shipping_id,
                                        'warehouse_value' => $warehouse
                                    ];
                                    ShipToWarehouse::create($req_data_warehouse);
                                }
                            }
                            $res['vendor_id'] = $request->vendor_id;
                        } else {
                            $res['vendor_id'] = $request->vendor_id;
                        }
                    }
                }
            }
            else {
                $validator = Validator::make($request->all(), [
                    'vendor_id' => 'required',
                    'logistics_handler_type' => 'required',
//                    'shipping_agent'=>'required',
                    'lcl_cost_for'=>'required',
                    'lcl_cost_per'=>'required',
                    'lcl_cost'=>'required',
                    'duties_cost_percentage'=>'required',
//                    'shipping_carrier_id'=>'required',
//                    'shipping_type'=>'required',
//                    'tracking_provided'=>'required'
                ]);
                if ($validator->fails()) {
                    $res['success'] = false;
                    $res['error'] = 1;
                    $res['message'] = 'Please provide required field data';
                    return response()->json($res);
                }
                else {
                    $req_data_shipping = [
                        'supplier_vendor' => env('VENDOR'),
                        'supplier_vendor_id' => $request->vendor_id,
                        'logistic_type_id' => ($request->logistics_handler_type ? $request->logistics_handler_type : ''),
                        'shipping_agent_id' => ($request->shipping_agent ? $request->shipping_agent : ''),
                        'lcl_cost_settings' => ($request->lcl_cost_for=='on' ? 1 : 0),
                        'lcl_cost_per' => ($request->lcl_cost_per ? $request->lcl_cost_per : ''),
                        'lcl_cost' => ($request->lcl_cost ? $request->lcl_cost : ''),
                        'duties_cost' => ($request->duties_cost_percentage ? $request->duties_cost_percentage : ''),
                        'direct_to_amazon' => ($request->direct_to_amazon=='on' ? 1 : 0),
                        'shipping_carrier_id' => ($request->shipping_carrier_id ? $request->shipping_carrier_id : ''),
                        'shipping_type' => ($request->shipping_type ? $request->shipping_type : ''),
                        'tracking_provided' => ($request->tracking_provided ? $request->tracking_provided : ''),
                    ];
                    $shipping_id = Shipping::where('supplier_vendor', env('VENDOR'))->where('supplier_vendor_id',$request->vendor_id)->get()->first();
                    if (empty($shipping_id)) {
                        $datas_of = Shipping::create($req_data_shipping);
                        $userLastInsertId = $datas_of->toArray();
                        $shipping_id = $userLastInsertId['id'];
                    } else {
                        $shipping_id = $shipping_id->id;
                        $shippingData = Shipping::findOrFail($shipping_id);
                        $shippingData->update($req_data_shipping);
                    }
                    if (!empty($shipping_id)) {
                        $req_data_container_20ft = [
                            'shipping_id' => $shipping_id,
                            'container_size' => '20ft',
                            'select_container' => ($request->twentyft_checkbox == 'on' ? 1 : 0),
                            'cbm_per_container' => $request->twentyft_cost_per,
                            'container_cost' => $request->twentyft_cost,
                        ];
                        $check20ftContainer = ShippingContainerSettings::where('shipping_id', $shipping_id)->where('container_size', '20ft')->get()->first();
                        if ($check20ftContainer == '') {
                            ShippingContainerSettings::create($req_data_container_20ft);
                        } else {
                            $check20ftContainer->update($req_data_container_20ft);
                        }
                        $req_data_container_40ft = [
                            'shipping_id' => $shipping_id,
                            'container_size' => '40ft',
                            'select_container' => ($request->fourtyft_checkbox == 'on' ? 1 : 0),
                            'cbm_per_container' => $request->fourtyft_cost_per,
                            'container_cost' => $request->fourtyft_cost,
                        ];
                        $check40ftContainer = ShippingContainerSettings::where('shipping_id', $shipping_id)->where('container_size', '40ft')->get()->first();
                        if ($check20ftContainer == '') {
                            ShippingContainerSettings::create($req_data_container_40ft);
                        } else {
                            $check40ftContainer->update($req_data_container_40ft);
                        }
                        $req_data_container_40hq = [
                            'shipping_id' => $shipping_id,
                            'container_size' => '40hq',
                            'select_container' => ($request->fourtyhq_checkbox == 'on' ? 1 : 0),
                            'cbm_per_container' => $request->fourtyhq_cost_per,
                            'container_cost' => $request->fourtyhq_cost,
                        ];
                        $check40hqContainer = ShippingContainerSettings::where('shipping_id', $shipping_id)->where('container_size', '40hq')->get()->first();
                        if ($check20ftContainer == '') {
                            ShippingContainerSettings::create($req_data_container_40hq);
                        } else {
                            $check40hqContainer->update($req_data_container_40hq);
                        }
                        $req_data_container_45hq = [
                            'shipping_id' => $shipping_id,
                            'container_size' => '45hq',
                            'select_container' => ($request->fourtyfivehq_checkbox == 'on' ? 1 : 0),
                            'cbm_per_container' => $request->fourtyfivehq_cost_per,
                            'container_cost' => $request->fourtyfivehq_cost,
                        ];
                        $check45hqContainer = ShippingContainerSettings::where('shipping_id', $shipping_id)->where('container_size', '45hq')->get()->first();
                        if ($check20ftContainer == '') {
                            ShippingContainerSettings::create($req_data_container_45hq);
                        } else {
                            $check45hqContainer->update($req_data_container_45hq);
                        }
                        $shippingAirPrice = $request->shipping_air;
                        if (!empty($shippingAirPrice)) {
                            foreach ($shippingAirPrice as $key => $shippingAir) {
                                $req_data_shipping = [
                                    'shipping_id' => $shipping_id,
                                    'ship_type' => $shippingAir['type'],
                                    'price' => $shippingAir['price'],
                                    'messuare_in' => $shippingAir['messure_in']
                                ];
                                $shippingAirCheck = Shippingair::where('shipping_id', $shipping_id)->where('ship_type', $shippingAir['type'])->where('messuare_in', $shippingAir['messure_in'])->get()->first();
                                if ($shippingAirCheck == '') {
                                    Shippingair::create($req_data_shipping);
                                } else {
                                    $shippingAirCheck->update($req_data_shipping);
                                }
                            }
                        }
                        $warehouses = $request->warehouses;
                        if (!empty($warehouses)) {
                            ShipToWarehouse::where('shipping_id', $shipping_id)->delete();
                            foreach ($warehouses as $warehouse) {
                                $req_data_warehouse = [
                                    'shipping_id' => $shipping_id,
                                    'warehouse_value' => $warehouse
                                ];
                                ShipToWarehouse::create($req_data_warehouse);
                            }
                        }
                    }
                }
            }
            if (!empty($shipping_id)) {
                $res['shipping_id'] = $shipping_id;
                //$message = get_messages('Shipping created successfully', 1);
                $res['success'] = true;
                $res['error'] = 0;
                $res['message'] = "Shipping created successfully";
                $res['vendor_id'] = $request->vendor_id;
            } else {
                //$message = get_messages('Shipping updated successfully', 1);
                $res['shipping_id'] = $shipping_id;
                $res['success'] = true;
                $res['error'] = 0;
                $res['message'] = "Shipping updated successfully";
                $res['vendor_id'] = $request->vendor_id;
            }
            return response()->json($res);
        }
        if ($request->insert_type == 'insert_update_lead_time') {
            if(empty($request->vendor_id)){
                $res_arr['success'] = false;
                $res_arr['error'] = 1;
                $res_arr['message'] = 'Please fill up basic details';
                return response()->json($res_arr);
            }
            if(isset($request->total_lead_time_days_boat)){
                $validator = Validator::make($request->all(), [
                    'product_manuf_days_boat' => 'required',
                    'to_port_days_boat' => 'required',
                    'transit_time_days_boat'=>'required',
                    'to_warehouse_days_boat'=>'required',
                    'to_amazon_boat'=>'required',
                    'po_to_production_days_boat'=>'required',
                    'safety_days_boat'=>'required',
                    'total_lead_time_days_boat'=>'required'
                ]);
                if ($validator->fails()) {
                    $res['success'] = false;
                    $res['error'] = 1;
                    $res['message'] = 'Please provide required field data';
                    return response()->json($res);
                }
                else {
                    $product_manuf_days = isset($request->product_manuf_days_boat) ? $request->product_manuf_days_boat : 0;
                    $to_port_days = isset($request->to_port_days_boat) ? $request->to_port_days_boat : 0;
                    $transit_time_days = isset($request->transit_time_days_boat) ? $request->transit_time_days_boat : 0;
                    $to_warehouse_days = isset($request->to_warehouse_days_boat) ? $request->to_warehouse_days_boat : 0;
                    $to_amazon = isset($request->to_amazon_boat) ? $request->to_amazon_boat : 0;
                    $po_to_production_days = isset($request->po_to_production_days_boat) ? $request->po_to_production_days_boat : 0;
                    $safety_days = isset($request->safety_days_boat) ? $request->safety_days_boat : 0;
                    $total_lead_time_days = isset($request->total_lead_time_days_boat) ? $request->total_lead_time_days_boat : 0;
                    $order_prep_days = isset($request->order_prep_days_boat) ? $request->order_prep_days_boat : 0;
                    $po_to_prep_days = isset($request->po_to_prep_days_boat) ? $request->po_to_prep_days_boat : 0;
                    $type = "1";
                }
            }else if(isset($request->total_lead_time_days_plane)){
                $validator = Validator::make($request->all(), [
                    'product_manuf_days_plane' => 'required',
                    'to_port_days_plane' => 'required',
                    'transit_time_days_plane'=>'required',
                    'to_warehouse_days_plane'=>'required',
                    'to_amazon_plane'=>'required',
                    'po_to_production_days_plane'=>'required',
                    'safety_days_plane'=>'required',
                    'total_lead_time_days_plane'=>'required'
                ]);
                if ($validator->fails()) {
                    $res['success'] = false;
                    $res['error'] = 1;
                    $res['message'] = 'Please provide required field data';
                    return response()->json($res);
                }
                else {
                    $product_manuf_days = isset($request->product_manuf_days_plane) ? $request->product_manuf_days_plane : 0;
                    $to_port_days = isset($request->to_port_days_plane) ? $request->to_port_days_plane : 0;
                    $transit_time_days = isset($request->transit_time_days_plane) ? $request->transit_time_days_plane : 0;
                    $to_warehouse_days = isset($request->to_warehouse_days_plane) ? $request->to_warehouse_days_plane : 0;
                    $to_amazon = isset($request->to_amazon_plane) ? $request->to_amazon_plane : 0;
                    $po_to_production_days = isset($request->po_to_production_days_plane) ? $request->po_to_production_days_plane : 0;
                    $safety_days = isset($request->safety_days_plane) ? $request->safety_days_plane : 0;
                    $total_lead_time_days = isset($request->total_lead_time_days_plane) ? $request->total_lead_time_days_plane : 0;
                    $order_prep_days = isset($request->order_prep_days_plane) ? $request->order_prep_days_plane : 0;
                    $po_to_prep_days = isset($request->po_to_prep_days_plane) ? $request->po_to_prep_days_plane : 0;
                    $type = "2";
                }
            }else if(isset($request->total_lead_time_days)){
                $validator = Validator::make($request->all(), [
                    'order_prep_days' => 'required',
                    'to_warehouse_days'=>'required',
                    'to_amazon'=>'required',
                    'po_to_prep_days'=>'required',
                    'safety_days'=>'required',
                    'total_lead_time_days'=>'required'
                ]);
                if ($validator->fails()) {
                    $res['success'] = false;
                    $res['error'] = 1;
                    $res['message'] = 'Please provide required field data';
                    return response()->json($res);
                }
                else {
                    $product_manuf_days = isset($request->product_manuf_days) ? $request->product_manuf_days : 0;
                    $to_port_days = isset($request->to_port_days) ? $request->to_port_days : 0;
                    $transit_time_days = isset($request->transit_time_days) ? $request->transit_time_days : 0;
                    $to_warehouse_days = isset($request->to_warehouse_days) ? $request->to_warehouse_days : 0;
                    $to_amazon = isset($request->to_amazon) ? $request->to_amazon : 0;
                    $po_to_production_days = isset($request->po_to_production_days) ? $request->po_to_production_days : 0;
                    $safety_days = isset($request->safety_days) ? $request->safety_days : 0;
                    $total_lead_time_days = isset($request->total_lead_time_days) ? $request->total_lead_time_days : 0;
                    $order_prep_days = isset($request->order_prep_days) ? $request->order_prep_days : 0;
                    $po_to_prep_days = isset($request->po_to_prep_days) ? $request->po_to_prep_days : 0;
                    $type = "0";
                }
            }
            $req_data = [
                'supplier_vendor' => env('VENDOR'),
                'supplier_vendor_id' => $request->vendor_id,
                'product_manuf_days' => $product_manuf_days,
                'to_port_days' => $to_port_days,
                'transit_time_days' => $transit_time_days,
                'to_warehouse_days' => $to_warehouse_days,
                'to_amazon' => $to_amazon,
                'po_to_production_days' => $po_to_production_days,
                'safety_days' => $safety_days,
                'total_lead_time_days' => $total_lead_time_days,
                'order_prep_days' => $order_prep_days,
                'po_to_prep_days' => $po_to_prep_days,
                'type' => $type
            ];

          
            $check_exists = Lead_time::where(array('supplier_vendor' => env('VENDOR'), 'supplier_vendor_id' => $request->vendor_id))->first();
            if (!empty($check_exists)) {
                Lead_time::where(array('id' => $check_exists->id))->update($req_data);
                //$message = get_messages('vendor lead time updated successfully', 1);
                $res['vendor_id'] = $request->vendor_id;
                $res['error'] = 0;
                $res['success'] = true;
                $res['message'] = "vendor lead time updated successfully";
                $res['lead_time_id'] = $check_exists->id;
            } else {
                $lead_data = Lead_time::create($req_data);
                //$message = get_messages('vendor lead time created successfully', 1);
                $res['vendor_id'] = $request->vendor_id;
                $res['error'] = 0;
                $res['success'] = true;
                $res['message'] = "vendor lead time created successfully";
                $res['lead_time_id'] = $lead_data->id;
            }

           

            if ($res['lead_time_id'] > 0) {
                Lead_time_value::where(array('lead_time_id' => $res['lead_time_id']))->delete();
                    if($type==1 || $type==2){
                        if(isset($request->emailing['production'])){
                            foreach ($request->emailing['production'] as $key=>$production){
                                $lead_time_value_data = [
                                    'lead_time_id' => $res['lead_time_id'],
                                    'lead_time_detail_id' => $key+1,
                                    'send_mail' => '1',
                                    'no_of_days' => @$production['days'],
                                    'contact_detail' => (isset($production['email'])) && is_array($production['email']) ? implode(",", array_unique(@$production['email'])) : '',
                                ];
                                //Lead_time_value::create($lead_time_value_data);
                                if(is_array(@$production['email'])){
                                    Lead_time_value::create($lead_time_value_data);
                                }
                            }
                        }
                        if(isset($request->emailing['order_prep'])){
                            foreach ($request->emailing['order_prep'] as $key=>$production){
                                $lead_time_value_data = [
                                    'lead_time_id' => $res['lead_time_id'],
                                    'lead_time_detail_id' => $key+1,
                                    'send_mail' => '1',
                                    'no_of_days' => @$production['days'],
                                    'contact_detail' => (isset($production['email'])) && is_array($production['email']) ? implode(",", array_unique(@$production['email'])) : '',
                                ];
                                //Lead_time_value::create($lead_time_value_data);
                                if(is_array(@$production['email'])){
                                    Lead_time_value::create($lead_time_value_data);
                                }
                            }
                        }
                        if(isset($request->emailing['port_departure'])){
                            foreach ($request->emailing['port_departure'] as $key=>$port_departure){
                                $lead_time_value_data = [
                                    'lead_time_id' => $res['lead_time_id'],
                                    'lead_time_detail_id' => $key+3,
                                    'send_mail' => '1',
                                    'no_of_days' => @$port_departure['days'],
                                    'contact_detail' => (isset($port_departure['email'])) && is_array($port_departure['email']) ? implode(",", array_unique(@$port_departure['email'])) : '',
                                ];
                                if(is_array(@$port_departure['email'])){
                                    Lead_time_value::create($lead_time_value_data);
                                }
                                //Lead_time_value::create($lead_time_value_data);
                            }
                        }
                        if(isset($request->emailing['in_transit'])){
                            foreach ($request->emailing['in_transit'] as $key=>$in_transit){
                                $lead_time_value_data = [
                                    'lead_time_id' => $res['lead_time_id'],
                                    'lead_time_detail_id' => $key+4,
                                    'send_mail' => '1',
                                    'no_of_days' => @$in_transit['days'],
                                    'contact_detail' => (isset($in_transit['email'])) && is_array($in_transit['email']) ? implode(",", array_unique(@$in_transit['email'])) : '',
                                ];
                                if(is_array(@$in_transit['email'])){
                                    Lead_time_value::create($lead_time_value_data);
                                }
                                //Lead_time_value::create($lead_time_value_data);
                            }
                        }
                        if(isset($request->emailing['to_warehouse'])){
                            foreach ($request->emailing['to_warehouse'] as $key=>$to_warehouse){
                                $lead_time_value_data = [
                                    'lead_time_id' => $res['lead_time_id'],
                                    'lead_time_detail_id' => $key+5,
                                    'send_mail' => '1',
                                    'no_of_days' => @$to_warehouse['days'],
                                    'contact_detail' => (isset($to_warehouse['email'])) && is_array($to_warehouse['email']) ? implode(",", array_unique(@$to_warehouse['email'])) : '',
                                ];
                                if(is_array(@$to_warehouse['email'])){
                                    Lead_time_value::create($lead_time_value_data);
                                }
                                //Lead_time_value::create($lead_time_value_data);
                            }
                        }
                    }
                    else{
                        if(isset($request->emailing['order_prep'])) {
                            foreach ($request->emailing['order_prep'] as $key => $order_prep) {
                                $lead_time_value_data = [
                                    'lead_time_id' => $res['lead_time_id'],
                                    'lead_time_detail_id' => $key + 8,
                                    'send_mail' => '1',
                                    'no_of_days' => @$order_prep['days'],
                                    'contact_detail' => (isset($order_prep['email'])) && is_array($order_prep['email']) ?  implode(",", array_unique(@$order_prep['email'])) : '',
                                ];
                                if(is_array(@$order_prep['email'])){
                                    Lead_time_value::create($lead_time_value_data);
                                }
                                //Lead_time_value::create($lead_time_value_data);
                            }
                        }
                        if(isset($request->emailing['ship_confirmation'])){
                            foreach ($request->emailing['ship_confirmation'] as $key=>$ship_confirm){
                                $lead_time_value_data = [
                                    'lead_time_id' => $res['lead_time_id'],
                                    'lead_time_detail_id' => $key+9,
                                    'send_mail' => '1',
                                    'no_of_days' => @$ship_confirm['days'],
                                    'contact_detail' => (isset($ship_confirm['email'])) && is_array($ship_confirm['email']) ?  implode(",", array_unique(@$ship_confirm['email'])) : '',
                                ];
                                if(is_array(@$ship_confirm['email'])){
                                    Lead_time_value::create($lead_time_value_data);
                                }
                                //Lead_time_value::create($lead_time_value_data);
                            }
                        }
                        if(isset($request->emailing['to_warehouse'])){
                            foreach ($request->emailing['to_warehouse'] as $key=>$to_warehouse){
                                $lead_time_value_data = [
                                    'lead_time_id' => $res['lead_time_id'],
                                    'lead_time_detail_id' => $key+10,
                                    'send_mail' => '1',
                                    'no_of_days' => @$to_warehouse['days'],
                                    'contact_detail' => (isset($to_warehouse['email'])) && is_array($to_warehouse['email']) ? implode(",", array_unique(@$to_warehouse['email'])) : '',
                                ];
                                if(is_array(@$to_warehouse['email'])){
                                    Lead_time_value::create($lead_time_value_data);
                                }
                                //Lead_time_value::create($lead_time_value_data);
                            }
                        }
                    }
                /*if (!empty($request->arr_test)) {
                    foreach ($request->arr_test as $key => $data) {
                        if ($data['class'] == 'boat_production_mail_1') $class_id = '1';
                        if ($data['class'] == 'boat_production_mail_2') $class_id = '2';
                        if ($data['class'] == 'boat_port_departure') $class_id = '3';
                        if ($data['class'] == 'boat_in_transist') $class_id = '4';
                        if ($data['class'] == 'boat_to_warehouse_1') $class_id = '5';
                        if ($data['class'] == 'boat_to_warehouse_2') $class_id = '6';
                        if ($data['class'] == 'boat_to_warehouse_3') $class_id = '7';

                        if ($data['class'] == 'plane_production_mail_1') $class_id = '1';
                        if ($data['class'] == 'plane_production_mail_2') $class_id = '2';
                        if ($data['class'] == 'plane_port_departure') $class_id = '3';
                        if ($data['class'] == 'plane_in_transist') $class_id = '4';
                        if ($data['class'] == 'plane_to_warehouse_1') $class_id = '5';
                        if ($data['class'] == 'plane_to_warehouse_2') $class_id = '6';
                        if ($data['class'] == 'plane_to_warehouse_3') $class_id = '7';

                        if ($data['class'] == 'demostic_order_prep') $class_id = '8';
                        if ($data['class'] == 'domestic_ship_confirmation') $class_id = '9';
                        if ($data['class'] == 'domestic_to_warehouse') $class_id = '10';

                        if(isset($data['contact']) && !empty($data['contact'])){
                            $req_data = [
                                'lead_time_id' => $res['lead_time_id'],
                                'lead_time_detail_id' => $class_id,
                                'send_mail' => '1',
                                'no_of_days' => @$data['days'],
                                'contact_detail' => implode(",", $data['contact']),
                            ];
                        }
                        Lead_time_value::create($req_data);
                    }
                }*/

            }
            return response()->json($res);
        }
        if ($request->insert_type == 'insert_update_order_setting') {

          
           //$get_vendors_lead_time = Lead_time::where(array(''));
            if(empty($request->vendor_id)){
                $res_arr['success'] = false;
                $res_arr['error'] = 1;
                $res_arr['message'] = 'Please fill up basic details';
                return response()->json($res_arr);
            }
            $message = get_messages('Something wrong', 0);
            $res['success'] = false;
            $res['message'] = $message;
            $res['vendor_id'] = $request->vendor_id;

            if($request->reorder_schedule == 'on_demand'){
                $validator = Validator::make($request->all(), [
                    'order_volume' => 'required',
                    'order_volume_type'=>'required'
                ]);
            }else{
                $validator = Validator::make($request->all(), [
                    'order_volume' => 'required',
                    'order_volume_type'=>'required',
                    'days'=>'required'
                ]);
            }
            //$messages = $validator->errors();

            if ($validator->fails()) {
                $res['success'] = false;
                $res['error'] = 1;
                $res['message'] = 'Please provide required field data';
                return response()->json($res);
            }
            else {
                $typeArray = ['on_demand'=>0 , 'weekly' => 1, 'bi_weekly' => 2, 'monthly' => 3, 'bi_monthly' => 4];

                //$check_exists = Lead_time::where(array('id' => $request->lead_time_id))->first();
                $check_exists = Lead_time::where(array('supplier_vendor' => env('VENDOR'), 'supplier_vendor_id' => $request->vendor_id))->first();
                if (!empty($check_exists)) {
                    $req_data = [
                        'order_volume_value' => $request->order_volume,
                        'order_volume_id' => $request->order_volume_type
                    ];
                    Lead_time::where(array('id' => $check_exists->id))->update($req_data);
                    //$message = get_messages('vendor order setting updated successfully', 1);
                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = "vendor order setting updated successfully";
                    $res['lead_time_id'] = $check_exists->id;
                    $res['order_volume'] = $request->order_volume;
                    $res['vendor_id'] = $request->vendor_id;
                } else {
                    $res['success'] = false;
                    $res['error'] = 1;
                    $res['message'] = 'Please first complete lead time details';
                    return response()->json($res);

                    $vendor = Vendor::where('id', $request->vendor_id)->first();
                    $req_data = [
                        'supplier_vendor' => env('VENDOR'),
                        'supplier_vendor_id' => $request->vendor_id,
                        'type' => ($vendor['vendor_type'] == 1) ? 0 : 1,
                        'order_volume_value' => $request->order_volume,
                        'order_volume_id' => $request->order_volume_type
                    ];
                   
                    $lead_data = Lead_time::create($req_data);
                    //$message = get_messages('vendor order setting created successfully', 1);

                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = "vendor order setting created successfully";
                    $res['lead_time_id'] = $lead_data->id;
                    $res['vendor_id'] = $request->vendor_id;
                }
               
                if (!empty($request->days)) {
                    Reorder_schedule_detail::where(array('supplier_vendor' => env('VENDOR'), 'supplier_vendor_id' => $request->vendor_id))->delete();
                    foreach ($request->days as $schedule_type) {
                        if (is_numeric($schedule_type)) {
                            $values_d = $schedule_type;
                        } else {
                            if ($schedule_type == 'Sun') $values_d = 1;
                            if ($schedule_type == 'Mon') $values_d = 2;
                            if ($schedule_type == 'Tue') $values_d = 3;
                            if ($schedule_type == 'Wed') $values_d = 4;
                            if ($schedule_type == 'Thu') $values_d = 5;
                            if ($schedule_type == 'Fri') $values_d = 6;
                            if ($schedule_type == 'Sat') $values_d = 7;
                        }
                        $reorder_schedule = ['supplier_vendor' => env('VENDOR'), 'supplier_vendor_id' => $request->vendor_id, 'reorder_schedule_type_id' => $typeArray[$request->reorder_schedule], 'schedule_days' => $values_d];
                        $reorder_schedule_data = Reorder_schedule_detail::create($reorder_schedule);
                    }
                }else{
                    Reorder_schedule_detail::where(array('supplier_vendor' => env('VENDOR'), 'supplier_vendor_id' => $request->vendor_id))->delete();
                    $reorder_schedule = ['supplier_vendor' => env('VENDOR'), 'supplier_vendor_id' => $request->vendor_id, 'reorder_schedule_type_id' => $typeArray[$request->reorder_schedule]];
                    $reorder_schedule_data = Reorder_schedule_detail::create($reorder_schedule);
                }
            }
            return response()->json($res);
        }
    }

    function displayDates($date1, $date2, $format = 'Y-m-d')
    {
        $dates = array();
        $current = strtotime($date1);
        $date2 = strtotime($date2);
        $stepVal = '+1 day';
        while ($current <= $date2) {
            $dates[] = date($format, $current);
            $current = strtotime($stepVal, $current);
        }
        return $dates;
    }

    /**
     * if id is not black then download structure of upload the vendors otherwise vendors view details show
     *
     * @method POST
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        if ($id == 'import') {

            $import = new VendorExport();
            return Excel::download($import, 'vendor.xlsx');

            // $file_name = 'vendor.xlsx';
            // $file = public_path().'/sheets/'.$file_name;
            // //$file= public_path(). "/download/info.pdf";

            // $headers = array(
            //         'Content-Type: application/csv',
            //         );

            // return Response::download($file, $file_name, $headers);

            // $filename = 'vendors.csv';
            // $headers = array(
            //     "Content-type" => "text/csv",
            //     "Content-Disposition" => "attachment; filename=" . $filename . " ",
            //     "Pragma" => "no-cache",
            //     "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            //     "Expires" => "0"
            // );

            // $columns = array('vendorname', 'Firstname', 'Lastname', 'Email', 'Address line 1', 'Address line 2 [Optional]', 'City', 'State', 'Zipcode', 'Country');
            // $fileputcsv = array('Test', 'Test', 'Test', 'demo@gmail.com', 'Address line 1', 'Address line 2', 'AAA', 'AA', '123456', 'US');

            // $callback = function () use ($columns, $fileputcsv) {
            //     ob_clean();
            //     $file = fopen('php://output', 'w+');
            //     fputcsv($file, $columns);
            //     fputcsv($file, $fileputcsv);
            //     fclose($file);
            // };
            // return Response::stream($callback, 200, $headers);
        } else {
            $data = Vendor::with(['vendor_contact', 'vendor_product', 'vendor_blackout_date', 'vendor_wise_setting'])->where(array('id' => $_GET['id']))->get()->toArray();
            return view('vendorsfinal.detail', [
                'vendorDetail' => $data
            ]);
        }
    }

    /**
     * Show the form for editing the specified vendor
     * @method GET
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $get_checks = get_access('vendor_module', 'edit');
        $get_user_access = get_user_check_access('vendor_module', 'edit');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }
        $global_marketplace = session('MARKETPLACE_ID');
        $vendors_data = Vendor::where(array('id' => $id))->get()->first();
        $primary_vendors = Vendor_contact::where(array('vendor_id' => $id, 'primary' => 1))->get()->first();
        $vendors_contact = Vendor_contact::where(array('vendor_id' => $id))->get()->toArray();
        $setting = vendor_wise_default_setting::where(array('vendor_id' => $id))->first();
        $shippingDetails = Shipping::where('supplier_vendor', env('VENDOR'))->where('supplier_vendor_id', $id)->get()->first();
        $shippingAgents = ShippingAgent::where(array('user_marketplace_id' => $global_marketplace))->get()->toArray();
        $shippingCarrier = Shippingcarrier::where(array('user_marketplace_id' => $global_marketplace))->get()->toArray();
        $containerDetails = $shiptoAirDetails = $shiptoWarehouseDetails = $shipDeliveryDateDetails = $shipCutOffDeliveryDetails = '';
        if (!empty($shippingDetails)) {
            $containerDetails = ShippingContainerSettings::where('shipping_id', $shippingDetails->id)->get()->toArray();
            $shiptoAirDetails = Shippingair::where('shipping_id', $shippingDetails->id)->get()->toArray();
            $shiptoWarehouseDetails = ShipToWarehouse::where('shipping_id', $shippingDetails->id)->get()->toArray();
            $shipDeliveryDateDetails = ShippingDeliveryDetails::where('shipping_id', $shippingDetails->id)->get()->toArray();
            $shipCutOffDeliveryDetails = Shippingcutoffdeliverytime::where('shipping_id', $shippingDetails->id)->select('cut_off_time','cut_off_day')->get()->toArray();
        }

        $get_lead_time = Lead_time::where('supplier_vendor', env('VENDOR'))->where('supplier_vendor_id', $id)->first();
        $lead_time_value = array();
        if (!empty($get_lead_time)) {
            $lead_time_value = Lead_time_value::where(array('lead_time_id' => $get_lead_time->id))->get()->toArray();
        }
        $country = get_country_list();
        $cid = null;
        $state_id = null;

        if (isset($vendors_data->country_id) && $vendors_data->country_id != null) {
            $cid = $vendors_data->country_id;
        }
        if (isset($vendors_data->state) && $vendors_data->state != "" && (isset($vendors_data->country_id)) && $vendors_data->country_id != "") {
            $state_id = $vendors_data->state;
        }

        $state_list = get_state_list($cid);
        $city_list = get_city_list($state_id);
        $warehouse = Warehouse::where(array('user_marketplace_id' => $global_marketplace))->get()->toArray();
        $blackout_dates = Vendor_blackout_date::where(array('vendor_id' => $id))->get()->toArray();
        //$blackout_dates = vendor_blackout_date::where(array('vendor_id' => $id))->selectRaw('*,GROUP_CONCAT(id) as ids,max(blackout_date) as max_date,min(blackout_date) as min_date')->groupBy('reason')->get()->toArray();
        $lead_time_type = Lead_time_type::get()->toArray();
        $lead_time_check = Lead_time_check::get()->toArray();
        $order_volume_type = Order_volume_type::get()->toArray();
        $reorder_schedule_type = Reorder_schedule_type::get()->toArray();
        $schedule_details = Reorder_schedule_detail::where('supplier_vendor', env('VENDOR'))->where('supplier_vendor_id', $id)->selectRaw('supplier_vendor,reorder_schedule_type_id,supplier_vendor_id,GROUP_CONCAT(schedule_days) as days')->first();
        $payment_term = Payment_terms_type::get()->toArray();
        $get_payment_id = Payment::where(array('supplier_vendor' => env('VENDOR'), 'supplier_vendor_id' => $id))->first();
        $get_deposit = array();
        $get_balance = array();
        if (!empty($get_payment_id)) {
            $get_deposit = Payment_details::where(array('payment_id' => $get_payment_id->id, 'payment_terms' => '1'))->first();
            $get_balance = Payment_details::where(array('payment_id' => $get_payment_id->id, 'payment_terms' => '2'))->first();
        }
        
        $shipping_delivery_schedule = $shipping_cutoff_details = array();
        if(!empty($shipDeliveryDateDetails)) {
            $shipping_delivery_schedule['type'] = $shipDeliveryDateDetails[0]['delivery_schedule_type'];
            $shipping_delivery_schedule['days'] = array_column($shipDeliveryDateDetails, 'schedule_day');
        }
        if(!empty($shipCutOffDeliveryDetails)){
            $shippingCutOffDetail['cut_off_time'] = array_column($shipCutOffDeliveryDetails, 'cut_off_time');
            $shippingCutOffDetail['cut_off_day'] = array_column($shipCutOffDeliveryDetails, 'cut_off_day');
            $shipping_cutoff_details = array_combine($shippingCutOffDetail['cut_off_day'],$shippingCutOffDetail['cut_off_time']);
        }
        return view('vendorsfinal.edit-vendors', [
                'shiptoWarehouseDetails' => $shiptoWarehouseDetails,
                'container_details' => $containerDetails,
                'ship_to_air_details' => $shiptoAirDetails,
                'ship_deliver_date' => $shipDeliveryDateDetails,
                'shipping_delivery_schedule' => $shipping_delivery_schedule,
                'ship_cut_off_delivery_time' => $shipCutOffDeliveryDetails,
                'shipping_cutoff_details' => $shipping_cutoff_details,
                'vendors' => $vendors_data,
                'primary_vendors' => $primary_vendors,
                'vendors_contact' => $vendors_contact,
                'country_list' => $country,
                'state_list' => $state_list,
                'city_list' => $city_list,
                'setting' => $setting,
                'warehouse' => $warehouse,
                'blackout_dates' => $blackout_dates,
                'shipping_agents' => $shippingAgents,
                'shipping_carrierss' => $shippingCarrier,
                'shippingDetails' => $shippingDetails,
                'payment_term' => $payment_term,
                'deposit' => $get_deposit,
                'balance' => $get_balance,
                'leadtime' => $get_lead_time,
                'lead_time_type' => $lead_time_type,
                'lead_time_check' => $lead_time_check,
                'order_volume_type' => $order_volume_type,
                'reorder_schedule_type' => $reorder_schedule_type,
                'schedule_details' => $schedule_details,
                'lead_time_value' => $lead_time_value
            ]
        );
    }

    /**
     *
     * There are 2 function added in update method :
     * @method PUT
     *
     *  Request Types:
     *
     * 1. edit_form : Update the specified vendor in storage
     * @param \Illuminate\Http\Response $request
     * @param \Illuminate\Http\Response $request
     * @return \Illuminate\Http\Response
     *
     * 2. setting_form: update the default vendor setting
     * @return \Illuminate\Http\Response
     *
     */


    public function update(Request $request, $id)
    {
        $inputs = $request->all();
        $get_checks = get_access('vendor_module', 'edit');
        $get_user_access = get_user_check_access('vendor_module', 'edit');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }

        if (isset($request->insert_type) && $request->insert_type == 'edit_form') {
            $firstname = !empty($request->firstname) ? array_filter($request->firstname) : array();
            $lastname = !empty($request->lastname) ? array_filter($request->lastname) : array();
            $email = !empty($request->email) ? array_filter($request->email) : array();

            if (!empty($check_emails)) {
                $message = get_messages('Email already exists!', 0);
                Session::flash('message', $message);
                return redirect()->route('vendors.index');
            }

            $req_data = [
                'user_marketplace_id' => $request->marketplace_id,
                'vendor_type' => $request->vendor_type,
                'vendor_name' => $request->vendor_name,
                'address_line_1' => $request->address_line1,
                'address_line_2' => $request->address_line2,
                'city' => $request->city,
                'state' => $request->states,
                'zipcode' => $request->zipcode,
                'country_id' => $request->country
            ];

            if ($id != '') {

                $vendors = Vendor::findOrFail($id);
                $vendors->update($req_data);

                if (!empty($firstname) && !empty($lastname) && !empty($email)) {

                    $check_email = Vendor_contact::whereIn('email', $request['email'])->get()->toArray();

                    if (!empty($check_email)) {
                        $message = get_messages('Email already exists!', 0);
                        Session::flash('message', $message);
                        return redirect()->route('vendors.index');
                    }
                    foreach ($firstname as $key => $value) {
                        if ($value == '' && $lastname[$key] == '' && $email[$key] == '') {
                        } else {
                            $sub_data = [
                                'vendor_id' => $id,
                                'first_name' => $value,
                                'last_name' => @$lastname[$key],
                                'email' => @$email[$key],
                                'primary' => 0,
                            ];
                            $datas_of = Vendor_contact::create($sub_data);
                        }
                    }
                }
                $message = get_messages('vendors updated successfully', 1);
                Session::flash('message', $message);
                return redirect()->route('vendors.index');
            } else {
                $message = get_messages('Failed to update vendor data', 0);
                Session::flash('message', $message);
                return redirect()->route('vendors.index');
            }
        } else if (isset($request->insert_type) && $request->insert_type == 'setting_form') {
            if ($id != '') {
                $req_data = [
                    'vendor_id' => $id,
                    'lead_time' => $request->lead_time,
                    'order_volume' => $request->order_volume,
                    'quantity_discount' => $request->quantity_discount,
                    'moq' => $request->moq,
                    'CBM_Per_Container' => $request->CBM_Per_Container,
                    'Production_Time' => $request->Production_Time,
                    'Boat_To_Port' => $request->Boat_To_Port,
                    'Port_To_Warehouse' => $request->Port_To_Warehouse,
                    'Warehouse_Receipt' => $request->Warehouse_Receipt != '' ? $request->Warehouse_Receipt : 0,
                    'Ship_To_Specific_Warehouse' => $request->Ship_To_Specific_Warehouse
                ];
                $vendor_setting = vendor_wise_default_setting::where('vendor_id', $id)->first();
                if (!empty($vendor_setting)) {
                    $vendor_setting->update($req_data);
                    $message = get_messages('vendor setting updated successfully', 1);
                    Session::flash('message', $message);
                    return redirect()->route('vendors.index');
                } else {
                    vendor_wise_default_setting::create($req_data);
                    $message = get_messages('vendor setting created successfully', 1);
                    Session::flash('message', $message);
                    return redirect()->route('vendors.index');
                }
            }
        } else {
            $message = get_messages('Something wrong', 0);
            Session::flash('message', $message);
            return redirect()->route('vendors.index');
        }
    }

    /**
     * Remove the specified vendor & it's contact from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $get_checks = get_access('vendor_module', 'delete');
        $get_user_access = get_user_check_access('vendor_module', 'delete');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }

        $vendors = Vendor::findOrFail($id);
        $vendors->delete();

        return json_encode(array('statusCode' => 200));

    }

    public function changeContact(Request $request)
    {
        $contact_id = $request->contact_id;
        $vendor_contact = Vendor_contact::where('id', $contact_id)->first();
        if (!empty($vendor_contact)) {
            $res['email'] = $vendor_contact['email'];
            $res['phone'] = $vendor_contact['phone_number'];
        } else {
            $res['email'] = '';
            $res['phone'] = '';
        }
        return response()->json($res);
    }

    public function ImportVendor()
    {
        $fileContent = file_get_contents(request()->file('file'));
        $enc = mb_detect_encoding($fileContent, mb_list_encodings(), true);
        \Config::set('excel.imports.csv.input_encoding', $enc);

        $collection= Excel::import(new VendorImport(),request()->file('file'),'UTF-8');
        // return redirect()->route('suppliers.index');
         return redirect('/vendor_import_message');
    }

    public function vendor_import_message()
    {
        $error_msg=((\Session::get('error_msg_array')) ? \Session::get('error_msg_array') : '');
        $success_msg=((\Session::get('success_message')) ? \Session::get('success_message') : '');
        
        $error_count=((\Session::get('error_count')) ? \Session::get('error_count') : '');
        $success_count=((\Session::get('success_count')) ? \Session::get('success_count') : '');
        $total_count=((\Session::get('total_count')) ? \Session::get('total_count') : '');
        if(!empty($error_msg)){
            return view('vendorsfinal.import_messages',compact('error_msg','success_msg','total_count','error_count','success_count'));
        }else{
            $message = get_messages("Total $total_count rows uploaded successfully", 1);
           \Session::flash('message', $message);
           return redirect()->route('vendors.index');
        }

        // $error_msg=((\Session::get('error_msg_array')) ? \Session::get('error_msg_array') : '');
        // $success_msg=((\Session::get('success_message')) ? \Session::get('success_message') : '');
       
        // return view('vendorsfinal.import_messages',compact('error_msg','success_msg'));
    }

    public function getData(){
        $global_marketplace = session('MARKETPLACE_ID');
        $vendors = Vendor::with('vendor_contact')->where(array('user_marketplace_id'=>$global_marketplace))->orderBy('id','desc')->get();
        $vendors->each(function($vendor){
            $check_return = 1;
            $vendors_contact = Vendor_contact::where(array('vendor_id' => $vendor['id']))->first();
            $vendors_blackoutdate = Vendor_blackout_date::where(array('vendor_id' => $vendor['id']))->first();
            $lead_time = Lead_time::where(array('supplier_vendor' => $vendor, 'supplier_vendor_id' => $vendor['id']))->first();
            $order_setting = Reorder_schedule_detail::where(array('supplier_vendor' => $vendor, 'supplier_vendor_id' => $vendor['id']))->first();
            $payment = Payment::where(array('supplier_vendor' => $vendor, 'supplier_vendor_id' => $vendor['id']))->first();
            $shipping = Shipping::where(array('supplier_vendor' => $vendor, 'supplier_vendor_id' => $vendor['id']))->first();
            if (empty($vendors_contact) || empty($vendors_blackoutdate) || empty($lead_time) || empty($order_setting) || empty($payment) || empty($shipping)) {
                $check_return = 0;
            }
            $vendor['completeFlag'] = $check_return;
            $vendor['sku'] = $vendor->vendor_product->count();
        });

        return $vendors;
    }

    public function bulkDelete(Request $request){
        $result = Vendor::whereKey($request->selected)->delete();
        return ['success' => $result];
    }

    public function bulkExport(){
        return response()->streamDownload(function(){
            echo Vendor::toCsv();
        }, 'vendors'. date("Ymdhis") .'.csv');
    }

    public function updateCols(){
        if(DB::table('cols')->where('user_id', auth()->id())->where('table_name', 'vendors')->get()->count()){
            DB::table('cols')
                ->where('user_id', auth()->id())
                ->where('table_name', 'vendors')
                ->update(['cols' => request()->cols]);
        }else{
            DB::table('cols')
                ->insert([
                    'user_id' => auth()->id(),
                    'table_name' => 'vendors',
                    'cols' => json_encode(request()->cols)
                ]);
        }
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Supplier_contact;
use App\Models\Supplier;
use Response;
use Illuminate\Support\Facades\Session;

class SuppliersContactController extends Controller
{
    /**
     * download the structure of the supplier contacts.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $filename = 'supplierscontacts.csv';
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=".$filename." ",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $columns = array('Firstname', 'Lastname', 'Email');
        $fileputcsv =  array('Test','Test','demo@gmail.com');

        $callback = function() use ( $columns,$fileputcsv)
        {
            ob_clean();
            $file = fopen('php://output', 'w+');
            fputcsv($file, $columns);
            fputcsv($file,$fileputcsv);
            fclose($file);
        };
        return Response::stream($callback, 200, $headers);
    }

    /**
     * Show the form for creating a new suppliers contact.
     * @method GET
     * @params no-params
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $supplier_id = $_GET['supplier_id'];
        return view('suppliers.contacts.create', [
            'supplier_id' => $supplier_id,
        ]);
    }

    /**
     * There are 3 function added in store method :
     *      @method POST
     *
     *  Request Types:
     *
     * 1. suppliers_contact_list : listing of suppliers contacts
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     * 2. create_form: create a new contact for suppliers in the form data
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     * 3. upload_csv: upload csv file for suppliers contacts
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
     if($request->request_type == 'suppliers_contact_list'){
         $get_suppliers_contact = Supplier_contact::where(array('supplier_id' =>$request->id))->get()->toArray();
          $suppliers_contact_data = array();
         if(!empty($get_suppliers_contact))
            {
                 foreach ($get_suppliers_contact as $key => $post)
                {
                    $action = '' ;
                    $nestedData['first_name'] =  $post['first_name'];
                    $nestedData['last_name'] =  $post['last_name'];
                    $nestedData['email'] =  $post['email'];
                    $nestedData['primary'] =  $post['primary']=='1' ? 'Yes' : 'No';
                    $action .= '<a href="'.route('supplierscontact.edit',$post['id']).'" class=" btn btn-info btn-sm btn-rounded waves-effect waves-light" ><i class="fas fa-edit" title="edit"></i></a>';
                    $action .=  '<button id="button" type="submit"  class=" btn btn-danger btn-sm btn-rounded waves-effect  waves-light sa-remove button" data-id='.$post['id'].'><i class="fas fa-trash-alt"></i></button>';
                    $nestedData['action'] =  $action ;
                    $suppliers_contact_data[] = $nestedData;               
                }    
             }
                
           echo json_encode($suppliers_contact_data); exit;


       }
   

        $userLastInsertId = $request->supplier_id;
        if($request->insert_type == 'create_form'){
            $userLastInsertId = $request->supplier_id;
            $firstname = array_filter($request->firstname);
            $lastname = array_filter($request->lastname);
            $email = array_filter($request->email);

            if(!empty($email)) {
                $emailList = implode(',', $email);
                $check_emails = Supplier_contact::whereIn('email', $request['email'])->get()->toArray();

                if (!empty($check_emails)) {
                    $message = get_messages('Email already exists!', 0);
                    Session::flash('message', $message);
                    return redirect('supplierscontact/'.$request->supplier_id);
                }
            }

            if($userLastInsertId != '') {
                if(!empty($firstname) && !empty($lastname) && !empty($email)) {
                    $get_vendors_contact = Supplier_contact::where(array('supplier_id' => $userLastInsertId,'primary'=>1))->get()->toArray();
                    $primary = !empty($get_vendors_contact) ? 0 : 1;
                    foreach ($firstname as $key => $value) {
                        if ($value == '' && $lastname[$key] == '' && $email[$key] == '') {
                        } else {
                            $sub_data = [
                                'supplier_id' =>$userLastInsertId,
                                'first_name' => $value,
                                'last_name' => @$lastname[$key],
                                'email' => @$email[$key],
                                'primary' => $primary,
                            ];
                            $datas_of = Supplier_contact::create($sub_data);
                        }
                    }
                }
                $message = get_messages('Suppliers contact added successfully', 1);
                Session::flash('message', $message);
                return redirect('supplierscontact/'.$request->supplier_id);
            }else{
                $message = get_messages('Failed to add suppliers contact  data', 0);
                Session::flash('message', $message);
                return redirect('supplierscontact/'.$request->supplier_id);
            }
        } else if($request->insert_type == 'upload_csv'){
            if ($request->hasFile('uploadFile')) {
                $emails_data = array();
                $size = $request->file('uploadFile')->getSize();
                $extension = $request->uploadFile->getClientOriginalExtension();
                if ($extension == "csv" && $size <= 100000) {
                    $filePath = $request->file('uploadFile')->getRealPath();
                    $file = fopen($filePath, "r");
                    $escapedHeader = [];
                    $detail = [];
                    $flag = 0;

                    while ($columns = fgetcsv($file)) {
                        $columnValue = [];
                        if ($flag == 0) {
                            if (trim($columns[0]) != 'Firstname' || trim($columns[1]) != 'Lastname' || trim($columns[2]) != 'Email') {
                                $message = get_messages('Please upload proper file format as provide in sample!', 0);
                                Session::flash('message', $message);
                                return redirect('supplierscontact/'.$request->supplier_id);
                            }
                        }
                        foreach ($columns as $key => &$value) {
                            if ($flag == 0) {
                                $lowerHeader = strtolower($value);
                                $escapedItems = preg_replace("/[^a-z]/", "", $lowerHeader);
                                array_push($escapedHeader, $escapedItems);
                            } else {
                                array_push($columnValue, $value);
                            }
                        }
                        $flag++;
                        if (!empty($columnValue))
                            $detail[] = array_combine($escapedHeader, $columnValue);
                    }
                    if(!empty($detail)){
                        $uploaded_file = $request->file('uploadFile');
                        $name = 'csv_' . time() . $uploaded_file->getClientOriginalName();
                        $uploaded_file->move(public_path('assets/images/csv/suppliers'), $name);
                        $images[] = $name;
                        $storefolder = 'assets/images/csv/suppliers/csv_' . time() . $uploaded_file->getClientOriginalName();
                    }
                    $i=0;
                    foreach ($detail as $data) {
                        $product = [];
                        if (strlen($data['firstname']) <= 0) {
                            $message = get_messages('firstname is empty.', 0);
                        } else if (strlen($data['lastname']) <= 0) {
                            $message = get_messages('lastname is empty.', 0);
                        } else if (strlen($data['email']) <= 0) {
                            $message = get_messages('email is empty.', 0);
                        } else {
                            $checkemail = Supplier_contact::where(array('email'=>$data['email']))->get()->toArray();
                            if(empty($checkemail)){
                                $get_primary = Supplier_contact::where(array('supplier_id'=>$request->supplier_id,'primary'=>1))->first();
                                $primary = !empty($get_primary) ? 0 : 1 ;
                                $req_datas = [
                                    'supplier_id' => $request->supplier_id,
                                    'first_name' => $data['firstname'],
                                    'last_name' => $data['lastname'],
                                    'email' => $data['email'],
                                    'primary' => $primary,
                                ];
                                $datas_of = Supplier_contact::create($req_datas);
                                 $message2 = "csv uploaded successfully !";
                                    Session::flash('message2', $message2);
                            }else{
                                $i++;
                                    $message3 = $i.' email already exists';
                                    Session::flash('message3', $message3);
                            }
                        }
                    }
                    
                    return redirect('supplierscontact/'.$request->supplier_id);
                }
            }else{
                $message = get_messages('CSV file must be required', 0);
                Session::flash('message', $message);
                return redirect('supplierscontact/'.$request->supplier_id);
            }
        }else{
            $message = get_messages('Something wrong', 0);
            Session::flash('message', $message);
            return redirect('supplierscontact/'.$request->supplier_id);
        }

    }

    /**
     * Display the specified suppliers contacts details.
     * @method POST
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id != '') {
            $get_suppliers = Supplier::where(array('id' => $id))->first();       
            return view('suppliers.contacts.index', [
                'supplier_id' => $id,
                'suppliers'=>$get_suppliers,
            ]);
        }else{
            $message = get_messages('Failed to get suppliers contact details', 0);
            Session::flash('message', $message);
            return redirect()->route('suppliers.index');
        }
    }

    /**
     * Show the form for editing the specified suppliers contact
     * @method GET
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $suppliers_data = Supplier_contact::where(array('id'=>$id))->get()->first();
        return view('suppliers.contacts.edit-suppliers', [
                'primary_suppliers' => $suppliers_data,
            ]
        );
    }


    /**
     * Update the specified contact in storage.
     * @method POST
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $inputs=$request->all(); 
        $firstname = !empty($request->firstname) ?  array_filter($request->firstname) : array();
        $lastname = !empty($request->lastname) ?  array_filter($request->lastname) : array();
        $email = !empty($request->email) ?  array_filter($request->email) : array();


            $emailList = implode(',', $email);
            $check_emails = Supplier_contact::where(array(['email','=',trim($inputs['email_edit'])],['id','!=',$id]))->get()->toArray();
            if (!empty($check_emails)) {
                $message = get_messages('Email already exists!', 0);
                Session::flash('message', $message);
                return redirect('supplierscontact/'.$request->supplier_id);
        }

        if($id != '') {

            $primary_supplier = $request->supplier_id;
            if($primary_supplier != '') {
                $req_datas = [
                    'first_name' => $request->firstname_edit,
                    'last_name' => $request->lastname_edit,
                    'email' => $request->email_edit,
                ];

                $suppliers = Supplier_contact::findOrFail($id);
                 $suppliers->update($req_datas);
            }


            if(!empty($firstname) && !empty($lastname) && !empty($email)) {
                $check_email = Supplier_contact::whereIn('email',$request['email'])->get()->toArray();
                
                 if (!empty($check_email)) {
                $message = get_messages('Email already exists!', 0);
                Session::flash('message', $message);
                return redirect('supplierscontact/'.$request->supplier_id);
                }
                foreach ($firstname as $key => $value) {
                    if ($value == '' && $lastname[$key] == '' && $email[$key] == '') {
                    } else {
                        $sub_data = [
                            'supplier_id' =>$request->supplier_id,
                            'first_name' => $value,
                            'last_name' => @$lastname[$key],
                            'email' => @$email[$key],
                            'primary' => 0,
                        ];
                        $datas_of = Supplier_contact::create($sub_data);
                    }
                }
            }
            $message = get_messages('Suppliers contact updated successfully', 1);
            Session::flash('message', $message);
            return redirect('supplierscontact/'.$request->supplier_id);
        }else{
            $message = get_messages('Failed to update suppliers contact data', 0);
            Session::flash('message', $message);
            return redirect('supplierscontact/'.$request->supplier_id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $suppliers = Supplier_contact::findOrFail($id);
        $suppliers->delete();
        return json_encode(array('statusCode'=>200));
    }

    

}

<?php

namespace App\Http\Controllers;

use App\Models\Default_logic_setting;
use App\Models\Mws_product;
use App\Models\Purchase_instance_details;
use App\Models\Purchase_instances;
use App\Models\Supplier;
use App\Models\Supplier_product;
use App\Models\Warehouse_product;
use App\Models\Product_assign_supply_logic;
use App\Models\Vendor;
use App\Models\Supply_reorder_logic;
use App\Models\Warehouse;
use App\Models\Daily_logic_Calculations;
use App\Models\Vendor_product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Models\Usermarketplace;
use App\Models\Temp_session;
use App\Models\Vendor_default_setting;
use App\Models\Vendor_wise_default_setting;
use App\Models\Supplier_wise_default_setting;
use App\Models\Application_modules;
use App\Models\Role_access_modules;
use App\Models\Users;
use Illuminate\Support\Facades\Gate;
use App\Models\Product_assign;

use App\Models\Vendor_contact;
use App\Models\Supplier_contact;

use App\Models\Vendor_blackout_date;
use App\Models\Supplier_blackout_date;
use App\Models\Lead_time;
use App\Models\Reorder_schedule_detail;
use App\Models\Payment;
use App\Models\Shipping;
use App\Models\Payment_details;
use App\Models\ShippingContainerSettings;
use App\Models\Shippingair;
use App\Models\ShipToWarehouse;
use App\Models\ShippingDeliveryDetails;
use App\Models\Shippingcutoffdeliverytime;

use App\Models\Product_dimensions;
use App\Models\Product_profit_analysis;

use App\Models\Product_lead_time;
use App\Models\Product_lead_time_value;
use App\Models\Product_reorder_schedule_detail;
use Validator;

use Carbon\Carbon;
use App\Models\Calculated_sales_qty;
use App\Models\Sales_total_trend_rate;
//use App\Models\Product_assign;
use App\Models\Mws_unsuppressed_inventory_data;
use App\Models\Purchase_orders;
use App\Models\Default_leadtime_setting;
use App\Models\Default_payment_setting;
use App\Models\Default_reorder_schedule;
use Illuminate\Support\Facades\Log;

use App\Models\Product_label;
use App\Models\Product_assign_to_label;
use App\Models\Mws_import_products;
use App\Models\Product_manage_mpn_qty;
use App\Models\Product_quantity_discounts;
use App\Models\Product_sub_quantity_discounts;

use App\Models\Product_custom_reorder_schedule_detail;
use App\Models\Product_custom_lead_time_table;

class MwsProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /*public function __construct()
    {
        $this->middleware('checkType');
    }*/

    Protected $userRolePermissionId;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {

            $moduleId = Application_modules::where('module', 'product_module')->first()->id;
            $userRoleID = Users::with('user_roles')->whereIn('id', [Auth::id()])->first()->user_roles[0]->id;
            $this->userRolePermissionId = Role_access_modules::where('role_id', $userRoleID)->where('application_module_id', $moduleId)->first();
            return $next($request);
        });
    }

    /**
     * Display a listing of the mws_product with it's vendors,suppliers & warehouse with marketplace wise.
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */
    public function oldproducttemplate()
    {
        $get_checks = get_access('product_module','view');
        $get_user_access = get_user_check_access('product_module','view');
        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }

        $global_marketplace = session('MARKETPLACE_ID');
        $data = array();
        $vendors = array();
        $suppliers = array();
        $marketplace = '';
        $logic_label_name = array();
        $total_marketplace = Usermarketplace::where(array('user_id'=>Auth::id()))->get();
        if(!empty($total_marketplace) && $global_marketplace != ''){
            $marketplace = $global_marketplace;
            $vendors = Vendor::where(array('user_marketplace_id'=>$global_marketplace))->get();
            $suppliers = Supplier::where(array('user_marketplace_id'=>$global_marketplace))->get();
            $logic_label_name=Supply_reorder_logic::where(array('user_marketplace_id'=>$global_marketplace))->get();
        }
        return view('product.index', ['marketplace'=>$marketplace,
                                        'marketplace_list'=>$total_marketplace,
                                        'vendors'=>$vendors,
                                        'logic_label_name'=>$logic_label_name,
                                        'suppliers'=>$suppliers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


    }

    /**
     * Display a listing of the product_sales_rate using daily_logic_calulation & supply_reorder_logic with product & marketplace wise
     * @method GET
     * @param  $product_id
     * @return \Illuminate\Http\Response
     */

    public  function products_sales_rate($id)
    {

      $product_sales_rate  = array();
        $global_marketplace = session('MARKETPLACE_ID');
       $product_sales_rate=Daily_logic_Calculations::with('usermarketplace','mws_product')->where(array('prod_id'=>$id))->get()->first();
        $data=Supply_reorder_logic::with('product_assing_supply_logic','usermarketplace')->whereHas('product_assing_supply_logic', function ($query ) use($id)  {$query->where(array('product_id'=>$id));})->first();

        if(empty($data)){
              $data = Default_logic_setting::where('user_marketplace_id',$global_marketplace)->first();
          }

        return view('product.salesrate', [
            'product_sales_rate'=> $product_sales_rate,
            'data' =>$data->toArray()
            ]);
    }

    /**
     * There are 7 function added in store method :
     *      @method POST
     *
     *  Request Types:
     *
     * 1. label_add_sales_calculation : Add supply logic to all products
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     * 2. selected_product_assign_supply_logic: Add supply logic to particular products
     *      @param  $product_id
     *      @return \Illuminate\Http\Response
     *
     * 3. Creates_form: create form to assign supply logic
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     * 4. Addtopurchaseinstance: Store the purchase instance details with product wise
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     * 5. product_filter: Filteration to all product with brand,vendors,suppliers & warehouse wise
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     * 6. warehose_form: warehouse assign to particular product
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     * 7. product_filter: assign vendors or suppliers to particular product
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     */
    public function store(Request $request)
    {
        $inputs = $request->all();

        if($request->insert_type == 'label_add_sales_calculation'){
            $supply_history=$inputs['supply_last_year_sales_percentage'];
            $supply_order = $inputs['supply_recent_last_7_day_sales_percentage'] +
                $inputs['supply_recent_last_14_day_sales_percentage'] +
                $inputs['supply_recent_last_30_day_sales_percentage'];
            $supply_trend_order =
                $inputs['supply_trends_year_over_year_historical_percentage'] +
                $inputs['supply_trends_year_over_year_current_percentage'] +
                $inputs['supply_trends_multi_month_trend_percentage'] +
                $inputs['supply_trends_30_over_30_days_percentage'] +
                $inputs['supply_trends_multi_week_trend_percentage'] +
                $inputs['supply_trends_7_over_7_days_percentage'] ;
            $reorder_history=$inputs['reorder_last_year_sales_percentage'];
            $reorder =  $inputs['reorder_recent_last_7_day_sales_percentage'] +
                $inputs['reorder_recent_last_14_day_sales_percentage'] +
                $inputs['reorder_recent_last_30_day_sales_percentage'];
            $reorder_trend =
                $inputs['reorder_trends_year_over_year_historical_percentage'] +
                $inputs['reorder_trends_year_over_year_current_percentage'] +
                $inputs['reorder_trends_multi_month_trend_percentage'] +
                $inputs['reorder_trends_30_over_30_days_percentage'] +
                $inputs['reorder_trends_multi_week_trend_percentage'] +
                $inputs['reorder_trends_7_over_7_days_percentage'];
            $stock=$inputs['reorder_safety_stock_percentage'] ;
            if(($supply_order == 100.00 || $supply_history <= 100.00 ) && ($reorder == 100.00 ||  $reorder_history <=100) && $supply_trend_order == 100.00 && $reorder_trend == 100.00  && $stock <=100 )
            {
                $check_lable_name = Supply_reorder_logic::where(array(['logic_label_name','=',trim($inputs['logic_label_name'])]))->get()->toArray();

                if(empty($check_lable_name)) {
                    $logic['user_marketplace_id'] = $inputs['user_marketplace_id'];
                    $logic['logic_label_name'] = $inputs['logic_label_name'];
                    $logic['supply_last_year_sales_percentage'] = $inputs['supply_last_year_sales_percentage'];
                    $logic['supply_recent_last_7_day_sales_percentage'] = $inputs['supply_recent_last_7_day_sales_percentage'];
                    $logic['supply_recent_last_14_day_sales_percentage'] = $inputs['supply_recent_last_14_day_sales_percentage'];
                    $logic['supply_recent_last_30_day_sales_percentage'] = $inputs['supply_recent_last_30_day_sales_percentage'];
                    $logic['supply_trends_year_over_year_historical_percentage'] = $inputs['supply_trends_year_over_year_historical_percentage'];
                    $logic['supply_trends_year_over_year_current_percentage'] = $inputs['supply_trends_year_over_year_current_percentage'];
                    $logic['supply_trends_multi_month_trend_percentage'] = $inputs['supply_trends_multi_month_trend_percentage'];
                    $logic['supply_trends_30_over_30_days_percentage'] = $inputs['supply_trends_30_over_30_days_percentage'];
                    $logic['supply_trends_multi_week_trend_percentage'] = $inputs['supply_trends_multi_week_trend_percentage'];
                    $logic['supply_trends_7_over_7_days_percentage'] = $inputs['supply_trends_7_over_7_days_percentage'];
                    $logic['reorder_last_year_sales_percentage'] = $inputs['reorder_last_year_sales_percentage'];
                    $logic['reorder_recent_last_7_day_sales_percentage'] = $inputs['reorder_recent_last_7_day_sales_percentage'];
                    $logic['reorder_recent_last_14_day_sales_percentage'] = $inputs['reorder_recent_last_14_day_sales_percentage'];
                    $logic['reorder_recent_last_30_day_sales_percentage'] = $inputs['reorder_recent_last_30_day_sales_percentage'];
                    $logic['reorder_trends_year_over_year_historical_percentage'] = $inputs['reorder_trends_year_over_year_historical_percentage'];
                    $logic['reorder_trends_year_over_year_current_percentage'] = $inputs['reorder_trends_year_over_year_current_percentage'];
                    $logic['reorder_trends_multi_month_trend_percentage'] = $inputs['reorder_trends_multi_month_trend_percentage'];
                    $logic['reorder_trends_30_over_30_days_percentage'] = $inputs['reorder_trends_30_over_30_days_percentage'];
                    $logic['reorder_trends_multi_week_trend_percentage'] = $inputs['reorder_trends_multi_week_trend_percentage'];
                    $logic['reorder_trends_7_over_7_days_percentage'] = $inputs['reorder_trends_7_over_7_days_percentage'];
                    $logic['reorder_safety_stock_percentage'] = $inputs['reorder_safety_stock_percentage'];

                    $insert_data = Supply_reorder_logic::create($logic);
                    $userLastInsertId = $insert_data->toArray();

                    if ($inputs['product_assign'] == 1){
                        $assign_products = [
                            'label_id' => $userLastInsertId['id'],
                            'product_id' => $inputs['product_id'],
                            'status' => 0
                        ];
                        $data_settings = Product_assign_supply_logic::create($assign_products);
                    }

                    $message = get_messages('Supply order logic added successfully', 1);
                    $res['error'] = 0;
                    $res['message'] = $message;
                }
                else {
                    $message = get_messages('Label name already exists!', 0);
                    $res['error'] = 0;
                    $res['message'] = $message;
                }
            }else{
                $message = get_messages('Supply order or reorder percentage must be 100 and at present the total of all the fields exceeds 100', 0);
                $res['error'] = 0;
                $res['message'] = $message;
            }
            return response()->json($res);
            exit;
        }

        if($request->insert_type == 'selected_product_assign_supply_logic'){
            $get_plabel_id = Product_assign_supply_logic::where(array('product_id'=>$inputs['product_id'],'status'=>0))->first();
            if(!empty($get_plabel_id)){
                $res['error']= 0;
                $res['label_id'] = $get_plabel_id->label_id;
            }else{
                $res['error'] = 1;
                $res['label_id'] = '';
            }
            return response()->json($res);
        }

        if($request->insert_type == 'Creates_form'){
            $check_already_assign_supply_logic = Product_assign_supply_logic::where(array('product_id'=>$inputs['pro_id']))->first();
            $data = [
                       'product_id' => $inputs['pro_id'],
                       'label_id'=>$inputs['supply_logic_id'],
                       'status'=>0
                   ];
            if(empty($check_already_assign_supply_logic)) {
                $datas_of = Product_assign_supply_logic::create($data);
            }
            else {
                $check_assign_data = Product_assign_supply_logic::where(array('product_id'=>$inputs['pro_id'],'status'=>0,'label_id'=>$inputs['supply_logic_id']))->first();
                if(empty($check_assign_data)) {
                    $update_status['status'] = 1;
                    $datas_of = Product_assign_supply_logic::where(array('product_id' => $inputs['pro_id']))->update($update_status);
                    $data = [
                        'product_id' => $inputs['pro_id'],
                        'label_id' => $inputs['supply_logic_id'],
                        'status' => 0
                    ];
                    $datas_of = Product_assign_supply_logic::create($data);
                }else{
                    $datas_of = 1;
                }
            }
            if($datas_of){
                $res['error'] = 0;
                $res['message'] = get_messages('Product supply logic assigned ', 1);
            }else{
                $res['error'] = 1;
                $res['message'] = get_messages('Failed to assign product supply logic ',0);
            }
            return response()->json($res);
         }

        if($request->insert_type == 'Addtopurchaseinstance'){
            $global_marketplace = session('MARKETPLACE_ID');
            $global_purchaseinstances = session('PURCHESINSTANCES_ID');
            $check_product_already_exist = check_product_already_exist($global_marketplace,$inputs['product_id']);
            if($check_product_already_exist == 1){
                $res['error'] = 1;
                $res['message'] = get_messages('Product instance already exists.', 0);
            }else{
                $vendors_id = 0;
                $suppliers_id = 0;
                $warehouse_id = 0;
                $projected_units = 0;
                $containers = 1;
                $items_per_cartoons = 0;
                $cbm1 = "";

                $check_vendors = get_product_vendors($inputs['product_id']);
                $check_suppliers = get_product_suppliers($inputs['product_id']);
                $check_warehouse = get_product_warehouse($inputs['product_id']);

                $check_projected_units=Daily_logic_Calculations::where(array('prod_id' =>$inputs['product_id']))->get()->toArray();
                if(!empty($check_projected_units)){
                    $projected_units = $check_projected_units[0]['projected_reorder_qty'];
                }

                if(!empty($check_vendors)){
                    $vendors_id = $check_vendors[0]['vendor_id'];
                    $cbm1 = $check_vendors[0]['CBM_Per_Container'];
                    $qty_per_carton = $check_vendors[0]['Qty_per_Carton'];
                    if($projected_units != "0"){ $items_per_cartoons = $projected_units / $qty_per_carton; }

                }if(!empty($check_suppliers)){
                    $suppliers_id = $check_suppliers[0]['supplier_id'];
                    $cbm1 = $check_suppliers[0]['CBM_Per_Container'];
                    $qty_per_carton = $check_suppliers[0]['Qty_per_Carton'];
                    if($projected_units != "0"){ $items_per_cartoons = $projected_units / $qty_per_carton; }

                }if(!empty($check_warehouse)){
                    $warehouse_id = $check_warehouse[0]['warehouse_id'];
                }

                if((!empty($check_vendors)) || (!empty($check_suppliers)) ){
                    if(!empty($check_vendors)){
                        $cbm = $cbm1;
                        if($cbm == "" && $cbm != null){
                            $getDefault = Vendor_wise_default_setting::where("vendor_id",$vendors_id)->first();
                            $cbm = $getDefault->cbm_per_container;
                        }
                    }else{
                        $cbm = $cbm1;
                        if($cbm == "" && $cbm != null){
                            $getDefault = Supplier_wise_default_setting::where("supplier_id",$suppliers_id)->first();
                            $cbm = $getDefault->CBM_Per_Container;
                        }
                    }
                }else{
                    $getDefault = Vendor_default_setting::first();
                    $cbm = $getDefault->cbm_per_container;
                }


                $get_check = Temp_session::where(array('product_id'=>$inputs['product_id'],'marketplace_id'=>session('MARKETPLACE_ID')))->first();
                $get_purchase_id = Purchase_instances::where(array('user_marketplace_id'=>$global_marketplace,'status'=>1))->first();
                if(!empty($global_purchaseinstances)){
                    $purchase_details = [
                       'purchesinstances_id' => $global_purchaseinstances,
                       'product_id' => $inputs['product_id'],
                       'supplier' => $suppliers_id,
                       'vendor' =>$vendors_id,
                       'warehouse' =>$warehouse_id,
                       'projected_units' => !empty($get_check) && $get_check->recalulated_qty != '' ? $get_check->recalulated_qty : $projected_units,
                       'items_per_cartoons' => $items_per_cartoons,
                       'containers' => $containers,
                       'cbm'=>$cbm,
                       'subtotal'=> $items_per_cartoons * $containers * $cbm * $projected_units,
                   ];
                    Purchase_instance_details::create($purchase_details);
                }else{

                    $product_instance = [
                        'user_marketplace_id'=>$global_marketplace,
                        'track_id'=> $this->gerate_track_id(),
                        'status'=>1
                    ];
                    $perchase = Purchase_instances::create($product_instance);
                    $purchaseinstanceid = $perchase->toArray();
                    $last_purchase_instace_id = $purchaseinstanceid['id'];

                    $purchase_details = [
                        'purchesinstances_id' => $last_purchase_instace_id,
                        'product_id' => $inputs['product_id'],
                        'supplier' => $suppliers_id,
                        'vendor' =>$vendors_id,
                        'warehouse' =>$warehouse_id,
                        'projected_units' => !empty($get_check) && $get_check->recalulated_qty != '' ? $get_check->recalulated_qty : $projected_units,
                        'items_per_cartoons' => $items_per_cartoons,
                        'containers' => $containers,
                        'cbm'=>$cbm,
                        'subtotal'=> $items_per_cartoons * $containers * $cbm * $projected_units,
                    ];
                    Purchase_instance_details::create($purchase_details);
                }

                $res['error'] = 0;
                $res['message'] = get_messages('Product Added to Purchase Instance', 1);

            }
            return response()->json($res);
            exit;
        }

        if($request->insert_type == 'product_filter'){
            $inputs = $request->all();
            $datas = array();
            $vendors_product = array();
            $suppliers_product = array();

            $vendors = Vendor::where(array('user_marketplace_id'=>$inputs['id']))->get();
            $suppliers = Supplier::where(array('user_marketplace_id'=>$inputs['id']))->get();

            if($inputs['vendors'] != ''){
                $vendors_product = Vendor_product::where(array('vendor_id'=>$inputs['vendors']))->selectRaw('GROUP_CONCAT(product_id) as product_ids')->get();
            }if($inputs['suppliers'] != ''){
                $suppliers_product = Supplier_product::where(array('supplier_id'=>$inputs['suppliers']))->selectRaw('GROUP_CONCAT(product_id) as product_ids')->get();
            }

            $data = Mws_product::join('mws_afn_inventory_data', 'mws_products.id', '=', 'mws_afn_inventory_data.product_id')
            ->join('daily_logic_calculations', 'mws_products.id', '=', 'daily_logic_calculations.prod_id','left')
                ->selectRaw('mws_products.id as id,
                            mws_products.prod_image as prod_image,
                            mws_products.prod_name as prod_name,
                            mws_products.your_price as your_price,
                            mws_products.sku as sku,
                            mws_products.asin as asin,
                            mws_afn_inventory_data.quantity_available as quantity_available,
                            daily_logic_calculations.total_inventory,
                            daily_logic_calculations.id as logic_id'
                    )
                ->where(array('mws_afn_inventory_data.user_marketplace_id'=>$inputs['id'],'warehouse_condition_code'=>'SELLABLE'));

            if($inputs['brand_name']!=''){
                $data = $data->where('mws_products.brand', 'LIKE', "".$inputs['brand_name']."%") ;
            }if(!empty($vendors_product)){
                $vendorsids = $vendors_product[0]->product_ids != '' ? $vendors_product[0]->product_ids : 0 ;
                $data = $data->whereRaw("find_in_set(mws_products.id,'".$vendorsids."')");
            }if(!empty($suppliers_product) && !empty($vendors_product)){
                $suppliersids = $suppliers_product[0]->product_ids != '' ? $suppliers_product[0]->product_ids : 0 ;
                $data = $data->orWhereRaw("find_in_set(mws_products.id,'".$suppliersids."')");
            }if(!empty($suppliers_product) && empty($vendors_product)){
                $suppliersids = $suppliers_product[0]->product_ids != '' ? $suppliers_product[0]->product_ids : 0 ;
                $data = $data->whereRaw("find_in_set(mws_products.id,'".$suppliersids."')");
            }

            $posts = $data->orderBy('daily_logic_calculations.total_inventory','desc')->get();

            if(!empty($posts))
            {
                foreach ($posts as $key => $post)
                {
                    $nestedData['id'] = $key + 1;
                    $nestedData['prod_image'] = '<img width="100px" src="'.$post->prod_image.'" title="'.@$post->prod_name.'">';
                    $nestedData['prod_name'] = '<span title="'.@$post->prod_name.'">'.substr(strip_tags(@$post->prod_name),0,25)."...".'</span>';
                    $nestedData['your_price'] =  $post->your_price;
                    $nestedData['sku'] =  $post->sku;
                    $nestedData['asin'] =  '<a href=" https://www.amazon.com/gp/product/'.$post->asin.'" target="_blank" >'.$post->asin.'</a>';
                    $nestedData['quantity_available'] =  ($post->total_inventory != '') ? $post->total_inventory : 0;
                    $check_vendors = check_vendors($post->id);
                    $check_suppliers = check_suppliers($post->id);
                    $check_warehouse = check_warehouse($post->id);
                    if(empty($check_vendors) && empty($check_suppliers)){
                        $action = '<a href="'.route('products.show',$post->id).'" class="btn btn-info btn-sm btn-rounded waves-effect waves-light" title="Vendor/Supplier Assign"><i class="fas fa-user-circle"></i></a>';
                    }else{
                        $action = '<a href="'.route('products.edit',$post->id).'" class="btn btn-info btn-sm btn-rounded waves-effect waves-light" title="Update Vendor/Supplier"><i class="fas fa-user-circle"></i></a>';
                    }
                    if(empty($check_warehouse)){
                        $action .= '<a href="'.url('products_warehouse_assign/'.$post->id).'" type="submit" class="btn btn-info btn-sm btn-rounded waves-effect waves-light" title="Warehouse Assign"><i class="fas fa-building"></i></a>';
                    }else {
                        $action .= '<a href="'.url('products_warehouse_assign/'.$post->id).'" type="submit" class="btn btn-info btn-sm btn-rounded waves-effect waves-light" title="Update Warhouse"><i class="fas fa-building"></i></a>';
                    }
                    if(!empty($post->logic_id)) {
                        $action .= '<a href="javascript:void(0);" data-product_id='.$post->id.' type="submit" class="addtocart btn btn-info btn-sm btn-rounded waves-effect waves-light" title="Add to Purchase Instance"><i class="fas fa-shopping-cart"></i></a>';
                    }
                    $action .= '<a href="'.url('products_sales_rate/'.$post->id).'" data-product_id='.$post->id.' type="submit" class=" btn btn-info btn-sm btn-rounded waves-effect waves-light" title="Projected Reorder for product"><i class="fa fa-percent"></i></a>';
                    $action .= '<button type="button" class=" addtoid btn btn-info btn-sm btn-rounded waves-effect waves-light" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false" data-product_id='.$post->id.' title="Apply Order logic"><i class="fa fa-list"></i></button>';
                    $nestedData['action'] =  $action ;
                    $datas[] = $nestedData;
                }
            }
            echo json_encode($datas);
            exit;
        }
        if($request->insert_type == 'warehose_form')
        {
             $inputs = $request->all();
             $check_already_assign_warehose = Warehouse_product::where(array('product_id'=>$inputs['product_id']))->get()->first();
        $warehouse = [
                    'warehouse_id' => $inputs['warehouses_id'],
                    'product_id' => $inputs['product_id'],
                    'qty' => $inputs['qty'],
                    'lead_time' => $inputs['lead_time']

                ];
            if(empty($check_already_assign_warehose))
            {
                $datas_of = Warehouse_product::create($warehouse);
            }
            else
            {
                $userDetails = Warehouse_product::findOrFail($check_already_assign_warehose->id);
                $datas_of = $userDetails->update($warehouse);
            }
            if($datas_of)
            {
                $message = get_messages('Warehouse assign successfully!',1);
                Session::flash('message', $message);
                return redirect()->route('products.index');
            }
            else
            {
                $message = get_messages('Failed to assign product',0);
                Session::flash('message', $message);
                return redirect()->route('products.index');
            }

        }
        else if($request->insert_type != 'product_filter'){

            $check_already_assign_vendors = Vendor_product::where(array('product_id' => $inputs['product_id']))->get()->first();
            $check_already_assign_suppliers = Supplier_product::where(array('product_id' => $inputs['product_id']))->get()->first();

            if ($inputs['customRadio'] == 'vendors') {
                $vendors = [
                    'product_id' => $inputs['product_id'],
                    'vendor_id' => $inputs['vendors_id'],
                    'lead_time' => $inputs['lead_time'],
                    'order_volume' => $inputs['order_volume'],
                    'quantity_discount' => $inputs['quantity_discount'],
                    'moq' => $inputs['moq'],
                    'shipping' => $inputs['shipping'],
                    'Cost' => $inputs['Cost'],
                    'Cost_Expenses' => $inputs['Cost_Expenses'],
                    'Box_Domensions_Height' => $inputs['Box_Domensions_Height'],
                    'Box_Domensions_Width' => $inputs['Box_Domensions_Width'],
                    'Box_Domensions_Weight' => $inputs['Box_Domensions_Weight'],
                    'CBM_Per_Container' => $inputs['CBM_Per_Container'],
                    'Qty_per_Carton' => $inputs['Qty_per_Carton'],
                    'Cartons_per_pallet' => $inputs['Cartons_per_pallet'],
                    'Inventory_level' => $inputs['Inventory_level'],
                    'ship_to_warehouse' => $inputs['ship_to_warehouse'],
                ];

                if (empty($check_already_assign_vendors)) {
                    $datas_of = Vendor_product::create($vendors);
                } else {
                    $datas_of = Vendor_product::where(array('id' => $check_already_assign_vendors->id))->update($vendors);
                }
                if (!empty($check_already_assign_suppliers)) {
                    $userDetails = Supplier_product::findOrFail($check_already_assign_suppliers->id);
                    $datas_of = $userDetails->delete();
                }
            } else {
                $suppliers = [
                    'product_id' => $inputs['product_id'],
                    'supplier_id' => $inputs['suppliers_id'],
                    'lead_time' => $inputs['lead_times'],
                    'order_volume' => $inputs['order_volumes'],
                    'quantity_discount' => $inputs['quantity_discounts'],
                    'moq' => $inputs['moqs'],
                    'CBM_Per_Container' => $inputs['CBM_Per_Containers'],
                    'Production_Time' => $inputs['Production_Times'],
                    'Boat_To_Port' => $inputs['Boat_To_Ports'],
                    'Port_To_Warehouse' => $inputs['Port_To_Warehouses'],
                    'Cost' => $inputs['Costs'],
                    'Cost_Expenses' => $inputs['Cost_Expensess'],
                    'Box_Domensions_Height' => $inputs['Box_Domensions_Heights'],
                    'Box_Domensions_Width' => $inputs['Box_Domensions_Widths'],
                    'Box_Domensions_Weight' => $inputs['Box_Domensions_Weights'],
                    'Qty_per_Carton' => $inputs['Qty_per_Cartons'],
                    'Cartons_per_pallet' => $inputs['Cartons_per_pallets'],
                    'Inventory_level' => $inputs['Inventory_levels'],
                    'Warehouse_Receipt' => $inputs['Warehouse_Receipts'] != '' ? $inputs['Warehouse_Receipts'] : 0,
                    'Ship_To_Specific_Warehouse' => $inputs['Ship_To_Specific_Warehouses'] != '' ? $inputs['Ship_To_Specific_Warehouses'] : 0,
                ];

                if (empty($check_already_assign_suppliers)) {
                    $datas_of = Supplier_product::create($suppliers);
                } else {
                    $datas_of = Supplier_product::where(array('id' => $check_already_assign_suppliers->id))->update($suppliers);
                }
                if (!empty($check_already_assign_vendors)) {
                    $userDetails = Vendor_product::findOrFail($check_already_assign_vendors->id);
                    $datas_of = $userDetails->delete();
                }
            }

            if ($datas_of) {
                $message = get_messages('Product assign successfully!', 1);
                Session::flash('message', $message);
                return redirect()->route('products.index');
            } else {
                $message = get_messages('Failed to assign product', 0);
                Session::flash('message', $message);
                return redirect()->route('products.index');
            }
        }
 }

    /**
     * product calucation & it's store temp. session
     * @method POST
     * @param  \Illuminate\Http\Response $request
     * @return \Illuminate\Http\Response
     */

 public function product_calculation(Request $request){

        $product_id = $request->product_id;
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);
        $get_calculation = get_product_calculation($request);

        $data['user_id'] = Auth::id();
        $data['marketplace_id'] = session('MARKETPLACE_ID');
        $data['product_id'] = $product_id;
        $data['recalulated_qty'] = $get_calculation['projected_reorder_qty'];

        $session_temp = update_insert_temp_session($data);

        echo json_encode($get_calculation);
        exit;
 }

    /**
     * Display the particular product setting
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vendors  = array();
        $suppliers = array();
        $global_marketplace = session('MARKETPLACE_ID');
        $total_marketplace = Usermarketplace::where(array('user_id'=>Auth::id()))->get();
        if($global_marketplace != ''){
            $vendors = Vendor::where(array('user_marketplace_id'=>$global_marketplace))->get();
            $suppliers = Supplier::where(array('user_marketplace_id'=>$global_marketplace))->get();
            $warehouse = Warehouse::where(array('user_marketplace_id'=>$global_marketplace))->get();
        }
        $vendors_data = Vendor_product::with('mws_product')->where(array('product_id'=>$id))->get()->first();
        $suppliers_data = Supplier_product::with('mws_product')->where(array('product_id'=>$id))->get()->first();
        $product_data = mws_product::where(array('id'=>$id))->get()->first();
        $isvendors = !empty($vendors_data) ? 1 : 0;
        $issuppliers = !empty($suppliers_data) ? 1 : 0;

                return view('product.settings', ['product_id' => $id,'vendors'=>$vendors,'suppliers'=>$suppliers,'warehouse'=>$warehouse,'vendor_setting'=>$vendors_data,'supplier_setting'=>$suppliers_data,'isvendors'=>$isvendors,'issuppliers'=>$issuppliers,'product_data'=>$product_data]);

    }

    /**
     * Show the form for editing the specified product setting.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vendors  = array();
        $suppliers = array();
        $global_marketplace = session('MARKETPLACE_ID');
        if($global_marketplace != ''){
            $vendors = Vendor::where(array('user_marketplace_id'=>$global_marketplace))->get();
            $suppliers = Supplier::where(array('user_marketplace_id'=>$global_marketplace))->get();
            $warehouse = Warehouse::where(array('user_marketplace_id'=>$global_marketplace))->get();
        }
        $vendors_data = Vendor_product::where(array('product_id'=>$id))->get()->first();
        $suppliers_data = Supplier_product::where(array('product_id'=>$id))->get()->first();
        $isvendors = !empty($vendors_data) ? 1 : 0;
        $issuppliers = !empty($suppliers_data) ? 1 : 0;
        $product_data = mws_product::where(array('id'=>$id))->get()->first();
        return view('product.settings', ['product_id' => $id,'vendors'=>$vendors,'suppliers'=>$suppliers,'warehouse'=>$warehouse,'vendor_setting'=>$vendors_data,'supplier_setting'=>$suppliers_data,'isvendors'=>$isvendors,'issuppliers'=>$issuppliers,'product_data'=>$product_data]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

    }


    /**
     * Show the product warehouse settings detail
     * @method PUT
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $warehouse=Warehouse::get()->toArray();
        $warehouse_data = Warehouse_product::where(array('product_id'=>$id))->get()->first();
        return view('product.warehousesetting',[
        'warehouse'=>$warehouse,
        'product_id' => $id,
        'warehouse_setting'=>$warehouse_data

        ]);
    }

    /**
     * Assign the warehouse with particular product.
     * @method GET
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function product_warehouse_assign($id){
        $global_marketplace = session('MARKETPLACE_ID');
        $warehouse=Warehouse::where(array('user_marketplace_id'=>$global_marketplace))->get()->toArray();
        $warehouse_data = Warehouse_product::with('mws_product')->where(array('product_id'=>$id))->get()->first();
        $product_data = mws_product::where(array('id'=>$id))->get()->first();
        return view('product.warehousesetting',[
            'warehouse'=>$warehouse,
            'product_id' => $id,
            'warehouse_setting'=>$warehouse_data,
            'product_data'=>$product_data

        ]);
    }

    /**
     * Get & check the track id in purchase instance
     * @method GET
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function gerate_track_id(){
        $generate_trake = rand(100,999);
        $track_id = 'PI'.$generate_trake;
        if(!empty($track_id)){
            $check_tranck_number = Purchase_instances::where(array('track_id'=>$track_id))->get()->toArray();
            if(!empty($check_tranck_number)){
                return $this->gerate_track_id();
            }else{
                return $track_id;
            }
        }
    }

    public function index(Request $request){
        $global_marketplace = session('MARKETPLACE_ID');

            $whereArray = [
                "user_marketplace_id" => $global_marketplace
            ];

            $products_data = Mws_product::where($whereArray)->get();

        $products = array();
        $products = $this->data_formatting($products_data);
        $countries = get_country_list();
        return view('productfinal.index', compact('products', 'countries'));
    }

    public function product_details($product_id){
        $global_marketplace = session('MARKETPLACE_ID');

        $whereArray = [
            "user_marketplace_id" => $global_marketplace,
            'id' => $product_id
        ];

        $products_data = Mws_product::where($whereArray)->get();

        $products = array();
        $products = $this->data_formatting($products_data);
        dd($products);
        return response()->json($products);
    }

    public function search($search){
        $global_marketplace = session('MARKETPLACE_ID');

        $whereArray = [
            "user_marketplace_id" => $global_marketplace
        ];

        if(isset($search) && $search != ''){

            $products_data = \DB::table('mws_products as p')
                        ->where('p.user_marketplace_id' ,'=', $global_marketplace)
                        ->where('p.prod_name', 'LIKE', "%{$search }%")
                        ->orWhere('p.sku', 'LIKE', "%{$search}%")
                        ->orWhere('p.fnsku', 'LIKE', "%{$search}%")
                        ->orWhere('p.asin', 'LIKE', "%{$search}%")
                        ->orWhere('p.upc', 'LIKE', "%{$search}%")
                        ->orWhere('p.brand', 'LIKE', "%{$search}%")
                        ->orWhere('p.nick_name', 'LIKE', "%{$search}%")
                        ->orWhere('p.category', 'LIKE', "%{$search}%")
                        ->leftjoin('product_assign_to_label as pa', 'pa.product_id', '=', 'p.id')
                        ->leftjoin('product_label as pl', 'pl.id', '=', 'pa.label_id')
                        ->leftjoin('product_assign as ass','ass.product_id',"=",'p.id')
                        ->leftjoin('suppliers as s','ass.supplier_vendor_id','=','s.id')
                        ->leftjoin('vendors as v','ass.supplier_vendor_id','=','v.id')
                        ->orWhere('pl.label_name','LIKE',"%{$search}%")
                        ->orWhere('v.vendor_name','LIKE',"%{$search}%")
                        ->orWhere('s.supplier_name','LIKE',"%{$search}%")
                        ->selectRaw('p.*')
                        ->groupBy('p.id')
                        ->get()->toArray();

            $products_data = (array)$products_data;
            $products_data = json_decode(json_encode($products_data), true);
        }else{
            $products_data = Mws_product::where($whereArray)->get();
        }

        $products = array();
        $searchResult['products'] = $this->data_formatting($products_data);
        $searchResult['countries'] = get_country_list();
        return $searchResult;
//        return view('productfinal.index', compact('products', 'countries'));
    }

    public function filter(Request $request){
        $global_marketplace = session('MARKETPLACE_ID');
        $get_user_id = Usermarketplace::where(array('id'=>$global_marketplace))->first();
        $whereArray = array([
           'id','!=','0',
        ]);

        // $request->country = 'US';
        // $request->from_sku = 'Q1-S6RF-V6XP';
        //$request->to_sku = 'O6-N01U-2KVY';

        $orwhere_sku = '';
        //dd($request->country);
        //dd($get_user_id->user_id);
        if(!empty($get_user_id)){
            if($request->country != ''){
                $get_new_marketplace = Usermarketplace::where(array('user_id'=>$get_user_id->user_id,'country'=>$request->country))->first();
                //dd($get_new_marketplace);
                if(!empty($get_new_marketplace)){
                    $whereArray[] = [
                        "user_marketplace_id" ,"=", $get_new_marketplace->id,
                    ];
                }else{
                    $whereArray[] = [
                        'user_marketplace_id','=','0',
                    ];
                }

            }if(isset($request->from_sku) && $request->from_sku != ''){

                $whereArray[] = [
                    'sku','LIKE','%'.$request->from_sku.'%',
                ];

            }if(isset($request->to_sku) && $request->to_sku != ''){
                $orwhere_sku = array([
                    'sku','LIKE','%'.$request->to_sku.'%',
                ]);

            }

        }

    if(isset($request->to_sku) && $request->to_sku != ''){
        $products_data = Mws_product::where($whereArray)->orwhere($orwhere_sku)->get();
    }else{
        $products_data = Mws_product::where($whereArray)->get();
    }


        $products = array();
        $filterResult['products'] = $this->data_formatting($products_data);
        $filterResult['countries'] = get_country_list();
        return $filterResult;
//        return view('productfinal.index', compact('products', 'countries'));
    }


    public function data_formatting($products_data){
        $products = array();
        $lead_time_setting = '';
        foreach($products_data as $key=>$pro_data){
            $get_assign_product = Product_assign::where(array('product_id'=>$pro_data['id']))->first();
            $get_dimension = Product_dimensions::where(array('product_id'=>$pro_data['id']))->first();
            $get_profit_analysis = Product_profit_analysis::where(array('product_id'=>$pro_data['id']))->first();
           // $get_product_reschedule = Product_reorder_schedule_detail::where(array('product_id'=>$pro_data['id']))->get();
            $get_product_reschedule = Product_reorder_schedule_detail::where(array('product_id'=>$pro_data['id']))->get();
            $get_lead_time = Product_lead_time::where(array('product_id'=>$pro_data['id']))->first();
           // $product_label = Product_assign_to_label::with('labels')->where(array('product_id'=>$pro_data['id']))->get()->toArray();
            $product_label = Product_assign_to_label::where(array('product_assign_to_label.product_id'=>$pro_data['id']))
                            ->leftjoin('product_label as pl', 'pl.id', '=', 'product_assign_to_label.label_id')
                            ->selectRaw('product_assign_to_label.*,pl.label_name')
                            ->get()->toArray();
            $productLabel = array();
             if(!empty($product_label)) {
                foreach ($product_label as $index => $label) {
                    $productLabel[$index]['id'] = $label['id'];
                    $productLabel[$index]['text'] = ($label['label_name'] ? $label['label_name'] : '');
                }
            }
            $order_setting_type = $leadtime_setting_type = 'Default';
            if(!empty($get_assign_product)){
                if($get_assign_product->leadtime_setting_type == 1){
                    $leadtime_setting_type = 'Default';
                    $get_lead_time = Lead_time::where(array('supplier_vendor'=>$get_assign_product->supplier_vendor,'supplier_vendor_id'=>$get_assign_product->supplier_vendor_id))->first();
                }else{
                    $leadtime_setting_type = 'Custom';
                    $get_lead_time = Product_lead_time::where(array('product_id'=>$pro_data['id'],'product_assign_id'=>$get_assign_product->id))->first();
                    if(empty($get_lead_time)){
                        $get_lead_time = Product_custom_lead_time_table::where(array('product_id'=>$pro_data['id'],'product_assign_id'=>$get_assign_product->id))->first();
                    }

                }

               
                if(!empty($get_lead_time)){
                    $lead_time_setting = $get_lead_time->type == 1 ? 'boat' : 'plane';
                }
                // echo '<pre>';
                // print_r($get_lead_time);
                // exit;

                if($get_assign_product->order_setting_type == 1){
                    $order_setting_type = 'Default';
                    $get_product_reschedule = Reorder_schedule_detail::where(array('supplier_vendor'=>$get_assign_product->supplier_vendor,'supplier_vendor_id'=>$get_assign_product->supplier_vendor_id))->get()->toArray();
                }else{
                    $order_setting_type = 'Custom';
                    $get_product_reschedule = Product_reorder_schedule_detail::where(array('product_id'=>$pro_data['id'],'product_assign_id'=>$get_assign_product->id))->get()->toArray();
                    if(empty($get_product_reschedule)){
                        $get_product_reschedule = Product_custom_reorder_schedule_detail::where(array('product_id'=>$pro_data['id'],'product_assign_id'=>$get_assign_product->id))->get()->toArray();
                    }
                }

            }

            $array_schedule = array();
            $array_schedule_type = '';
            if(!empty($get_product_reschedule)){
                foreach($get_product_reschedule as $reorder){

                    if ($reorder['schedule_days'] == 1) $values_d = 'Sun';
                    if ($reorder['schedule_days'] == 2) $values_d = 'Mon';
                    if ($reorder['schedule_days'] == 3) $values_d = 'Tue';
                    if ($reorder['schedule_days'] == 4) $values_d = 'Wed';
                    if ($reorder['schedule_days'] == 5) $values_d = 'Thu';
                    if ($reorder['schedule_days'] == 6) $values_d = 'Fri';
                    if ($reorder['schedule_days'] == 7) $values_d = 'Sat';

                    if($reorder['reorder_schedule_type_id'] == '1'){
                        $array_schedule[]  = $values_d;
                        $array_schedule_type = 'weekly';
                    }else if($reorder['reorder_schedule_type_id'] == '2'){
                        $array_schedule[]  = $values_d;
                        $array_schedule_type = 'multi-weekly';
                    }else if($reorder['reorder_schedule_type_id'] == '3'){
                        $array_schedule[]  = $reorder['schedule_days'];
                        $array_schedule_type = 'monthly';
                    }else if($reorder['reorder_schedule_type_id'] == '4'){
                        $array_schedule[]  = $reorder['schedule_days'];
                        $array_schedule_type = 'multi-monthly';
                    }else{
                        $array_schedule[] = '';
                        $array_schedule_type = 'on-demand';
                    }
                }
            }
            if(!empty($array_schedule)){
                $array_schedule = implode(', ', $array_schedule);
            }

            $total_lead_days = @$get_lead_time->product_manuf_days + @$get_lead_time->to_port_days + @$get_lead_time->transit_time_days + @$get_lead_time->to_warehouse_days + @$get_lead_time->to_amazon + @$get_lead_time->safety_days;


            $products[$key] =
                [
                    'id' => $pro_data['id'],
                    'basic_info'=> [
                        'image'=> isset($pro_data['prod_image']) && $pro_data['prod_image'] != '' ? $pro_data['prod_image'] : '' ,
                        'nickname'=> isset($pro_data['nick_name']) && $pro_data['nick_name'] != '' ? $pro_data['nick_name'] : '' ,
                        'sku'=> isset($pro_data['sku']) && $pro_data['sku'] != '' ? $pro_data['sku'] : '' ,
                        'fnsku'=> isset($pro_data['fnsku']) && $pro_data['fnsku'] != '' ? $pro_data['fnsku'] : '' ,
                        'title'=> isset($pro_data['prod_name']) && $pro_data['prod_name'] != '' ? $pro_data['prod_name'] : '' ,
                        'asin'=> isset($pro_data['asin']) && $pro_data['asin'] != '' ? $pro_data['asin'] : '' ,
                        'upc'=> isset($pro_data['upc']) && $pro_data['upc'] != '' ? $pro_data['upc'] : '' ,
                        'brand'=> isset($pro_data['brand']) && $pro_data['brand'] != '' ? $pro_data['brand'] : '' ,
                        'category'=> isset($pro_data['category']) && $pro_data['category'] != '' ? $pro_data['category'] : '' ,
                        'variation'=> isset($pro_data['variation']) && $pro_data['variation'] != '' ? $pro_data['variation'] : '' ,
                        'labels'=> isset($productLabel) && $productLabel != '' ? $productLabel : '' ,
                    ],
                    'sources'=> [
                        'source_type'=> isset($get_assign_product) && $get_assign_product->supplier_vendor != '' ? ($get_assign_product->supplier_vendor == 2 ? 'Vendors' : 'Suppliers') : 0,
                        'sources'=> isset($get_assign_product->supplier_vendor_id) && $get_assign_product->supplier_vendor_id != '' ? $get_assign_product->supplier_vendor_id : '',
                        'mpn'=> 0,
                        'qty_per_sku'=> 0,
                    ],
                    'dimensions'=> [
                        //'item_height'=> isset($get_dimension['item_height']) && $get_dimension['item_height'] != '' ? $get_dimension['item_height'].@$get_dimension['size_in'] :'0cm',
                        'item_height'=> isset($pro_data['longestside']) && $pro_data['longestside'] != '' ? $pro_data['longestside'] :'0',
                       //'item_width'=> isset($get_dimension['item_width']) && $get_dimension['item_width'] != '' ? $get_dimension['item_width'].@$get_dimension['size_in'] : '0cm' ,
                        'item_width'=> isset($pro_data['medianside']) && $pro_data['medianside'] != '' ? $pro_data['medianside'] : '0' ,
                        //'item_length'=> isset($get_dimension['item_length']) && $get_dimension['item_length'] != '' ? $get_dimension['item_length'].@$get_dimension['size_in'] : '0cm',
                        'item_length'=> isset($pro_data['shortestside']) && $pro_data['shortestside'] != '' ? $pro_data['shortestside'] : '0',
                        //'item_weight'=> isset($get_dimension['item_weight']) && $get_dimension['item_weight'] != '' ? $get_dimension['item_weight'].@$get_dimension['weigth_in'] : '0kg',
                        'item_weight'=> isset($pro_data['itempackweight']) && $pro_data['itempackweight'] != '' ? $pro_data['itempackweight'] : '0',
                        'amz_dim_weight'=> isset($get_dimension['item_weight']) && $get_dimension['item_weight'] != '' ? $get_dimension['item_weight'] : 0,
                        'qty_per_box'=> isset($get_dimension['box_qty']) && $get_dimension['box_qty'] != '' ? $get_dimension['box_qty'] : '0' ,
                        'box_height'=> isset($get_dimension['box_height']) && $get_dimension['box_height'] != '' ? $get_dimension['box_height'] :'0',
                        'box_length'=> isset($get_dimension['box_length']) && $get_dimension['box_length'] != '' ? $get_dimension['box_length']:'0',
                        'box_width'=>isset($get_dimension['box_width']) && $get_dimension['box_width'] != '' ? $get_dimension['box_width']:'0',
                        'box_weight'=> isset($get_dimension['box_weight']) && $get_dimension['box_weight'] != '' ? @$get_dimension['box_weight'] :'0',
                        'box_cbm'=> '0',
                        'boxes_per_carton'=>isset($get_dimension['box_par_carton_qty']) && $get_dimension['box_par_carton_qty'] != '' ? $get_dimension['box_par_carton_qty'] : '0' ,
                        'carton_length'=> isset($get_dimension['box_par_carton_height']) && $get_dimension['box_par_carton_height'] != '' ? $get_dimension['box_par_carton_height'] :'0',
                        'carton_width'=> isset($get_dimension['box_par_carton_width']) && $get_dimension['box_par_carton_width'] != '' ? $get_dimension['box_par_carton_width'] :'0',
                        'carton_height'=> isset($get_dimension['box_par_carton_length']) && $get_dimension['box_par_carton_length'] != '' ? $get_dimension['box_par_carton_length'] :'0',
                        'carton_weight'=> isset($get_dimension['box_par_carton_weight']) && $get_dimension['box_par_carton_weight'] != '' ? $get_dimension['box_par_carton_weight'] :'0',
                        'carton_cbm'=> 0,
                        'cartons_per_pallet'=> isset($get_dimension['cartons_par_pallet_qty']) && $get_dimension['cartons_par_pallet_qty'] != '' ? $get_dimension['cartons_par_pallet_qty'] : '0' ,
                        'pallet_length'=> isset($get_dimension['cartons_par_pallet_length']) && $get_dimension['cartons_par_pallet_length'] != '' ? $get_dimension['cartons_par_pallet_length'] :'0',
                        'pallet_width'=> isset($get_dimension['cartons_par_pallet_width']) && $get_dimension['cartons_par_pallet_width'] != '' ? $get_dimension['cartons_par_pallet_width'] :'0',
                        'pallet_height'=> isset($get_dimension['cartons_par_pallet_height']) && $get_dimension['cartons_par_pallet_height'] != '' ? $get_dimension['cartons_par_pallet_height'] :'0',
                        'pallet_weight'=> isset($get_dimension['cartons_par_pallet_weight']) && $get_dimension['cartons_par_pallet_weight'] != '' ? $get_dimension['cartons_par_pallet_weight'] :'0',
                        'pallet_cbm'=> 0,
                        'unitofdimensions' => $pro_data['unitofdimensions'],
                        'unitofweight' => $pro_data['unitofweight']
                    ],
                    'quantity_discounts'=> [
                        'qd_type'=> 'Dollar',
                        'moq'=> 0,
                        'unit_cost'=> isset($get_profit_analysis['unit_cost']) && $get_profit_analysis['unit_cost'] != '' ? '$'.$get_profit_analysis['unit_cost'] : '$0' ,
                        'tiers'=> 0,
                        'qty_calc'=> '',
                        'qd_bundled'=> ''
                    ],
                    'profit_analysis'=> [
                        'unit_cost'=> isset($get_profit_analysis['unit_cost']) && $get_profit_analysis['unit_cost'] != '' ? '$'.$get_profit_analysis['unit_cost'] : '$0' ,
                        'shipping'=> isset($get_profit_analysis['shipping']) && $get_profit_analysis['shipping'] != '' ? '$'.$get_profit_analysis['shipping'] : '$0' ,
                        'customs'=> isset($get_profit_analysis['customs']) && $get_profit_analysis['customs'] != '' ? '$'.$get_profit_analysis['customs'] : '$0' ,
                        'other_pc'=> isset($get_profit_analysis['other_landed_cost']) && $get_profit_analysis['other_landed_cost'] != '' ? '$'.$get_profit_analysis['other_landed_cost'] : '$0' ,
                        'total_landed'=> isset($get_profit_analysis['total_landed']) && $get_profit_analysis['total_landed'] != '' ? '$'.$get_profit_analysis['total_landed'] : '$0' ,
                        'warehouse_storage'=> isset($get_profit_analysis['warehouse_storage']) && $get_profit_analysis['warehouse_storage'] != '' ? '$'.$get_profit_analysis['warehouse_storage'] : '$0' ,
                        'inbound_shipping'=> isset($get_profit_analysis['inbound_shipping']) && $get_profit_analysis['inbound_shipping'] != '' ? '$'.$get_profit_analysis['inbound_shipping'] : '$0' ,
                        'other_ic'=> isset($get_profit_analysis['other_inland_cost']) && $get_profit_analysis['other_inland_cost'] != '' ? '$'.$get_profit_analysis['other_inland_cost'] : '$0' ,
                        'total_inland'=> isset($get_profit_analysis['total_inland']) && $get_profit_analysis['total_inland'] != '' ? '$'.$get_profit_analysis['total_inland'] : '$0' ,
                        'fba'=> isset($get_profit_analysis['fba_fee']) && $get_profit_analysis['fba_fee'] != '' ? '$'.$get_profit_analysis['fba_fee'] : '$0' ,
                        'referral'=> isset($get_profit_analysis['referal_fee']) && $get_profit_analysis['referal_fee'] != '' ? '$'.$get_profit_analysis['referal_fee'] : '$0' ,
                        'storage'=> isset($get_profit_analysis['storage_fee']) && $get_profit_analysis['storage_fee'] != '' ? '$'.$get_profit_analysis['storage_fee'] : '$0' ,
                        'total_amz_fees'=> isset($get_profit_analysis['total_amazon_fee']) && $get_profit_analysis['total_amazon_fee'] != '' ? '$'.$get_profit_analysis['total_amazon_fee'] : '$0' ,
                        'total_costs'=> isset($get_profit_analysis['total_costs']) && $get_profit_analysis['total_costs'] != '' ? '$'.$get_profit_analysis['total_costs'] : '$0' ,
                        'price_point'=> isset($get_profit_analysis['price_profit']) && $get_profit_analysis['price_profit'] != '' ? '$'.$get_profit_analysis['price_profit'] : '$0' ,
                        'total_profit'=> isset($get_profit_analysis['total_profit']) && $get_profit_analysis['total_profit'] != '' ? '$'.$get_profit_analysis['total_profit'] : '$0' ,
                        'profit_margin'=> isset($get_profit_analysis['profit_margin']) && $get_profit_analysis['profit_margin'] != '' ? $get_profit_analysis['profit_margin'].'%' : '0%' ,
                        'roi'=> isset($get_profit_analysis['roi']) && $get_profit_analysis['roi'] != '' ? $get_profit_analysis['roi'].'%' : '0%' ,
                    ],
                    'order_settings'=> [
                        'order_setting_type' => @$order_setting_type,
                        'order_volumn'=> isset($get_lead_time->order_volume_value) ? $get_lead_time->order_volume_value.' '.($get_lead_time->order_volume_id == 1 ? 'Days' : 'Calendar Month') : '',
                        'reorder_schedule_type'=> @$array_schedule_type,
                        'reorder_schedule'=> @$array_schedule,
                    ],
                    'lead_time'=> [
                        'leadtime_setting_type'=> @$leadtime_setting_type,
                        'transit_type'=> $lead_time_setting,
                        'po_to_prod'=> 0,
                        'production'=> isset($get_lead_time->product_manuf_days) && $get_lead_time->product_manuf_days != '' ? $get_lead_time->product_manuf_days : 0,
                        'to_port'=> isset($get_lead_time->to_port_days) && $get_lead_time->to_port_days != ''  ? $get_lead_time->to_port_days : 0,
                        'transit'=> isset($get_lead_time->transit_time_days) && $get_lead_time->transit_time_days != ''  ? $get_lead_time->transit_time_days : 0,
                        'to_warehouse'=> isset($get_lead_time->to_warehouse_days) && $get_lead_time->to_warehouse_days != ''  ? $get_lead_time->to_warehouse_days : 0,
                        'to_amazon'=> isset($get_lead_time->to_amazon) && $get_lead_time->to_amazon != ''  ? $get_lead_time->to_amazon : 0,
                        'safety'=> isset($get_lead_time->safety_days) && $get_lead_time->safety_days != ''  ? $get_lead_time->safety_days : 0,
                        'total_lead_time'=> isset($get_lead_time->total_lead_time_days) && $get_lead_time->total_lead_time_days != ''  ? $get_lead_time->total_lead_time_days : $total_lead_days,
                    ]
                ];
        }
        // echo '<pre>';
        // print_r($products);
        // exit;
        return $products;
    }

    public function product_cell_change(Request $request){
        $global_marketplace = session('MARKETPLACE_ID');
        if($request->product_id != ''){
            if($request->val != ''){
                $product_update = [
                    'nick_name' => $request->val,
                ];
                Mws_product::where(array('id'=>$request->product_id))->update($product_update);
                $res['success'] = true;
            }else{
                $res['success'] = false;
                $res['message'] = 'nickname must be required';
            }
        }else{
            $res['success'] = false;
            $res['message'] = 'product must be required';
        }
        return response()->json($res);
    }

    public function product_detail_save(Request $request){

        if(isset($request->tab) && $request->tab == 'basic_info'){
            $global_marketplace = session('MARKETPLACE_ID');
            if($request->product_id != ''){
                $product_update = [
                    'nick_name' => $request->nickname,
                ];
                Mws_product::where(array('id'=>$request->product_id))->update($product_update);

                if(!empty($request->labels)){
                    foreach($request->labels as $label_data){
                        if($label_data != ''){
                            $checklabels = Product_label:: where(array('user_marketplace_id'=>$global_marketplace,'label_name'=>trim($label_data)))->first();
                            if(empty($checklabels)){
                                $req_label = [
                                    'user_marketplace_id' =>$global_marketplace,
                                    'label_name' => trim($label_data),
                                ];
                                $datas_of = Product_label::create($req_label);
                                $userLastInsertId = $datas_of->toArray();
                                    $check_assign = product_assign_to_label::where(array('label_id'=>$userLastInsertId['id'],'product_id'=>$request->product_id))->first();
                                    if(empty($check_assign)){
                                        $req_data1 = [
                                            'user_marketplace_id'=> $global_marketplace,
                                            'label_id'=> $userLastInsertId['id'],
                                            'product_id'=> $request->product_id
                                        ];
                                        Product_assign_to_label::create($req_data1);
                                    }
                            }else{
                                $check_assign = product_assign_to_label::where(array('label_id'=>$checklabels->id,'product_id'=>$request->product_id))->first();
                                if(empty($check_assign)){
                                    $req_data2 = [
                                        'user_marketplace_id'=> $global_marketplace,
                                        'label_id'=> $checklabels->id,
                                        'product_id'=> $request->product_id
                                    ];
                                    Product_assign_to_label::create($req_data2);
                                }
                            }
                        }
                    }
                }
                $res['success'] = true;
            }else{
                $res['success'] = false;
            }
           return response()->json($res);
        }

        if(isset($request->tab) && $request->tab == 'dimensions'){
           // dd($request->all());
            if($request->product_id != ''){

                $req_data = [
                    'product_id'=>$request->product_id,

                    'item_height'=>$request->product_size_h,
                    'item_width'=>$request->product_size_w,
                    'item_length'=>$request->product_size_l,
                    'size_in'=>$request->product_size_unit,
                    'item_weight'=>$request->product_weight,
                    'weigth_in'=>$request->product_weight_unit,

                    'box_qty'=>$request->packaging_qty_per_box_qty,
                    'box_height'=>$request->packaging_qty_per_box_size_h,
                    'box_width'=>$request->packaging_qty_per_box_size_w,
                    'box_length'=>$request->packaging_qty_per_box_size_l,
                    'box_weight'=>$request->packaging_qty_per_box_weight,
                    'box_volume'=>$request->packaging_qty_per_box_volume,

                    'box_size_in'=>$request->packaging_size_unit,
                    'box_weight_unit'=>$request->packaging_weight_unit,
                    'box_volumn_in'=>$request->packaging_volume_unit,

                    'box_par_carton_qty'=>$request->packaging_box_per_carton_qty,
                    'box_par_carton_height'=>$request->packaging_box_per_carton_size_h,
                    'box_par_carton_width'=>$request->packaging_box_per_carton_size_w,
                    'box_par_carton_length'=>$request->packaging_box_per_carton_size_l,
                    'box_par_carton_weight'=>$request->packaging_box_per_carton_weight,
                    'box_par_carton_volume'=>$request->packaging_box_per_carton_volume,

                    'cartons_par_pallet_qty'=>$request->packaging_carton_per_box_qty,
                    'cartons_par_pallet_height'=>$request->packaging_carton_per_box_size_h,
                    'cartons_par_pallet_width'=>$request->packaging_carton_per_box_size_w,
                    'cartons_par_pallet_length'=>$request->packaging_carton_per_box_size_l,
                    'cartons_par_pallet_weight'=>$request->packaging_carton_per_box_weight,
                    'cartons_par_pallet_volume'=>$request->packaging_carton_per_box_volume,

                ];

                //dd($req_data);

                $check_data = Product_dimensions::where(array('product_id'=>$request->product_id))->first();
                if(!empty($check_data)){
                    Product_dimensions::where(array('id'=>$check_data->id))->update($req_data);
                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = 'product dimensions updated successfully';
                }else{
                    Product_dimensions::create($req_data);
                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = 'product dimensions created successfully';
                }

            }else{
                $res['success'] = false;
                $res['error'] = 1;
                $res['message'] = 'product id is missing';
            }
            return response()->json($res);
        }

        if(isset($request->tab) && $request->tab == 'quantity_discounts'){
            //dd($request->all());
            if($request->product_id != ''){
                $quality_discounts_id = '';
                $req_data = [
                    'product_id'=>$request->product_id,
                    'moq_quantity_unit' =>$request->moq_quantity_unit,
                    'moq_quantity_price' => $request->moq_quantity_price,
                ];
                $check_data = Product_quantity_discounts::where(array('product_id'=>$request->product_id))->first();
                if(!empty($check_data)){
                    Product_quantity_discounts::where(array('id'=>$check_data->id))->update($req_data);
                    $quality_discounts_id = $check_data->id;
                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = 'quantity discount updated successfully';
                }else{
                    $datas_of = Product_quantity_discounts::create($req_data);
                    $userLastInsertId = $datas_of->toArray();
                    $quality_discounts_id = $userLastInsertId['id'];
                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = 'quantity discount created successfully';
                }
                Product_sub_quantity_discounts::where(array('product_id'=>$request->product_id))->delete();
                if($quality_discounts_id != ''){
                    if(!empty($request->quantity_discounts)){
                        foreach($request->quantity_discounts as $discount){
                            $req_sub_data = [
                                'product_id'=>$request->product_id,
                                'product_quantity_id' => $quality_discounts_id,
                                'unit' =>$discount['unit'],
                                'price' => $discount['price'],
                            ];
                            $datas_of = Product_sub_quantity_discounts::create($req_sub_data);
                        }
                    }
                }
            }else{
                $res['success'] = false;
                $res['error'] = 1;
                $res['message'] = 'product id is missing';
            }
            return response()->json($res);
        }

        if(isset($request->tab) && $request->tab == 'profit_analysis'){
            //dd($request->all());
            if($request->product_id != ''){

                $req_data = [
                    'product_id'=>$request->product_id,

                    'total_landed'=>$request->landed_total != '' ? $request->landed_total : 0,
                    'unit_cost'=>$request->landed_unit_cost != '' ? $request->landed_unit_cost : 0,
                    'shipping'=>$request->landed_shipping != '' ? $request->landed_shipping : 0,
                    'customs'=>$request->landed_customs != '' ? $request->landed_customs : 0,
                    'other_landed_cost'=>$request->landed_other != '' ? $request->landed_other : 0,
                    'total_inland'=>$request->inland_total != '' ? $request->inland_total : 0,
                    'warehouse_storage'=>$request->inland_warehouse_storage != '' ? $request->inland_warehouse_storage : 0,
                    'inbound_shipping'=>$request->inland_inbound_shipping != '' ? $request->inland_inbound_shipping : 0,
                    'other_inland_cost'=>$request->inland_other != '' ? $request->inland_other : 0,
                    'total_amazon_fee'=>$request->amazon_total != '' ? $request->amazon_total : 0,
                    'fba_fee'=>$request->fba != '' ? str_replace("$","",$request->fba) : 0,
                    'referal_fee'=>$request->referral != '' ? str_replace("$","",$request->referral) : 0,
                    'storage_fee' => $request->storage != '' ? str_replace("$","",$request->storage) : 0,
                    'price_profit' => $request->total_profit != '' ? str_replace("$","",$request->storage) : 0,
                    'profit_margin' => $request->profit_margin != '' ? str_replace("$","",$request->storage) : 0,
                    'roi' =>$request->roi != '' ? chop($request->roi,"%") : 0 ,
                ];

               // dd($req_data);

                $check_data = Product_profit_analysis::where(array('product_id'=>$request->product_id))->first();
                if(!empty($check_data)){
                    Product_profit_analysis::where(array('id'=>$check_data->id))->update($req_data);
                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = 'product profit analysis updated successfully';
                }else{
                    Product_profit_analysis::create($req_data);
                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = 'product profit analysis created successfully';
                }

            }else{
                $res['success'] = false;
                $res['error'] = 1;
                $res['message'] = 'product id is missing';
            }
            return response()->json($res);
        }

        if(isset($request->tab) && $request->tab == 'lead_time'){
           //dd($request->all());

            $setting_type_id = $request->lead_time_set_type != '' && $request->lead_time_set_type == 'default' ? '1' : '2';
            $assign_product_id = '';
            $get_assign_id = Product_assign::where(array('product_id'=>$request->product_id,'primary_user'=>1))->first();
            if(!empty($get_assign_id)){
                $assign_product_id = $get_assign_id->id;
                $req_datas = [
                    'leadtime_setting_type' => $setting_type_id,
                ];
                Product_assign::where(array('id' => $assign_product_id))->update($req_datas);
            }

            if(empty($request->product_id)){
                $res_arr['success'] = false;
                $res_arr['error'] = 1;
                $res_arr['message'] = 'Product not found';
                return response()->json($res_arr);
            }

            if(isset($request->lead_time_type) && $request->lead_time_type == 'boat'){

                    $product_manuf_days = isset($request->lead_time_boat_production) ? $request->lead_time_boat_production : 0;
                    $to_port_days = isset($request->lead_time_boat_port) ? $request->lead_time_boat_port : 0;
                    $transit_time_days = isset($request->lead_time_boat_transit) ? $request->lead_time_boat_transit : 0;
                    $to_warehouse_days = isset($request->lead_time_boat_warehouse) ? $request->lead_time_boat_warehouse : 0;
                    $to_amazon = isset($request->lead_time_boat_amazon) ? $request->lead_time_boat_amazon : 0;
                    $po_to_production_days = isset($request->lead_time_boat_po_to_production) ? $request->lead_time_boat_po_to_production : 0;
                    $safety_days = isset($request->lead_time_boat_safety) ? $request->lead_time_boat_safety : 0;
                    $total_lead_time_days = isset($request->total_lead_time_days_boat) ? $request->total_lead_time_days_boat : 0;
                    // $order_prep_days = isset($request->order_prep_days_boat) ? $request->order_prep_days_boat : 0;
                    // $po_to_prep_days = isset($request->po_to_prep_days_boat) ? $request->po_to_prep_days_boat : 0;
                    $type = "1";

            } else if(isset($request->lead_time_type) && $request->lead_time_type == 'plane'){

                    $product_manuf_days = isset($request->lead_time_plane_production) ? $request->lead_time_plane_production : 0;
                    $to_port_days = isset($request->lead_time_plane_port) ? $request->lead_time_plane_port : 0;
                    $transit_time_days = isset($request->lead_time_plane_transit) ? $request->lead_time_plane_transit : 0;
                    $to_warehouse_days = isset($request->lead_time_plane_warehouse) ? $request->lead_time_plane_warehouse : 0;
                    $to_amazon = isset($request->lead_time_plane_amazon) ? $request->lead_time_plane_amazon : 0;
                    $po_to_production_days = isset($request->lead_time_plane_po_to_production) ? $request->lead_time_plane_po_to_production : 0;
                    $safety_days = isset($request->lead_time_plane_po_to_production) ? $request->lead_time_plane_po_to_production : 0;
                    $total_lead_time_days = isset($request->lead_time_plane_safety) ? $request->lead_time_plane_safety : 0;
                    // $order_prep_days = isset($request->order_prep_days_plane) ? $request->order_prep_days_plane : 0;
                    // $po_to_prep_days = isset($request->po_to_prep_days_plane) ? $request->po_to_prep_days_plane : 0;
                    $type = "2";

            }
            else{

                $product_manuf_days =  0;
                $to_port_days =  0;
                $transit_time_days =  0;
                $to_warehouse_days =  0;
                $to_amazon =  0;
                $po_to_production_days =  0;
                $safety_days = 0;
                $total_lead_time_days =  0;
                // $order_prep_days =  0;
                // $po_to_prep_days =  0;
                $type = "0";

        }

        $total_leadtime = $product_manuf_days + $to_port_days + $transit_time_days + $to_warehouse_days + $to_amazon + $po_to_production_days + $safety_days;
            /*todo*/
            if($request->lead_time_set_type == 'default'){
                $req_data = [
                    'product_id' => $request->product_id,
                    'product_assign_id' => $assign_product_id,
                    'product_manuf_days' => $product_manuf_days,
                    'to_port_days' => $to_port_days,
                    'transit_time_days' => $transit_time_days,
                    'to_warehouse_days' => $to_warehouse_days,
                    'to_amazon' => $to_amazon,
                    'po_to_production_days' => $po_to_production_days,
                    'safety_days' => $safety_days,
                    'total_lead_time_days' => $total_lead_time_days != '' ? $total_lead_time_days : $total_leadtime,
                    // 'order_prep_days' => $order_prep_days,
                    // 'po_to_prep_days' => $po_to_prep_days,
                    'type' => $type,
                ];

                //dd($req_data);



                $check_exists = Product_lead_time::where(array('product_id' => $request->product_id, 'product_assign_id' => $assign_product_id))->first();
                if (!empty($check_exists)) {
                    Product_lead_time::where(array('id' => $check_exists->id))->update($req_data);
                    $res['error'] = 0;
                    $res['success'] = true;
                    $res['message'] = "product lead time updated successfully";
                } else {
                    $lead_data = Product_lead_time::create($req_data);
                    $res['error'] = 0;
                    $res['success'] = true;
                    $res['message'] = "product lead time created successfully";
                }
            }else{
                $req_data = [
                    'product_id' => $request->product_id,
                    'product_manuf_days' => $product_manuf_days,
                    'to_port_days' => $to_port_days,
                    'transit_time_days' => $transit_time_days,
                    'to_warehouse_days' => $to_warehouse_days,
                    'to_amazon' => $to_amazon,
                    'po_to_production_days' => $po_to_production_days,
                    'safety_days' => $safety_days,
                    'total_lead_time_days' => $total_lead_time_days,
                    // 'order_prep_days' => $order_prep_days,
                    // 'po_to_prep_days' => $po_to_prep_days,
                    'type' => $type,
                ];

                $check_exists = Product_custom_lead_time_table::where(array('product_id' => $request->product_id))->first();
                if (!empty($check_exists)) {
                    Product_custom_lead_time_table::where(array('id' => $check_exists->id))->update($req_data);
                    $res['error'] = 0;
                    $res['success'] = true;
                    $res['message'] = "product lead time updated successfully";
                } else {
                    $lead_data = Product_custom_lead_time_table::create($req_data);
                    $res['error'] = 0;
                    $res['success'] = true;
                    $res['message'] = "product lead time created successfully";
                }
            }
            return response()->json($res);
        }

        if(isset($request->tab) && $request->tab == 'order_setting'){
            $request->setting_type = 'default';
            $setting_type_id = $request->setting_type != '' && $request->setting_type == 'default' ? '1' : '2';

            $assign_product_id = '';
            $get_assign_id = Product_assign::where(array('product_id'=>$request->product_id,'primary_user'=>1))->first();
            if(!empty($get_assign_id)){
                $assign_product_id = $get_assign_id->id;
                $req_datas = [
                    'order_setting_type' => $setting_type_id,
                ];
                Product_assign::where(array('id' => $assign_product_id))->update($req_datas);
            }

            if($request->setting_type == 'default'){
                $typeArray = ['on-demand'=>0 , 'weekly' => 1, 'multi-weekly' => 2, 'monthly' => 3, 'multi-monthly' => 4];

                //$check_exists = Lead_time::where(array('id' => $request->lead_time_id))->first();
                $check_exists = Product_lead_time::where(array('product_id' =>$request->product_id,'product_assign_id'=>$assign_product_id))->first();
                if (!empty($check_exists)) {
                    $req_data = [
                        'order_volume_value' => $request->order_volume,
                        'order_volume_id' => $request->order_type == 'month' ? 2 : 1,
                    ];
                    $product_lead_time_id = $check_exists->id;
                    Product_lead_time::where(array('id' => $check_exists->id))->update($req_data);
                    //$message = get_messages('vendor order setting updated successfully', 1);
                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = "product order setting updated successfully";
                } else {
                    // $res['success'] = false;
                    // $res['error'] = 1;
                    // $res['message'] = 'Please first complete lead time details';
                    // return response()->json($res);

                    // $vendor = Vendor::where('id', $request->vendor_id)->first();
                    $req_data = [
                        'product_id' => $request->product_id,
                        'product_assign_id' => $assign_product_id,
                        'order_volume_value' => $request->order_volume,
                        'order_volume_id' => $request->order_type == 'month' ? 2 : 1,
                    ];

                    $lead_data = Product_lead_time::create($req_data);
                    $userLastInsertId = $lead_data->toArray();
                    $product_lead_time_id = $userLastInsertId['id'];
                    //$message = get_messages('vendor order setting created successfully', 1);

                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = "product order setting created successfully";
                }
               //dd($product_lead_time_id);
                if($request->order_schedule != ''){
                    Product_reorder_schedule_detail::where(array('product_id' => $request->product_id,'product_assign_id'=>$assign_product_id))->delete();
                    if($request->order_schedule == 'monthly'){

                        $reorder_schedule = ['product_id' => $request->product_id, 'product_assign_id'=>$assign_product_id ,'reorder_schedule_type_id' => $product_lead_time_id, 'reorder_schedule_type_id' => $typeArray[$request->order_schedule], 'schedule_days' => $request->order_schedule_monthly];
                        $reorder_schedule_data = Product_reorder_schedule_detail::create($reorder_schedule);

                    }else if($request->order_schedule == 'weekly'){
                        if ($request->order_schedule_weekly == 'Sun') $values_d = 1;
                        if ($request->order_schedule_weekly == 'Mon') $values_d = 2;
                        if ($request->order_schedule_weekly == 'Tue') $values_d = 3;
                        if ($request->order_schedule_weekly == 'Wed') $values_d = 4;
                        if ($request->order_schedule_weekly == 'Thu') $values_d = 5;
                        if ($request->order_schedule_weekly == 'Fri') $values_d = 6;
                        if ($request->order_schedule_weekly == 'Sat') $values_d = 7;

                        $reorder_schedule = ['product_id' => $request->product_id, 'product_assign_id'=>$assign_product_id, 'reorder_schedule_type_id' => $product_lead_time_id, 'reorder_schedule_type_id' => $typeArray[$request->order_schedule], 'schedule_days' => $values_d];
                        $reorder_schedule_data = Product_reorder_schedule_detail::create($reorder_schedule);

                    }else if($request->order_schedule == 'multi-weekly'){
                        if(!empty($request->order_schedule_multi_weekly)){
                            foreach($request->order_schedule_multi_weekly as $weekly){
                                if ($weekly == 'Sun') $values_d = 1;
                                if ($weekly == 'Mon') $values_d = 2;
                                if ($weekly == 'Tue') $values_d = 3;
                                if ($weekly == 'Wed') $values_d = 4;
                                if ($weekly == 'Thu') $values_d = 5;
                                if ($weekly == 'Fri') $values_d = 6;
                                if ($weekly == 'Sat') $values_d = 7;
                                $reorder_schedule = ['product_id' => $request->product_id, 'product_assign_id'=>$assign_product_id, 'reorder_schedule_type_id' => $product_lead_time_id, 'reorder_schedule_type_id' => $typeArray[$request->order_schedule], 'schedule_days' => $values_d];
                                $reorder_schedule_data = Product_reorder_schedule_detail::create($reorder_schedule);
                            }
                        }

                    }else if($request->order_schedule == 'multi-monthly'){
                        if(!empty($request->order_schedule_multi_weekly)){
                            foreach($request->order_schedule_multi_monthly as $monthly){
                                $reorder_schedule = ['product_id' => $request->product_id, 'product_assign_id'=>$assign_product_id, 'reorder_schedule_type_id' => $product_lead_time_id, 'reorder_schedule_type_id' => $typeArray[$request->order_schedule], 'schedule_days' => $monthly];
                                $reorder_schedule_data = Product_reorder_schedule_detail::create($reorder_schedule);
                            }
                        }
                    }else{
                        $reorder_schedule = ['product_id' => $request->product_id, 'product_assign_id'=>$assign_product_id, 'reorder_schedule_type_id' => $product_lead_time_id, 'reorder_schedule_type_id' => $typeArray[$request->order_schedule]];
                        $reorder_schedule_data = Product_reorder_schedule_detail::create($reorder_schedule);

                    }
                }
            }else{
                $typeArray = ['on-demand'=>0 , 'weekly' => 1, 'multi-weekly' => 2, 'monthly' => 3, 'multi-monthly' => 4];

                //$check_exists = Lead_time::where(array('id' => $request->lead_time_id))->first();
                $check_exists = Product_custom_lead_time_table::where(array('product_id' =>$request->product_id,'product_assign_id'=>$assign_product_id))->first();
                if (!empty($check_exists)) {
                    $req_data = [
                        'order_volume_value' => $request->order_volume,
                        'order_volume_id' => $request->order_type == 'month' ? 2 : 1,
                    ];
                    $product_lead_time_id = $check_exists->id;
                    Product_custom_lead_time_table::where(array('id' => $check_exists->id))->update($req_data);
                    //$message = get_messages('vendor order setting updated successfully', 1);
                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = "product order setting updated successfully";
                } else {
                    // $res['success'] = false;
                    // $res['error'] = 1;
                    // $res['message'] = 'Please first complete lead time details';
                    // return response()->json($res);

                    // $vendor = Vendor::where('id', $request->vendor_id)->first();
                    $req_data = [
                        'product_id' => $request->product_id,
                        'product_assign_id'=>$assign_product_id,
                        'order_volume_value' => $request->order_volume,
                        'order_volume_id' => $request->order_type == 'month' ? 2 : 1,
                    ];

                    $lead_data = Product_custom_lead_time_table::create($req_data);
                    $userLastInsertId = $lead_data->toArray();
                    $product_lead_time_id = $userLastInsertId['id'];
                    //$message = get_messages('vendor order setting created successfully', 1);

                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = "product order setting created successfully";
                }
               //dd($product_lead_time_id);
                if($request->order_schedule != ''){
                    Product_custom_reorder_schedule_detail::where(array('product_id' => $request->product_id,'product_assign_id'=>$assign_product_id))->delete();
                    if($request->order_schedule == 'monthly'){

                        $reorder_schedule = ['product_id' => $request->product_id,'product_assign_id'=>$assign_product_id, 'reorder_schedule_type_id' => $product_lead_time_id, 'reorder_schedule_type_id' => $typeArray[$request->order_schedule], 'schedule_days' => $request->order_schedule_monthly];
                        $reorder_schedule_data = Product_custom_reorder_schedule_detail::create($reorder_schedule);

                    }else if($request->order_schedule == 'weekly'){
                        if ($request->order_schedule_weekly == 'Sun') $values_d = 1;
                        if ($request->order_schedule_weekly == 'Mon') $values_d = 2;
                        if ($request->order_schedule_weekly == 'Tue') $values_d = 3;
                        if ($request->order_schedule_weekly == 'Wed') $values_d = 4;
                        if ($request->order_schedule_weekly == 'Thu') $values_d = 5;
                        if ($request->order_schedule_weekly == 'Fri') $values_d = 6;
                        if ($request->order_schedule_weekly == 'Sat') $values_d = 7;

                        $reorder_schedule = ['product_id' => $request->product_id,'product_assign_id'=>$assign_product_id, 'reorder_schedule_type_id' => $product_lead_time_id, 'reorder_schedule_type_id' => $typeArray[$request->order_schedule], 'schedule_days' => $values_d];
                        //dd($reorder_schedule);
                        $reorder_schedule_data = Product_custom_reorder_schedule_detail::create($reorder_schedule);

                    }else if($request->order_schedule == 'multi-weekly'){
                        if(!empty($request->order_schedule_multi_weekly)){
                            foreach($request->order_schedule_multi_weekly as $weekly){
                                if ($weekly == 'Sun') $values_d = 1;
                                if ($weekly == 'Mon') $values_d = 2;
                                if ($weekly == 'Tue') $values_d = 3;
                                if ($weekly == 'Wed') $values_d = 4;
                                if ($weekly == 'Thu') $values_d = 5;
                                if ($weekly == 'Fri') $values_d = 6;
                                if ($weekly == 'Sat') $values_d = 7;
                                $reorder_schedule = ['product_id' => $request->product_id, 'product_assign_id'=>$assign_product_id, 'reorder_schedule_type_id' => $product_lead_time_id, 'reorder_schedule_type_id' => $typeArray[$request->order_schedule], 'schedule_days' => $values_d];
                                $reorder_schedule_data = Product_custom_reorder_schedule_detail::create($reorder_schedule);
                            }
                        }

                    }else if($request->order_schedule == 'multi-monthly'){
                        if(!empty($request->order_schedule_multi_weekly)){
                            foreach($request->order_schedule_multi_monthly as $monthly){
                                $reorder_schedule = ['product_id' => $request->product_id,'product_assign_id'=>$assign_product_id, 'reorder_schedule_type_id' => $product_lead_time_id, 'reorder_schedule_type_id' => $typeArray[$request->order_schedule], 'schedule_days' => $monthly];
                                $reorder_schedule_data = Product_custom_reorder_schedule_detail::create($reorder_schedule);
                            }
                        }
                    }else{
                        $reorder_schedule = ['product_id' => $request->product_id,'product_assign_id'=>$assign_product_id, 'reorder_schedule_type_id' => $product_lead_time_id, 'reorder_schedule_type_id' => $typeArray[$request->order_schedule]];
                        $reorder_schedule_data = Product_custom_reorder_schedule_detail::create($reorder_schedule);

                    }
                }
            }

            return response()->json($res);
        }

    }

    public function get_data(Request $request){
        if(isset($request->type) && $request->type == 'product_list'){
            $marketplace = session('MARKETPLACE_ID');

            ///$get_marketplace = Usermarketplace::where(array('user_id'=>Auth::id(),'country'=>$marketplace))->first();
            //if(!empty($get_marketplace)){
                $product_data = Mws_product::where(array('user_marketplace_id'=>$marketplace))->get()->toArray();
                $res['product_list'] = $product_data;
                $res['success'] = true;
                $res['error'] = 0;
            //}else{
                // $res['success'] = false;
                // $res['error'] = 1;
                // $res['message'] = 'marketplace not found';
            //}
            return response()->json($res);
        }
        if(isset($request->type) && $request->type == 'vendors_list'){
            //$marketplace = 'US';
            $marketplace = session('MARKETPLACE_ID');
            //$get_marketplace = Usermarketplace::where(array('user_id'=>Auth::id(),'country'=>$marketplace))->first();
            //if(!empty($get_marketplace)){
                $product_data = Vendor::where(array('user_marketplace_id'=>$marketplace))->get()->toArray();
                $res['vendors_list'] = $product_data;
                $res['success'] = true;
                $res['error'] = 0;
            // }else{
            //     $res['success'] = false;
            //     $res['error'] = 1;
            //     $res['message'] = 'marketplace not found';
            // }
            return response()->json($res);
        }
        if(isset($request->type) && $request->type == 'suppliers_list'){
           // $marketplace = 'US';
           $marketplace = session('MARKETPLACE_ID');
            // $get_marketplace = Usermarketplace::where(array('user_id'=>Auth::id(),'country'=>$marketplace))->first();
            // if(!empty($get_marketplace)){
                $product_data = Supplier::where(array('user_marketplace_id'=>$marketplace))->get()->toArray();
                $res['suppliers_list'] = $product_data;
                $res['success'] = true;
                $res['error'] = 0;
            // }else{
            //     $res['success'] = false;
            //     $res['error'] = 1;
            //     $res['message'] = 'marketplace not found';
            // }
            return response()->json($res);
        }

        if(isset($request->type) && $request->type == 'view_details'){
            $type = $request->supplier_vendor;
            $vendor_supplier_id = $request->supplier_vendor_id;
            if($type == 1) {
                $user_type = env('VENDOR');
                $basic_detail_data = Vendor::where(array('id'=>$vendor_supplier_id))->first();

                $basic_details['name'] = @$basic_detail_data->vendor_name;
                $basic_details['address_line_1'] = @$basic_detail_data->address_line_1;
                $basic_details['address_line_2'] = @$basic_detail_data->address_line_2;
                $basic_details['state'] = @$basic_detail_data->state;
                $basic_details['country_id'] = @$basic_detail_data->country_id;
                $basic_details['zipcode'] = @$basic_detail_data->zipcode;

                $contacts_data = Vendor_contact::where(array('vendor_id'=>$vendor_supplier_id))->get()->toArray();

                if(!empty($contacts_data)){
                    foreach($contacts_data as $key=>$con){
                        $contacts[$key]['id'] = @$con['id'];
                        $contacts[$key]['first_name'] = @$con['first_name'];
                        $contacts[$key]['last_name'] = @$con['last_name'];
                        $contacts[$key]['title'] = @$con['title'];
                        $contacts[$key]['phone_number'] = @$con['phone_number'];
                        $contacts[$key]['email'] = @$con['email'];
                        $contacts[$key]['primary'] = @$con['primary'];
                    }
                }
                $blackoutdate_data = Vendor_blackout_date::where(array('vendor_id'=>$vendor_supplier_id))->get()->toArray();
                if(!empty($blackoutdate_data)){
                    foreach($blackoutdate_data as $key=>$bdate ){
                        $blackoutdates[$key]['id'] = @$bdate['id'];
                        $blackoutdates[$key]['event_name'] = @$bdate['event_name'];
                        $blackoutdates[$key]['start_date'] = @$bdate['start_date'];
                        $blackoutdates[$key]['end_date'] = @$bdate['end_date'];
                        $blackoutdates[$key]['number_of_days'] = @$bdate['number_of_days'];
                        $blackoutdates[$key]['type'] = @$bdate['type'] == 1 ? 'production' : 'office';
                    }
                }
            }
            else {
                $user_type = env('SUPPLIER');
                $basic_detail_data = Supplier::where(array('id'=>$vendor_supplier_id))->first();

                $basic_details['name'] = @$basic_detail_data->supplier_name;
                $basic_details['address_line_1'] = @$basic_detail_data->address_line_1;
                $basic_details['address_line_2'] = @$basic_detail_data->address_line_2;
                $basic_details['state'] = @$basic_detail_data->state;
                $basic_details['country_id'] = @$basic_detail_data->country_id;
                $basic_details['zipcode'] = @$basic_detail_data->zipcode;

                $contacts_data = Supplier_contact::where(array('supplier_id'=>$vendor_supplier_id))->get()->toArray();

                if(!empty($contacts_data)){
                    foreach($contacts_data as $key=>$con){
                        $contacts[$key]['id'] = @$con['id'];
                        $contacts[$key]['first_name'] = @$con['first_name'];
                        $contacts[$key]['last_name'] = @$con['last_name'];
                        $contacts[$key]['title'] = @$con['title'];
                        $contacts[$key]['phone_number'] = @$con['phone_number'];
                        $contacts[$key]['email'] = @$con['email'];
                        $contacts[$key]['primary'] = @$con['primary'];
                    }
                }
                $blackoutdate_data = Supplier_blackout_date::where(array('supplier_id'=>$vendor_supplier_id))->get()->toArray();
                if(!empty($blackoutdate_data)){
                    foreach($blackoutdate_data as $key=>$bdate ){
                        $blackoutdates[$key]['id'] = @$bdate['id'];
                        $blackoutdates[$key]['event_name'] = @$bdate['event_name'];
                        $blackoutdates[$key]['start_date'] = @$bdate['start_date'];
                        $blackoutdates[$key]['end_date'] = @$bdate['end_date'];
                        $blackoutdates[$key]['number_of_days'] = @$bdate['number_of_days'];
                        $blackoutdates[$key]['type'] = @$bdate['type'] == 1 ? 'production' : 'office';
                    }
                }
            }

            $lead_time_data = Lead_time::where(array('supplier_vendor' => $user_type, 'supplier_vendor_id' => $vendor_supplier_id))->first();
            $order_setting_data = Reorder_schedule_detail::where(array('supplier_vendor' => $user_type, 'supplier_vendor_id' => $vendor_supplier_id))->get()->toArray();
            $payment_data = Payment::where(array('supplier_vendor' => $user_type, 'supplier_vendor_id' => $vendor_supplier_id))->first();
            //$shipping = Shipping::where(array('supplier_vendor' => $user_type, 'supplier_vendor_id' => $vendor_supplier_id))->first();

            if(@$lead_time_data->type == 1) $ladtimetype = 'boat';
            if(@$lead_time_data->type == 2) $ladtimetype = 'plane';
            if(@$lead_time_data->type == 0) $ladtimetype = 'domestic';

            if(!empty($lead_time_data)){
                $lead_time['id'] = @$lead_time_data->id;
                $lead_time['product_manuf_days'] = @$lead_time_data->product_manuf_days;
                $lead_time['to_port_days'] = @$lead_time_data->to_port_days;
                $lead_time['transit_time_days'] = @$lead_time_data->transit_time_days;
                $lead_time['to_warehouse_days'] = @$lead_time_data->to_warehouse_days;
                $lead_time['to_amazon'] = @$lead_time_data->to_amazon;
                $lead_time['po_to_production_days'] = @$lead_time_data->po_to_production_days;
                $lead_time['safety_days'] = @$lead_time_data->safety_days;
                $lead_time['total_lead_time_days'] = @$lead_time_data->total_lead_time_days;
                $lead_time['order_prep_days'] = @$lead_time_data->order_prep_days;
                $lead_time['po_to_prep_days'] = @$lead_time_data->po_to_prep_days;
                $lead_time['order_volume_value'] = @$lead_time_data->order_volume_value;
                $lead_time['order_volume_id'] = @$lead_time_data->order_volume_id;
                $lead_time['type'] = $ladtimetype;
            }



            if(!empty($order_setting_data)){
                $weeklyDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
                foreach($order_setting_data as $key=> $os_data){
                    if($os_data['reorder_schedule_type_id'] == 1) $type_name = 'Weekly';
                    elseif($os_data['reorder_schedule_type_id'] == 2) $type_name = 'Bi-Weekly';
                    elseif($os_data['reorder_schedule_type_id'] == 3) $type_name = 'Monthly';
                    elseif($os_data['reorder_schedule_type_id'] == 4) $type_name = 'Bi-Monthly';
                    else $type_name = 'On Demand';

                    $order_setting[$key]['schedule_type']=$type_name;
                    if($os_data['reorder_schedule_type_id']== '1' || $os_data['reorder_schedule_type_id'] == '2'){
                        $order_setting[$key]['schedule_days'] =  $weeklyDays[$key+1] ;
                    }else{
                        $order_setting[$key]['schedule_days'] = $os_data['schedule_days'];
                    }
                }
            }



            if(!empty($payment_data)){
                $get_datas = Payment_details::with(['term_type'])->where(array('payment_id'=>$payment_data->id))->get()->toArray();
                foreach($get_datas as $key=>$pdata){
                    $payment[$key]['id'] = $pdata['id'];
                    $payment[$key]['payment_terms'] = $pdata['payment_terms'] == 1 ? 'deposit' : 'balance';
                    $payment[$key]['percentage'] = $pdata['percentage'];
                    $payment[$key]['dues'] = $pdata['dues'];
                    $payment[$key]['days_before_after'] = $pdata['days_before_after'] == 1 ? 'before' : 'after';
                    $payment[$key]['payment_term_type_id'] = $pdata['term_type']['payment_type'];
                }
            }

            $shippingDetails = Shipping::with(['shipping_agent','shipping_carrier'])->where(array('supplier_vendor' => $user_type, 'supplier_vendor_id' => $vendor_supplier_id))->get()->first();
            // $shippingAgents = ShippingAgent::where(array('user_marketplace_id' => $global_marketplace))->get()->toArray();
            // $shippingCarrier = Shippingcarrier::where(array('user_marketplace_id' => $global_marketplace))->get()->toArray();
            $containerDetails = $shiptoAirDetails = $shiptoWarehouseDetails = $shipDeliveryDateDetails = $shipCutOffDeliveryDetails = '';
            if (!empty($shippingDetails)) {
                $containerDetails = ShippingContainerSettings::where('shipping_id', $shippingDetails->id)->get()->toArray();
                $shiptoAirDetails = Shippingair::where('shipping_id', $shippingDetails->id)->get()->toArray();
                $shiptoWarehouseDetails = ShipToWarehouse::where('shipping_id', $shippingDetails->id)->get()->toArray();
                $shipDeliveryDateDetails = ShippingDeliveryDetails::where('shipping_id', $shippingDetails->id)->get()->toArray();
                $shipCutOffDeliveryDetails = Shippingcutoffdeliverytime::where('shipping_id', $shippingDetails->id)->get()->toArray();
            }


            if(!empty($shippingDetails)){
                $shipping['id'] = @$shippingDetails->id;
                $shipping['logistic_type_id'] = @$shippingDetails->logistic_type_id;
                $shipping['shipping_agent'] = @$shippingDetails['shipping_agent']['company_name'];
                $shipping['shipping_carrier'] = @$shippingDetails['shipping_carrier']['carrier_name'];
                $shipping['shipping_type'] = @$shippingDetails->shipping_type;
                $shipping['tracking_provided'] = @$shippingDetails->tracking_provided;
                $shipping['lcl_cost_settings'] = @$shippingDetails->lcl_cost_settings;
                $shipping['lcl_cost_per'] = @$shippingDetails->lcl_cost_per;
                $shipping['lcl_cost'] = @$shippingDetails->lcl_cost;
                $shipping['duties_cost'] = @$shippingDetails->duties_cost;
                $shipping['direct_to_amazon'] = @$shippingDetails->direct_to_amazon;

                if(!empty($containerDetails)){
                    foreach($containerDetails as $key=>$container){
                        $shipping['container_details'][$key]['id'] = $container['id'];
                        $shipping['container_details'][$key]['container_size'] = $container['container_size'];
                        $shipping['container_details'][$key]['select_container'] = $container['select_container'];
                        $shipping['container_details'][$key]['cbm_per_container'] = $container['cbm_per_container'];
                        $shipping['container_details'][$key]['container_cost'] = $container['container_cost'];
                    }
                }

                if(!empty($shiptoAirDetails)){
                    foreach($shiptoAirDetails as $key=>$air){
                        $shipping['air_details'][$key]['id'] = $air['id'];
                        $shipping['air_details'][$key]['ship_type'] = $air['ship_type'];
                        $shipping['air_details'][$key]['price'] = $air['price'];
                        $shipping['air_details'][$key]['messuare_in'] = $air['messuare_in'];
                    }
                }

                if(!empty($shiptoWarehouseDetails)){
                    foreach($shiptoWarehouseDetails as $key=>$warehouse){
                        $shipping['warehouse_details'][$key]['id'] = $warehouse['id'];
                        $shipping['warehouse_details'][$key]['warehouse_value'] = $warehouse['warehouse_value'];
                    }
                }

                if(!empty($shipDeliveryDateDetails)){
                    foreach($shipDeliveryDateDetails as $key=>$date_details){
                        $shipping['delivery_details'][$key]['id'] = $date_details['id'];
                        $shipping['delivery_details'][$key]['delivery_schedule_type'] = $date_details['delivery_schedule_type'];
                        $shipping['delivery_details'][$key]['schedule_day'] = $date_details['schedule_day'];
                    }
                }

                if(!empty($shipCutOffDeliveryDetails)){
                    foreach($shipCutOffDeliveryDetails as $key=>$cut_off_details){
                        $shipping['cut_off_details'][$key]['id'] = @$cut_off_details['id'];
                        $shipping['cut_off_details'][$key]['shipping_delivery_id'] = @$cut_off_details['shipping_delivery_id'];
                        $shipping['cut_off_details'][$key]['cut_off_day'] = @$cut_off_details['cut_off_day'];
                        $shipping['cut_off_details'][$key]['cut_off_time'] = @$cut_off_details['cut_off_time'];
                    }
                }
            }


            $res['basic_details'] = isset($basic_details) ? $basic_details : '';
            $res['contacts'] = isset($contacts) ? $contacts : '';
            $res['blackoutdate'] = isset($blackoutdates) ? $blackoutdates : '';
            $res['leadtime'] = isset($lead_time) ? $lead_time : '';
            $res['ordersetting'] = isset($order_setting) ? $order_setting : '';
            $res['shipping'] = isset($shipping) ? $shipping : '';
            $res['payment'] = isset($payment) ? $payment : '';
            return response()->json($res);
        }
    }

    public function get_products_labels(){
        $global_marketplace = session('MARKETPLACE_ID');
        $get_all_label = Product_label::where('user_marketplace_id',$global_marketplace)->selectRaw('id,label_name')->orderBy('id','desc')->get();
        return response()->json($get_all_label);
    }

    public function apply_bulk_sources(Request $request){
        $request = [
                'sources' => [
                    '0'=>[
                    'product_id' => 1,
                    'source_type' => 'vendor',
                    'vendor_supplier_id' => 28,
                    'parts' => [
                        '0'=>[
                            'select_mpn' => 'test',
                            'qty' => 15,
                            'shipping_option' => 'ships_with_parent',
                        ]
                    ],
                ],
            ],
        ];

        $global_marketplace = session('MARKETPLACE_ID');
        if(!empty($request['sources'])){
            foreach($request['sources']  as $key=> $source){

                $product_id = $source['product_id'];
                $vendor_supplier = $source['source_type'] == 'vendor' ?  env('VENDOR') :  env('SUPPLIER');
                $vendor_supplier_id = $source['vendor_supplier_id'];
                    $array_schedule = array();
                    $array_schedule_type = '';
                    $get_lead_time = Lead_time::where(array('supplier_vendor'=>$vendor_supplier,'supplier_vendor_id'=>$vendor_supplier_id))->first();
                    $get_order_setting = Reorder_schedule_detail::where(array('supplier_vendor'=>$vendor_supplier,'supplier_vendor_id'=>$vendor_supplier_id))->get()->toArray();
                    if(!empty($get_order_setting)){
                        foreach($get_order_setting as $reorder){

                            if ($reorder['schedule_days'] == 1) $values_d = 'Sun';
                            if ($reorder['schedule_days'] == 2) $values_d = 'Mon';
                            if ($reorder['schedule_days'] == 3) $values_d = 'Tue';
                            if ($reorder['schedule_days'] == 4) $values_d = 'Wed';
                            if ($reorder['schedule_days'] == 5) $values_d = 'Thu';
                            if ($reorder['schedule_days'] == 6) $values_d = 'Fri';
                            if ($reorder['schedule_days'] == 7) $values_d = 'Sat';

                            if($reorder['reorder_schedule_type_id'] == '1'){
                                $array_schedule[]  = $values_d;
                                $array_schedule_type = 'weekly';
                            }else if($reorder['reorder_schedule_type_id'] == '2'){
                                $array_schedule[]  = $values_d;
                                $array_schedule_type = 'multi-weekly';
                            }else if($reorder['reorder_schedule_type_id'] == '3'){
                                $array_schedule[]  = $reorder['schedule_days'];
                                $array_schedule_type = 'monthly';
                            }else if($reorder['reorder_schedule_type_id'] == '4'){
                                $array_schedule[]  = $reorder['schedule_days'];
                                $array_schedule_type = 'multi-monthly';
                            }else{
                                $array_schedule[] = '';
                                $array_schedule_type = 'on-demand';
                            }
                        }
                    }
                    if(!empty($array_schedule)){
                        $array_schedule = implode(', ', $array_schedule);
                    }
                    //dd($get_lead_time);
                    $reqs[$key]['leadtime'] = [
                        'leadtime_id' =>$get_lead_time->id,
                        'product_manuf_days'=> $get_lead_time->product_manuf_days,
                        'to_port_days'=> $get_lead_time->to_port_days,
                        'transit_time_days'=> $get_lead_time->transit_time_days,
                        'to_warehouse_days'=> $get_lead_time->to_warehouse_days,
                        'to_amazon'=> $get_lead_time->to_amazon,
                        'po_to_production_days'=> $get_lead_time->po_to_production_days,
                        'safety_days'=> $get_lead_time->safety_days,
                        'order_prep_days'=> $get_lead_time->order_prep_days,
                        'po_to_prep_days'=>$get_lead_time->po_to_prep_days,
                        'total_lead_time_days'=> $get_lead_time->total_lead_time_days
                    ];

                    $reqs[$key]['order_setting'] = [
                        'order_volume_value'=>$get_lead_time->order_volume_value,
                        'order_volume_id'=>$get_lead_time->order_volume_id,
                        'schedule_type' =>$array_schedule_type,
                        'schedule' => $array_schedule,
                    ];

                    //dd($reqs);

                $req_label = [
                    'product_id' => $product_id,
                    'supplier_vendor' => $vendor_supplier,
                    'supplier_vendor_id' => $vendor_supplier_id,
                ];

                $datas_of = Product_assign::create($req_label);
                $product_assign = $datas_of->toArray();
                $product_assign_id = $product_assign['id'];
                if(!empty($source['parts'])){
                    foreach($source['parts'] as $parts){
                        $import_product = [
                            'product_id' => $product_id,
                            'prod_name' => $parts['select_mpn']
                        ];
                        $check_import_product = Mws_import_products::where(array('product_id'=>$product_id,'prod_name'=>$parts['select_mpn']))->first();
                        if(!empty($check_import_product)){
                            $product_import = Mws_import_products::where(array('id'=>$check_import_product->id))->update($import_product);
                            $product_import_id = $check_import_product->id;
                        }else{
                            $datas_product = Mws_import_products::create($import_product);
                            $product_import = $datas_product->toArray();
                            $product_import_id = $product_import['id'];
                        }

                        if($product_import_id != '' || $product_assign_id != ''){
                            $product_manage_mpn_qty = Product_manage_mpn_qty::where(array('product_import_id'=>$product_import_id,'product_assign_id'=>$product_assign_id))->first();
                            $req_data_mpn_qty = [
                                'product_import_id' => $product_import_id,
                                'product_assign_id' => $product_assign_id,
                                'qty' => $parts['qty'],
                                'shipping_option' => $parts['shipping_option'],
                            ];
                            if(!empty($product_manage_mpn_qty)){
                                Product_manage_mpn_qty::where(array('id'=>$product_manage_mpn_qty))->update($req_data_mpn_qty);
                            }else{
                                Product_manage_mpn_qty::create($req_data_mpn_qty);
                            }
                        }
                    }

                }
            }
        }
        $res['success'] = true;
        $res['request'] = $reqs;
        return response()->json($res);
    }

    // public function product_assign_to_label($label_id,$product_id){
    //         $global_marketplace = session('MARKETPLACE_ID');
    //         Product_assign_to_label::where(array('product_id'=>$product_id))->delete();
    //         $req_data = [
    //             'user_marketplace_id'=> $global_marketplace,
    //             'label_id'=> $label_id,
    //             'product_id'=> $product_id
    //         ];
    //         Product_assign_to_label::create($req_data);
    //         $res['success'] = true;
    //         return response()->json($res);
    //     }
    public function product_assign_to_label(Request $request){

        $global_marketplace = session('MARKETPLACE_ID');
        $newlabels = array();

        if($request->newLabel != ''){
            $newlabels = explode(',',$request->newLabel);
        }


       if(!empty($request->selected)){
           foreach($request->selected as $value){
                if($request->label  != '' || $request->label != 0){
                    //Product_assign_to_label::where(array('label_id'=>$request->label,'product_id'=>$value))->delete();
                    $check_assign = product_assign_to_label::where(array('label_id'=>$request->label,'product_id'=>$value))->first();
                    if(empty($check_assign)){
                        $req_datas = [
                            'user_marketplace_id'=> $global_marketplace,
                            'label_id'=> $request->label,
                            'product_id'=> $value
                        ];
                        Product_assign_to_label::create($req_datas);
                    }
               }
            }
            if(!empty($newlabels)){
                foreach($newlabels as $newl){
                    foreach($request->selected as $value){
                       $checklabels = Product_label:: where(array('user_marketplace_id'=>$global_marketplace,'label_name'=>trim($newl)))->first();
                       if(empty($checklabels)){
                           $req_label = [
                               'user_marketplace_id' =>$global_marketplace,
                               'label_name' => $newl,
                           ];
                           $datas_of = Product_label::create($req_label);
                           $userLastInsertId = $datas_of->toArray();
                            $check_assign = product_assign_to_label::where(array('label_id'=>$userLastInsertId['id'],'product_id'=>$value))->first();
                            if(empty($check_assign)){
                                $req_data1 = [
                                    'user_marketplace_id'=> $global_marketplace,
                                    'label_id'=> $userLastInsertId['id'],
                                    'product_id'=> $value
                                ];
                                Product_assign_to_label::create($req_data1);
                            }
                       }else{
                        $check_assign = product_assign_to_label::where(array('label_id'=>$checklabels->id,'product_id'=>$value))->first();
                        if(empty($check_assign)){
                            $req_data2 = [
                                'user_marketplace_id'=> $global_marketplace,
                                'label_id'=> $checklabels->id,
                                'product_id'=> $value
                            ];
                            Product_assign_to_label::create($req_data2);
                        }
                       }
                   }
               }
           }
       }
    //    if($request->label  != '' || $request->label != 0){
    //         $check_assign = Product_label::where(array('id'=>$request->label))->first();
    //         if(!empty($newlabels)){
    //             array_push($newlabels,$check_assign->label_name);
    //         }else{
    //             $newlabels[] = $check_assign->label_name;
    //         }
    //     }

    //    $labels = array();

    //    if(!empty($newlabels)){
    //        foreach($newlabels as $key => $newq){
    //            $labels['labels'][$key]['slug'] = $newq;
    //            $labels['labels'][$key]['text'] = $newq;
    //        }
    //    }

        $res['success'] = true;
        return response()->json($res);
    }

    /*
        delete label for particular product
    */

    public function delete_bulk_label(Request $request){
        $global_marketplace = session('MARKETPLACE_ID');

        if($request->label != ''){
            $checklabels = Product_label:: where(array('user_marketplace_id'=>$global_marketplace,'label_name'=>$request->label))->first();
             if(!empty($checklabels)){
               Product_assign_to_label::where(array('label_id'=>$checklabels->id,'product_id'=>$request->product_id))->delete();
            }
            $res['success'] = true;
        }else{
            $res['success'] = false;
        }

        return response()->json($res);
    }

    /*
        add label for the product
    */

    public function product_add_label(Request $request){
        $global_marketplace = session('MARKETPLACE_ID');
        if($request->label != ''){
            $checklabels = Product_label:: where(array('user_marketplace_id'=>$global_marketplace,'label_name'=>trim($request->label)))->first();
            if(empty($checklabels)){
                $req_label = [
                    'user_marketplace_id' =>$global_marketplace,
                    'label_name' => trim($request->label),
                ];
                $datas_of = Product_label::create($req_label);
                $userLastInsertId = $datas_of->toArray();
                $check_assign = product_assign_to_label::where(array('label_id'=>$userLastInsertId['id'],'product_id'=>$request->product_id))->first();
                if(empty($check_assign)){
                    $req_data1 = [
                        'user_marketplace_id'=> $global_marketplace,
                        'label_id'=> $userLastInsertId['id'],
                        'product_id'=> $request->product_id,
                    ];
                    Product_assign_to_label::create($req_data1);
                }
            }else{
                $check_assign = product_assign_to_label::where(array('label_id'=>$checklabels->id,'product_id'=>$request->product_id))->first();
                if(empty($check_assign)){
                    $req_data1 = [
                        'user_marketplace_id'=> $global_marketplace,
                        'label_id'=> $checklabels->id,
                        'product_id'=> $request->product_id,
                    ];
                    Product_assign_to_label::create($req_data1);
                }
            }
            $res['success'] = true;
        }else{
            $res['success'] = false;
            $res['message'] = 'label must be required';
        }
        return response()->json($res);
    }

    /*
        get labels for the product
    */

    public function get_labels_for_product(Request $request){
        $product_id = $request->product_id;
        $get_label = Product_assign_to_label::with(['labels'])->where(array('product_id'=>$product_id))->get();
        $label_list = array();
        if(!empty($get_label)){
            foreach($get_label as $list){
                $label_list[] = $list['labels']['label_name'];
            }
        }
        return response()->json($label_list);
    }

    /*
        get all sources for particular product
    */

    public function get_sources(Request $request){
        $product_id = $request->product_id;
        $get_sources = array();
        $get_all_source = product_assign::where(array('product_id'=>$product_id))->get();
        if(!empty($get_all_source)){
            foreach($get_all_source as $value){
                if($value['supplier_vendor_id'] != ''){
                    $users_type = $value['supplier_vendor'] == 1 ? 'suppliers' : 'vendors';
                    $get_name = get_vendor_supplier_name($users_type,$value['supplier_vendor_id']);
                    $get_sources[] = $get_name;
                }
            }
        }
        return response()->json($get_sources);
    }

    /*
        insert update the sources for particular product
    */

    public function insert_update_sources(Request $request){
        $global_marketplace = session('MARKETPLACE_ID');
        Product_assign::where(array('product_id'=>$request->product_id,'supplier_vendor_id'=>$request->supplier_vendor_id,'supplier_vendor'=>$request->supplier_vendor))->delete();
        $req_data = [
            'product_id'=> $request->product_id,
            'supplier_vendor'=> $request->supplier_vendor,
            'supplier_vendor_id'=> $request->supplier_vendor_id
        ];
        Product_assign::create($req_data);
        $res['success'] = true;
        return response()->json($res);
    }

    /**
     * There are 5 function added in store method :
     *      @method POST
     *
     *  Request Types:
     *
     * 1. assign_product : Assign sources to product
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     * 2. profit_analysis: Add or update profit analysis
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     * 3. product_dimensions: Add or update product dimensions
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     * 4. insert_update_lead_time: Add or update lead time for particular product
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     *
     * 5. insert_update_order_setting: Add or update order setting for particular product
     *      @param  \Illuminate\Http\Response $request
     *      @return \Illuminate\Http\Response
     **/


    public function crud_process(Request $request){
        if(isset($request->type) && $request->type == 'assign_product'){
            if($request->supplier_vendor_id != '' && $request->product_id != ''){
                $req_data = [
                    'product_id'=>$request->product_id,
                    'supplier_vendor'=>$request->supplier_vendor,
                    'supplier_vendor_id'=>$request->supplier_vendor_id,
                ];

                $check_data = Product_assign::where(array('product_id'=>$request->product_id))->first();
                if(!empty($check_data)){
                    Product_assign::where(array('id'=>$check_data->id))->update($req_data);
                }else{
                    Product_assign::create($req_data);
                }

                $res['success'] = true;
                $res['error'] = 0;
                $res['message'] = 'product assign successfully';
            }else{
                $res['success'] = false;
                $res['error'] = 1;
                $res['message'] = 'fileds are missing';
            }
            return response()->json($res);
        }
        if(isset($request->type) && $request->type == 'profit_analysis'){
            if($request->product_id != ''){
                $req_data = [
                    'product_id'=>$request->product_id,
                    'unit_cost'=>$request->unit_cost,
                    'shipping'=>$request->shipping,
                    'customs'=>$request->customs,
                    'other_landed_cost'=>$request->other_pc,
                    'total_landed'=>$request->total_landed,
                    'warehouse_storage'=>$request->warehouse_storage,
                    'inbound_shipping'=>$request->inbound_shipping,
                    'other_inland_cost'=>$request->other_ic,
                    'total_inland'=>$request->total_inland,
                    'fba_fee'=>$request->fba,
                    'referal_fee'=>$request->referral,
                    'storage_fee'=>$request->storage,
                    'total_amazon_fee'=>$request->total_amz_fees,
                    'total_costs'=>$request->total_costs,
                    'price_profit'=>$request->price_point,
                    'total_profit'=>$request->total_profit,
                    'profit_margin'=>$request->profit_margin,
                    'roi'=>$request->roi,
                ];

                $check_data = Product_profit_analysis::where(array('product_id'=>$request->product_id))->first();
                if(!empty($check_data)){
                    Product_profit_analysis::where(array('id'=>$check_data->id))->update($req_data);
                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = 'product profit analysis updated successfully';
                }else{
                    Product_profit_analysis::create($req_data);
                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = 'product profit analysis created successfully';
                }

            }else{
                $res['success'] = false;
                $res['error'] = 1;
                $res['message'] = 'product id is missing';
            }
            return response()->json($res);

        }
        if(isset($request->type) && $request->type == 'product_dimensions'){

            //dd($request->all());

            if($request->product_id != ''){

                $req_data = [
                    'product_id'=>$request->product_id,

                    'item_height'=>$request->item_height,
                    'item_width'=>$request->item_width,
                    'item_length'=>$request->item_length,
                    'size_in'=>$request->size_in,
                    'item_weight'=>$request->item_weight,
                    'weigth_in'=>$request->weigth_in,

                    'box_qty'=>$request->box_qty,
                    'box_height'=>$request->box_height,
                    'box_width'=>$request->box_width,
                    'box_length'=>$request->box_length,
                    'box_size_in'=>$request->box_size_in,
                    'box_weight'=>$request->box_weight,
                    'box_volume'=>$request->box_volume,
                    'box_volumn_in'=>$request->box_volumn_in,

                    'box_par_carton_qty'=>$request->box_par_carton_qty,
                    'box_par_carton_height'=>$request->box_par_carton_height,
                    'box_par_carton_width'=>$request->box_par_carton_width,
                    'box_par_carton_length'=>$request->box_par_carton_length,
                    'box_par_carton_size_in'=>$request->box_par_carton_size_in,
                    'box_par_carton_weight'=>$request->box_par_carton_weight,
                    'box_par_carton_volume'=>$request->box_par_carton_volume,
                    'box_par_carton_volumn_in'=>$request->box_par_carton_volumn_in,

                    'cartons_par_pallet_qty'=>$request->cartons_par_pallet_qty,
                    'cartons_par_pallet_height'=>$request->cartons_par_pallet_height,
                    'cartons_par_pallet_width'=>$request->cartons_par_pallet_width,
                    'cartons_par_pallet_length'=>$request->cartons_par_pallet_length,
                    'cartons_par_pallet_size_in'=>$request->cartons_par_pallet_size_in,
                    'cartons_par_pallet_weight'=>$request->cartons_par_pallet_weight,
                    'cartons_par_pallet_volume'=>$request->cartons_par_pallet_volume,
                    'cartons_par_pallet_volumn_in'=>$request->cartons_par_pallet_volumn_in,

                ];

                $check_data = Product_dimensions::where(array('product_id'=>$request->product_id))->first();
                if(!empty($check_data)){
                    Product_dimensions::where(array('id'=>$check_data->id))->update($req_data);
                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = 'product dimensions updated successfully';
                }else{
                    Product_dimensions::create($req_data);
                    $res['success'] = true;
                    $res['error'] = 0;
                    $res['message'] = 'product dimensions created successfully';
                }

            }else{
                $res['success'] = false;
                $res['error'] = 1;
                $res['message'] = 'product id is missing';
            }
            return response()->json($res);

        }

        if (isset($request->type) && $request->type == 'insert_update_lead_time') {
            if(empty($request->product_id)){
                $res_arr['success'] = false;
                $res_arr['error'] = 1;
                $res_arr['message'] = 'Product not found';
                return response()->json($res_arr);
            }

            if(isset($request->total_lead_time_days_boat)){
                $validator = Validator::make($request->all(), [
                    'product_manuf_days_boat' => 'required',
                    'to_port_days_boat' => 'required',
                    'transit_time_days_boat'=>'required',
                    'to_warehouse_days_boat'=>'required',
                    'to_amazon_boat'=>'required',
                    'po_to_production_days_boat'=>'required',
                    'safety_days_boat'=>'required',
                    'total_lead_time_days_boat'=>'required'
                ]);
                if ($validator->fails()) {
                    $res['success'] = false;
                    $res['error'] = 1;
                    $res['message'] = 'Please provide required field data';
                    return response()->json($res);
                }else {
                    $product_manuf_days = isset($request->product_manuf_days_boat) ? $request->product_manuf_days_boat : 0;
                    $to_port_days = isset($request->to_port_days_boat) ? $request->to_port_days_boat : 0;
                    $transit_time_days = isset($request->transit_time_days_boat) ? $request->transit_time_days_boat : 0;
                    $to_warehouse_days = isset($request->to_warehouse_days_boat) ? $request->to_warehouse_days_boat : 0;
                    $to_amazon = isset($request->to_amazon_boat) ? $request->to_amazon_boat : 0;
                    $po_to_production_days = isset($request->po_to_production_days_boat) ? $request->po_to_production_days_boat : 0;
                    $safety_days = isset($request->safety_days_boat) ? $request->safety_days_boat : 0;
                    $total_lead_time_days = isset($request->total_lead_time_days_boat) ? $request->total_lead_time_days_boat : 0;
                    $order_prep_days = isset($request->order_prep_days_boat) ? $request->order_prep_days_boat : 0;
                    $po_to_prep_days = isset($request->po_to_prep_days_boat) ? $request->po_to_prep_days_boat : 0;
                    $type = "1";
                }
            } else if(isset($request->total_lead_time_days_plane)){
                $validator = Validator::make($request->all(), [
                    'product_manuf_days_plane' => 'required',
                    'to_port_days_plane' => 'required',
                    'transit_time_days_plane'=>'required',
                    'to_warehouse_days_plane'=>'required',
                    'to_amazon_plane'=>'required',
                    'po_to_production_days_plane'=>'required',
                    'safety_days_plane'=>'required',
                    'total_lead_time_days_plane'=>'required'
                ]);
                if ($validator->fails()) {
                    $res['success'] = false;
                    $res['error'] = 1;
                    $res['message'] = 'Please provide required field data';
                    return response()->json($res);
                }else {
                    $product_manuf_days = isset($request->product_manuf_days_plane) ? $request->product_manuf_days_plane : 0;
                    $to_port_days = isset($request->to_port_days_plane) ? $request->to_port_days_plane : 0;
                    $transit_time_days = isset($request->transit_time_days_plane) ? $request->transit_time_days_plane : 0;
                    $to_warehouse_days = isset($request->to_warehouse_days_plane) ? $request->to_warehouse_days_plane : 0;
                    $to_amazon = isset($request->to_amazon_plane) ? $request->to_amazon_plane : 0;
                    $po_to_production_days = isset($request->po_to_production_days_plane) ? $request->po_to_production_days_plane : 0;
                    $safety_days = isset($request->safety_days_plane) ? $request->safety_days_plane : 0;
                    $total_lead_time_days = isset($request->total_lead_time_days_plane) ? $request->total_lead_time_days_plane : 0;
                    $order_prep_days = isset($request->order_prep_days_plane) ? $request->order_prep_days_plane : 0;
                    $po_to_prep_days = isset($request->po_to_prep_days_plane) ? $request->po_to_prep_days_plane : 0;
                    $type = "2";
                }
            } else if(isset($request->total_lead_time_days)){
                $validator = Validator::make($request->all(), [
                    'order_prep_days' => 'required',
                    'to_warehouse_days'=>'required',
                    'to_amazon'=>'required',
                    'po_to_prep_days'=>'required',
                    'safety_days'=>'required',
                    'total_lead_time_days'=>'required'
                ]);
                if ($validator->fails()) {
                    $res['success'] = false;
                    $res['error'] = 1;
                    $res['message'] = 'Please provide required field data';
                    return response()->json($res);
                } else {
                    $product_manuf_days = isset($request->product_manuf_days) ? $request->product_manuf_days : 0;
                    $to_port_days = isset($request->to_port_days) ? $request->to_port_days : 0;
                    $transit_time_days = isset($request->transit_time_days) ? $request->transit_time_days : 0;
                    $to_warehouse_days = isset($request->to_warehouse_days) ? $request->to_warehouse_days : 0;
                    $to_amazon = isset($request->to_amazon) ? $request->to_amazon : 0;
                    $po_to_production_days = isset($request->po_to_production_days) ? $request->po_to_production_days : 0;
                    $safety_days = isset($request->safety_days) ? $request->safety_days : 0;
                    $total_lead_time_days = isset($request->total_lead_time_days) ? $request->total_lead_time_days : 0;
                    $order_prep_days = isset($request->order_prep_days) ? $request->order_prep_days : 0;
                    $po_to_prep_days = isset($request->po_to_prep_days) ? $request->po_to_prep_days : 0;
                    $type = "0";
                }
            }
            /*todo*/
            $req_data = [
                'product_id' => $request->product_id,
                'product_assign_id' => $request->product_assign_id,
                'product_manuf_days' => $product_manuf_days,
                'to_port_days' => $to_port_days,
                'transit_time_days' => $transit_time_days,
                'to_warehouse_days' => $to_warehouse_days,
                'to_amazon' => $to_amazon,
                'po_to_production_days' => $po_to_production_days,
                'safety_days' => $safety_days,
                'total_lead_time_days' => $total_lead_time_days,
                'order_prep_days' => $order_prep_days,
                'po_to_prep_days' => $po_to_prep_days,
                'type' => $type,
            ];

            $check_exists = Product_lead_time::where(array('product_id' => $request->product_id, 'product_assign_id' => $request->product_assign_id))->first();
            if (!empty($check_exists)) {
                Product_lead_time::where(array('id' => $check_exists->id))->update($req_data);
                //$message = get_messages('product lead time updated successfully', 1);
                $res['product_id'] = $request->product_id;
                $res['error'] = 0;
                $res['success'] = true;
                $res['message'] = "product lead time updated successfully";
                $res['lead_time_id'] = $check_exists->id;
            } else {
                $lead_data = Product_lead_time::create($req_data);
                $res['product_id'] = $request->product_id;
                $res['error'] = 0;
                $res['success'] = true;
                $res['message'] = "product lead time created successfully";
                $res['lead_time_id'] = $lead_data->id;
            }

            if ($res['lead_time_id'] > 0) {
                Product_lead_time_value::where(array('lead_time_id' => $res['lead_time_id']))->delete();
                if($type==1 || $type==2){
                    if(isset($request->emailing['production'])){
                        foreach ($request->emailing['production'] as $key=>$production){
                            $lead_time_value_data = [
                                'lead_time_id' => $res['lead_time_id'],
                                'lead_time_detail_id' => $key+1,
                                'send_mail' => '1',
                                'no_of_days' => @$production['days'],
                                'contact_detail' => (isset($production['email'])) && is_array($production['email']) ? implode(",", array_unique(@$production['email'])) : '',
                            ];
                            //Lead_time_value::create($lead_time_value_data);
                            if(is_array(@$production['email'])){
                                Product_lead_time_value::create($lead_time_value_data);
                            }
                        }
                    }
                    if(isset($request->emailing['order_prep'])){
                        foreach ($request->emailing['order_prep'] as $key=>$production){
                            $lead_time_value_data = [
                                'lead_time_id' => $res['lead_time_id'],
                                'lead_time_detail_id' => $key+1,
                                'send_mail' => '1',
                                'no_of_days' => @$production['days'],
                                'contact_detail' => (isset($production['email'])) && is_array($production['email']) ? implode(",", array_unique(@$production['email'])) : '',
                            ];
                            //Lead_time_value::create($lead_time_value_data);
                            if(is_array(@$production['email'])){
                                Product_lead_time_value::create($lead_time_value_data);
                            }
                        }
                    }
                    if(isset($request->emailing['port_departure'])){
                        foreach ($request->emailing['port_departure'] as $key=>$port_departure){
                            $lead_time_value_data = [
                                'lead_time_id' => $res['lead_time_id'],
                                'lead_time_detail_id' => $key+3,
                                'send_mail' => '1',
                                'no_of_days' => @$port_departure['days'],
                                'contact_detail' => (isset($port_departure['email'])) && is_array($port_departure['email']) ? implode(",", array_unique(@$port_departure['email'])) : '',
                            ];
                            if(is_array(@$port_departure['email'])){
                                Product_lead_time_value::create($lead_time_value_data);
                            }
                            //Lead_time_value::create($lead_time_value_data);
                        }
                    }
                    if(isset($request->emailing['in_transit'])){
                        foreach ($request->emailing['in_transit'] as $key=>$in_transit){
                            $lead_time_value_data = [
                                'lead_time_id' => $res['lead_time_id'],
                                'lead_time_detail_id' => $key+4,
                                'send_mail' => '1',
                                'no_of_days' => @$in_transit['days'],
                                'contact_detail' => (isset($in_transit['email'])) && is_array($in_transit['email']) ? implode(",", array_unique(@$in_transit['email'])) : '',
                            ];
                            if(is_array(@$in_transit['email'])){
                                Product_lead_time_value::create($lead_time_value_data);
                            }
                            //Lead_time_value::create($lead_time_value_data);
                        }
                    }
                    if(isset($request->emailing['to_warehouse'])){
                        foreach ($request->emailing['to_warehouse'] as $key=>$to_warehouse){
                            $lead_time_value_data = [
                                'lead_time_id' => $res['lead_time_id'],
                                'lead_time_detail_id' => $key+5,
                                'send_mail' => '1',
                                'no_of_days' => @$to_warehouse['days'],
                                'contact_detail' => (isset($to_warehouse['email'])) && is_array($to_warehouse['email']) ? implode(",", array_unique(@$to_warehouse['email'])) : '',
                            ];
                            if(is_array(@$to_warehouse['email'])){
                                Product_lead_time_value::create($lead_time_value_data);
                            }
                            //Lead_time_value::create($lead_time_value_data);
                        }
                    }
                } else{
                    if(isset($request->emailing['order_prep'])) {
                        foreach ($request->emailing['order_prep'] as $key => $order_prep) {
                            $lead_time_value_data = [
                                'lead_time_id' => $res['lead_time_id'],
                                'lead_time_detail_id' => $key + 8,
                                'send_mail' => '1',
                                'no_of_days' => @$order_prep['days'],
                                'contact_detail' => (isset($order_prep['email'])) && is_array($order_prep['email']) ?  implode(",", array_unique(@$order_prep['email'])) : '',
                            ];
                            if(is_array(@$order_prep['email'])){
                                Product_lead_time_value::create($lead_time_value_data);
                            }
                            //Lead_time_value::create($lead_time_value_data);
                        }
                    }
                    if(isset($request->emailing['ship_confirmation'])){
                        foreach ($request->emailing['ship_confirmation'] as $key=>$ship_confirm){
                            $lead_time_value_data = [
                                'lead_time_id' => $res['lead_time_id'],
                                'lead_time_detail_id' => $key+9,
                                'send_mail' => '1',
                                'no_of_days' => @$ship_confirm['days'],
                                'contact_detail' => (isset($ship_confirm['email'])) && is_array($ship_confirm['email']) ?  implode(",", array_unique(@$ship_confirm['email'])) : '',
                            ];
                            if(is_array(@$ship_confirm['email'])){
                                Product_lead_time_value::create($lead_time_value_data);
                            }
                            //Lead_time_value::create($lead_time_value_data);
                        }
                    }
                    if(isset($request->emailing['to_warehouse'])){
                        foreach ($request->emailing['to_warehouse'] as $key=>$to_warehouse){
                            $lead_time_value_data = [
                                'lead_time_id' => $res['lead_time_id'],
                                'lead_time_detail_id' => $key+10,
                                'send_mail' => '1',
                                'no_of_days' => @$to_warehouse['days'],
                                'contact_detail' => (isset($to_warehouse['email'])) && is_array($to_warehouse['email']) ? implode(",", array_unique(@$to_warehouse['email'])) : '',
                            ];
                            if(is_array(@$to_warehouse['email'])){
                                Product_lead_time_value::create($lead_time_value_data);
                            }
                            //Lead_time_value::create($lead_time_value_data);
                        }
                    }
                }
            }
            return response()->json($res);
        }
        if (isset($request->type) && $request->type == 'insert_update_order_setting') {
            //$request->setting_type == 'custom';
           //dd($request->all);
            if(empty($request->product_id)){
                $res_arr['success'] = false;
                $res_arr['error'] = 1;
                $res_arr['message'] = 'Product not found';
                return response()->json($res_arr);
            }
            $message = get_messages('Something wrong', 0);
            $res['success'] = false;
            $res['message'] = $message;
            $res['product_id'] = $request->product_id;
            // dump($request->all());
            if($request->reorder_schedule == 'on_demand'){
                $validator = Validator::make($request->all(), [
                    'order_volume' => 'required',
                    'order_volume_type'=>'required'
                ]);
            }else{
                $validator = Validator::make($request->all(), [
                    'order_volume' => 'required',
                    'order_volume_type'=>'required',
                    'days'=>'required'
                ]);
            }

            if ($validator->fails()) {
                $res['success'] = false;
                $res['error'] = 1;
                $res['message'] = 'Please provide required field data';
                return response()->json($res);
            } else {
                $typeArray = ['weekly' => 1, 'bi_weekly' => 2, 'monthly' => 3, 'bi_monthly' => 4];
                if($request->setting_type == 'default'){
                    $check_exists = Product_lead_time::where(array('product_id' => $request->product_id, 'product_assign_id' => $request->product_assign_id))->first();
                    if (!empty($check_exists)) {
                        $req_data = [
                            'order_volume_value' => $request->order_volume,
                            'order_volume_id' => $request->order_volume_type
                        ];
                        Product_lead_time::where(array('id' => $check_exists->id))->update($req_data);
                        $res['success'] = true;
                        $res['error'] = 0;
                        $res['message'] = "product order setting updated successfully";
                        $res['lead_time_id'] = $check_exists->id;
                        $res['order_volume'] = $request->order_volume;
                        $res['product_id'] = $request->product_id;
                    }else{
                        $res['success'] = false;
                        $res['error'] = 1;
                        $res['message'] = 'Please first complete lead time details';
                        return response()->json($res);

                        $req_data = [
                            'product_id' => $request->product_id,
                            'product_assign_id' => $request->product_assign_id,
                            'type' => $request->type,
                            'order_volume_value' => $request->order_volume,
                            'order_volume_id' => $request->order_volume_type
                        ];

                        $lead_data = Product_lead_time::create($req_data);

                        $res['success'] = true;
                        $res['error'] = 0;
                        $res['message'] = "product order setting created successfully";
                        $res['lead_time_id'] = $lead_data->id;
                        $res['product_id'] = $request->product_id;
                    }

                    if (!empty($request->days)) {
                        Product_reorder_schedule_detail::where(array('product_id' =>  $request->product_id, 'product_assign_id' => $request->product_assign_id))->delete();
                        foreach ($request->days as $schedule_type) {
                            if (is_numeric($schedule_type)) {
                                $values_d = $schedule_type;
                            } else {
                                if ($schedule_type == 'Sun') $values_d = 1;
                                if ($schedule_type == 'Mon') $values_d = 2;
                                if ($schedule_type == 'Tue') $values_d = 3;
                                if ($schedule_type == 'Wed') $values_d = 4;
                                if ($schedule_type == 'Thu') $values_d = 5;
                                if ($schedule_type == 'Fri') $values_d = 6;
                                if ($schedule_type == 'Sat') $values_d = 7;
                            }
                            $reorder_schedule = ['product_id' => $request->product_id, 'product_assign_id' => $request->product_assign_id, 'reorder_schedule_type_id' => $typeArray[$request->reorder_schedule], 'schedule_days' => $values_d];
                            $reorder_schedule_data = Product_reorder_schedule_detail::create($reorder_schedule);
                        }
                    }
                }else{
                    $check_exists = Product_custom_lead_time_table::where(array('product_id' => $request->product_id))->first();
                    if (!empty($check_exists)) {
                        $req_data = [
                            'order_volume_value' => $request->order_volume,
                            'order_volume_id' => $request->order_volume_type
                        ];
                        Product_custom_lead_time_table::where(array('id' => $check_exists->id))->update($req_data);
                        $res['success'] = true;
                        $res['error'] = 0;
                        $res['message'] = "product order setting updated successfully";
                        $res['lead_time_id'] = $check_exists->id;
                        $res['order_volume'] = $request->order_volume;
                        $res['product_id'] = $request->product_id;
                    }else{
                        $res['success'] = false;
                        $res['error'] = 1;
                        $res['message'] = 'Please first complete lead time details';
                        return response()->json($res);

                        $req_data = [
                            'product_id' => $request->product_id,
                            'type' => $request->type,
                            'order_volume_value' => $request->order_volume,
                            'order_volume_id' => $request->order_volume_type
                        ];

                        $lead_data = Product_custom_lead_time_table::create($req_data);

                        $res['success'] = true;
                        $res['error'] = 0;
                        $res['message'] = "product order setting created successfully";
                        $res['lead_time_id'] = $lead_data->id;
                        $res['product_id'] = $request->product_id;
                    }

                    if (!empty($request->days)) {
                        Product_custom_reorder_schedule_detail::where(array('product_id' =>  $request->product_id, 'product_assign_id' => $request->product_assign_id))->delete();
                        foreach ($request->days as $schedule_type) {
                            if (is_numeric($schedule_type)) {
                                $values_d = $schedule_type;
                            } else {
                                if ($schedule_type == 'Sun') $values_d = 1;
                                if ($schedule_type == 'Mon') $values_d = 2;
                                if ($schedule_type == 'Tue') $values_d = 3;
                                if ($schedule_type == 'Wed') $values_d = 4;
                                if ($schedule_type == 'Thu') $values_d = 5;
                                if ($schedule_type == 'Fri') $values_d = 6;
                                if ($schedule_type == 'Sat') $values_d = 7;
                            }
                            $reorder_schedule = ['product_id' => $request->product_id, 'product_assign_id' => $request->product_assign_id, 'reorder_schedule_type_id' => $typeArray[$request->reorder_schedule], 'schedule_days' => $values_d];
                            $reorder_schedule_data = Product_custom_reorder_schedule_detail::create($reorder_schedule);
                        }
                    }
                }
            }
            return response()->json($res);
        }

    }
    function logiccal(){
        $marketplace_id = 1;
        try {
            Log::debug("Executing job CalculateTrend for account {$marketplace_id}");
            $fetchProducts = Mws_product::where('user_marketplace_id', $marketplace_id)->get()->toArray();
            foreach ($fetchProducts as $product) {
                $amazonInventory = Mws_unsuppressed_inventory_data::where('product_id', $product['id'])->first();
                if($amazonInventory) {
                    $wareHouseInventory = Warehouse_product::where('product_id', $product['id'])->get();
                    $warehouseInventoryTotal = $wareHouseLeadTime = 0;
                    $logicCalculations = Product_assign_supply_logic::with(['supply_reoder_logic'])->where('product_id', $product['id'])->first();
                    if (empty($logicCalculations)) {
                        $logicCalculations = Default_logic_setting::where('user_marketplace_id', $product['user_marketplace_id'])->first();
                    }
                    if (isset($wareHouseInventory[0])) {
                        foreach ($wareHouseInventory as $warehouse) {
                            $warehouseInventoryTotal += $warehouse->qty;
                            $leadTimeWarehouse = Warehouse_wise_default_setting::where('id', $warehouse->warehouse_id)->first();
                            if ($wareHouseLeadTime < $leadTimeWarehouse->lead_time) {
                                $wareHouseLeadTime = $leadTimeWarehouse->lead_time;
                            }
                        }
                    }
                    $vendor = env('VENDOR');
                    $supplier = env('SUPPLIER');
                    $blackOutDayFlat = 0;
                    $productSettings = Product_assign::where('product_id', $product['id'])->first();

                    if(!empty($productSettings)){
                        $blackOutDayFlat = $productSettings->supplier_vendor == $vendor ? 0 : 1 ;
                        $productSettings = Lead_time::where(array('supplier_vendor'=>$productSettings->supplier_vendor,'supplier_vendor_id'=>$productSettings->supplier_vendor_id))->first();
                    }else{
                        $productSettings = Default_leadtime_setting::first();
                        $blackOutDayFlat = 2;
                    }

                    if ($productSettings) {
                        $fetchPO = Purchase_orders::where('user_marketplace_id', $marketplace_id)->where('reorder_date','>=',Carbon::now()->format('Y-m-d'));
                        $fetchProductPOInventory = 0;
                        foreach($fetchPO as $poDetails){
                            $fetchProductPODetail = Purchase_order_details::where('purchase_orders_id',$poDetails->id)->where('product_id',$product['id'])->first();
                            $fetchProductPOInventory += $fetchProductPODetail->projected_units;
                        }
                        $TotalInventory = $amazonInventory->afn_total_qty - $amazonInventory->afn_unsellable_qty + $warehouseInventoryTotal + $fetchProductPOInventory;
                        if ($TotalInventory > 0) {
                            $TotalLeadTime = $wareHouseLeadTime + $productSettings->total_lead_time_days;
                            $orderVolume = $productSettings->order_volume_value;
                            //Seven Days Calculations
                            $sevenDaysBackDate = Carbon::now()->subDays(7)->format('Y-m-d');
                            $sevenDaysSales = Calculated_sales_qty::where('product_id',$product['id'])->where('order_date', '>=', $sevenDaysBackDate)->sum('total_qty');
                            if ($sevenDaysSales > 0) {
                                $sevenDaysDayRate = round(($sevenDaysSales / 7), 6);
                                $sevenDaysDayOfSupply = round(($TotalInventory / $sevenDaysDayRate), 2);
                            } else {
                                $sevenDaysSales = $sevenDaysDayRate = $sevenDaysDayOfSupply = 0;
                            }

                            //Fourteen Days Calculations
                            $fourteenDaysBackDate = Carbon::now()->subDays(14)->format('Y-m-d');
                            $fourteenDaysSales = Calculated_sales_qty::where('product_id', $product['id'])->where('order_date', '>=', $fourteenDaysBackDate)->sum('total_qty');
                            if ($fourteenDaysSales > 0) {
                                $fourteenDaysDayRate = round(($fourteenDaysSales / 14), 6);
                                $fourteenDaysDayOfSupply = round(($TotalInventory / $fourteenDaysDayRate), 2);
                            } else {
                                $fourteenDaysSales = $fourteenDaysDayRate = $fourteenDaysDayOfSupply = 0;
                            }

                            //Thirty Days Calculations
                            $thirtyDaysBackDate = Carbon::now()->subDays(30)->format('Y-m-d');
                            $thirtyDaysSales = Calculated_sales_qty::where('product_id', $product['id'])->where('order_date', '>=', $thirtyDaysBackDate)->sum('total_qty');
                            if ($thirtyDaysSales > 0) {
                                $thirtyDaysDayRate = round(($thirtyDaysSales / 30), 6);
                                $thirtyDaysDayOfSupply = round(($TotalInventory / $thirtyDaysDayRate), 2);
                            } else {
                                $thirtyDaysSales = $thirtyDaysDayRate = $thirtyDaysDayOfSupply = 0;
                            }

                            //Previous Seven Days Calculations
                            $previousSevenDaysSales = Calculated_sales_qty::where('product_id', $product['id'])->where('order_date', '<=', $sevenDaysBackDate)->where('order_date', '>=', $fourteenDaysBackDate)->sum('total_qty');
                            if ($previousSevenDaysSales > 0) {
                                $previousSevenDaysDayRate = round(($previousSevenDaysSales / 7), 6);
                            } else {
                                $previousSevenDaysDayRate = 0;
                            }

                            //Previous Thirty Days Calculations
                            $sixtyDaysBackDate = Carbon::now()->subDays(60)->format('Y-m-d');
                            $previousThirtyDaysSales = Calculated_sales_qty::where('product_id', $product['id'])->where('order_date', '<=', $thirtyDaysBackDate)->where('order_date', '>=', $sixtyDaysBackDate)->sum('total_qty');
                            if ($previousThirtyDaysSales > 0) {
                                $previousThirtyDaysDayRate = round(($previousThirtyDaysSales / 30), 6);
                            } else {
                                $previousThirtyDaysDayRate = 0;
                            }
                            //Last Year 30 Days Sales
                            $thirtyDaysStartDateLastYear = Carbon::now()->subDays(395)->format('Y-m-d');
                            $thirtyDaysEndDateLastYear = Carbon::now()->subDays(365)->format('Y-m-d');
                            $thirtyDaysSalesLastyear = Calculated_sales_qty::where('product_id', $product['id'])->where('order_date', '<=', $thirtyDaysEndDateLastYear)->where('order_date', '>=', $thirtyDaysStartDateLastYear)->sum('total_qty');
                            if ($thirtyDaysSalesLastyear > 0) {
                                $ThirtyDaysDayRateLastYear = round(($thirtyDaysSalesLastyear / 30), 6);
                            } else {
                                $ThirtyDaysDayRateLastYear = 0;
                            }

                            $currentMonthPreviousYearStartDate = Carbon::now()->subYear(1)->format('Y-m-01');
                            $currentMonthPreviousYearEndDate = Carbon::now()->subYear(1)->format('Y-m-t');
                            $currentMonthLastYearStartDate = Carbon::now()->subYear(2)->format('Y-m-01');
                            $currentMonthLastYearEndDate = Carbon::now()->subYear(2)->format('Y-m-t');

                            $previousYearSameMonthSales = Calculated_sales_qty::where('product_id', $product['id'])->where('order_date', '<=', $currentMonthPreviousYearEndDate)->where('order_date', '>=', $currentMonthPreviousYearStartDate)->sum('total_qty');
                            $lastYearSameMonthSales = Calculated_sales_qty::where('product_id', $product['id'])->where('order_date', '<=', $currentMonthLastYearEndDate)->where('order_date', '>=', $currentMonthLastYearStartDate)->sum('total_qty');
                            $trendDirectionPrevious = $breakStatus = $monthlySalesTrendTotal = 0;

                            // Multi Month Trend Calculations
                            $trendDirectionPrevious = $breakStatus = $monthlySalesTrendTotal = $monthToMonthAverageTrend = 0;
                            for ($counter = 1; $counter < 18; $counter++) {
                                $calculationMonth = Carbon::now()->subMonth($counter)->format('Y-m');
                                $monthlySales = Sales_total_trend_rate::where('prod_id', $product['id'])->where('month_year', $calculationMonth)->first();
                                $trendDirection = $this->sign($monthlySales['trend_percentage']);
                                if ($counter > 1) {
                                    if ($trendDirection == $trendDirectionPrevious) {
                                        $monthlySalesTrendTotal += $monthlySales['trend_percentage'];
                                    } else {
                                        $monthToMonthAverageTrend = $monthlySalesTrendTotal / ($counter - 1);
                                        $breakStatus = 1;
                                    }
                                } else {
                                    $monthlySalesTrendTotal += $monthlySales['trend_percentage'];
                                }
                                $trendDirectionPrevious = $trendDirection;
                                if ($breakStatus == 1)
                                    break;
                            }
                            // Multi Week Trend Calculations
                            $trendDirectionPrevious = $breakStatus = $weeklySalesTrendTotal = $multiWeekAverageTrend = $weeklySalesPreviousDayRate = 0;
                            for ($counter = 1; $counter < 18; $counter++) {
                                $subtractStartDateValue = Carbon::now()->subDays($counter * 7)->format('Y-m-d');
                                if ($counter == 1)
                                    $subtractEndDateValue = Carbon::now()->format('Y-m-d');
                                else
                                    $subtractEndDateValue = Carbon::now()->subDays(($counter - 1) * 7)->format('Y-m-d');

                                $weeklySales = Calculated_sales_qty::where('product_id', $product['id'])->where('order_date', '>=', $subtractStartDateValue)->where('order_date', '<=', $subtractEndDateValue)->sum('total_qty');
                                $weeklySalesDayRate = round($weeklySales / 7, 6);
                                $trendPercentage = 0;
                                if ($counter > 1) {
                                    if (($weeklySalesDayRate - $weeklySalesPreviousDayRate) != 0 && $weeklySalesDayRate != 0) {
                                        $trendPercentage = round(($weeklySalesDayRate - $weeklySalesPreviousDayRate) / $weeklySalesDayRate, 2) * 100;
                                    } else {
                                        $trendPercentage = 0;
                                    }
                                    $trendDirection = $this->sign($trendPercentage);
                                    if ($trendDirection == $trendDirectionPrevious || $trendDirection == 0 || $trendDirectionPrevious == 0) {
                                        $weeklySalesTrendTotal += $trendPercentage;
                                    } else {
                                        $multiWeekAverageTrend = $weeklySalesTrendTotal / ($counter - 1);
                                        $breakStatus = 1;
                                    }
                                    $trendDirectionPrevious = $trendDirection;
                                } else {
                                    $weeklySalesTrendTotal += $trendPercentage;
                                }
                                $weeklySalesPreviousDayRate = $weeklySalesDayRate;
                                if ($breakStatus == 1)
                                    break;
                            }

                            //Trend Calculations
                            if ($previousYearSameMonthSales > 0 && $lastYearSameMonthSales > 0)
                                $yearOverYearHistoricalTrend = round((($previousYearSameMonthSales - $lastYearSameMonthSales) / $lastYearSameMonthSales *100), 2);
                            else
                                $yearOverYearHistoricalTrend = 0;

                            //Year Over year Trend
                            $previousMonthYearDate = Carbon::now()->subMonth(1)->subYear(1)->format('Y-m');
                            $salesTrend = Sales_total_trend_rate::where('prod_id', $product['id'])->where('month_year', $previousMonthYearDate)->first();
                            if (!empty($salesTrend) && $thirtyDaysSales > 0) {
                                $yearOveryearCurrent = round(($thirtyDaysSales - $salesTrend->sales_total) / $salesTrend->sales_total *100 , 2);
                            } else {
                                $yearOveryearCurrent = 0;
                            }
                            //Multi Month Trend
                            $multiMonthTrend = $monthToMonthAverageTrend;

                            if ($thirtyDaysDayRate > 0 && $previousThirtyDaysDayRate > 0)
                                $thirtyOverThirtyTrend = round((($thirtyDaysDayRate - $previousThirtyDaysDayRate) / $thirtyDaysDayRate)* 100, 2);
                            else
                                $thirtyOverThirtyTrend = 0;

                            //Multi Week Trend
                            $multiWeekTrend = $multiWeekAverageTrend;

                            if ($sevenDaysDayRate > 0 && $previousSevenDaysDayRate > 0)
                                $sevenOverSevenTrend = round((($sevenDaysDayRate - $previousSevenDaysDayRate) / $sevenDaysDayRate)* 100, 2);
                            else
                                $sevenOverSevenTrend = 0;

                            if($logicCalculations->supply_last_year_sales_percentage > 0){
                                $historicalRecentDifference = (100 - $logicCalculations->supply_last_year_sales_percentage) / 100;
                            }else {
                                $historicalRecentDifference = 1;
                            }

                            if($logicCalculations->reorder_last_year_sales_percentage > 0){
                                $historicalRecentReorderDifference = (100 - $logicCalculations->reorder_last_year_sales_percentage) / 100;
                            }else {
                                $historicalRecentReorderDifference = 1;
                            }

                            $supplyCalculationSalesDayRate = (($ThirtyDaysDayRateLastYear != 0 && $logicCalculations->supply_last_year_sales_percentage > 0) ? round(($ThirtyDaysDayRateLastYear * $logicCalculations->supply_last_year_sales_percentage / 100), 6) : 0) +
                                (($sevenDaysDayRate != 0 && $logicCalculations->supply_recent_last_7_day_sales_percentage > 0) ? round(($sevenDaysDayRate * round(($logicCalculations->supply_recent_last_7_day_sales_percentage * $historicalRecentDifference),6) / 100), 6) : 0) +
                                (($fourteenDaysDayRate != 0 && $logicCalculations->supply_recent_last_14_day_sales_percentage > 0) ? round(($fourteenDaysDayRate * round(($logicCalculations->supply_recent_last_14_day_sales_percentage * $historicalRecentDifference),6) / 100), 6) : 0) +
                                (($thirtyDaysDayRate != 0 && $logicCalculations->supply_recent_last_30_day_sales_percentage > 0) ? round(($thirtyDaysDayRate * round(($logicCalculations->supply_recent_last_30_day_sales_percentage * $historicalRecentDifference),6) / 100), 6) : 0);

                            $supplyCalculationsTrend = (($yearOverYearHistoricalTrend != 0 && $logicCalculations->supply_trends_year_over_year_historical_percentage > 0) ? round(($yearOverYearHistoricalTrend * $logicCalculations->supply_trends_year_over_year_historical_percentage / 100), 2) : 0) +
                                (($yearOveryearCurrent != 0 && $logicCalculations->supply_trends_year_over_year_current_percentage > 0) ? round(($yearOveryearCurrent * $logicCalculations->supply_trends_year_over_year_current_percentage / 100), 2) : 0) +
                                (($multiMonthTrend != 0 && $logicCalculations->supply_trends_multi_month_trend_percentage > 0) ? round(($multiMonthTrend * $logicCalculations->supply_trends_multi_month_trend_percentage / 100), 2) : 0) +
                                (($thirtyOverThirtyTrend != 0 && $logicCalculations->supply_trends_30_over_30_days_percentage > 0) ? round(($thirtyOverThirtyTrend * $logicCalculations->supply_trends_30_over_30_days_percentage / 100), 2) : 0) +
                                (($multiWeekTrend != 0 && $logicCalculations->supply_trends_multi_week_trend_percentage > 0) ? round(($multiWeekTrend * $logicCalculations->supply_trends_multi_week_trend_percentage / 100), 2) : 0) +
                                (($sevenOverSevenTrend != 0 && $logicCalculations->supply_trends_7_over_7_days_percentage > 0) ? round(($sevenOverSevenTrend * $logicCalculations->supply_trends_7_over_7_days_percentage / 100), 2) : 0);

                            $reorderCalculationsTrend = (($yearOverYearHistoricalTrend != 0 && $logicCalculations->reorder_trends_year_over_year_historical_percentage > 0) ? round(($yearOverYearHistoricalTrend * $logicCalculations->reorder_trends_year_over_year_historical_percentage / 100), 2) : 0) +
                                (($yearOveryearCurrent != 0 && $logicCalculations->reorder_trends_year_over_year_current_percentage > 0) ? round(($yearOveryearCurrent * $logicCalculations->reorder_trends_year_over_year_current_percentage / 100), 2) : 0) +
                                (($multiMonthTrend != 0 && $logicCalculations->reorder_trends_multi_month_trend_percentage > 0) ? round(($multiMonthTrend * $logicCalculations->reorder_trends_multi_month_trend_percentage / 100), 2) : 0) +
                                (($thirtyOverThirtyTrend != 0 && $logicCalculations->reorder_trends_30_over_30_days_percentage > 0) ? round(($thirtyOverThirtyTrend * $logicCalculations->reorder_trends_30_over_30_days_percentage / 100), 2) : 0) +
                                (($multiWeekTrend != 0 && $logicCalculations->reorder_trends_multi_week_trend_percentage > 0) ? round(($multiWeekTrend * $logicCalculations->reorder_trends_multi_week_trend_percentage / 100), 2) : 0) +
                                (($sevenOverSevenTrend != 0 && $logicCalculations->reorder_trends_7_over_7_days_percentage > 0) ? round(($sevenOverSevenTrend * $logicCalculations->reorder_trends_7_over_7_days_percentage / 100), 2) : 0);

                            $reorderCalculationSales = (($sevenDaysSales > 0 && $logicCalculations->reorder_recent_last_7_day_sales_percentage > 0) ? round(($sevenDaysSales * round(($logicCalculations->reorder_recent_last_7_day_sales_percentage * $historicalRecentReorderDifference),6) / 100), 6) : 0) +
                                (($fourteenDaysSales > 0 && $logicCalculations->reorder_recent_last_14_day_sales_percentage > 0) ? round(($fourteenDaysSales * round(($logicCalculations->reorder_recent_last_14_day_sales_percentage * $historicalRecentReorderDifference),6) / 100), 6) : 0) +
                                (($thirtyDaysSales > 0 && $logicCalculations->reorder_recent_last_30_day_sales_percentage > 0) ? round(($thirtyDaysSales * round(($logicCalculations->reorder_recent_last_30_day_sales_percentage * $historicalRecentReorderDifference),6) / 100), 6) : 0);

                            $overAllDayRate = round(($supplyCalculationsTrend * $supplyCalculationSalesDayRate / 100) + $supplyCalculationSalesDayRate, 6);
                            //$overAllDayRate = 10;
                            $overAllDaysSupply = 0;
                            if ($overAllDayRate > 0) {
                                $overAllDaysSupply = round(($TotalInventory / $overAllDayRate), 6);
                                $startDate = $overAllProjectedOrderStartDateRange = Carbon::now()->addDays($overAllDaysSupply);
                                $overAllProjectedOrderEndDateRange = Carbon::now()->addDays($overAllDaysSupply + $orderVolume)->format('Y-m-d');
                                $blackoutDays = 0;
                                if($blackOutDayFlat == 0){
                                    //$blackoutDays
                                    $blackoutDays = Supplier_blackout_date::where('supplier_id',$productSettings->id)->where('start_date','>=',Carbon::now()->format('Y-m-d'))->where('end_date','<=',$overAllProjectedOrderStartDateRange->format('Y-m-d'))->sum('number_of_days');

                                } else{
                                    $blackoutDays = Vendor_blackout_date::where('vendor_id',$productSettings->id)->where('start_date','>=',Carbon::now()->format('Y-m-d'))->where('end_date','<=',$overAllProjectedOrderStartDateRange->format('Y-m-d'))->sum('number_of_days');
                                }

                                $addDaysForProjectedDate = round($overAllDaysSupply - $TotalLeadTime - $blackoutDays);
                                $overAllProjectedReorderDate = Carbon::now()->addDays($addDaysForProjectedDate)->format('Y-m-d');
                                $todaySDate = Carbon::now();
                                $overAllDaysLeftToOrder = $todaySDate->diffInDays($overAllProjectedReorderDate, false);
                                $count = $counter = $breakLoop = $projectedReorderQty = 0;
                                $projectedReorderQty += $reorderCalculationSales;
                                if ($logicCalculations->reorder_last_year_sales_percentage > 0) {
                                    while ($counter != $orderVolume) {
                                        $calculatedDays = 0;
                                        $monthCalculation = Carbon::parse($startDate)->addMonths($count)->format('Y-m-t');
                                        if ($overAllProjectedOrderEndDateRange <= $monthCalculation) {
                                            $calculatedDays = Carbon::parse($overAllProjectedOrderEndDateRange)->format('d');
                                            $breakLoop = 1;
                                        } else if ($counter == 0) {
                                            $calculatedDays = $overAllProjectedOrderStartDateRange->diffInDays($monthCalculation) + 1;
                                            $monthCalculation = Carbon::parse($startDate)->addMonths($count);
                                        } else {
                                            $calculatedDays = Carbon::parse($startDate)->addMonths($count)->format('t');
                                        }
                                        $counter += (int)$calculatedDays;
                                        $count++;
                                        $newCalculatedMonthYear = Carbon::parse($monthCalculation)->subYear(1)->format('Y-m');
                                        $salesTrendValue = Sales_total_trend_rate::where('prod_id', $product['id'])->where('month_year', $newCalculatedMonthYear)->first();
                                        if ($salesTrendValue) {
                                            $salesTrendValue = round($salesTrendValue->day_rate * $logicCalculations->reorder_last_year_sales_percentage / 100, 6);
                                            if ($salesTrendValue) {
                                                $projectedReorderQty += ($salesTrendValue * $reorderCalculationsTrend / 100) + ($salesTrendValue * $calculatedDays);
                                            }
                                        }
                                        if ($breakLoop == 1) {
                                            break;
                                        }
                                    }
                                }
                            }
                            //Saving Calculated Logic
                            $dailyLogicCalculations = Daily_logic_Calculations::where('prod_id', $product['id'])->first();

                            if (empty($dailyLogicCalculations)) {
                                $dailyLogicCalculations = new Daily_logic_Calculations();
                                $dailyLogicCalculations->user_marketplace_id = $product['user_marketplace_id'];
                                $dailyLogicCalculations->prod_id = $product['id'];
                            }
                            $overAllProjectedOrderEndDateRange = isset($overAllProjectedOrderEndDateRange) ? $overAllProjectedOrderEndDateRange : '';
                            $dailyLogicCalculations->total_inventory = $TotalInventory;
                            $dailyLogicCalculations->lead_time_amazon = $TotalLeadTime;
                            $dailyLogicCalculations->order_frequency = $orderVolume;
                            $dailyLogicCalculations->day_rate = $overAllDayRate;
                            $dailyLogicCalculations->days_supply = $overAllDaysSupply != '' ? $overAllDaysSupply : 0 ;
                            $dailyLogicCalculations->blackout_days = 0;
                            $dailyLogicCalculations->projected_reorder_date = (isset($overAllProjectedReorderDate) && $overAllProjectedReorderDate != '' ? $overAllProjectedReorderDate : 0);
                            $dailyLogicCalculations->projected_reorder_qty = isset($projectedReorderQty) ? $projectedReorderQty : 0;
                            $dailyLogicCalculations->days_left_to_order = isset($overAllDaysLeftToOrder) ? $overAllDaysLeftToOrder : 0 ;
                            $dailyLogicCalculations->projected_order_date_range = isset($overAllProjectedOrderStartDateRange) ? $overAllProjectedOrderStartDateRange->format('Y-m-d') : '' . ' - ' . $overAllProjectedOrderEndDateRange;
                            $dailyLogicCalculations->sevendays_sales = $sevenDaysSales;
                            $dailyLogicCalculations->sevenday_day_rate = $sevenDaysDayRate;
                            $dailyLogicCalculations->fourteendays_sales = $fourteenDaysSales;
                            $dailyLogicCalculations->fourteendays_day_rate = $fourteenDaysDayRate;
                            $dailyLogicCalculations->thirtydays_sales = $thirtyDaysSales;
                            $dailyLogicCalculations->thirtyday_day_rate = $thirtyDaysDayRate;
                            $dailyLogicCalculations->sevendays_previous = $previousSevenDaysSales;
                            $dailyLogicCalculations->sevendays_previous_day_rate = $previousSevenDaysDayRate;
                            $dailyLogicCalculations->thirtydays_previous = $previousThirtyDaysSales;
                            $dailyLogicCalculations->thirtydays_previous_day_rate = $previousThirtyDaysDayRate;
                            $dailyLogicCalculations->thirtydays_last_year = $thirtyDaysSalesLastyear;
                            $dailyLogicCalculations->thirtydays_last_year_day_rate = $ThirtyDaysDayRateLastYear;
                            $dailyLogicCalculations->save();
                        }
                    }
                }
            }
            Log::error("Error while executing job CalculateTrend for account {$marketplace_id}");
            echo 'Done CalculateTrend Calculations';
            //$this->log_it("Done CalculateTrend Calculations");
        } catch (\Exception $ex) {
            dd($ex->getMessage());
            Log::error("Could not execute job CalculateTrend: " . $ex->getMessage());
            //$this->log_it($ex->getTraceAsString());
            throw new \Exception($ex->getMessage());
        }
    }

    public function sign($number)
    {
        return ($number > 0) ? 1 : (($number < 0) ? -1 : 0);
    }

    // public function products_data()
    // {
    //     $ProductData=Mws_product::limit(10)->get();
    //
    // }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Models\Mws_product;
use App\Models\Inbound_shipment_items;
use Response;
class InventoryController extends Controller
{
    /**
     * Display a listing of the mws_product with mws_afn_inventory & mws_reserved_inventory.
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get_checks = get_access('mws_inventory_module','view');
        $get_user_access = get_user_check_access('mws_inventory_module','view');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }
         $global_marketplace = session('MARKETPLACE_ID');
         $get_all_product_sku = Mws_product::with(['mws_afn_inventory','mws_reserved_inventory'])->where(array('user_marketplace_id'=>$global_marketplace))->whereHas('mws_afn_inventory', function ($query) {$query->where(array('warehouse_condition_code'=>'SELLABLE'));})->selectRaw('sku,id')->get()->toArray();
         return view('inventory.index', ['product_sku'=>$get_all_product_sku,'marketplace'=>$global_marketplace]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * There is 1 function added in store method :
     *      @method POST
     *
     *  Request Types:
     *
     * 1. show_inboung_shipment_id : listing of inbound shipment items with product.
     *      @param  $type, $product_id
     *      @return \Illuminate\Http\Response
     *
     */
    public function store(Request $request)
    {
        if(isset($request->user_type) && $request->user_type == 'show_inboung_shipment_id'){
            $auth_Id = Auth::id();
            $product_id = $request->news_id;
            $data = Inbound_shipment_items::with(['inbound_shipment'])->where(array('product_id'=>$product_id))->get()->toArray();

            $view = View::make('inventory.get_inbound_shipment', [
                'inbound_shipment' => $data,
            ]);
            $html = $view->render();
            $res['error'] = 0;
            $res['view'] = $html;
            return response()->json($res);
        }
    }

    /**
     * Download all mws_product with mws_afn_inventory & inbound_shipment
     *
     * @method POST
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id == 'import') {

            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename=Inventory.csv');
            $output = fopen("php://output", "w");
            fputcsv($output, array('SKU', 'AMAZON STOCK', 'AMAZON RECEIVING', 'AMAZON INBOUND', 'NAME', 'SHIPMENT ID', 'SENT', 'RECIEVED'));
            $global_marketplace = session('MARKETPLACE_ID');
            $get_all_product_sku = Mws_product::with(['mws_afn_inventory'])->where(array('user_marketplace_id'=>$global_marketplace))->selectRaw('sku,id')->get()->toArray();
            foreach($get_all_product_sku as $result)
            {
                $datas = Inbound_shipment_items::with(['inbound_shipment'])->where(array('product_id'=>$result['id']))->get()->toArray();
                $fileputcsv['SKU'] = $result['sku'];
                $fileputcsv['AMAZON STOCK'] = $result['mws_afn_inventory']['quantity_available'];
                $fileputcsv['AMAZON RECEIVING'] = '';
                $fileputcsv['AMAZON INBOUND'] = '';

                if(empty($datas)) {
                    $fileputcsv['NAME'] = '';
                    $fileputcsv['SHIPMENT ID'] = '';
                    $fileputcsv['SENT'] = '';
                    $fileputcsv['RECIEVED'] = '';
                    fputcsv($output, $fileputcsv);
                }else{
                    $j = 1;
                    foreach($datas as $inbound) {
                        if($j != 1){
                            $fileputcsv['SKU'] = '';
                            $fileputcsv['AMAZON STOCK'] ='';
                        }
                        $fileputcsv['NAME'] = $inbound['inbound_shipment']['ShipmentName'];
                        $fileputcsv['SHIPMENT ID'] = $inbound['inbound_shipment']['ShipmentId'];
                        $fileputcsv['SENT'] = $inbound['QuantityShipped'];
                        $fileputcsv['RECIEVED'] = $inbound['QuantityReceived'];
                        $j++;
                        fputcsv($output, $fileputcsv);
                    }
                }
            }
            fclose($output);
            exit;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Amazon_requestlog;
use App\Models\Mwsdeveloperaccount;
use App\Models\Settings;
use App\Models\SPApiMarketplaceDetails;
use App\Models\SPApiUserMarketplace;
use App\Models\Usermarketplace;
use App\Models\Users;
use ClouSale\AmazonSellingPartnerAPI\Models\Authorization\AuthorizationCode;
use ClouSale\AmazonSellingPartnerAPI\SellingPartnerOAuth;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Jobs\RequestReport;
use App\Jobs\ProcessReportsJob;
use Illuminate\Support\Facades\Auth;
use App\Models\Amzdateiteration;
use Illuminate\Support\Facades\Session;
use App\Models\Default_logic_setting;

class AmazonsettingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $service;


    public function __construct()
    {
    }

    /**
     * Display a amazon setting listing.
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        $get_user_access = get_user_check_access('mws_module','view');
        $get_checks = get_access('mws_module','view');
        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }
//        MWS Settings Query
//        $data["user"] = Users::with(['usermarketplace', 'userdetails', 'user_assigned_role', 'user_roles'])->where('id', Auth::user()->id)->first()->toArray();
        $data["user"] = Users::with(['spAPIUserMarketplace', 'userdetails', 'user_assigned_role', 'user_roles'])->where('id', Auth::user()->id)->first()->toArray();
        if ($data["user"]['user_roles'][0]['id'] == 1) {
            $data['developerAccountDetails'] = Mwsdeveloperaccount::where('marketplace_status', 1)->get();
            return view('mwscredentials/developerlist', ['developerAccountDetails'=>$data['developerAccountDetails'],'user_access_create_details' => get_user_check_access('mws_module','create')]);
        } else if ($data["user"]['user_roles'][0]['id'] == 3) {
//            $data['developerAccountDetails'] = SPApiMarketplaceDetails::where('marketplace_status', 1)->get()->toArray();
//            return view('mwscredentials/mwssettinglist', ['usermarketplace'=>$data['user']['sp_a_p_i_user_marketplace'],'user_access_create_details' => get_user_check_access('mws_module','create')]);
            return view('mwscredentials/spapisettinglist', ['usermarketplace'=>$data['user']['sp_a_p_i_user_marketplace'],'user_access_create_details' => get_user_check_access('mws_module','create')]);
        } else {
            return redirect('amazonsettings');
        }
    }
    /**
     * Show the form for update & create amazon settings.
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $get_user_access = get_user_check_access('mws_module','create');
        $get_checks = get_access('mws_module','create');
        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }
//        $data["user"] = Users::with(['usermarketplace', 'userdetails', 'user_assigned_role', 'user_roles'])->where('id', Auth::user()->id)->first()->toArray();
        $data["user"] = Users::with(['spAPIUserMarketplace', 'userdetails', 'user_assigned_role', 'user_roles'])->where('id', Auth::user()->id)->first()->toArray();
        if ($data["user"]['user_roles'][0]['id'] == 1) {
            $data['developerAccountDetails'] = Mwsdeveloperaccount::all();
            return view('mwscredentials/developercreateedit', $data);
        } else if ($data["user"]['user_roles'][0]['id'] == 3) {
//            $data['developerAccountDetails'] = Mwsdeveloperaccount::where('marketplace_status', 1)->get()->toArray();
            $data['developerAccountDetails'] = SPApiMarketplaceDetails::where('marketplace_status', 1)->get()->toArray();
            foreach ($data['developerAccountDetails'] as $developerDetails) {
                foreach ($data['user']['sp_a_p_i_user_marketplace'] as $marketplace) {
                    if ($developerDetails['id'] == $marketplace['marketplace_id']) {
                        $data['user']['marketpalce'][$developerDetails['marketplace_slug']] = $marketplace;
                    }
                }
            }
//            echo "<pre />";
//            print_r($data);exit;
//            return view('mwscredentials/mwssettingcreateedit', $data);
            return view('mwscredentials/spapisettingcreateedit', $data);
        } else {
            return redirect('amazonsettings');
        }
    }

    /**
     * Update the specified amazon setting.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $userid
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $userid)
    {
        $get_user_access = get_user_check_access('mws_module','edit');
        $get_checks = get_access('mws_module','edit');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }

        $data["user"] = Users::with(['usermarketplace', 'userdetails', 'user_assigned_role', 'user_roles'])->where('id', Auth::user()->id)->first()->toArray();
        if ($data["user"]['user_roles'][0]['id'] == 3) {
            foreach ($_POST['marketplacedetails'] as $devAccountDetails) {
                if (!empty($_POST['mws_sellerid'][$devAccountDetails][0]) && !empty(base64_encode($_POST['mws_authtoken'][$devAccountDetails][0]))) {
                    $userMarketplace = Usermarketplace::where('user_id', $userid)->where('country', $_POST['country'][$devAccountDetails][0])->first();
                    if ($userMarketplace) {
                        $userMarketplace->mws_sellerid = base64_encode($_POST['mws_sellerid'][$devAccountDetails][0]);
                        $userMarketplace->mws_marketplaceid = $devAccountDetails;
                        $userMarketplace->mws_authtoken = base64_encode($_POST['mws_authtoken'][$devAccountDetails][0]);
                        $userMarketplace->country = $_POST['country'][$devAccountDetails][0];
                        $userMarketplace->mws_report_url = $_POST['mrketplaceurl'][$devAccountDetails][0];
                        $userMarketplace->amazonstorename = $_POST['amazonstorename'][$devAccountDetails][0];
                        $userMarketplace->apiactive = 1;
                        $userMarketplace->save();
                    } else {
                        Usermarketplace::create([
                            'user_id' => $userid,
                            'mws_sellerid' => base64_encode($_POST['mws_sellerid'][$devAccountDetails][0]),
                            'mws_marketplaceid' => $devAccountDetails,
                            'mws_authtoken' => base64_encode($_POST['mws_authtoken'][$devAccountDetails][0]),
                            'country' => $_POST['country'][$devAccountDetails][0],
                            'mws_report_url' => $_POST['mrketplaceurl'][$devAccountDetails][0],
                            'amazonstorename' => $_POST['amazonstorename'][$devAccountDetails][0],
                            'apiactive' => 1,
                            'api_token' => bin2hex(random_bytes(20))
                        ]);
                    }
                    $userMarketplace = Usermarketplace::where('user_id', $userid)->where('country', $_POST['country'][$devAccountDetails][0])->first();
                    $this->dispatch(new RequestReport($userMarketplace, '_GET_FBA_ESTIMATED_FBA_FEES_TXT_DATA_'));
                    sleep(15);
                    $validamazonMWS = Amazon_requestlog::where('user_marketplace_id', $userMarketplace->id)
                        ->where('report_type', '_GET_FBA_ESTIMATED_FBA_FEES_TXT_DATA_')
                        ->orderBy('id', 'desc')->first();
                    if (empty($validamazonMWS->request_response)) {
                        $this->dispatch(new RequestReport($userMarketplace, '_GET_FBA_ESTIMATED_FBA_FEES_TXT_DATA_'));
                        $processJobRequest = (new ProcessReportsJob($userMarketplace, '_GET_FBA_ESTIMATED_FBA_FEES_TXT_DATA_'))->delay(Carbon::now()->addMinutes(2));
                        $this->dispatch($processJobRequest);
                        $processJobProcess = (new ProcessReportsJob($userMarketplace, '_GET_FBA_ESTIMATED_FBA_FEES_TXT_DATA_'))->delay(Carbon::now()->addMinutes(5));
                        $this->dispatch($processJobProcess);
                        $this->addDate($userMarketplace->id);
                    } else {
                        $userMarketplace->mws_sellerid = '';
                        $userMarketplace->mws_authtoken = '';
                        $userMarketplace->apiactive = 0;
                        $userMarketplace->save();
                        $message = get_messages($validamazonMWS->request_response, 0);
                        Session::flash('message', $message);
                        return redirect('amazonsettings');
                    }
                }
            }

            if(isset($userMarketplace->id) && $userMarketplace->id != ''){
                $check_marketplace = Default_logic_setting::where(array('user_marketplace_id'=>$userMarketplace->id))->get()->toArray();
                if(empty($check_marketplace)){
                    $default_orer_logic = [
                        'user_marketplace_id'=>$userMarketplace->id,
                        'logic_label_name' => 'Default Logic',
                        'supply_last_year_sales_percentage' => 0,
                        'supply_recent_last_7_day_sales_percentage' => 0,
                        'supply_recent_last_14_day_sales_percentage' => 0,
                        'supply_recent_last_30_day_sales_percentage' => 100.00,
                        'supply_trends_year_over_year_historical_percentage' => 0,
                        'supply_trends_year_over_year_current_percentage' => 50.00,
                        'supply_trends_multi_month_trend_percentage' => 0,
                        'supply_trends_30_over_30_days_percentage' => 0,
                        'supply_trends_multi_week_trend_percentage' => 50.00,
                        'supply_trends_7_over_7_days_percentage' => 0,
                        'reorder_last_year_sales_percentage' => 100.00,
                        'reorder_recent_last_7_day_sales_percentage' => 0,
                        'reorder_recent_last_14_day_sales_percentage' => 0,
                        'reorder_recent_last_30_day_sales_percentage' => 0,
                        'reorder_trends_year_over_year_historical_percentage' => 0,
                        'reorder_trends_year_over_year_current_percentage' =>100.00,
                        'reorder_trends_multi_month_trend_percentage' => 0,
                        'reorder_trends_30_over_30_days_percentage' => 0,
                        'reorder_trends_multi_week_trend_percentage' => 0,
                        'reorder_trends_7_over_7_days_percentage' => 0,
                        'reorder_safety_stock_percentage' => 2.50,
                    ];
                    Default_logic_setting::create($default_orer_logic);
                }
            }


            $message = get_messages('Your amazon credentials validated successfully!',1);
            Session::flash('message', $message);
            return redirect('amazonsettings');
        } else if ($data["user"]['user_roles'][0]['id'] == 1) {
            foreach ($_POST['marketplacedetails'] as $devAccountDetails) {
                $developerAccountDetail = Mwsdeveloperaccount::where('marketplace_id', $devAccountDetails)->first();
                $developerAccountDetail->developer_accno = $_POST['developer_accno'][$devAccountDetails][0];
                $developerAccountDetail->access_key = base64_encode($_POST['access_key'][$devAccountDetails][0]);
                $developerAccountDetail->secret_key = base64_encode($_POST['secret_key'][$devAccountDetails][0]);
                $developerAccountDetail->marketplace_id = (isset($devAccountDetails) ? $devAccountDetails : '');
                $developerAccountDetail->marketplace_status = (isset($_POST['marketplace_status'][$devAccountDetails]) ? $_POST['marketplace_status'][$devAccountDetails][0] : 0);
                $developerAccountDetail->save();
            }
            $message = get_messages('Your amazon credentials validated successfully!',1);
            Session::flash('message', $message);
            return redirect('amazonsettings');
        } else {
            return redirect('/amazonsettings');
        }
    }
    /**
     * Adddate for amazon date interations  .
     *
     * @param  $acountId
     * @return \Illuminate\Http\Response
     */
    public function addDate($acountId)
    {
        $detail = Settings::first();
        $start_date = date('m/d/Y', strtotime(date('Y-m') . " -". (isset($detail['historical_fetch_number_months']) ? $detail['historical_fetch_number_months'] : '18') ." month"));
        $end_date = date('m/d/Y', strtotime("+9 days"));
        $data = $this->rent_range($start_date, $end_date);
        $totCount = count($data['start_dates']);
        for ($count = 0; $count < $totCount; $count++) {
            $stamp = strtotime($data['start_dates'][$count] . ' 00:00:00');
            $startDate = date("Y-m-d H:i:s", $stamp);
            $stamp = strtotime($data['end_dates'][$count] . ' 23:59:59');
            $endDate = date("Y-m-d H:i:s", $stamp);

            $amzDateIteration[$count] = array(
                'user_marketplace_id' => $acountId,
                'startDate' => $startDate,
                'endDate' => $endDate,
                'OrderStatus' => '0',
                'InventoryStatus' => '0',
                'ReimburseStatus' => '0',
                'ReturnStatus' => '0',
                'FinanceStatus' => '0',
                'CustomersalesStatus' => '0',
                'InventoryshipmentStatus' => '0',
                'ShipmentOrderRemovalStatus' => '0',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            );
            if(Amzdateiteration::where('user_marketplace_id', $acountId)->where('startDate', $startDate)->where('endDate',$endDate)->first())
                continue;
            Amzdateiteration::insert($amzDateIteration[$count]);
        }
    }

    /**
     * Rent range from start date to end date
     *
     * @param  $start_date, $end_date
     * @return \Illuminate\Http\Response
     */

    public function rent_range($start_date, $end_date)
    {
        $start = (new DateTime($start_date))->modify('first day of this month');
        $end = (new DateTime($end_date))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);

        foreach ($period as $dt) {
            $months[] = $dt->format("Y-m");
        }

        $start_date_time = strtotime($start_date);
        $end_date_time = strtotime($end_date);

        $start_dates_arr = [];
        $end_dates_arr = [];
        foreach ($months as $mon) {
            $mon_arr = explode("-", $mon);
            $y = $mon_arr[0];
            $m = $mon_arr[1];
            if ($start_date_time < $end_date_time) {
                $start_dates_arr[] = date("m/d/Y", strtotime($m . '/01/' . $y . ' 00:00:00'));
                $end_dates = date("m/d/Y", strtotime('-1 minute', strtotime($m . '/11/' . $y . ' 00:00:00')));
                if (strtotime($end_dates) > $end_date_time) {
                    $end_dates_arr[] = $end_date;
                    continue;
                } else {
                    $end_dates_arr[] = $end_dates;
                    $start_dates_arr[] = date("m/d/Y", strtotime($m . '/11/' . $y . ' 00:00:00'));
                    $end_dates = date("m/d/Y", strtotime('-1 minute', strtotime($m . '/21/' . $y . ' 00:00:00')));
                    if (strtotime($end_dates) > $end_date_time) {
                        $end_dates_arr[] = $end_date;
                        continue;
                    } else {
                        $end_dates_arr[] = $end_dates;
                        $start_dates_arr[] = date("m/d/Y", strtotime($m . '/21/' . $y . ' 00:00:00'));
                        $end_dates = date("m/d/Y", strtotime('-1 minute', strtotime('+1 month', strtotime($m . '/01/' . $y . ' 00:00:00'))));
                        if (strtotime($end_dates) > $end_date_time) {
                            $end_dates_arr[] = $end_date;
                            continue;
                        } else {
                            $end_dates_arr[] = $end_dates;
                        }
                    }
                }
            }
        }

        $result['start_dates'] = $start_dates_arr;
        $result['end_dates'] = $end_dates_arr;
        return $result;
    }
    public function show($id)
    {

    }

    /**
     * There are 1 function added in store method :
     *      @method POST
     *
     *  Request Types:
     *
     * 1. marketplace_get : marketplace_id
     *      @param  $marketplace_id
     *      @return \Illuminate\Http\Response
     *
     */
    public function store(Request $request){
        if($request->type == 'marketplace_get'){
            $get_account_no = Mwsdeveloperaccount::where(array('marketplace_id'=>$request->marketplace_id))->first()->toArray();
            $res['error'] = 0;
            $res['accountno'] = $get_account_no['developer_accno'];
        }else{
            $res['error'] = 0;
            $res['accountno'] = '';
        }
        return response()->json($res);
    }

    public function registertoamazon(){
        if($_GET['selling_partner_id']){
            try {
                $state = $_GET['state'];
                $sellingPartnerID = $_GET['selling_partner_id'];
                $spAPIAuthCode = $_GET['spapi_oauth_code'];
                $developerDetails = SPApiMarketplaceDetails::where('marketplace_slug', $state)->first();
                $refreshToken = SellingPartnerOAuth::getRefreshTokenFromLwaAuthorizationCode($spAPIAuthCode, $developerDetails->client_id, $developerDetails->client_secret, 'https://www.stockful.com/logintoamazon');
                if(!empty($refreshToken)){
                    $spapiUserMarketplace = SPApiUserMarketplace::where('user_id',Auth::user()->id)->where('marketplace_id',$developerDetails->id)->first();
                    if($spapiUserMarketplace){
                        $spapiUserMarketplace->selling_partner_id = $sellingPartnerID;
                        $spapiUserMarketplace->refresh_token = $refreshToken;
                    }else{
                        $spapiUserMarketplace = new SPApiUserMarketplace([
                            'user_id' => Auth::user()->id,
                            'marketplace_id' => $developerDetails->id,
                            'selling_partner_id' => $sellingPartnerID,
                            'client_id' => $developerDetails->client_id,
                            'client_secret' => $developerDetails->client_secret,
                            'access_key' => $developerDetails->access_key,
                            'secret_key' => $developerDetails->secret_key,
                            'role_arn' => $developerDetails->role_arn,
                            'end_point' => '',
                            'region' => '',
                            'refresh_token' => $refreshToken,
                            'expire_time' => date('Y-m-d H:i:s'),
                        ]);
                    }
                    $spapiUserMarketplace->save();
                }
                return redirect('amazonsettings');
            } catch (\Exception $ex){
//                echo "<pre />"; print_r($ex->getMessage());
                return redirect('amazonsettings');
            }
        }else{
            return redirect('amazonsettings');
        }
    }
}

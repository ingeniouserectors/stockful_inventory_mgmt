<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Default_logic_setting;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LogicController extends Controller
{

    /**
     * Show the form for Default logic which set in it.
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $get_checks = get_access('default_logic','view');
        $get_user_access = get_user_check_access('default_logic','view');
        if ($get_checks == 1) {
            if($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }

        $global_marketplace = session('MARKETPLACE_ID');
        $get_default_logic = array();
        if($global_marketplace != ''){
            $get_default_logic = Default_logic_setting::where(array('user_marketplace_id'=>$global_marketplace))->first();
        }
        return view('defaultlogic.create',['logic'=>$get_default_logic]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created default logic if logic is already created then update it.
     * @method POST
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs =  $request->all();

        $supply_history=$inputs['supply_last_year_sales_percentage'];

        $supply_order = 
            $inputs['supply_recent_last_7_day_sales_percentage'] +
            $inputs['supply_recent_last_14_day_sales_percentage'] +
            $inputs['supply_recent_last_30_day_sales_percentage'];

        $supply_trend_order =
            $inputs['supply_trends_year_over_year_historical_percentage'] +
            $inputs['supply_trends_year_over_year_current_percentage'] +
            $inputs['supply_trends_multi_month_trend_percentage'] +
            $inputs['supply_trends_30_over_30_days_percentage'] +
            $inputs['supply_trends_multi_week_trend_percentage'] +
            $inputs['supply_trends_7_over_7_days_percentage'];

        $reorder_history=$inputs['reorder_last_year_sales_percentage'];  

        $reorder = 
            $inputs['reorder_recent_last_7_day_sales_percentage'] +
            $inputs['reorder_recent_last_14_day_sales_percentage'] +
            $inputs['reorder_recent_last_30_day_sales_percentage'];

        $reorder_trend =
            $inputs['reorder_trends_year_over_year_historical_percentage'] +
            $inputs['reorder_trends_year_over_year_current_percentage'] +
            $inputs['reorder_trends_multi_month_trend_percentage'] +
            $inputs['reorder_trends_30_over_30_days_percentage'] +
            $inputs['reorder_trends_multi_week_trend_percentage'] +
            $inputs['reorder_trends_7_over_7_days_percentage'];
            $stock=$inputs['reorder_safety_stock_percentage'] ;
            
        if(($supply_order == 100.00 || $supply_history <= 100.00 ) && ($reorder == 100.00 ||  $reorder_history <=100) && $supply_trend_order == 100.00 && $reorder_trend == 100.00  && $stock <=100 ) {
                if(isset($inputs['id']) && $inputs['id'] != ''){
                    unset($inputs['_token']);
                    $insert_data = Default_logic_setting::where(array('id'=>$inputs['id']))->update($inputs);
                }else {
                    $insert_data = Default_logic_setting::create($request->all());
                }
                if ($insert_data) {
                    $message = get_messages('Default order logic updated successfully', 1);
                    Session::flash('message', $message);
                    return redirect()->route('defaultlogic.index');
                } else {
                    $message = get_messages('Failed to update default order logic data', 0);
                    Session::flash('message', $message);
                    return redirect()->route('defaultlogic.index');
                }
        }else{
            $message = get_messages('Supply order or reorder percentage must be 100 and at present the total of all the fields exceeds 100', 0);
            Session::flash('message', $message);
            return redirect()->route('defaultlogic.index');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

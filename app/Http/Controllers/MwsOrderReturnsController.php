<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Mws_order_returns;

use Response;

class MwsOrderReturnsController extends Controller
{
    /**
     * Display a listing of the order returns view.
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get_checks = get_access('order_return_module','view');
        $get_user_access = get_user_check_access('order_return_module','view');

        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        }
        $global_marketplace = session('MARKETPLACE_ID');
        $marketplace = '';
        if($global_marketplace != ''){
            $marketplace = $global_marketplace;
        }
        return view('order_returns.index',[
            'marketplace'=>$marketplace
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * There is 1 function added in store method :
     *      @method POST
     *
     *  Request Types:
     *
     * 1. order_return_list : Get order return list with particular marketplace wise,
     *      @param  $request type,$marketplace_id
     *      @return \Illuminate\Http\Response
     *
     */
    public function store(Request $request)
    {
        if($request->request_type == 'order_return_list'){

            $data=Mws_order_returns::with('mws_product','usermarketplace')->where(array('user_marketplace_id'=>$request->id))->get()->ToArray();        
          $order_return_list = array();
         if(!empty($data))
            {
                 foreach ($data as $key => $post)
                {
                    $action = '' ;
                    
                    $nestedData['sku'] =  $post['sku'];
                    $nestedData['product_name'] =  '<span title="'.$post['product_name'].'">'.substr(strip_tags($post['product_name']),0,25)."...".'</span>';
                    $nestedData['asin'] =  $post['asin'];
                    $nestedData['fnsku'] =  $post['fnsku'];
                    $nestedData['return_date'] =  date('Y-m-d',strtotime($post['return_date']));
                    $nestedData['quantity'] =  $post['quantity'];
                    $nestedData['fulfillment_center_id'] =  $post['fulfillment_center_id'];
                    $nestedData['detailed_disposition'] =  $post['detailed_disposition'];
                    $nestedData['reason'] =  $post['reason'];
                    $nestedData['status'] =  $post['status'];
                    $nestedData['license_plate_number'] =  $post['license_plate_number'];
                    $nestedData['customer_comments'] =  $post['customer_comments'] != null ? $post['customer_comments'] : "-";  
                    $order_return_list[] = $nestedData;
                }    
             }
                
           echo json_encode($order_return_list); exit;
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

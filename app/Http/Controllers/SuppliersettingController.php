<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Supplier_default_setting;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use App\Models\Application_modules;
use App\Models\Role_access_modules;
use App\Models\Users;

class SuppliersettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    Protected $userRolePermissionId;

    function __construct()
    {
        $this->middleware(function ($request, $next) {

            if (!Auth::user()) {
                return redirect('login');
            }
            $moduleId = Application_modules::where('module', 'supplier_setting_module')->first()->id;
            $userRoleID = Users::with('user_roles')->whereIn('id', [Auth::id()])->first()->user_roles[0]->id;
            $this->userRolePermissionId = Role_access_modules::where('role_id', $userRoleID)->where('application_module_id', $moduleId)->first();
            return $next($request);
        });
    }

    /**
     * Display a suppliers default setting.
     *
     * @param  no-params
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $get_checks = get_access('supplier_setting_module','view');
        $get_user_access = get_user_check_access('supplier_setting_module','view');
        if ($get_checks == 1) {
            if ($get_user_access == 0) {
                $message = get_messages("Don't have Access Rights for this Functionality", 0);
                Session::flash('message', $message);
                return redirect('/index');
            }
        } else {
                if ($get_user_access == 0) {
                    $message = get_messages("Don't have Access Rights for this Functionality", 0);
                    Session::flash('message', $message);
                    return redirect('/index');
                }
        }

        $userRoleId = get_user_role(Auth::user()->id);
        if($userRoleId['user_assigned_role']['user_role_id'] != 1 && $userRoleId['user_assigned_role']['user_role_id'] != 2){
            $message = get_messages("Don't have Access Rights for this Functionality", 0);
            Session::flash('message', $message);
            return redirect('/index');
        }

        $supplier_setting = Supplier_default_setting::first();
        return view('suppliers.suppliersetting.setting',[

            'supplier_setting'=> $supplier_setting
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * update or create the suppliers settings .
     * @method POST
     * @param  \Illuminate\Http\Response $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $userRoleId = get_user_role(Auth::user()->id);
        if($userRoleId['user_assigned_role']['user_role_id'] != 1 && $userRoleId['user_assigned_role']['user_role_id'] != 2){
            $message = get_messages("Don't have Access Rights for this Functionality", 0);
            Session::flash('message', $message);
            return redirect('/index');
        }
        $req_data = [
        'lead_time' => $request->lead_time,
        'order_volume' => $request->order_volume,
        'quantity_discount' => $request->quantity_discount,
        'moq' => $request->moq,
        'CBM_Per_Container' => $request->CBM_Per_Container,
        'Production_Time'=>$request->Production_Time,
        'Boat_To_Port' =>$request->Boat_To_Port,
        'Port_To_Warehouse' =>$request->Port_To_Warehouse,
        'Warehouse_Receipt' =>$request->Warehouse_Receipt != '' ? $request->Warehouse_Receipt : 0,
        'Ship_To_Specific_Warehouse' =>$request->Ship_To_Specific_Warehouse
        ];
        $supplier_setting = Supplier_default_setting::first();
        if (!empty($supplier_setting)) {
        $supplier_setting->update($req_data);
        $message = get_messages('Suppliers setting updated successfully', 1);
        Session::flash('message', $message);
        return redirect()->back();
        } 
        else
        {
            Supplier_default_setting::create($req_data);
            $message = get_messages('Suppliers setting created successfully', 1);
            Session::flash('message', $message);
            return redirect()->back();
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

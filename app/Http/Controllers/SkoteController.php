<?php

namespace App\Http\Controllers;

use App\Models\Passwordreset;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
class SkoteController extends Controller
{

    public function index(Request $request)
    {
        return view($request->path());
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    /**
     * Send mail to forgot your password
     * @method POST
     * @param  $email
     * @return \Illuminate\Http\Response
     */

    public function forgetpassword()
    {
        $userDetails = User::where('email', $_POST['email'])->first();
        if (!empty($userDetails)) {
            $activate_code = md5(rand(1, (int) 10000000000) . "skote" . rand(1, (int) 100000));
            $data['confirmation_code'] = $activate_code;
            Mail::send('emailtemplate.resetpassword', $data, function ($message) use ($data, $userDetails) {
                $message->to($userDetails['email'], $userDetails['name'])
                    ->subject('Verify your email address');
            });
            if (Mail::failures()) {
                $message = get_messages('Something wrong to sending Mail Please Contact System Admin.', 0);
                Session::flash('message', $message);
            } else {
                $passwordReset = new Passwordreset();
                $passwordReset->email = $_POST['email'];
                $passwordReset->token = $activate_code;
                $passwordReset->save();
                $message = get_messages('Check your mail to update password.', 1);
                Session::flash('message', $message);
            }
            return redirect(url('login'));
        } else {
            return redirect(url('login'));
        }
    }

    /**
     * Update or reset your current password
     * @method POST
     * @param  $email, $password
     * @return \Illuminate\Http\Response
     */

    public function resetpassword(Request $request){
         $request->validate([
            'token' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
            $getDetail = Passwordreset::where(['email'=>$request->email,'token'=>$request->token])->first();
            if(!empty($getDetail)){
                $user =  User::where('email',$request->email)->update(['password'=>Hash::make($request->password)]);
                Passwordreset::where('id',$getDetail['id'])->delete();
                $message = get_messages('Your password update successfully', 1);
                Session::flash('message', $message);
            }
            else{
                $message = get_messages('Something went wrong', 0);
                Session::flash('message', $message);
            }
        return redirect(url('login'));
    }
}

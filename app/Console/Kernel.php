<?php

namespace App\Console;

use App\Jobs\CalculateSalesDaily;
use App\Jobs\CalculateTrend;
use App\Jobs\ExportDataExcel;
use App\Jobs\InboundShipments;
use App\Jobs\LogicCalculations;
use App\Jobs\LogicCalculations_final;
use App\Jobs\OrderProcessReportsJob;
use App\Jobs\ProcessReportsHighJob;
use App\Jobs\ProcessReportsJob;
use App\Jobs\ProcessRequestedReport;
use App\Jobs\ProcessSPAPIReportsJob;
use App\Jobs\RequestReport;
use App\Jobs\StockCalculations;
use App\Jobs\FetchProductDetail;
use App\Models\Amazon_requestlog;
use App\Models\Amzdateiteration;
use App\Jobs\RequestSPAPIReport;
use App\Models\SPApiUserMarketplace;
use App\Models\Usermarketplace;
use Illuminate\Console\Scheduling\Schedule;
use App\Jobs\AdjustedInventoryDaily;
use App\Jobs\InboundShipmentSPAPI;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//        $this->RequestOrderSPAPIReport($schedule);
//        $this->requestProductSPAPIReport($schedule);
//        $this->requestInventorySPAPIReport($schedule);
//        $this->requestExcessInventorySPAPIReport($schedule);
//        $this->requestAFNInventorySPAPIReport($schedule);
//        $this->reservedInventorySPAPI($schedule);
//        $this->requestAdjustmentInventorySPAPIReport($schedule);
//        $this->unsuppressedInventorySPAPIReport($schedule);
//        $this->requestOrderReturnSPAPIReport($schedule);
//        $this->requestRestockInventoryRecommendationsSPAPI($schedule);
//        $this->ProcessSPAPIReport($schedule);
        $this->unsuppressedInventoryRequestCheckReport($schedule);
        $this->processReports($schedule);
        $this->processReportsHigh($schedule);
        $this->outofstockcalculationshistorical($schedule);
        $this->outofstockcalculations($schedule);
        $this->calculateTrends($schedule);
        $this->requestProductReport($schedule);
        $this->requestOrderReport($schedule);
        $this->requestHistoricalInventoryReport($schedule);
        $this->requestInventoryReport($schedule);
        $this->requestHistoricalOrderReport($schedule);
        $this->requestExcessInventoryReport($schedule);
        $this->requestAFNInventoryReport($schedule);
        $this->dailyLogicCalculations($schedule);
        $this->dailyLogicCalculations_final($schedule);
        $this->inboundshipments($schedule);
        $this->reservedInventory($schedule);
        $this->requestAdjustmentInventoryReport($schedule);
        $this->unsuppressedInventoryReport($schedule);
        $this->fetchProductDetail($schedule);
//        $this->exportDataExcel($schedule);
        $this->requestOrderReturnReport($schedule);
        $this->requestHistoricalOrderReturnReport($schedule);
        $this->orderProcessReports($schedule);
        $this->calculateSalesDaily($schedule);
        $this->adjustedInventoryDaily($schedule);
        $this->adjustedInventoryDailyHistorical($schedule);
        $this->requestRestockInventoryRecommendations($schedule);
        //$this->historicalUnsuppressed($schedule);
        // $schedule->command('inspire')
        //          ->hourly();
        //$this->inboundshipmentSPAPI($schedule);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
//        $this->load(__DIR__.'/Commands');

//        require base_path('routes/console.php');
    }

    private function RequestOrderSPAPIReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (SPApiUserMarketplace::with('market_place_developer')->get()->all() as $marketplace) {
                if (empty($marketplace->refresh_token) || empty($marketplace->client_id)) {
                    continue;
                }
                if (Amazon_requestlog::where('report_type', 'GET_FLAT_FILE_ALL_ORDERS_DATA_BY_LAST_UPDATE_GENERAL')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $productRequestJob = new RequestSPAPIReport($marketplace, 'GET_FLAT_FILE_ALL_ORDERS_DATA_BY_LAST_UPDATE_GENERAL');
                dispatch($productRequestJob);
            }
        })->everyThirtyMinutes();
    }
    private function requestProductSPAPIReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (SPApiUserMarketplace::with('market_place_developer')->get()->all() as $marketplace) {
                if (empty($marketplace->refresh_token) || empty($marketplace->client_id)) {
                    continue;
                }
                if (Amazon_requestlog::where('report_type', 'GET_FBA_ESTIMATED_FBA_FEES_TXT_DATA')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $productRequestJob = new RequestSPAPIReport($marketplace, 'GET_FBA_ESTIMATED_FBA_FEES_TXT_DATA');
                dispatch($productRequestJob);
            }
        })->everyThirtyMinutes();
    }
    private function requestInventorySPAPIReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (SPApiUserMarketplace::with('market_place_developer')->get()->all() as $marketplace) {
                if (empty($marketplace->refresh_token) || empty($marketplace->client_id)) {
                    continue;
                }
                if (Amazon_requestlog::where('report_type', 'GET_FBA_FULFILLMENT_CURRENT_INVENTORY_DATA')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $productRequestJob = new RequestSPAPIReport($marketplace, 'GET_FBA_FULFILLMENT_CURRENT_INVENTORY_DATA');
                dispatch($productRequestJob);
            }
        })->everyThirtyMinutes();
    }
    private function requestExcessInventorySPAPIReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (SPApiUserMarketplace::with('market_place_developer')->get()->all() as $marketplace) {
                if (empty($marketplace->refresh_token) || empty($marketplace->client_id)) {
                    continue;
                }
                if (Amazon_requestlog::where('report_type', 'GET_EXCESS_INVENTORY_DATA')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $productRequestJob = new RequestSPAPIReport($marketplace, 'GET_EXCESS_INVENTORY_DATA');
                dispatch($productRequestJob);
            }
        })->everyThirtyMinutes();
    }
    private function requestAFNInventorySPAPIReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (SPApiUserMarketplace::with('market_place_developer')->get()->all() as $marketplace) {
                if (empty($marketplace->refresh_token) || empty($marketplace->client_id)) {
                    continue;
                }
                if (Amazon_requestlog::where('report_type', 'GET_AFN_INVENTORY_DATA')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $productRequestJob = new RequestSPAPIReport($marketplace, 'GET_AFN_INVENTORY_DATA');
                dispatch($productRequestJob);
            }
        })->everyThirtyMinutes();
    }
    private function reservedInventorySPAPI(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (SPApiUserMarketplace::with('market_place_developer')->get()->all() as $marketplace) {
                if (empty($marketplace->refresh_token) || empty($marketplace->client_id)) {
                    continue;
                }
                if (Amazon_requestlog::where('report_type', 'GET_RESERVED_INVENTORY_DATA')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $productRequestJob = new RequestSPAPIReport($marketplace, 'GET_RESERVED_INVENTORY_DATA');
                dispatch($productRequestJob);
            }
        })->everyThirtyMinutes();
    }
    private function requestAdjustmentInventorySPAPIReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (SPApiUserMarketplace::with('market_place_developer')->get()->all() as $marketplace) {
                if (empty($marketplace->refresh_token) || empty($marketplace->client_id)) {
                    continue;
                }
                if (Amazon_requestlog::where('report_type', 'GET_FBA_FULFILLMENT_INVENTORY_ADJUSTMENTS_DATA')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $productRequestJob = new RequestSPAPIReport($marketplace, 'GET_FBA_FULFILLMENT_INVENTORY_ADJUSTMENTS_DATA');
                dispatch($productRequestJob);
            }
        })->everyThirtyMinutes();
    }
    private function unsuppressedInventorySPAPIReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (SPApiUserMarketplace::with('market_place_developer')->get()->all() as $marketplace) {
                if (empty($marketplace->refresh_token) || empty($marketplace->client_id)) {
                    continue;
                }
                if (Amazon_requestlog::where('report_type', 'GET_FBA_MYI_UNSUPPRESSED_INVENTORY_DATA')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $productRequestJob = new RequestSPAPIReport($marketplace, 'GET_FBA_MYI_UNSUPPRESSED_INVENTORY_DATA');
                dispatch($productRequestJob);
            }
        })->everyThirtyMinutes();
    }
    private function requestOrderReturnSPAPIReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (SPApiUserMarketplace::with('market_place_developer')->get()->all() as $marketplace) {
                if (empty($marketplace->refresh_token) || empty($marketplace->client_id)) {
                    continue;
                }
                if (Amazon_requestlog::where('report_type', 'GET_FLAT_FILE_RETURNS_DATA_BY_RETURN_DATE')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $productRequestJob = new RequestSPAPIReport($marketplace, 'GET_FLAT_FILE_RETURNS_DATA_BY_RETURN_DATE');
                dispatch($productRequestJob);
            }
        })->everyThirtyMinutes();
    }
    private function requestRestockInventoryRecommendationsSPAPI(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (SPApiUserMarketplace::with('market_place_developer')->get()->all() as $marketplace) {
                if (empty($marketplace->refresh_token) || empty($marketplace->client_id)) {
                    continue;
                }
                if (Amazon_requestlog::where('report_type', 'GET_RESTOCK_INVENTORY_RECOMMENDATIONS_REPORT')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $productRequestJob = new RequestSPAPIReport($marketplace, 'GET_RESTOCK_INVENTORY_RECOMMENDATIONS_REPORT');
                dispatch($productRequestJob);
            }
        })->everyThirtyMinutes();
    }
    private function ProcessSPAPIReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (SPApiUserMarketplace::with('market_place_developer')->get()->all() as $marketplace) {
                if (empty($marketplace->refresh_token) || empty($marketplace->client_id)) {
                    continue;
                }
                $loopBreakStatus = 0;
                $queueArray = \DB::table(config('queue.connections.database.table'))->get()->toArray();
                if (count($queueArray) > 0) {
                    foreach ($queueArray as $queue) {
                        $payload = json_decode($queue->payload, true);
                        $payloadCommandData = unserialize($payload['data']['command']);
                        if ($payload['displayName'] == 'App\Jobs\ProcessSPAPIReportsJob' && $payloadCommandData->marketplace->user_id == $marketplace->user_id) {
                            $loopBreakStatus = 1;
                            break;
                        }
                    }
                }
                if ($loopBreakStatus == 1) {
                    continue;
                } else {
                    $processReportJob = new ProcessSPAPIReportsJob($marketplace);
                    dispatch($processReportJob);
                }
            }
        })->everyFifteenMinutes();
    }

    private function requestProductReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                if (Amazon_requestlog::where('report_type', '_GET_FBA_ESTIMATED_FBA_FEES_TXT_DATA_')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $productRequestJob = new RequestReport($marketplace, '_GET_FBA_ESTIMATED_FBA_FEES_TXT_DATA_');
                dispatch($productRequestJob)->onQueue('low');
            }
        })->daily();
    }

    private function requestOrderReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                if (Amazon_requestlog::whereDay('created_at', '=', date('d'))->where('report_type', '_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_LAST_UPDATE_')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $amzDateitetration = Amzdateiteration::where('user_marketplace_id', $marketplace['id'])->where('OrderStatus', '0')->first();
                if (empty($amzDateitetration)) {
                    $orderRequestJob = new RequestReport($marketplace, '_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_LAST_UPDATE_', $amzDateitetration);
                    dispatch($orderRequestJob)->onQueue('low');
                }
            }
        })->cron("45 4,8,12,16,20,23 * * *");
    }

    private function requestHistoricalOrderReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                if (Amazon_requestlog::whereDay('created_at', '=', date('d'))->where('report_type', '_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_LAST_UPDATE_')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $amzDateitetration = Amzdateiteration::where('user_marketplace_id', $marketplace['id'])->where('OrderStatus', '0')->first();
                if (!empty($amzDateitetration)) {
                    $orderRequestJob = new RequestReport($marketplace, '_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_LAST_UPDATE_', $amzDateitetration);
                    dispatch($orderRequestJob)->onQueue('low');
                }
            }
        })->hourly();
    }

    private function outofstockcalculations(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                $loopBreakStatus = 0;
                $queueArray = \DB::table(config('queue.connections.database.table'))->get()->toArray();
                if (count($queueArray) > 0) {
                    foreach ($queueArray as $queue) {
                        $payload = json_decode($queue->payload, true);
                        $payloadCommandData = unserialize($payload['data']['command']);
                        if ($payload['displayName'] == 'App\Jobs\StockCalculations' && $payloadCommandData->marketplace->user_id == $marketplace->user_id) {
                            $loopBreakStatus = 1;
                            break;
                        }
                    }
                }
                if ($loopBreakStatus == 1) {
                    continue;
                } else {
                    $amzDateitetration = Amzdateiteration::where('user_marketplace_id', $marketplace['id'])->where('InventoryStatus', '2')->first();
                    if (empty($amzDateitetration)) {
                        $orderRequestJob = new StockCalculations($marketplace);
                        dispatch($orderRequestJob)->onQueue('low');
                    }
                }
            }
        })->twiceDaily(1, 13);
    }

    private function outofstockcalculationshistorical(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                $loopBreakStatus = 0;
                $queueArray = \DB::table(config('queue.connections.database.table'))->get()->toArray();
                if (count($queueArray) > 0) {
                    foreach ($queueArray as $queue) {
                        $payload = json_decode($queue->payload, true);
                        $payloadCommandData = unserialize($payload['data']['command']);
                        if ($payload['displayName'] == 'App\Jobs\StockCalculations' && $payloadCommandData->marketplace->user_id == $marketplace->user_id) {
                            $loopBreakStatus = 1;
                            break;
                        }
                    }
                }
                if ($loopBreakStatus == 1) {
                    continue;
                } else {
                    $amzDateitetration = Amzdateiteration::where('user_marketplace_id', $marketplace['id'])->where('InventoryStatus', '1')->first();
                    if (!empty($amzDateitetration)) {
                        $outofstockRequestJob = new StockCalculations($marketplace, $amzDateitetration);
                        dispatch($outofstockRequestJob)->onQueue('high');
                    }
                }
            }
        })->cron("0 */4 * * *");
    }

    private function requestInventoryReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                if (Amazon_requestlog::whereDay('created_at', '=', date('d'))->where('report_type', '_GET_FBA_FULFILLMENT_CURRENT_INVENTORY_DATA_')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $amzDateitetration = Amzdateiteration::where('user_marketplace_id', $marketplace['id'])->where('InventoryStatus', '0')->first();
                if (empty($amzDateitetration)) {
                    $inventoryRequestJob = new RequestReport($marketplace, '_GET_FBA_FULFILLMENT_CURRENT_INVENTORY_DATA_');
                    dispatch($inventoryRequestJob)->onQueue('low');
                }
            }
        })->cron("0 */4 * * *");
    }

    private function requestHistoricalInventoryReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                if (Amazon_requestlog::whereDay('created_at', '=', date('d'))->where('report_type', '_GET_FBA_FULFILLMENT_CURRENT_INVENTORY_DATA_')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $amzDateitetration = Amzdateiteration::where('user_marketplace_id', $marketplace['id'])->where('InventoryStatus', '0')->first();
                if (!empty($amzDateitetration)) {
                    $inventoryRequestJob = new RequestReport($marketplace, '_GET_FBA_FULFILLMENT_CURRENT_INVENTORY_DATA_', $amzDateitetration);
                    dispatch($inventoryRequestJob)->onQueue('high');
                }
            }
        })->cron("0 */2 * * *");
    }

    private function requestExcessInventoryReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                if (Amazon_requestlog::whereDay('created_at', '=', date('d'))->where('report_type', '_GET_EXCESS_INVENTORY_DATA_')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $excessInventoryRequestJob = new RequestReport($marketplace, '_GET_EXCESS_INVENTORY_DATA_');
                dispatch($excessInventoryRequestJob)->onQueue('low');
            }
        })->daily();
    }

    private function requestAFNInventoryReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                if (Amazon_requestlog::whereDay('created_at', '=', date('d'))->where('report_type', '_GET_AFN_INVENTORY_DATA_')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $excessInventoryRequestJob = new RequestReport($marketplace, '_GET_AFN_INVENTORY_DATA_');
                dispatch($excessInventoryRequestJob)->onQueue('low');
            }
        })->cron("20 */4 * * *");
    }

    private function processReports(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                $loopBreakStatus = 0;
                $queueArray = \DB::table(config('queue.connections.database.table'))->get()->toArray();
                if (count($queueArray) > 0) {
                    foreach ($queueArray as $queue) {
                        $payload = json_decode($queue->payload, true);
                        $payloadCommandData = unserialize($payload['data']['command']);
                        if ($payload['displayName'] == 'App\Jobs\ProcessReportsJob' && $payloadCommandData->marketplace->user_id == $marketplace->user_id) {
                            $loopBreakStatus = 1;
                            break;
                        }
                    }
                }
                if ($loopBreakStatus == 1) {
                    continue;
                } else {
                    $processReportJob = new ProcessReportsJob($marketplace);
                    dispatch($processReportJob)->onQueue('low');
                }
            }
        })->everyFifteenMinutes();
    }

    private function processReportsHigh(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                $loopBreakStatus = 0;
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                $queueArray = \DB::table(config('queue.connections.database.table'))->get()->toArray();
                if (count($queueArray) > 0) {
                    foreach ($queueArray as $queue) {
                        $payload = json_decode($queue->payload, true);
                        $payloadCommandData = unserialize($payload['data']['command']);
                        if ($payload['displayName'] == 'App\Jobs\ProcessReportsHighJob' && $payloadCommandData->marketplace->user_id == $marketplace->user_id) {
                            $loopBreakStatus = 1;
                            break;
                        }
                    }
                }
                if ($loopBreakStatus == 1) {
                    continue;
                } else {
                    $processReportJob = new ProcessReportsHighJob($marketplace);
                    dispatch($processReportJob)->onQueue('high');
                }
            }
        })->everyFifteenMinutes();
    }

    private function calculateTrends(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                $loopBreakStatus = 0;
                $queueArray = \DB::table(config('queue.connections.database.table'))->get()->toArray();
                if (count($queueArray) > 0) {
                    foreach ($queueArray as $queue) {
                        $payload = json_decode($queue->payload, true);
                        $payloadCommandData = unserialize($payload['data']['command']);
                        if ($payload['displayName'] == 'App\Jobs\CalculateTrend' && $payloadCommandData->marketplace->user_id == $marketplace->user_id) {
                            $loopBreakStatus = 1;
                            break;
                        }
                    }
                }
                if ($loopBreakStatus == 1) {
                    continue;
                } else {
                    $amzDateitetration = Amzdateiteration::where('user_marketplace_id', $marketplace['id'])->where('OrderStatus', '1')->first();
                    $calculateTrendRequestJob = new CalculateTrend($marketplace, $amzDateitetration);
                    dispatch($calculateTrendRequestJob)->onQueue('low');
                }
            }
        })->twiceDaily(3, 15);
    }

    private function dailyLogicCalculations(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                $loopBreakStatus = 0;
                $queueArray = \DB::table(config('queue.connections.database.table'))->get()->toArray();
                if (count($queueArray) > 0) {
                    foreach ($queueArray as $queue) {
                        $payload = json_decode($queue->payload, true);
                        $payloadCommandData = unserialize($payload['data']['command']);
                        if ($payload['displayName'] == 'App\Jobs\LogicCalculations' && $payloadCommandData->marketplace->user_id == $marketplace->user_id) {
                            $loopBreakStatus = 1;
                            break;
                        }
                    }
                }
                if ($loopBreakStatus == 1) {
                    continue;
                } else {
                    $amzDateitetration = Amzdateiteration::where('user_marketplace_id', $marketplace['id'])->where('OrderStatus', '1')->count();
                    if ($amzDateitetration > 18) {
                        $logicCalculationsJob = new LogicCalculations($marketplace);
                        dispatch($logicCalculationsJob)->onQueue('low');
                    }
                }
            }
        })->twiceDaily(1, 12);
    }

    private function dailyLogicCalculations_final(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                $loopBreakStatus = 0;
                $queueArray = \DB::table(config('queue.connections.database.table'))->get()->toArray();
                if (count($queueArray) > 0) {
                    foreach ($queueArray as $queue) {
                        $payload = json_decode($queue->payload, true);
                        $payloadCommandData = unserialize($payload['data']['command']);
                        if ($payload['displayName'] == 'App\Jobs\LogicCalculations_final' && $payloadCommandData->marketplace->user_id == $marketplace->user_id) {
                            $loopBreakStatus = 1;
                            break;
                        }
                    }
                }
                if ($loopBreakStatus == 1) {
                    continue;
                } else {
                    $amzDateitetration = Amzdateiteration::where('user_marketplace_id', $marketplace['id'])->where('OrderStatus', '1')->count();
                    if ($amzDateitetration > 18) {
                        $logicCalculationsJob = new LogicCalculations_final($marketplace);
                        dispatch($logicCalculationsJob)->onQueue('low');
                    }
                }
            }
        })->twiceDaily(1, 12);
    }


    private function inboundshipments(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken) || ($marketplace->id == 1)) {
                    continue;
                }
                $loopBreakStatus = 0;
                $queueArray = \DB::table(config('queue.connections.database.table'))->get()->toArray();
                if (count($queueArray) > 0) {
                    foreach ($queueArray as $queue) {
                        $payload = json_decode($queue->payload, true);
                        $payloadCommandData = unserialize($payload['data']['command']);
                        if ($payload['displayName'] == 'App\Jobs\InboundShipments' && $payloadCommandData->marketplace->user_id == $marketplace->user_id) {
                            $loopBreakStatus = 1;
                            break;
                        }
                    }

                }
                if ($loopBreakStatus == 1) {
                    continue;
                } else {
                    $inboundShipmentJob = new InboundShipments($marketplace);
                    dispatch($inboundShipmentJob)->onQueue('low');
                }
            }
        })->cron("30 7,15,23 * * *");
    }

    private function reservedInventory(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                if (Amazon_requestlog::whereDay('created_at', '=', date('d'))->where('report_type', '_GET_RESERVED_INVENTORY_DATA_')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $reservedInventoryRequestJob = new RequestReport($marketplace, '_GET_RESERVED_INVENTORY_DATA_');
                dispatch($reservedInventoryRequestJob)->onQueue('low');
            }
        })->twiceDaily(4, 16);
    }

    private function requestAdjustmentInventoryReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                if (Amazon_requestlog::whereDay('created_at', '=', date('d'))->where('report_type', '_GET_FBA_FULFILLMENT_INVENTORY_ADJUSTMENTS_DATA_')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $adjustmentInventoryRequestJob = new RequestReport($marketplace, '_GET_FBA_FULFILLMENT_INVENTORY_ADJUSTMENTS_DATA_');
                dispatch($adjustmentInventoryRequestJob)->onQueue('low');
            }
        })->daily();
    }

    private function unsuppressedInventoryReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                if (Amazon_requestlog::whereDay('created_at', '=', date('d'))->where('report_type', '_GET_FBA_MYI_UNSUPPRESSED_INVENTORY_DATA_')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $unsuppressedInventoryReportRequestJob = new RequestReport($marketplace, '_GET_FBA_MYI_UNSUPPRESSED_INVENTORY_DATA_');
                dispatch($unsuppressedInventoryReportRequestJob)->onQueue('low');
            }
        })->cron("15 6,12,18,23 * * *");
    }

    private function unsuppressedInventoryRequestCheckReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                if (Amazon_requestlog::whereDay('created_at', '=', date('d'))->where('report_type', '_GET_FBA_MYI_UNSUPPRESSED_INVENTORY_DATA_')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $unsuppressedInventoryReportRequestJob = new ProcessRequestedReport($marketplace);
                dispatch($unsuppressedInventoryReportRequestJob)->onQueue('low');
            }
        })->everyThirtyMinutes();
    }

    private function fetchProductDetail(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                $fetchproductDetailJob = new FetchProductDetail($marketplace);
                dispatch($fetchproductDetailJob)->onQueue('low');
            }
        })->daily();
    }

    private function exportDataExcel(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                $exportDataExcelJob = new ExportDataExcel($marketplace);
                dispatch($exportDataExcelJob)->onQueue('low');
            }
        })->cron('0 1 * * *');
    }

    private function requestOrderReturnReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                if (Amazon_requestlog::whereDay('created_at', '=', date('d'))->where('report_type', '_GET_FBA_FULFILLMENT_CUSTOMER_RETURNS_DATA_')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $amzDateitetration = Amzdateiteration::where('user_marketplace_id', $marketplace['id'])->where('ReturnStatus', '0')->first();
                if (empty($amzDateitetration)) {
                    $orderReturnRequestJob = new RequestReport($marketplace, '_GET_FBA_FULFILLMENT_CUSTOMER_RETURNS_DATA_', $amzDateitetration);
                    dispatch($orderReturnRequestJob)->onQueue('low');
                }
            }
        })->cron("30 */4 * * *");
    }

    private function requestHistoricalOrderReturnReport(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                if (Amazon_requestlog::whereDay('created_at', '=', date('d'))->where('report_type', '_GET_FBA_FULFILLMENT_CUSTOMER_RETURNS_DATA_')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $amzDateitetration = Amzdateiteration::where('user_marketplace_id', $marketplace['id'])->where('ReturnStatus', '0')->first();
                if (!empty($amzDateitetration)) {
                    $orderReturnRequestJob = new RequestReport($marketplace, '_GET_FBA_FULFILLMENT_CUSTOMER_RETURNS_DATA_', $amzDateitetration);
                    dispatch($orderReturnRequestJob)->onQueue('low');
                }
            }
        })->hourly();
    }

    private function orderProcessReports(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                $loopBreakStatus = 0;
                $queueArray = \DB::table(config('queue.connections.database.table'))->get()->toArray();
                if (count($queueArray) > 0) {
                    foreach ($queueArray as $queue) {
                        $payload = json_decode($queue->payload, true);
                        $payloadCommandData = unserialize($payload['data']['command']);
                        if ($payload['displayName'] == 'App\Jobs\OrderProcessReportsJob' && $payloadCommandData->marketplace->user_id == $marketplace->user_id) {
                            $loopBreakStatus = 1;
                            break;
                        }
                    }
                }
                if ($loopBreakStatus == 1) {
                    continue;
                } else {
                    $orderProcessReportJob = new OrderProcessReportsJob($marketplace);
                    dispatch($orderProcessReportJob)->onQueue('medium');
                }
            }
        })->everyFifteenMinutes();
    }

    private function calculateSalesDaily(Schedule $schedule){
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                $loopBreakStatus = 0;
                $queueArray = \DB::table(config('queue.connections.database.table'))->get()->toArray();
                if (count($queueArray) > 0) {
                    foreach ($queueArray as $queue) {
                        $payload = json_decode($queue->payload, true);
                        $payloadCommandData = unserialize($payload['data']['command']);
                        if ($payload['displayName'] == 'App\Jobs\CalculateSalesDaily' && $payloadCommandData->marketplace->user_id == $marketplace->user_id) {
                            $loopBreakStatus = 1;
                            break;
                        }
                    }
                }
                if ($loopBreakStatus == 1) {
                    continue;
                } else {
                    $calculateSalesDailyJob = new CalculateSalesDaily($marketplace);
                    dispatch($calculateSalesDailyJob)->onQueue('custom');
                }
            }
        })->cron("45 0,5,9,13,17,21 * * *");
    }

    private function adjustedInventoryDailyHistorical(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                $loopBreakStatus = 0;
                $queueArray = \DB::table(config('queue.connections.database.table'))->get()->toArray();
                if (count($queueArray) > 0) {
                    foreach ($queueArray as $queue) {
                        $payload = json_decode($queue->payload, true);
                        $payloadCommandData = unserialize($payload['data']['command']);
                        if ($payload['displayName'] == 'App\Jobs\CalculateSalesDaily' && $payloadCommandData->marketplace->user_id == $marketplace->user_id) {
                            $loopBreakStatus = 1;
                            break;
                        }
                    }
                }
                if ($loopBreakStatus == 1) {
                    continue;
                } else {
                    $amzDateitetration = Amzdateiteration::where('user_marketplace_id', $marketplace->id)
                        ->where('adjustedCalculations', '0')
                        ->where('OrderStatus', '1')
                        ->where('ReturnStatus', '1')
                        ->where('InventoryStatus', '2')
                        ->orderBy('startDate', 'asc')
                        ->first();
                    if (!empty($amzDateitetration)) {
                        $adjustedInventoryDaily = new AdjustedInventoryDaily($marketplace, $amzDateitetration);
                        dispatch($adjustedInventoryDaily)->onQueue('low');
                    }
                }
            }
        })->cron("30 */1 * * *");
    }

    private function adjustedInventoryDaily(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                $loopBreakStatus = 0;
                $queueArray = \DB::table(config('queue.connections.database.table'))->get()->toArray();
                if (count($queueArray) > 0) {
                    foreach ($queueArray as $queue) {
                        $payload = json_decode($queue->payload, true);
                        $payloadCommandData = unserialize($payload['data']['command']);
                        if ($payload['displayName'] == 'App\Jobs\CalculateSalesDaily' && $payloadCommandData->marketplace->user_id == $marketplace->user_id) {
                            $loopBreakStatus = 1;
                            break;
                        }
                    }
                }
                if ($loopBreakStatus == 1) {
                    continue;
                } else {
                    $amzDateitetration = Amzdateiteration::where('user_marketplace_id', $marketplace->id)
                        ->where('adjustedCalculations', '0')
                        ->where('OrderStatus', '1')
                        ->where('ReturnStatus', '1')
                        ->where('InventoryStatus', '2')
                        ->orderBy('startDate', 'asc')
                        ->first();
                    if (empty($amzDateitetration)) {
                        $adjustedInventoryDaily = new AdjustedInventoryDaily($marketplace);
                        dispatch($adjustedInventoryDaily)->onQueue('low');
                    }
                }
            }
        })->cron("15 */6 * * *");
    }
    private function requestRestockInventoryRecommendations(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (Usermarketplace::all() as $marketplace) {
                if (empty($marketplace->mws_sellerid) || empty($marketplace->mws_authtoken)) {
                    continue;
                }
                if (Amazon_requestlog::whereDay('created_at', '=', date('d'))->where('report_type', '_GET_RESTOCK_INVENTORY_RECOMMENDATIONS_REPORT_')->where('processed', 0)->where('user_marketplace_id', $marketplace->id)->count() > 0) {
                    continue;
                }
                $inventoryRequestJob = new RequestReport($marketplace, '_GET_RESTOCK_INVENTORY_RECOMMENDATIONS_REPORT_');
                dispatch($inventoryRequestJob)->onQueue('low');
            }
        })->daily();
    }
    private function inboundshipmentSPAPI(Schedule $schedule)
    {
        $schedule->call(function () {
            foreach (SPApiUserMarketplace::with('market_place_developer')->get()->all() as $marketplace) {
                if (empty($marketplace->refresh_token) || empty($marketplace->client_id)) {
                    continue;
                }
                $loopBreakStatus = 0;
                $queueArray = \DB::table(config('queue.connections.database.table'))->get()->toArray();
                if (count($queueArray) > 0) {
                    foreach ($queueArray as $queue) {
                        $payload = json_decode($queue->payload, true);
                        $payloadCommandData = unserialize($payload['data']['command']);
                        if ($payload['displayName'] == 'App\Jobs\InboundShipmentSPAPI' && $payloadCommandData->marketplace->user_id == $marketplace->user_id) {
                            $loopBreakStatus = 1;
                            break;
                        }
                    }

                }
                if ($loopBreakStatus == 1) {
                    continue;
                } else {
                    $inboundShipmentJob = new InboundShipmentSPAPI($marketplace);
                    dispatch($inboundShipmentJob)->onQueue('low');
                }
            }
        })->everyMinute();
    }

}

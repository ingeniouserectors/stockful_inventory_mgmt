@extends('layouts.master-final')

@section('title') Warehouses @endsection

@section('content')
    <div class="container-fluid" id="content" :key="forceRender">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0 font-size-18">Warehouses</h4>
            <form action="{{ route('warehouses.store') }}" method="post" id="import_csv"  enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="page-title-right">
                    <a href="{{ route('warehouses.show','import') }}" class="btn btn-light1 waves-effect mr-3">
                        <i class="bx bx-download align-middle" > </i>
                    </a>
                    <a href="javascript: void(0);" onclick="$('#file_upload_input').trigger('click');" class="btn btn-outline-light1 waves-effect mr-3">
                        <i class="bx bx-upload align-middle mr-2"></i>Import Warehouses
                    </a>
                    <a href="{{ route('warehouses.create') }}" class="btn btn-primary waves-effect waves-light">
                        <i class="bx bx-plus-circle align-middle mr-2"></i>Add New Warehouse
                    </a>
                </div>
            </form>
        </div>

        <form action="{{ url('import_warehouse') }}" method="post" id="import_csv_form"  style="display: none;"  enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="file" name="file" id="file_upload_input">
            <button type="submit">Import</button>
        </form>
        @if(session()->has('message'))
                {!! session('message') !!}
            @endif
        <div class="card">
            <div class="card-body">
                <div style="display: inline-flex">
                    <div class="dropdown">
                        <button type="button" class="btn btn-sm btn-light position-relative noti-icon mr-sm-3"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="overflow:visible;"
                        >
                            <i class="mdi mdi-filter font-size-16"></i>
                            <span class="badge badge-primary badge-pill pos2">6</span>
                        </button>
                        <div class="dropdown-menu p-3 mt-1" style="width: 480px;">
                            <form @submit.prevent>
                                <h5> <i class="mdi mdi-filter font-size-16"></i> Filter </h5>
                                <p>
                                    You can pin filters on the page by clicking on <i class="mdi mdi-pin mdi-rotate-45"></i> this button.
                                </p>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="country" class="mb-2">Country</label>
                                            <div class="d-inline-flex">
                                                <select v-model="filter_country" class="form-select w-160px">
                                                    <option value="">Select Country</option>
                                                    <option v-for="(val, key) in countries" :value="key">@{{ val + " (" + key + ")" }}</option>
                                                </select>
                                                <button class="btn" @click="filter_country_pinned = !filter_country_pinned">
                                                    <i class="mdi mdi-pin mdi-rotate-45"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="" class="mb-2"># of SKUs</label>
                                            <div class="d-inline-flex align-items-center">
                                                <input type="text" v-model="filter_min_sku" class="form-control w-75px">
                                                <span class="px-2"> - </span>
                                                <input type="text" v-model="filter_max_sku" class="form-control w-75px">
                                                <button class="btn" @click="filter_sku_pinned = !filter_sku_pinned">
                                                    <i class="mdi mdi-pin mdi-rotate-45"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div>
                                <button class="btn btn-secondary mr-2">Cancel</button>
                                <button class="btn btn-primary" @click="filterData"> Apply </button>
                            </div>
                        </div>
                    </div>

                    <div class="input-group mr-sm-3">
                        <input type="text" class="form-control input-lg" v-model="search" placeholder="Search">
                        <span class="input-group-append">
                            <button class="btn btn-secondary search_all">
                                <i class="bx bx-search-alt-2 align-middle"></i>
                            </button>
                        </span>
                    </div>
                    <div class="dropdown mr-3">
                        <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                            <i class="mdi mdi-chevron-down"></i>
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">Action 1</a>
                            <a class="dropdown-item" href="#">Action 2</a>
                            <a class="dropdown-item" href="#">Action 3</a>
                        </div>
                    </div>
                    <select
                        v-if="filter_country_pinned"
                        v-model="filter_country"
                        class="form-select w-160px mr-4"
                        @change="filterData"
                    >
                        <option value="">Select Country</option>
                        <option v-for="(val, key) in countries" :value="key"> @{{ val + " (" + key + ")" }} </option>
                    </select>
                    <div v-if="filter_sku_pinned">
                        <div class="d-inline-flex align-items-center">
                            <input type="text" v-model="filter_min_sku" class="form-control w-75px" @change="filterData">
                            <span class="px-2"> - </span>
                            <input type="text" v-model="filter_max_sku" class="form-control w-75px" @change="filterData">
                        </div>
                    </div>
                </div>
                <div class="bg-light1 p-2 mt-4">
                    <div class="row">
                        <div class="col-md-6 form-inline">
                            <button
                                id="main_delete_btn"
                                class="text-light1 mr-4 border-0 bg-transparent"
                                @click="bulkDelete()"
                                data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                <i class="mdi mdi-trash-can font-size-18"></i>
                            </button>
                            <label for="displaying" class="mr-2">Displaying</label>
                            <select class="custom-select custom-select-sm form-control-sm mr-2" id="inlineFormCustomSelect">
                                <option value="20">1-20</option>
                                <option value="40">1-40</option>
                                <option value="60">1-60</option>
                                <option value="80">1-80</option>.
                                <option value="100">1-100</option>
                            </select>
                            <label for="" class="mr-4">of @{{ processedRows.length }}</label>

                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="customSwitch1">
                                <label class="custom-control-label" for="customSwitch1">Incomplete</label>
                            </div>
                        </div>

                        <div class="col-md-6 d-flex justify-content-end align-items-center">
                            <div class="dropdown">
                                <button type="button" class="btn btn-sm btn-light position-relative noti-icon mr-sm-3"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="overflow:visible;"
                                        data-toggle-second="tooltip" data-placement="left" data-original-title="Manage Table Columns"
                                >
                                    <i class="mdi mdi-view-parallel font-size-16"></i>
                                    <template v-if="fields.length-cols.length">
                                        <span class="badge badge-primary badge-pill pos2">@{{ fields.length-cols.length }}</span>
                                    </template>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="columns-dropdown" style="width:200px;">
                                    <form>
                                        <p class="px-2"><small>Manage Table Columns</small></p>
                                        <div class="p-2 hover-bg-blue-50" v-for="field in fields" :key="field.slug">
                                            <label class="stockful-checkbox">@{{ field.caption }}
                                                <input type="checkbox" v-model="cols" :value="field.id"  @change="updateCols">
                                                <span></span>
                                            </label>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="d-flex">
                                <button
                                    class="border-0 rounded bg-light shadow pagination-btn p-0 mx-1"
                                    @click.prevent="prevPage"
                                    data-toggle="tooltip" data-placement="top" title="" data-original-title="Prev Page"
                                >
                                    <i class="mdi mdi-chevron-left"></i>
                                </button>
                                <template v-if="currentPage!=1">
                                    <button class="border-0 bg-transparent pagination-btn p-0 mx-1">
                                        1
                                    </button>
                                </template>
                                <button class="border-0 rounded shadow pagination-btn p-0 mx-1 btn-primary">
                                    @{{ currentPage }}
                                </button>
                                <template v-if="currentPage < lastPage">
                                    <button class="border-0 bg-transparent pagination-btn p-0 mx-1">
                                        @{{ lastPage }}
                                    </button>
                                </template>
                                <button
                                    class="border-0 rounded shadow pagination-btn p-0 mx-1"
                                    @click.prevent="nextPage"
                                    data-toggle="tooltip" data-placement="top" title="" data-original-title="Next Page">
                                    <i class="mdi mdi-chevron-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
                <table class="table table-striped">
                    <thead style="background-color: #EEF2FF;">
                    <tr>
                        <th style="width: 3rem;">
                            <label class="stockful-checkbox">
                                <input type="checkbox" @click="updatedSelectPage" v-model="selectPage" >
                                <span></span>
                            </label>
                        </th>
                        <th style="width: 3rem;"></th>
                        <th v-for="field in columns()" :key="field.id" @click="sortBy(field.slug)">
                                @{{ field.caption }}
                                <span :class="(field.slug==sortField && sortDirection=='asc') ? 'text-blue-500 font-weight-bold' : 'text-gray-200'">↑</span>
                                <span :class="(field.slug==sortField && sortDirection=='desc') ? 'text-blue-500 font-weight-bold' : 'text-gray-200'">↓</span>
                            </th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <template v-if="selectPage">
                            <td :colspan="cols.length + 3" class="text-center" style="background-color: #F2F7FF;">
                                <template v-if="selectAll">
                                    Selected all <strong> @{{ processedRows.length }} </strong> warehouses.
                                </template>
                                <template v-else>
                                    Selected @{{ selected.length }} out of @{{ processedRows.length }}.
                                    <button class="btn btn-link" @click="selectAllItems">
                                        Select All @{{ processedRows.length }}
                                    </button>
                                </template>
                            </td>
                        </template>
                    </tr>
                    <tr v-for="row in processedRows.slice(perPage*(currentPage-1), perPage*currentPage)" :key="row.id">
                        <td>
                            <label class="stockful-checkbox">
                                <input type="checkbox" v-model="selected" :value="row.id" @click="updatedSelected">
                                <span></span>
                            </label>
                        </td>
                        <td class="p-2">
                            <template v-if="!row.completeFlag">
                                <i class="fas fa-exclamation-triangle" style="color:#F1B44C;" data-toggle="tooltip" title="" data-original-title="Missing required settings"></i>
                            </template>
                        </td>
                        <td v-for="field in columns()" :key="field.id">
                            <template v-if="field.slug=='contact'">
                                <select v-if="row.contacts.length" 
                                    class="form-select border-0 w-100 bg-transparent"
                                    :value="contacts[row.id] ? contacts[row.id] : 0"
                                    @change="updateContact(row, $event.target.value)"
                                >
                                <option v-for="(contact, index) in row.contacts" :value="index" :key="contact.id">
                                        @{{ contact.first_name }} @{{ contact.last_name }}
                                    </option>
                                </select>
                            </template>
                            <template v-else-if="field.slug=='email'">
                                @{{ row.contacts.length ? ( contacts[row.id] ? row.contacts[contacts[row.id]].email : row.contacts[0].email ) : '' }}
                            </template>
                            <template v-else-if="field.slug=='phone'">
                                @{{ row.contacts.length ? ( contacts[row.id] ? row.contacts[contacts[row.id]].phone_number : row.contacts[0].phone_number ) : '' }}
                            </template>
                            <template v-else-if="field.slug=='type'">
                                @{{ ['Self', '3PL'][row.type - 1] }}
                            </template>
                            <template v-else-if="field.slug=='country'">
                                @{{ countries[row.country_id] }}
                            </template>
                            <template v-else>
                                @{{ row[field.slug] }}
                            </template>
                        </td>
                        <td>
                            <a :href="'{{route('warehouses.index')}}/' + row.id + '/edit'"
                               class="mr-3 text-secondary row-hover-action"
                               data-toggle="tooltip" data-placement="top" data-original-title="Edit"
                            >
                                <i class="mdi mdi-square-edit-outline font-size-18"></i>
                            </a>
                            <a
                                @click.prevent="bulkDelete(row.id)"
                                class="text-secondary row-hover-action delete_single"
                                data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"
                            >
                                <i class="mdi mdi-trash-can font-size-18"></i>
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>

@endsection
@section('script')
    <script>
        var content = new Vue({
            el: '#content',
            data: {
                perPage: 3,
                currentPage: 1,
                search: '',
                sortField: '',
                sortDirection: '',
                countries: [],
                fields: [
                    { id: 0, caption: 'Type', slug: 'type' },
                    { id: 1, caption: 'Warehouse Name', slug: 'warehouse_name' },
                    { id: 2, caption: 'Country', slug: 'country' },
                    { id: 3, caption: 'Contacts', slug: 'contact' },
                    { id: 4, caption: 'Email', slug: 'email' },
                    { id: 5, caption: 'Phone', slug: 'phone' },
                ],
                rows: [],
                current_page: null,
                selected: [],
                contacts:[],
                cols: @if($cols) {{ $cols }} @else [0,1,2,3,4,5] @endif,
                selectPage: false,
                selectAll: false,
                filter_country: '',
                filter_min_sku: '',
                filter_max_sku: '',
                filter_country_pinned : false,
                filter_sku_pinned : false,
                forceRender: 0,
            },
            created(){
                fetch("{{ route('countries') }}")
                    .then(response => response.json())
                    .then(data => { this.countries = data });

                this.getData()
            },
            updated(){
            },
            methods: {
                columns(){
                    return this.fields.filter((field)=>this.cols.includes(field.id))
                },

                getData(url){
                    fetch("{{ route('get_warehouses') }}", {
                        method: 'post',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            _token: '{{ csrf_token() }}',
                        }),
                    })
                    .then(response => response.json())
                    .then(data => { this.rows = data });
                },

                bulkDelete(id){
                    if(confirm('Really Delete?')){
                        let selected = id ? [id] : this.selected;
                        fetch('{{ route('warehouses_bulk_delete') }}', {
                            method: 'delete', // or 'PUT'
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                _token: '{{ csrf_token() }}',
                                selected : selected
                            }),
                        })
                            .then(response => response.json())
                            .then(data => {
                                this.selected = []
                                this.getData()
                            })
                            .catch((error) => {
                                console.error('Error:', error);
                            });
                    }
                },

                bulkExport(){
                    let selected = this.selected;
                    fetch('{{ route('warehouses_bulk_export') }}', {
                        method: 'get', // or 'PUT'
                        headers: {
                            'Content-Type': 'application/json',
                        },
                    })
                        .catch((error) => {
                            console.error('Error:', error);
                        });
                },

                updatedSelectPage(){
                    if(!this.selectPage) {
                        this.selected = this.processedRows.slice(this.perPage*(this.currentPage-1), this.perPage*this.currentPage).map( i => i.id)
                    }else{
                        this.selected = []
                    }
                    this.selectAll = false
                },

                updatedSelected(){
                    this.selectAll = false
                    this.selectPage = false
                },

                selectAllItems(){
                    this.selectAll = true
                },

                filterData(){
                    console.log('Filter');
                },

                updateCols(){
                    fetch("{{ route('update_warehouses_cols') }}", {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            _token: '{{ csrf_token() }}',
                            cols : this.cols
                        }),
                    })
                },
                nextPage(){
                    if(this.currentPage < this.lastPage) this.currentPage++ ;
                },

                prevPage(){
                    if(this.currentPage > 1 ) this.currentPage-- ;
                },

                updateContact(row, val){
                    this.contacts[row.id] = val
                    this.forceRender++
                },

                sortBy(field){
                    if(this.sortField == field){
                        this.sortDirection = this.sortDirection == 'asc' ? 'desc' : 'asc'
                    }else{
                        this.sortDirection = 'asc'
                    }

                    this.sortField = field
                },
            },

            computed: {
                lastPage(){
                    return Math.floor( this.processedRows.length / this.perPage) + ( this.processedRows.length % this.perPage ? 1 : 0 )
                },

                processedRows: function(){
                    let filtered = this.rows.filter(item => {
                        let props = Object.values(item)
                        
                        return props.some(prop => !this.search || ((typeof prop === 'string') ? prop.includes(this.search) : false))
                        // return props.some(prop => !this.search || ((typeof prop === 'string') ? prop.includes(this.search) : prop.toString(10).includes(this.search)))
                    })
                    .filter(item => {
                        if(this.inComplete)  return !item.completeFlag;
                        return true;
                    })

                    return filtered.sort((a,b) => {
                        let modifier = 1;
                        if(this.sortDirection === 'desc') modifier = -1;
                        if(a[this.sortField] < b[this.sortField]) return -1 * modifier;
                        if(a[this.sortField] > b[this.sortField]) return 1 * modifier;
                        return 0;
                    });
                }
            }
        })
        $(function () {
            setTimeout(function(){
                $('[data-toggle="tooltip"]').tooltip();
            }, 2000);
            document.getElementById("file_upload_input").onchange = function() {
                document.getElementById("form").submit();
            };
            $("#file_upload_input").on('change',function(){
                $("#import_csv_form").submit();
            });
        })
    </script>
@endsection

@extends('layouts.master-final')

@section('title') Create Warehouse @endsection

@section('content')
    @php($country_list=get_country_list())


            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Warehouses</h4>

                            <div class="page-title-right">

                            </div>

                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <div class="card">
                    <div class="card-body">

                        <input type="hidden" id="warehouse_id" value="{{@$warhouse_details->id}}">
                        <input type="hidden" id="marketplace_id" value="{{session('MARKETPLACE_ID')}}">

                        <ul class="nav font-weight-bold tablist w-max-900px" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="basic-details-tab" data-toggle="tab" href="#basic-details" role="tab">
                                    Basic Details
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contacts-tab" data-toggle="tab" href="#contacts" role="tab">
                                    Contacts
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="users-tab" data-toggle="tab" href="#users" role="tab">
                                    Users
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="settings-tab" data-toggle="tab" href="#settings" role="tab">
                                    Settings
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content twitter-bs-wizard-tab-content  w-max-900px">

                            <div class="tab-pane active" id="basic-details" role="tabpanel">
                                <form name="basic_details_form" id="basic_details_form">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group row mb-4">
                                                <label for="" class="col-sm-3 col-form-label">Warehouse Type</label>
                                                <div class="col-sm-9">
                                                    <select name="" id="warehouse_type" class="form-select w-100">
                                                        <option value="1" {{@$warhouse_details->type == 1 ? 'selected' : ''}}>Self</option>
                                                        <option value="2" {{@$warhouse_details->type == 2 ? 'selected' : ''}}>3PL</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-4">
                                                <label for="" class="col-sm-3 col-form-label">Warehouse Name <span class="text-danger">*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="Warehouse_name" id="Warehouse_name" value="{{@$warhouse_details->warehouse_name}}" placeholder="Enter Warehouse Name">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-4">
                                                <label for="" class="col-sm-3 col-form-label">Country <span class="text-danger">*</span></label>
                                                <div class="col-sm-9">
                                                    <select name="countryId" id="countryId" class="form-select countries w-100">
                                                        <option value="">Select Country</option>
                                                        @foreach($country_list as  $key=> $country)
                                                            <option value=" {{ $key }}">{{ $key."(".$country.")" }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-4">
                                                <label for="" class="col-sm-3 col-form-label">Address <span class="text-danger">*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="address_line1" id="address_line1" placeholder="Enter Address Line One">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-4">
                                                <label for="" class="col-sm-3 col-form-label"></label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="address_line2" placeholder="Enter Address Line Two">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-4">
                                                <label for="" class="col-sm-3 col-form-label">State <span class="text-danger">*</span></label>
                                                <div class="col-sm-9">
                                                    <select name="stateId" id="stateId" class="form-select w-100 states">
                                                        <option value="">Select State</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="" class="col-sm-3 col-form-label">Postal Code <span class="text-danger">*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="postal_code" id="postal_code" placeholder="Enter Postal Code" maxlength="8">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-5">
                                        <div class="col-md-8">
                                            <div class="row justify-content-between">
                                                <div class="col-4">
                                                    <a href="{{ url('/warehouses') }}" class="btn btn-outline-secondary">Cancel</a>
                                                </div>
                                                <div class="col-8 text-right">
                                                    <button class="btn btn-outline-primary w-md save save_datas" data-edit="0" data-val="1">Save</button>
                                                    <button class="btn btn-primary w-md save-next save_datas" data-edit="0" data-val="2">Save And Next</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                                <form class="d-none" id="basic_details_form_edit">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group row mb-4">
                                                <label for="" class="col-sm-3 col-form-label">Warehouse Type</label>
                                                <div class="col-sm-9">
                                                    <label for="" class="col-form-label val-label">
                                                        <span class="edit_warehouse_type">Domestic</span>
                                                        <button class="btn text-primary p-0 ml-2 invisible">
                                                            <i class="mdi mdi-square-edit-outline"></i>
                                                        </button>
                                                    </label>
                                                </div>
                                                <div class="col-sm-9 d-none">
                                                    <select name="warehouse_type_edit" id="warehouse_type_edit" class="form-select w-100">
                                                        <option value="1">Self</option>
                                                        <option value="2">3PL</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-4">
                                                <label for="" class="col-sm-3 col-form-label">Warehouse Name</label>
                                                <div class="col-sm-9">
                                                    <label for="" class="col-form-label val-label">
                                                        <span class="edit_warehouse_name"></span>
                                                        <button class="btn text-primary p-0 ml-2 invisible">
                                                            <i class="mdi mdi-square-edit-outline"></i>
                                                        </button>
                                                    </label>
                                                </div>
                                                <div class="col-sm-9 d-none">
                                                    <input type="text" class="form-control" id="Warehouse_name_edit" name="Warehouse_name_edit" value="" placeholder="Enter Warehouse Name">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-4">
                                                <label for="" class="col-sm-3 col-form-label">Country</label>
                                                <div class="col-sm-9">
                                                    <label for="" class="col-form-label val-label">
                                                        <span class="edit_countryId"></span>
                                                        <button class="btn text-primary p-0 ml-2 invisible">
                                                            <i class="mdi mdi-square-edit-outline"></i>
                                                        </button>
                                                    </label>
                                                </div>
                                                <div class="col-sm-9 d-none">
                                                    <select name="countryId_edit" id="countryId_edit" class="form-select w-100 countries">
                                                        <option value="">Select Country</option>
                                                        @foreach($country_list as  $key=> $country)
                                                            <option value=" {{ $key }}">{{ $key."(".$country.")" }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-4">
                                                <label for="" class="col-sm-3 col-form-label">Address</label>
                                                <div class="col-sm-9">
                                                    <label for="" class="col-form-label val-label mb-4">
                                                        <span class="edit_address_line1"></span>
                                                        <button class="btn text-primary p-0 ml-2 invisible">
                                                            <i class="mdi mdi-square-edit-outline"></i>
                                                        </button>
                                                    </label><br>
                                                    <label for="" class="col-form-label val-label">
                                                        <span class="edit_address_line2"></span>
                                                        <!-- <button class="btn text-primary p-0 ml-2 invisible">
                                                            <i class="mdi mdi-square-edit-outline"></i>
                                                        </button> -->
                                                    </label>
                                                </div>
                                                <div class="col-sm-9 d-none">
                                                    <input type="text" class="form-control mb-4" name="address_line1_edit" id="address_line1_edit" value="" placeholder="Enter Address Line One">
                                                    <input type="text" class="form-control" id="address_line2_edit" value="" placeholder="Enter Address Line Two">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-4">
                                                <label for="" class="col-sm-3 col-form-label">State</label>
                                                <div class="col-sm-9">
                                                    <label for="" class="col-form-label val-label">
                                                        <span class="edit_stateId"></span>
                                                        <button class="btn text-primary p-0 ml-2 invisible">
                                                            <i class="mdi mdi-square-edit-outline"></i>
                                                        </button>
                                                    </label>
                                                </div>
                                                <div class="col-sm-9 d-none">
                                                    <select name="stateId_edit" id="stateId_edit" class="form-select w-100 states">
                                                        <option value="">Select State</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-5">
                                                <label for="" class="col-sm-3 col-form-label">Postal Code</label>
                                                <div class="col-sm-9">
                                                    <label for="" class="col-form-label val-label">
                                                        <span class="edit_postal_code"></span>
                                                        <button class="btn text-primary p-0 ml-2 invisible">
                                                            <i class="mdi mdi-square-edit-outline"></i>
                                                        </button>
                                                    </label>
                                                </div>
                                                <div class="col-sm-9 d-none">
                                                    <input type="text" class="form-control" name="postal_code_edit" id="postal_code_edit"  maxlength="8" value="" placeholder="Enter Postal Code">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-5">
                                        <div class="col-md-8">
                                            <div class="row justify-content-between">
                                                <div class="col-4">
                                                    <a href="{{ url('/warehouses') }}" class="btn btn-outline-secondary">Cancel</a>
                                                </div>
                                                <div class="col-8 text-right">
                                                    <button class="btn btn-outline-primary w-md save save_datas" data-edit="1" data-val="1">Save</button>
                                                    <button class="btn btn-primary w-md save-next save_datas" data-edit="1" data-val="2">Save And Next</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>

                            <div class="tab-pane" id="contacts" role="tabpanel">
                            <span class="basic_details_msg"></span>
                                <form>
                                    <table
                                            id="list"
                                            class="table dataTable border-bottom dt-responsive nowrap mb-5">
                                        <thead class="">
                                        <tr>
                                            <th style="display: none;"></th>
                                            <th class="sorting col1">First Name</th>
                                            <th class="sorting col2">Last Name</th>
                                            <th class="sorting col3">Title</th>
                                            <th class="sorting col4">Email</th>
                                            <th class="sorting col5">Phone Number</th>
                                            <td class=""></td>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <tr class="no-list add-row-tr">
                                            <td colspan="7" class="text-center">
                                                Click to add first contact
                                                <span class="text-primary1 add-row ml-3">
                                                <i class="bx bx-plus-circle font-size-16 align-middle"></i>
                                                Add Contacts
                                            </span>
                                            </td>
                                        </tr>

                                        </tbody>

                                    </table>

                                    <div class="row mt-5">
                                        <div class="col-md-8">
                                            <div class="row justify-content-between">
                                                <div class="col-4">
                                                    <a href="{{ url('/warehouses') }}" class="btn btn-outline-secondary">Cancel</a>
                                                </div>
                                                <div class="col-8 text-right">
                                                    <button class="btn btn-outline-primary w-md save save_contacts" data-val="1">Save</button>
                                                    <button class="btn btn-primary w-md save-next save_contacts"  data-val="2">Save And Next</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>

                            <div class="tab-pane" id="users">
                            <span class="basic_details_msg"></span>
                                <form class="" id="data-users-form">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group row">
                                                <label for="horizontal-firstname-input" class="col-sm-4 col-form-label">Warehouse Manager(s)</label>

                                                <div class="col-sm-8 pr-0" >
                                                    <select name="user_manager" id="user_manager" class="form-control select2 select2-multiple" multiple="multiple">
                                                        @if(!empty($warehouse_users))
                                                        @foreach($warehouse_users as $data)
                                                                <option value="{{$data['id']}}">{{$data['full_name']}}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-5">
                                        <div class="col-md-8">
                                            <div class="row justify-content-between">
                                                <div class="col-4">
                                                    <a href="{{ url('/warehouses') }}" class="btn btn-outline-secondary">Cancel</a>
                                                </div>
                                                <div class="col-8 text-right">
                                                    <button class="btn btn-outline-primary w-md save save_users" data-edit="0" data-val="1">Save</button>
                                                    <button class="btn btn-primary w-md save-next save_users" data-edit="0"  data-val="2">Save And Next</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                                <form class="d-none" id="data-users-form-edit">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group row">
                                                <label for="horizontal-firstname-input" class="col-sm-4 col-form-label">Warehouse Manager(s)</label>
                                                <div class="col-sm-8">
                                                    <label for="" class="col-form-label val-label">
                                                        <span class="warehouse_manager_user_label"></span>
                                                        <button class="btn text-primary p-0 ml-2 invisible">
                                                            <i class="mdi mdi-square-edit-outline"></i>
                                                        </button>
                                                    </label>
                                                </div>
                                                <div class="col-sm-8 pr-0 d-none">
                                                    <select name="user_manager_edit" id="user_manager_edit" class="form-control select2 select2-multiple" multiple="multiple">
                                                        @if(!empty($warehouse_users))
                                                            @foreach($warehouse_users as $data)
                                                                <option value="{{$data['id']}}">{{$data['full_name']}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-5">
                                        <div class="col-md-8">
                                            <div class="row justify-content-between">
                                                <div class="col-4">
                                                    <a href="{{ url('/warehouses') }}" class="btn btn-outline-secondary">Cancel</a>
                                                </div>
                                                <div class="col-8 text-right">
                                                <button class="btn btn-outline-primary w-md save save_users" data-edit="1" data-val="1">Save</button>
                                                    <button class="btn btn-primary w-md save-next save_users" data-edit="1"  data-val="2">Save And Next</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>

                            <div class="tab-pane" id="settings">
                            <span class="basic_details_msg"></span>
                                <form class="" id="data-setting-form">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group row">
                                                <label for="horizontal-firstname-input" class="col-sm-4 col-form-label">Lead Time</label>
                                                <div class="col-sm-8 pr-0">
                                                    <input type="text" class="form-control" name="lead_time" id="lead_time" placeholder="Enter Days" maxlength="4">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-5">
                                        <div class="col-md-8">
                                            <div class="row justify-content-between">
                                                <div class="col-4">
                                                    <a href="{{ url('/warehouses') }}" class="btn btn-outline-secondary">Cancel</a>
                                                </div>
                                                <div class="col-8 text-right">
                                                    <button class="btn btn-outline-primary w-md save data_settings" data-edit="0" data-val="1">Save</button>
                                                    <button class="btn btn-primary w-md save-next data_settings" data-edit="0" data-val="2">Save And Next</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                                <form class="d-none" id="data-setting-form-edit">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group row">
                                                <label for="horizontal-firstname-input" class="col-sm-4 col-form-label">Lead Time</label>
                                                <div class="col-sm-8">
                                                    <label for="" class="col-form-label val-label">
                                                        <span id="lead_time_edit_label"></span> Days
                                                        <button class="btn text-primary p-0 ml-2 invisible">
                                                            <i class="mdi mdi-square-edit-outline"></i>
                                                        </button>
                                                    </label>
                                                </div>
                                                <div class="col-sm-8 pr-0 d-none">
                                                    <input type="text" class="form-control" name="lead_time_edit" id="lead_time_edit" placeholder="Enter Days" val="" maxlength="4">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-5">
                                        <div class="col-md-8">
                                            <div class="row justify-content-between">
                                                <div class="col-4">
                                                    <a href="{{ url('/warehouses') }}" class="btn btn-outline-secondary">Cancel</a>
                                                </div>
                                                <div class="col-8 text-right">
                                                    <button class="btn btn-outline-primary w-md save data_settings" data-edit="1" data-val="1">Save</button>
                                                    <button class="btn btn-primary w-md save-next data_settings" data-edit="1" data-val="2">Save And Next</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>

                        </div>

                    </div>

                </div>

            </div> <!-- container-fluid -->
@endsection
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="{{ URL::asset('assets/js/common.js')}}"></script>
    <script src="{{ URL::asset('js/app.js')}}"></script>
    <script src="{{ URL::asset('assetsnew/js/pages/warehouse.js')}}"></script>
    <script src="https://rawgithub.com/padolsey/jQuery-Plugins/master/sortElements/jquery.sortElements.js"></script>

    <script>
        $(document).ready(function () {
            sort_table();
        });
        function sort_table(){ 
            var table = $('#list');
            $('.col1, .col2,.col3, .col4, .col5, .col6')
                .wrapInner('<span title="sort this column"/>')
                .each(function(){

                var th = $(this),
                    thIndex = th.index(),
                    inverse = false;

                th.on('click',function(){

                    table.find('td').filter(function(){

                        return $(this).index() === thIndex;

                    }).sortElements(function(a, b){
                        // var aText = a.innerHTML;
                        // var bText = b.innerHTML;
                        console.log($(a).find('.col1').text());
                        return $.text([a]) > $.text([b]) ?
                            inverse ? -1 : 1
                            : inverse ? 1 : -1;

                    }, function(){

                        // parentNode is the element we want to move
                        return this.parentNode;

                    });

                    inverse = !inverse;

                });

            });
        }
        var warehouse_id = $("#warehouse_id").val();
        if(warehouse_id == ''){
            $(".basic_details_msg").html('<div class="alert alert-danger">please fill up basic details<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');
        }else{
            $(".basic_details_msg").html('');
        }
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) ||/^[A-Za-z0-9\-\_]+$/.test(value);
        }, "Letters only please");

        $('#basic_details_form').validate({ // initialize the plugin
            rules: {
                Warehouse_name: {required: true},
                countryId: {required: true},
                address_line1: {required: true},
                stateId: {required: true},
                postal_code: {required: true,lettersonly:true},
            },
            messages: {
                Warehouse_name: { required: "Enter warehouse name"},
                countryId: { required: "Select country"},
                address_line1: { required: "Enter address"},
                stateId: { required: "Enter state"},
                postal_code: { required: "Enter postal code",lettersonly:"Only Alphabets, Numbers & hyphen allowed. "}
            }
        });

        $('#basic_details_form_edit').validate({ // initialize the plugin
            rules: {
                Warehouse_name_edit: {required: true},
                countryId_edit: {required: true},
                address_line1_edit: {required: true},
                stateId_edit: {required: true},
                postal_code_edit: {required: true,lettersonly:true},
            },
            messages: {
                Warehouse_name_edit: { required: "Enter warehouse name"},
                countryId_edit: { required: "Select country"},
                address_line1_edit: { required: "Enter address"},
                stateId_edit: { required: "Enter state"},
                postal_code_edit: { required: "Enter postal code",lettersonly:"Only Alphabets, Numbers & hyphen allowed. "}
            }
        });

        $('#data-setting-form').validate({ // initialize the plugin
            rules: {
                lead_time: {required: true, digits:true}
            },
            messages: {
                lead_time: { required: "Enter lead time days", digits:"Please enter only numbers"}
            }
        });

        $('#data-setting-form-edit').validate({ // initialize the plugin
            rules: {
                lead_time_edit: {required: true, digits:true}
            },
            messages: {
                lead_time_edit: { required: "Enter lead time days", digits:"Please enter only numbers"}
            }
        });

        $('#data-users-form').validate({ // initialize the plugin
            rules: {
                user_manager: {required: true}
            },
            messages: {
                user_manager: { required: "Please select user"}
            }
        });

        $('#data-users-form-edit').validate({ // initialize the plugin
            rules: {
                user_manager_edit: {required: true}
            },
            messages: {
                user_manager_edit: { required: "Please select user"}
            }
        });


        $(document).on('click','.save_datas',function(e){
            e.preventDefault();
            var types = $(this).data('val');
            var edit = $(this).data('edit');
            if($('#basic_details_form').valid()==false && edit == '0') {
                return false;
            }
            if($('#basic_details_form_edit').valid()==false && edit == '1') {
                return false;
            }

            if(edit == 0){
                var warehouse_type = $("#warehouse_type").val();
                var Warehouse_name = $("#Warehouse_name").val();
                var countryId = $("#countryId").val();
                var address_line1 = $("#address_line1").val();
                var address_line2 = $("#address_line2").val();
                var stateId = $("#stateId").val();
                var postal_code = $("#postal_code").val();
                var countrytext = $('#countryId option:selected').text();
                var statetext = $('#stateId option:selected').text();
            }else{
                var warehouse_type = $("#warehouse_type_edit").val();
                var Warehouse_name = $("#Warehouse_name_edit").val();
                var countryId = $("#countryId_edit").val();
                var address_line1 = $("#address_line1_edit").val();
                var address_line2 = $("#address_line2_edit").val();
                var stateId = $("#stateId_edit").val();
                var postal_code = $("#postal_code_edit").val();
                var countrytext = $('#countryId_edit option:selected').text();
                var statetext = $('#stateId_edit option:selected').text();
            }

            //alert(stateId);

            var basic_details = $('#basic-details')
            basic_details.find('form:nth-child(1)').addClass('d-none');
            basic_details.find('form:nth-child(2)').removeClass('d-none');
            basic_details.find('.form-group').find('.col-sm-9:nth-child(3)').addClass('d-none');
            basic_details.find('.form-group').find('.col-sm-9:nth-child(2)').removeClass('d-none');

            var countryId = $("#countryId").val();
            var marketplace_id = $("#marketplace_id").val();
            var warehouse_id = $("#warehouse_id").val();
           // var warehouse_id = '1';F
            var insert_type = 'insert_update_basic_details';
            $.ajax({
                type:"POST",
                url:"{{route('warehouses.store')}}",
                data:{types:types,
                    warehouse_type:warehouse_type,
                    Warehouse_name:Warehouse_name,
                    countryId:countryId,
                    address_line1:address_line1,
                    address_line2:address_line2,
                    stateId:stateId,
                    postal_code:postal_code,
                    marketplace_id:marketplace_id,
                    warehouse_id:warehouse_id,
                    insert_type:insert_type,
                    _token:$('meta[name="csrf-token"]').attr('content'),
                    },
                success:function(res){
                    if(res.error == 0){
                        showAlert(true, res.message)
                        // $("#messages").html(res.message);
                        $("#warehouse_id").val(res.warehouse_id);
                        $(".basic_details_msg").html('');
                        if(warehouse_type == 1) var warehouse_type_name = 'Self';
                        else var warehouse_type_name = '3PL';
                        $(".edit_warehouse_type").text(warehouse_type_name);
                        $(".edit_warehouse_name").text(Warehouse_name);
                        $(".edit_countryId").text(countrytext);
                        $(".edit_address_line1").text(address_line1);
                        $(".edit_address_line2").text(address_line2);
                        $(".edit_stateId").text(statetext);
                        $(".edit_postal_code").text(postal_code);

                        $('#warehouse_type_edit').val(warehouse_type);
                        $('#Warehouse_name_edit').val(Warehouse_name);
                        $('#countryId_edit').val(countryId);
                        $('#address_line1_edit').val(address_line1);
                        $('#address_line2_edit').val(address_line2);
                        $('#stateId_edit').val(stateId);
                        $('#postal_code_edit').val(postal_code);
                        if(types == 1){
                        }else{
                            $(".tab-pane").removeClass('show active');
                            $(".nav-link").removeClass('active');
                            if(warehouse_type == 1){
                                $("#users-tab").addClass('active');
                                $("#users").addClass('show active');
                            }else{
                                $("#contacts-tab").addClass('active');
                                $("#contacts").addClass('show active');
                            }
                        }
                    }else{
                        showAlert(false, res.message)
                        // $("#messages").html(res.message);
                    }
                }
            });

        });

        $(document).on('click','.save_users',function(e){
            e.preventDefault();
            var types = $(this).data('val');
            var edit = $(this).data('edit');

            if($('#data-users-form').valid()==false && edit == '0') {
                return false;
            }
            if($('#data-users-form-edit').valid()==false && edit == '1') {
                return false;
            }

            if(edit == 0){
                var user_manager = $("#user_manager").val();
                var user_manager_text = $('#user_manager option:selected').text();

                var option_all = $("#user_manager option:selected").map(function () {
                    return $(this).text();
                }).get().join(',');
                
            }else{
                var user_manager = $("#user_manager_edit").val();
                var user_manager_text = $('#user_manager_edit option:selected').text();

                var option_all = $("#user_manager_edit option:selected").map(function () {
                    return $(this).text();
                }).get().join(',');

            }
           
            //console.log(option_all);
            //console.log('1'+user_manager_text);

            if(types === 1) {
                if (edit === 0) {
                    $('#data-users-form').addClass('d-none');
                    $('#data-users-form-edit').removeClass('d-none');
                    $('#data-users-form').find('.form-group').find('.col-sm-8:nth-child(3)').addClass('d-none');
                    $('#data-users-form').find('.form-group').find('.col-sm-8:nth-child(2)').removeClass('d-none');
                } else{
                    $('#data-users-form').addClass('d-none');
                    $('#data-users-form-edit').removeClass('d-none');
                    $('#data-users-form-edit').find('.form-group').find('.col-sm-8:nth-child(3)').addClass('d-none');
                    $('#data-users-form-edit').find('.form-group').find('.col-sm-8:nth-child(2)').removeClass('d-none');
                }
            }
            var marketplace_id = $("#marketplace_id").val();
            var warehouse_id = $("#warehouse_id").val();
            //var warehouse_id = '1';
            if(warehouse_id == ''){
                $(".tab-pane").removeClass('show active');
                $(".nav-link").removeClass('active');
                $("#basic-details-tab").addClass('active');
                $("#basic-details").addClass('show active');
            }else{
                var insert_type = 'insert_update_users';
                $.ajax({
                    type:"POST",
                    url:"{{route('warehouses.store')}}",
                    data:{types:types,
                        user_manager:user_manager,
                        marketplace_id:marketplace_id,
                        warehouse_id:warehouse_id,
                        insert_type:insert_type,
                        _token:$('meta[name="csrf-token"]').attr('content'),
                    },
                    success:function(res){
                        if(res.error == 0){
                            // $("#messages").html(res.message);
                            showAlert(true, res.message)
                            //                            $("#warehouse_id").val(res.warehouse_id);
                            $(".warehouse_manager_user_label").text(option_all);
                            if(types == 1){
                            }else{
                                $(".tab-pane").removeClass('show active');
                                $(".nav-link").removeClass('active');
                                $("#settings-tab").addClass('active');
                                $("#settings").addClass('show active');
                            }
                        }else{
                            showAlert(false, res.message)
                            // $("#messages").html(res.message);
                        }
                    }
                });
            }


        });

        $(document).on("click",".save_contacts",function(){
            var types = $(this).data('val');
            var first_names = [];
            var last_names = [];
            var titles = [];
            var emailss = [];
            var phone_numbers = [];
            var primarys = [];
            var warehouse_contact_ids = [];

            $("table > tbody > tr ").each(function( index, element ) {
                //datass[index] = [];
                var primary = $(this).find(".primary_contact").val();
                var first_name = $(this).find(".first_name").val();
                var last_name = $(this).find(".last_name").val();
                var title = $(this).find(".title").val();
                var emails = $(this).find(".emails").val();
                var phone_number = $(this).find(".phone_number").val();
                //var warehouse_contact_id = $(this).$(".warehouse_contact_id").val();
                //var primary = $(this).find(".primary").val();

                if($.trim(first_name) != '') {
                    var isValid = false;
                    var regex = /^[a-zA-Z ]*$/;
                    isValid = regex.test($.trim(first_name));
                    if (isValid == false) {
                        swal('Enter valid first name');
                        return isValid;
                    }
                }if($.trim(last_name) != ''){
                    var isValid = false;
                    var regex = /^[a-zA-Z ]*$/;
                    isValid = regex.test($.trim(last_name));
                    if(isValid == false){
                        swal('Enter valid last name');
                        return isValid;
                    }
                }if($.trim(title) != ''){
                    var isValid = false;
                    var regex = /^[a-zA-Z ]*$/;
                    isValid = regex.test($.trim(title));
                    if(isValid == false){
                        swal('Enter valid title');
                        return isValid;
                    }
                }if($.trim(emails) != ''){
                    var isValid = false;
                    var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    isValid = regex.test($.trim(emails));
                    if(isValid == false){
                        swal('Enter valid email');
                        return isValid;
                    }
                }if($.trim(phone_number) != ''){
                    var isValid = false;
                    //var regex = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
                    var reg = /^[+-]?\d+$/;
                    isValid = reg.test($.trim(phone_number));
                    if(isValid == false){
                        swal('Enter valid phone');
                        return isValid;
                    }
                }

                primarys. push($.trim(primary));
                first_names. push($.trim(first_name));
                last_names. push($.trim(last_name));
                titles. push($.trim(title));
                emailss. push($.trim(emails));
                phone_numbers. push($.trim(phone_number));
                //warehouse_contact_ids. push($.trim(warehouse_contact_id));
                //primary.push($.trim(primary));
            });

            // if(first_names == '' && last_names == '' && titles == '' && emailss == '' && phone_numbers == ''){
            //     swal('Please create at least one contact');
            //     return false;
            // }

            var warehouse_id = $("#warehouse_id").val();
            //var warehouse_id = '1';
            var insert_type = 'insert_contacts';
            if(warehouse_id == ''){
                return false;
                $(".tab-pane").removeClass('show active');
                $(".nav-link").removeClass('active');
                $("#basic-details-tab").addClass('active');
                $("#basic-details").addClass('show active');
            }else {

                $.ajax({
                    type: "POST",
                    url: "{{route('warehouses.store')}}",
                    data: {
                        warehouse_id: warehouse_id,
                        first_names: first_names,
                        last_names: last_names,
                        emails: emailss,
                        titles:titles,
                        phone_numbers:phone_numbers,
                        insert_type: insert_type,
                        //warehouse_contact_id: warehouse_contact_ids,
                        primary : primarys,
                        _token:$('meta[name="csrf-token"]').attr('content'),
                    },
                    success: function (res) {
                        if (res.error == 0) {
                            if(res.message != ''){
                                showAlert(true, res.message)
                            }
                            // $("#messages").html(res.message);
                            //$(".vendors_contact_id").val(res.vendors_contact_id);
                            if(types == 2) {
                                $(".tab-pane").removeClass('show active');
                                $(".nav-link").removeClass('active');
                                $("#settings-tab").addClass('active');
                                $("#settings").addClass('show active');
                            }
                        } else {
                            if(res.message != ''){
                                showAlert(false, res.message)
                            }
                            // $("#messages").html(res.message);
                        }
                    }
                });
            }
        });

        $(document).on('click','.data_settings',function(e){
            e.preventDefault();
            var types = $(this).data('val');
            var edit = $(this).data('edit');

            if($('#data-setting-form').valid()==false && edit == '0') {
                return false;
            }
            if($('#data-setting-form-edit').valid()==false && edit == '1') {
                return false;
            }


            var warehouse_id = $("#warehouse_id").val();
            var lead_time = '';
            var type = $("#type").val();
            if(edit == 0) {
                lead_time = $("#lead_time").val();
            }else{
                lead_time = $("#lead_time_edit").val();
            }

            // if(types === 1){
            //     $("#data-setting-form-edit").find('.col-sm-8:nth-child(3)').addClass('d-none');
            //     $("#data-setting-form-edit").find('.col-sm-8:nth-child(2)').removeClass('d-none');
            // }
            // var settings = $('#settings');
            // settings.find('form:nth-child(1)').addClass('d-none');
            // settings.find('form:nth-child(2)').removeClass('d-none')
            // settings.find('.form-group').find('.col-sm-8:nth-child(3)').addClass('d-none');
            // settings.find('.form-group').find('.col-sm-8:nth-child(2)').removeClass('d-none');

            if(types === 1) {
                if (edit === 0) {
                    $('#data-setting-form').addClass('d-none');
                    $('#data-setting-form-edit').removeClass('d-none');
                    $('#data-setting-form').find('.form-group').find('.col-sm-8:nth-child(3)').addClass('d-none');
                    $('#data-setting-form').find('.form-group').find('.col-sm-8:nth-child(2)').removeClass('d-none');
                } else{
                    $('#data-setting-form').addClass('d-none');
                    $('#data-setting-form-edit').removeClass('d-none');
                    $('#data-setting-form-edit').find('.form-group').find('.col-sm-8:nth-child(3)').addClass('d-none');
                    $('#data-setting-form-edit').find('.form-group').find('.col-sm-8:nth-child(2)').removeClass('d-none');
                }
            }
            // var settings = $('#settings');
            // settings.find('form:nth-child(1)').addClass('d-none');
            // settings.find('form:nth-child(2)').removeClass('d-none')
            // settings.find('.form-group').find('.col-sm-8:nth-child(3)').addClass('d-none');
            // settings.find('.form-group').find('.col-sm-8:nth-child(2)').removeClass('d-none');

            console.log(types);
            console.log(edit);
            console.log(lead_time);
            console.log(warehouse_id);
            var insert_type = 'insert_update_setting';
            if(warehouse_id == ''){
                $(".tab-pane").removeClass('show active');
                $(".nav-link").removeClass('active');
                $("#basic-details-tab").addClass('active');
                $("#basic-details").addClass('show active');
            }else{
                $.ajax({
                    type:"POST",
                    url:"{{route('warehouses.store')}}",
                    data:{warehouse_id:warehouse_id,type:type,insert_type:insert_type,lead_time:lead_time,_token:$('meta[name="csrf-token"]').attr('content')},
                    success:function(res){
                        if(res.error == 0){
                            showAlert(true, res.message)
                            // $("#messages").html(res.message);
                            //                            $("#warehouse_id").val(res.warehouse_id);
                            $('#lead_time_edit').val(lead_time);
                            $('#lead_time_edit_label').text(lead_time);
                            if(types == 2){
                                window.location.href = "{{route('warehouses.index')}}"
                            }else{

                            }
                        }else{
                            showAlert(false, res.message)
                            // $("#messages").html(res.message);
                        }
                    }
                });
            }
        });

        $(document).on('click','.delete-btn-vendors-tab',function(){
            var insert_type = 'delete_vendors_contact';
            var rand_text = makeid(5);
            var vendors_contact_id = $(this).find(".vendors_contact_id").val();
            $(this).closest('tr').addClass(rand_text);
            //var vendors_contact_id = $('.'+rand_text).find(".vendors_contact_id").val();
            //$(this).closest('tr').attr('data-random',vendors_contact_id);

            {{--if(vendors_contact_id != ''){--}}
                {{--$.ajax({--}}
                    {{--type:"POST",--}}
                    {{--url:"{{route('vendors.store')}}",--}}
                    {{--data:{vendors_contact_id:vendors_contact_id,insert_type:insert_type },--}}
                    {{--success:function(res){--}}
                        {{--$("."+rand_text).remove();--}}
                    {{--}--}}
                {{--});--}}
            {{--}else{--}}
                $("."+rand_text).remove();
            // }
        });

        function makeid(length) {
            var result           = '';
            var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;
            for ( var i = 0; i < length; i++ ) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            return result;
        }

        $(document).ready(function(){
            var type = $("#type").val();
            if(type == 1){
                $('#nav-profile-tab').hide();
            }else{
                $('#nav-profile-tab').show();
            }
        });
        //        $(document).on('change','#type',function(){
        //            var type = $(this).val();
        //            if(type == 1){
        //                $('#nav-profile-tab').hide();
        //            }else{
        //                $('#nav-profile-tab').show();
        //            }
        //        });

        //    $(document).ready(function(){
        //        $("select").change(function(){
        //            check();
        //        }).change();
        //        check();
        //    });
        //
        function check() {
            var data= $('#type').val();
            if(data=="1")  {
                $(".primary_first_name").hide();
                $(".primary_last_name").hide();
                $(".primary_email").hide();
                $('.primary_first_name').rules('remove',  {
                    required : true,
                    messages : { required : 'Firstname is required' }
                });
                $('.primary_last_name').rules('remove',  {
                    required : true,
                    messages : { required : 'Lastname is required' }
                });
                $('.primary_email').rules('remove',  {
                    required : true,
                    messages : { required : 'Email is required' }
                });

            }
            else
            {

                $(".primary_first_name").show();
                $(".primary_last_name").show();
                $(".primary_email").show();

                $('.primary_first_name').rules('add',  {
                    required : true,
                    messages : { required : 'Firstname number is required' }
                });
                $('.primary_last_name').rules('add',  {
                    required : true,
                    messages : { required : 'Lastname is required' }
                });
                $('.primary_email').rules('add',  {
                    required : true,
                    messages : { required : 'Email is required' }
                });
            }
        }
        function showAlert(type, msg){
            var cardBody = $('.card-body');
            var errMsg = $('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error! </strong> <span class="msg"></span> </div>');
            var successMsg = $('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success! </strong> <span class="msg"></span> </div>');
            $(".alert").remove();
            debugger;
            var alrt = type ? successMsg : errMsg ;
            alrt.find('.msg').text(msg)
            cardBody.prepend(alrt)
            setTimeout(() => {
                $('.alert').remove()
            }, 10000);
        }
        
        //alert('sdfhbsdhf    ');

        $('#countryId').change(function(){
            //alert('sdsjhf');
            var country = $(this).val();
            var request_type = 'Get_all_state';
            if(country){
                $.ajax({
                    type:"POST",
                    url:"{{route('warehouses.store')}}",
                    data:{country:country,request_type:request_type,_token:$('meta[name="csrf-token"]').attr('content')},
                    success:function(res){
                        if(res){
                            $("#stateId").empty();
                            $("#stateId_edit").empty();
                            $("#stateId").append('<option value="">Select</option>');
                            $("#stateId_edit").append('<option value="">Select</option>');
                            $.each(res,function(key,value){
                                $("#stateId").append('<option value="'+key+'">'+value+ '</option>');
                                $("#stateId_edit").append('<option value="'+key+'">'+value+ '</option>');
                            });

                        }else{
                            $("#stateId").empty();
                        }
                    }
                });
            }else{
                $("#stateId").empty();
                $("#city").empty();
            }
        });
        $('#countryId_edit').change(function(){
            var country = $(this).val();
            var request_type = 'Get_all_state';
            if(country){
                $.ajax({
                    type:"POST",
                    url:"{{route('warehouses.store')}}",
                    data:{country:country,request_type:request_type,_token:$('meta[name="csrf-token"]').attr('content')},
                    success:function(res){
                        if(res){
                            $("#stateId").empty();
                            $("#stateId_edit").empty();
                            $("#stateId").append('<option value="">Select</option>');
                            $("#stateId_edit").append('<option value="">Select</option>');
                            $.each(res,function(key,value){
                                $("#stateId").append('<option value="'+key+'">'+value+ '</option>');
                                $("#stateId_edit").append('<option value="'+key+'">'+value+ '</option>');
                            });

                        }else{
                            $("#stateId").empty();
                        }
                    }
                });
            }else{
                $("#stateId").empty();
                $("#city").empty();
            }
        });
        {{--$('#state').on('change',function(){--}}
            {{--var state = $(this).val();--}}
            {{--var request_type = 'Get_all_city';--}}
            {{--if(state){--}}
                {{--$.ajax({--}}
                    {{--type:"POST",--}}
                    {{--url:"{{ route('warehouses.store') }}",--}}
                    {{--data:{state:state,request_type:request_type,_token:$('meta[name="csrf-token"]').attr('content')},--}}
                    {{--success:function(res){--}}
                        {{--if(res){--}}
                            {{--console.log(res);--}}
                            {{--$("#city").empty();--}}
                            {{--$("#city").append('<option>Select</option>');--}}
                            {{--$.each(res,function(key,value){--}}
                                {{--$("#city").append('<option value="'+key+'">'+key+'('+value+')'+'</option>');--}}
                            {{--});--}}

                        {{--}else{--}}
                            {{--$("#city").empty();--}}
                        {{--}--}}
                    {{--}--}}
                {{--});--}}
            {{--}else{--}}
                {{--$("#city").empty();--}}
            {{--}--}}

        {{--});--}}
        
    </script>

@endsection

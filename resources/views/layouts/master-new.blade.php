<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title> @yield('title') | {{ config('app.name') }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ URL::asset('asset/images/favicon.ico')}}">
        @include('layouts.head-new')
        @yield('customcss')
    </head>

    @section('body')
    @show
    <body data-sidebar="dark">
        <!-- Begin page -->
        <div id="layout-wrapper">
        @include('layouts.top-hor-new')
        @include('layouts.sidebar-new')
        <!-- Start right Content here -->
        <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                @yield('content')
            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->
        @include('layouts.footer-new')
        </div>
            <!-- end main content-->
    </div>
    <!-- END layout-wrapper -->

    <!-- Right Sidebar -->
    @include('layouts.right-sidebar-new')
    <!-- /Right-bar -->

    <!-- Right bar overlay-->
    <div class="rightbar-overlay"></div>

    <!-- JAVASCRIPT -->
    @include('layouts.footer-script-new')
    </body>
</html>

<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title> @yield('title') | {{ config('app.name') }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ URL::asset('asset/images/favicon.ico')}}">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        @include('layouts.head-final')
        @yield('customcss')
        <style>
            #toggle-columns .custom-control-label:hover{
                color: #227CFF;
            }
        </style>

        <!-- Scripts -->
        <script src="{{ asset('js/lib/vue.min.js') }}"></script>
        <script src="{{ asset('js/lib/alpine.min.js') }}"></script>
        <script src="{{ URL::asset('assetsnew/libs/jquery/jquery.min.js')}}"></script>
        <script src="{{ URL::asset('assetsnew/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{ asset('js/lib/jquery.validate.js')}}"></script>
        <script src="{{ asset('js/lib/my.validate.js')}}"></script>
        <script src="{{ URL::asset('assetsnew/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{ URL::asset('assetsnew/libs/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
        <script src="{{ URL::asset('assetsnew/libs/select2/js/select2.min.js')}}"></script>
        <script src="{{ URL::asset('assetsnew/libs/metismenu/metisMenu.min.js')}}"></script>
        <script src="{{ URL::asset('assetsnew/libs/node-waves/waves.min.js')}}"></script>
    </head>

    @section('body')
    @show
    <body data-sidebar="dark">
        <!-- Begin page -->
        <div id="layout-wrapper">
        @include('layouts.top-hor-final')
        @include('layouts.sidebar-final')
        <!-- Start right Content here -->
        <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                @yield('content')
            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->
        @include('layouts.footer-final')
        </div>
            <!-- end main content-->
    </div>
    <!-- END layout-wrapper -->

    <!-- Right Sidebar -->
    @include('layouts.right-sidebar-final')
    <!-- /Right-bar -->

    <!-- Right bar overlay-->
    <div class="rightbar-overlay"></div>


        <script>
            $(document).on('mouseover', '[data-toggle="tooltip"], [data-toggle-second="tooltip"]', function(){
                $(this).tooltip('show')
            })

            $(document).on('mouseout', '[data-toggle="tooltip"], [data-toggle-second="tooltip"]', function(){
                $(this).tooltip('hide')
            })
        </script>
        @yield('script')
        <script src="{{ URL::asset('js/app.js')}}"></script>
    </body>
</html>

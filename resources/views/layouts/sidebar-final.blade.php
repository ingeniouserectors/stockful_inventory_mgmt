<div class="vertical-menu" style="z-index: 100;">
    <div data-simplebar class="h-100">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>
                <li {{ ((Request::segment(1) ==  'dashboard' ? 'class=mm-active': '')  ? 'class=mm-active': '') }}>
                    <a href="/dashboard" class="waves-effect">
                        <i class="bx bx-home-circle"></i>
                        <span>Dashboards</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-layout"></i>
                        <span class="menu-link">Rolodex</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li {{ ((Request::segment(1) ==  'suppliers' ? 'class=mm-active': '')  ? 'class=mm-active': '') }}>
                            <a class="waves-effect {{ ((Request::segment(1) ==  'suppliers' ? 'class=mm-active': '')  ? 'active': '') }}" href="{{route('suppliers.index')}}">Suppliers</a>
                        </li>
                        <li {{ ((Request::segment(1) ==  'vendors' ? 'class=mm-active': '')  ? 'class=mm-active': '') }}>
                            <a class="waves-effect {{ ((Request::segment(1) ==  'vendors' ? 'class=mm-active': '')  ? 'active': '') }}" href="{{ route('vendors.index') }}">Vendors</a>
                        </li>
                        <li {{ ((Request::segment(1) ==  'warehouses' ? 'class=mm-active': '')  ? 'class=mm-active': '') }}>
                            <a class="waves-effect {{ ((Request::segment(1) ==  'warehouses'  ? 'class=mm-active': '')  ? 'active': '') }}" href="{{ route('warehouses.index') }}">Warehouses</a>
                        </li>
                        <li {{ ((Request::segment(1) ==  'shippingagent' ? 'class=mm-active': '')  ? 'class=mm-active': '') }}>
                            <a class="waves-effect {{ ((Request::segment(1) ==  'shippingagent'  ? 'class=mm-active': '')  ? 'active': '') }}" href="{{ route('shippingagent.index') }}">Shipping Agent</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="{{ route('products.index') }}" class=" waves-effect">
                        <i class="bx bx-calendar"></i>
                        <span>Products</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('defaultlogic.index')}}" class=" waves-effect">
                        <i class="bx bx-chat"></i>
                        <span>Logic</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('mws_inventory.index')}}">
                        <i class="bx bx-store"></i>
                        <span>Inventory</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);">
                        <i class="bx bx-bitcoin"></i>
                        <span>Cashflow</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);">
                        <i class="bx bx-envelope"></i>
                        <span>Reorder</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);">
                        <i class="bx bx-receipt"></i>
                        <span>Replenish</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);">
                        <i class="bx bx-briefcase-alt-2"></i>
                        <span>Removals</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>

@yield('css')
<!-- Bootstrap Css -->
<link href="{{ url('asset/css/bootstrap-dark.min.css')}}" id="bootstrap-dark" rel="stylesheet" type="text/css" />
<link href="{{ url('asset/css/bootstrap.min.css')}}" id="bootstrap-style-new" rel="stylesheet" type="text/css" />

<!-- Icons Css -->
<link href="{{ url('asset/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
<!-- App Css-->
<link href="{{ url('asset/css/app.min.css')}}" id="app-style-new" rel="stylesheet" type="text/css" />
<link href="{{ url('asset/css/app-dark.min.css')}}" id="app-dark" rel="stylesheet" type="text/css" />
<link href="{{ url('asset/css/app.min.css')}}" id="app-light" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="{{ url('asset/css/style-custom.css')}}">
<link rel="stylesheet" href="{{ url('asset/css/responsive-custom.css')}}">

<!-- DataTables -->
<link href="{{ url('asset/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ url('asset/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />

<!-- Responsive datatable examples -->
<link href="{{ url('asset/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
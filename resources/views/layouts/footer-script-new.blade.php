<!-- JAVASCRIPT -->
<script src="{{ URL::asset('asset/libs/jquery/jquery.min.js')}}"></script>
<script src="{{ URL::asset('asset/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{ URL::asset('asset/libs/metismenu/metisMenu.min.js')}}"></script>
<script src="{{ URL::asset('asset/libs/simplebar/simplebar.min.js')}}"></script>
<script src="{{ URL::asset('asset/libs/node-waves/waves.min.js')}}"></script>

<!-- Required datatable js -->
<script src="{{ URL::asset('asset/libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('asset/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>

<!-- Datatable init js -->
<script src="{{ URL::asset('asset/js/pages/datatables.init.js')}}"></script>

<!-- Buttons examples -->
<script src="{{ URL::asset('asset/libs/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ URL::asset('asset/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ URL::asset('asset/libs/jszip/jszip.min.js')}}"></script>
<script src="{{ URL::asset('asset/libs/pdfmake/build/vfs_fonts.js')}}"></script>
<script src="{{ URL::asset('asset/libs/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{ URL::asset('asset/libs/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{ URL::asset('asset/libs/datatables.net-buttons/js/buttons.colVis.min.js')}}"></script>

<!-- Responsive examples -->
<script src="{{ URL::asset('asset/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{ URL::asset('asset/libs/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ URL::asset('asset/js/sortelements.js')}}"></script>
@yield('script')

<!-- App js -->

<script src="{{ URL::asset('asset/libs/apexcharts/apexcharts.min.js')}}"></script>

<script src="{{ URL::asset('asset/js/pages/dashboard.init.js')}}"></script>
<script src="{{ URL::asset('asset/js/mindmup-editabletable.js')}}"></script>

<script src="{{ URL::asset('asset/js/app.js')}}"></script>
<script src="{{ URL::asset('asset/js/app-custom.js')}}"></script>
<script src="{{ URL::asset('assets/js/common.js')}}"></script>



@yield('script-bottom')

<header id="page-topbar">
    <div class="navbar-header">
        <div class="d-flex">
           
            <!-- LOGO -->
            <div class="navbar-brand-box">
                <a href="{{ url('/index') }}" class="logo logo-dark">
                    <span class="logo-sm">
                        <img src="{{ URL::to('/assets/images/logo.svg') }}" alt="" height="22">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ URL::to('/assets/images/logo-dark.png') }}" alt="" height="17">
                    </span>
                </a>

                <a href="{{ url('/index') }}" class="logo logo-light">
                    <span class="logo-sm">
                        <img src="{{ URL::to('/assets/images/logo-light.svg') }}" alt="" height="22">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ URL::to('/assets/images/logo-light.png') }}" alt="" height="19">
                    </span>
                </a>
            </div>

            <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect" id="vertical-menu-btn">
                <i class="fa fa-fw fa-bars"></i>
            </button>

            <!-- App Search-->
            @php($userRoleId = get_user_role(Auth::user()->id))
            @php( Session::flash('SESSION_ROLE', $userRoleId['user_assigned_role']['user_role_id']) )
            @if(session()->has('MARKETPLACE_ID'))
                @php($global_marketplace = session('MARKETPLACE_ID'))
            @else
                @php($global_marketplace = '')
            @endif

            @php($userRoleId = get_user_role(Auth::user()->id))
            @php( Session::flash('SESSION_ROLE', $userRoleId['user_assigned_role']['user_role_id']) )
            @if(session()->has('PURCHESINSTANCES_ID'))
                @php($global_purchaseinstances = session('PURCHESINSTANCES_ID'))
            @else
                @php($global_purchaseinstances = '')
            @endif
</div>

<div class="d-flex">

<div class="dropdown d-inline-block d-lg-none ml-2">
    <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-search-dropdown"
        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="mdi mdi-magnify"></i>
    </button>
    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0"
        aria-labelledby="page-header-search-dropdown">

        <form class="p-3">
            <div class="form-group m-0">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search ..." aria-label="Recipient's username">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit"><i class="mdi mdi-magnify"></i></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

    <div class="dropdown d-inline-block" style="margin-right:10px;">
        @if(session('SESSION_ROLE') == 3 || session('SESSION_ROLE') == 5 )
            @php($get_usermarketplace = get_purchase_instace(session('MARKETPLACE_ID')))
            @if(!empty($get_usermarketplace))
                <form class="app-search d-none d-lg-block">
                    <div class="position-relative">
                        <select id="myselect" class="custom-select tranck_id_change" style="width:100%; height:34px;">
                            @foreach($get_usermarketplace as $lists)
                                <option value="{{$lists['id']}}"{{$lists['id'] ==$global_purchaseinstances  ? 'selected' : ''}}>{{$lists['track_id']}}</option>
                            @endforeach
                        </select>
                    </div>
                </form>
            @endif
        @endif
    </div>


<div class="dropdown d-inline-block">
    @if(session('SESSION_ROLE') == 3)
        @php($get_usermarketplace = get_usermarketplace(Auth::id()))
        @if(!empty($get_usermarketplace))
            <form class="app-search d-none d-lg-block">
                <div class="position-relative">
                    <select class="custom-select header_marketplace" style="width:149px; height:34px;">
                        @foreach($get_usermarketplace as $lists)
                            <option value="{{$lists['id']}}" {{$lists['id'] == $global_marketplace ? 'selected' : ''}}>{{$lists['mws_market_place_country']['marketplace_country']}}</option>
                        @endforeach
                    </select>
                </div>
            </form>
        @endif
     @elseif(session('SESSION_ROLE') == 5)
        @php($get_usermarketplace = get_parent_usermarketplace(Auth::id()))
        @if(!empty($get_usermarketplace))
            <form class="app-search d-none d-lg-block">
                <div class="position-relative">
                    <select class="form-control-lg header_marketplace"style="width:149px; height:34px; font-size: 14px; border: 1px solid #ced4da;border-radius: .25rem;">
                        @foreach($get_usermarketplace as $lists)
                            <option value="{{$lists['id']}}" {{$lists['id'] == $global_marketplace ? 'selected' : ''}}>{{$lists['mws_market_place_country']['marketplace_country']}}</option>
                        @endforeach
                    </select>
                </div>
            </form>
        @endif
    @endif
</div>

<div class="dropdown d-inline-block">
    <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <img class="rounded-circle header-profile-user" src="{{ URL::to('/assets/images/users/avatar-1.jpg') }}"
            alt="Header Avatar">
        <span class="d-none d-xl-inline-block ml-1">{{ @Auth::user()->name }}</span>
        <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
    </button>
    <div class="dropdown-menu dropdown-menu-right">
        <!-- item-->
        <a class="dropdown-item" href="{{ route('users.show',\App\Services\UrlService::base64UrlEncode(Auth::user()->id)) }}"><i class="bx bx-user font-size-16 align-middle mr-1"></i> Profile</a>
        {{--<a class="dropdown-item" href="#"><i class="bx bx-wallet font-size-16 align-middle mr-1"></i> My Wallet</a>--}}
        @if(Auth::user()->id == 1)
            <a class="dropdown-item d-block" href="{{ route('setting.index') }}"><i class="bx bx-wrench font-size-16 align-middle mr-1"></i> Settings</a>
        @endif
        <div class="dropdown-divider"></div>
        <a class="dropdown-item text-danger" href="javascript:void();" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="bx bx-power-off font-size-16 align-middle mr-1 text-danger"></i> {{ __('Logout') }} </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </div>
</div>

</div>
</div>
</header>
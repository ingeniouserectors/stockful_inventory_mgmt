        <!-- JAVASCRIPT -->
        <script src="{{ URL::asset('assets/libs/jquery/jquery.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/metismenu/metisMenu.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/simplebar/simplebar.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/node-waves/waves.min.js')}}"></script>

        <!-- Required datatable js -->

        <script src="{{ URL::asset('assets/libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>

        <!-- Datatable init js -->
        <script src="{{ URL::asset('assets/js/pages/datatables.init.js')}}"></script>

        <!-- Buttons examples -->
        <script src="{{ URL::asset('assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/jszip/jszip.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/pdfmake/build/vfs_fonts.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/datatables.net-buttons/js/buttons.colVis.min.js')}}"></script>

        <!-- Responsive examples -->
        <!-- <script src="{{ URL::asset('assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script> -->
        <!-- <script src="{{ URL::asset('assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script> -->

        <!-- bootstrap-select examples -->
        <script src="{{ URL::asset('assets/libs/multi-select-bootstrap/js/bootstrap-select.min.js')}}"></script>
        <script src="{{ URL::asset('assets/js/jquery.validate.min.js')}}"></script>
        <script src="{{ URL::to('/assets/js/jquery-confirm.min.js') }}"></script>
        <script src="{{ URL::asset('assets/js/processbar.js')}}"></script>
        <script src="{{  URL::asset('assets/js/customValidation.js') }}"></script>

        @yield('script')

        <!-- App js -->

        <!-- <script src="{{ URL::asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script> -->

        <script src="{{ URL::asset('assets/js/pages/dashboard.init.js')}}"></script>

        <script src="{{ URL::asset('assets/js/app.js')}}"></script>
        <script src="{{ URL::asset('assets/js/common.js')}}"></script>

        <script src="{{ URL::asset('assets/js/sortelements.js')}}"></script>
        @yield('script-bottom')

        <script>
            $(document).on('change','.header_marketplace',function(){
                var marketplace_id = $(this).val();
                change_marketplace(marketplace_id);
            });
        </script>
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            function change_marketplace(marketplace_id){
                var session_marketplce = '{{session('MARKETPLACE_ID')}}';
                var session_role = '{{session('SESSION_ROLE')}}';
                var type = "set_marketplace";

                //localStorage.setItem('marketplace_id', '');
                ///localStorage.setItem('marketplace_id', marketplace_id);
                if(session_role == 3 || session_role == 5){
                    $.ajax({
                        type:"POST",
                        url:'{{url('set_marketplace')}}',
                        data:{marketplace_id:marketplace_id,type:type},
                        success: function(res){
                            console.log(res);
                            if(session_marketplce != res){
                                location.reload();
                            }
                        },
                        error: function(){
                            console.log('error');
                        }
                    });
                }
            }
            $(document).on('change','.tranck_id_change',function(){
                
                var purchaseinstances_id = $(this).val();  
                var session_purchaseinstance = '{{session('PURCHESINSTANCES_ID')}}';
                var session_role = '{{session('SESSION_ROLE')}}';
                var type = "set_trackid";

                if(session_role == 3 || session_role == 5){
                    $.ajax({
                        type:"POST",
                        url:'{{url('set_marketplace')}}',
                        data:{purchaseinstances_id:purchaseinstances_id,type:type},
                        success: function(res){
                            console.log(res);
                            if(session_purchaseinstance != res){
                            }
                        },
                        error: function(){
                            console.log('error');
                        }
                    });
                }
              });
            $(document).on('click','.reorder_class',function () {
                var url = '{{url('reorder')}}';
                window.location.href = url;
            });

            $(document).on('click','.replenish_class',function () {
                var url = '{{url('replenish')}}';
                window.location.href = url;
            });
        </script>

========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">
        <!--- Sidemenu -->
        {{--        @php($moduleNameArray = get_user_access_modules(Auth::user()->id))--}}
        @php( $previous_url = URL::previous() )
        <?php $previous_urls = str_replace(url('/'), '', url()->previous())?>
        @php($userRoleId = get_user_role(Auth::user()->id))

        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>

                <li>
                    <a href="{{ url('/index') }}" class="waves-effect">
                        <i class="bx bx-home-circle"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <?php $get_check_roles = get_check_access('role_module');?>
                <?php $get_user_access = get_user_check_access('role_module', 'access');?>

                @if($get_check_roles == 1)
                    @if($get_user_access == 1)
                        <li {{ Request::segment(1) == 'roles' ? 'class=mm-active': '' }}>
                            <a href="{{ route('roles.index') }}" class="waves-effect {{ Request::segment(1) == 'roles' ? 'active': '' }}" {{Request::segment(1) == 'roles' ? 'aria-expanded=false' : ''}}>
                                <i class="bx bxs-user-detail"></i>
                                <span>Roles</span>
                            </a>
                        </li>
                    @endif
                @else
                    @if($get_user_access == 1)
                        <li {{ Request::segment(1) == 'roles' ? 'class=mm-active': '' }}>
                            <a href="{{ route('roles.index') }}" class="waves-effect {{ Request::segment(1) == 'roles' ? 'active': '' }}" {{Request::segment(1) == 'roles' ? 'aria-expanded=false' : ''}}>
                                <i class="bx bxs-user-detail"></i>
                                <span>Roles</span>
                            </a>
                        </li>
                    @endif
                @endif

                @php($get_check_roles = get_check_access('report_request_module'))
                @php($get_user_access = get_user_check_access('report_request_module', 'access'))

                @if($get_check_roles == 1)
                    @if($get_user_access == 1)
                        <li><a href="{{ route('report.index') }}"><i class="bx bx-spreadsheet"></i><span>Report</span></a></li>
                    @endif
                @else
                    @if($get_user_access == 1)
                        <li><a href="{{ route('report.index') }}"><i class="bx bx-spreadsheet"></i><span>Report</span></a></li>
                    @endif
                @endif

                <?php $get_check_users = get_check_access('user_module');?>
                <?php $get_user_access = get_user_check_access('user_module', 'access');?>

                @if($get_check_users == 1)
                    @if($get_user_access == 1)
                        <li {{ Request::segment(1) == 'users' || Request::segment(1) == 'sub_user_create' || Request::segment(1)== 'display_access' ? 'class=mm-active': '' }}>
                            <a href="{{ route('users.index') }}" class="waves-effect {{ Request::segment(1) == 'users' || Request::segment(1) == 'sub_user_create' || Request::segment(1)== 'display_access' ? 'active': '' }}" {{Request::segment(1) == 'users' ? 'aria-expanded=false' : ''}}>
                                <i class="bx bxs-user-circle"></i>
                                <span>Users</span>
                            </a>
                        </li>
                    @endif
                @else
                    @if($get_user_access == 1)
                        <li {{ Request::segment(1) == 'users' || Request::segment(1) == 'sub_user_create' ? 'class=mm-active': '' }}>
                            <a href="{{ route('users.index') }}" class="waves-effect {{ Request::segment(1) == 'users' || Request::segment(1) == 'sub_user_create' ? 'active': '' }}" {{Request::segment(1) == 'users' ? 'aria-expanded=false' : ''}}>
                                <i class="bx bxs-user-circle"></i>
                                <span>Users</span>
                            </a>
                        </li>
                    @endif
                @endif

                <?php $get_check_users = get_check_access('affiliate_module');?>
                <?php $get_user_access = get_user_check_access('affiliate_module', 'access');?>

                @if($get_check_users == 1)
                    @if($get_user_access == 1)
                        @if($userRoleId['user_assigned_role']['user_role_id'] == 3 || $userRoleId['user_assigned_role']['user_role_id'] == 4)
                            <li  {{ Request::segment(1) == 'affiliate_programs' ? 'class=mm-active': '' }}>
                                <a href="{{ url('/affiliate_programs') }}" class="waves-effect {{ Request::segment(1) == 'affiliate_programs'  ? 'active': '' }}" {{Request::segment(1) == 'affiliate_programs' ? 'aria-expanded=false' : ''}}>
                                    <i class="bx bx-user-plus"></i>
                                    <span>Affiliate Program</span>
                                </a>
                            </li>
                        @else
                            <li {{ Request::segment(1) == 'affiliate_programs' ? 'class=mm-active': '' }}>
                                <a href="{{ url('/affiliate_programs') }}" class="waves-effect {{ Request::segment(1) == 'affiliate_programs'  ? 'active': '' }}" {{Request::segment(1) == 'affiliate_programs' ? 'aria-expanded=false' : ''}}>
                                    <i class="fas fa-users"></i>
                                    <span>Affiliate Program</span>
                                </a>
                            </li>
                        @endif
                    @endif
                @else
                    @if($get_user_access == 1)
                        @if($userRoleId['user_assigned_role']['user_role_id'] == 3 || $userRoleId['user_assigned_role']['user_role_id'] == 4)
                            <li  {{ Request::segment(1) == 'affiliate_programs' ? 'class=mm-active': '' }}>
                                <a href="{{ url('/affiliate_programs') }}" class="waves-effect {{ Request::segment(1) == 'affiliate_programs'  ? 'active': '' }}" {{Request::segment(1) == 'affiliate_programs' ? 'aria-expanded=false' : ''}} >
                                    <i class="bx bx-user-plus"></i>
                                    <span>Affiliate Program</span>
                                </a>
                            </li>
                        @else
                            <li  {{ Request::segment(1) == 'affiliate_programs' ? 'class=mm-active': '' }}>
                                <a href="{{ url('/affiliate_programs') }}" class="waves-effect {{ Request::segment(1) == 'affiliate_programs'  ? 'active': '' }}" {{Request::segment(1) == 'affiliate_programs' ? 'aria-expanded=false' : ''}}>
                                    <i class="fas fa-users"></i>
                                    <span>Affiliate Program</span>
                                </a>
                            </li>
                        @endif
                    @endif
                @endif

                <?php $get_check_setting = get_check_access('mws_module');?>
                <?php $get_user_access = get_user_check_access('mws_module', 'access');?>

                @if($get_check_setting == 1)
                    @if($get_user_access == 1)
                        <li {{ Request::segment(1) == 'amazonsettings' ? 'class=mm-active': '' }}>
                            <a href="{{ route('amazonsettings.index') }}" class="waves-effect {{ Request::segment(1) == 'amazonsettings'  ? 'active': '' }}">
                                <i class="fab fa-amazon "></i>
                                @if(Auth::user()->id == 1)
                                    <span>Developer Keys</span>
                                @else
                                    <span>Amazon Marketplace</span>
                                @endif
                            </a>
                        </li>
                    @endif
                @else
                    @if($get_user_access == 1)
                        <li>
                            <a href="{{ route('amazonsettings.index') }}" class="waves-effect">
                                <i class="fab fa-amazon "></i>
                                @if(Auth::user()->id == 1)
                                    <span>Developer Keys</span>
                                @else
                                    <span>Amazon Marketplace</span>
                                @endif
                            </a>
                        </li>
                    @endif
                @endif

                @php($get_check_main_setting1 = get_check_access('common_setting_module'))
                @php($get_usercheck_access1 = get_user_check_access('common_setting_module', 'access'))

                @php($get_check_main_setting2 = get_check_access('fba_setting_module'))
                @php($get_usercheck_access2 = get_user_check_access('fba_setting_module', 'access'))

                @php($get_check_main_setting3 = get_check_access('vendor_setting_module'))
                @php($get_usercheck_access3 = get_user_check_access('vendor_setting_module', 'access'))

                @php($get_check_main_setting4 = get_check_access('supplier_setting_module'))
                @php($get_usercheck_access4 = get_user_check_access('supplier_setting_module', 'access'))

                @php($get_check_main_setting5 = get_check_access('warehouse_setting_module'))
                @php($get_usercheck_access5 = get_user_check_access('warehouse_setting_module', 'access'))

                @php($get_check_main_setting6 = get_check_access('blackoute_setting_module'))
                @php($get_usercheck_access6 = get_user_check_access('blackoute_setting_module', 'access'))

                @php( $get_check_main_setting7 = get_check_access('vendor_module'))
                @php( $get_usercheck_access7 = get_user_check_access('vendor_module','access'))

                @php( $get_check_main_setting8 = get_check_access('supplier_module'))
                @php( $get_usercheck_access8 = get_user_check_access('supplier_module','access'))

                @php( $get_check_main_setting9 = get_check_access('warehouse_module'))
                @php( $get_usercheck_access9 = get_user_check_access('warehouse_module','access'))

                @php( $get_check_main_setting10 = get_check_access('default_logic'))
                @php( $get_usercheck_access10 = get_user_check_access('default_logic','access'))

                @if(($get_check_main_setting1 == 1) || ($get_check_main_setting2 == 1) || ($get_check_main_setting4 == 1) || ($get_check_main_setting4 == 1) || ($get_check_main_setting5 == 1) || ($get_check_main_setting6 == 1) || ($get_check_main_setting7 == 1) || ($get_check_main_setting8 == 1) || ($get_check_main_setting9 == 1) || ($get_check_main_setting10 == 1))

                    <li class="">
                        <a href="javascript: void(0);" class="has-arrow waves-effect" aria-expanded="true">
                            <i class="bx bxs-cog"></i>
                            <span>Setting</span>
                        </a>
                        <ul class="" aria-expanded="false" >
                            @if($get_usercheck_access1 == 1)
                                <li><a href="{{ route('setting.index') }}"><i class="bx bxs-cog"></i><span>Common Setting</span></a></li>
                            @endif
                            @if($get_usercheck_access2 == 1)
                                <li {{ Request::segment(1) == 'fbasetting' ? 'class=mm-active': '' }}>
                                    <a href="{{ route('fbasetting.index') }}" class="waves-effect {{ Request::segment(1) == 'fbasetting' ? 'active': '' }}">
                                        <i class="bx bxs-cog"></i>
                                        <span>FBA Setting</span>
                                    </a>
                                </li>
                            @endif
                            @if($get_usercheck_access3 == 1)
                                <li><a href="{{route('vendorsetting.index')}}"><i class="fa fa-user"></i><span>Vendors Setting</span></a></li>
                            @endif
                            @if($get_usercheck_access4 == 1)
                                <li><a href="{{route('suppliersetting.index')}}"><i class="fa fa-male"></i><span>Suppliers Setting</span></a></li>
                            @endif
                            @if($get_usercheck_access5 == 1)
                                <li><a href="{{ route('warehoussetting.index') }}" ><i class="fa fa-university"></i><span>Warehouse Setting</span></a></li>
                            @endif

                            @if($get_check_main_setting7 == 1)
                                @if($get_usercheck_access7 == 1)
                                    <li {{ Request::segment(1) == 'vendors' || Request::segment(1) == 'vendorscontact' || Request::segment(1) == 'vendor_blackoute_date' ? 'class=mm-active': '' }}>
                                        <a href="{{ route('vendors.index') }}" class="waves-effect {{ Request::segment(1) == 'vendors' || Request::segment(1) == 'vendorscontact' || Request::segment(1) == 'vendor_blackoute_date' ? 'active': '' }}">
                                            <i class="fa fa-user"></i><span>Vendors</span>
                                        </a>
                                    </li>
                                @endif
                            @else
                                @if($get_usercheck_access7 == 1)
                                    <li{{ Request::segment(1) == 'vendors' || Request::segment(1) == 'vendorscontact' || Request::segment(1) == 'vendor_blackoute_date' ? 'class=mm-active': '' }}>
                                        <a href="{{ route('vendors.index') }}" class="waves-effect {{ Request::segment(1) == 'vendors' || Request::segment(1) == 'vendorscontact' || Request::segment(1) == 'vendor_blackoute_date' ? 'active': '' }}">
                                            <i class="fa fa-user"></i><span>Vendors</span>
                                        </a>
                                    </li>
                                @endif
                            @endif

                            @if($get_check_main_setting8 == 1)
                                @if($get_usercheck_access8 == 1)
                                    <li {{ Request::segment(1) == 'suppliers' || Request::segment(1) == 'supplierscontact' || Request::segment(1) == 'supplier_blackoute_date' ? 'class=mm-active': '' }}>
                                        <a href="{{ route('suppliers.index') }}" class="waves-effect {{ Request::segment(1) == 'suppliers' || Request::segment(1) == 'supplierscontact' || Request::segment(1) == 'supplier_blackoute_date' ? 'active': '' }}">
                                            <i class="fa fa-male"></i><span>Suppliers</span>
                                        </a>
                                    </li>
                                @endif
                            @else
                                @if($get_usercheck_access8 == 1)
                                    <li {{ Request::segment(1) == 'suppliers' || Request::segment(1) == 'supplierscontact' || Request::segment(1) == 'supplier_blackoute_date' ? 'class=mm-active': '' }}>
                                        <a href="{{ route('suppliers.index') }}" class="waves-effect {{ Request::segment(1) == 'suppliers' || Request::segment(1) == 'supplierscontact' || Request::segment(1) == 'supplier_blackoute_date' ? 'active': '' }}">
                                            <i class="fa fa-male"></i><span>Suppliers</span>
                                        </a>
                                    </li>
                                @endif
                            @endif

                            @if($get_check_main_setting9 == 1)
                                @if($get_usercheck_access9 == 1)
                                    <li {{ Request::segment(1) == 'warehouses' ? 'class=mm-active': '' }}>
                                        <a href="{{ route('warehouses.index') }}" class="waves-effect {{ Request::segment(1) == 'warehouses' ? 'active': '' }}">
                                            <i class="fa fa-university"></i><span>Warehouse</span>
                                        </a>
                                    </li>
                                @endif
                            @else
                                @if($get_usercheck_access9 == 1)
                                    <li {{ Request::segment(1) == 'warehouses' ? 'class=mm-active': '' }}>
                                        <a href="{{ route('warehouses.index') }}" class="waves-effect {{ Request::segment(1) == 'warehouses' ? 'active': '' }}">
                                            <i class="fa fa-university"></i><span>Warehouse</span>
                                        </a>
                                    </li>
                                @endif
                            @endif

                            @if($get_check_main_setting10 == 1)
                                @if($get_usercheck_access10 == 1)
                                    <li class="">
                                        <a href="{{route('defaultlogic.index')}}" class="waves-effect">
                                            <i class="fa fa-cogs"></i>
                                            <span>Logic</span>
                                        </a>
                                    </li>
                                @endif
                            @else
                                @if($get_usercheck_access10 == 1)
                                    <li class="">
                                        <a href="{{route('defaultlogic.index')}}" class="waves-effect">
                                            <i class="fa fa-cogs"></i>
                                            <span>Logic</span>
                                        </a>
                                    </li>
                                @endif
                            @endif
                            @if($get_check_main_setting6 == 1)    
                                @if($get_usercheck_access6 == 1)
                                    <li {{ Request::segment(1) == 'blakoutedatesetting' ? 'class=mm-active': '' }}><a  href="{{ route('blakoutedatesetting.index') }}" class="waves-effect {{ Request::segment(1) == 'blakoutedatesetting'  ? 'active': '' }}" {{Request::segment(1) == 'blakoutedatesetting' ? 'aria-expanded=false' : ''}}><i class="fa fa-asterisk"></i><span>Blackoutedate Setting</span></a></li>
                                @endif
                            @else
                                @if($get_usercheck_access6 == 1)
                                <li {{ Request::segment(1) == 'blakoutedatesetting' ? 'class=mm-active': '' }}><a  href="{{ route('blakoutedatesetting.index') }}" class="waves-effect {{ Request::segment(1) == 'blakoutedatesetting'  ? 'active': '' }}" {{Request::segment(1) == 'blakoutedatesetting' ? 'aria-expanded=false' : ''}}><i class="fa fa-asterisk"></i><span>Blackoutedate Setting</span></a></li>
                                @endif
                            @endif
                        </ul>
                    </li>
                @endif

                <?php $get_check_setting = get_check_access('subscription_module');?>
                <?php $get_user_access = get_user_check_access('subscription_module', 'access');?>

                @if($get_check_setting == 1)
                    @if($get_user_access == 1)
                        <li {{ Request::segment(1) == 'modules'  || Request::segment(1) == 'moduletable' ? 'class=mm-active': '' ? 'class=mm-active': '' }}>
                            <a href="{{ route('modules.index') }}" class="waves-effect {{ Request::segment(1) == 'modules'  || Request::segment(1) == 'moduletable' ? 'class=mm-active': '' ? 'active': '' }}">
                                <i class="fas fa-list"></i>
                                <span>Modules</span>
                            </a>
                        </li>
                    @endif
                @else
                    @if($get_user_access == 1)
                        <li {{ Request::segment(1) == 'modules'  || Request::segment(1) == 'moduletable' ? 'class=mm-active': '' }}>
                            <a href="{{ route('modules.index') }}" class="waves-effect {{ Request::segment(1) == 'modules' || Request::segment(1) == 'moduletable' ? 'active': '' }}">
                                <i class="fas fa-list"></i>
                                <span>Modules</span>
                            </a>
                        </li>
                    @endif
                @endif

                @php( $get_check_setting = get_check_access('product_module'))
                @php( $get_user_access = get_user_check_access('product_module','access'))

                @if($get_check_setting == 1)
                    @if($get_user_access == 1)
                    <li  {{ Request::segment(1) == 'products_warehouse_assign' || $previous_urls == '/products' || Request::segment(1) == 'products' ? 'class=mm-active': '' }}>
                        <a href="{{ route('products.index') }}" class="waves-effect {{ Request::segment(1) == 'products_warehouse_assign' || $previous_urls == '/products' || Request::segment(1) == 'products'  ? 'active': '' }}" {{Request::segment(1) == 'products' ? 'aria-expanded=false' : ''}}>
                            <i class="fab fa-amazon "></i><span>Products</span>
                        </a>
                    </li>
                    @endif
                @else
                    @if($get_user_access == 1)
                    <li  {{ Request::segment(1) == 'products_warehouse_assign' || $previous_urls == '/products' || Request::segment(1) == 'products' ? 'class=mm-active': '' }}>
                        <a href="{{ route('products.index') }}" class="waves-effect {{ Request::segment(1) == 'products_warehouse_assign' || $previous_urls == '/products' || Request::segment(1) == 'products'  ? 'active': '' }}" {{Request::segment(1) == 'products' ? 'aria-expanded=false' : ''}}>
                            <i class="fab fa-amazon "></i><span>Products</span>
                        </a>
                    </li>
                    @endif
                @endif

                @php( $get_check_setting1 = get_check_access('reorder_module'))
                @php( $get_user_access1 = get_user_check_access('reorder_module','access'))

                @php( $get_check_setting2 = get_check_access('pi_module'))
                @php( $get_user_access2 = get_user_check_access('pi_module','access'))

                @php( $get_check_setting3 = get_check_access('po_module'))
                @php( $get_user_access3 = get_user_check_access('po_module','access'))

                @if(($get_check_setting1 == 1) || ($get_check_setting2 == 1) || ($get_check_setting3 == 1))
                    <li>
                        @if($get_user_access1 == 1)
                            <a href="javascript:void(0)"   class="has-arrow waves-effect click {{ (($previous_urls == '/reorder') || Request::segment(1) == 'reorder') ? 'mm-active': ''}}" aria-expanded="true">
                                <i class="fas fa-shopping-cart "></i>
                                <span class="reorder_class">Reorder</span>
                            </a>
                            @else
                                <a href="javascript:void(0)"   class="has-arrow waves-effect click {{ (($previous_urls == '/reorder') || Request::segment(1) == 'reorder') ? 'mm-active': ''}}" aria-expanded="true">
                                    <i class="fas fa-shopping-cart "></i>
                                    <span class="">Reorder</span>
                                </a>
                        @endif

                        <ul class="" aria-expanded="false" >
                            @if($get_user_access2 == 1)
                                <li {{ Request::segment(1) == 'purchaseinstances' ? 'class=mm-active': '' }}>
                                    <a href="{{route('purchaseinstances.index')}}" class="waves-effect {{ Request::segment(1) == 'purchaseinstances'  ? 'active': '' }}">
                                        <i class="fa fa-cart-arrow-down "></i>
                                        <span>PI</span>
                                    </a>
                                </li>
                            @endif
                            @if($get_user_access3 == 1)
                                <li {{ Request::segment(1) == 'purchaseorder' ? 'class=mm-active': '' }}>
                                    <a href="{{route('purchaseorder.index')}}" class="waves-effect {{ Request::segment(1) == 'purchaseorder'  ? 'active': '' }}">
                                        <i class="fa fa-shopping-basket"></i>
                                        <span>PO</span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </li> 
                   
                @else
                    
                    <li>
                        @if($get_user_access1 == 1)
                            <a href="javascript:void(0)"   class="has-arrow waves-effect click {{ (($previous_urls == '/reorder') || Request::segment(1) == 'reorder') ? 'mm-active': ''}}" aria-expanded="true">
                                <i class="fas fa-shopping-cart "></i>
                                <span class="reorder_class">Reorder</span>
                            </a>
                        @endif

                        <ul class="" aria-expanded="false" >
                            @if($get_user_access2 == 1)
                                <li {{ Request::segment(1) == 'purchaseinstances' ? 'class=mm-active': '' }}>
                                    <a href="{{route('purchaseinstances.index')}}" class="waves-effect {{ Request::segment(1) == 'purchaseinstances'  ? 'active': '' }}">
                                        <i class="fa fa-cart-arrow-down "></i>
                                        <span>PI</span>
                                    </a>
                                </li>
                            @endif
                            @if($get_user_access3 == 1)
                                <li {{ Request::segment(1) == 'purchaseorder' ? 'class=mm-active': '' }}>
                                    <a href="{{route('purchaseorder.index')}}" class="waves-effect {{ Request::segment(1) == 'purchaseorder'  ? 'active': '' }}">
                                        <i class="fa fa-shopping-basket"></i>
                                        <span>PO</span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </li>
                   
                @endif


                @php( $get_check_setting1 = get_check_access('replenish_module'))
                @php( $get_user_access1 = get_user_check_access('replenish_module','access'))

                @php( $get_check_setting2 = get_check_access('outbound_module'))
                @php( $get_user_access2 = get_user_check_access('outbound_module','access'))

                @php( $get_check_setting3 = get_check_access('inbound_module'))
                @php( $get_user_access3 = get_user_check_access('inbound_module','access'))

                @if(($get_check_setting1 == 1) || ($get_check_setting2 == 1) || ($get_check_setting3 == 1))
                    <li>
                        @if($get_user_access1 == 1)
                            <a href="javascript:void(0)"   class="has-arrow waves-effect click {{  (Request::segment(1) == 'replenish') || Request::segment(1) == 'replenish' ? 'mm-active': ''}}" aria-expanded="true">
                            <i class="bx bxs-cog"></i>
                            <span class="replenish_class">Replenish</span>
                        </a>
                        @else
                            <a href="javascript:void(0)"   class="has-arrow waves-effect click {{  (Request::segment(1) == 'replenish') || Request::segment(1) == 'replenish' ? 'mm-active': ''}}" aria-expanded="true">
                            <i class="bx bxs-cog"></i>
                            <span class="">Replenish</span>
                        </a>
                        @endif

                        <ul class="" aria-expanded="false" >
                            @if($get_user_access2 == 1)
                                <li>
                                <a href="javascript:void(0)" class="waves-effect">
                                    <i class="bx bxs-user-circle"></i>
                                    <span>Outbound</span>
                                </a>
                            </li>
                            @endif
                            @if($get_user_access3 == 1)
                                <li {{ Request::segment(1) == 'inbound' ? 'class=mm-active': '' }} >
                                <a href="{{route('inbound.index')}}" class="waves-effect {{ Request::segment(1) == 'inbound' ? 'active': '' }}">
                                    <i class="bx bxs-user-circle"></i>
                                    <span>Inbound</span>
                                </a>
                            </li>
                            @endif
                        </ul>
                    </li> 
                   
                @else

                    <li>
                        @if($get_user_access1 == 1)
                            <a href="javascript:void(0)"   class="has-arrow waves-effect click {{  (Request::segment(1) == 'replenish') || Request::segment(1) == 'replenish' ? 'mm-active': ''}}" aria-expanded="true">
                            <i class="bx bxs-cog"></i>
                            <span class="replenish_class">Replenish</span>
                        </a>
                        @endif

                        <ul class="" aria-expanded="false" >
                            @if($get_user_access2 == 1)
                                <li>
                                <a href="javascript:void(0)" class="waves-effect">
                                    <i class="bx bxs-user-circle"></i>
                                    <span>Outbound</span>
                                </a>
                            </li>
                            @endif
                            @if($get_user_access3 == 1)
                                <li {{ Request::segment(1) == 'inbound' ? 'class=mm-active': '' }} >
                                <a href="{{route('inbound.index')}}" class="waves-effect {{ Request::segment(1) == 'inbound' ? 'active': '' }}">
                                    <i class="bx bxs-user-circle"></i>
                                    <span>Inbound</span>
                                </a>
                            </li>
                            @endif
                        </ul>
                    </li>
                @endif


                @php( $get_check_access = get_check_access('order_module'))
                @php( $get_user_access = get_user_check_access('order_module','access'))     
                
                @if($get_check_access == 1)
                    @if($get_user_access == 1)
                    <li>
                        <a href="{{route('memberorder.index')}}">
                            <i class="bx bxs-user-circle"></i>
                            <span>Order</span>
                        </a>
                    </li>
                    @endif
                @else
                    @if($get_user_access == 1)
                        <li>
                            <a href="{{route('memberorder.index')}}">
                                <i class="bx bxs-user-circle"></i>
                                <span>Order</span>
                            </a>
                        </li>
                    @endif
                @endif

                @php($get_check_access = get_check_access('order_return_module'))
                @php( $get_user_access = get_user_check_access('order_return_module','access'))

                @if($get_check_access == 1)
                    @if($get_user_access == 1)                                      
                        <li>
                            <a href="{{route('orderreturns.index')}}">
                                <i class="bx bxs-user-circle"></i>
                                <span>Order returns</span>
                            </a>
                        </li>
                    @endif
                @else
                    @if($get_user_access == 1)
                        <li>
                            <a href="{{route('orderreturns.index')}}">
                                <i class="bx bxs-user-circle"></i>
                                <span>Order returns</span>
                            </a>
                        </li>
                    @endif
                @endif 

                @php($get_check_access = get_check_access('removals_module'))
                @php( $get_user_access = get_user_check_access('removals_module','access'))

                @if($get_check_access == 1)
                    @if($get_user_access == 1)                                      
                        <li>
                            <a href="javascript:void(0)">
                                <i class="bx bxs-user-circle"></i>
                                <span>REMOVALS</span>
                            </a>
                        </li>
                    @endif
                @else
                    @if($get_user_access == 1)
                        <li>
                            <a href="javascript:void(0)">
                                <i class="bx bxs-user-circle"></i>
                                <span>REMOVALS</span>
                            </a>
                        </li>
                    @endif
                @endif 

                @php($get_check_access = get_check_access('mws_excess_inventory_module'))
                @php( $get_user_access = get_user_check_access('mws_excess_inventory_module','access'))

                @if($get_check_access == 1)
                    @if($get_user_access == 1)                                      
                        <li>
                            <a href="{{route('mws_excess_inventory.index')}}">
                                <i class="fab fa-amazon "></i>
                                <span> Excess Inventory</span>
                            </a>
                         </li>
                    @endif
                @else
                    @if($get_user_access == 1)
                        <li>
                            <a href="{{route('mws_excess_inventory.index')}}">
                                <i class="fab fa-amazon "></i>
                                <span> Excess Inventory</span>
                            </a>
                         </li>
                    @endif
                @endif

                @php($get_check_access = get_check_access('mws_inventory_module'))
                @php( $get_user_access = get_user_check_access('mws_inventory_module','access'))  

                @if($get_check_access == 1)
                    @if($get_user_access == 1)                                      
                        <li>
                            <a href="{{route('mws_inventory.index')}}">
                            <i class="fab fa-amazon "></i>
                            <span>Inventory</span>
                            </a>
                        </li>
                    @endif
                @else
                    @if($get_user_access == 1)
                        <li>
                            <a href="{{route('mws_inventory.index')}}">
                            <i class="fab fa-amazon "></i>
                            <span>Inventory</span>
                            </a>
                        </li>
                    @endif
                @endif

                @php($get_check_access = get_check_access('afn_inventory_module'))
                @php( $get_user_access = get_user_check_access('afn_inventory_module','access'))     
                
                @if($get_check_access == 1)
                    @if($get_user_access == 1)                                      
                        <li>
                            <a href="{{route('mws_afn_inventory.index')}}">
                                <i class="fab fa-amazon "></i>
                                <span>AFN Inventory</span>
                            </a>
                        </li>
                    @endif
                @else
                    @if($get_user_access == 1)
                        <li>
                            <a href="{{route('mws_afn_inventory.index')}}">
                                <i class="fab fa-amazon "></i>
                                <span>AFN Inventory</span>
                            </a>
                        </li>
                    @endif
                @endif

                @php($get_checkorder_setting = get_check_access('supply_order_logic_module'))
                @php($get_userorder_access = get_user_check_access('supply_order_logic_module','access'))

                @if($get_checkorder_setting == 1)
                    @if($get_userorder_access == 1)
                        <li {{ Request::segment(1) == 'supplyorderlogic' ? 'class=mm-active': '' }}>
                            <a href="{{route('supplyorderlogic.index')}}" class="waves-effect {{ Request::segment(1) == 'supplyorderlogic'  ? 'active': '' }}">
                                <i class="fas fa-shopping-cart "></i>
                                <span>Supply order logic</span>
                            </a>
                        </li>
                    @endif
                @else
                    @if($get_userorder_access == 1)
                        <li {{ Request::segment(1) == 'supplyorderlogic' ? 'class=mm-active': '' }}>
                            <a href="{{route('supplyorderlogic.index')}}" class="waves-effect {{ Request::segment(1) == 'supplyorderlogic'  ? 'active': '' }}">
                                <i class="fas fa-shopping-cart "></i>
                                <span>Supply order logic</span>
                            </a>
                        </li>
                    @endif
                @endif

            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->

<!-- JAVASCRIPT -->


<script src="{{ URL::asset('assetsnew/libs/simplebar/simplebar.min.js')}}"></script>


<!-- Required datatable js -->
<script src="{{ URL::asset('assetsnew/libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('assetsnew/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>

<!-- Buttons examples -->
<script src="{{ URL::asset('assetsnew/libs/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ URL::asset('assetsnew/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ URL::asset('assetsnew/libs/jszip/jszip.min.js')}}"></script>
<script src="{{ URL::asset('assetsnew/libs/pdfmake/build/pdfmake.min.js')}}"></script>
<script src="{{ URL::asset('assetsnew/libs/pdfmake/build/vfs_fonts.js')}}"></script>
<script src="{{ URL::asset('assetsnew/libs/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{ URL::asset('assetsnew/libs/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{ URL::asset('assetsnew/libs/datatables.net-buttons/js/buttons.colVis.min.js')}}"></script>

<!-- Responsive examples -->
<script src="{{ URL::asset('assetsnew/libs/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ URL::asset('assetsnew/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
{{--<script src="{{ URL::asset('asset/js/sortelements.js')}}"></script>--}}


{{--<script src="{{ URL::asset('assetsnew/js/countrystatecity.js')}}"></script>--}}

<script src="{{ URL::asset('assetsnew/js/app.js')}}"></script>

<!-- Page init js -->

@yield('script')

@yield('script-bottom')

@yield('css')
<!-- Bootstrap Css -->
<link href="{{ URL::asset('assetsnew/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assetsnew/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assetsnew/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />

<link href="{{ URL::asset('assetsnew/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />

<link href="{{ URL::asset('assetsnew/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('assetsnew/libs/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css">


<!-- Bootstrap Css -->
<link href="{{ URL::asset('assetsnew/css/bootstrap.min.css')}}" id="bootstrap-style" rel="stylesheet" type="text/css" />
<!-- Icons Css -->
<link href="{{ URL::asset('assetsnew/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
<!-- App Css-->
<!-- <link href="{{ URL::asset('assetsnew/css/app.min.css')}}" id="app-style" rel="stylesheet" type="text/css" /> -->
<link href="{{ URL::asset('assetsnew/css/app.css')}}" id="app-style" rel="stylesheet" type="text/css" />


<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
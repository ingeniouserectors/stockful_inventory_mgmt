<div class="card-body" style="margin-bottom: 30px;">
    <h4 class="card-title mb-4">Inbound Shipment</h4>
    <div class="box">
        <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap users-datatable" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                <thead class="thead-light">
                <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>Shipment ID</th>
                    <th>Sent</th>
                    <th>Received</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($inbound_shipment))
                    @foreach($inbound_shipment as $key => $inbound_shipment)

                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$inbound_shipment['inbound_shipment']['ShipmentName']}}</td>
                            <td>{{$inbound_shipment['inbound_shipment']['ShipmentId']}}</td>
                            <td>{{$inbound_shipment['QuantityShipped']}}</td>
                            <td>{{$inbound_shipment['QuantityReceived']}}</td>
                        </tr>
                    @endforeach
                @else
                    <td colspan="5" align="center">No record found.</td>
                @endif

                </tbody>
            </table>
        </div>
    </div>
</div>
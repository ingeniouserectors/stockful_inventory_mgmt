@extends('layouts.master')

@section('title') Inventory Order List @endsection

@section('content')

    <style>
        .success-icon{background-color: green !important;
            color: white;
            border-radius: 50%;}
        .failer-icon{
            background-color:#ff7068 !important;
            color: white;
            border-radius: 50%;
        }
        td .fancybox-button{background: rgba(222, 216, 216, 0.6);display: inline-block; width: 120px; height: auto; margin: 5px !important;}
        td .fancybox-button[css-attr="video"]{width: 220px;}
        .tdname span{margin:0;padding:5px 0 0 0;}
        .table-news-image{height:100px; width:100px; object-fit:cover; display:inline-block; padding:5px;}
        .table-news-text{max-height:200px; overflow:auto;}
    </style>


    <div class="row">
        <div class="col-12">

            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Inventory</h4>
                <input type="hidden" value="{{$marketplace}}" name="marketplaces" id="marketplaces">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Inventory</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <a class="btn btn-info waves-effect waves-light mb-3"  href="{{ route('inventory.show','import') }}" role="button"><i class="fa fa-download"></i> Export CSV </a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">

            </div>
        </div>
    </div>
    <!-- end row -->
    <div class="card">

        <div class="card-body" style="margin-bottom: 30px;">
            <h4 class="card-title mb-4">Inventory List</h4>
            <div class="box">
                <div class="table-responsive">
                    <table id="datatables" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead class="thead-light">
                        <tr>
                            <th></th>
                            <th>No.</th>
                            <th>SKU</th>
                            <th>Amazon Stock </th>
                            <th>FC transfer</th>
                            <th>FC Processing</th>
                            <th>Amazon Receiving </th>
                            <th>Amazon Inbound </th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty($product_sku))
                            @foreach($product_sku as $key => $skus)

                                <tr id="row_{{$skus["id"]}}" data-val="{{$skus["id"]}}">
                                    <td><a href="javascript:void(0);" class="view_media view_medias_{{$skus['id']}} btn btn-sm btn-icon btn-circle btn-default"><i class="fa fa-plus success-icon"></i></a></td>
                                    <td>{{$key+1}}</td>
                                    <td>{{$skus['sku']}}</td>
                                    <td>{{$skus['mws_afn_inventory']['quantity_available']}}</td>
                                    <td> {{ $skus['mws_reserved_inventory']['reserved_fc_transfer'] }} </td>
                                    <td> {{ $skus['mws_reserved_inventory']['reserved_fc_processing'] }} </td>
                                    <td> - </td>
                                    <td> - </td>

                                </tr>
                            @endforeach
                        @endif

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @endsection
        @section('script')

            <script>
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $(document).on('click','.view_media',function(){
                    var news_id = $(this).parents('tr').attr('data-val');
                    var $obj = $(this);
                    $tr = $(this).parents('tr');
                    var data_class = $("#row_"+news_id).find("i").hasClass('success-icon');
                    console.log(data_class);
                    if(data_class === true) {
                        if (news_id != '') {
                            var user_type = 'show_inboung_shipment_id';
                            $.ajax({
                                type: "POST",
                                url: '{{ route('inventory.store') }}',
                                data:{news_id:news_id,user_type:user_type},
                                dataType: "json",
                                beforeSend: function () {
                                    $(".view_media").find("i").removeClass("fa-plus").addClass("fa-minus");
                                    $(".view_media").find("i").removeClass("success-icon").addClass("failer-icon");
                                    $(".view_media").find("i").not($obj.find("i")).removeClass("fa-minus").addClass("fa-plus");
                                    $(".view_media").find("i").not($obj.find("i")).removeClass("failer-icon").addClass("success-icon");
                                    //$(".view_media").removeClass("success-icon").addClass("failer-icon");
                                    $('.newstr').remove();
                                },
                                success: function (res) {

                                    $("#wait").css("display", "none");
                                    if (typeof res.error != 'undefined' && res.error == 0) {
                                        $tr.after('<tr class="media_' + news_id + ' newstr"><td colspan="6">' + res.view + '</td><tr>');
                                        //$(".fancybox-button").fancybox({openEffect	: 'none', closeEffect	: 'none'});
                                        $obj.find("i").addClass("fa-minus");
                                        $obj.find("i").removeClass("fa-plus");

                                        $obj.find("i").addClass("failer-icon");
                                        $obj.find("i").removeClass("success-icon");

                                    } else if (typeof res.error != 'undefined' && res.error == 1) {
                                        alert(res.msg);
                                    }
                                }
                            });
                        }
                    }else{
                        $("#row_").find("i").hasClass('success-icon');
                        $(".view_medias_"+news_id).find("i").removeClass("fa-minus").addClass("fa-plus");
                        $(".view_medias_"+news_id).find("i").removeClass("failer-icon").addClass("success-icon");
                        $(".view_medias_"+news_id).find("i").removeClass("failer-icon").addClass("success-icon");
                        $(".media_"+news_id).toggle();
                    }
                });
            </script>

            <script>

                function archiveFunction() {
                    event.preventDefault(); // prevent form submit
                    var form = event.target.form; // storing the form
                    swal({
                            title: "Are you sure?",
                            text: "You want to delete this record",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Yes, Delete it!",
                            cancelButtonText: "No, cancel please!",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },
                        function(isConfirm){
                            if (isConfirm) {
                                form.submit();          // submitting the form when user press yes
                            } else {
                                swal("Cancelled", "", "error");
                            }
                        });
                }
            </script>
@endsection
@extends('layouts.master')

@section('title') Excess Inventory List @endsection

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Excess Inventory</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Excess Inventory</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
    @if(session()->has('message'))
        {!! session('message') !!}
    @endif

    <!-- end row -->
    <div class="card marketplace_wise_data">
        <div class="card-body">
                <h4 class="card-title mb-4">Excess Inventory List</h4>
            <input type="hidden" value="{{$marketplace}}" name="marketplaces" id="marketplaces">
            <div class="table-responsive">
                <table id="datatables" class="table table-striped table-bordered dt-responsive nowrap users-datatable" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead class="thead-light">
                        <tr>
                            <th>No.</th>
                            <th>FNSKU</th>
                            <th>ASIN</th>
                            <th>Your Price</th>
                            <th>Product Name</th>
                            <th>Updated On</th>
                            <th>Quantity</th>
                            <th>Cost</th>
                        </tr>
                        </thead>
                    <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>        

@endsection
@section('script')
    <script>

        $(document).ready(function () {
            var id = $('#marketplaces').val();
                     loadDashboard(id);
        });
        function loadDashboard(id){
            var count=1;
            var table = $('#datatables').DataTable({
                "order": [ 6, 'desc' ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('mws_excess_inventory.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.id=id
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                'columnDefs': [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return  row['fnsku'];
                        }
                    },
                    {
                        targets: 2,
                        render: function (data, type, row) {
                            return row['asin'];
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, row) {
                            return row['your_price'];

                        }
                    },
                    {
                        targets: 4,
                        render: function (data, type, row) {
                            var result = row['product_name'].substring(0, 25)+'...';
                            return pro_name = '<span title="'+row['product_name']+'">'+result+'</span>';
                        }
                    },
                    {
                        targets: 5,
                        render: function (data, type, row) {
                           return row['request_date'];
                        }
                    },
                    {
                        targets: 6,
                        render: function (data, type, row) {
                            return row['total_qty_sellable'];
                        }
                    },
                    {
                        targets: 7,
                        render: function (data, type, row) {
                            return row['buybox_price'];
                        }
                    }
                ]
            });
        }
    </script>
@endsection
 @extends('layouts.master')

@section('title') Order returns List @endsection

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Order returns</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Order returns</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">

            </div>
        </div>
    </div>
    <!-- end row -->
    <div class="card marketplace_wise_data">
        <div class="card-body">
                <h4 class="card-title mb-4">Order returns List</h4>
            <input type="hidden" value="{{$marketplace}}" name="marketplaces" id="marketplaces">
            <div class="table-responsive">
                <table id="datatables" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead class="thead-light">
                        <tr>
                            <th>No.</th>
                            <th>SKU</th>
                            <th>Product Name</th>
                            <th>ASIN</th>
                            <th>FNSKU</th>
                            <th>Return Date</th>
                            <th>Quantity</th>
                            <th>Fulfillment Center Id</th>
                            <th>Detailed Disposition</th>
                            <th>Reason</th>
                            <th>Status</th>
                            <th>License Plate Number</th>
                            <th>Customer Comments</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>        

@endsection
@section('script')
    <script>
        $(document).ready(function () {
            var id = $('#marketplaces').val();
            loadDashboard(id);
        });
        function loadDashboard(id){
        	var order_return_list='order_return_list';
            var count = 1;
            var table = $('#datatables').DataTable({
                "order": [ 0, 'asc' ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('orderreturns.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.id=id,d.request_type=order_return_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },

                'columnDefs': [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return row['sku'];
                        }
                    },
                    {
                        targets: 2,
                        render: function (data, type, row) {
                            return row['product_name'];
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, row) {
                            return row['asin'];
                        }
                    },
                    {
                        targets: 4,
                        render: function (data, type, row) {
                            return row['fnsku'];
                        }
                    },
                    {
                        targets: 4,
                        render: function (data, type, row) {
                            return row['user_marketplace_id'];
                        }
                    },
                    {
                        targets: 5,
                        render: function (data, type, row) {
                            return row['return_date'];
                        }
                    },
                    {
                        targets: 6,
                        render: function (data, type, row) {
                            return row['quantity'];
                        }
                    },
                    {
                        targets: 7,
                        render: function (data, type, row) {
                            return row['fulfillment_center_id'];
                        }
                    },
                    {
                        targets: 8,
                        render: function (data, type, row) {
                            return row['detailed_disposition'];
                        }
                    },
                    {
                        targets: 9,
                        render: function (data, type, row) {
                            return row['reason'];
                        }
                    },
                    {
                        targets: 10,
                        render: function (data, type, row) {
                            return row['status'];
                        }
                    },
                    {
                        targets: 11,
                        render: function (data, type, row) {
                            return row['license_plate_number'];
                        }
                    },
                    {
                        targets: 12,
                        render: function (data, type, row) {
                            return row['customer_comments'];
                        }
                    }  
                ]
            });
        }
    </script>
@endsection
@extends('layouts.master')

@section('title') Edit Blackoute Date @endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Edit  Blackoute Date</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                         <li class="breadcrumb-item"><a href="{{ route('blakoutedatesetting.index') }}"> Blackoute Date Setting</a></li>
                        <li class="breadcrumb-item active">Edit Blackoute Date</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">
                <div class="card-body">

                    <form name="main-edit-balckoutdate" id="main-edit-balckoutdate" action="{{ route('blakoutedatesetting.update',$blackout_date_Details['id']) }}" method="POST" onreset="myFunction()">
                        @csrf
                       @method('PUT') 
                        <input type="hidden" name="insert_type" value="edit_form">
                        <input type="hidden" name="country_id" value="{{$country_id}}">

                        <div class="form-group row">
                            <label for="example-tel-input"  class="col-md-2 col-form-label ">Marketplace :</label>
                            <div class="col-md-10">
                                <input  class="form-control" type="text" style="cursor: no-drop;" disabled value="{{$country}} ({{$marketplace}})">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-tel-input"  class="col-md-2 col-form-label ">Blackout Date:</label>
                            <div class="col-md-4 phone">
                                <input  class="form-control min-today" type="date" max="3000-01-01"  name="blackout_date" id="blackout_date" value="{{$blackout_date_Details['blackout_date']}}">
                            </div>
                                 <label for="example-tel-input"  class="col-md-2 col-form-label phone">Reason:</label>
                            <div class="col-md-4 phone">
                                <input  class="form-control " type="text"   name="reason" id="reason" value="{{$blackout_date_Details['reason']}}" >
                            </div>
                        </div>    
                        <div class="button-items mt-3">
                            <input class="btn btn-info" type="submit" value="Submit" id="submit">
                            <a class="btn btn-danger waves-effect waves-light" href="{{ route('blakoutedatesetting.index') }}" role="button">Cancel</a>
                            <input class="btn btn-warning" type="reset" value="Reset">
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->

@endsection

@section('script')
<script>  
    function myFunction() {
      var x = document.getElementById("blackout_date").max;
      document.getElementById().innerHTML = x;
    }

    $(function(){
        $('[type="date"].min-today').prop('min', function(){
            return new Date().toJSON().split('T')[0];
        });
    });
</script>
@endsection
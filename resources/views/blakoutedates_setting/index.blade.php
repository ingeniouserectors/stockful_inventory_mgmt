@extends('layouts.master')

@section('title')  Blackoute Date @endsection

@section('content')

@php(@$userRole = \App\Models\Users::with('user_assigned_role')->select('users.id')->where(array('id' => Auth::user()->id))->first())
@php(@$finalRole = $userRole['user_assigned_role']['user_role_id'])


<style type="text/css">
    .multiselect {
 
}

.selectBox {
  position: relative;
}

.selectBox select {
  width: 100%
  
}

.selectBoxes {
  position: relative;
}

.selectBoxes select {
  width: 100%
  
}

.overSelect {
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
}

#checkboxes {
  display: none;
  border: 1px #dadada solid;
  width: 100%;
}
#checkbox {
  display: none;
  border: 1px #dadada solid;
  width: 100%;
}

#checkboxes label {
  display: block;
}

#checkbox label {
  display: block;
}

#checkboxes label:hover {
  background-color: #1e90ff;
}

#checkbox label:hover {
  background-color: #1e90ff;
}
</style>

    <div class="row">
        <div class="col-12">
  
             <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18"> Blackoute Date</h4>
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active"> Blackoute Date</li>
                    </ol>
                </div>
           </div>
            <!-- </div> -->
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <form action="{{ route('blakoutedatesetting.create') }}">
                @csrf
                <button type="submit" class="btn btn-info waves-effect waves-light mb-3">Create Blackoute Date</button>
                <a class="btn btn-info waves-effect waves-light mb-3"  href="{{ route('blakoutedatesetting.show','1') }}" role="button"><i class="fa fa-download"></i> Download CSV File</a>
            </form>
        </div>
        <div class="col-lg-7">
            <form action="{{ route('blakoutedatesetting.store') }}" method="post" id="import_csv"  enctype="multipart/form-data">
                {{ csrf_field() }}
                <label>Import CSV file : *</label>
                <input type="hidden" name="insert_type" value="upload_csv">
                <input type="file" name="uploadFile" accept=".csv">
                <input type="submit" class="btn btn-primary import_csv" value="Import CSV File">
            </form>
        </div>
    </div>

    <!-- end page title -->
    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <input type="hidden" name="insert_type" value="search_data">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Select Country : </label>
                                <select name="marketplace_country" id="marketplace_country" class="form-control">
                                    @if(!empty($marketplace))
                                        @foreach($marketplace as $list)
                                            <option value="{{$list['id']}}">{{$list['country_code']}}&nbsp;({{$list['country_name']}})</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>                       

    <!-- end row -->
       
    <div class="card marketplace_wise_data">
     
        <div class="card-body">
           @if( $users["user"]['user_roles'][0]['id'] == 3 || $users["user"]['user_roles'][0]['id'] == 5 )
             <form name="create-form" id="create-form" action="{{ route('blakoutedatesetting.store') }}" method="POST">
               <input type="hidden" id="checkalls" name="checkboxes[]" value="">
                <input type="hidden" id="supplier_id" name="supplier_id" value=""> 
                <input type="hidden" id="vendor_id" name="vendor_id" value=""> 
              <input type="hidden" name="insert_type" value="Assign_vendors_suppliers">   
               @csrf   
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Select User type :</label>
                                <div class="col-md-4">
                                    <div class="multiselect">
                                        <div class="selectBox" onclick="showCheckbox()">
                                          <select class="form-control">
                                            <option>Select Vendors</option>
                                          </select>
                                          <div class="overSelect"></div>
                                        </div>
                                        <div id="checkbox">
                                             @foreach($vendors as $vendor)
                                            <input type="checkbox" value="{{$vendor['id']}}"  name="" id="vendors_id" class="vendors_checkbox" />{{$vendor['vendor_name']}}<br>
                                            @endforeach
                                        </div>
                                </div>
                            </div>
                                <div class="col-md-4">                               
                                    <div class="multiselect">
                                        <div class="selectBoxes" onclick="showCheckboxes()">
                                          <select class="form-control">
                                            <option>Select Suppliers</option>
                                          </select>
                                          <div class="overSelect"></div>
                                        </div>
                                        <div id="checkboxes">
                                             @foreach($suppliers as $supplier)
                                            <input type="checkbox" value="{{$supplier['id']}}" id="" class="suppliers_checkbox" />{{$supplier['supplier_name']}}<br>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>                      
                             <span><input class="btn btn-info assign_checkboxess" type="submit" value="Assign" id="submit"></span>
                        </div>
                          
                      </form>
                    @endif
            <h4 class="card-title mb-4"> Blackoute Date List</h4>
             <input type="hidden" value="{{$marketplaces}}" name="marketplaces" id="marketplaces">
             <input type="hidden" value="{{$users['user']['user_roles'][0]['id']}}" name="user_role_id" id="user_role_id">
            <div class="table-responsive">
                <table id="datatables" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead class="thead-light">
                    <tr>
                        @if($users["user"]['user_roles'][0]['id'] == 3 || $users["user"]['user_roles'][0]['id'] == 5)
                        <th><input id="checkall" class='' type="checkbox"></th>
                        @endif
                        <th>No.</th>
                        <th>Blackoute Date</th>
                        <th>Reason</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                   
                </table>
            </div>
        </div>
    </div>

@endsection
@section('script')

<script>




 @if(@$finalRole == 3 || @$finalRole == 5 ) 
  $(document).ready(function () {
            var id = $('#marketplaces').val();
                     loadDashboard(id);
        });

        function loadDashboard(id){
           var blackout_date_list='blackout_date_list';
            var count=1;
            var table = $('#datatables').DataTable({

                "order": [ 0, 'asc' ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('blakoutedatesetting.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.id=id,d.request_type=blackout_date_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                success:function($res){
                    console.log(res);
                    //return false;
                },
                'columnDefs': [
                  
                     {
                        targets: 0,
                        render: function (data, type, row) {
                            return row['checkboxes'];
                        }
                    },
                     {
                        targets: 1,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 2,
                        render: function (data, type, row) {
                            return  row['blackout_date'];
                        }
                    },
                    {
                         targets: 3,
                        render: function (data, type, row) {
                            return row['reason'];
                        }
                    },
                    {
                         targets: 4,
                        render: function (data, type, row) {
                            return row['action'];
                        }
                    }
                    
                    
                ]
            });
        }

     @endif



 @if(@$finalRole == 1 || @$finalRole == 2)
        $(document).ready(function () {
           
                     loadDashboard();
        });
 
        function loadDashboard(){
           var blackout_date_list='blackout_date_list';
            var count=1;
            var table = $('#datatables').DataTable({

                "order": [ 0, 'asc' ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('blakoutedatesetting.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.request_type=blackout_date_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                success:function($res){
                    console.log(res);
                    //return false;
                },
                'columnDefs': [
                  
                    
                     {
                        targets: 0,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return  row['blackout_date'];
                        }
                    },
                    {
                         targets: 2,
                        render: function (data, type, row) {
                            return row['reason'];
                        }
                    },
                    {
                         targets: 3,
                        render: function (data, type, row) {
                            return row['action'];
                        }
                    }
                    
                    
                ]
            });
        }

        @endif
</script>

    <script>
        $(document).on('change','#marketplace_country',function () {
            $.ajax({
                type: "POST",
                url: '{{ route('blakoutedatesetting.store') }}',
                data: {
                    insert_type:"show_marketplace_wise_data",
                    id : $(this).val(),
                },
                success: function (response) {
                    if(response.error == 0){
                        $('.marketplace_wise_data').html(response.view);
                        $('#datatable').DataTable({});
                    }else{
                        alert('Something wrong');
                    }
                }
            });
        })
    </script>
 <script>
  
$(document).on('click','#checkall',function(){
    if ($("#checkall").is(':checked')){
        var data = [];
        $(".checkboxes").each(function (){
            $(this).prop("checked", true);
            data.push($(this).val());
        });
        $('#checkalls').val(data);
    }
    else{
        $(".checkboxes").each(function (){
            var data = [];
            $(this).prop("checked", false);
            data.pop($(this).val());
            $('#checkalls').val(data);
        });
    }
});
$(document).ready(function(){
    var data = [];
    $('.checkboxes').click(function(){
       if ($(".checkboxes").is(':checked')){  
       data.push($(this).val());
       $('#checkalls').val(data);
   }
   else
   {
        data.pop($(this).val());
        $(this).prop("checked", false);
        $('#checkalls').val(data);
   }
    });
});

var expanded = false;
function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}

var result = false;
function showCheckbox() {
  var checkbox = document.getElementById("checkbox");
  if (!result) {
    checkbox.style.display = "block";
    result = true;
  } else {
    checkbox.style.display = "none";
    result = false;
  }
}

$(document).ready(function(){
  
    var data = [];
    $('.suppliers_checkbox').click(function(){
       if ($(".suppliers_checkbox").is(':checked')){  
       data.push($(this).val());
       $('#supplier_id').val(data);
   }
   else
   {
        data.pop($(this).val());
        $(this).prop("checked", false);
        $('#supplier_id').val(data);
   }
    });
});

$(document).ready(function(){
    var data = [];
    $('.vendors_checkbox').click(function(){
       if ($(".vendors_checkbox").is(':checked')){  
       data.push($(this).val());
       $('#vendor_id').val(data);
   }
   else
   {
        data.pop($(this).val());
        $(this).prop("checked", false);
        $('#vendor_id').val(data);
   }
    });
});

$(document).on('click','.assign_checkboxess',function () {
    $('#vendor_id').val($('.vendors_checkbox:checked').map(function() {return this.value;}).get().join(','));
    $('#supplier_id').val($('.suppliers_checkbox:checked').map(function() {return this.value;}).get().join(','));
    $('#checkalls').val($('.checkboxes:checked').map(function() {return this.value;}).get().join(','));
});



$(document).on('click', '#button', function () {
    var $ele = $(this).parent().parent();
    var id = $(this).data('id');
            var url = "{{URL('blakoutedatesetting')}}";
            var destroyurl = url+"/"+id;


    swal({
             title: "Are you sure?",
            text: "You want to delete this record",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
             
            $.ajax({
                type: "DELETE",
                url:destroyurl, 
                data:{ _token:'{{ csrf_token() }}'},
                dataType: "html",
                success: function (data) {
                    var dataResult = JSON.parse(data);
                if(dataResult.statusCode==200){
                    $ele.fadeOut().remove();
                               swal({
              title: "Done!",
              text: "It was succesfully deleted!",
              type: "success",
              timer: 700
           });
    
                      
                    } 
        }

         });
          }  
    
        else
        {
             swal("Cancelled", "", "error");
        }
            
       
    });

});

    </script>

    

  

@endsection
@extends('layouts.master')

@section('title') Create Blackoute Date @endsection

@section('content')


    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Create  Blackoute Date</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                         <li class="breadcrumb-item"><a href="{{ route('blakoutedatesetting.index') }}"> Blackoute Date Setting</a></li>
                        <li class="breadcrumb-item active">Create Blackoute Date</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">
                <div class="card-body">

                    <form id="main-settings-blackout-create" name="create-user" action="{{ route('blakoutedatesetting.store') }}" method="POST" onreset="myFunction()">
                        @csrf

                        <input type="hidden" name="insert_type" value="create_form">

                        <div class="form-group row">
                            <label for="example-tel-input"  class="col-md-2 col-form-label">Country:</label>
                            <div class="col-md-10">
                                <select class="form-control" name="country_id" required>
                                        <?php foreach ($countryDetails as $countryDetails) { ?>
                                        <option value="<?php echo $countryDetails['id'] ?>"><?php echo $countryDetails['country_name'] ?> ({{$countryDetails['country_code']}})</option>
                                        <?php } ?>
                                    </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Select User Type</label>
                            <div class="col-md-4">
                                <div class="custom-control custom-radio mb-3">
                                    <input type="radio" id="single" name="customRadio" class="custom-control-input form-control" value="single" checked>
                                    <label class="custom-control-label" for="single">Single</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="daterange" name="customRadio" class="custom-control-input form-control" value="daterange">
                                    <label class="custom-control-label" for="daterange">Date range</label>
                                </div>
                            </div>
                        </div>

                        
                     <div class="single_form">
                        <div class="form-group row">
                            <label for="example-tel-input"  class="col-md-2 col-form-label">Blackout Date:</label>
                            <div class="col-md-4">
                                <input  class="form-control min-today" type="date" max="3000-01-01"  name="blackout_date_single" id="blackout_date_single" >
                            </div>
                                 <label for="example-tel-input"  class="col-md-2 col-form-label">Reason:</label>
                            <div class="col-md-4">
                                <input  class="form-control" type="text" name="reason_single" id="reason_single" >
                            </div>
                        </div>
                        
                    </div>
                     <div class="datarange_form">

                        <div class="form-group row">
                            <label for="example-tel-input"  class="col-md-2 col-form-label ">Blackout Date:</label>
                            <div class="col-md-4 phone">
                                From<input  class="form-control min-today" type="date" max="3000-01-01"  name="blackout_date_multiple_start" id="blackout_date_multiple_start" >
                                 
                            </div>
                            <div class="col-md-4 phone">
                                To<input  class="form-control min-today" type="date" max="3000-01-01"  name="blackout_date_multiple_end" id="blackout_date_multiple_end" >
                                 
                            </div>
                            </div>
                            <div class="form-group row">
                                 <label for="example-tel-input"  class="col-md-2  col-md-1form-label phone">Reason:</label>
                                  <div class="col-md-4 phone">
                                     <input  class="form-control " type="text"   name="reason_multiple" id="reason_multiple" >
                                    </div>
                            </div>
                        
                     </div>    
                        <div class="button-items mt-3">
                            <div class="single_button">
                            <input class="btn btn-info single" type="submit" value="Submit" id="submit">
                            <a class="btn btn-danger waves-effect waves-light" href="{{ route('blakoutedatesetting.index') }}" role="button">Cancel</a>
                            <input class="btn btn-warning" type="reset" value="Reset">
                        </div>
                    </div>
                    <div class="button-items mt-3">
                            <div class="daterange_button">
                            <input class="btn btn-info daterange" type="submit" value="Submit" id="submit">
                            <a class="btn btn-danger waves-effect waves-light" href="{{ route('blakoutedatesetting.index') }}" role="button">Cancel</a>
                            <input class="btn btn-warning" type="reset" value="Reset">
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div>

    <!-- end row -->

@endsection

@section('script')

    <script>
        $(document).ready(function(){
            var valuedata = $('input[name="customRadio"]:checked').val();
            if(valuedata == 'single'){
                $('.datarange_form').hide();
                $('.single_form').show();
                $('.daterange_button').hide();
                $('.single_button').show();
            }else{
                $('.single_form').hide();
                $('.daterange_form').show();
                $('.daterange_button').show();
                $('.single_button').hide();
            }
        });
        $(document).on('click','input[name="customRadio"]',function(){
            var valuedata = $(this).val();
            if(valuedata == 'daterange'){
                $('.single_form').hide();
                $('.datarange_form').show();
                $('.single_button').hide();
                $('.daterange_button').show();
            }else{
                $('.datarange_form').hide();
                $('.single_form').show();
                $('.single_button').show();
                $('.daterange_button').hide();
            }
        });
   
function myFunction() {
  var x = document.getElementById("blackout_date").max;
  document.getElementById().innerHTML = x;
}

$(function(){
    $('[type="date"].min-today').prop('min', function(){
        return new Date().toJSON().split('T')[0];
    });
});

    </script>

  



@endsection
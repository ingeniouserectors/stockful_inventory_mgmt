
    <div class="card-body">
        @if(Auth::user()->id == 3 || $users_role_id["user"]['user_roles'][0]['id'] == 3 || $users_role_id["user"]['user_roles'][0]['id'] == 5 )
             <form name="create-form" id="create-form" action="{{ route('blakoutedatesetting.store') }}" method="POST">
               <input type="hidden" id="checkalls" name="checkboxes[]" value="">
                <input type="hidden" id="supplier_id" name="supplier_id" value=""> 
                <input type="hidden" id="vendor_id" name="vendor_id" value=""> 
              <input type="hidden" name="insert_type" value="Assign_vendors_suppliers">   
               @csrf   
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Select User type :</label>
                                <div class="col-md-4">
                                    <div class="multiselect">
                                        <div class="selectBox" onclick="showCheckbox()">
                                          <select class="form-control">
                                            <option>Select Vendors</option>
                                          </select>
                                          <div class="overSelect"></div>
                                        </div>
                                        <div id="checkbox">
                                             @foreach($vendors_list as $vendor)
                                            <input type="checkbox" value="{{$vendor['id']}}"  name="vendors_id" id="vendors_id" class="vendors_checkbox" />{{$vendor['vendor_name']}}<br>
                                            @endforeach
                                        </div>
                                </div>
                            </div>
                                <div class="col-md-4">                               
                                    <div class="multiselect">
                                        <div class="selectBoxes" onclick="showCheckboxes()">
                                          <select class="form-control">
                                            <option>Select Suppliers</option>
                                          </select>
                                          <div class="overSelect"></div>
                                        </div>
                                        <div id="checkboxes">
                                             @foreach($suppliers_list as $supplier)
                                            <input type="checkbox" value="{{$supplier['id']}}" id="suppliers_id" class="suppliers_checkbox" />{{$supplier['supplier_name']}}<br>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>                      
                             <span><input class="btn btn-info assign_checkboxess" type="submit" value="Assign" id="submit"></span>
                        </div>
                          
                      </form>
                    @endif
         <div class="card marketplace_wise_data">           
        <h4 class="card-title mb-4"> Blackoute Date List</h4>
        <div class="table-responsive">
            <table id="datatables" class="table table-striped table-bordered dt-responsive nowrap users-datatable" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                <thead class="thead-light">
                <tr>
                    @if(Auth::user()->id == 3 || $users_role_id["user"]['user_roles'][0]['id'] == 3 || $users_role_id["user"]['user_roles'][0]['id'] == 5)
                    <th><input id="checkall" class='' type="checkbox"></th>
                    @endif
                    <th>No.</th>
                    <th>Blackoute Date</th>
                    <th>Reason</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $no = 1; ?>
                @if(!empty($blackout_date_Details))
                    @foreach($blackout_date_Details as $users)

                        <tr>
                             @if(Auth::user()->id == 3 || $users_role_id["user"]['user_roles'][0]['id'] == 3 || $users_role_id["user"]['user_roles'][0]['id'] == 5)
                            <td><input type="checkbox" name="checkalls[]" class='checkboxes' value="{{$users['id']}}" id="checkalls"  /></td>
                            @endif
                            <td>{{$no++}}</td>
                            <td>{{$users['blackout_date']}}</td>
                            <td>{{$users['reason']}}</td>
                            <td>
                                <a class="btn btn-primary btn-sm btn-rounded waves-effect waves-light"  href="{{ route('blakoutedatesetting.edit',$users['id']) }}" role="button"><i class="fas fa-edit"></i></a>
                                
                                    <button type="submit" id="button" data-id="{{$users['id']}}" class="btn btn-danger btn-sm btn-rounded waves-effect waves-light"><i class="fas fa-trash-alt"></i></button>     

                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr><td colspan="10" align="center">No record found.</td></tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

@section('script')
<script>

$(document).on('click', '#button', function () {
    var $ele = $(this).parent().parent();
    var id = $(this).data('id');
    alert(id);
            var url = "{{URL('blakoutedatesetting')}}";
            var destroyurl = url+"/"+id;


    swal({
             title: "Are you sure?",
            text: "You want to delete this record",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
             
            $.ajax({
                type: "DELETE",
                url:destroyurl, 
                data:{ _token:'{{ csrf_token() }}'},
                dataType: "html",
                success: function (data) {
                    var dataResult = JSON.parse(data);
                if(dataResult.statusCode==200){
                    $ele.fadeOut().remove();
                               swal({
              title: "Done!",
              text: "It was succesfully deleted!",
              type: "success",
              timer: 700
           });
    
                      
                    } 
        }

         });
          }  
    
        else
        {
             swal("Cancelled", "", "error");
        }
            
       
    });

});

    </script>

@endsection



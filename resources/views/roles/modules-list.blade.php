@extends('layouts.master')

@section('title') Module List @endsection

@section('content')

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Modules</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('roles.index') }}">Role</a></li>
                            <li class="breadcrumb-item active">{{ ucfirst(@$rolesName) }} Modules</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-lg-12">
                @if(session()->has('message'))
                 {!! session('message') !!}
                @endif
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-4">{{ ucfirst(@$rolesName) }} Module List</h4>
                        <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead class="thead-light">
                                    <tr>
                                        <th>No.</th>
                                        <th>Modules</th>
                                        <th>Create</th>
                                        <th>Edit</th>
                                        <th>View</th>
                                        <th>Delete</th>
                                        <th>Access</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	 <?php $no = 1; ?>
                                @if(!empty($module_details))
                                    @foreach($module_details as $modules)
                                        <tr>
                                            <td>{{ $no++ }}</td>

                                            @if($modules['application_modules']['table_name'] != '')
                                                <td>{{ $modules['application_modules']['module'] }}</td>
                                            @else
                                                <td>{{ $modules['application_modules']['module'] }}</td>
                                            @endif


                                            <td>
                                                <div>
                                                    <input type="checkbox" id="switch-create-{{ $modules['id'] }}" value="{{ $modules['create'] }}" switch="success" @if($modules['create'] == 1) checked @endif onclick="updateUserAccessModules('create',{{ $modules['create'] }},{{ $modules['id'] }},{{  $modules['role_id'] }},{{$modules['application_module_id']}})"/>
                                                    <label for="switch-create-{{ $modules['id'] }}" data-on-label="Yes" data-off-label="No"></label>
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    <input type="checkbox" id="switch-edit-{{ $modules['id'] }}" value="{{ $modules['edit'] }}" switch="primary" @if($modules['edit'] == 1) checked @endif onclick="updateUserAccessModules('edit',{{ $modules['edit'] }},{{ $modules['id'] }},{{  $modules['role_id'] }},{{$modules['application_module_id']}})"/>
                                                    <label for="switch-edit-{{ $modules['id'] }}" data-on-label="Yes" data-off-label="No"></label>
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    <input type="checkbox" id="switch-view-{{ $modules['id'] }}" value="{{ $modules['view'] }}" switch="info" @if($modules['view'] == 1) checked @endif onclick="updateUserAccessModules('view',{{ $modules['view'] }},{{ $modules['id'] }},{{  $modules['role_id'] }},{{$modules['application_module_id']}})"/>
                                                    <label for="switch-view-{{ $modules['id'] }}" data-on-label="Yes" data-off-label="No"></label>
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    <input type="checkbox" id="switch-delete-{{ $modules['id'] }}" value="{{ $modules['delete'] }}" switch="danger" @if($modules['delete'] == 1) checked @endif onclick="updateUserAccessModules('delete',{{ $modules['delete'] }},{{ $modules['id'] }},{{  $modules['role_id'] }},{{$modules['application_module_id']}})"/>
                                                    <label for="switch-delete-{{ $modules['id'] }}" data-on-label="Yes" data-off-label="No"></label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="square-switch">
                                                    <input type="checkbox" id="square-access-{{ $modules['id'] }}" value="{{ $modules['access'] }}"  switch="bool" @if($modules['access'] == 1) checked @endif onclick="updateUserAccessModules('access',{{ $modules['access'] }},{{ $modules['id'] }},{{  $modules['role_id'] }},{{$modules['application_module_id']}})"/>
                                                    <label for="square-access-{{ $modules['id'] }}" data-on-label="Yes" data-off-label="No"></label>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- end table-responsive -->
                    </div>
                </div>
            </div>
        </div>
@endsection	

@section('script')
        <!-- plugin js -->
        <script src="{{ URL::asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>

        <!-- Calendar init -->
        <script src="{{ URL::asset('assets/js/pages/dashboard.init.js')}}"></script>
        <script>
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            function updateUserAccessModules(type,type_value,id,role_id,application_module_id) {
                var dataroletype = 'rolewiseuseraccessmodules';
                $.ajax({
                    type: "POST",
                    url: '{{ route('roles.store') }}',
                    data: {
                        type: type,
                        type_value : type_value,
                        id : id,
                        role_id : role_id,
                        dataroletype : dataroletype,
                        application_module_id:application_module_id
                    },
                    success: function (response) {
                        console.log(response);
                        return false;
                    }
                });
            }
        </script>
@endsection
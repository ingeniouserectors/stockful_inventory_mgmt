@extends('layouts.master')

@section('title') {{$table_name}} @endsection

@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">{{$table_name}}</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">{{$table_name}} Fields</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">{{$table_name}} fields list</h4>
                    <div class="table-responsive">
                        <table id="datatables" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead class="thead-light">
                            <tr>
                                <th>No.</th>
                                <th>Original Fields</th>
                                <th>Display Name</th>
                                <th>Foreign key (optional)</th>
                                <th>Reference table name (optional)</th>
                                <th>Reference table field (optional)</th>
                            </tr>
                            </thead>
                            <form name="create-module" id="create-module" action="{{ route('moduletable.store') }}" method="POST">
                                <tbody>
                                @csrf
                                @if(!empty($table_column))
                                    @php($i = 0)
                                    @foreach($table_column as $key=>$value)
                                        @php($get_data = table_module_data($module_id,$value))


                                        @if($value != 'id' && $value != 'user_marketplace_id' && $value != 'created_at' && $value != 'updated_at' && $value != 'deleted_at')
                                            @php($display_style = '')
                                            @php($i++)
                                        @else
                                            @php($display_style = 'style=display:none;')
                                        @endif
                                        <input type="hidden" name="module_id" value="{{$module_id}}">
                                        <tr {{$display_style}}>
                                            <input type="hidden" name="original_name[]" value="{{$value}}">
                                            <td>{{ $i  }}</td>
                                            <td>{{$value}}</td>
                                            <td><input type="text" class="form-control" name="display_name[]" value="{{@$get_data->display_name}}" placeholder="Display Name" required></td>
                                            <td><input type="text" class="form-control" name="foreign_key[]"value="{{@$get_data->field_type}}" placeholder="Foreign key"></td>
                                            <td><input type="text" class="form-control" name="ref_table_name[]"value="{{@$get_data->referance_table}}" placeholder="Reference table name"></td>
                                            <td><input type="text" class="form-control" name="ref_table_field[]"value="{{@$get_data->referance_table_field}}" placeholder="Reference table field"></td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <input type="submit" class="btn btn-info waves-effect waves-light" value="Submit">
                                        <a href="{{route('roles.index')}}" type="button" class="btn btn-basic waves-effect waves-light">Cancel</a>
                                    </td>
                                </tr>
                                </tfoot>
                            </form>
                        </table>

                        <form name="create-module" id="create-module" action="{{ route('moduletable.store') }}" method="POST">
                        <table id="datatabless" class="table table-striped table-bordered dt-responsive nowrap add_join_table" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead class="thead-light">
                            <tr>

                                <th>Table Name</th>
                                <th>Display Name</th>
                                <th>Display Fileds</th>
                                <th>join with field</th>
                                <th>Add More Table +</th>
                            </tr>
                            </thead>
                                <input type="hidden" name="module_id" value="{{$module_id}}">
                                <input type="hidden" name="insert_type" value="fields_management">
                                <tbody>
                                @csrf
                                    @if(!empty($get_joins_table))
                                        @foreach($get_joins_table as $join)
                                            <tr class="add_join_u{{$join['id']}}">
                                                <td>
                                                    @if(!empty($match_table))
                                                        <select id="table_name_u{{$join['id']}}" name="table_class[]" data-val="u{{$join['id']}}" class="form-control select_table">
                                                            <option value="">Select Table</option>
                                                            @foreach($match_table as $table)
                                                                <option value="{{$table}}" {{$table == $join['table_name'] ? 'selected' : ''}}>{{$table}}</option>
                                                            @endforeach
                                                        </select>
                                                    @endif
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" id="display_name_u{{$join['id']}}" value="{{$join['display_name']}}" name="display_name[]" placeholder="Display Name">
                                                </td>

                                                <input type="hidden" id="display_edit_fields_u{{$join['id']}}" value="{{$join['display_field']}}">
                                                <input type="hidden" id="edit_join_with_fields_u{{$join['id']}}" value="{{$join['join_with']}}">

                                                <td>
                                                    <select class="form-control" id="display_fields_u{{$join['id']}}" name="display_fields[]">
                                                        <option value="">Select Fileds</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control" id="join_with_u{{$join['id']}}" name="join_with_fields[]">
                                                        <option value="">Join with fields</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <a href="javascript:void(0);" class="btn btn-danger btn-xs form-control remove_joining_update" data-val="{{$join['id']}}">X</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                <tr class="add_join_0">
                                    <td>
                                        @if(!empty($match_table))
                                            <select id="table_name_0" name="table_class[]" data-val="0" class="form-control select_table">
                                                <option value="">Select Table</option>
                                                @foreach($match_table as $table)
                                                    <option value="{{$table}}">{{$table}}</option>
                                                @endforeach
                                            </select>
                                        @endif
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" id="display_name_0" name="display_name[]" placeholder="Display Name">
                                    </td>
                                    <td>
                                        <select class="form-control" id="display_fields_0" name="display_fields[]">
                                            <option value="">Select Fileds</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control" id="join_with_0" name="join_with_fields[]">
                                            <option value="">Join with fields</option>
                                        </select>
                                    </td>
                                    <td>
                                        <a href="javascript:void(0);" class="btn btn-success btn-xs form-control add_new_joining">Add +</a>
                                    </td>
                                </tr>

                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <input type="submit" class="btn btn-info waves-effect waves-light" value="Submit">
                                        <a href="{{route('roles.index')}}" type="button" class="btn btn-basic waves-effect waves-light">Cancel</a>
                                    </td>
                                </tr>
                                </tfoot>
                        </table>
                        </form>
                    </div>
                    <!-- end table-responsive -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- plugin js -->
    <script src="{{ URL::asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>

    <!-- Calendar init -->
    <script src="{{ URL::asset('assets/js/pages/dashboard.init.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        function updateUserAccessModules(type,type_value,id,role_id,application_module_id) {
            var dataroletype = 'rolewiseuseraccessmodules';
            $.ajax({
                type: "POST",
                url: '{{ route('roles.store') }}',
                data: {
                    type: type,
                    type_value : type_value,
                    id : id,
                    role_id : role_id,
                    dataroletype : dataroletype,
                    application_module_id:application_module_id
                },
                success: function (response) {
                    console.log(response);
                    return false;
                }
            });
        }

        $(document).ready(function(){


                $( ".select_table" ).each(function() {
                    var value = $( this ).data( "val" );
                    var display_fields = $("#display_edit_fields_"+value).val();
                    var join_fields = $("#edit_join_with_fields_"+value).val();
                    if(value != 0){
                        var table = $(this).val();
                        var insert_type = 'get_table_fileds';
                        $.ajax({
                            type: "POST",
                            url: '{{ route('moduletable.store') }}',
                            data: {
                                insert_type: insert_type,
                                table : table,
                                display_fields : display_fields,
                                join_fields : join_fields
                            },
                            success: function (response) {
                                $("#display_fields_"+value).html(response);
                                $("#join_with_"+value).html(response);

                                $('#display_fields_'+value+' option').filter(function() {
                                    return ($(this).text() == display_fields); //To select Blue
                                }).prop('selected', true);

                                $('#join_with_'+value+' option').filter(function() {
                                    return ($(this).text() == join_fields); //To select Blue
                                }).prop('selected', true);
                            }
                        });
                    }
                });

            {{--$.each(table,function(index,value){--}}
                    {{--console.log(value);--}}
                    {{--return false;--}}
                {{--});--}}
                {{--console.log(table);--}}
                {{--return false;--}}

                {{--var data_id = $('.select_table').data('val');--}}
                {{--var insert_type = 'get_table_fileds';--}}
                {{--$.ajax({--}}
                    {{--type: "POST",--}}
                    {{--url: '{{ route('moduletable.store') }}',--}}
                    {{--data: {--}}
                        {{--insert_type: insert_type,--}}
                        {{--table : table,--}}
                    {{--},--}}
                    {{--success: function (response) {--}}
                        {{--$("#display_fields_"+data_id).html(response);--}}
                        {{--$("#join_with_"+data_id).html(response);--}}
                    {{--}--}}
                {{--});--}}
        });

        $(document).on('change','.select_table',function(){
            var table = $(this).val();
            var data_id = $(this).data('val');
            var insert_type = 'get_table_fileds';
           $.ajax({
               type: "POST",
               url: '{{ route('moduletable.store') }}',
               data: {
                   insert_type: insert_type,
                   table : table,
               },
               success: function (response) {
                   $("#display_fields_"+data_id).html(response);
                   $("#join_with_"+data_id).html(response);
               }
           });
        });


        var numberIncr = 1;
        $(document).on('click',".add_new_joining",function(){
            $('.add_join_table').append($('<tr class="add_join_'+numberIncr+'">'+
                '<td>'+
                    @if(!empty($match_table))
                        '<select id="table_name_'+numberIncr+'" name="table_class['+numberIncr+']" data-val="'+numberIncr+'" class="form-control select_table">'+
                        '<option value="">Select Table</option>'+
                    @foreach($match_table as $table)
                        '<option value="{{$table}}">{{$table}}</option>'+
                    @endforeach
                        '</select>'+
                    @endif
                        '</td>'+
            '<td>'+
            '<input type="text" class="form-control" name="display_name[]" id="display_name_'+numberIncr+'" placeholder="Display Name">'+
            '</td>'+
            '<td>'+
            '<select class="form-control" id="display_fields_'+numberIncr+'" name="display_fields[]">'+
            '<option value="">Select Fileds</option>'+
            '</select>'+
            '</td>'+
            '<td>'+
            '<select class="form-control" id="join_with_'+numberIncr+'" name="join_with_fields[]">'+
            '<option value="">Join with fields</option>'+
            '</select>'+
            '</td>'+
            '<td>'+
            '<a href="javascript:void(0);"  data-val="'+numberIncr+'" class="btn btn-danger btn-xs form-control remove_new_joining">X</a>'+
            '</td>'+
            '</tr>'));
            numberIncr++;
        });

        $(document).on('click','.remove_new_joining',function(){
            var data_id = $(this).data('val');
            $(".add_join_"+data_id).remove();
        });

        $(document).on('click','.remove_joining_update',function(){
            var data_id = $(this).data('val');
            var insert_type = 'remove_joining_update';
            $.ajax({
                type: "POST",
                url: '{{ route('moduletable.store') }}',
                data: {
                    insert_type: insert_type,
                    data_id : data_id,
                },
                success: function (response) {
                    location.reload();
                    //$("#display_fields_"+data_id).html(response);
                    //$("#join_with_"+data_id).html(response);
                }
            });
        });

    </script>
@endsection
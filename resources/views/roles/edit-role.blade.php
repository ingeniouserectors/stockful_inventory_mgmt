@extends('layouts.master')

@section('title') Edit Role @endsection

@section('content')

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Edit Role</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('roles.index') }}">Role</a></li>
                            <li class="breadcrumb-item active">Edit Role</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <form name="edit-role" id="edit-role" action="{{ route('roles.update',$roleDetails['id']) }}" method="POST" onreset="myFunction()">
                            @csrf
                            @method('PUT')

                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Role</label>
                                <div class="col-md-4">
                                    <input class="form-control" type="text" value="{{ $roleDetails['role'] }}" name="role" id="role">
                                    @if ($errors->has('role'))
                                        <span class="help-block alert-danger"><strong>{{ $errors->first('role') }}</strong></span>
                                    @endif
                                </div>
                            </div>

                            <div class="button-items mt-3">
                            <input class="btn btn-info" type="submit" value="Submit">
                            <a class="btn btn-danger waves-effect waves-light" href="{{ route('roles.index') }}" role="button">Cancel</a>
                            <input class="btn btn-warning" type="reset" value="Reset">
                        </div>

                        </form>
                    </div>
                </div>
            </div> <!-- end col -->
        </div>
        <!-- end row -->

@endsection
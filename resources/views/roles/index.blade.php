@extends('layouts.master')

@section('title') Roles List @endsection

@section('content')

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Roles</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Roles</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->
        @php($get_usercheck_access = get_user_check_access('role_module', 'create'))
        @if($get_usercheck_access == 1)
            <div class="row">
                <div class="col-lg-6">
                    <a class="btn btn-info waves-effect waves-light mb-3"  href="{{ route('roles.create') }}" role="button"> Create New Role</a>
                </div>
                <div class="col-lg-6"></div>
            </div>
        @else
            @can('create', $userpermission)
            <div class="row">
                <div class="col-lg-6">
                    <a class="btn btn-info waves-effect waves-light mb-3"  href="{{ route('roles.create') }}" role="button"> Create New Role</a>
                </div>
                <div class="col-lg-6"></div>
            </div>
            @endcan
        @endif
        <div class="row">
            <div class="col-lg-12">
                 @if(session()->has('message'))
                  {!! session('message') !!}
                 @endif
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-4">Role List</h4>
                        <input type="hidden" value="{{$userpermission}}" id=>
                        <div class="table-responsive">
                                <table id="datatables" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead class="thead-light">
                                    <tr>
                                        <th>No.</th>
                                        <th>Role</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                           </table>
                        </div>
                        <!-- end table-responsive -->
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->

@endsection
@section('script')

<script>
    $(document).ready(function () {
                     loadDashboard();

        });
        function loadDashboard(){
           var user_role_list='user_role_list';
            var count=1;
            var table = $('#datatables').DataTable({

                "order": [ 0, 'asc' ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": '{{ route('roles.store') }}',
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.request_type=user_role_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                success:function($res){
                    console.log(res);
                    return false;
                },
                'columnDefs': [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return  row['role'];
                        }
                    },
                    {
                        targets: 2,
                        render: function (data, type, row) {
                            return  row['action'];
                        }
                    }

                    
                ]
            });
        }
</script>        

@endsection

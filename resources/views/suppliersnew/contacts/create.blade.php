@extends('layouts.master')

@section('title') Create Suppliers Contact @endsection

@section('content')

<style type="text/css">
    .error
    {
        color:red;
    }
</style>
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Create Suppliers Contact</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('suppliers.index') }}">suppliers</a></li>
                        <li class="breadcrumb-item"><a href="{{route('supplierscontact.show',$supplier_id)}}">Suppliers Contact</a></li>
                        <li class="breadcrumb-item active">Create Suppliers Contact</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">
                <div class="card-body">

                    <form name="create-supplier-contact" id="create-supplier-contact" action="{{ route('supplierscontact.store') }}" method="POST" onreset="myFunction()">
                        @csrf

                        <input type="hidden" name="insert_type" value="create_form">
                        <input type="hidden" name="supplier_id" value="{{$supplier_id}}">

                        <div class="form-group row">
                            <label for="example-tel-input"  class="col-md-2 col-form-label phone">Fistname</label>
                            <div class="col-md-4 phone">
                                <input  class="form-control" type="text" name="firstname[]" id="firstname" >
                            </div>

                            <label for="example-text-input" class="col-md-2 col-form-label state">Lastname</label>
                            <div class="col-md-4 state">
                                <input class="form-control" type="text" name="lastname[]" id="lastname" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-tel-input"  class="col-md-2 col-form-label phone">Email</label>
                            <div class="col-md-4 phone">
                                <input  class="form-control" type="email" name="email[]" id="email" >
                            </div>
                        </div>

                        <div id="basic_primary_details">
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-4 phone">
                            </div>
                            <div class="col-md-4 phone">
                                <a  class=" btn btn-success" type="submit"  name="add_primary" id="add_primary" style="color:white;">Add New  Details + </a>
                            </div>
                            <div class="col-md-4 phone">
                            </div>
                        </div>
                        <div class="button-items mt-3">
                            <input class="btn btn-info" type="submit" value="Submit" id="submit">
                            <a class="btn btn-danger waves-effect waves-light" href="{{ route('suppliers.index') }}" role="button">Cancel</a>
                            <input class="btn btn-warning" type="reset" value="Reset">
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->

@endsection

@section('script')
    <script>
            $(document).ready(function () {
                var numberIncr = 1;
                $("#add_primary").on('click', function () {
                    $('#basic_primary_details').append($('<div id="rowsdata'+numberIncr+'" class="dynamic-added"><hr><b> Other Primary Details : </b><br/>'+
                            '<div class="form-group row">'+
                            '<label for="example-tel-input"  class="col-md-2 col-form-label phone">Fistname</label>'+
                            '<div class="col-md-4 phone">'+
                            '<input  class="form-control" type="text"name="firstname[' + numberIncr + ']"data-rule-required="true" data-msg-required="Please enter firstname" >'+
                            '</div>'+

                            '<label for="example-text-input" class="col-md-2 col-form-label state">Lastname</label>'+
                            '<div class="col-md-4 state">'+
                            '<input class="form-control" type="text" name="lastname[' + numberIncr + ']" data-rule-required="true" data-msg-required="Please enter lastname">'+
                            '</div>'+
                            '</div>'+ 

                            '<div class="form-group row">'+
                            '<label for="example-tel-input"  class="col-md-2 col-form-label phone">Email</label>'+
                            '<div class="col-md-4 phone">'+
                            '<input type="email" class="comment required form-control" name="email[' + numberIncr + ']" data-rule-required="true" data-msg-required="Please enter email" />'+ 
                            '</div>'+
                            '<div class="col-md-4 phone">'+
                            ' <input class="btn btn-danger" type="submit" style="float: right;" value="Remove  Details &times;" id="remove_primary" data-id="'+numberIncr+'" name="remove_primary[' + numberIncr + ']">'+
                            '</div>'+
                            '</div>'
                            ));
                    numberIncr++;
                });

               
            });

            $(document).on('click','#remove_primary',function(){
                var remove_id = $(this).data('id');
                $('#rowsdata'+remove_id+'').remove();
            });
    </script>

@endsection
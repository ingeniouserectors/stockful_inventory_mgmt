@extends('layouts.master-new')

@section('title') Supplier Details @endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">

            </div>
        </div>
        @if(session()->has('message2') || session()->has('message3') || session()->has('message4'))
            <div class="col-lg-12">
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    @if(session()->has('message2'))
                        {!! session('message2') !!}
                    @endif
                    <br>
                    @if(session()->has('message3'))
                        {!! session('message3') !!}
                    @endif
                    <br>
                    @if(session()->has('message4'))
                        {!! session('message4') !!}
                    @endif
                </div>
            </div>
        @endif
    </div>
    <!-- end row -->
    <div class="suppliers-top-main-box">
        <div class="row">
            <div class="col-md-4">
                <div class="suppliers-first-column">
                    <h2>Suppliers Details</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="suppliers-main-box suppliers-main-box-scroll">
        <div class="row">
            <div class="col-lg-12">
                <nav class="suppliers-nav-box">
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home"
                           role="tab" aria-controls="nav-home" aria-selected="true">Basic Details</a>
                        <a class="nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile"
                           role="tab" aria-controls="nav-profile" aria-selected="false">Contacts</a>
                        <!-- <a class="nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact"
                            role="tab" aria-controls="nav-contact" aria-selected="false">Shipping
                            Agent</a> -->
                        <a class="nav-link" id="nav-setting-tab" data-toggle="tab" href="#nav-setting"
                           role="tab" aria-controls="nav-setting" aria-selected="false">Settings</a>
                        <!-- <a class="nav-link" id="nav-load-time-tab" data-toggle="tab"
                            href="#nav-load-time" role="tab" aria-controls="nav-load-time"
                            aria-selected="false">Lead Time</a>
                        <a class="nav-link" id="nav-blackout-tab" data-toggle="tab" href="#nav-blackout"
                            role="tab" aria-controls="nav-blackout" aria-selected="false">Blackout
                            Dates</a> -->
                        <a class="nav-link" id="nav-load-tab" data-toggle="tab" href="#nav-load"
                           role="tab" aria-controls="nav-load" aria-selected="false">Lead Time</a>
                        <a class="nav-link" id="nav-black-tab" data-toggle="tab" href="#nav-black"
                           role="tab" aria-controls="nav-black" aria-selected="false">Blackout
                            Dates</a>
                    </div>
                </nav>
                <div class="row">
                    <div class="col-lg-10 pt-5">
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                 aria-labelledby="nav-home-tab">
                                <form class="main-address-from">
                                    <div class="suppliers-address-box">
                                        <ul
                                                class="d-flex flex-row align-items-center justify-content-between pb-3">
                                            <li>Supplier Name <span>*</span></li>
                                            <li>
                                                <div class="form-group">
                                                    <input type="text" class="form-control"
                                                           placeholder="Enter Supplier Name">
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="suppliers-address-box">
                                        <ul
                                                class="d-flex flex-row align-items-center justify-content-between pb-3">
                                            <li>Country <span>*</span></li>
                                            <li>
                                                <div class="form-group">
                                                    <select class="form-control"
                                                            id="exampleFormControlSelect1">
                                                        <option>Select Country</option>
                                                        <option>Bangladesh</option>
                                                        <option>USA</option>
                                                        <option>India</option>
                                                        <option>Pakistan</option>
                                                    </select>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="suppliers-address-box">
                                        <ul
                                                class="d-flex flex-row align-items-start justify-content-between pb-3">
                                            <li>Address (optional)</li>
                                            <li>
                                                <div class="form-group pb-3">
                                                    <input type="text" class="form-control"
                                                           placeholder="Enter Address Line One">
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control"
                                                           placeholder="Enter Address Line Two">
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="suppliers-address-box">
                                        <ul
                                                class="d-flex flex-row align-items-center justify-content-between pb-3">
                                            <li>State</li>
                                            <li>
                                                <div class="form-group">
                                                    <input type="text" class="form-control"
                                                           placeholder="Enter Supplier Name">
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="suppliers-address-box">
                                        <ul
                                                class="d-flex flex-row align-items-center justify-content-between pb-3">
                                            <li>Postal Code (optional)</li>
                                            <li>
                                                <div class="form-group">
                                                    <input type="text" class="form-control"
                                                           placeholder="Enter Postal Code">
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <ul class="d-flex flex-row justify-content-between">
                                        <li>
                                            <button type="submit"
                                                    class="btn address-cancel-btn">Cancel
                                            </button>
                                        </li>
                                        <li>
                                            <button onclick="saveAddress()" type="button"
                                                    class="btn btn-primary">Save
                                            </button>
                                            <button type="submit" class="btn address-save-btn">Save And
                                                Next
                                            </button>
                                        </li>
                                    </ul>
                                </form>
                                <div class="address-save-form">
                                    <div class="suppliers-address-box">
                                        <ul
                                                class="d-flex flex-row align-items-start justify-content-between pb-3">
                                            <li>Address (optional)</li>
                                            <li class="address-save-right">
                                                <p>{{ $supplierDetail[0]['supplier_name'] }} <span class="suppliers-address-box-icon"
                                                                 onclick="editeAdress()"><svg
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                width="13.208" height="13.208"
                                                                viewBox="0 0 13.208 13.208">
                                                                            <g id="Icon_feather-edit"
                                                                               data-name="Icon feather-edit"
                                                                               transform="translate(1)">
                                                                                <path id="Path_314" data-name="Path 314"
                                                                                      d="M8.46,6H4.213A1.213,1.213,0,0,0,3,7.213v8.494a1.213,1.213,0,0,0,1.213,1.213h8.494a1.213,1.213,0,0,0,1.213-1.213V11.46"
                                                                                      transform="translate(-3 -4.713)"
                                                                                      fill="none" stroke="#227cff"
                                                                                      stroke-linecap="round"
                                                                                      stroke-linejoin="round"
                                                                                      stroke-width="2"/>
                                                                                <path id="Path_315" data-name="Path 315"
                                                                                      d="M18.37,3.195a1.287,1.287,0,0,1,1.82,1.82l-5.764,5.764L12,11.386l.607-2.427Z"
                                                                                      transform="translate(-8.36 -2.818)"
                                                                                      fill="#227cff"/>
                                                                            </g>
                                                                        </svg>
                                                                    </span></p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="suppliers-address-box">
                                        <ul
                                                class="d-flex flex-row align-items-start justify-content-between pb-3">
                                            <li>Country</li>
                                            <li class="address-save-right">
                                                <p>{{ get_country_fullname($supplierDetail[0]['country_id']) }}</p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="suppliers-address-box">
                                        <ul
                                                class="d-flex flex-row align-items-start justify-content-between pb-3">
                                            <li>Address</li>
                                            <li class="address-save-right">
                                                <p>{{ $supplierDetail[0]['address_line_1'] }} <br/> {{$supplierDetail[0]['address_line_2'] }}</p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="suppliers-address-box">
                                        <ul
                                                class="d-flex flex-row align-items-start justify-content-between pb-3">
                                            <li>Postal Code</li>
                                            <li class="address-save-right">
                                                <p>{{ $supplierDetail[0]['zipcode'] }}</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-profile" role="tabpanel"
                                 aria-labelledby="nav-profile-tab">
                                <div class="suplier-table-box">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th scope="col" width="20%">
                                                <ul
                                                        class="d-flex flex-row align-items-center table-checkbox-main">
                                                    <li>
                                                        <span>Title</span>
                                                        <img src="assets/img/table-up-down.png"
                                                             alt="">
                                                    </li>
                                                </ul>
                                            </th>
                                            <th scope="col">First Name <img
                                                        src="assets/img/table-up-down-two.png" alt="">
                                            </th>
                                            <th scope="col">Last Name <img
                                                        src="assets/img/table-up-down-two.png" alt="">
                                            </th>
                                            <th scope="col">Email <span
                                                        style="color: #227CFF">[?]</span> <img
                                                        src="assets/img/table-up-down-two.png" alt="">
                                            </th>
                                            <th scope="col">Phone Number <img
                                                        src="assets/img/table-up-down-two.png" alt="">
                                            </th>
                                        </tr>
                                        </thead>
                                    </table>
                                    <div class="card">
                                        <div class="card-body">
                                            <div id="table" class="table-editable">
                                                {{--<div class="table-text-selector table-add">--}}
                                                    {{--<p>Click to add first contact <a href="#"><span--}}
                                                                    {{--class="mr-1 ml-2">--}}
                                                                                {{--<svg xmlns="http://www.w3.org/2000/svg"--}}
                                                                                     {{--width="11.25" height="11.25"--}}
                                                                                     {{--viewBox="0 0 11.25 11.25">--}}
                                                                                    {{--<g id="Icon_ionic-ios-add-circle-outline"--}}
                                                                                       {{--data-name="Icon ionic-ios-add-circle-outline"--}}
                                                                                       {{--transform="translate(0.25 0.25)">--}}
                                                                                        {{--<path id="Path_308"--}}
                                                                                              {{--data-name="Path 308"--}}
                                                                                              {{--d="M15.6,12.85H13.677V10.925a.413.413,0,0,0-.827,0V12.85H10.925a.4.4,0,0,0-.413.413.4.4,0,0,0,.413.413H12.85V15.6a.4.4,0,0,0,.413.413.411.411,0,0,0,.413-.413V13.677H15.6a.413.413,0,1,0,0-.827Z"--}}
                                                                                              {{--transform="translate(-7.889 -7.889)"--}}
                                                                                              {{--fill="#227cff"--}}
                                                                                              {{--stroke="#227cff"--}}
                                                                                              {{--stroke-width="0.5"/>--}}
                                                                                        {{--<path id="Path_309"--}}
                                                                                              {{--data-name="Path 309"--}}
                                                                                              {{--d="M8.75,4.1A4.65,4.65,0,1,1,5.46,5.46,4.621,4.621,0,0,1,8.75,4.1m0-.724A5.375,5.375,0,1,0,14.125,8.75,5.374,5.374,0,0,0,8.75,3.375Z"--}}
                                                                                              {{--transform="translate(-3.375 -3.375)"--}}
                                                                                              {{--fill="#227cff"--}}
                                                                                              {{--stroke="#227cff"--}}
                                                                                              {{--stroke-width="0.5"/>--}}
                                                                                    {{--</g>--}}
                                                                                {{--</svg>--}}
                                                                            {{--</span><i style="font-style: normal;">Add--}}
                                                                {{--Contacts</i></a></p>--}}
                                                {{--</div>--}}
                                                <table class="table table-striped">
                                                    <tbody>
                                                    @foreach($supplierDetail[0]['supplier_contact'] as $supplierContact)
                                                    <tr>
                                                        <th scope="row" contenteditable="true">
                                                            <ul
                                                                    class="d-flex flex-row align-items-center table-checkbox-main">
                                                                <li>
                                                                    <span>{{ $supplierContact['first_name'] }}</span>
                                                                </li>
                                                            </ul>
                                                        </th>
                                                        <td contenteditable="true">{{ $supplierContact['last_name'] }}</td>
                                                        <td contenteditable="true">{{ $supplierContact['email'] }}</td>
                                                        <td class="d-flex flex-row justify-content-between align-items-center"
                                                            contenteditable="true">
                                                            <span></span>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                                <div class="table-add my-3">
                                                    <ul class="table-footer-add-btn">
                                                        <li>
                                                            <p><a href="#">
                                                                                    <span class="mr-1 ml-2">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                                             width="11.25"
                                                                                             height="11.25"
                                                                                             viewBox="0 0 11.25 11.25">
                                                                                            <g id="Icon_ionic-ios-add-circle-outline"
                                                                                               data-name="Icon ionic-ios-add-circle-outline"
                                                                                               transform="translate(0.25 0.25)">
                                                                                                <path id="Path_308"
                                                                                                      data-name="Path 308"
                                                                                                      d="M15.6,12.85H13.677V10.925a.413.413,0,0,0-.827,0V12.85H10.925a.4.4,0,0,0-.413.413.4.4,0,0,0,.413.413H12.85V15.6a.4.4,0,0,0,.413.413.411.411,0,0,0,.413-.413V13.677H15.6a.413.413,0,1,0,0-.827Z"
                                                                                                      transform="translate(-7.889 -7.889)"
                                                                                                      fill="#227cff"
                                                                                                      stroke="#227cff"
                                                                                                      stroke-width="0.5"/>
                                                                                                <path id="Path_309"
                                                                                                      data-name="Path 309"
                                                                                                      d="M8.75,4.1A4.65,4.65,0,1,1,5.46,5.46,4.621,4.621,0,0,1,8.75,4.1m0-.724A5.375,5.375,0,1,0,14.125,8.75,5.374,5.374,0,0,0,8.75,3.375Z"
                                                                                                      transform="translate(-3.375 -3.375)"
                                                                                                      fill="#227cff"
                                                                                                      stroke="#227cff"
                                                                                                      stroke-width="0.5"/>
                                                                                            </g>
                                                                                        </svg>
                                                                                    </span><i
                                                                            style="font-style: normal;">Add
                                                                        Contacts</i>
                                                                </a>
                                                            </p>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <ul
                                                    class="d-flex flex-row justify-content-between address-btn-box">
                                                <li>
                                                    <button type="submit"
                                                            class="btn address-cancel-btn">Cancel
                                                    </button>
                                                </li>
                                                <li>
                                                    <button type="button"
                                                            class="btn btn-primary">Save
                                                    </button>
                                                    <button type="submit"
                                                            class="btn address-save-btn">Save And
                                                        Next
                                                    </button>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Editable table -->
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-contact" role="tabpanel"
                                 aria-labelledby="nav-contact-tab">
                                <div class="shipping-agent-box">

                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-setting" role="tabpanel"
                                 aria-labelledby="nav-setting-tab">
                                <div class="suppliers-settings-main-box">
                                    <div
                                            class="d-flex flex-row align-items-center justify-content-between mb-3">
                                        <div class="suppliers-form-txt-head">
                                            <span>Order Volume</span>
                                        </div>
                                        <div class="suppliers-settings-li-01">
                                            <div class="form-group d-flex flex-row align-items-center">
                                                <input type="text" class="form-control"
                                                       style="width: 58px;" placeholder="90">
                                                <select class="form-control ml-2"
                                                        id="exampleFormControlSelect1">
                                                    <option>Days</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                </select>
                                                <!-- <div class="form-group form-check">
                                                    <input type="checkbox" class="form-check-input"
                                                        id="exampleCheck1">
                                                    <label class="form-check-label"
                                                        for="exampleCheck1">Calendar Month</label>
                                                </div> -->
                                            </div>
                                            <div class="form-group d-flex flex-row align-items-center justify-content-end mt-2">
                                                <div class="form-group form-check">
                                                    <input type="checkbox" class="form-check-input"
                                                           id="exampleCheck1">
                                                    <label class="form-check-label"
                                                           for="exampleCheck1">Calendar Month</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="suppliers-settings-li-txt">
                                            <p>{{ $supplierDetail[0]['supplier_wise_setting'][0]['order_volume'] }}  Days <span onclick="suppliersSettingEdit()">
                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                         width="13.208" height="13.208"
                                                                         viewBox="0 0 13.208 13.208">
                                                                        <g id="Icon_feather-edit"
                                                                           data-name="Icon feather-edit"
                                                                           transform="translate(1)">
                                                                            <path id="Path_314" data-name="Path 314"
                                                                                  d="M8.46,6H4.213A1.213,1.213,0,0,0,3,7.213v8.494a1.213,1.213,0,0,0,1.213,1.213h8.494a1.213,1.213,0,0,0,1.213-1.213V11.46"
                                                                                  transform="translate(-3 -4.713)"
                                                                                  fill="none" stroke="#227cff"
                                                                                  stroke-linecap="round"
                                                                                  stroke-linejoin="round"
                                                                                  stroke-width="2"/>
                                                                            <path id="Path_315" data-name="Path 315"
                                                                                  d="M18.37,3.195a1.287,1.287,0,0,1,1.82,1.82l-5.764,5.764L12,11.386l.607-2.427Z"
                                                                                  transform="translate(-8.36 -2.818)"
                                                                                  fill="#227cff"/>
                                                                        </g>
                                                                    </svg>
                                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                    <div
                                            class="d-flex flex-row align-items-start justify-content-between mb-3">
                                        <div class="suppliers-form-txt-head">
                                            <span>Reorder Schedule</span>
                                        </div>
                                        <div class="suppliers-settings-li-02">
                                            <div class="form-group">
                                                <select class="form-control suppliers-settings-option"
                                                        id="exampleFormControlSelect1">
                                                    <!-- <option value="">Select Options</option> -->
                                                    <option value="1">Bi-Monthly</option>
                                                    <option value="2">Weekly</option>
                                                </select>
                                            </div>
                                            <label for="">Select two days to start your bi-monthly
                                                schedule.</label>
                                            <div class="suppliers-settings-btns-weekly">
                                                <button type="button" class="btn btn-active">Sun</button>
                                                <button type="button" class="btn">Mon</button>
                                                <button type="button" class="btn">Tue</button>
                                                <button type="button" class="btn">Wed</button>
                                                <button type="button" class="btn">Thu</button>
                                                <button type="button" class="btn">Fri</button>
                                                <button type="button" class="btn">Sat</button>
                                            </div>
                                            <div class="suppliers-settings-btns-month">
                                                <button type="button" class="btn btn-active">1</button>
                                                <button type="button" class="btn">2</button>
                                                <button type="button" class="btn">3</button>
                                                <button type="button" class="btn">4</button>
                                                <button type="button" class="btn">5</button>
                                                <button type="button" class="btn">6</button>
                                                <button type="button" class="btn">7</button>
                                                <button type="button" class="btn">8</button>
                                                <button type="button" class="btn">9</button>
                                                <button type="button" class="btn">10</button>
                                                <button type="button" class="btn">11</button>
                                                <button type="button" class="btn">12</button>
                                                <button type="button" class="btn">13</button>
                                                <button type="button" class="btn">14</button>
                                                <button type="button" class="btn btn-active">15</button>
                                                <button type="button" class="btn">16</button>
                                                <button type="button" class="btn">17</button>
                                                <button type="button" class="btn">18</button>
                                                <button type="button" class="btn">19</button>
                                                <button type="button" class="btn">20</button>
                                                <button type="button" class="btn">21</button>
                                                <button type="button" class="btn">22</button>
                                                <button type="button" class="btn">23</button>
                                                <button type="button" class="btn">24</button>
                                                <button type="button" class="btn">25</button>
                                                <button type="button" class="btn">26</button>
                                                <button type="button" class="btn">27</button>
                                                <button type="button" class="btn">28</button>
                                                <button type="button" class="btn">29</button>
                                                <button type="button" class="btn">30</button>
                                                <button type="button" class="btn">31</button>
                                            </div>
                                        </div>
                                        <div class="suppliers-settings-btns-month-txt">
                                            <p>Bi-Weekly <span>Monday</span></p>
                                        </div>
                                    </div>
                                    <div
                                            class="d-flex flex-row align-items-start justify-content-between">
                                        <div class="suppliers-form-txt-head">
                                            <span>Container Settings </span>
                                        </div>
                                        <div
                                                class="suppliers-settings-li-02 suppliers-settings-li-02-show">
                                            <ul>
                                                <li class="pb-3">
                                                    <div
                                                            class="form-check form-check-inline d-flex justify-content-between">
                                                        <input class="form-check-input" type="checkbox"
                                                               id="inlineCheckbox1" value="option1">
                                                        <span>20ft = </span>
                                                        <button type="button"
                                                                class="btn ml-2">28
                                                        </button>
                                                    </div>
                                                </li>
                                                <li class="pb-3">
                                                    <div
                                                            class="form-check form-check-inline d-flex justify-content-between">
                                                        <input class="form-check-input" type="checkbox"
                                                               id="inlineCheckbox1" value="option1">
                                                        <span>40ft = </span>
                                                        <button type="button"
                                                                class="btn ml-2">28
                                                        </button>
                                                    </div>
                                                </li>
                                                <li class="pb-3">
                                                    <div
                                                            class="form-check form-check-inline d-flex justify-content-between">
                                                        <input class="form-check-input" type="checkbox"
                                                               id="inlineCheckbox1" value="option1">
                                                        <span>40HQ = </span>
                                                        <button type="button"
                                                                class="btn ml-2">28
                                                        </button>
                                                    </div>
                                                </li>
                                                <li class="pb-3">
                                                    <div
                                                            class="form-check form-check-inline d-flex justify-content-between">
                                                        <input class="form-check-input" type="checkbox"
                                                               id="inlineCheckbox1" value="option1">
                                                        <span>45HQ = </span>
                                                        <button type="button"
                                                                class="btn ml-2">28
                                                        </button>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <ul
                                            class="d-flex flex-row justify-content-between address-btn-box mt-4">
                                        <li>
                                            <button type="submit"
                                                    class="btn address-cancel-btn">Cancel
                                            </button>
                                        </li>
                                        <li>
                                            <button onclick="saveMonth()" type="button"
                                                    class="btn btn-primary">Save
                                            </button>
                                            <button type="submit" class="btn address-save-btn">Save And
                                                Next
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-load" role="tabpanel"
                                 aria-labelledby="nav-load-tab">
                                <div class="suppliers-lead-time">
                                    <p>This is your default lead time for this supplier. You can also
                                        modify and set specific lead times at the product level where it
                                        differs from your default.</p>
                                    <ul
                                            class="d-flex flex-row align-items-center suppliers-lead-time-btn">
                                        <li><a class="active-lead-btn" href="#">
                                                                <span>
                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                         width="16.291" height="16.379"
                                                                         viewBox="0 0 16.291 16.379">
                                                                        <g id="boat-outline"
                                                                           transform="translate(0.498 0.5)">
                                                                            <path id="Path_432" data-name="Path 432"
                                                                                  d="M18.591,15.5a.677.677,0,0,0-.429-.349l-6.88-2.738a.874.874,0,0,0-.521,0L3.888,15.154a.672.672,0,0,0-.434.35.73.73,0,0,0-.037.554l1.71,4.38a.274.274,0,0,0,.274.17,4.072,4.072,0,0,0,2.513-1.194.282.282,0,0,1,.386,0,3.984,3.984,0,0,0,2.72,1.194,3.97,3.97,0,0,0,2.716-1.2.282.282,0,0,1,.386,0,4.071,4.071,0,0,0,2.513,1.2.274.274,0,0,0,.274-.169l1.717-4.38a.743.743,0,0,0-.037-.558Z"
                                                                                  transform="translate(-3.378 -7.67)"
                                                                                  fill="none" stroke="#fff"
                                                                                  stroke-miterlimit="10"
                                                                                  stroke-width="1"/>
                                                                            <path id="Path_433" data-name="Path 433"
                                                                                  d="M18.511,31.352a.251.251,0,0,0-.131-.221,10.454,10.454,0,0,1-2.3-1.764.37.37,0,0,0-.467-.055,5.4,5.4,0,0,1-5.953,0,.371.371,0,0,0-.471.058,9.947,9.947,0,0,1-2.295,1.755.263.263,0,0,0-.143.211.247.247,0,0,0,.291.263,8.379,8.379,0,0,0,2.213-.876.32.32,0,0,1,.294,0,6.805,6.805,0,0,0,6.17,0,.324.324,0,0,1,.3,0,8.55,8.55,0,0,0,2.206.875.247.247,0,0,0,.292-.247Z"
                                                                                  transform="translate(-4.987 -15.725)"
                                                                                  fill="#fff"/>
                                                                            <path id="Path_434" data-name="Path 434"
                                                                                  d="M18.2,5.139V4.257a.885.885,0,0,0-.882-.882H14.381a.885.885,0,0,0-.882.882V5.14"
                                                                                  transform="translate(-8.208 -3.375)"
                                                                                  fill="none" stroke="#fff"
                                                                                  stroke-linecap="round"
                                                                                  stroke-linejoin="round"
                                                                                  stroke-width="1"/>
                                                                            <path id="Path_435" data-name="Path 435"
                                                                                  d="M18.512,11.786V8.514A1.77,1.77,0,0,0,16.748,6.75H8.514A1.77,1.77,0,0,0,6.75,8.514V11.9"
                                                                                  transform="translate(-4.987 -4.986)"
                                                                                  fill="none" stroke="#fff"
                                                                                  stroke-linecap="round"
                                                                                  stroke-linejoin="round"
                                                                                  stroke-width="1"/>
                                                                            <path id="Path_436" data-name="Path 436"
                                                                                  d="M18,12.909v7.824"
                                                                                  transform="translate(-10.356 -7.925)"
                                                                                  fill="none" stroke="#fff"
                                                                                  stroke-linecap="round"
                                                                                  stroke-linejoin="round"
                                                                                  stroke-width="1"/>
                                                                        </g>
                                                                    </svg>
                                                                </span>
                                                Boat
                                            </a></li>
                                        <li><a class="active-plane-btn" href="#">
                                                                <span>
                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                         width="15.872" height="15.879"
                                                                         viewBox="0 0 15.872 15.879">
                                                                        <path id="plane"
                                                                              d="M17.765,9.478,12.454,6.785V4.517a2.267,2.267,0,1,0-4.535,0V6.785L2.607,9.478A.567.567,0,0,0,2.25,10v2.834a.556.556,0,0,0,.731.539L7.919,11.32v2.834l-1.956.975a.567.567,0,0,0-.312.51v1.916a.567.567,0,0,0,.567.567.51.51,0,0,0,.159,0l3.809-1.134L14,18.122a.51.51,0,0,0,.159,0,.567.567,0,0,0,.567-.567V15.64a.567.567,0,0,0-.312-.51l-1.956-.976V11.32l4.938,2.046a.556.556,0,0,0,.731-.539V9.993a.567.567,0,0,0-.357-.516Zm-.776,2.494L11.32,9.619v5.238l2.267,1.134V16.8l-3.4-.992-3.4.992v-.811l2.267-1.134V9.619L3.384,11.972V10.357L9.052,7.482V4.517a1.134,1.134,0,1,1,2.267,0V7.482l5.669,2.874Z"
                                                                              transform="translate(-2.25 -2.25)"
                                                                              fill="#495057" opacity="0.54"/>
                                                                    </svg>
                                                                </span>
                                                Plane
                                            </a></li>
                                    </ul>
                                    <div class="boat-column-card">
                                        <div class="row">
                                            <div class="boat-column">
                                                <div class="card-box-txt">
                                                    <h4>Production</h4>
                                                    <p>Manufacture your products</p>
                                                    <span id="card-box-txt-svg">
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="10.119" height="9.862"
                                                                             viewBox="0 0 10.119 9.862">
                                                                            <path id="Icon_awesome-arrow-right"
                                                                                  data-name="Icon awesome-arrow-right"
                                                                                  d="M4.3,3.308l.5-.5a.54.54,0,0,1,.766,0L9.96,7.195a.54.54,0,0,1,0,.766l-4.39,4.39a.54.54,0,0,1-.766,0l-.5-.5a.543.543,0,0,1,.009-.775L7.033,8.482H.542A.541.541,0,0,1,0,7.94V7.217a.541.541,0,0,1,.542-.542H7.033L4.311,4.082A.539.539,0,0,1,4.3,3.308Z"
                                                                                  transform="translate(0 -2.647)"
                                                                                  fill="#227cff"/>
                                                                        </svg>
                                                                    </span>
                                                    <h2><span><input
                                                                    style="width: 25%; border: none; font-weight: 600; color: #495057;"
                                                                    type="text" value="3"></span>Days</h2>
                                                </div>
                                            </div>
                                            <div class="boat-column">
                                                <div class="card-box-txt">
                                                    <h4>Port Departure</h4>
                                                    <p>Manufacture your products</p>
                                                    <span id="card-box-txt-svg">
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="10.119" height="9.862"
                                                                             viewBox="0 0 10.119 9.862">
                                                                            <path id="Icon_awesome-arrow-right"
                                                                                  data-name="Icon awesome-arrow-right"
                                                                                  d="M4.3,3.308l.5-.5a.54.54,0,0,1,.766,0L9.96,7.195a.54.54,0,0,1,0,.766l-4.39,4.39a.54.54,0,0,1-.766,0l-.5-.5a.543.543,0,0,1,.009-.775L7.033,8.482H.542A.541.541,0,0,1,0,7.94V7.217a.541.541,0,0,1,.542-.542H7.033L4.311,4.082A.539.539,0,0,1,4.3,3.308Z"
                                                                                  transform="translate(0 -2.647)"
                                                                                  fill="#227cff"/>
                                                                        </svg>
                                                                    </span>
                                                    <h2><span><input
                                                                    style="width: 25%; border: none; font-weight: 600; color: #495057;"
                                                                    type="text" value="7"></span>Days</h2>
                                                </div>
                                            </div>
                                            <div class="boat-column">
                                                <div class="card-box-txt">
                                                    <h4>In Transit</h4>
                                                    <p>Manufacture your products</p>
                                                    <span id="card-box-txt-svg">
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="10.119" height="9.862"
                                                                             viewBox="0 0 10.119 9.862">
                                                                            <path id="Icon_awesome-arrow-right"
                                                                                  data-name="Icon awesome-arrow-right"
                                                                                  d="M4.3,3.308l.5-.5a.54.54,0,0,1,.766,0L9.96,7.195a.54.54,0,0,1,0,.766l-4.39,4.39a.54.54,0,0,1-.766,0l-.5-.5a.543.543,0,0,1,.009-.775L7.033,8.482H.542A.541.541,0,0,1,0,7.94V7.217a.541.541,0,0,1,.542-.542H7.033L4.311,4.082A.539.539,0,0,1,4.3,3.308Z"
                                                                                  transform="translate(0 -2.647)"
                                                                                  fill="#227cff"/>
                                                                        </svg>
                                                                    </span>
                                                    <h2><span><input
                                                                    style="width: 25%; border: none; font-weight: 600; color: #495057;"
                                                                    type="text" value="21"></span>Days</h2>
                                                </div>
                                            </div>
                                            <div class="boat-column">
                                                <div class="card-box-txt">
                                                    <h4>To Warehouse</h4>
                                                    <p>Manufacture your products</p>
                                                    <span id="card-box-txt-svg">
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="10.119" height="9.862"
                                                                             viewBox="0 0 10.119 9.862">
                                                                            <path id="Icon_awesome-arrow-right"
                                                                                  data-name="Icon awesome-arrow-right"
                                                                                  d="M4.3,3.308l.5-.5a.54.54,0,0,1,.766,0L9.96,7.195a.54.54,0,0,1,0,.766l-4.39,4.39a.54.54,0,0,1-.766,0l-.5-.5a.543.543,0,0,1,.009-.775L7.033,8.482H.542A.541.541,0,0,1,0,7.94V7.217a.541.541,0,0,1,.542-.542H7.033L4.311,4.082A.539.539,0,0,1,4.3,3.308Z"
                                                                                  transform="translate(0 -2.647)"
                                                                                  fill="#227cff"/>
                                                                        </svg>
                                                                    </span>
                                                    <h2><span><input
                                                                    style="width: 25%; border: none; font-weight: 600; color: #495057;"
                                                                    type="text" value="10"></span>Days</h2>
                                                </div>
                                            </div>
                                            <div class="boat-column">
                                                <div class="card-box-txt">
                                                    <h4>To Amazon</h4>
                                                    <p>Manufacture your products</p>
                                                    <h2><span><input
                                                                    style="width: 25%; border: none; font-weight: 600; color: #495057;"
                                                                    type="text" value="10"></span>Days</h2>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row pt-3">
                                            <div class="col-lg-6 align-self-center">
                                                <div class="safety-select-box">
                                                    <ul class="d-flex flex-row align-items-center">
                                                        <li>Safety:</li>
                                                        <li class="px-2">
                                                            <!-- <select class="custom-select mr-sm-2"
                                                                id="inlineFormCustomSelect">
                                                                <option selected>10</option>
                                                                <option value="1">20</option>
                                                                <option value="2">30</option>
                                                                <option value="3">40</option>
                                                            </select> -->
                                                            <div class="form-group"
                                                                 style="width: 60px;">
                                                                <input type="number"
                                                                       class="form-control"
                                                                       placeholder="0">
                                                            </div>
                                                        </li>
                                                        <li>days</li>
                                                    </ul>
                                                    <p>Add extra days to pad delivery</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div
                                                        class="safety-select-box-right text-left float-right">
                                                    <h2>Total Lead Time</h2>
                                                    <h4>99 Days</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row pt-4">
                                            <div class="col-lg-10">
                                                <div class="safety-select-box-content">
                                                    <h2>Lead Time Check</h2>
                                                    <p>You can set automatic follow ups to your supplier
                                                        contacts email to confirm or report time
                                                        changes. Setup your
                                                        preferred contacts per each part of the
                                                        product's journey here. To view and control the
                                                        email template, see how it
                                                        works or set one global setting for all
                                                        suppliers, go to the settings page.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row pt-4">
                                            <div class="col-lg-6">
                                                <div class="production-column-box">
                                                    <div>
                                                        <h4>Production</h4>
                                                        <ul
                                                                class="d-flex flex-row align-items-center justify-content-between">
                                                            <li>
                                                                <div class="form-check">
                                                                    <input class="form-check-input"
                                                                           type="checkbox"
                                                                           id="autoSizingCheck2" checked>
                                                                    <label class="form-check-label"
                                                                           for="autoSizingCheck2">
                                                                        Send an email
                                                                    </label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="form-group"
                                                                     style="width: 60px;">
                                                                    <input type="number"
                                                                           class="form-control"
                                                                           placeholder="0">
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <p>days from the end of production</p>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 align-self-end">
                                                <div class="production-column-box">
                                                    <div>
                                                        <ul class="d-flex flex-row align-items-center">
                                                            <li>
                                                                <p>Select Contact</p>
                                                            </li>
                                                            <li class="pl-2">
                                                                <select id="inputState"
                                                                        class="form-control">
                                                                    <option selected>Select Contact
                                                                    </option>
                                                                    <option>...</option>
                                                                </select>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="production-column-box">
                                                    <div>
                                                        <h4>Port Departure</h4>
                                                        <ul class="d-flex flex-row">
                                                            <li>
                                                                <div class="form-check">
                                                                    <input class="form-check-input"
                                                                           type="checkbox"
                                                                           id="autoSizingCheck2" checked>
                                                                </div>
                                                            </li>
                                                            <label class="form-check-label-a"
                                                                   for="autoSizingCheck2">
                                                                Send an email to confirm your products
                                                                left the supplier
                                                                and are on their way to the port.
                                                            </label>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 align-self-end">
                                                <div class="production-column-box">
                                                    <div>
                                                        <ul class="d-flex flex-row align-items-center">
                                                            <li>
                                                                <p>Select Contact</p>
                                                            </li>
                                                            <li class="pl-2">
                                                                <select id="inputState"
                                                                        class="form-control">
                                                                    <option selected>Select Contact
                                                                    </option>
                                                                    <option>...</option>
                                                                </select>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="production-column-box">
                                                    <div>
                                                        <h4>In Transit</h4>
                                                        <ul class="d-flex flex-row align-items-center">
                                                            <li>
                                                                <div class="form-check">
                                                                    <input class="form-check-input"
                                                                           type="checkbox"
                                                                           id="autoSizingCheck2">
                                                                </div>
                                                            </li>
                                                            <label class="form-check-label-a"
                                                                   for="autoSizingCheck2">
                                                                Send an email to confirm your products
                                                                are in transit.
                                                            </label>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 align-self-end">
                                                <div class="production-column-box">
                                                    <div>
                                                        <ul class="d-flex flex-row align-items-center">
                                                            <li>
                                                                <p>Select Contact</p>
                                                            </li>
                                                            <li class="pl-2">
                                                                <select id="inputState"
                                                                        class="form-control">
                                                                    <option selected>Select Contact
                                                                    </option>
                                                                    <option>...</option>
                                                                </select>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="production-column-box">
                                                    <div>
                                                        <h4>To Warehouse</h4>
                                                        <ul class="d-flex flex-row align-items-center">
                                                            <li>
                                                                <div class="form-check">
                                                                    <input class="form-check-input"
                                                                           type="checkbox"
                                                                           id="autoSizingCheck2">
                                                                </div>
                                                            </li>
                                                            <label class="form-check-label-a"
                                                                   for="autoSizingCheck2">
                                                                Send an email to confirm your products
                                                                are in transit.
                                                            </label>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 align-self-end">
                                                <div class="production-column-box">
                                                    <div>
                                                        <ul class="d-flex flex-row align-items-center">
                                                            <li>
                                                                <p>Select Contact</p>
                                                            </li>
                                                            <li class="pl-2">
                                                                <select id="inputState"
                                                                        class="form-control">
                                                                    <option selected>Select Contact
                                                                    </option>
                                                                    <option>...</option>
                                                                </select>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="production-column-box">
                                                    <div>
                                                        <ul class="d-flex flex-row align-items-center">
                                                            <li>
                                                                <div class="form-check">
                                                                    <input class="form-check-input"
                                                                           type="checkbox"
                                                                           id="autoSizingCheck2">
                                                                </div>
                                                            </li>
                                                            <label class="form-check-label-a"
                                                                   for="autoSizingCheck2">
                                                                Send an email to confirm your products
                                                                are in transit.
                                                            </label>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 align-self-end">
                                                <div class="production-column-box">
                                                    <div>
                                                        <ul class="d-flex flex-row align-items-center">
                                                            <li>
                                                                <p>Select Contact</p>
                                                            </li>
                                                            <li class="pl-2">
                                                                <select id="inputState"
                                                                        class="form-control">
                                                                    <option selected>Select Contact
                                                                    </option>
                                                                    <option>...</option>
                                                                </select>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <ul
                                                        class="d-flex flex-row justify-content-between pt-5">
                                                    <li>
                                                        <button type="submit"
                                                                class="btn address-cancel-btn">Cancel
                                                        </button>
                                                    </li>
                                                    <li>
                                                        <button type="button"
                                                                class="btn btn-primary">Save
                                                        </button>
                                                        <button type="submit"
                                                                class="btn address-save-btn ml-2">Save And
                                                            Next
                                                        </button>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-black" role="tabpanel"
                                 aria-labelledby="nav-black-tab">
                                <div class="backout-dates-box">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="backout-dates-box-left-content">
                                                <h2>Event Lists</h2>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="backout-dates-box-right-content">
                                                <ul class="d-flex justify-content-end">
                                                    <li>
                                                                        <span>
                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                 width="11.25" height="11.25"
                                                                                 viewBox="0 0 11.25 11.25">
                                                                                <g id="Icon_ionic-ios-add-circle-outline"
                                                                                   data-name="Icon ionic-ios-add-circle-outline"
                                                                                   transform="translate(-3.125 -3.125)">
                                                                                    <path id="Path_308"
                                                                                          data-name="Path 308"
                                                                                          d="M15.6,12.85H13.677V10.925a.413.413,0,0,0-.827,0V12.85H10.925a.4.4,0,0,0-.413.413.4.4,0,0,0,.413.413H12.85V15.6a.4.4,0,0,0,.413.413.411.411,0,0,0,.413-.413V13.677H15.6a.413.413,0,1,0,0-.827Z"
                                                                                          transform="translate(-4.514 -4.514)"
                                                                                          fill="#227cff"
                                                                                          stroke="#227cff"
                                                                                          stroke-width="0.5"/>
                                                                                    <path id="Path_309"
                                                                                          data-name="Path 309"
                                                                                          d="M8.75,4.1A4.65,4.65,0,1,1,5.46,5.46,4.621,4.621,0,0,1,8.75,4.1m0-.724A5.375,5.375,0,1,0,14.125,8.75,5.374,5.374,0,0,0,8.75,3.375Z"
                                                                                          fill="#227cff"
                                                                                          stroke="#227cff"
                                                                                          stroke-width="0.5"/>
                                                                                </g>
                                                                            </svg>
                                                                        </span>
                                                        <a href="#" class="back-out-add"
                                                           onclick="backOutAdd()">Add New Event</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="backout-dates-table">
                                                <table class="table" width="100%">
                                                    <thead>
                                                    <tr class="black-out-tr">
                                                        <th scope="col">Event Name<span><svg
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        width="9.519" height="6.776"
                                                                        viewBox="0 0 9.519 6.776">
                                                                                        <g id="Group_8726"
                                                                                           data-name="Group 8726"
                                                                                           transform="translate(-479.75 -318)">
                                                                                            <path
                                                                                                    id="Icon_ionic-ios-arrow-round-down"
                                                                                                    data-name="Icon ionic-ios-arrow-round-down"
                                                                                                    d="M15.68,12.2a.308.308,0,0,0-.433,0l-1.433,1.429V8.179a.306.306,0,0,0-.612,0v5.444l-1.433-1.431a.31.31,0,0,0-.433,0,.3.3,0,0,0,0,.431l1.953,1.939h0a.344.344,0,0,0,.1.064.292.292,0,0,0,.118.024.307.307,0,0,0,.214-.087l1.953-1.939A.3.3,0,0,0,15.68,12.2Z"
                                                                                                    transform="translate(495.517 332.651) rotate(180)"
                                                                                                    fill="#227cff"/>
                                                                                            <path
                                                                                                    id="Icon_ionic-ios-arrow-round-down-2"
                                                                                                    data-name="Icon ionic-ios-arrow-round-down"
                                                                                                    d="M4.433,2.455a.308.308,0,0,1-.433,0L2.566,1.029V6.472a.306.306,0,0,1-.612,0V1.029L.521,2.46a.31.31,0,0,1-.433,0,.3.3,0,0,1,0-.431L2.044.087h0a.344.344,0,0,1,.1-.064A.292.292,0,0,1,2.258,0a.307.307,0,0,1,.214.087L4.426,2.026A.3.3,0,0,1,4.433,2.455Z"
                                                                                                    transform="translate(489.27 324.776) rotate(180)"
                                                                                                    fill="#ced4da"/>
                                                                                        </g>
                                                                                    </svg>
                                                                                </span></th>
                                                        <th scope="col">Start Date<span><svg
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        width="9.519" height="6.776"
                                                                        viewBox="0 0 9.519 6.776">
                                                                                        <g id="Group_8727"
                                                                                           data-name="Group 8727"
                                                                                           transform="translate(-479.75 -318)">
                                                                                            <path
                                                                                                    id="Icon_ionic-ios-arrow-round-down"
                                                                                                    data-name="Icon ionic-ios-arrow-round-down"
                                                                                                    d="M15.68,12.2a.308.308,0,0,0-.433,0l-1.433,1.429V8.179a.306.306,0,0,0-.612,0v5.444l-1.433-1.431a.31.31,0,0,0-.433,0,.3.3,0,0,0,0,.431l1.953,1.939h0a.344.344,0,0,0,.1.064.292.292,0,0,0,.118.024.307.307,0,0,0,.214-.087l1.953-1.939A.3.3,0,0,0,15.68,12.2Z"
                                                                                                    transform="translate(495.517 332.651) rotate(180)"
                                                                                                    fill="#a6b0cf"/>
                                                                                            <path
                                                                                                    id="Icon_ionic-ios-arrow-round-down-2"
                                                                                                    data-name="Icon ionic-ios-arrow-round-down"
                                                                                                    d="M4.433,2.455a.308.308,0,0,1-.433,0L2.566,1.029V6.472a.306.306,0,0,1-.612,0V1.029L.521,2.46a.31.31,0,0,1-.433,0,.3.3,0,0,1,0-.431L2.044.087h0a.344.344,0,0,1,.1-.064A.292.292,0,0,1,2.258,0a.307.307,0,0,1,.214.087L4.426,2.026A.3.3,0,0,1,4.433,2.455Z"
                                                                                                    transform="translate(489.27 324.776) rotate(180)"
                                                                                                    fill="#a6b0cf"/>
                                                                                        </g>
                                                                                    </svg>
                                                                                </span></th>
                                                        <th scope="col">End Date<span><svg
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        width="9.519" height="6.776"
                                                                        viewBox="0 0 9.519 6.776">
                                                                                        <g id="Group_8728"
                                                                                           data-name="Group 8728"
                                                                                           transform="translate(-479.75 -318)">
                                                                                            <path
                                                                                                    id="Icon_ionic-ios-arrow-round-down"
                                                                                                    data-name="Icon ionic-ios-arrow-round-down"
                                                                                                    d="M15.68,12.2a.308.308,0,0,0-.433,0l-1.433,1.429V8.179a.306.306,0,0,0-.612,0v5.444l-1.433-1.431a.31.31,0,0,0-.433,0,.3.3,0,0,0,0,.431l1.953,1.939h0a.344.344,0,0,0,.1.064.292.292,0,0,0,.118.024.307.307,0,0,0,.214-.087l1.953-1.939A.3.3,0,0,0,15.68,12.2Z"
                                                                                                    transform="translate(495.517 332.651) rotate(180)"
                                                                                                    fill="#a6b0cf"/>
                                                                                            <path
                                                                                                    id="Icon_ionic-ios-arrow-round-down-2"
                                                                                                    data-name="Icon ionic-ios-arrow-round-down"
                                                                                                    d="M4.433,2.455a.308.308,0,0,1-.433,0L2.566,1.029V6.472a.306.306,0,0,1-.612,0V1.029L.521,2.46a.31.31,0,0,1-.433,0,.3.3,0,0,1,0-.431L2.044.087h0a.344.344,0,0,1,.1-.064A.292.292,0,0,1,2.258,0a.307.307,0,0,1,.214.087L4.426,2.026A.3.3,0,0,1,4.433,2.455Z"
                                                                                                    transform="translate(489.27 324.776) rotate(180)"
                                                                                                    fill="#a6b0cf"/>
                                                                                        </g>
                                                                                    </svg>
                                                                                </span></th>
                                                        <th scope="col">Number Of Days <span>
                                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                                         width="9.519" height="6.776"
                                                                                         viewBox="0 0 9.519 6.776">
                                                                                        <g id="Group_8728"
                                                                                           data-name="Group 8728"
                                                                                           transform="translate(-479.75 -318)">
                                                                                            <path
                                                                                                    id="Icon_ionic-ios-arrow-round-down"
                                                                                                    data-name="Icon ionic-ios-arrow-round-down"
                                                                                                    d="M15.68,12.2a.308.308,0,0,0-.433,0l-1.433,1.429V8.179a.306.306,0,0,0-.612,0v5.444l-1.433-1.431a.31.31,0,0,0-.433,0,.3.3,0,0,0,0,.431l1.953,1.939h0a.344.344,0,0,0,.1.064.292.292,0,0,0,.118.024.307.307,0,0,0,.214-.087l1.953-1.939A.3.3,0,0,0,15.68,12.2Z"
                                                                                                    transform="translate(495.517 332.651) rotate(180)"
                                                                                                    fill="#a6b0cf"/>
                                                                                            <path
                                                                                                    id="Icon_ionic-ios-arrow-round-down-2"
                                                                                                    data-name="Icon ionic-ios-arrow-round-down"
                                                                                                    d="M4.433,2.455a.308.308,0,0,1-.433,0L2.566,1.029V6.472a.306.306,0,0,1-.612,0V1.029L.521,2.46a.31.31,0,0,1-.433,0,.3.3,0,0,1,0-.431L2.044.087h0a.344.344,0,0,1,.1-.064A.292.292,0,0,1,2.258,0a.307.307,0,0,1,.214.087L4.426,2.026A.3.3,0,0,1,4.433,2.455Z"
                                                                                                    transform="translate(489.27 324.776) rotate(180)"
                                                                                                    fill="#a6b0cf"/>
                                                                                        </g>
                                                                                    </svg>

                                                                                </span></th>
                                                        <th scope="col">Type <span><svg
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        width="9.519" height="6.776"
                                                                        viewBox="0 0 9.519 6.776">
                                                                                        <g id="Group_8728"
                                                                                           data-name="Group 8728"
                                                                                           transform="translate(-479.75 -318)">
                                                                                            <path
                                                                                                    id="Icon_ionic-ios-arrow-round-down"
                                                                                                    data-name="Icon ionic-ios-arrow-round-down"
                                                                                                    d="M15.68,12.2a.308.308,0,0,0-.433,0l-1.433,1.429V8.179a.306.306,0,0,0-.612,0v5.444l-1.433-1.431a.31.31,0,0,0-.433,0,.3.3,0,0,0,0,.431l1.953,1.939h0a.344.344,0,0,0,.1.064.292.292,0,0,0,.118.024.307.307,0,0,0,.214-.087l1.953-1.939A.3.3,0,0,0,15.68,12.2Z"
                                                                                                    transform="translate(495.517 332.651) rotate(180)"
                                                                                                    fill="#a6b0cf"/>
                                                                                            <path
                                                                                                    id="Icon_ionic-ios-arrow-round-down-2"
                                                                                                    data-name="Icon ionic-ios-arrow-round-down"
                                                                                                    d="M4.433,2.455a.308.308,0,0,1-.433,0L2.566,1.029V6.472a.306.306,0,0,1-.612,0V1.029L.521,2.46a.31.31,0,0,1-.433,0,.3.3,0,0,1,0-.431L2.044.087h0a.344.344,0,0,1,.1-.064A.292.292,0,0,1,2.258,0a.307.307,0,0,1,.214.087L4.426,2.026A.3.3,0,0,1,4.433,2.455Z"
                                                                                                    transform="translate(489.27 324.776) rotate(180)"
                                                                                                    fill="#a6b0cf"/>
                                                                                        </g>
                                                                                    </svg>
                                                                                </span></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="back-out-tbody">
                                                    @foreach($supplierDetail[0]['supplier_blackout_date'] as $blackoutDates)
                                                        <tr>
                                                        <th scope="row"><input id="back-out-input"
                                                                               type="text" value="{{$blackoutDates['reason']}}"
                                                                               placeholder="Event Name"></th>
                                                        <th scope="row"><input id="back-out-input"
                                                                               type="date" value="{{$blackoutDates['blackout_date']}}"></th>
                                                        <th scope="row"><input id="back-out-input"
                                                                               type="date" value="{{$blackoutDates['blackout_date']}}"></th>
                                                        <th scope="row"><input id="back-out-input"
                                                                               type="text" value="1"
                                                                               placeholder="Number Of Days"></th>
                                                        <th scope="row" class="d-flex">
                                                            <input id="back-out-input" type="text"
                                                                   value="{{$blackoutDates['reason']}}"
                                                                   placeholder="Type">
                                                            <div class="backout-delete-icon">
                                                                <i class="fas fa-trash-alt"></i>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <ul
                                                    class="d-flex flex-row justify-content-between pt-5 shipping-agent-footer">
                                                <li>
                                                    <button type="submit"
                                                            class="btn address-cancel-btn">Cancel
                                                    </button>
                                                </li>
                                                <li>
                                                    <button type="button"
                                                            class="btn btn-primary">Save
                                                    </button>
                                                    <button type="submit"
                                                            class="btn address-save-btn ml-2">Save And
                                                        Next
                                                    </button>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
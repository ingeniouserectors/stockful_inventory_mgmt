@extends('layouts.master')

@section('title') Supplier Blackoute Date @endsection

@section('content')
    <div class="row">
        <div class="col-12">
  
             <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Supplier Blackoute Date</h4>
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('suppliers.index') }}">Supplier</a></li>
                        <li class="breadcrumb-item active">Supplier Blackoute Date</li>
                    </ol>
                </div>
           </div>
            <!-- </div> -->
        </div>
    </div>
    <!-- end page title -->
    <div class="row">
        <div class="col-lg-5">
                <form action="{{ route('supplier_blackoute_date.create') }}">
                    <input type="hidden" name="supplier_id" value="{{$suppliers_id}}" id="supplier_id">
                    <button type="submit" class="btn btn-info waves-effect waves-light mb-3">Create Blackoute Date</button>
                     <a class="btn btn-info waves-effect waves-light mb-3"  href="{{ route('supplier_blackoute_date.index') }}" role="button"><i class="fa fa-download"></i> Download CSV File</a>
                </form>
        </div>
        <div class="col-lg-7">
            <form action="{{ route('supplier_blackoute_date.store') }}" method="post" id="import_csv"  enctype="multipart/form-data">
                {{ csrf_field() }}
                <label>Import CSV file : *</label>
                <input type="hidden" name="insert_type" value="upload_csv">
                <input type="hidden" name="supplier_id" value="{{$suppliers_id}}">
                <input type="file" name="uploadFile" accept=".csv">
                <input type="submit" class="btn btn-primary import_csv" value="Import CSV File">
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">

            </div>
        </div>
    </div>

    <!-- end row -->

    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-4">Supplier Blackoute Date List</h4>
            <div class="table-responsive">
                <table id="datatables" class="table table-striped table-bordered dt-responsive nowrap users-datatables" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead class="thead-light">
                    <tr>
                        <th>No.</th>
                        <th>Blackoute Date</th>
                        <th>Reason</th>
                        <th>Action</th>
                    </tr>
                    </thead>
               </table>
            </div>
        </div>
    </div>

@endsection
@section('script')

    <script>
   $(document).ready(function () {
       var id=$('#supplier_id').val();
                     loadDashboard(id);          
        });
        function loadDashboard(id){
           var suppliers_blackoutedate_list='suppliers_blackoutedate_list';
            var count=1;
            var table = $('#datatables').DataTable({

                "order": [ 0, 'asc' ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('supplier_blackoute_date.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.id=id,d.request_type=suppliers_blackoutedate_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                'columnDefs': [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return  row['blackout_date'];
                        }
                    },
                    {
                        targets: 2,
                        render: function (data, type, row) {
                            return row['reason'];

                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, row) {
                            return row['action'];

                        }
                    }
                    
                    
                ]
            });
        } 
    </script>
 <script>
$(document).on('click', '#button', function () {
    var $ele = $(this).parent().parent();
    var id = $(this).data('id');
            var url = "{{URL('supplier_blackoute_date')}}";
            var destroyurl = url+"/"+id;


    swal({
             title: "Are you sure?",
            text: "You want to delete this record",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
             
            $.ajax({
                type: "DELETE",
                url:destroyurl, 
                data:{ _token:'{{ csrf_token() }}'},
                dataType: "html",
                success: function (data) {
                    var dataResult = JSON.parse(data);
                if(dataResult.statusCode==200){
                    $ele.fadeOut().remove();
                               swal({
              title: "Done!",
              text: "It was succesfully deleted!",
              type: "success",
              timer: 700
           });
    
                      
                    } 
        }

         });
          }  
    
        else
        {
             swal("Cancelled", "", "error");
        }
            
       
    });

});
 </script>              
@endsection
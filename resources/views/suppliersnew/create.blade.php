@extends('layouts.master-new')

@section('title') Create vendors @endsection

@section('content')
        <div class="suppliers-top-main-box">
            <div class="row">
                <div class="col-md-4">
                    <div class="suppliers-first-column">
                        <h2>Suppliers</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="suppliers-main-box suppliers-main-box-scroll">
            <div class="row">
                <div class="col-lg-12">
                    <nav class="suppliers-nav-box">
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home"
                               role="tab" aria-controls="nav-home" aria-selected="true">Basic Details</a>
                            <a class="nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile"
                               role="tab" aria-controls="nav-profile" aria-selected="false">Contacts</a>
                            <a class="nav-link" id="nav-setting-tab" data-toggle="tab" href="#nav-setting"
                               role="tab" aria-controls="nav-setting" aria-selected="false">Settings</a>
                            <a class="nav-link" id="nav-load-tab" data-toggle="tab" href="#nav-load"
                               role="tab" aria-controls="nav-load" aria-selected="false">Lead Time</a>
                            <a class="nav-link" id="nav-black-tab" data-toggle="tab" href="#nav-black"
                               role="tab" aria-controls="nav-black" aria-selected="false">Blackout
                                Dates</a>
                        </div>
                    </nav>
                    <div class="row">
                        <div class="col-lg-10 pt-5">
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                     aria-labelledby="nav-home-tab">
                                    <form class="main-address-from vendors-tab-01" id="forms1">
                                        <input type="hidden" id="supplier_id" value="">
                                        <input type="hidden" id="marketplace_id" value="{{session('MARKETPLACE_ID')}}">
                                        <div class="suppliers-address-box">
                                            <ul
                                                    class="d-flex flex-row align-items-center justify-content-between pb-3">
                                                <li>Supplier Name</li>
                                                <li>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" id="supplier_name" required placeholder="Enter supplier name" required>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="suppliers-address-box">
                                            <ul
                                                    class="d-flex flex-row align-items-start justify-content-between pb-3">
                                                <li>Address (optional) </li>
                                                <li>
                                                    <div class="form-group pb-3">
                                                        <input class="form-control" type="text" name="address_line_1" id="address_line_1" placeholder="Enter Address Line">
                                                    </div>
                                                    <div class="form-group">
                                                        <input class="form-control" type="text" name="address_line_2" id="address_line_2" placeholder="Enter Address Line Two">
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="suppliers-address-box">
                                            <ul
                                                    class="d-flex flex-row align-items-center justify-content-between pb-3">
                                                <li>Country</li>
                                                <li>
                                                    <div class="form-group country">
                                                        <select name="country" id="country" class="form-control" required>
                                                            <option value="">Select Country</option>
                                                            @foreach($country_list as  $key=> $country)
                                                                <option value=" {{ $key }}">{{ $key."(".$country.")" }}</option>
                                                            @endforeach
                                                        </select>

                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="suppliers-address-box">
                                            <ul
                                                    class="d-flex flex-row align-items-center justify-content-between pb-3">
                                                <li>State</li>
                                                <li>
                                                    <div class="form-group state">
                                                        <select name="state" id="state" class="form-control" required>
                                                        </select>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="suppliers-address-box">
                                            <ul
                                                    class="d-flex flex-row align-items-center justify-content-between pb-3">
                                                <li>City</li>
                                                <li>
                                                    <div class="form-group city">
                                                        <select name="city" id="city" class="form-control" required>
                                                        </select>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="suppliers-address-box">
                                            <ul
                                                    class="d-flex flex-row align-items-center justify-content-between pb-3">
                                                <li>Postal Code (optional) </li>
                                                <li>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="zipcode" id="zipcode" maxlength="6" placeholder="Enter Postal Code" required>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>

                                        <ul class="d-flex flex-row justify-content-between">
                                            <li>
                                                <button type="submit"
                                                        class="btn address-cancel-btn">Cancel</button>
                                            </li>
                                            <li>
                                                <button type="button" class="btn btn-primary save_datas" data-val="1">Save</button>
                                                <button type="button" class="btn address-save-btn save_datas" data-val="2">Save And Next</button>
                                            </li>
                                        </ul>
                                    </form>
                                </div>
                                <div class="tab-pane fade" id="nav-profile" role="tabpanel"
                                     aria-labelledby="nav-profile-tab">
                                    <div class="suplier-table-box">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th scope="col" width="20%">First Name <img
                                                            src="assets/img/table-up-down-two.png" alt="">
                                                </th>
                                                <th scope="col" width="20%">Last Name <img
                                                            src="assets/img/table-up-down-two.png" alt="">
                                                </th>
                                                <th scope="col" width="20%">
                                                    <ul
                                                            class="d-flex flex-row align-items-center table-checkbox-main">
                                                        <li>
                                                            <span>Title</span>
                                                            <img src="assets/img/table-up-down.png"
                                                                 alt="">
                                                        </li>
                                                    </ul>
                                                </th>
                                                <th scope="col" width="20%">Email <span
                                                            style="color: #227CFF">[?]</span> <img
                                                            src="assets/img/table-up-down-two.png" alt="">
                                                </th>
                                                <th scope="col" width="20%">Phone Number <img
                                                            src="assets/img/table-up-down-two.png" alt="">
                                                </th>
                                            </tr>
                                            </thead>
                                        </table>
                                        <div class="card">
                                            <div class="card-body">
                                                <div id="table" class="table-editable">
                                                    <div class="table-text-selector table-add">
                                                        <p>Click to add first contact <a href="#"><span
                                                                        class="mr-1 ml-2">
                                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                                     width="11.25" height="11.25"
                                                                                     viewBox="0 0 11.25 11.25">
                                                                                    <g id="Icon_ionic-ios-add-circle-outline"
                                                                                       data-name="Icon ionic-ios-add-circle-outline"
                                                                                       transform="translate(0.25 0.25)">
                                                                                        <path id="Path_308"
                                                                                              data-name="Path 308"
                                                                                              d="M15.6,12.85H13.677V10.925a.413.413,0,0,0-.827,0V12.85H10.925a.4.4,0,0,0-.413.413.4.4,0,0,0,.413.413H12.85V15.6a.4.4,0,0,0,.413.413.411.411,0,0,0,.413-.413V13.677H15.6a.413.413,0,1,0,0-.827Z"
                                                                                              transform="translate(-7.889 -7.889)"
                                                                                              fill="#227cff"
                                                                                              stroke="#227cff"
                                                                                              stroke-width="0.5" />
                                                                                        <path id="Path_309"
                                                                                              data-name="Path 309"
                                                                                              d="M8.75,4.1A4.65,4.65,0,1,1,5.46,5.46,4.621,4.621,0,0,1,8.75,4.1m0-.724A5.375,5.375,0,1,0,14.125,8.75,5.374,5.374,0,0,0,8.75,3.375Z"
                                                                                              transform="translate(-3.375 -3.375)"
                                                                                              fill="#227cff"
                                                                                              stroke="#227cff"
                                                                                              stroke-width="0.5" />
                                                                                    </g>
                                                                                </svg>
                                                                            </span><i style="font-style: normal;">Add
                                                                    Contacts</i></a></p>
                                                    </div>
                                                    <table class="table table-striped vendors-tab-rows">
                                                        <tbody>
                                                        <tr>
                                                            <input type="hidden" id="supplier_contact_id" value="">
                                                            <th scope="row" width="20%"
                                                                contenteditable="true">
                                                                <ul
                                                                        class="d-flex flex-row align-items-center table-checkbox-main">
                                                                    <li class="first_name">
                                                                        Josh
                                                                    </li>
                                                                </ul>
                                                            </th>
                                                            <td contenteditable="true" width="20%" class="last_name">
                                                                Bochner</td>
                                                            <td contenteditable="true" width="20%" class="title">
                                                                Title 2</td>
                                                            <td contenteditable="true" width="20%" class="emails">
                                                                joshbochner@gmail.com
                                                            </td>
                                                            <td width="20%" contenteditable="true">
                                                                <ul
                                                                        class="d-flex justify-content-between align-items-center">
                                                                    <li><span class="phone_number">+00 - 1234567890</span>
                                                                    </li>
                                                                    <li class="delete-btn-vendors-tab">
                                                                        <i class="fas fa-trash-alt"></i>
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="table-add my-3">
                                                        <ul class="table-footer-add-btn">
                                                            <li>
                                                                <p><a href="#">
                                                                                    <span class="mr-1 ml-2">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                                             width="11.25" height="11.25"
                                                                                             viewBox="0 0 11.25 11.25">
                                                                                            <g id="Icon_ionic-ios-add-circle-outline"
                                                                                               data-name="Icon ionic-ios-add-circle-outline"
                                                                                               transform="translate(0.25 0.25)">
                                                                                                <path id="Path_308"
                                                                                                      data-name="Path 308"
                                                                                                      d="M15.6,12.85H13.677V10.925a.413.413,0,0,0-.827,0V12.85H10.925a.4.4,0,0,0-.413.413.4.4,0,0,0,.413.413H12.85V15.6a.4.4,0,0,0,.413.413.411.411,0,0,0,.413-.413V13.677H15.6a.413.413,0,1,0,0-.827Z"
                                                                                                      transform="translate(-7.889 -7.889)"
                                                                                                      fill="#227cff"
                                                                                                      stroke="#227cff"
                                                                                                      stroke-width="0.5" />
                                                                                                <path id="Path_309"
                                                                                                      data-name="Path 309"
                                                                                                      d="M8.75,4.1A4.65,4.65,0,1,1,5.46,5.46,4.621,4.621,0,0,1,8.75,4.1m0-.724A5.375,5.375,0,1,0,14.125,8.75,5.374,5.374,0,0,0,8.75,3.375Z"
                                                                                                      transform="translate(-3.375 -3.375)"
                                                                                                      fill="#227cff"
                                                                                                      stroke="#227cff"
                                                                                                      stroke-width="0.5" />
                                                                                            </g>
                                                                                        </svg>
                                                                                    </span><i
                                                                                style="font-style: normal;">Add
                                                                            Contacts</i>
                                                                    </a>
                                                                </p>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <ul
                                                        class="d-flex flex-row justify-content-between address-btn-box">
                                                    <li>
                                                        <button type="submit"
                                                                class="btn address-cancel-btn">Cancel</button>
                                                    </li>
                                                    <li>
                                                        <button type="button"
                                                                class="btn btn-primary save_contacts">Save</button>
                                                        <button type="submit"
                                                                class="btn address-save-btn save_contacts">Save And
                                                            Next</button>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Editable table -->
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-contact" role="tabpanel"
                                     aria-labelledby="nav-contact-tab">
                                    <div class="shipping-agent-box">

                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-setting" role="tabpanel"
                                     aria-labelledby="nav-setting-tab">
                                    <div class="setting-vendors-form">
                                        <div class="setting-form-box">
                                            <ul class="d-flex justify-content-between align-items-center mb-3">
                                                <li>
                                                    <span>Lead Time</span>
                                                </li>
                                                <li>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" id="lead_time" placeholder="Days">
                                                    </div>
                                                </li>
                                            </ul>
                                            <ul class="d-flex justify-content-between align-items-center mb-3">
                                                <li>
                                                    <span>Order Volume</span>
                                                </li>
                                                <li>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" placeholder="Volume" id="order_volume">
                                                    </div>
                                                </li>
                                            </ul>
                                            <ul
                                                    class="d-flex justify-content-between align-items-center mb-3">
                                                <li>
                                                    <span>MOQ</span>
                                                </li>
                                                <li>
                                                    <div class="form-group">
                                                        <input type="number" class="form-control" id="moqs" placeholder="Enter Number" >
                                                    </div>
                                                </li>
                                            </ul>
                                            <ul class="d-flex justify-content-between align-items-center mb-3">
                                                <li>
                                                    <span>Production Time</span>
                                                </li>
                                                <li>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" placeholder="Production Time" id="Production_Time">
                                                    </div>
                                                </li>
                                            </ul>
                                            <ul class="d-flex justify-content-between align-items-center mb-3">
                                                <li>
                                                    <span>Boat To Port</span>
                                                </li>
                                                <li>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" placeholder="Boat To Port" id="Boat_To_Port">
                                                    </div>
                                                </li>
                                            </ul>
                                            <ul class="d-flex justify-content-between align-items-center mb-3">
                                                <li>
                                                    <span>Port_To_Warehouse</span>
                                                </li>
                                                <li>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" placeholder="Port To Warehouse" id="Port_To_Warehouse">
                                                    </div>
                                                </li>
                                            </ul>
                                            <ul class="d-flex justify-content-between align-items-center mb-3">
                                                <li>
                                                    <span>CBM Per Container</span>
                                                </li>
                                                <li>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" placeholder="CBM Per Container" id="cbm_per_container">
                                                    </div>
                                                </li>
                                            </ul>
                                            <ul class="d-flex justify-content-between align-items-center mb-3">
                                                <li>
                                                    <span>Quantity Discounts</span>
                                                </li>
                                                <li>
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" id="quantity_discount" placeholder="1">
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                            <ul class="d-flex justify-content-between align-items-center mb-3">
                                                <li>
                                                    <span>Warehouse Receipt</span>
                                                </li>
                                                <li>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" placeholder="Warehouse Receipt" id="Warehouse_Receipt">
                                                    </div>
                                                </li>
                                            </ul>
                                            <ul
                                                    class="d-flex justify-content-between align-items-center mb-3">
                                                <li>
                                                    <span>Ship to Warehouse</span>
                                                </li>
                                                <li>
                                                    <div class="form-group">
                                                        <select id="warehouse_id" class="form-control">
                                                            @if(!empty($warehouse))
                                                                <option value="">Select warehouse</option>
                                                                @foreach($warehouse as $house)
                                                                    @if($house['warehouse_name'] != '')
                                                                        <option value="{{$house['id']}}">{{$house['warehouse_name']}}</option>
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </li>
                                            </ul>

                                            <ul
                                                    class="d-flex flex-row justify-content-between address-btn-box">
                                                <li>
                                                    <button type="submit" class="btn address-cancel-btn">Cancel</button>
                                                </li>
                                                <li>
                                                    <button type="button" class="btn btn-primary save_setting">Save</button>
                                                    <button type="button" class="btn address-save-btn save_setting">Save And Next</button>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-load" role="tabpanel"
                                     aria-labelledby="nav-load-tab">
                                    <div class="suppliers-lead-time">
                                        <p>This is your default lead time for this supplier. You can also
                                            modify and set specific lead times at the product level where it
                                            differs from your default.</p>
                                        <ul
                                                class="d-flex flex-row align-items-center suppliers-lead-time-btn">
                                            <li><a class="active-lead-btn" href="#">
                                                        <span>
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="16.291" height="16.379"
                                                                 viewBox="0 0 16.291 16.379">
                                                                <g id="boat-outline"
                                                                   transform="translate(0.498 0.5)">
                                                                    <path id="Path_432" data-name="Path 432"
                                                                          d="M18.591,15.5a.677.677,0,0,0-.429-.349l-6.88-2.738a.874.874,0,0,0-.521,0L3.888,15.154a.672.672,0,0,0-.434.35.73.73,0,0,0-.037.554l1.71,4.38a.274.274,0,0,0,.274.17,4.072,4.072,0,0,0,2.513-1.194.282.282,0,0,1,.386,0,3.984,3.984,0,0,0,2.72,1.194,3.97,3.97,0,0,0,2.716-1.2.282.282,0,0,1,.386,0,4.071,4.071,0,0,0,2.513,1.2.274.274,0,0,0,.274-.169l1.717-4.38a.743.743,0,0,0-.037-.558Z"
                                                                          transform="translate(-3.378 -7.67)"
                                                                          fill="none" stroke="#fff"
                                                                          stroke-miterlimit="10"
                                                                          stroke-width="1" />
                                                                    <path id="Path_433" data-name="Path 433"
                                                                          d="M18.511,31.352a.251.251,0,0,0-.131-.221,10.454,10.454,0,0,1-2.3-1.764.37.37,0,0,0-.467-.055,5.4,5.4,0,0,1-5.953,0,.371.371,0,0,0-.471.058,9.947,9.947,0,0,1-2.295,1.755.263.263,0,0,0-.143.211.247.247,0,0,0,.291.263,8.379,8.379,0,0,0,2.213-.876.32.32,0,0,1,.294,0,6.805,6.805,0,0,0,6.17,0,.324.324,0,0,1,.3,0,8.55,8.55,0,0,0,2.206.875.247.247,0,0,0,.292-.247Z"
                                                                          transform="translate(-4.987 -15.725)"
                                                                          fill="#fff" />
                                                                    <path id="Path_434" data-name="Path 434"
                                                                          d="M18.2,5.139V4.257a.885.885,0,0,0-.882-.882H14.381a.885.885,0,0,0-.882.882V5.14"
                                                                          transform="translate(-8.208 -3.375)"
                                                                          fill="none" stroke="#fff"
                                                                          stroke-linecap="round"
                                                                          stroke-linejoin="round"
                                                                          stroke-width="1" />
                                                                    <path id="Path_435" data-name="Path 435"
                                                                          d="M18.512,11.786V8.514A1.77,1.77,0,0,0,16.748,6.75H8.514A1.77,1.77,0,0,0,6.75,8.514V11.9"
                                                                          transform="translate(-4.987 -4.986)"
                                                                          fill="none" stroke="#fff"
                                                                          stroke-linecap="round"
                                                                          stroke-linejoin="round"
                                                                          stroke-width="1" />
                                                                    <path id="Path_436" data-name="Path 436"
                                                                          d="M18,12.909v7.824"
                                                                          transform="translate(-10.356 -7.925)"
                                                                          fill="none" stroke="#fff"
                                                                          stroke-linecap="round"
                                                                          stroke-linejoin="round"
                                                                          stroke-width="1" />
                                                                </g>
                                                            </svg>
                                                        </span>
                                                    Boat
                                                </a></li>
                                            <li><a class="active-plane-btn" href="#">
                                                        <span>
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="15.872" height="15.879"
                                                                 viewBox="0 0 15.872 15.879">
                                                                <path id="plane"
                                                                      d="M17.765,9.478,12.454,6.785V4.517a2.267,2.267,0,1,0-4.535,0V6.785L2.607,9.478A.567.567,0,0,0,2.25,10v2.834a.556.556,0,0,0,.731.539L7.919,11.32v2.834l-1.956.975a.567.567,0,0,0-.312.51v1.916a.567.567,0,0,0,.567.567.51.51,0,0,0,.159,0l3.809-1.134L14,18.122a.51.51,0,0,0,.159,0,.567.567,0,0,0,.567-.567V15.64a.567.567,0,0,0-.312-.51l-1.956-.976V11.32l4.938,2.046a.556.556,0,0,0,.731-.539V9.993a.567.567,0,0,0-.357-.516Zm-.776,2.494L11.32,9.619v5.238l2.267,1.134V16.8l-3.4-.992-3.4.992v-.811l2.267-1.134V9.619L3.384,11.972V10.357L9.052,7.482V4.517a1.134,1.134,0,1,1,2.267,0V7.482l5.669,2.874Z"
                                                                      transform="translate(-2.25 -2.25)"
                                                                      fill="#495057" opacity="0.54" />
                                                            </svg>
                                                        </span>
                                                    Plane
                                                </a></li>
                                        </ul>
                                        <div class="boat-column-card">
                                            <div class="row">
                                                <div class="boat-column">
                                                    <div class="card-box-txt">
                                                        <h4>Production</h4>
                                                        <p>Manufacture your products</p>
                                                        <span id="card-box-txt-svg">
                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                     width="10.119" height="9.862"
                                                                     viewBox="0 0 10.119 9.862">
                                                                    <path id="Icon_awesome-arrow-right"
                                                                          data-name="Icon awesome-arrow-right"
                                                                          d="M4.3,3.308l.5-.5a.54.54,0,0,1,.766,0L9.96,7.195a.54.54,0,0,1,0,.766l-4.39,4.39a.54.54,0,0,1-.766,0l-.5-.5a.543.543,0,0,1,.009-.775L7.033,8.482H.542A.541.541,0,0,1,0,7.94V7.217a.541.541,0,0,1,.542-.542H7.033L4.311,4.082A.539.539,0,0,1,4.3,3.308Z"
                                                                          transform="translate(0 -2.647)"
                                                                          fill="#227cff" />
                                                                </svg>
                                                            </span>
                                                        <h2><span><input
                                                                        style="width: 25%; border: none; font-weight: 600; color: #495057;"
                                                                        type="text" value="3"></span>Days</h2>
                                                    </div>
                                                </div>
                                                <div class="boat-column">
                                                    <div class="card-box-txt">
                                                        <h4>Port Departure</h4>
                                                        <p>Manufacture your products</p>
                                                        <span id="card-box-txt-svg">
                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                     width="10.119" height="9.862"
                                                                     viewBox="0 0 10.119 9.862">
                                                                    <path id="Icon_awesome-arrow-right"
                                                                          data-name="Icon awesome-arrow-right"
                                                                          d="M4.3,3.308l.5-.5a.54.54,0,0,1,.766,0L9.96,7.195a.54.54,0,0,1,0,.766l-4.39,4.39a.54.54,0,0,1-.766,0l-.5-.5a.543.543,0,0,1,.009-.775L7.033,8.482H.542A.541.541,0,0,1,0,7.94V7.217a.541.541,0,0,1,.542-.542H7.033L4.311,4.082A.539.539,0,0,1,4.3,3.308Z"
                                                                          transform="translate(0 -2.647)"
                                                                          fill="#227cff" />
                                                                </svg>
                                                            </span>
                                                        <h2><span><input
                                                                        style="width: 25%; border: none; font-weight: 600; color: #495057;"
                                                                        type="text" value="7"></span>Days</h2>
                                                    </div>
                                                </div>
                                                <div class="boat-column">
                                                    <div class="card-box-txt">
                                                        <h4>In Transit</h4>
                                                        <p>Manufacture your products</p>
                                                        <span id="card-box-txt-svg">
                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                     width="10.119" height="9.862"
                                                                     viewBox="0 0 10.119 9.862">
                                                                    <path id="Icon_awesome-arrow-right"
                                                                          data-name="Icon awesome-arrow-right"
                                                                          d="M4.3,3.308l.5-.5a.54.54,0,0,1,.766,0L9.96,7.195a.54.54,0,0,1,0,.766l-4.39,4.39a.54.54,0,0,1-.766,0l-.5-.5a.543.543,0,0,1,.009-.775L7.033,8.482H.542A.541.541,0,0,1,0,7.94V7.217a.541.541,0,0,1,.542-.542H7.033L4.311,4.082A.539.539,0,0,1,4.3,3.308Z"
                                                                          transform="translate(0 -2.647)"
                                                                          fill="#227cff" />
                                                                </svg>
                                                            </span>
                                                        <h2><span><input
                                                                        style="width: 25%; border: none; font-weight: 600; color: #495057;"
                                                                        type="text" value="21"></span>Days</h2>
                                                    </div>
                                                </div>
                                                <div class="boat-column">
                                                    <div class="card-box-txt">
                                                        <h4>To Warehouse</h4>
                                                        <p>Manufacture your products</p>
                                                        <span id="card-box-txt-svg">
                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                     width="10.119" height="9.862"
                                                                     viewBox="0 0 10.119 9.862">
                                                                    <path id="Icon_awesome-arrow-right"
                                                                          data-name="Icon awesome-arrow-right"
                                                                          d="M4.3,3.308l.5-.5a.54.54,0,0,1,.766,0L9.96,7.195a.54.54,0,0,1,0,.766l-4.39,4.39a.54.54,0,0,1-.766,0l-.5-.5a.543.543,0,0,1,.009-.775L7.033,8.482H.542A.541.541,0,0,1,0,7.94V7.217a.541.541,0,0,1,.542-.542H7.033L4.311,4.082A.539.539,0,0,1,4.3,3.308Z"
                                                                          transform="translate(0 -2.647)"
                                                                          fill="#227cff" />
                                                                </svg>
                                                            </span>
                                                        <h2><span><input
                                                                        style="width: 25%; border: none; font-weight: 600; color: #495057;"
                                                                        type="text" value="10"></span>Days</h2>
                                                    </div>
                                                </div>
                                                <div class="boat-column">
                                                    <div class="card-box-txt">
                                                        <h4>To Amazon</h4>
                                                        <p>Manufacture your products</p>
                                                        <h2><span><input
                                                                        style="width: 25%; border: none; font-weight: 600; color: #495057;"
                                                                        type="text" value="10"></span>Days</h2>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row pt-3">
                                                <div class="col-lg-6 align-self-center">
                                                    <div class="safety-select-box">
                                                        <ul class="d-flex flex-row align-items-center">
                                                            <li>Safety:</li>
                                                            <li class="px-2">
                                                                <!-- <select class="custom-select mr-sm-2"
                                                                    id="inlineFormCustomSelect">
                                                                    <option selected>10</option>
                                                                    <option value="1">20</option>
                                                                    <option value="2">30</option>
                                                                    <option value="3">40</option>
                                                                </select> -->
                                                                <div class="form-group"
                                                                     style="width: 60px;">
                                                                    <input type="number"
                                                                           class="form-control"
                                                                           placeholder="0">
                                                                </div>
                                                            </li>
                                                            <li>days</li>
                                                        </ul>
                                                        <p>Add extra days to pad delivery</p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div
                                                            class="safety-select-box-right text-left float-right">
                                                        <h2>Total Lead Time</h2>
                                                        <h4>99 Days</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row pt-4">
                                                <div class="col-lg-10">
                                                    <div class="safety-select-box-content">
                                                        <h2>Lead Time Check</h2>
                                                        <p>You can set automatic follow ups to your supplier
                                                            contacts email to confirm or report time
                                                            changes. Setup your
                                                            preferred contacts per each part of the
                                                            product's journey here. To view and control the
                                                            email template, see how it
                                                            works or set one global setting for all
                                                            suppliers, go to the settings page.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row pt-4">
                                                <div class="col-lg-6">
                                                    <div class="production-column-box">
                                                        <div>
                                                            <h4>Production</h4>
                                                            <ul
                                                                    class="d-flex flex-row align-items-center justify-content-between">
                                                                <li>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input"
                                                                               type="checkbox"
                                                                               id="autoSizingCheck2" checked>
                                                                        <label class="form-check-label"
                                                                               for="autoSizingCheck2">
                                                                            Send an email
                                                                        </label>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="form-group"
                                                                         style="width: 60px;">
                                                                        <input type="number"
                                                                               class="form-control"
                                                                               placeholder="0">
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <p>days from the end of production</p>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 align-self-end">
                                                    <div class="production-column-box">
                                                        <div>
                                                            <ul class="d-flex flex-row align-items-center">
                                                                <li>
                                                                    <p>Select Contact</p>
                                                                </li>
                                                                <li class="pl-2">
                                                                    <select id="inputState"
                                                                            class="form-control">
                                                                        <option selected>Select Contact
                                                                        </option>
                                                                        <option>...</option>
                                                                    </select>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="production-column-box">
                                                        <div>
                                                            <h4>Port Departure</h4>
                                                            <ul class="d-flex flex-row">
                                                                <li>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input"
                                                                               type="checkbox"
                                                                               id="autoSizingCheck2" checked>
                                                                    </div>
                                                                </li>
                                                                <label class="form-check-label-a"
                                                                       for="autoSizingCheck2">
                                                                    Send an email to confirm your products
                                                                    left the supplier
                                                                    and are on their way to the port.
                                                                </label>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 align-self-end">
                                                    <div class="production-column-box">
                                                        <div>
                                                            <ul class="d-flex flex-row align-items-center">
                                                                <li>
                                                                    <p>Select Contact</p>
                                                                </li>
                                                                <li class="pl-2">
                                                                    <select id="inputState"
                                                                            class="form-control">
                                                                        <option selected>Select Contact
                                                                        </option>
                                                                        <option>...</option>
                                                                    </select>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="production-column-box">
                                                        <div>
                                                            <h4>In Transit</h4>
                                                            <ul class="d-flex flex-row align-items-center">
                                                                <li>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input"
                                                                               type="checkbox"
                                                                               id="autoSizingCheck2">
                                                                    </div>
                                                                </li>
                                                                <label class="form-check-label-a"
                                                                       for="autoSizingCheck2">
                                                                    Send an email to confirm your products
                                                                    are in transit.
                                                                </label>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 align-self-end">
                                                    <div class="production-column-box">
                                                        <div>
                                                            <ul class="d-flex flex-row align-items-center">
                                                                <li>
                                                                    <p>Select Contact</p>
                                                                </li>
                                                                <li class="pl-2">
                                                                    <select id="inputState"
                                                                            class="form-control">
                                                                        <option selected>Select Contact
                                                                        </option>
                                                                        <option>...</option>
                                                                    </select>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="production-column-box">
                                                        <div>
                                                            <h4>To Warehouse</h4>
                                                            <ul class="d-flex flex-row align-items-center">
                                                                <li>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input"
                                                                               type="checkbox"
                                                                               id="autoSizingCheck2">
                                                                    </div>
                                                                </li>
                                                                <label class="form-check-label-a"
                                                                       for="autoSizingCheck2">
                                                                    Send an email to confirm your products
                                                                    are in transit.
                                                                </label>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 align-self-end">
                                                    <div class="production-column-box">
                                                        <div>
                                                            <ul class="d-flex flex-row align-items-center">
                                                                <li>
                                                                    <p>Select Contact</p>
                                                                </li>
                                                                <li class="pl-2">
                                                                    <select id="inputState"
                                                                            class="form-control">
                                                                        <option selected>Select Contact
                                                                        </option>
                                                                        <option>...</option>
                                                                    </select>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="production-column-box">
                                                        <div>
                                                            <ul class="d-flex flex-row align-items-center">
                                                                <li>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input"
                                                                               type="checkbox"
                                                                               id="autoSizingCheck2">
                                                                    </div>
                                                                </li>
                                                                <label class="form-check-label-a"
                                                                       for="autoSizingCheck2">
                                                                    Send an email to confirm your products
                                                                    are in transit.
                                                                </label>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 align-self-end">
                                                    <div class="production-column-box">
                                                        <div>
                                                            <ul class="d-flex flex-row align-items-center">
                                                                <li>
                                                                    <p>Select Contact</p>
                                                                </li>
                                                                <li class="pl-2">
                                                                    <select id="inputState"
                                                                            class="form-control">
                                                                        <option selected>Select Contact
                                                                        </option>
                                                                        <option>...</option>
                                                                    </select>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-10">
                                                    <ul
                                                            class="d-flex flex-row justify-content-between pt-5">
                                                        <li>
                                                            <button type="submit"
                                                                    class="btn address-cancel-btn">Cancel</button>
                                                        </li>
                                                        <li>
                                                            <button type="button"
                                                                    class="btn btn-primary">Save</button>
                                                            <button type="submit"
                                                                    class="btn address-save-btn ml-2">Save And
                                                                Next</button>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-black" role="tabpanel"
                                     aria-labelledby="nav-black-tab">
                                    <div class="backout-dates-box">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="backout-dates-box-left-content">
                                                    <h2>Event Lists</h2>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="backout-dates-box-right-content">
                                                    <ul class="d-flex justify-content-end">
                                                        <li>
                                                                <span>
                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                         width="11.25" height="11.25"
                                                                         viewBox="0 0 11.25 11.25">
                                                                        <g id="Icon_ionic-ios-add-circle-outline"
                                                                           data-name="Icon ionic-ios-add-circle-outline"
                                                                           transform="translate(-3.125 -3.125)">
                                                                            <path id="Path_308"
                                                                                  data-name="Path 308"
                                                                                  d="M15.6,12.85H13.677V10.925a.413.413,0,0,0-.827,0V12.85H10.925a.4.4,0,0,0-.413.413.4.4,0,0,0,.413.413H12.85V15.6a.4.4,0,0,0,.413.413.411.411,0,0,0,.413-.413V13.677H15.6a.413.413,0,1,0,0-.827Z"
                                                                                  transform="translate(-4.514 -4.514)"
                                                                                  fill="#227cff" stroke="#227cff"
                                                                                  stroke-width="0.5" />
                                                                            <path id="Path_309"
                                                                                  data-name="Path 309"
                                                                                  d="M8.75,4.1A4.65,4.65,0,1,1,5.46,5.46,4.621,4.621,0,0,1,8.75,4.1m0-.724A5.375,5.375,0,1,0,14.125,8.75,5.374,5.374,0,0,0,8.75,3.375Z"
                                                                                  fill="#227cff" stroke="#227cff"
                                                                                  stroke-width="0.5" />
                                                                        </g>
                                                                    </svg>
                                                                </span>
                                                            <a href="#" class="back-out-add">Add New Event</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="backout-dates-table">
                                                    <table class="table" width="100%" id="blackout_date">
                                                        <thead>
                                                        <tr class="black-out-tr">
                                                            <th scope="col">Event Name <span><svg
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            width="9.519" height="6.776"
                                                                            viewBox="0 0 9.519 6.776">
                                                                                <g id="Group_8726"
                                                                                   data-name="Group 8726"
                                                                                   transform="translate(-479.75 -318)">
                                                                                    <path
                                                                                            id="Icon_ionic-ios-arrow-round-down"
                                                                                            data-name="Icon ionic-ios-arrow-round-down"
                                                                                            d="M15.68,12.2a.308.308,0,0,0-.433,0l-1.433,1.429V8.179a.306.306,0,0,0-.612,0v5.444l-1.433-1.431a.31.31,0,0,0-.433,0,.3.3,0,0,0,0,.431l1.953,1.939h0a.344.344,0,0,0,.1.064.292.292,0,0,0,.118.024.307.307,0,0,0,.214-.087l1.953-1.939A.3.3,0,0,0,15.68,12.2Z"
                                                                                            transform="translate(495.517 332.651) rotate(180)"
                                                                                            fill="#227cff" />
                                                                                    <path
                                                                                            id="Icon_ionic-ios-arrow-round-down-2"
                                                                                            data-name="Icon ionic-ios-arrow-round-down"
                                                                                            d="M4.433,2.455a.308.308,0,0,1-.433,0L2.566,1.029V6.472a.306.306,0,0,1-.612,0V1.029L.521,2.46a.31.31,0,0,1-.433,0,.3.3,0,0,1,0-.431L2.044.087h0a.344.344,0,0,1,.1-.064A.292.292,0,0,1,2.258,0a.307.307,0,0,1,.214.087L4.426,2.026A.3.3,0,0,1,4.433,2.455Z"
                                                                                            transform="translate(489.27 324.776) rotate(180)"
                                                                                            fill="#ced4da" />
                                                                                </g>
                                                                            </svg>
                                                                        </span></th>
                                                            <th scope="col">Start Date <span><svg
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            width="9.519" height="6.776"
                                                                            viewBox="0 0 9.519 6.776">
                                                                                <g id="Group_8727"
                                                                                   data-name="Group 8727"
                                                                                   transform="translate(-479.75 -318)">
                                                                                    <path
                                                                                            id="Icon_ionic-ios-arrow-round-down"
                                                                                            data-name="Icon ionic-ios-arrow-round-down"
                                                                                            d="M15.68,12.2a.308.308,0,0,0-.433,0l-1.433,1.429V8.179a.306.306,0,0,0-.612,0v5.444l-1.433-1.431a.31.31,0,0,0-.433,0,.3.3,0,0,0,0,.431l1.953,1.939h0a.344.344,0,0,0,.1.064.292.292,0,0,0,.118.024.307.307,0,0,0,.214-.087l1.953-1.939A.3.3,0,0,0,15.68,12.2Z"
                                                                                            transform="translate(495.517 332.651) rotate(180)"
                                                                                            fill="#a6b0cf" />
                                                                                    <path
                                                                                            id="Icon_ionic-ios-arrow-round-down-2"
                                                                                            data-name="Icon ionic-ios-arrow-round-down"
                                                                                            d="M4.433,2.455a.308.308,0,0,1-.433,0L2.566,1.029V6.472a.306.306,0,0,1-.612,0V1.029L.521,2.46a.31.31,0,0,1-.433,0,.3.3,0,0,1,0-.431L2.044.087h0a.344.344,0,0,1,.1-.064A.292.292,0,0,1,2.258,0a.307.307,0,0,1,.214.087L4.426,2.026A.3.3,0,0,1,4.433,2.455Z"
                                                                                            transform="translate(489.27 324.776) rotate(180)"
                                                                                            fill="#a6b0cf" />
                                                                                </g>
                                                                            </svg>
                                                                        </span></th>
                                                            <th scope="col">End Date <span><svg
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            width="9.519" height="6.776"
                                                                            viewBox="0 0 9.519 6.776">
                                                                                <g id="Group_8728"
                                                                                   data-name="Group 8728"
                                                                                   transform="translate(-479.75 -318)">
                                                                                    <path
                                                                                            id="Icon_ionic-ios-arrow-round-down"
                                                                                            data-name="Icon ionic-ios-arrow-round-down"
                                                                                            d="M15.68,12.2a.308.308,0,0,0-.433,0l-1.433,1.429V8.179a.306.306,0,0,0-.612,0v5.444l-1.433-1.431a.31.31,0,0,0-.433,0,.3.3,0,0,0,0,.431l1.953,1.939h0a.344.344,0,0,0,.1.064.292.292,0,0,0,.118.024.307.307,0,0,0,.214-.087l1.953-1.939A.3.3,0,0,0,15.68,12.2Z"
                                                                                            transform="translate(495.517 332.651) rotate(180)"
                                                                                            fill="#a6b0cf" />
                                                                                    <path
                                                                                            id="Icon_ionic-ios-arrow-round-down-2"
                                                                                            data-name="Icon ionic-ios-arrow-round-down"
                                                                                            d="M4.433,2.455a.308.308,0,0,1-.433,0L2.566,1.029V6.472a.306.306,0,0,1-.612,0V1.029L.521,2.46a.31.31,0,0,1-.433,0,.3.3,0,0,1,0-.431L2.044.087h0a.344.344,0,0,1,.1-.064A.292.292,0,0,1,2.258,0a.307.307,0,0,1,.214.087L4.426,2.026A.3.3,0,0,1,4.433,2.455Z"
                                                                                            transform="translate(489.27 324.776) rotate(180)"
                                                                                            fill="#a6b0cf" />
                                                                                </g>
                                                                            </svg>
                                                                        </span></th>
                                                            <th scope="col">Number Of Days <span>
                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                 width="9.519" height="6.776"
                                                                                 viewBox="0 0 9.519 6.776">
                                                                                <g id="Group_8728"
                                                                                   data-name="Group 8728"
                                                                                   transform="translate(-479.75 -318)">
                                                                                    <path
                                                                                            id="Icon_ionic-ios-arrow-round-down"
                                                                                            data-name="Icon ionic-ios-arrow-round-down"
                                                                                            d="M15.68,12.2a.308.308,0,0,0-.433,0l-1.433,1.429V8.179a.306.306,0,0,0-.612,0v5.444l-1.433-1.431a.31.31,0,0,0-.433,0,.3.3,0,0,0,0,.431l1.953,1.939h0a.344.344,0,0,0,.1.064.292.292,0,0,0,.118.024.307.307,0,0,0,.214-.087l1.953-1.939A.3.3,0,0,0,15.68,12.2Z"
                                                                                            transform="translate(495.517 332.651) rotate(180)"
                                                                                            fill="#a6b0cf" />
                                                                                    <path
                                                                                            id="Icon_ionic-ios-arrow-round-down-2"
                                                                                            data-name="Icon ionic-ios-arrow-round-down"
                                                                                            d="M4.433,2.455a.308.308,0,0,1-.433,0L2.566,1.029V6.472a.306.306,0,0,1-.612,0V1.029L.521,2.46a.31.31,0,0,1-.433,0,.3.3,0,0,1,0-.431L2.044.087h0a.344.344,0,0,1,.1-.064A.292.292,0,0,1,2.258,0a.307.307,0,0,1,.214.087L4.426,2.026A.3.3,0,0,1,4.433,2.455Z"
                                                                                            transform="translate(489.27 324.776) rotate(180)"
                                                                                            fill="#a6b0cf" />
                                                                                </g>
                                                                            </svg>

                                                                        </span></th>
                                                            <th scope="col">Type <span><svg
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            width="9.519" height="6.776"
                                                                            viewBox="0 0 9.519 6.776">
                                                                                <g id="Group_8728"
                                                                                   data-name="Group 8728"
                                                                                   transform="translate(-479.75 -318)">
                                                                                    <path
                                                                                            id="Icon_ionic-ios-arrow-round-down"
                                                                                            data-name="Icon ionic-ios-arrow-round-down"
                                                                                            d="M15.68,12.2a.308.308,0,0,0-.433,0l-1.433,1.429V8.179a.306.306,0,0,0-.612,0v5.444l-1.433-1.431a.31.31,0,0,0-.433,0,.3.3,0,0,0,0,.431l1.953,1.939h0a.344.344,0,0,0,.1.064.292.292,0,0,0,.118.024.307.307,0,0,0,.214-.087l1.953-1.939A.3.3,0,0,0,15.68,12.2Z"
                                                                                            transform="translate(495.517 332.651) rotate(180)"
                                                                                            fill="#a6b0cf" />
                                                                                    <path
                                                                                            id="Icon_ionic-ios-arrow-round-down-2"
                                                                                            data-name="Icon ionic-ios-arrow-round-down"
                                                                                            d="M4.433,2.455a.308.308,0,0,1-.433,0L2.566,1.029V6.472a.306.306,0,0,1-.612,0V1.029L.521,2.46a.31.31,0,0,1-.433,0,.3.3,0,0,1,0-.431L2.044.087h0a.344.344,0,0,1,.1-.064A.292.292,0,0,1,2.258,0a.307.307,0,0,1,.214.087L4.426,2.026A.3.3,0,0,1,4.433,2.455Z"
                                                                                            transform="translate(489.27 324.776) rotate(180)"
                                                                                            fill="#a6b0cf" />
                                                                                </g>
                                                                            </svg>
                                                                        </span></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody class="back-out-tbody">
                                                        <tr class="parent_class">
                                                            <input type="hidden" class="blackout_date" value="">
                                                            <th scope="row"><input id="back-out-input"
                                                                                   type="text"
                                                                                   placeholder="Event Name" class="event_name"></th>
                                                            <th scope="row"><input id="back-out-input"
                                                                                   type="date" class="start_date"></th>
                                                            <th scope="row"><input id="back-out-input"
                                                                                   type="date" class="last_date"></th>
                                                            <th scope="row"><input id="back-out-input"
                                                                                   type="text" value="0"
                                                                                   placeholder="Number Of Days" class="number_of_day"></th>
                                                            <th scope="row" class="d-flex">
                                                                <input id="back-out-input" type="text" value="Production" class="type" placeholder="Type">
                                                                <div class="backout-delete-icon">
                                                                    <i class="fas fa-trash-alt"></i>
                                                                </div>
                                                            </th>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <ul
                                                        class="d-flex flex-row justify-content-between pt-5 shipping-agent-footer">
                                                    <li>
                                                        <button type="submit"
                                                                class="btn address-cancel-btn">Cancel</button>
                                                    </li>
                                                    <li>
                                                        <button type="button"
                                                                class="btn btn-primary save_blankout_date">Save</button>
                                                        <button type="submit"
                                                                class="btn address-save-btn ml-2 save_blankout_date">Save And
                                                            Next</button>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script>
        function makeid(length) {
            var result           = '';
            var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;
            for ( var i = 0; i < length; i++ ) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            return result;
        }
        $(function(){
            var rand_text = makeid(5);
            $("#blackout_date tbody > tr").attr("id",rand_text);
            var dtToday = new Date();
            var month = dtToday.getMonth() + 1;
            var day = dtToday.getDate();
            var year = dtToday.getFullYear();
            if(month < 10)
                month = '0' + month.toString();
            if(day < 10)
                day = '0' + day.toString();

            var maxDate = year + '-' + month + '-' + day;
            $('.start_date').attr('min', maxDate);
            $('.last_date').attr('min', maxDate);
            $('.start_date').val(maxDate);
            $('.last_date').val(maxDate);

            //$(".last_date").prop('disabled',true);
            $(".number_of_day").prop('readonly',true);

            $(".start_date").addClass(rand_text+'start_date');
            $(".last_date").addClass(rand_text+'last_date');
            $(".event_name").addClass(rand_text+'event_name');
            $(".number_of_day").addClass(rand_text+'number_of_day');
            $(".type").addClass(rand_text+'type');

            $(".start_date").attr('data-val',rand_text);
            $(".last_date").attr('data-val',rand_text);
            $(".event_name").attr('data-val',rand_text);
            $(".number_of_day").attr('data-val',rand_text);
            $(".type").attr('data-val',rand_text);

        });

        $(document).on('change','.start_date',function(){
            var startdate = $(this).val();
            var randtext = $(this).data('val');
            if(startdate != ''){
                var dtToday = new Date(startdate);
                var month = dtToday.getMonth() + 1;
                var day = dtToday.getDate() + 1;
                var year = dtToday.getFullYear();
                if(month < 10)
                    month = '0' + month.toString();
                if(day < 10)
                    day = '0' + day.toString();

                var maxDate = year + '-' + month + '-' + day;
                $('.'+randtext+'last_date').attr('min', maxDate);
                $('.'+randtext+'last_date').val(maxDate);

                $('.'+randtext+'last_date').prop('disabled',false);
            }
        });

        $(document).on('change','.last_date',function(){
            var randtext = $(this).data('val');
            var start = $('.'+randtext+'start_date').val();
            var end = $(this).val();

            var startDay = new Date(start);
            var endDay = new Date(end);
            var millisecondsPerDay = 1000 * 60 * 60 * 24;

            var millisBetween = endDay.getTime() - startDay.getTime();
            var days = millisBetween / millisecondsPerDay;

            $('.'+randtext+'number_of_day').val(Math.floor(days));
        });


        $(document).on('click',".save_blankout_date",function(){
            var event_names = [];
            var start_dates = [];
            var last_dates = [];
            var number_of_days = [];
            var types = [];
            var backouot_dates = [];

            $("#blackout_date tbody > tr").each(function( index, element ) {
                var event_name = $(this).find(".event_name").val();
                var start_date = $(this).find(".start_date").val();
                var last_date = $(this).find(".last_date").val();
                var number_of_day = $(this).find(".number_of_day").val();
                var type = $(this).find(".type").val();
                var backouot_date = $(this).find(".blackout_date").val();

                event_names.push($.trim(event_name));
                start_dates.push($.trim(start_date));
                last_dates.push($.trim(last_date));
                number_of_days.push($.trim(number_of_day));
                types.push($.trim(type));
                backouot_dates.push($.trim(backouot_date));
            });

            var supplier_id = $("#supplier_id").val();
            //var supplier_id = '6';
            var insert_type = 'insert_update_backout_date';

            if(supplier_id == ''){
                $(".tab-pane").removeClass('show active');
                $(".nav-link").removeClass('active');
                $("#nav-home-tab").addClass('active');
                $("#nav-home").addClass('show active');
            }else {

                $.ajax({
                    type: "POST",
                    url: "{{route('suppliers.store')}}",
                    data: {
                        supplier_id: supplier_id,
                        event_names: event_names,
                        start_dates: start_dates,
                        last_dates: last_dates,
                        number_of_days: number_of_days,
                        types: types,
                        insert_type: insert_type,
                        backouot_dates: backouot_dates
                    },
                    success: function (res) {
                        if (res.error == 0) {
                            $("#messages").html(res.message);
                            $("#supplier_id").val(supplier_id);
                            window.location.href = "{{route('suppliers.index')}}";
//                            $("#messages").html(res.message);
//                            $(".backouot_dates").val(res.backouot_dates);
//                            $(".tab-pane").removeClass('show active');
//                            $(".nav-link").removeClass('active');
//                            $("#nav-contact-tab").addClass('active');
//                            $("#nav-setting").addClass('show active');
                        } else {
                            $("#messages").html(res.message);
                        }
                    }
                });
            }

        });

        $(document).on("click",".save_contacts",function(){
            var first_names = [];
            var last_names = [];
            var titles = [];
            var emailss = [];
            var phone_numbers = [];
            var supplier_contact_ids = [];

            $("table > tbody > tr").each(function( index, element ) {
                //datass[index] = [];
                var first_name = $(this).find(".first_name").text();
                var last_name = $(this).find(".last_name").text();
                var title = $(this).find(".title").text();
                var emails = $(this).find(".emails").text();
                var phone_number = $(this).find(".phone_number").text();
                var supplier_contact_id = $(this).find(".supplier_contact_id").val();

                first_names.push($.trim(first_name));
                last_names.push($.trim(last_name));
                titles.push($.trim(title));
                emailss.push($.trim(emails));
                phone_numbers.push($.trim(phone_number));
                supplier_contact_ids.push($.trim(supplier_contact_id));
            });

            var supplier_id = $("#supplier_id").val();
           // var supplier_id = '6';
            var insert_type = 'insert_contacts';

            if(supplier_id == ''){
                $(".tab-pane").removeClass('show active');
                $(".nav-link").removeClass('active');
                $("#nav-home-tab").addClass('active');
                $("#nav-home").addClass('show active');
            }else {

                $.ajax({
                    type: "POST",
                    url: "{{route('suppliers.store')}}",
                    data: {
                        supplier_id: supplier_id,
                        first_names: first_names,
                        last_names: last_names,
                        emails: emailss,
                        insert_type: insert_type,
                        supplier_contact_ids: supplier_contact_ids
                    },
                    success: function (res) {
                        if (res.error == 0) {
                            $("#messages").html(res.message);
                            $(".supplier_contact_id").val(res.supplier_contact_id);
                            $(".tab-pane").removeClass('show active');
                            $(".nav-link").removeClass('active');
                            $("#nav-contact-tab").addClass('active');
                            $("#nav-setting").addClass('show active');
                        } else {
                            $("#messages").html(res.message);
                        }
                    }
                });
            }
        });

        $(document).on('click','.save_setting',function(){
            var supplier_id = $("#supplier_id").val();
            var insert_type = 'insert_update_setting';
            var lead_time = $("#lead_time").val();
            var order_volume = $("#order_volume").val();
            var moqs = $("#moqs").val();
            var Production_Time = $("#Production_Time").val();
            var Boat_To_Port = $("#Boat_To_Port").val();
            var Port_To_Warehouse = $("#Port_To_Warehouse").val();
            var Warehouse_Receipt = $("#Warehouse_Receipt").val();
            var cbm_per_container = $("#cbm_per_container").val();
            var quantity_discount = $("#quantity_discount").val();
            var warehouse_id = $("#warehouse_id").val();


            if(supplier_id == ''){
                $(".tab-pane").removeClass('show active');
                $(".nav-link").removeClass('active');
                $("#nav-home-tab").addClass('active');
                $("#nav-home").addClass('show active');
            }else{
                $.ajax({
                    type:"POST",
                    url:"{{route('suppliers.store')}}",
                    data:{supplier_id:supplier_id,
                        lead_time:lead_time,
                        moqs:moqs,
                        order_volume:order_volume,
                        Production_Time:Production_Time,
                        Boat_To_Port:Boat_To_Port,
                        Port_To_Warehouse:Port_To_Warehouse,
                        Warehouse_Receipt:Warehouse_Receipt,
                        cbm_per_container:cbm_per_container,
                        quantity_discount:quantity_discount,
                        warehouse_id:warehouse_id,
                        insert_type:insert_type
                    },
                    success:function(res){
                        if(res.error == 0){
                            $("#messages").html(res.message);
                            $("#supplier_id").val(supplier_id);
                            $(".tab-pane").removeClass('show active');
                            $(".nav-link").removeClass('active');
                            $("#nav-black-tab").addClass('active');
                            $("#nav-black").addClass('show active');
                        }else{
                            $("#messages").html(res.message);
                        }
                    }
                });
            }
        });
        $(document).on('click','.save_datas',function(){
            var types = $(this).data('val');
            var supplier_name = $('#supplier_name').val();
            var country = $('#country').val();
            var state = $('#state').val();
            var city = $('#city').val();
            var zipcode = $('#zipcode').val();
            var address_line_1 = $('#address_line_1').val();
            var address_line_2 = $('#address_line_2').val();
            var marketplace_id = $("#marketplace_id").val();
            var supplier_id = $("#supplier_id").val();
            var insert_type = 'insert_update_basic_details';
            if(supplier_name == ''){
                $("#supplier_name").css("border", "1px solid red");
                $("#supplier_name").focus();
            }else if(country == ''){
                $("#country").css("border", "1px solid red");
                $("#country").focus();
            }else {
                $.ajax({
                    type: "POST",
                    url: "{{route('suppliers.store')}}",
                    data: {
                        supplier_id: supplier_id,
                        marketplace_id: marketplace_id,
                        supplier_name: supplier_name,
                        country: country,
                        state: state,
                        city: city,
                        zipcode: zipcode,
                        address_line_1: address_line_1,
                        address_line_2: address_line_2,
                        insert_type: insert_type
                    },
                    success: function (res) {
                        if (res.error == 0) {
                            $("#messages").html(res.message);
                            $("#supplier_id").val(res.supplier_id);
                            if (types == 1) {
                            } else {
                                $(".tab-pane").removeClass('show active');
                                $(".nav-link").removeClass('active');
//                                if (type == 1) {
//                                    $("#nav-contact-tab").addClass('active');
//                                    $("#nav-setting").addClass('show active');
//                                } else {
                                $("#nav-profile-tab").addClass('active');
                                $("#nav-profile").addClass('show active');
                                //}
                            }
                        } else {
                            $("#messages").html(res.message);
                        }
                    }
                });
            }

        });


        $(document).on('click','.save_setting',function(){
            var vendor_id = $("#vendors_id").val();
            var insert_type = 'insert_update_setting';
            var lead_time = $("#lead_time").val();
            var order_volume = $("#order_volume").val();
            var moqs = $("#moqs").val();
            var shipping = $("#shipping").val();
            var cbm_per_container = $("#cbm_per_container").val();
            var quantity_discount = $("#quantity_discount").val();
            var warehouse_id = $("#warehouse_id").val();


            if(vendor_id == ''){
                $(".tab-pane").removeClass('show active');
                $(".nav-link").removeClass('active');
                $("#nav-home-tab").addClass('active');
                $("#nav-home").addClass('show active');
            }else{
                $.ajax({
                    type:"POST",
                    url:"{{route('vendors.store')}}",
                    data:{vendor_id:vendor_id,
                        lead_time:lead_time,
                        moqs:moqs,
                        order_volume:order_volume,
                        shipping:shipping,
                        cbm_per_container:cbm_per_container,
                        quantity_discount:quantity_discount,
                        warehouse_id:warehouse_id,
                        insert_type:insert_type
                    },
                    success:function(res){
                        if(res.error == 0){
                            $("#messages").html(res.message);
                            $("#vendors_id").val(vendor_id);
                            window.location.href = "{{route('vendors.index')}}";
                        }else{
                            $("#messages").html(res.message);
                        }
                    }
                });
            }
        });




        $(document).on('click','.backout-delete-icon',function(){
            var insert_type = 'delete_vendors_contact';
            var rand_text = makeid(5);
            var supplier_contact_id = $(this).find(".supplier_contact_id").val();
            $(this).closest('tr').addClass(rand_text);
            //var supplier_contact_id = $('.'+rand_text).find(".supplier_contact_id").val();
            //$(this).closest('tr').attr('data-random',supplier_contact_id);

            {{--if(supplier_contact_id != ''){--}}
                {{--$.ajax({--}}
                    {{--type:"POST",--}}
                    {{--url:"{{route('vendors.store')}}",--}}
                    {{--data:{supplier_contact_id:supplier_contact_id,insert_type:insert_type },--}}
                    {{--success:function(res){--}}
                        {{--$("."+rand_text).remove();--}}
                    {{--}--}}
                {{--});--}}
            {{--}else{--}}
                $("."+rand_text).remove();
            // }
        });

        $(document).on('click','.delete-btn-vendors-tab',function(){
            var insert_type = 'delete_vendors_contact';
            var rand_text = makeid(5);
            var supplier_contact_id = $(this).find(".supplier_contact_id").val();
            $(this).closest('tr').addClass(rand_text);
            //var supplier_contact_id = $('.'+rand_text).find(".supplier_contact_id").val();
            //$(this).closest('tr').attr('data-random',supplier_contact_id);

            {{--if(supplier_contact_id != ''){--}}
                {{--$.ajax({--}}
                    {{--type:"POST",--}}
                    {{--url:"{{route('vendors.store')}}",--}}
                    {{--data:{supplier_contact_id:supplier_contact_id,insert_type:insert_type },--}}
                    {{--success:function(res){--}}
                        {{--$("."+rand_text).remove();--}}
                    {{--}--}}
                {{--});--}}
            {{--}else{--}}
                $("."+rand_text).remove();
            // }
        });


        //        $(document).ready(function(){
        //            var type = $("#type").val();
        //            if(type == 1){
        //                $('#nav-profile-tab').hide();
        //            }else{
        //                $('#nav-profile-tab').show();
        //            }
        //        });
        //        $(document).on('change','#type',function(){
        //            var type = $(this).val();
        //            if(type == 1){
        //                $('#nav-profile-tab').hide();
        //            }else{
        //                $('#nav-profile-tab').show();
        //            }
        //        });

        //    $(document).ready(function(){
        //        $("select").change(function(){
        //            check();
        //        }).change();
        //        check();
        //    });
        //
        function check() {
            var data= $('#type').val();
            if(data=="1")  {
                $(".primary_first_name").hide();
                $(".primary_last_name").hide();
                $(".primary_email").hide();
                $('.primary_first_name').rules('remove',  {
                    required : true,
                    messages : { required : 'Firstname is required' }
                });
                $('.primary_last_name').rules('remove',  {
                    required : true,
                    messages : { required : 'Lastname is required' }
                });
                $('.primary_email').rules('remove',  {
                    required : true,
                    messages : { required : 'Email is required' }
                });

            }
            else
            {

                $(".primary_first_name").show();
                $(".primary_last_name").show();
                $(".primary_email").show();

                $('.primary_first_name').rules('add',  {
                    required : true,
                    messages : { required : 'Firstname number is required' }
                });
                $('.primary_last_name').rules('add',  {
                    required : true,
                    messages : { required : 'Lastname is required' }
                });
                $('.primary_email').rules('add',  {
                    required : true,
                    messages : { required : 'Email is required' }
                });
            }
        }

    </script>
    <script type="text/javascript">

        $('#country').change(function(){
            var country = $(this).val();
            var request_type = 'Get_all_state';
            if(country){
                $.ajax({
                    type:"POST",
                    url:"{{route('vendors.store')}}",
                    data:{country:country,request_type:request_type},
                    success:function(res){
                        if(res){
                            $("#state").empty();
                            $("#state").append('<option>Select</option>');
                            $.each(res,function(key,value){
                                $("#state").append('<option value="'+key+'">'+key+'('+value+')'+'</option>');
                            });

                        }else{
                            $("#state").empty();
                        }
                    }
                });
            }else{
                $("#state").empty();
                $("#city").empty();
            }
        });
        $('#state').on('change',function(){
            var state = $(this).val();
            var request_type = 'Get_all_city';
            if(state){
                $.ajax({
                    type:"POST",
                    url:"{{ route('vendors.store') }}",
                    data:{state:state,request_type:request_type},
                    success:function(res){
                        if(res){
                            console.log(res);
                            $("#city").empty();
                            $("#city").append('<option>Select</option>');
                            $.each(res,function(key,value){
                                $("#city").append('<option value="'+key+'">'+key+'('+value+')'+'</option>');
                            });

                        }else{
                            $("#city").empty();
                        }
                    }
                });
            }else{
                $("#city").empty();
            }

        });
    </script>
@endsection
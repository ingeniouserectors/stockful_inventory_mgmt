@extends('layouts.master')

@section('title') Sales History  List @endsection

@section('content')

    <div class="row">
        <div class="col-12">

            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Sales History</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Sales History</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
     <div class="row">
        <div class="col-lg-6">
            <a class="btn btn-info waves-effect waves-light mb-3"  href="{{ route('saleshistory.show','import') }}" role="button"><i class="fa fa-download"></i> Export CSV </a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">

            </div>
        </div>
    </div>
    <!-- end row -->
    <div class="card">

        <div class="card-body" style="margin-bottom: 30px;">
            <h4 class="card-title mb-4">Sales History List</h4>
            <div class="box">
                <div class="table-responsive">
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap " style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead class="thead-light">
                        <tr>
                            <th>No.</th>
                            <th>SKU</th>
                            <th>7 Days Sales</th>
                            <th>7 Days Rate</th>
                            <th>14 Days Sales</th>
                            <th>14 Days Rate</th>
                            <th>30 Days Sales</th>
                            <th>30 Days Rate</th>
                            <th>7 Over 7</th>
                            <th>30 Over 30</th>
                            <th>30 Days Sales - Last Year</th>
                            <?php
                            for ($count=0;$count<=24;$count++){
                            ?>
                            <th><?php echo date('m-Y',strtotime("-$count month")); ?></th>
                            <th><?php echo date('m-Y',strtotime("-$count month"))." rate" ; ?></th>
                            <?php
                            }
                            ?>
                        </tr>
                        </thead>
                        <tbody>
                            @if(!empty($Daily_logic_calculations))
                                @foreach($Daily_logic_calculations as $key => $record)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$record['sku']}}</td>

                                        <td>{{isset($record['daily_logic_calculations'][0]['sevendays_sales']) ? $record['daily_logic_calculations'][0]['sevendays_sales'] : '' }}</td>
                                        <td>{{isset($record['daily_logic_calculations'][0]['sevenday_day_rate']) ? $record['daily_logic_calculations'][0]['sevenday_day_rate'] : '' }}</td>
                                        <td>{{isset($record['daily_logic_calculations'][0]['fourteendays_sales']) ? $record['daily_logic_calculations'][0]['fourteendays_sales'] : '' }}</td>
                                        <td>{{isset($record['daily_logic_calculations'][0]['fourteendays_day_rate']) ? $record['daily_logic_calculations'][0]['fourteendays_day_rate'] : '' }}</td>
                                        <td>{{isset($record['daily_logic_calculations'][0]['thirtydays_sales']) ? $record['daily_logic_calculations'][0]['thirtydays_sales'] : '' }}</td>
                                        <td>{{isset($record['daily_logic_calculations'][0]['thirtyday_day_rate']) ? $record['daily_logic_calculations'][0]['thirtyday_day_rate'] : '' }}</td>
                                        <td>{{isset($record['daily_logic_calculations'][0]['sevendays_previous']) ? $record['daily_logic_calculations'][0]['sevendays_previous'] : '' }}</td>
                                        <td>{{isset($record['daily_logic_calculations'][0]['thirtydays_previous'])  ? $record['daily_logic_calculations'][0]['thirtydays_previous'] : '' }}</td>
                                        <td>{{isset($record['daily_logic_calculations'][0]['thirtydays_last_year']) ? $record['daily_logic_calculations'][0]['thirtydays_last_year'] : '' }}</td>
                                        <?php
                                        for ($count=0;$count<=24;$count++){?>
                                        @php($get_datemonth =  date('Y-m',strtotime("-$count month")))
                                        @php($get_supply_total_trands = get_supply_total_trands($get_datemonth,$record['id']))
                                        @if(!empty($get_supply_total_trands))
                                            <td>{{$get_supply_total_trands->sales_total}}</td>
                                            <td>{{$get_supply_total_trands->day_rate}}</td>
                                        @else
                                            <td></td>
                                            <td></td>
                                        @endif
                                        <?php } ?>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

        @endsection
        @section('script')

            <script>
                $(function () {

                    $('.datatable').DataTable({
                        "pageLength": 10,
                        'autoWidth': false,
                        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
                    });
                });
            </script>

            <script>

                function archiveFunction() {
                    event.preventDefault(); // prevent form submit
                    var form = event.target.form; // storing the form
                    swal({
                            title: "Are you sure?",
                            text: "You want to delete this record",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Yes, Delete it!",
                            cancelButtonText: "No, cancel please!",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },
                        function(isConfirm){
                            if (isConfirm) {
                                form.submit();          // submitting the form when user press yes
                            } else {
                                swal("Cancelled", "", "error");
                            }
                        });
                }
            </script>
@endsection
@extends('layouts.master')

@section('title') Create Report @endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Create Report</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Create Reports</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <span class="error_message"></span>
            <div class="card">
                <div class="card-body">
                    <form name="main-report-create" id="main-report-create" action="{{ route('report.store') }}" method="POST" onreset="myFunction()" onsubmit="return check()">
                        @csrf

                        <input type="hidden" name="insert_type" value="insert_request_report">
                        
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Report Type : </label>
                            <div class="col-md-4">
                                <select class="custom-select" name="report_type" id="report_type">
                                    <option value="">Select Report Type</option>
                                    <option value="_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_LAST_UPDATE_">Order</option>
                                    <option value="_GET_FBA_FULFILLMENT_CUSTOMER_RETURNS_DATA_
                                    ">Order Return</option>
                                    <option value="_GET_FBA_MYI_UNSUPPRESSED_INVENTORY_DATA_">Unsuppressed Inventory</option>
                                    <option value="_GET_EXCESS_INVENTORY_DATA_">Excess Inventory</option>
                                    <option value="_GET_FBA_FULFILLMENT_CURRENT_INVENTORY_DATA_">Historical Inventory</option>
                                    <option value="_GET_FBA_ESTIMATED_FBA_FEES_TXT_DATA_">Products</option>
                                </select>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">Marketplace : </label>
                            <div class="col-md-4">
                                <select class="custom-select" name="marketplace_id" id="marketplace_id">
                                    <option value="">Select Marketplace</option>
                                    @forelse($userMarketplace as $userMarketplace)
                                        <option value="{{ $userMarketplace['id'] }}">{{ $userMarketplace['user']['name'] }}</option>
                                    @empty
                                        <option value="">No Marketplace Found</option>
                                    @endforelse
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Start Date : </label>
                            <div class="col-md-4">
                                <input  class="form-control min-today dtchange" type="date" max="3000-01-01"  name="start_date" id="start_date" >
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">End Date</label>
                            <div class="col-md-4">
                               <input  class="form-control min-today dtchange" type="date" max="3000-01-01"  name="end_date" id="end_date" >
                            </div>
                        </div>
                        

                        <div class="button-items mt-3">
                            <input class="btn btn-info dtchange" type="submit" value="Submit" id="submit">
                            <a class="btn btn-danger waves-effect waves-light" href="{{ route('report.index') }}" role="button">Cancel</a>
                            <input class="btn btn-warning" type="reset" value="Reset">
                        </div>
                    </form>

                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->

@section('script')
<script>
    end_date.max = new Date().toISOString().split("T")[0];
    start_date.max = new Date().toISOString().split("T")[0];

    $(document).ready(function () {
        $('.dtchange').change(function(){
            check();
        });
    });

    function check(){
        var start = $('#start_date').val();
        var end = $('#end_date').val();
        
        if(start > end && start != "" && end != ""){
            $('.error_message').html('<div class="alert alert-danger">PLease select start date lower of end date<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');
            return false;
        }

         var startDay = new Date(start);
         var endDay = new Date(end);
         var millisecondsPerDay = 1000 * 60 * 60 * 24;

         var millisBetween = endDay.getTime() - startDay.getTime();
         var days = millisBetween / millisecondsPerDay;

         days = Math.floor(days);
         if(days > 30){
            $('.error_message').html('<div class="alert alert-danger">PLease select start date and date between range of one month<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');
            return false;
         }else{
            return true;
         }
    }
</script>
@endsection


@endsection
@extends('layouts.master')

@section('title')  Report List @endsection

@section('content')

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18"> Request Report List </h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active"> Request Report List </li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        @php($get_check_setting = get_check_access('report_request_module'))
        @php($get_usercheck_access = get_user_check_access('report_request_module', 'create'))

        @if($get_check_setting == 1)
            @if($get_usercheck_access == 1)
                <div class="row">
                    <div class="col-lg-6">
                        <a class="btn btn-info waves-effect waves-light mb-3"  href="{{ route('report.create') }}" role="button"> Create New Request report </a>
                    </div>
                    <div class="col-lg-6"></div>
                </div>
            @endif
        @else
            @if($get_usercheck_access == 1)
                <div class="row">
                    <div class="col-lg-6">
                        <a class="btn btn-info waves-effect waves-light mb-3"  href="{{ route('report.create') }}" role="button"> Create New Request Report </a>
                    </div>
                    <div class="col-lg-6"></div>
                </div>
            @endif
        @endif

        @if(session()->has('message'))
            {!! session('message') !!}
        @endif
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                       
                        <h4 class="card-title mb-4"> Request Report List</h4>
                        <div class="table-responsive">
                            <table id="datatables" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead class="thead-light">
                                <tr>
                                    <th>No.</th>
                                    <th>User</th>
                                    <th>Request Id</th>
                                    <th>Status</th>
                                    <th>Report Type</th>
                                </tr>
                                </thead>
                                
                            </table>
                        </div>
                          
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function () {
        loadDashboard();
    });
 
        function loadDashboard(){
           var request_report_list='request_report_list';
            var count=1;
            var table = $('#datatables').DataTable({

                "order": [ 0, 'asc' ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('report.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.request_type=request_report_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                success:function($res){
                    console.log(res);
                    //return false;
                },
                'columnDefs': [    
                    
                     {
                        targets: 0,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return  row['user_name'];
                        }
                    },
                    {
                         targets: 2,
                        render: function (data, type, row) {
                            return row['request_id'];
                        }
                    },
                    {
                         targets: 3,
                        render: function (data, type, row) {
                            return row['status'];
                        }
                    },
                    {
                         targets: 4,
                        render: function (data, type, row) {
                            return row['report_type'];
                        }
                    }
                    
                    
                ]
            });
        }
</script>
@endsection
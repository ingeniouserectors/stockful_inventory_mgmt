@extends('layouts.master')

@section('title') Create Module @endsection

@section('content')

<!-- start  page title -->
 <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Create Module</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('modules.index') }}">Modules</a></li>
                            <li class="breadcrumb-item active">Create Module</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>

<!-- end page title -->

<div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form name="create-module" id="create-module" action="{{ route('modules.store') }}" method="POST" onreset="myFunction()">
                            @csrf

                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Module</label>
                                <div class="col-md-4">
                                    <input class="form-control @error('module') is-invalid @enderror" type="text" value="{{ old('module') }}" name="module" id="module">
                                    @error('module')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('module') }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="button-items mt-3">
                            <input class="btn btn-info" type="submit" value="Submit" id="submit">
                            <a class="btn btn-danger waves-effect waves-light" href="{{ route('modules.index') }}" role="button">Cancel</a>
                            <input class="btn btn-warning" type="reset" value="Reset">
                        </div>

                        </form>
                    </div>
                </div>
            </div> <!-- end col -->
        </div>


@endsection
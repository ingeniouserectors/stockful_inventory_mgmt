@extends('layouts.master')

@section('title') Update Puraches Instances @endsection

@section('content')
    <style>
      #myCheck:checked + #customaizeProjected_units{
      display: block !important;
      }
    </style>
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Update Puraches Instances</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('purchaseinstances.index') }}">Puraches Instances</a></li>
                        <li class="breadcrumb-item active">Update Puraches Instances</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>

    
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif

            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Track ID : {{$purchase['track_id']}}</h4>
                    <div align="right" style="margin-top: -5%;">
                        <form action="{{ url('createMore') }}" method="POST" style="margin-left: 40px;margin-top: -27px;">
                            @csrf
                            <input type="hidden" id="id" name="id" value="{{ $purchase['id'] }}">
                            <button type="submit" class="btn btn-info waves-effect waves-light mb-3">Create More + </button>
                        </form>
                    </div>
                    <div class="box">
                           
                            <table class="table table-striped table-bordered dt-responsive nowrap datatable" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead class="thead-light">
                                <tr>
                                    <th>No.</th>
                                    <th>Product</th>
                                    <th>Vendor/Supplier </th>
                                    <th>Warehouse</th>
                                    <th>Projected Units</th>
                                    <th>Cartons</th>
                                    <th>CBM</th>
                                    <th>Containers</th>
                                    <th>Subtotal</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $no = 1; ?>
                                    @if(!empty($purchase_details))
                                    @foreach($purchase_details as $users)
                                        @php($vendors_suppliers_name = ' - ')
                                        @php($get_warehouse_name = ' - ')
                                        @php($product_name = ' - ')

                                        @if($users['supplier'] != 0)
                                            @php($vendors_suppliers_name = get_suppliers_name($users['supplier']))
                                        @endif
                                        @if($users['vendor'] != 0)
                                            @php($vendors_suppliers_name = get_vendors_name($users['vendor']))
                                        @endif
                                        @if($users['warehouse'])
                                            @php($get_warehouse_name = get_warehouse_name($users['warehouse']))
                                        @endif
                                        @if($users['product_id'])
                                            @php($product_name = get_product_details($users['product_id']))
                                        @endif

                                        <tr id="row{{$users['id']}}">
                                            <td id="rows_no{{$users['id']}}">{{$no++}}</td>
                                            <td id="product_name_no{{$users['id']}}" title="{{$product_name}}">{{substr(strip_tags($product_name),0,25)."..."}}</td>
                                            <td id="product_suppliers_name{{$users['id']}}">{{$vendors_suppliers_name}}</td>
                                            <td id="warehouse{{$users['id']}}">{{$get_warehouse_name}}</td>
                                            <td id="projected_units{{$users['id']}}">{{$users['projected_units']}}</td>
                                            <td id="items_per_cartoons{{$users['id']}}">{{$users['items_per_cartoons']}}</td>
                                            <td id="cbm{{$users['id']}}">{{$users['cbm']}}</td>
                                            <td id="containers{{$users['id']}}">{{$users['containers']}}</td>
                                            <td id="subtotal{{$users['id']}}">{{$users['subtotal']}}</td>
                                            <td>
                                                <a class="btn btn-primary btn-sm btn-rounded waves-effect waves-light edit edit_row" data-id="{{$users['id']}}"  href="javascript:void(0);" id="edit_button{{$users['id']}}" ><i class="fas fa-edit"></i></a>
                                                <a id="save_button{{$users['id']}}" pi-id="{{$users['id']}}" product-id="{{$users['product_id']}}" value="{{$users['id']}}"  style=" display:none;width: 32px;" class="btn btn-primary btn-sm btn-rounded waves-effect waves-light save"  href="javascript:void(0);" ><i class="fas fa-save"></i></a>
                                                <a id="delete_button{{$users['id']}}" class="btn btn-danger btn-sm btn-rounded waves-effect waves-light delete" style=""  href="javascript:void(0);" onclick="delete_row({{$users['id']}})"><i class="fas fa-trash"></i></a>
                                            </td>
                                        </tr>

                                    @endforeach
                                @endif
                        
                                </tbody>
                            </table>
                        
                        </div>

                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->
    <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 600px;">
                <!-- Modal Header -->
                <div class="modal-header">

                    <h4 class="modal-title span7 text-left" id="myModalLabel"><span class="title">Projected Units</span></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row" align="center">
                        <div class="column" style="margin-right: 80px; margin-left: 80px">
                            <span style="font-size: 15px;color:black;">Projected Units</span>
                            <br><br><span id="projected_unit_span"></span>&nbsp;&nbsp;&nbsp;
                            <input type="checkbox" name="checkbox" id="projected_unit" value="" checked>
                        </div>
                        <div class="column" style="margin-right: 50px">
                            <span style="font-size: 15px;color:black;">Session projected Units</span>
                            <br><br><span id="session_projected_unit_span"></span>
                            &nbsp;&nbsp;&nbsp;<input type="checkbox" name="checkbox"  id="session_projected_unit">

                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="column" style="margin-right: 50px; margin-left: 66px">
                            <span style="font-size: 15px;color:black;">Daily Projected Units</span><br><br><span id="daily_projected_unit_span" style="margin-left: 53px;"></span>&nbsp;&nbsp;&nbsp;
                            <input type="checkbox" name="checkbox"  id="daily_projected_unit">
                        </div>
                        <div class="column" style="margin-right: 50px">
                            <span style="font-size: 15px;color:black;">Customize Projected Units</span><br><br>
                            <span id="" ></span>&nbsp;&nbsp;&nbsp;
                            <input type="checkbox" name="checkbox"  id="myCheck" class="custmaizeProjected Units">
                            <input type="text" name="customaizeProjected_units" id="customaizeProjected_units" style="display: none; float:left">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">

                    <button id="save" class="btn btn-primary save_row">Save</button>

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>


            </div>
        </div>
    </div>

@endsection

@section('script')


   <script>
$(document).ready(function() {
  /**
   * for showing edit item popup
   */

  $(document).on('click', ".save", function() {
    var pi_id =$(this).attr("pi-id");
    var pro_id =$(this).attr("product-id");
    var dataroletype = 'assign_model_data';
    $.ajax({
        type: "POST",
        url: '{{ route('purchaseinstances.store') }}',
        data: {
            pi_id : pi_id,
            pro_id : pro_id,
            input_type : dataroletype,
        },
        success: function (response) {
            $('#projected_unit_span').text(response.projected_units);
            $('#projected_unit').val(response.projected_units);
            $('#daily_projected_unit').val(response.daily_projected_units);
            $('#daily_projected_unit_span').text(response.daily_projected_units);
            $('#session_projected_unit').val(response.session_projected_units);
            $('#session_projected_unit_span').text(response.session_projected_units);
            $('#save').attr('data-id',pi_id);
            $('#pids').val(pi_id);
        }
    });
    $(this).addClass('edit-item-trigger-clicked'); //useful for identifying which trigger was clicked and consequently grab data from the correct row and not the wrong one.

    var options = {
      'backdrop': 'static'
    };
    $('#myModal').modal(options)
  })

  // on modal show
  $('#myModal').on('show.bs.modal', function() {
    var el = $(".edit-item-trigger-clicked"); // See how its usefull right here? 
    var row = el.closest(".data-row");

    // get the data
    var id = el.data('item-id');
    var name = row.children(".name").text();
    var description = row.children(".description").text();

    // fill the data in the input fields
    $("#modal-input-id").val(id);
    $("#modal-input-name").val(name);
    $("#modal-input-description").val(description);

  })

  // on modal hide
  $('#edit-modal').on('hide.bs.modal', function() {
    $('.edit-item-trigger-clicked').removeClass('edit-item-trigger-clicked')
    $("#edit-form").trigger("reset");
  })
})
          
       
          $(document).on('click', ".save_row", function() {
            var no=$(this).attr("data-id");
            $.each($("input[name='checkbox']:checked"), function() {
              if($('#session_projected_units').is(":checked")) {
                var projected_units = $('#session_projected_unit').val();
              }
              else if($('#myCheck').is(":checked"))
              {
                var projected_units=$('#customaizeProjected_units').val();
              }
              else
              {
                var projected_units = $(this).val();
              }

           var items_per_cartoons=document.getElementById("items_per_cartoons_text"+no).value;
           var cbm=document.getElementById("cbm_text"+no).value;
           var containers=document.getElementById("containers_text"+no).value;

           $.ajax({
               type:"POST",
               url:'{{route('purchaseinstances.store')}}',
               data:{
                   input_type : 'ajax_save_purchase_details',
                   projected_units:projected_units,
                   items_per_cartoons:items_per_cartoons,
                   cbm:cbm,
                   containers:containers,
                   id:no,
               },
               datatype:"json",
               success :function (res) {
                  if(res.error == 0){
                      location.reload();
                  }else{
                      $('.error_message').html(res.message);
                  }
               },
               error :function(){
                   console.log('something wrong ...');
               }
           });
           });
       });


       function delete_row(no)
       {
           $.ajax({
               type:"POST",
               url:'{{route('purchaseinstances.store')}}',
               data:{
                   input_type : 'delete_purchase_details',
                   id:no,
               },
               datatype:"json",
               success :function (res) {
                   if(res.error == 0){
                       var url = '{{ route("purchaseinstances.index") }}';
                       window.location.href=url;
                   }else{
                       $('.error_message').html(res.message);
                   }
               },
               error :function(){
                   console.log('something wrong ...');
               }
           });
       }
   </script>

   <script type="text/javascript">
     $(function () {

       $('.datatable').DataTable({
           "pageLength": 10,
           'autoWidth': false,
           "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
         
       });
   });

$("input:checkbox").on('click', function() {
  
  var $box = $(this);
  if ($box.is(":checked")) {
    var group = "input:checkbox[name='" + $box.attr("name") + "']";
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});

   </script>

@endsection
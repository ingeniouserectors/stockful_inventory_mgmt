@extends('layouts.master')

@section('title') Purchase Instance List @endsection

@section('content')
    <style>
        .disabled {
            pointer-events: none;
            cursor: default;
            cursor: no-drop;
        }
    </style>

    <div class="row">
        <div class="col-12">

            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Purchase Instance</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Purchase Instance</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="row">
        <div class="col-lg-6">
                <a class="btn btn-info waves-effect waves-light mb-3"  href="{{ route('purchaseinstances.create') }}" role="button">Create purchase Instance</a>
            
        </div>
    </div>
<div class="row">
  <div class="col-lg-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif           
    
    <!-- end row -->
  <div class="card">
        <div class="card-body" style="margin-bottom: 30px;">
             <h4 class="card-title mb-4">Purchase Instance List</h4>

            @if(!empty($Completed_Purchase_instances) || !empty($purchaseinstances_details))
                <ul class="nav nav-tabs nav-tabs-custom">
                    <li class="nav-item"> <a href="#pendding" class="nav-link active" data-toggle="tab"> Pending ({{ count($purchaseinstances_details) }}) </a></li>
                    <li class="nav-item"><a href="#completed" class="nav-link" data-toggle="tab"> Completed ({{ count($Completed_Purchase_instances) }})</a></li>
                </ul>
            @endif
            <div class="tab-content p-3 text-muted">
                <div class="tab-pane active" id="pendding">
                    <div class="box">
                    <div class="table-responsive">
                        <table id="datatables" class="table table-striped table-bordered dt-responsive nowrap datatable" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead class="thead-light">
                            <tr>
                                <th>No.</th>
                                <th>Date</th>
                                <th>Track Id</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

            <div class="tab-pane" id="completed">
                <div class="box">
                <div class="table-responsive">
                     <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap datatable" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead class="thead-light">
                        <tr>
                          <th>No.</th>
                            <th>Date</th>
                            <th>Track Id</th>
                           <!--  <th>Status</th> -->
                            <th>Action</th>
                        </tr>
                        </thead>
                     </table>
                      </div>
                </div>
            </div>
        </div>
            </div>
        </div>
    </div>
  </div>        

@endsection
@section('script')

<script>
   
     $(document).ready(function () {
            var id = $('#marketplaces').val();
                     loadDashboard(id);
        });
        function loadDashboard(id){
           var pendding_purchaseinstances_list='pendding_purchaseinstances_list';
            var count=1;
            var table = $('#datatables').DataTable({

                "order": [ 0, 'asc' ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('purchaseinstances.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.id=id,d.status=status,d.request_type=pendding_purchaseinstances_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                success:function($res){
                    console.log(res);
                    return false;
                },
                'columnDefs': [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return  row['created_at'];
                        }
                    },
                    {
                         targets: 2,
                        render: function (data, type, row) {
                            return row['track_id'];
                        }
                    },
                    
                     
                    {
                        targets: 3,
                        render: function (data, type, row) {
                           return row['action']
                        }
                    }
                ]
            });
        }
        $(document).ready(function () {
            var id = $('#marketplaces').val();
            
                     loadDashboards(id);
        });
        function loadDashboards(id){
           var comleted_purchaseinstances_list='comleted_purchaseinstances_list';
            var count=1;
            var table = $('#datatable').DataTable({

                "order": [ 0, 'asc' ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('purchaseinstances.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.id=id,d.status=status,d.request_type=comleted_purchaseinstances_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                success:function($res){
                    console.log(res);
                    return false;
                },
                'columnDefs': [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return  row['created_at'];
                        }
                    },
                    {
                         targets: 2,
                        render: function (data, type, row) {
                            return row['track_id'];
                        }
                    },
                     
                    {
                        targets: 3,
                        render: function (data, type, row) {
                           return row['action']
                        }
                    }
                ]
            });
        } 
 
</script>

<script>
$(document).on('click', '#button', function () {
    //e.preventDefault();
    var $ele = $(this).parent().parent();
    var id = $(this).data('id');
            var url = "{{URL('purchaseinstances')}}";
            var destroyurl = url+"/"+id;


    swal({
             title: "Are you sure?",
            text: "You want to delete this record",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
             
            $.ajax({
                type: "DELETE",
                url:destroyurl, 
                data:{ _token:'{{ csrf_token() }}'},
                dataType: "html",
                success: function (data) {
                    var dataResult = JSON.parse(data);
                if(dataResult.statusCode==200){
                    $ele.fadeOut().remove();
                               swal({
              title: "Done!",
              text: "It was succesfully deleted!",
              type: "success",
              timer: 700
           });
    
                      
                    } 
        }

         });
          }  
    
        else
        {
             swal("Cancelled", "", "error");
        }
            
       
    });

});
   
</script>
@endsection
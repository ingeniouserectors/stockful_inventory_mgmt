@extends('layouts.master')

@section('title') Create Purchase Instances @endsection

@section('content')
    <style>
        .design{
            cursor: no-drop;
            color: #74788d !important;
            background-color: #eff2f7 !important;
        }
        .error
        {
            color: red;
        }
    </style>

        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Create Purchase Instances</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('purchaseinstances.index') }}">Purchase Instances</a></li>
                            <li class="breadcrumb-item active">Create Purchase Instances</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-12">
                @if(session()->has('message'))
                    {!! session('message') !!}
                @endif
                <div class="card">
                    <div class="card-body">
                        <form name="create-puraches_instances" id="create-puraches_instances" action="{{ route('purchaseinstances.store') }}" method="POST" onreset="myFunction()">
                            @csrf
                            <input type="hidden" name="input_type" value="Create_form">
                            <input type="hidden" name="purchase_instance_id" value="{{isset($purchase_instance_id) && $purchase_instance_id != '' ? $purchase_instance_id : ''}}">
                           <div id="main">
                           <div id="portion1">
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Select Products</label>
                                <div class="col-md-2">
                                    <select class="product_listing custom-select js-delivery" name="product_id[]" id="product_id1" data-idcnt="1">
                                      <option value="">Select Products</option>
                                        @if(!empty($products))
                                            @foreach($products as $list)
                                                @if($list['mws_product']['prod_name'] !=  '' && $list['mws_product']['prod_name'] != '-')
                                                    <option value="{{ $list['mws_product']['id'] }}" title="{{$list['mws_product']['prod_name']}}">{{substr(strip_tags( $list['mws_product']['prod_name']),0,25)."..."}}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    </select>
                                </div>

                                <input type="hidden" name="vendors_select_id[]" id="vendors_select_id1" value="">
                                <input type="hidden" name="suppliers_select_id[]" id="suppliers_select_id1" value="">
                                <input type="hidden" name="warehouse_select_id[]" id="warehouse_select_id1" value="">
                                <input type="hidden" name="projected_units[]" id="projected_units1" value="">

                                <label class="col-md-2 col-form-label">Vendor/Supplier</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control design" value=" - " id="vendors_suppliers_name1" disabled>
                                </div>
                                <label class="col-md-2 col-form-label">Warehouse</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control design" value=" - " id="warehouse_name1" disabled>
                                </div>
                            </div>    
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Projected Units</label>
                                <div class="col-md-2">
                                    <input class="form-control allow_integer sub design" type="text" value=" -"  id="projected_unitss1" maxlength="9" disabled>
                                </div>
                                <label for="example-text-input" class="col-md-2 col-form-label">Cartoons</label>
                                <div class="col-md-2">
                                    <input class="form-control allow_integer sub design" type="text" value="{{ old('items_per_cartoons1') }}" name="items_per_cartoons[]" id="items_per_cartoons1" maxlength="9" readonly>
                                </div>
                                <label class="col-md-2 col-form-label">Containers</label>
                                <div class="col-md-2">
                                    <input class="form-control allow_integer sub" type="text" value="1" name="containers[]" id="containers1" maxlength="9">
                                </div>
                                <label for="containers" generated="true" class="error"></label>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">CBM</label>
                                <div class="col-md-2">
                                    <input class="form-control allow_float sub design" type="text" value="{{ old('cbm1') }}" name="cbm[]" id="cbm1" maxlength="9" readonly>
                                </div>
                                <input type="hidden" value="" name="default_cbm1" id="default_cbm1" >
                                <label class="col-md-2 col-form-label">Sub Total</label>
                                <div class="col-md-2">
                                    <input  class="form-control allow_float design" type="text" value="{{ old('subtotal1') }}" name="subtotal[]" id="subtotal1" maxlength="15" readonly>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                           </div>
                           </div>
                            <input type="hidden" name="cnt" id="cnt" value="1">
                            <input type="button" class="btn btn-primary" name="add" id="add" value="+ Add Product" onclick="addNewProduct()">
                            <div class="button-items mt-3">
                            <input class="btn btn-info checks_product_units" type="submit" value="Submit" id="submit">
                            <a class="btn btn-danger waves-effect waves-light" href="{{ route('purchaseinstances.index') }}" role="button">Cancel</a>
                            <input class="btn btn-warning" type="reset" value="Reset">
                        </div>

                        </form>
                    </div>
                </div>
            </div> <!-- end col -->
        </div>
        <!-- end row -->

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <b> Change Projected Unit  </b>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-md-3">Product name </label>
                        <label class="col-md-3">Current Projected Units</label>
                        <label class="col-md-3">Calculated Projected Units</label>
                    </div><br/>

                    <div class="display_all_products">

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

    </div>

@endsection

@section('script')
    <script>
function addNewProduct() {

    var cnt = $("#cnt").val();
    cnt++;
    var html='';
    html+='<div id="portion'+cnt+'"class="item"><hr><div class="form-group row"><label class="col-md-2 col-form-label">Select Products</label><div class="col-md-2"><select class="product_listing custom-select js-delivery " data-rule-required="true" data-msg-required="Please select product" name="product_id['+cnt+']" id="product_id'+cnt+'"><option value="">Select Products</option>';
    var product =[];
    product = <?php echo $products; ?>


    $.each(product, function( index, value ) {
            html += '<option value="' + value['mws_product']['id'] + '" title="' + value['mws_product']['prod_name'] + '">' + value['mws_product']['prod_name'].slice(0,25)+'...' + '</option>';
    });

    
    html+='</select></div>';
    html+='<input type="hidden" name="vendors_select_id['+cnt+']" id="vendors_select_id'+cnt+'" value="">';
    html+=' <input type="hidden" name="projected_units['+cnt+']" id="projected_unit'+cnt+'" value="">';
    html+='<input type="hidden" name="suppliers_select_id['+cnt+']" id="suppliers_select_id'+cnt+'" value="">';
    html+='<input type="hidden" name="warehouse_select_id['+cnt+']" id="warehouse_select_id'+cnt+'" value="">';
    html+='<input type="hidden" class="default_cbm" id="default_cbm'+cnt+'" value="">';
    html+='<label class="col-md-2 col-form-label">Vendor/Supplier</label><div class="col-md-2"> <input type="text" class="form-control design" value=" - " id="vendors_suppliers_name'+cnt+'" disabled></div>';
    html+='<label class="col-md-2 col-form-label">Warehouse</label> <div class="col-md-2"> <input type="text" class="form-control design" value=" - " id="warehouse_name'+cnt+'" disabled> </div></div>';
    html+='<span>';
    html+='<div class="form-group row mutlti"><label class="col-md-2 col-form-label">Projected Units</label> <div class="col-md-2"><input class="form-control allow_integer sub design one" type="text"  id="projected_units'+cnt+'" maxlength="9" value="-" data-rule-required="true" data-msg-required="Please enter projected units" disabled></div>';
    html+='<label for="example-text-input" class="col-md-2 col-form-label">Items/Cartoons</label> <div class="col-md-2"> <input class="form-control allow_integer sub design two" type="text"  name="items_per_cartoons['+cnt+']" id="items_per_cartoons'+cnt+'" maxlength="9" data-rule-required="true" data-msg-required="Please enter items per cartoons" readonly/> </div>';
    html+='<label class="col-md-2 col-form-label">Containers</label> <div class="col-md-2"> <input class="form-control  allow_integer three" type="text" value="1" name="containers['+cnt+']" id="containers2'+cnt+'" maxlength="9" data-rule-required="true" data-msg-required="Please enter containers" /></div></div>';
    html+='<div class="form-group row"> <label class="col-md-2 col-form-label">CBM</label> <div class="col-md-2"><input class="form-control allow_float four sub design" type="text"  name="cbm['+cnt+']" id="cbm'+cnt+'" maxlength="9" data-rule-required="true" data-msg-required="Please enter cbm" readonly /></div>';
    html+='<label class="col-md-2 col-form-label">Sub Total</label> <div class="col-md-2"> <input  class="form-control allow_float design five" type="text"  name="subtotal['+cnt+']" id="subtotal'+cnt+'" maxlength="15" data-rule-required="true" data-msg-required="Please enter subtotal" readonly /></div> <div class="col-md-2"> <input type="button" id="remove'+cnt+'" name="remove'+cnt+'" value="-" class="btn btn-danger" style="float: right" onclick="removeProduct('+cnt+')"> </div> </div></div>';
    html+='</span>';
    $("#main").append(html);
    $("#cnt").val(cnt);

     $('form.commentForm').validate();


     

}
function removeProduct(cnt) {
    $("#portion"+cnt).remove();
}
$(document).on('change','.product_listing',function(){
    var id=$(this).attr('id').match(/\d+/);
    var dataroletype = 'vendors_suppliers_warehouse';
    $.ajax({
        type: "POST",
        url: '{{ route('purchaseinstances.store') }}',
        data: {
            product_id : $(this).val(),
            input_type : dataroletype,
        },
        success: function (response) {
            $('#vendors_select_id'+id).val(response.vendors_id);
            $('#suppliers_select_id'+id).val(response.suppliers_id);
            $('#projected_unit'+id).val(response.projected_units);
            $('#projected_unitss'+id).val(response.projected_units);
            $('#projected_units'+id).val(response.projected_units);
            $('#warehouse_select_id'+id).val(response.warehouseid);
            $('#vendors_suppliers_name'+id).val(response.vendors_suppliers_name);
            $('#warehouse_name'+id).val(response.warehousename);
            $('#cbm'+id).val(response.cbm);
            $('#default_cbm'+id).val(response.cbm);
            $('#items_per_cartoons'+id).val(response.items_per_cartoons);


            var st = response.projected_units * response.items_per_cartoons  * response.cbm;
            if(isNaN(st)){ var stval = 0; }else{ var stval = st; }
            $('#subtotal'+id).val(Math.round(stval));   

        }
    });
});

</script>
<script>
   
$('#projected_units1, #items_per_cartoons1,#containers1,#cbm1').keyup(function(){
    var projected_units1 = parseFloat($('#projected_units1').val()) || 0;
    var items_per_cartoons1 = parseFloat($('#items_per_cartoons1').val()) || 0;
    var containers1 = parseFloat($('#containers1').val()) || 1;
    var cbm1 = parseFloat($('#cbm1').val()) * containers1 || 0;

    var final = parseFloat($('#default_cbm1').val()) * containers1 || parseFloat($('#default_cbm1').val());
    $('#cbm1').val(final);

    var st = projected_units1 * items_per_cartoons1  * containers1  * cbm1;
    if(isNaN(st)){ var stval = 0; }else{ var stval = st; }
    $('#subtotal1').val(Math.round(stval));    
});

$("form").on("keyup", "input", function () {  //use event delegation
  var data = $(this).closest(".item");  //from input find row
  var one = parseFloat(data.find(".one").val()) || 0;  //get first textbox
  var two = parseFloat(data.find(".two").val()) || 0;  //get second textbox
  var three = parseFloat(data.find(".three").val()) || 1;  //get second textbox

  var default_cbm = parseFloat(data.find(".default_cbm").val()) * three || parseFloat(data.find(".default_cbm").val());
  var default_cbm = data.find(".four").val(default_cbm) || 0;

  var four = parseFloat(data.find(".four").val()) || 0;  //get second textbox
  //var total = parseFloat(one * two * three * default_cbm) ;  //calculate total
  
  data.find(".five").val(Math.round(one * two * three * four));  //set value
});

$(document).on('click','.checks_product_units',function () {
    var products = []; var product_id = []; var projected_unit =[]; var product_name = [];
    var temp_pro = [];
    var ids_arr = [];
    $.each($(".product_listing"), function(){
        product_id.push($(this).attr('id'));
        products.push($(this).find('option:selected').val());
        var id=$(this).attr('id').match(/(\d+)/g);
        projected_unit.push($("#projected_units"+id).val());
        ids_arr.push(id);
        product_name.push($(this).find('option:selected').text());
    });

    $.each(projected_unit,function (index,value) {
        //alert("Units : "+index + " "+value);
    })

    var products_id = products.join(", ");
    var projected_units = projected_unit.join(", ");
    var product_name = product_name.join(", ");
    var ids_arr = ids_arr.join(", ");
    if(products_id.length > 0){
        var dataroletype = 'product_assign_calculated_qty';
        var html = '';
        $.ajax({
            type: "POST",
            url: '{{ route('purchaseinstances.store') }}',
            data: {
                product_id: products_id,
                projected_units: projected_units,
                product_name: product_name,
                input_type: dataroletype,
                ids_arr:ids_arr
            },
            async: false,
            success: function (response) {
               if(response.length != 0){
                    $.each(response,function (index,value) {
                        html += '<div class="form-group row">';
                        html += '<label class="col-md-3">'+value.product_namess+'</label>';
                        html += '<label class="col-md-3">'+value.projected_unitss+'</label>';
                        html += '<label class="col-md-3">'+value.change_units+'</label>';
                        html += '<label class="col-md-3 btn btn-primary btn-xs product_calculation" data-unique_id="'+value.uniqueid+'" id="product_id'+value.productid+'" data-caluted_qty="'+value.change_units+'" data-val="'+value.productid+'" type="button">Change</label>';
                        html += '</div>';
                    });
                    $('.display_all_products').html(html);
                    $('#myModal').modal('show');
                }
            },
        });
    }
});
$(document).on('click','.product_calculation',function(){
    var calculated = $(this).data('caluted_qty');
    var unique_id = $(this).data('unique_id');

   // function check_calculate(response,product_id,index){
        swal({
                title: "Are you sure?",
                text: "Your recalculated projected unit is "+calculated+", You want to chnage the projected units as recalculated",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Chnage it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    $('#projected_unitss'+unique_id).val(calculated);
                    $('#projected_units'+unique_id).val(calculated);// submitting the form when user press yes
                    $('#projected_unitss'+unique_id).attr('disabled','disabled');
                    swal("Confirmed", " Projected units changed successfully", "success");
                } else {
                    $('#projected_unitss'+unique_id).attr('disabled','disabled');
                    swal("Cancelled", "", "error");
                }
            });
    //}
});

function check_calculate(response,product_id,index){
    swal({
            title: "Are you sure?",
            text: "Your recalculated projected unit is "+response.calculate_qty+", You want to chnage the projected units as recalculated for product id "+product_id,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Chnage it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $('#projected_unitss'+index).val(response.calculate_qty);
                $('#projected_units'+index).val(response.calculate_qty);// submitting the form when user press yes
                $('#projected_unitss'+index).attr('disabled','disabled');
                swal("Confirmed", " Projected units changed successfully", "success");
            } else {
                $('#projected_unitss'+index).attr('disabled','disabled');
                swal("Cancelled", "", "error");
            }
        });
}

</script>


@endsection
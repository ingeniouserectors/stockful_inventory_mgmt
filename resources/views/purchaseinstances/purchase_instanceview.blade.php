@extends('layouts.master')

@section('title') Purchase Instance View @endsection

@section('content')


<style>
    .design{
         font-style:bold;
         font-size:14px;         

    }
    </style>

<style>
table, th, td {
  border: 2px solid ;
  border-collapse: collapse;
}
</style>    
    <div class="row">
        <div class="col-12">

            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Purchase Instance View</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('purchaseinstances.index')}}">Purchase Instance</a></li>
                        <li class="breadcrumb-item active"> Purchase Instance View</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
   <div class="row">
            <div class="col-12">
                @if(session()->has('message'))
                    {!! session('message') !!}
                @endif
                <div class="card">
                    @php($vendors_suppliers_name = ' - ')
                    @if(isset($view->purchase_instances_details->supplier) && $view->purchase_instances_details->supplier != 0)
                        @php($vendors_suppliers_name = get_suppliers_name($view->purchase_instances_details->supplier))
                    @endif
                    @if(isset($view->purchase_instances_details->vendor) && $view->purchase_instances_details->vendor != 0)
                        @php($vendors_suppliers_name = get_vendors_name($view->purchase_instances_details->vendor))
                    @endif
                    @php($warehouse_name = ' - ')
                    @if(isset($view->purchase_instances_details->warehouse) && $view->purchase_instances_details->warehouse != 0)
                        @php($warehouse_name = get_product_warehouse_name($view->purchase_instances_details->warehouse))
                    @endif

                    <div class="card-body">
                        
                            <!-- <div class="form-group row"> -->

                                
                                <div  style="margin-bottom: 10px;">
                                    <span style="font-size: 15px;color:black;">Date:</span>&nbsp;&nbsp;
                                        {{date('Y-m-d',strtotime($view['created_at']))}} &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                                   <span style="font-size: 15px;color:black;"> Track Id:</span>&nbsp;&nbsp;{{isset($view->track_id) && $view->track_id != '' ? $view->track_id : ''}} &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;

                                    <span style="font-size: 15px;color:black;">Status:</span>&nbsp;&nbsp;{{isset($view->status) && $view->status != '' && $view->status == 2 ? 'completed' : 'pending'}}

                            </div> 
              
                        <div class="box">
                        
                            <table class="table table-striped table-bordered dt-responsive nowrap datatable" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead class="thead-light">
                                <tr>
                                    <th>No.</th>
                                    <th>Product</th>
                                    <th>SKU</th>
                                    <th>Vendor/Supplier </th>
                                    <th>Warehouse</th>
                                    <th>Projected Units</th>
                                    <th>Items/Cartons</th>
                                    <th>CBM</th>
                                    <th>Containers</th>
                                    <th>Subtotal</th>
                                   
                                </tr>
                                </thead>
                                <tbody>
                                <?php $no = 1; ?>
                                    @if(!empty($purchase_details))
                                    @foreach($purchase_details as $users)
                                        @php($vendors_suppliers_name = ' - ')
                                        @php($get_warehouse_name = ' - ')
                                        @php($product_name = ' - ')

                                        @if($users['supplier'] != 0)
                                            @php($vendors_suppliers_name = get_suppliers_name($users['supplier']))
                                        @endif
                                        @if($users['vendor'] != 0)
                                            @php($vendors_suppliers_name = get_vendors_name($users['vendor']))
                                        @endif
                                        @if($users['warehouse'])
                                            @php($get_warehouse_name = get_warehouse_name($users['warehouse']))
                                        @endif
                                        @if($users['product_id'])
                                            @php($product_name = get_product_details($users['product_id']))
                                        @endif

                                        @php($product_sku = ' - ')
                                        @if(isset($order['product_id']) && $order['product_id'] != 0)
                                            @php($product_sku = get_product_sku($order['product_id']))
                                        @endif

                                        <tr id="row{{$users['id']}}">
                                            <td id="rows_no{{$users['id']}}">{{$no++}}</td>
                                            <td id="product_name_no{{$users['id']}}" title="{{$product_name}}">{{substr(strip_tags($product_name),0,25)."..."}}</td>
                                             <td>{{isset($product_sku) && $product_sku != '' ? $product_sku : ''}}</td>
                                            <td id="product_suppliers_name{{$users['id']}}">{{$vendors_suppliers_name}}</td>
                                            <td id="warehouse{{$users['id']}}">{{$get_warehouse_name}}</td>
                                            <td id="projected_units{{$users['id']}}">{{$users['projected_units']}}</td>
                                            <td id="items_per_cartoons{{$users['id']}}">{{$users['items_per_cartoons']}}</td>
                                            <td id="cbm{{$users['id']}}">{{$users['cbm']}}</td>
                                            <td id="containers{{$users['id']}}">{{$users['containers']}}</td>
                                            <td id="subtotal{{$users['id']}}">{{$users['subtotal']}}</td>
                                            
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                            <div class="button-items mt-3" align="center">
                            <a class="btn btn-info" href="{{ route('purchaseinstances.index') }}" role="button" style="width:116px;">Back</a>
                        </div>
                    </div>
                
            <!-- </div> -->
   </div>
@endsection
@section('script')

<script>
   $(function () {

       $('.datatable').DataTable({
           "pageLength": 10,
           'autoWidth': false,
           "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
       });
   });
</script>

@endsection

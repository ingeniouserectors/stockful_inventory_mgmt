@extends('layouts.master')

@section('title') Update Puraches Instances @endsection

@section('content')
    <style>
        .design{
            cursor: no-drop;
            color: #74788d !important;
            background-color: #eff2f7 !important;
        }
    </style>
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Update Puraches Instances</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('purchaseinstances.index') }}">Puraches Instances</a></li>
                        <li class="breadcrumb-item active">Update Puraches Instances</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">
                <div class="card-body">
                    <form name="edit-puraches_instances" id="edit-puraches_instances" action="{{ route('purchaseinstances.update',$id) }}" method="POST" onreset="myFunction()">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="input_type" value="Create_form">
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Select Products</label>
                            <div class="col-md-4">
                                <select class="product_listing custom-select js-delivery @error('product_id') is-invalid @enderror" name="product_id" id="product_id">
                                    <option value="">Select Products</option>
                                    @if(!empty($products))
                                        @foreach($products as $list)
                                            @if($list['mws_product']['prod_name'] !=  '' && $list['mws_product']['prod_name'] != '-')
                                                <option value="{{ $list['mws_product']['id'] }}" title="{{$list['mws_product']['prod_name']}}">{{substr(strip_tags( $list['mws_product']['prod_name']),0,25)."..."}}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <input type="hidden" name="vendors_select_id" id="vendors_select_id" value="">
                            <input type="hidden" name="suppliers_select_id" id="suppliers_select_id" value="">
                            <input type="hidden" name="warehouse_select_id" id="warehouse_select_id" value="">

                            <label class="col-md-2 col-form-label">Vendor/Supplier</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control design" value=" - " id="vendors_suppliers_name" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Warehouse</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control design" value=" - " id="warehouse_name" disabled>
                            </div>
                            <label class="col-md-2 col-form-label">Projected Units</label>
                            <div class="col-md-4">
                                <input class="form-control @error('projected_units') is-invalid @enderror" type="text" value="{{ isset($purchase_details->projected_units) && $purchase_details->projected_units != '' ? $purchase_details->projected_units : '' }}" name="projected_units" id="projected_units">
                                @if ($errors->has('projected_units'))
                                    <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('projected_units') }}</strong></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Items/Cartoons</label>
                            <div class="col-md-4">
                                <input class="form-control @error('items_per_cartoons') is-invalid @enderror" type="text" value="{{ isset($purchase_details->items_per_cartoons) && $purchase_details->items_per_cartoons != '' ? $purchase_details->items_per_cartoons : '' }}" name="items_per_cartoons" id="items_per_cartoons">
                                @if ($errors->has('items_per_cartoons'))
                                    <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('items_per_cartoons') }}</strong></span>
                                @endif
                            </div>
                            <label class="col-md-2 col-form-label">Containers</label>
                            <div class="col-md-4">
                                <input class="form-control @error('containers') is-invalid @enderror" type="text" value="{{ isset($purchase_details->containers) && $purchase_details->containers != '' ? $purchase_details->containers : '' }}" name="containers" id="containers">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('containers') }}</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">CBM</label>
                            <div class="col-md-4">
                                <input class="form-control @error('cbm') is-invalid @enderror " type="text" value="{{ isset($purchase_details->cbm) && $purchase_details->cbm != '' ? $purchase_details->cbm : '' }}" name="cbm" id="cbm">
                                @if ($errors->has('cbm'))
                                    <span class="invalid-feedback" role="alert"><strong class="errors">{{ $errors->first('cbm') }}</strong></span>
                                @endif
                            </div>
                            <label class="col-md-2 col-form-label">Sub Total</label>
                            <div class="col-md-4">
                                <input  class="form-control @error('subtotal') is-invalid @enderror" type="text" value="{{ isset($purchase_details->subtotal) && $purchase_details->subtotal != '' ? $purchase_details->subtotal: ''  }}" name="subtotal" id="subtotal" maxlength="15">
                                @if ($errors->has('subtotal'))
                                    <span class="invalid-feedback" role="alert"><strong class="errors">{{ $errors->first('subtotal') }}</strong></span>
                                @endif
                            </div>
                        </div>

                        <div class="button-items mt-3">
                            <input class="btn btn-info" type="submit" value="Submit" id="submit">
                            <a class="btn btn-danger waves-effect waves-light" href="{{ route('purchaseinstances.index') }}" role="button">Cancel</a>
                            <input class="btn btn-warning" type="reset" value="Reset">
                        </div>

                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->

@endsection

@section('script')
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script>

        $("#projected_units").keypress(function (e)
        {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
        $("#items_per_cartoons").keypress(function (e)
        {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
        $("#containers").keypress(function (e)
        {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
        $("#cbm").keypress(function (e)
        {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
        $("#subtotal").keypress(function (e)
        {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });

    </script>
    <script>
        $(document).ready(function(){
            var id = $('.product_listing').val();
            get_product_datas(id);
        });
        $(document).on('change','.product_listing',function() {
            var id = $(this).val();
            get_product_datas(id);
        });
        function get_product_datas(id){
            var dataroletype = 'vendors_suppliers_warehouse';
            $.ajax({
                type: "POST",
                url: '{{ route('purchaseinstances.store') }}',
                data: {
                    product_id : id,
                    input_type : dataroletype,
                },
                success: function (response) {
                    $('#vendors_select_id').val(response.vendors_id);
                    $('#suppliers_select_id').val(response.suppliers_id);
                    $('#warehouse_select_id').val(response.warehouseid);
                    $('#vendors_suppliers_name').val(response.vendors_suppliers_name);
                    $('#warehouse_name').val(response.warehousename);
                }
            });
        }
    </script>


    <script>
   $(function () {

       $('#datatables').DataTable({
           "pageLength": 10,
           'autoWidth': false,
           "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
       });
   });
</script>

@endsection
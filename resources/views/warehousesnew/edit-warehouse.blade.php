@extends('layouts.master')

@section('title') Edit Warehouse @endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Edit Warehouse</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('warehouses.index')}}">Warehouse</a></li>
                        <li class="breadcrumb-item active">Edit warehouse</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">
                <div class="card-body">

                    <form name="edit-warhouse" id="edit-warhouse" action="{{route('warehouses.update',$warhouse_details['id'])}}" method="POST" onreset="myFunction()">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="insert_type" value="create_form">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Type</label>
                            <div class="col-md-4">
                                <select class="custom-select" name="type" id="type">
                                    <option value="1" {{ $warhouse_details->type == 1 ? 'selected' : '' }}>Self</option>
                                    <option value="2" {{ $warhouse_details->type == 2 ? 'selected' : '' }}>Third Party</option>
                                    
                                </select>
                            </div>

                            <input type="hidden" name="marketplace_id" id="marketplace_id" value="{{session('MARKETPLACE_ID')}}">

                            <label for="example-text-input" class="col-md-2 col-form-label">Warehouse Name</label>
                            <div class="col-md-4 city">
                                <input class="form-control" type="text" name="warehouse_name" id="warehouse_name" value="{{$warhouse_details['warehouse_name']}}">
                            </div>

                        </div>

                        <div class="form-group row">

                            <label for="example-text-input" class="col-md-2 col-form-label">Address line 1</label>
                            <div class="col-md-4 city">
                                <input class="form-control" type="text" name="address_line_1" id="address" value="{{$warhouse_details['address_line_1']}}">
                            </div>
                            <label for="example-email-input" class="col-md-2 col-form-label">Address line 2</label>
                            <div class="col-md-4">
                                <input class="form-control" type="text" name="address_line_2" value="{{$warhouse_details['address_line_2']}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-tel-input"  class="col-md-2 col-form-label country">Country</label>
                            <div class="col-md-4 country">
                                <select name="country" id="country" class="form-control">
                                    @foreach($country as $key=> $newCountry)
                                        <option value=" {{ $key }}" @if($newCountry== $warhouse_details['country_id']) selected @endif>{{ $key." (". $newCountry." )"  }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label city">State</label>
                             <div class="col-md-4 country">
                                <select name="state" id="state" class="form-control">
                                    @if(!empty($state_list))
                                    @foreach($state_list as $key=> $state_list)
                                        <option value=" {{ $key }}" @if($key== $warhouse_details['state']) selected @endif>{{ $key." (". $state_list." )"  }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-tel-input"  class="col-md-2 col-form-label phone">Cities</label>
                            <div class="col-md-4 phone">
                                <select name="city" id="city" class="form-control">
                                    @if(!empty($city_list))
                                    @foreach($city_list as $key=> $city)
                                        <option value="{{ $key }}" @if($key== $warhouse_details['city']) selected @endif>{{ $key." (". $city." )"  }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label state">Zip Code</label>
                            <div class="col-md-4 state">
                                <input class="form-control allow_integer" type="text" name="zipcode" id="zipcode" value="{{$warhouse_details['zipcode']}}" maxlength="6">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="example-tel-input"  class="col-md-2 col-form-label primary_first_name">Primary Fistname</label>
                            <div class="col-md-4 primary_first_name">
                                <input  class="form-control" type="text" name="primary_first_name" value="{{$warhouse_details['primary_first_name']}}">
                            </div>

                            <label for="example-text-input" class="col-md-2 col-form-label primary_last_name">Primary Lastname</label>
                            <div class="col-md-4 primary_last_name">
                                <input class="form-control" type="text" name="primary_last_name" value="{{$warhouse_details['primary_last_name']}}" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-tel-input"  class="col-md-2 col-form-label primary_email" >Primary Email</label>
                            <div class="col-md-4 primary_email">
                                <input  class="form-control" type="email" name="primary_email" value="{{$warhouse_details['primary_email']}}">
                            </div>
                        </div>

                        <div class="button-items mt-3">
                            <input class="btn btn-info" type="submit" value="Submit" id="submit">
                            <a class="btn btn-danger waves-effect waves-light" href="{{ route('warehouses.index') }}" role="button">Cancel</a>
                            <input class="btn btn-warning" type="reset" value="Reset">
                        </div>
                    </form>

                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->

@endsection

@section('script')
<script>
    $(document).ready(function(){
        $("select").change(function(){
            check();
        }).change();
        check();
    });
    
    function check() {
      var data= $('#type').val();
         if(data=="1")  {
             $(".primary_first_name").hide();
             $(".primary_last_name").hide();
             $(".primary_email").hide();
             $('.primary_first_name').rules('remove',  {
                 required : true,
                 messages : { required : 'Firstname is required' }
             });
             $('.primary_last_name').rules('remove',  {
                 required : true,
                 messages : { required : 'Lastname is required' }
             });
             $('.primary_email').rules('remove',  {
                 required : true,
                 messages : { required : 'Email is required' }
             });
             
         } 
         else 
         {
             
             $(".primary_first_name").show();
             $(".primary_last_name").show();
             $(".primary_email").show();

                 $('.primary_first_name').rules('add',  {
                     required : true,
                     messages : { required : 'Firstname number is required' }
                 });
                 $('.primary_last_name').rules('add',  {
                     required : true,
                     messages : { required : 'Lastname is required' }
                 });
                 $('.primary_email').rules('add',  {
                     required : true,
                     messages : { required : 'Email is required' }
                 });
            }
        
    }             
           
     
    </script>
<script type="text/javascript">
    
  $('#country').change(function(){
    var country = $(this).val();   
    if(country){
        $.ajax({
           type:"GET",
           url:"{{url('get_state_list')}}",
           data:{country:country},
           success:function(res){               
            if(res){
                $("#state").empty();
                $("#state").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#state").append('<option value="'+key+'">'+key+'('+value+')'+'</option>');
                });
           
            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
        $("#city").empty();
    }      
   });
    $('#state').on('change',function(){
    var state = $(this).val();   
    if(state){
        $.ajax({
           type:"GET",
           url:"{{url('get_city_list')}}",
           data:{state:state},
           success:function(res){               
            if(res){
                $("#city").empty();
                $("#city").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#city").append('<option value="'+key+'">'+key+'('+value+')'+'</option>');
                });
           
            }else{
               $("#city").empty();
            }
           }
        });
    }else{
        $("#city").empty();
    }
        
   });    
</script>
@endsection
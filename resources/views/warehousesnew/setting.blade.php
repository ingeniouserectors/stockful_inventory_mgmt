@extends('layouts.master')

@section('title') Warehouse Setting @endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Warehouse Setting</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('warehouses.index')}}">Warehoouse</a></li>
                        <li class="breadcrumb-item active">Warehouse Settings</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">
                <div class="card-body">

                    <form name="main-warehouse-settings" id="main-warehouse-settings" action="{{ route('warehouses.update',$id) }}" method="POST" onreset="myFunction()">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="insert_type" value="setting_form">
                        <input type="hidden" name="warehouse_id" value="{{$id}}">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Lead Time</label>
                            <div class="col-md-4">
                                <input class="form-control allow_integer" type="text" name="lead_time" id="lead_time" value="{{ $warehouse_setting['lead_time'] }}" maxlength="3">
                            </div>
                        </div>
                        <div class="button-items mt-3">
                            <input class="btn btn-info" type="submit" value="Submit" id="submit">
                            <a class="btn btn-danger waves-effect waves-light" href="{{ route('warehouses.index') }}" role="button">Cancel</a>
                            <input class="btn btn-warning" type="reset" value="Reset">
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->

@endsection

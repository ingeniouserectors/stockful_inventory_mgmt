@extends('layouts.master')

@section('title') User List @endsection

@section('content')

@php(@$userRole = \App\Models\Users::with('user_assigned_role')->select('users.id')->where(array('id' => Auth::user()->id))->first())
@php(@$finalRole = $userRole['user_assigned_role']['user_role_id'])

    <style>
    .success-icon::before{
    content: '+';    
    height: 14px;
    width: 14px;
    display: block;
    position: absolute;
    color: white;
    border: 2px solid white;
    border-radius: 14px;
    box-shadow: 0 0 3px #444;
    box-sizing: content-box;
    text-align: center;
    text-indent: 0 !important;
    font-family:'Times New Roman', Times, serif;
    line-height: 14px;
    
    background-color: #0275d8;}
    .failer-icon::after{
     content: '-';       
     height: 14px;
    width: 14px;
    display: block;
    position: absolute;
    color: white;
    border: 2px solid white;
    border-radius: 14px;
    box-shadow: 0 0 3px #444;
    box-sizing: content-box;
    text-align: center;
    text-indent: 0 !important;
    font-family: 'Courier New', Courier, monospace;
    line-height: 14px;       
    background-color: #d33333;
        }
        td .fancybox-button{background: rgba(222, 216, 216, 0.6);display: inline-block; width: 120px; height: auto; margin: 5px !important;}
        td .fancybox-button[css-attr="video"]{width: 220px;}
        .tdname span{margin:0;padding:5px 0 0 0;}
        .table-news-image{height:100px; width:100px; object-fit:cover; display:inline-block; padding:5px;}
        .table-news-text{max-height:200px; overflow:auto;}
    </style>
        <!-- start page title -->

        <div class="row">
            <div class="col-12">
                 
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Users list</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Users list</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            @php($userRoleId = get_user_role(Auth::user()->id))
            @if($create_access == 1)
                @if($user_access_create_details == 1)
                    <div class="col-lg-6">
                        <a class="btn btn-info waves-effect waves-light mb-3"  href="{{ route('users.create') }}" role="button"><?php echo $userRoleId['user_assigned_role']['user_role_id'] == 3 ? 'Create Sub Member': 'Create New User';?></a>
                    </div>
                    <div class="col-lg-6"></div>
                @endif
            @else
                @if($user_access_create_details == 1)
                    <div class="col-lg-6">
                        <a class="btn btn-info waves-effect waves-light mb-3"  href="{{ route('users.create') }}" role="button"><?php echo $userRoleId['user_assigned_role']['user_role_id'] == 3 ? 'Create Sub Member': 'Create New User';?></a>
                    </div>
                    <div class="col-lg-6"></div>
                @endif
            @endif

        </div>
        <div class="row">
            <div class="col-lg-12">
                <span class="error_message"></span>
                @if(session()->has('message'))
                    {!! session('message') !!}
                @endif
                <div class="card">

                    <div class="card-body" style="margin-bottom: 30px;">
                        
                       <!--  <div class="nav-tabs-custom"> -->
                       
                        

                        @if(!empty($adminusers_details) || !empty($memberusers_details) || !empty($affiliateusers_details))
                        <ul class="nav nav-tabs nav-tabs-custom">
                             <!--<li class="active"><a href="#online" data-toggle="tab">Users ({{ count($users_details) }})</a></li>&nbsp;&nbsp;-->
                            <li class="nav-item"><a href="#admins" class="nav-link active admins" data-toggle="tab"> Admin ({{ count($adminusers_details) }})</a></li>
                            <li class="nav-item"> <a href="#members" class="nav-link" data-toggle="tab"> Member ({{ count($memberusers_details) }}) </a></li>
                            <li class="nav-item"><a href="#offline" class="nav-link" data-toggle="tab"> Affiliate ({{ count($affiliateusers_details)}})</a></li>
                        </ul>
                        @endif
                        
                    <!-- </div> -->

                    @if(!empty($adminusers_details) || !empty($memberusers_details) || !empty($affiliateusers_details))
                        <div class="tab-content p-3 text-muted">

                        <div class="tab-pane active" id="admins">
                            <div class="box">
                            <div class="table-responsive">
                                @php($userRoleId = get_user_role(Auth::user()->id))
                                <input type="hidden" name="user_id" value="{{Auth::user()->id}}" id="user_id">
                                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap " style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead class="thead-light">
                                    <tr>
                                        <th>No.</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Role</th>

                                         @if(@$finalRole == 1)
                                            <th>IsActive</th>
                                        @endif
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>

                            <div class="tab-pane" id="offline">
                            <div class="box">
                            <div class="table-responsive">
                                <table id="fdatatables" class="table table-striped table-bordered dt-responsive nowrap " style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead class="thead-light">
                                    <tr>
                                        <th>No.</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Commission</th>
                                        @if(@$finalRole == 1)
                                            <th>IsActive</th>
                                        @endif
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>

                            <div class="tab-pane" id="members" value="members">
                                <div class="box">
                            <div class="table-responsive">
                                <table id="mdatatables" class="table table-striped table-bordered dt-responsive nowrap " style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead class="thead-light">
                                    <tr>
                                         <th></th>
                                        <th>No.</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Commission</th>
                                         @if(@$finalRole == 1)
                                            <th>IsActive</th>
                                        @endif
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                </table>
                                    </div>
                                </div>
                            </div>
                     @else
                            <div class="box">
                                <div class="table-responsive">
                                    <table id="Udatatable" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                        <thead class="thead-light">
                                        <tr>
                                            <th>No.</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Role</th>
                                            @if(@$finalRole == 3)
                                                <th>IsActive</th>
                                            @endif
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                     @endif
                     <!-- end table-responsive -->
                    </div>
                </div>
                </div>
                </div>
            </div>
        </div>
        <!-- end row -->

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                   <b> Apply Commission </b>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <form id="commissionFrom" action="" method="POST">
                            @csrf
                        <input type="hidden" name="cu_id" id="cu_id" value="">
                        <input type="hidden" name="input_type" value="commission_update">
                            <div class="form-group row">
                                <label class="col-md-6">Enter Commission : </label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name="commission" id="commission" value="" placeholder="Enter Commission">
                                </div>
                            </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary btnAdd" id="btnAdd" data-dismiss="modal">Submit</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </form>
            </div>

        </div>
    </div>
@endsection
@section('script')

<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    function updateActiveStatus(activeStatus,id) {
        var user_type = 'updatestatus';
        $.ajax({
            type: "POST",
            url: '{{ route('users.store') }}',
            data: {
                active_status : activeStatus,
                id : id,
                user_type : user_type,
            },
            success: function (response) {
            }
        });
    }
</script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $(document).on('click','.view_media',function(){
        var news_id = $(this).attr('data-val');
        var $obj = $(this);
        $tr = $(this).parents('tr');
        var data_class = $("#row_"+news_id).find("i").hasClass('success-icon');
        console.log(data_class);
        if(data_class === true) {
            if (news_id != '') {
                var user_type = 'show_sub_members';
                $.ajax({
                    type: "POST",
                    url: '{{ route('users.store') }}',
                    data:{news_id:news_id,user_type:user_type},
                    dataType: "json",
                    beforeSend: function () {
                        $(".view_media").find("i").removeClass("success-icon").addClass("failer-icon");
                        $(".view_media").find("i").removeClass("success-icon").addClass("failer-icon");
                        $(".view_media").find("i").not($obj.find("i")).removeClass("failer-icon").addClass("success-icon");
                        $(".view_media").find("i").not($obj.find("i")).removeClass("failer-icon").addClass("success-icon");
                        $('.newstr').remove();
                    },
                    success: function (res) {

                        $("#wait").css("display", "none");
                        if (typeof res.error != 'undefined' && res.error == 0) {
                            $tr.after('<tr class="media_' + news_id + ' newstr"><td colspan="6">' + res.view + '</td><tr>');    
                            $obj.find("i").addClass("failer-icon");
                            $obj.find("i").removeClass("success-icon");

                            $obj.find("i").addClass("failer-icon");
                            $obj.find("i").removeClass("success-icon");

                        } else if (typeof res.error != 'undefined' && res.error == 1) {
                            alert(res.msg);
                        }
                    }
                });
            }
        }else{
            $("#row_").find("i").hasClass('success-icon');
            $obj.find("i").addClass("success-icon");
            $obj.find("i").removeClass("failer-icon");
            $obj.find("i").addClass("success-icon");
            $obj.find("i").removeClass("failer-icon");
            $(".media_"+news_id).toggle();
        }
    });



    </script>
    
    <script>
 @if(@$finalRole == 2)
$(document).ready(function () {
    
                     loadDashboards();
        });
        function loadDashboards(){
             id=$('#user_id').val();
              var dt = $('#datatable').DataTable();
             // alert(dt);      
           
           var admin_users_list='admin_users_list';
            var count=1;
            var table = $('#datatable').DataTable({

                "order": [ 0, 'asc' ],
                dom: 'Bfrtip',
                buttons:[
                'csvHtml5'  
                ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('users.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.request_type=admin_users_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                success:function($res){
                    console.log(res);
                    return false;
                },
                'columnDefs': [
                

                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return  row['name'];
                        }
                    },
                    {
                         targets: 2,
                        render: function (data, type, row) {
                            return row['email'];
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, row) {
                            return row['role'];

                        }
                    },

                    
                    {
                        targets: 4,
                        render: function (data, type, row) {
                            return row['action'];
                        }
                    }
                ]
            });
        }
        @endif
$(document).ready(function () {
   
    
    
                     loadDashboard();
        });
        function loadDashboard(){
             id=$('#user_id').val();
              var dt = $('#datatable').DataTable();
             // alert(dt);


             
           
           var admin_users_list='admin_users_list';
            var count=1;
            var table = $('#datatable').DataTable({

                "order": [ 0, 'asc' ],
                dom: 'Bfrtip',
                buttons:[
                'csvHtml5'  
                ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "language": {
                                 },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('users.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.request_type=admin_users_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                success:function($res){
                    console.log(res);
                    return false;
                },
                'columnDefs': [
                

                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return  row['name'];
                        }
                    },
                    {
                         targets: 2,
                        render: function (data, type, row) {
                            return row['email'];
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, row) {
                            return row['role'];

                        }
                    },

                    {

                        targets: 4,
                        render: function (data, type, row) {

                            return row['active'];
                        }
                    },

                    {
                        targets: 5,
                        render: function (data, type, row) {
                            return row['action'];
                        }
                    }
                ]
            });
        }

</script>
<script type="text/javascript">
@if(@$finalRole == 2)    
$(document).ready(function () {
            
                     MemerloadDashboards();
        });
        function MemerloadDashboards(){
           var member_users_list='member_users_list';
            var count=1;
            var table = $('#mdatatables').DataTable({

                "order": [ 0, 'asc' ],
                dom: 'Bfrtip',
                buttons:[
                'csvHtml5'  
                ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('users.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.request_type=member_users_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                success:function($res){
                    console.log(res);
                    return false;
                },
                'columnDefs': [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return  row[''];
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 2,
                        render: function (data, type, row) {
                            return  row['name'];
                        }
                    },
                    {
                         targets: 3,
                        render: function (data, type, row) {
                            return row['email'];
                        }
                    },
                    {
                        targets: 4,
                        render: function (data, type, row) {
                            return row['role'];

                        }
                    },
                    {
                        targets: 5,
                        render: function (data, type, row) {
                            return row['commission'];

                        }
                    },
                    
                    {
                        targets: 6,
                        render: function (data, type, row) {
                            return row['action'];
                        }
                    }
                ]
            });
        } 
 @endif
 </script>
 <script type="text/javascript"> 
 @if(@$finalRole == 1)         
$(document).ready(function () {
                    
                     MemerloadDashboard();
        });
        function MemerloadDashboard(){
           var member_users_list='member_users_list';
            var count=1;
            var table = $('#mdatatables').DataTable({

                "order": [ 0, 'asc' ],
                dom: 'Bfrtip',
                buttons:[
                'csvHtml5'  
                ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('users.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.request_type=member_users_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                success:function($res){
                    console.log(res);
                    return false;
                },
                'columnDefs': [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return  row[''];
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 2,
                        render: function (data, type, row) {
                            return  row['name'];
                        }
                    },
                    {
                         targets: 3,
                        render: function (data, type, row) {
                            return row['email'];
                        }
                    },
                    {
                        targets: 4,
                        render: function (data, type, row) {
                            return row['role'];

                        }
                    },
                    {
                        targets: 5,
                        render: function (data, type, row) {
                            return row['commission'];

                        }
                    },
                    {
                        targets: 6,
                        render: function (data, type, row) {
                            return row['active'];
                        }
                    },
                    {
                        targets: 7,
                        render: function (data, type, row) {
                            return row['action'];
                        }
                    }
                ]
            });
        }
@endif        
</script>
<script type="text/javascript">

@if(@$finalRole == 2)
$(document).ready(function () {
                        
            AffiliateloadDashboards();
        });
        function AffiliateloadDashboards(){
           var affiliate_users_list='affiliate_users_list';
            var count=1;
            var table = $('#fdatatables').DataTable({

                "order": [ 0, 'asc' ],
                dom: 'Bfrtip',
                buttons:[
                'csvHtml5'  
                ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('users.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.request_type=affiliate_users_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                success:function($res){
                    console.log(res);
                    return false;
                },
                'columnDefs': [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return  row['name'];
                        }
                    },
                    {
                         targets: 2,
                        render: function (data, type, row) {
                            return row['email'];
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, row) {
                            return row['role'];

                        }
                    },

                    {
                        targets: 4,
                        render: function (data, type, row) {
                            return row['commission'];

                        }
                    },
                    
                    {
                        targets: 5,
                        render: function (data, type, row) {
                            return row['action'];
                        }
                    }
                ]
            });
        }
 @endif      
@if(@$finalRole == 1)  
$(document).ready(function () {
            
            AffiliateloadDashboard();
        });
        function AffiliateloadDashboard(){
           var affiliate_users_list='affiliate_users_list';
            var count=1;
            var table = $('#fdatatables').DataTable({

                "order": [ 0, 'asc' ],
                dom: 'Bfrtip',
                buttons:[
                'csvHtml5'  
                ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('users.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.request_type=affiliate_users_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                success:function($res){
                    console.log(res);
                    return false;
                },
                'columnDefs': [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return  row['name'];
                        }
                    },
                    {
                         targets: 2,
                        render: function (data, type, row) {
                            return row['email'];
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, row) {
                            return row['role'];

                        }
                    },
                    {
                        targets: 4,
                        render: function (data, type, row) {
                            return row['commission'];

                        }
                    },
                    {
                        targets: 5,
                        render: function (data, type, row) {
                            return row['active'];
                        }
                    },
                    {
                        targets: 6,
                        render: function (data, type, row) {
                            return row['action'];
                        }
                    }
                ]
            });
        }    
@endif   
    </script>
    <script type="text/javascript">

  @if(@$finalRole == 3)      
    $(document).ready(function () {
            
                     UserloadDashboards();
        });
        function UserloadDashboards(){
           var users_list='users_list';
            var count=1;
            var table = $('#Udatatable').DataTable({

                "order": [ 0, 'asc' ],
                dom: 'Bfrtip',
                buttons:[
                'csvHtml5'  
                ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('users.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.request_type=users_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                success:function($res){
                    console.log(res);
                    return false;
                },
                'columnDefs': [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return  row['name'];
                        }
                    },
                    {
                         targets: 2,
                        render: function (data, type, row) {
                            return row['email'];
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, row) {
                            return row['role'];

                        }
                    },
                    {
                        targets: 4,
                        render: function (data, type, row) {
                            return row['active'];

                        }
                    },
                    
                    {
                        targets: 5,
                        render: function (data, type, row) {
                            return row['action'];
                        }
                    }
                ]
            });
        }
 @endif
@if(@$finalRole !== 3)
    $(document).ready(function () {
            
                     UserloadDashboard();
        });
        function UserloadDashboard(){
           var users_list='users_list';
            var count=1;
            var table = $('#Udatatable').DataTable({

                "order": [ 0, 'asc' ],
                dom: 'Bfrtip',
                buttons:[
                'csvHtml5'  
                ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('users.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.request_type=users_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                success:function($res){
                    console.log(res);
                    return false;
                },
                'columnDefs': [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return  row['name'];
                        }
                    },
                    {
                         targets: 2,
                        render: function (data, type, row) {
                            return row['email'];
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, row) {
                            return row['role'];

                        }
                    },
                    
                    {
                        targets: 4,
                        render: function (data, type, row) {
                            return row['action'];
                        }
                    }
                ]
            });
        }        
    
   @endif             

    </script>
    <script>
$(document).on('click', '#button', function () {
    //e.preventDefault();
    var $ele = $(this).parent().parent();
    var id = $(this).data('id');
            var url = "{{URL('users')}}";
            var destroyurl = url+"/"+id;


    swal({
             title: "Are you sure?",
            text: "You want to delete this record",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
             
            $.ajax({
                type: "DELETE",
                url:destroyurl, 
                data:{ _token:'{{ csrf_token() }}'},
                dataType: "html",
                success: function (data) {
                    var dataResult = JSON.parse(data);
                if(dataResult.statusCode==200){
                    $ele.fadeOut().remove();
                               swal({
              title: "Done!",
              text: "It was succesfully deleted!",
              type: "success",
              timer: 700
           });
    
                      
                    } 
        }

         });
          }  
    
        else
        {
             swal("Cancelled", "", "error");
        }
            
       
    });

});

function Cmodel($userId,$oldcms){
    $('#myModal').modal('show');
    $('#cu_id').val($userId);
    $('#commission').val($oldcms);
}

$(document).ready(function(){

   

    $(".btnAdd").click(function(){

        
        var insert_type = 'commission_update';
        var commission= $('#commission').val();
        var user_id = $("#cu_id").val();
       
        if(commission ==''){
            $('#commission').addClass('error');
            return false;
        }else {
         
            $.ajax({
                type: "POST",
                url: '{{ route('users.store') }}',
                data: {insert_type: insert_type, user_id: user_id, commission: commission},
                datatype: "json",
                success: function (res) {
                    $('#myModal').modal('hide');
                    $('.error_message').html(res.message);
                    $("html, body").animate({scrollTop: 0}, "slow");
                    setTimeout(function(){ $('.error_message').html(""); }, 5000);

                    var role = "{{@$finalRole}}";
                    if(role == "1"){
                        MemerloadDashboard();
                        AffiliateloadDashboard();
                    }else if(role == "2"){
                        MemerloadDashboards();
                        AffiliateloadDashboards();
                    }

                },

                error: function () {
                    console.log('Something wrong');
                }
            });

        }
    });
});

</script>

@endsection
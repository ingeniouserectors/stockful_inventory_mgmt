<div class="card">
    <div class="card-body">
        <h4 class="card-title mb-4">Sub User List</h4>
        <div class="row">
            @if($create_access == 1)
            <div class="col-lg-6">
                <a class="btn btn-info waves-effect waves-light mb-3"  href="{{ url('sub_user_create/'.$main_parent_id) }}" role="button"> Create New Sub User</a>
            </div>
            <div class="col-lg-6"></div>
            @endif
        </div>
        <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap users-datatable" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                <thead class="thead-light">
                <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                     @if(Auth::user()->id == 1)
                        <th>IsActive</th>
                    @endif
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>

                <?php $no = 1; ?>
                @if(!empty($user_data))
                    @foreach($user_data as $users)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $users['name'] }}</td>
                            <td>{{ $users['email'] }}</td>
                            <td>{{ @$users['user_roles'][0]['role'] }}</td>
                             @if(Auth::user()->id == 1)
                                <td>
                                    <div class="square-switch mt-2">
                                        <input type="checkbox" id="square-access-{{ $users['id'] }}" value="{{$users['active']}}"  switch="bool" @if($users['active'] == 1) checked @endif onclick="updateActiveStatus({{ $users['active'] }},{{ $users['id'] }})"/>
                                        <label for="square-access-{{ $users['id'] }}" data-on-label="YES" data-off-label="NO"></label>
                                    </div>
                                </td>
                                @endif
                            <td>
                               
                                    @if($edit_access == 1)
                                        <a class="btn btn-primary btn-sm btn-rounded waves-effect waves-light"  href="{{ route('users.edit',\App\Services\UrlService::base64UrlEncode($users['id'])) }}" role="button"><i class="fas fa-edit"></i></a>
                                    @endif
                                     <form action="{{ route('users.destroy',$users['id']) }}" method="POST" onclick="archiveFunction()" style="display: inline;">
                                    @csrf
                                    @method('DELETE')
                                    @if($delete_access == 1)
                                        <button type="submit" class="btn btn-danger btn-sm btn-rounded waves-effect waves-light"><i class="fas fa-trash-alt"></i></button>
                                    @endif
                                </form>
                            </td>
                        </tr>

                    @endforeach
                 @else
                    <td colspan="6" align="center">
                        No sub member found.
                    </td>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

    <script>
    
 function archiveFunction() {
event.preventDefault(); // prevent form submit
var form = event.target.form; // storing the form
        swal({
  title: "Are you sure?",
  text: "You want to delete this record",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Yes, Delete it!",
  cancelButtonText: "No, cancel please!",
  closeOnConfirm: false,
  closeOnCancel: false
},
function(isConfirm){
  if (isConfirm) {
    form.submit();          // submitting the form when user press yes
  } else {
    swal("Cancelled", "", "error");
  }
});
}
    </script>
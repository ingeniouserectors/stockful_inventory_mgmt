@extends('layouts.master')

@section('title') Supply Order Logic @endsection

@section('content')    
    <div class="row">
        <div class="col-12">

            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Supply Order Logic</h4>
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Supply Order Logic</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    @php($get_check_setting = get_check_access('supply_order_logic_module'))
    @php($get_usercheck_access = get_user_check_access('supply_order_logic_module', 'create'))

    @if($get_check_setting == 1)
        @if($get_usercheck_access == 1)
            <div class="row">
                <div class="col-lg-6">
                    <a class="btn btn-info waves-effect waves-light mb-3"  href="{{ route('supplyorderlogic.create') }}" role="button">Create Logic</a>
                </div>
            </div>
        @endif
    @else
        @if($get_usercheck_access == 1)
            <div class="row">
                <div class="col-lg-6">
                    <a class="btn btn-info waves-effect waves-light mb-3"  href="{{ route('supplyorderlogic.create') }}" role="button">Create Logic</a>
                </div>
            </div>
        @endif
    @endif

    <div class="row">
        <div class="col-lg-12">
            <span class="error_message"></span>
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">

            </div>
        </div>
    </div>

    <!-- end row -->

    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-4">Supply Order Logic List</h4>
            <div class="table-responsive">
                <table id="datatables" class="table table-striped table-bordered dt-responsive nowrap users-datatable" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead class="thead-light">
                    <tr>
                        <th>No.</th>
                        <th>Logic Label Name</th>
                        <th>Action</th>
                       
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
<div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                   <b> Apply Order logic </b>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                      <form name="create-product_assign_supply_logic" id="create-product_assign_supply_logic">
                            @csrf
                        <input type="hidden" name="logic_label_id" id="logic_label_id" value="" >
                           <input type="hidden" id="" name="" value="">
                          <input type="hidden" name="input_type" value="Creates_form">
                          <div class="col-md-12">

                    </div><br><br>
                          <div class="col-md-12">           
                                <select class="products product_select" name="selectedproduct" id="product_id" multiple="multiple" style="width: 33.75em;">
                                @foreach($product_list as $data)                                 
                            <option value="{{$data['id']}}">{{substr(strip_tags( $data['prod_name']),0,50)."..."}}</option>
                         @endforeach
                        </select>
                         <span id="error"></span>
                     </div>
                 <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btnAdd" id="btnAdd" data-dismiss="modal">Assign</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
            </div>

        </div>
    </div>

    </div>
 
@endsection
@section('script')
    <script>
    $(document).ready(function () {
                     loadDashboard();

        });
        function loadDashboard(){
           var supply_order_logic_list='supply_order_logic_list';
            var count=1;
            var table = $('#datatables').DataTable({

                "order": [ 0, 'asc' ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": '{{ route('supplyorderlogic.store') }}',
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.request_type=supply_order_logic_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                success:function($res){
                    console.log(res);
                    return false;
                },
                'columnDefs': [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return  row['logic_label_name'];
                        }
                    },
                    {
                        targets: 2,
                        render: function (data, type, row) {
                            return  row['action'];
                        }
                    }

                    
                ]
            });
        }
</script>
<script>
$(document).on('click', '#button', function () {
    //e.preventDefault();
    var $ele = $(this).parent().parent();
    var id = $(this).data('id');
            var url = "{{URL('supplyorderlogic')}}";
            var destroyurl = url+"/"+id;


    swal({
             title: "Are you sure?",
            text: "You want to delete this record",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
             
            $.ajax({
                type: "DELETE",
                url:destroyurl, 
                data:{ _token:'{{ csrf_token() }}'},
                dataType: "html",
                success: function (data) {
                    var dataResult = JSON.parse(data);
                if(dataResult.statusCode==200){
                    $ele.fadeOut().remove();
                               swal({
              title: "Done!",
              text: "It was succesfully deleted!",
              type: "success",
              timer: 700
           });
    
                      
                    } 
        }

         });
          }  
    
        else
        {
             swal("Cancelled", "", "error");
        }
            
       
    });

});


$(document).on('click','.get_lableid',function () {
   var logic_label_id =$(this).attr('data-product_id');
   $('#logic_label_id').val(logic_label_id);

});

$(".btnAdd").click(function(){
    var input_type = 'Creates_form';
    var pro_id= $('#product_id').val();
    var logic_label_id= $('#logic_label_id').val();
    var error = document.getElementById("error") 
    if(pro_id ==''){
                 error.textContent = "Please select product";
                 error.style.color = "red";
                 $("#error").load(location.href + " #error");
                 return false;
            }else {
                $.ajax({
                    type: "POST",
                    url: '{{ route('supplyorderlogic.store') }}',
                    data: {input_type: input_type, logic_label_id: logic_label_id, pro_id: pro_id},
                    datatype: "json",
                    success: function (res) {
                        $('.error_message').html(res.message);
                        $("html, body").animate({scrollTop: 0}, "slow");
                        setTimeout(function(){ $('.error_message').html(""); }, 5000);
                        $("#product_id").val(null).trigger("change");                    
                    },
                    error: function () {
                        console.log('Something wrong');
                    }
                });
            }       
           
    });
</script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
$(document).ready(function() {
    $('.products').select2();

});
</script>
@endsection
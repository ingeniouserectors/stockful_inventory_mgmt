@extends('layouts.master')

@section('title') Edit Supply Order Logic @endsection
<style>
    .adjust
        {
            float:right;
             margin-top: -16px;
        }
</style>
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Edit Supply Order Logic</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('supplyorderlogic.index')}}">Supply Order Logic</a></li>
                        <li class="breadcrumb-item active">Edit Supply Order Logic</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <span class="error_message"></span>
            <div class="card">
                <div class="card-body">

                    <form name="order_logic" id="order_logic" action="{{ route('supplyorderlogic.update',$supply_rorder_logic_details['id']) }}" method="POST" onreset="myFunction()">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="user_marketplace_id" id="user_marketplace_id" value="{{session('MARKETPLACE_ID')}}">

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Logic Label Name</label>
                            <div class="col-md-4">
                                <input class="form-control" type="text" name="logic_label_name" id="logic_label_name" value="{{$supply_rorder_logic_details['logic_label_name']}}">
                            </div>
                        </div>
                         <b>Supply History in Percentage(%)</b><br/><br/>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Last Year</label>
                            <div class="col-md-4">
                                <input type="range" name="supply_last_year_sales_percentage" id="supply_last_year_sales_percentage" value="{{$supply_rorder_logic_details['supply_last_year_sales_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage supply_last_year_sales_percentage adjust">0.00%</span>
                            </div>
                        </div>
                        <b>Supply Recent Sales in Percentage(%)</b><br/><br/>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Last 7 Days</label>
                            <div class="col-md-4">
                                <input type="range" name="supply_recent_last_7_day_sales_percentage" id="supply_recent_last_7_day_sales_percentage" value="{{$supply_rorder_logic_details['supply_recent_last_7_day_sales_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage supply_recent_last_7_day_sales_percentage adjust">0.00%</span>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">Last 14 Days</label>
                            <div class="col-md-4">
                                <input type="range" name="supply_recent_last_14_day_sales_percentage" id="supply_recent_last_14_day_sales_percentage" value="{{$supply_rorder_logic_details['supply_recent_last_14_day_sales_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage supply_recent_last_14_day_sales_percentage adjust">0.00%</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Last 30 Days </label>
                            <div class="col-md-4">
                                <input type="range" name="supply_recent_last_30_day_sales_percentage" id="supply_recent_last_30_day_sales_percentage" value="{{$supply_rorder_logic_details['supply_recent_last_30_day_sales_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage supply_recent_last_30_day_sales_percentage adjust">0.00%</span>
                            </div>
                        </div>

                        <b>Supply Trends in Percentage(%)</b><br/><br/>


                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Year Over Year Historical</label>
                            <div class="col-md-4">
                                 <input type="range" name="supply_trends_year_over_year_historical_percentage" id="supply_trends_year_over_year_historical_percentage" value="{{$supply_rorder_logic_details['supply_trends_year_over_year_historical_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage supply_trends_year_over_year_historical_percentage adjust">0.00%</span>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">Year Over Year Current</label>
                            <div class="col-md-4">
                                <input type="range" name="supply_trends_year_over_year_current_percentage" id="supply_trends_year_over_year_current_percentage" value="{{$supply_rorder_logic_details['supply_trends_year_over_year_current_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage supply_trends_year_over_year_current_percentage adjust">0.00%</span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Multi Month</label>
                            <div class="col-md-4">
                                 <input type="range" name="supply_trends_multi_month_trend_percentage" id="supply_trends_multi_month_trend_percentage" value="{{$supply_rorder_logic_details['supply_trends_multi_month_trend_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage supply_trends_multi_month_trend_percentage adjust">0.00%</span>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">30 Over 30 Days</label>
                            <div class="col-md-4">
                                <input type="range" name="supply_trends_30_over_30_days_percentage" id="supply_trends_30_over_30_days_percentage" value="{{$supply_rorder_logic_details['supply_trends_30_over_30_days_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage supply_trends_30_over_30_days_percentage adjust">0.00%</span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Multi Week </label>
                            <div class="col-md-4">
                                 <input type="range" name="supply_trends_multi_week_trend_percentage" id="supply_trends_multi_week_trend_percentage" value="{{$supply_rorder_logic_details['supply_trends_multi_week_trend_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage supply_trends_multi_week_trend_percentage adjust">0.00%</span>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">7 Over 7 Days </label>
                            <div class="col-md-4">
                                <input type="range" name="supply_trends_7_over_7_days_percentage" id="supply_trends_7_over_7_days_percentage" value="{{$supply_rorder_logic_details['supply_trends_7_over_7_days_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage supply_trends_7_over_7_days_percentage adjust">0.00%</span>
                            </div>
                        </div>
                         <b>Supply History in Percentage(%)</b><br/><br/>
                         <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Last Year</label>
                            <div class="col-md-4">
                                 <input type="range" name="reorder_last_year_sales_percentage" id="reorder_last_year_sales_percentage" value="{{$supply_rorder_logic_details['reorder_last_year_sales_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage reorder_last_year_sales_percentage adjust">0.00%</span>
                            </div>
                        </div>
                        <b>Reorder Recent Sales in Percentage(%)</b><br/><br/>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Last 7 Days</label>
                            <div class="col-md-4">
                                <input type="range" name="reorder_recent_last_7_day_sales_percentage" id="reorder_recent_last_7_day_sales_percentage" value="{{$supply_rorder_logic_details['reorder_recent_last_7_day_sales_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage reorder_recent_last_7_day_sales_percentage adjust">0.00%</span>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">Last 14 Days</label>
                            <div class="col-md-4">
                                 <input type="range" name="reorder_recent_last_14_day_sales_percentage" id="reorder_recent_last_14_day_sales_percentage" value="{{$supply_rorder_logic_details['reorder_recent_last_14_day_sales_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage reorder_recent_last_14_day_sales_percentage adjust">0.00%</span>
                            </div>
                        </div>
                         <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Last 30 Days </label>
                            <div class="col-md-4">
                                <input type="range" name="reorder_recent_last_30_day_sales_percentage" id="reorder_recent_last_30_day_sales_percentage" value="{{$supply_rorder_logic_details['reorder_recent_last_30_day_sales_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage reorder_recent_last_30_day_sales_percentage adjust">0.00%</span>
                            </div>
                        </div>

                        <b>Reorder Trends in Percentage(%)</b><br/><br/>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Year Over Year Historical</label>
                            <div class="col-md-4">
                                 <input type="range" name="reorder_trends_year_over_year_historical_percentage" id="reorder_trends_year_over_year_historical_percentage" value="{{$supply_rorder_logic_details['reorder_trends_year_over_year_historical_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage reorder_trends_year_over_year_historical_percentage adjust">0.00%</span>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">Year Over Year Current</label>
                            <div class="col-md-4">
                                <input type="range" name="reorder_trends_year_over_year_current_percentage" id="reorder_trends_year_over_year_current_percentage" value="{{$supply_rorder_logic_details['reorder_trends_year_over_year_current_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage reorder_trends_year_over_year_current_percentage adjust">0.00%</span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Multi Month</label>
                            <div class="col-md-4">
                                 <input type="range" name="reorder_trends_multi_month_trend_percentage" id="reorder_trends_multi_month_trend_percentage" value="{{$supply_rorder_logic_details['reorder_trends_multi_month_trend_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage reorder_trends_multi_month_trend_percentage adjust">0.00%</span>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">30 Over 30 Days</label>
                            <div class="col-md-4">
                                <input type="range" name="reorder_trends_30_over_30_days_percentage" id="reorder_trends_30_over_30_days_percentage" value="{{$supply_rorder_logic_details['reorder_trends_30_over_30_days_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage reorder_trends_30_over_30_days_percentage adjust">0.00%</span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Multi Week </label>
                            <div class="col-md-4">
                                 <input type="range" name="reorder_trends_multi_week_trend_percentage" id="reorder_trends_multi_week_trend_percentage" value="{{$supply_rorder_logic_details['reorder_trends_multi_week_trend_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage reorder_trends_multi_week_trend_percentage adjust">0.00%</span>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">7 Over 7 Days</label>
                            <div class="col-md-4">
                                <input type="range" name="reorder_trends_7_over_7_days_percentage" id="reorder_trends_7_over_7_days_percentage" value="{{$supply_rorder_logic_details['reorder_trends_7_over_7_days_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage reorder_trends_7_over_7_days_percentage adjust">0.00%</span>
                            </div>
                        </div>
                         <b>Stock in Percentage(%)</b><br/><br/>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Safety Stock </label>
                            <div class="col-md-4">
                                 <input type="range" name="reorder_safety_stock_percentage" id="reorder_safety_stock_percentage" value="{{$supply_rorder_logic_details['reorder_safety_stock_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage reorder_safety_stock_percentage adjust">0.00%</span>
                            </div>
                        </div>


                        <div class="button-items mt-3">
                            <input class="btn btn-info check_totals" type="submit" value="Submit" id="submit">
                            <a class="btn btn-danger waves-effect waves-light" href="{{ route('supplyorderlogic.index') }}" role="button">Cancel</a>
                            <input class="btn btn-warning reset_button" type="button" value="Reset">
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->
@endsection
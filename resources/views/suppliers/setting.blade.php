@extends('layouts.master')

@section('title') Suppliers Setting @endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Suppliers Setting</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('suppliers.index')}}">Suppliers</a></li>
                        <li class="breadcrumb-item active">Suppliers Settings</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">
                <div class="card-body">

                    <form name="setting-supplier-inside" id="setting-supplier-inside" action="{{ route('suppliers.update',$id) }}" method="POST" onreset="myFunction()">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="insert_type" value="setting_form">
                        <input type="hidden" name="supplier_id" value="{{$id}}">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Lead Time</label>
                            <div class="col-md-4">
                                <input class="form-control allow_integer" type="text" name="lead_time" id="lead_time" value="{{ $supplier_setting['lead_time'] }}" maxlength="3">
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">Order Volume</label>
                            <div class="col-md-4">
                                <input class="form-control allow_integer" type="text" name="order_volume" id="order_volume" value="{{ $supplier_setting['order_volume'] }}" maxlength="3">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Quantity Discount</label>
                            <div class="col-md-4">
                                <input class="form-control allow_integer" type="text" name="quantity_discount" id="quantity_discount" value="{{ $supplier_setting['quantity_discount'] }}" maxlength="3">
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">MOQ</label>
                            <div class="col-md-4">
                                <input class="form-control allow_integer" type="text" name="moq" id="moq" value="{{ $supplier_setting['moq'] }}" maxlength="3">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">CBM Per Container</label>
                            <div class="col-md-4">
                                <input class="form-control allow_integer" type="text" name="CBM_Per_Container" id="CBM_Per_Container" value="{{ $supplier_setting['CBM_Per_Container'] }}" maxlength="3">
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">Production Time</label>
                            <div class="col-md-4">
                                <input class="form-control allow_integer" type="text" name="Production_Time" id="Production_Time" value="{{ $supplier_setting['Production_Time'] }}" maxlength="3">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Boat To Port</label>
                            <div class="col-md-4">
                                <input class="form-control allow_integer" type="text" name="Boat_To_Port" id="Boat_To_Port " value="{{ $supplier_setting['Boat_To_Port'] }}" maxlength="3">
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">Port To Warehouse</label>
                            <div class="col-md-4">
                                <input class="form-control allow_integer" type="text" name="Port_To_Warehouse" id="Port_To_Warehouse allow_integer" value="{{ $supplier_setting['Port_To_Warehouse'] }}" maxlength="3">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Warehouse Receipt</label>
                            <div class="col-md-4">
                                <select class="custom-select" name="Warehouse_Receipt" id="Warehouse_Receipt">
                                    <option value="0">Select Warehouse Receipt</option>
                                </select>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">Ship To Specific Warehouse</label>
                            <div class="col-md-4">
                                <select class="custom-select" name="Ship_To_Specific_Warehouse" id="Ship_To_Specific_Warehouse">
                                    <option value="">Select Specific Warehouse</option>
                                    @if($warehouse)
                                        @foreach($warehouse as $house)
                                            <option value="{{$house['id']}}" {{$house['id'] == $supplier_setting['Ship_To_Specific_Warehouse'] ? 'selected="selected"' : '' }}>{{$house['warehouse_name']}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="button-items mt-3">
                            <input class="btn btn-info" type="submit" value="Submit" id="submit">
                            <a class="btn btn-danger waves-effect waves-light" href="{{ route('suppliers.index') }}" role="button">Cancel</a>
                            <input class="btn btn-warning" type="reset" value="Reset">
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->

@endsection
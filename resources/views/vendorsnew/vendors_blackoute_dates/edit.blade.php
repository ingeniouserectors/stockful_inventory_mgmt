@extends('layouts.master')

@section('title') Edit Vendors Blackoute Date @endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Edit Vendors Blackoute Date</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('vendors.index')}}">Vendors</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('vendor_blackoute_date.show',$vendors_blackoute_dates_details['vendor_id']) }}">Vendors Blackoute Date</a></li>
                        <li class="breadcrumb-item active">Edit Vendors Blackoute Date</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">
                <div class="card-body">

                    <form name="edit-vendor_blackoute_date" id="edit-vendor_blackoute_date" action="{{ route('vendor_blackoute_date.update',$vendors_blackoute_dates_details['id']) }}" method="POST" onreset="myFunction()">
                        @csrf
                         @method('PUT')
                        <input type="hidden" name="insert_type" value="create_form">
                        <input type="hidden" name="vendor_id" value="{{$vendors_blackoute_dates_details['vendor_id']}}">

                        <div class="form-group row">
                            <label for="example-tel-input"  class="col-md-2 col-form-label phone">Blackout Date</label>
                            <div class="col-md-4 phone">
                                <input  class="form-control min-today" type="date" max="3000-01-01" name="blackout_date" id="blackout_date" value="{{$vendors_blackoute_dates_details['blackout_date']}}" >
                            </div>
                                 <label for="example-tel-input"  class="col-form-label phone">Reason:</label>
                            <div class="col-md-4 phone">
                                <input  class="form-control " type="text"   name="reason" id="reason" value="{{$vendors_blackoute_dates_details['reason']}}" >
                            </div>
                        </div>    
                        <div class="button-items mt-3">
                            <input class="btn btn-info" type="submit" value="Submit" id="submit">
                            <a class="btn btn-danger waves-effect waves-light" href="{{ route('vendor_blackoute_date.show',$vendors_blackoute_dates_details['vendor_id']) }}" role="button">Cancel</a>
                            <input class="btn btn-warning" type="reset" value="Reset">
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->

@endsection

@section('script')

    <script>
  
          function myFunction() {
          var x = document.getElementById("blackout_date").max;
          document.getElementById().innerHTML = x;
        }

        $(function(){
            $('[type="date"].min-today').prop('min', function(){
                return new Date().toJSON().split('T')[0];
            });
        });     
    </script>
@endsection
@extends('layouts.master-new')

@section('title') Create vendors @endsection

@section('content')



                <div class="suppliers-top-main-box">
                    <span id="messages"></span>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="suppliers-first-column">
                                <h2>Vendors</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="suppliers-main-box suppliers-main-box-scroll">
                    <div class="row">
                        <div class="col-lg-12">
                            <nav class="suppliers-nav-box">
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <a class="nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home"
                                       role="tab" aria-controls="nav-home" aria-selected="true">Basic Details</a>
                                    <a class="nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile"
                                       role="tab" aria-controls="nav-profile" aria-selected="false">Contacts</a>
                                    <a class="nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-setting"
                                       role="tab" aria-controls="nav-setting" aria-selected="false">Settings</a>
                                    <a class="nav-link" id="nav-black-tab" data-toggle="tab" href="#nav-black"
                                       role="tab" aria-controls="nav-black" aria-selected="false">Blackout Dates</a>
                                </div>
                            </nav>
                            <div class="row">
                                <div class="col-lg-10 pt-5">
                                    <div class="tab-content" id="nav-tabContent">
                                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                             aria-labelledby="nav-home-tab">
                                            <form class="main-address-from vendors-tab-01" id="forms1">
                                                <input type="hidden" id="vendors_id" value="">
                                                <input type="hidden" id="marketplace_id" value="{{session('MARKETPLACE_ID')}}">
                                                <div class="suppliers-address-box">
                                                    <ul
                                                            class="d-flex flex-row align-items-center justify-content-between pb-3">
                                                        <li>vendor Name</li>
                                                        <li>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="vendor_name" required placeholder="Enter vendor name" required>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="suppliers-address-box">
                                                    <ul
                                                            class="d-flex flex-row align-items-start justify-content-between pb-3">
                                                        <li>Address (optional) </li>
                                                        <li>
                                                            <div class="form-group pb-3">
                                                                <input class="form-control" type="text" name="address_line_1" id="address_line_1" placeholder="Enter Address Line">
                                                            </div>
                                                            <div class="form-group">
                                                                <input class="form-control" type="text" name="address_line_2" id="address_line_2" placeholder="Enter Address Line Two">
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="suppliers-address-box">
                                                    <ul
                                                            class="d-flex flex-row align-items-center justify-content-between pb-3">
                                                        <li>Country</li>
                                                        <li>
                                                            <div class="form-group country">
                                                                <select name="country" id="country" class="form-control" required>
                                                                    <option value="">Select Country</option>
                                                                    @foreach($country_list as  $key=> $country)
                                                                        <option value=" {{ $key }}">{{ $key."(".$country.")" }}</option>
                                                                    @endforeach
                                                                </select>

                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="suppliers-address-box">
                                                    <ul
                                                            class="d-flex flex-row align-items-center justify-content-between pb-3">
                                                        <li>State</li>
                                                        <li>
                                                            <div class="form-group state">
                                                                <select name="state" id="state" class="form-control" required>
                                                                </select>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="suppliers-address-box">
                                                    <ul
                                                            class="d-flex flex-row align-items-center justify-content-between pb-3">
                                                        <li>City</li>
                                                        <li>
                                                            <div class="form-group city">
                                                                <select name="city" id="city" class="form-control" required>
                                                                </select>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="suppliers-address-box">
                                                    <ul
                                                            class="d-flex flex-row align-items-center justify-content-between pb-3">
                                                        <li>Postal Code (optional) </li>
                                                        <li>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="zipcode" id="zipcode" maxlength="6" placeholder="Enter Postal Code" required>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>


                                                <ul class="d-flex flex-row justify-content-between">
                                                    <li>
                                                        <button type="submit" class="btn address-cancel-btn">Cancel</button>
                                                    </li>
                                                    <li>
                                                        <button type="button" class="btn btn-primary save_datas" data-val="1">Save</button>
                                                        <button type="button" class="btn address-save-btn save_datas" data-val="2">Save And Next</button>
                                                    </li>
                                                </ul>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade" id="nav-profile" role="tabpanel"
                                             aria-labelledby="nav-profile-tab">
                                            <div class="suplier-table-box">
                                                <table class="table table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col" width="20%">First Name <img
                                                                    src="assets/img/table-up-down-two.png" alt="">
                                                        </th>
                                                        <th scope="col" width="20%">Last Name <img
                                                                    src="assets/img/table-up-down-two.png" alt="">
                                                        </th>
                                                        <th scope="col" width="20%">
                                                            <ul
                                                                    class="d-flex flex-row align-items-center table-checkbox-main">
                                                                <li>
                                                                    <span>Title</span>
                                                                    <img src="assets/img/table-up-down.png"
                                                                         alt="">
                                                                </li>
                                                            </ul>
                                                        </th>
                                                        <th scope="col" width="20%">Email <span
                                                                    style="color: #227CFF">[?]</span> <img
                                                                    src="assets/img/table-up-down-two.png" alt="">
                                                        </th>
                                                        <th scope="col" width="20%">Phone Number <img
                                                                    src="assets/img/table-up-down-two.png" alt="">
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div id="table" class="table-editable">
                                                            <div class="table-text-selector table-add">
                                                                <p>Click to add first contact <a href="#"><span
                                                                                class="mr-1 ml-2">
                                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                                     width="11.25" height="11.25"
                                                                                     viewBox="0 0 11.25 11.25">
                                                                                    <g id="Icon_ionic-ios-add-circle-outline"
                                                                                       data-name="Icon ionic-ios-add-circle-outline"
                                                                                       transform="translate(0.25 0.25)">
                                                                                        <path id="Path_308"
                                                                                              data-name="Path 308"
                                                                                              d="M15.6,12.85H13.677V10.925a.413.413,0,0,0-.827,0V12.85H10.925a.4.4,0,0,0-.413.413.4.4,0,0,0,.413.413H12.85V15.6a.4.4,0,0,0,.413.413.411.411,0,0,0,.413-.413V13.677H15.6a.413.413,0,1,0,0-.827Z"
                                                                                              transform="translate(-7.889 -7.889)"
                                                                                              fill="#227cff"
                                                                                              stroke="#227cff"
                                                                                              stroke-width="0.5" />
                                                                                        <path id="Path_309"
                                                                                              data-name="Path 309"
                                                                                              d="M8.75,4.1A4.65,4.65,0,1,1,5.46,5.46,4.621,4.621,0,0,1,8.75,4.1m0-.724A5.375,5.375,0,1,0,14.125,8.75,5.374,5.374,0,0,0,8.75,3.375Z"
                                                                                              transform="translate(-3.375 -3.375)"
                                                                                              fill="#227cff"
                                                                                              stroke="#227cff"
                                                                                              stroke-width="0.5" />
                                                                                    </g>
                                                                                </svg>
                                                                            </span><i style="font-style: normal;">Add
                                                                            Contacts</i></a></p>
                                                            </div>
                                                            <table class="table table-striped vendors-tab-rows">
                                                                <tbody>
                                                                <tr>
                                                                   <input type="hidden" id="vendors_contact_id" value="">
                                                                    <th scope="row" width="20%"
                                                                         contenteditable="true">
                                                                    <ul
                                                                            class="d-flex flex-row align-items-center table-checkbox-main">
                                                                        <li class="first_name">
                                                                            Josh
                                                                        </li>
                                                                    </ul>
                                                                </th>
                                                                <td contenteditable="true" width="20%" class="last_name">
                                                                    Bochner</td>
                                                                <td contenteditable="true" width="20%" class="title">
                                                                    Title 2</td>
                                                                <td contenteditable="true" width="20%" class="emails">
                                                                    joshbochner@gmail.com
                                                                </td>
                                                                <td width="20%" contenteditable="true">
                                                                    <ul
                                                                            class="d-flex justify-content-between align-items-center">
                                                                        <li><span class="phone_number">+00 - 1234567890</span>
                                                                        </li>
                                                                        <li class="delete-btn-vendors-tab">
                                                                            <i class="fas fa-trash-alt"></i>
                                                                        </li>
                                                                    </ul>
                                                                </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                            <div class="table-add my-3">
                                                                <ul class="table-footer-add-btn">
                                                                    <li>
                                                                        <p><a href="#">
                                                                                    <span class="mr-1 ml-2">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                                             width="11.25" height="11.25"
                                                                                             viewBox="0 0 11.25 11.25">
                                                                                            <g id="Icon_ionic-ios-add-circle-outline"
                                                                                               data-name="Icon ionic-ios-add-circle-outline"
                                                                                               transform="translate(0.25 0.25)">
                                                                                                <path id="Path_308"
                                                                                                      data-name="Path 308"
                                                                                                      d="M15.6,12.85H13.677V10.925a.413.413,0,0,0-.827,0V12.85H10.925a.4.4,0,0,0-.413.413.4.4,0,0,0,.413.413H12.85V15.6a.4.4,0,0,0,.413.413.411.411,0,0,0,.413-.413V13.677H15.6a.413.413,0,1,0,0-.827Z"
                                                                                                      transform="translate(-7.889 -7.889)"
                                                                                                      fill="#227cff"
                                                                                                      stroke="#227cff"
                                                                                                      stroke-width="0.5" />
                                                                                                <path id="Path_309"
                                                                                                      data-name="Path 309"
                                                                                                      d="M8.75,4.1A4.65,4.65,0,1,1,5.46,5.46,4.621,4.621,0,0,1,8.75,4.1m0-.724A5.375,5.375,0,1,0,14.125,8.75,5.374,5.374,0,0,0,8.75,3.375Z"
                                                                                                      transform="translate(-3.375 -3.375)"
                                                                                                      fill="#227cff"
                                                                                                      stroke="#227cff"
                                                                                                      stroke-width="0.5" />
                                                                                            </g>
                                                                                        </svg>
                                                                                    </span><i
                                                                                        style="font-style: normal;">Add
                                                                                    Contacts</i>
                                                                            </a>
                                                                        </p>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <ul
                                                                class="d-flex flex-row justify-content-between address-btn-box">
                                                            <li>
                                                                <button type="submit"
                                                                        class="btn address-cancel-btn">Cancel</button>
                                                            </li>
                                                            <li>
                                                                <button type="button"
                                                                        class="btn btn-primary save_contacts">Save</button>
                                                                <button type="submit"
                                                                        class="btn address-save-btn save_contacts">Save And
                                                                    Next</button>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <!-- Editable table -->
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="nav-setting" role="tabpanel"
                                             aria-labelledby="nav-setting-tab">
                                            <div class="setting-vendors-form">
                                                <div class="setting-form-box">
                                                    <ul class="d-flex justify-content-between align-items-center mb-3">
                                                        <li>
                                                            <span>Lead Time</span>
                                                        </li>
                                                        <li>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="lead_time" placeholder="Days">
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <ul class="d-flex justify-content-between align-items-center mb-3">
                                                        <li>
                                                            <span>Order Volume</span>
                                                        </li>
                                                        <li>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="Volume" id="order_volume">
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    {{--<ul--}}
                                                            {{--class="d-flex justify-content-between align-items-center mb-3">--}}
                                                        {{--<li>--}}
                                                            {{--<span>Reorder Schedule</span>--}}
                                                        {{--</li>--}}
                                                        {{--<li>--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<select id="inputSchedule" class="form-control">--}}
                                                                    {{--<option selected>Select Schedule</option>--}}
                                                                    {{--<option>...</option>--}}
                                                                {{--</select>--}}
                                                            {{--</div>--}}
                                                        {{--</li>--}}
                                                    {{--</ul>--}}
                                                    <ul
                                                            class="d-flex justify-content-between align-items-center mb-3">
                                                        <li>
                                                            <span>MOQ</span>
                                                        </li>
                                                        <li>
                                                        {{--<li class="d-flex flex-row moq-inputs">--}}
                                                            {{--<div class="form-group">--}}
                                                                <div class="form-group">
                                                                    <input type="number" class="form-control" id="moqs" placeholder="Enter Number">
                                                                </div>
                                                            {{--</div>--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<select id="inputUnits" class="form-control">--}}
                                                                    {{--<option selected>Units</option>--}}
                                                                    {{--<option>...</option>--}}
                                                                {{--</select>--}}
                                                            {{--</div>--}}
                                                        </li>
                                                    </ul>
                                                    <ul class="d-flex justify-content-between align-items-center mb-3">
                                                        <li>
                                                            <span>Shipping</span>
                                                        </li>
                                                        <li>
                                                            <div class="form-group">
                                                                <select class="form-control" name="shipping" id="shipping">
                                                                    <option value="">Select Shipping</option>
                                                                    <option value="1">UPS</option>
                                                                    <option value="2">USPS</option>
                                                                    <option value="3">FEDEX</option>
                                                                    <option value="4">Private Carrier</option>
                                                                    <option value="5">Direct</option>
                                                                    <option value="6">Pickup</option>
                                                                </select>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <ul class="d-flex justify-content-between align-items-center mb-3">
                                                        <li>
                                                            <span>CBM Per Container</span>
                                                        </li>
                                                        <li>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="Volume" id="cbm_per_container">
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <ul class="d-flex justify-content-between align-items-center mb-3">
                                                        <li>
                                                            <span>Quantity Discounts</span>
                                                        </li>
                                                        <li>
                                                        {{--<li class="d-flex flex-row quantity-inputs">--}}
                                                            <div class="form-group">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" id="quantity_discount" placeholder="1">
                                                                </div>
                                                            </div>
                                                            {{--<div class="form-group">--}}
                                                                {{--<select id="inputBoxes" class="form-control">--}}
                                                                    {{--<option selected>Boxes</option>--}}
                                                                    {{--<option>...</option>--}}
                                                                {{--</select>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<div class="form-group">--}}
                                                                    {{--<input type="text" class="form-control"--}}
                                                                           {{--placeholder="1">--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<select id="inputOff" class="form-control">--}}
                                                                    {{--<option selected>$ Off</option>--}}
                                                                    {{--<option>...</option>--}}
                                                                {{--</select>--}}
                                                            {{--</div>--}}
                                                        </li>
                                                    </ul>
                                                    {{--<ul--}}
                                                            {{--class="d-flex justify-content-between align-items-center mb-3">--}}
                                                        {{--<li>--}}
                                                            {{--<span>Carrier</span>--}}
                                                        {{--</li>--}}
                                                        {{--<li>--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<select id="inputSchedule" class="form-control">--}}
                                                                    {{--<option selected>Select</option>--}}
                                                                    {{--<option>...</option>--}}
                                                                {{--</select>--}}
                                                            {{--</div>--}}
                                                        {{--</li>--}}
                                                    {{--</ul>--}}
                                                    {{--<ul--}}
                                                            {{--class="d-flex justify-content-between align-items-center mb-3">--}}
                                                        {{--<li>--}}
                                                            {{--<span>Quantity Discounts</span>--}}
                                                        {{--</li>--}}
                                                        {{--<li class="d-flex flex-row align-items-center">--}}
                                                            {{--<div style="padding-right: 30px;">--}}
                                                                {{--<ul class="d-flex flex-row align-items-center">--}}
                                                                    {{--<li>--}}
                                                                        {{--<div class="form-check">--}}
                                                                            {{--<input class="form-check-input"--}}
                                                                                   {{--type="checkbox" id="gridCheck">--}}
                                                                        {{--</div>--}}
                                                                    {{--</li>--}}
                                                                    {{--<li>Send shipment confirmation</li>--}}
                                                                {{--</ul>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="form-group">--}}
                                                                {{--<select id="inputSelect" class="form-control">--}}
                                                                    {{--<option selected>Select</option>--}}
                                                                    {{--<option>...</option>--}}
                                                                {{--</select>--}}
                                                            {{--</div>--}}
                                                        {{--</li>--}}
                                                    {{--</ul>--}}
                                                    {{--<div class="d-flex justify-content-end mb-3">--}}
                                                        {{--<a style="color: #227CFF; font-weight: 500;" href="#"><span>--}}
                                                                    {{--<svg xmlns="http://www.w3.org/2000/svg"--}}
                                                                         {{--width="11.25" height="11.25"--}}
                                                                         {{--viewBox="0 0 11.25 11.25">--}}
                                                                        {{--<g id="Icon_ionic-ios-add-circle-outline"--}}
                                                                           {{--data-name="Icon ionic-ios-add-circle-outline"--}}
                                                                           {{--transform="translate(0.25 0.25)">--}}
                                                                            {{--<path id="Path_308" data-name="Path 308"--}}
                                                                                  {{--d="M15.6,12.85H13.677V10.925a.413.413,0,0,0-.827,0V12.85H10.925a.4.4,0,0,0-.413.413.4.4,0,0,0,.413.413H12.85V15.6a.4.4,0,0,0,.413.413.411.411,0,0,0,.413-.413V13.677H15.6a.413.413,0,1,0,0-.827Z"--}}
                                                                                  {{--transform="translate(-7.889 -7.889)"--}}
                                                                                  {{--fill="#227cff" stroke="#227cff"--}}
                                                                                  {{--stroke-width="0.5" />--}}
                                                                            {{--<path id="Path_309" data-name="Path 309"--}}
                                                                                  {{--d="M8.75,4.1A4.65,4.65,0,1,1,5.46,5.46,4.621,4.621,0,0,1,8.75,4.1m0-.724A5.375,5.375,0,1,0,14.125,8.75,5.374,5.374,0,0,0,8.75,3.375Z"--}}
                                                                                  {{--transform="translate(-3.375 -3.375)"--}}
                                                                                  {{--fill="#227cff" stroke="#227cff"--}}
                                                                                  {{--stroke-width="0.5" />--}}
                                                                        {{--</g>--}}
                                                                    {{--</svg>--}}

                                                                {{--</span> Add Discount</a>--}}
                                                    {{--</div>--}}
                                                    <ul
                                                            class="d-flex justify-content-between align-items-center mb-3">
                                                        <li>
                                                            <span>Ship to Warehouse</span>
                                                        </li>
                                                        <li>
                                                            <div class="form-group">
                                                                <select id="warehouse_id" class="form-control">
                                                                    @if(!empty($warehouse))
                                                                        <option value="">Select warehouse</option>
                                                                        @foreach($warehouse as $house)
                                                                            @if($house['warehouse_name'] != '')
                                                                                <option value="{{$house['id']}}">{{$house['warehouse_name']}}</option>
                                                                            @endif
                                                                        @endforeach
                                                                     @endif
                                                                </select>
                                                            </div>
                                                        </li>
                                                    </ul>

                                                    <ul
                                                            class="d-flex flex-row justify-content-between address-btn-box">
                                                        <li>
                                                            <button type="submit" class="btn address-cancel-btn">Cancel</button>
                                                        </li>
                                                        <li>
                                                            <button type="button" class="btn btn-primary save_setting">Save</button>
                                                            <button type="button" class="btn address-save-btn save_setting">Save And Next</button>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="nav-black" role="tabpanel"
                                             aria-labelledby="nav-black-tab">
                                            <div class="backout-dates-box">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="backout-dates-box-left-content">
                                                            <h2>Event Lists</h2>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="backout-dates-box-right-content">
                                                            <ul class="d-flex justify-content-end">
                                                                <li>
                                                                <span>
                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                         width="11.25" height="11.25"
                                                                         viewBox="0 0 11.25 11.25">
                                                                        <g id="Icon_ionic-ios-add-circle-outline"
                                                                           data-name="Icon ionic-ios-add-circle-outline"
                                                                           transform="translate(-3.125 -3.125)">
                                                                            <path id="Path_308"
                                                                                  data-name="Path 308"
                                                                                  d="M15.6,12.85H13.677V10.925a.413.413,0,0,0-.827,0V12.85H10.925a.4.4,0,0,0-.413.413.4.4,0,0,0,.413.413H12.85V15.6a.4.4,0,0,0,.413.413.411.411,0,0,0,.413-.413V13.677H15.6a.413.413,0,1,0,0-.827Z"
                                                                                  transform="translate(-4.514 -4.514)"
                                                                                  fill="#227cff" stroke="#227cff"
                                                                                  stroke-width="0.5" />
                                                                            <path id="Path_309"
                                                                                  data-name="Path 309"
                                                                                  d="M8.75,4.1A4.65,4.65,0,1,1,5.46,5.46,4.621,4.621,0,0,1,8.75,4.1m0-.724A5.375,5.375,0,1,0,14.125,8.75,5.374,5.374,0,0,0,8.75,3.375Z"
                                                                                  fill="#227cff" stroke="#227cff"
                                                                                  stroke-width="0.5" />
                                                                        </g>
                                                                    </svg>
                                                                </span>
                                                                    <a href="#" class="back-out-add">Add New Event</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="backout-dates-table">
                                                            <table class="table" width="100%" id="blackout_date">
                                                                <thead>
                                                                <tr class="black-out-tr">
                                                                    <th scope="col">Event Name <span><svg
                                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                                    width="9.519" height="6.776"
                                                                                    viewBox="0 0 9.519 6.776">
                                                                                <g id="Group_8726"
                                                                                   data-name="Group 8726"
                                                                                   transform="translate(-479.75 -318)">
                                                                                    <path
                                                                                            id="Icon_ionic-ios-arrow-round-down"
                                                                                            data-name="Icon ionic-ios-arrow-round-down"
                                                                                            d="M15.68,12.2a.308.308,0,0,0-.433,0l-1.433,1.429V8.179a.306.306,0,0,0-.612,0v5.444l-1.433-1.431a.31.31,0,0,0-.433,0,.3.3,0,0,0,0,.431l1.953,1.939h0a.344.344,0,0,0,.1.064.292.292,0,0,0,.118.024.307.307,0,0,0,.214-.087l1.953-1.939A.3.3,0,0,0,15.68,12.2Z"
                                                                                            transform="translate(495.517 332.651) rotate(180)"
                                                                                            fill="#227cff" />
                                                                                    <path
                                                                                            id="Icon_ionic-ios-arrow-round-down-2"
                                                                                            data-name="Icon ionic-ios-arrow-round-down"
                                                                                            d="M4.433,2.455a.308.308,0,0,1-.433,0L2.566,1.029V6.472a.306.306,0,0,1-.612,0V1.029L.521,2.46a.31.31,0,0,1-.433,0,.3.3,0,0,1,0-.431L2.044.087h0a.344.344,0,0,1,.1-.064A.292.292,0,0,1,2.258,0a.307.307,0,0,1,.214.087L4.426,2.026A.3.3,0,0,1,4.433,2.455Z"
                                                                                            transform="translate(489.27 324.776) rotate(180)"
                                                                                            fill="#ced4da" />
                                                                                </g>
                                                                            </svg>
                                                                        </span></th>
                                                                    <th scope="col">Start Date <span><svg
                                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                                    width="9.519" height="6.776"
                                                                                    viewBox="0 0 9.519 6.776">
                                                                                <g id="Group_8727"
                                                                                   data-name="Group 8727"
                                                                                   transform="translate(-479.75 -318)">
                                                                                    <path
                                                                                            id="Icon_ionic-ios-arrow-round-down"
                                                                                            data-name="Icon ionic-ios-arrow-round-down"
                                                                                            d="M15.68,12.2a.308.308,0,0,0-.433,0l-1.433,1.429V8.179a.306.306,0,0,0-.612,0v5.444l-1.433-1.431a.31.31,0,0,0-.433,0,.3.3,0,0,0,0,.431l1.953,1.939h0a.344.344,0,0,0,.1.064.292.292,0,0,0,.118.024.307.307,0,0,0,.214-.087l1.953-1.939A.3.3,0,0,0,15.68,12.2Z"
                                                                                            transform="translate(495.517 332.651) rotate(180)"
                                                                                            fill="#a6b0cf" />
                                                                                    <path
                                                                                            id="Icon_ionic-ios-arrow-round-down-2"
                                                                                            data-name="Icon ionic-ios-arrow-round-down"
                                                                                            d="M4.433,2.455a.308.308,0,0,1-.433,0L2.566,1.029V6.472a.306.306,0,0,1-.612,0V1.029L.521,2.46a.31.31,0,0,1-.433,0,.3.3,0,0,1,0-.431L2.044.087h0a.344.344,0,0,1,.1-.064A.292.292,0,0,1,2.258,0a.307.307,0,0,1,.214.087L4.426,2.026A.3.3,0,0,1,4.433,2.455Z"
                                                                                            transform="translate(489.27 324.776) rotate(180)"
                                                                                            fill="#a6b0cf" />
                                                                                </g>
                                                                            </svg>
                                                                        </span></th>
                                                                    <th scope="col">End Date <span><svg
                                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                                    width="9.519" height="6.776"
                                                                                    viewBox="0 0 9.519 6.776">
                                                                                <g id="Group_8728"
                                                                                   data-name="Group 8728"
                                                                                   transform="translate(-479.75 -318)">
                                                                                    <path
                                                                                            id="Icon_ionic-ios-arrow-round-down"
                                                                                            data-name="Icon ionic-ios-arrow-round-down"
                                                                                            d="M15.68,12.2a.308.308,0,0,0-.433,0l-1.433,1.429V8.179a.306.306,0,0,0-.612,0v5.444l-1.433-1.431a.31.31,0,0,0-.433,0,.3.3,0,0,0,0,.431l1.953,1.939h0a.344.344,0,0,0,.1.064.292.292,0,0,0,.118.024.307.307,0,0,0,.214-.087l1.953-1.939A.3.3,0,0,0,15.68,12.2Z"
                                                                                            transform="translate(495.517 332.651) rotate(180)"
                                                                                            fill="#a6b0cf" />
                                                                                    <path
                                                                                            id="Icon_ionic-ios-arrow-round-down-2"
                                                                                            data-name="Icon ionic-ios-arrow-round-down"
                                                                                            d="M4.433,2.455a.308.308,0,0,1-.433,0L2.566,1.029V6.472a.306.306,0,0,1-.612,0V1.029L.521,2.46a.31.31,0,0,1-.433,0,.3.3,0,0,1,0-.431L2.044.087h0a.344.344,0,0,1,.1-.064A.292.292,0,0,1,2.258,0a.307.307,0,0,1,.214.087L4.426,2.026A.3.3,0,0,1,4.433,2.455Z"
                                                                                            transform="translate(489.27 324.776) rotate(180)"
                                                                                            fill="#a6b0cf" />
                                                                                </g>
                                                                            </svg>
                                                                        </span></th>
                                                                    <th scope="col">Number Of Days <span>
                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                 width="9.519" height="6.776"
                                                                                 viewBox="0 0 9.519 6.776">
                                                                                <g id="Group_8728"
                                                                                   data-name="Group 8728"
                                                                                   transform="translate(-479.75 -318)">
                                                                                    <path
                                                                                            id="Icon_ionic-ios-arrow-round-down"
                                                                                            data-name="Icon ionic-ios-arrow-round-down"
                                                                                            d="M15.68,12.2a.308.308,0,0,0-.433,0l-1.433,1.429V8.179a.306.306,0,0,0-.612,0v5.444l-1.433-1.431a.31.31,0,0,0-.433,0,.3.3,0,0,0,0,.431l1.953,1.939h0a.344.344,0,0,0,.1.064.292.292,0,0,0,.118.024.307.307,0,0,0,.214-.087l1.953-1.939A.3.3,0,0,0,15.68,12.2Z"
                                                                                            transform="translate(495.517 332.651) rotate(180)"
                                                                                            fill="#a6b0cf" />
                                                                                    <path
                                                                                            id="Icon_ionic-ios-arrow-round-down-2"
                                                                                            data-name="Icon ionic-ios-arrow-round-down"
                                                                                            d="M4.433,2.455a.308.308,0,0,1-.433,0L2.566,1.029V6.472a.306.306,0,0,1-.612,0V1.029L.521,2.46a.31.31,0,0,1-.433,0,.3.3,0,0,1,0-.431L2.044.087h0a.344.344,0,0,1,.1-.064A.292.292,0,0,1,2.258,0a.307.307,0,0,1,.214.087L4.426,2.026A.3.3,0,0,1,4.433,2.455Z"
                                                                                            transform="translate(489.27 324.776) rotate(180)"
                                                                                            fill="#a6b0cf" />
                                                                                </g>
                                                                            </svg>

                                                                        </span></th>
                                                                    <th scope="col">Type <span><svg
                                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                                    width="9.519" height="6.776"
                                                                                    viewBox="0 0 9.519 6.776">
                                                                                <g id="Group_8728"
                                                                                   data-name="Group 8728"
                                                                                   transform="translate(-479.75 -318)">
                                                                                    <path
                                                                                            id="Icon_ionic-ios-arrow-round-down"
                                                                                            data-name="Icon ionic-ios-arrow-round-down"
                                                                                            d="M15.68,12.2a.308.308,0,0,0-.433,0l-1.433,1.429V8.179a.306.306,0,0,0-.612,0v5.444l-1.433-1.431a.31.31,0,0,0-.433,0,.3.3,0,0,0,0,.431l1.953,1.939h0a.344.344,0,0,0,.1.064.292.292,0,0,0,.118.024.307.307,0,0,0,.214-.087l1.953-1.939A.3.3,0,0,0,15.68,12.2Z"
                                                                                            transform="translate(495.517 332.651) rotate(180)"
                                                                                            fill="#a6b0cf" />
                                                                                    <path
                                                                                            id="Icon_ionic-ios-arrow-round-down-2"
                                                                                            data-name="Icon ionic-ios-arrow-round-down"
                                                                                            d="M4.433,2.455a.308.308,0,0,1-.433,0L2.566,1.029V6.472a.306.306,0,0,1-.612,0V1.029L.521,2.46a.31.31,0,0,1-.433,0,.3.3,0,0,1,0-.431L2.044.087h0a.344.344,0,0,1,.1-.064A.292.292,0,0,1,2.258,0a.307.307,0,0,1,.214.087L4.426,2.026A.3.3,0,0,1,4.433,2.455Z"
                                                                                            transform="translate(489.27 324.776) rotate(180)"
                                                                                            fill="#a6b0cf" />
                                                                                </g>
                                                                            </svg>
                                                                        </span></th>
                                                                </tr>
                                                                </thead>
                                                                <tbody class="back-out-tbody">
                                                                <tr class="parent_class">
                                                                    <input type="hidden" class="blackout_date" value="">
                                                                    <th scope="row"><input id="back-out-input"
                                                                                           type="text"
                                                                                           placeholder="Event Name" class="event_name"></th>
                                                                    <th scope="row"><input id="back-out-input"
                                                                                           type="date" class="start_date"></th>
                                                                    <th scope="row"><input id="back-out-input"
                                                                                           type="date" class="last_date"></th>
                                                                    <th scope="row"><input id="back-out-input"
                                                                                           type="text" value="0"
                                                                                           placeholder="Number Of Days" class="number_of_day"></th>
                                                                    <th scope="row" class="d-flex">
                                                                        <input id="back-out-input" type="text" value="Production" class="type" placeholder="Type">
                                                                        <div class="backout-delete-icon">
                                                                            <i class="fas fa-trash-alt"></i>
                                                                        </div>
                                                                    </th>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <ul
                                                                class="d-flex flex-row justify-content-between pt-5 shipping-agent-footer">
                                                            <li>
                                                                <button type="submit"
                                                                        class="btn address-cancel-btn">Cancel</button>
                                                            </li>
                                                            <li>
                                                                <button type="button"
                                                                        class="btn btn-primary save_blankout_date">Save</button>
                                                                <button type="submit"
                                                                        class="btn address-save-btn ml-2 save_blankout_date">Save And
                                                                    Next</button>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script>
        $(document).on("click",".save_contacts",function(){
            var first_names = [];
            var last_names = [];
            var titles = [];
            var emailss = [];
            var phone_numbers = [];
            var vendors_contact_ids = [];

            $("table > tbody > tr").each(function( index, element ) {
                //datass[index] = [];
                var first_name = $(this).find(".first_name").text();
                var last_name = $(this).find(".last_name").text();
                var title = $(this).find(".title").text();
                var emails = $(this).find(".emails").text();
                var phone_number = $(this).find(".phone_number").text();
                var vendors_contact_id = $(this).find(".vendors_contact_id").val();

                first_names. push($.trim(first_name));
                last_names. push($.trim(last_name));
                titles. push($.trim(title));
                emailss. push($.trim(emails));
                phone_numbers. push($.trim(phone_number));
                vendors_contact_ids. push($.trim(vendors_contact_id));
            });

            var vendors_id = $("#vendors_id").val();
           // var vendors_id = '1';
            var insert_type = 'insert_contacts';

            if(vendors_id == ''){
                $(".tab-pane").removeClass('show active');
                $(".nav-link").removeClass('active');
                $("#nav-home-tab").addClass('active');
                $("#nav-home").addClass('show active');
            }else {

                $.ajax({
                    type: "POST",
                    url: "{{route('vendors.store')}}",
                    data: {
                        vendors_id: vendors_id,
                        first_names: first_names,
                        last_names: last_names,
                        emails: emailss,
                        insert_type: insert_type,
                        vendors_contact_ids: vendors_contact_ids
                    },
                    success: function (res) {
                        if (res.error == 0) {
                            $("#messages").html(res.message);
                            $(".vendors_contact_id").val(res.vendors_contact_id);
                            $(".tab-pane").removeClass('show active');
                            $(".nav-link").removeClass('active');
                            $("#nav-contact-tab").addClass('active');
                            $("#nav-setting").addClass('show active');
                        } else {
                            $("#messages").html(res.message);
                        }
                    }
                });
            }
        });

        $(document).on('click','.save_datas',function(){
            var types = $(this).data('val');
            var vendor_name = $('#vendor_name').val();
            var country = $('#country').val();
            var state = $('#state').val();
            var city = $('#city').val();
            var zipcode = $('#zipcode').val();
            var address_line_1 = $('#address_line_1').val();
            var address_line_2 = $('#address_line_2').val();
            var marketplace_id = $("#marketplace_id").val();
            var vendor_id = $("#vendors_id").val();
            var insert_type = 'insert_update_basic_details';
            if(vendor_name == ''){
                $("#vendor_name").css("border", "1px solid red");
                $("#vendor_name").focus();
            }else if(country == ''){
                $("#country").css("border", "1px solid red");
                $("#country").focus();
            }else {
                $.ajax({
                    type: "POST",
                    url: "{{route('vendors.store')}}",
                    data: {
                        vendor_id: vendor_id,
                        marketplace_id: marketplace_id,
                        vendor_name: vendor_name,
                        country: country,
                        state: state,
                        city: city,
                        zipcode: zipcode,
                        address_line_1: address_line_1,
                        address_line_2: address_line_2,
                        insert_type: insert_type
                    },
                    success: function (res) {
                        if (res.error == 0) {
                            $("#messages").html(res.message);
                            $("#vendors_id").val(res.vendor_id);
                            if (types == 1) {
                            } else {
                                $(".tab-pane").removeClass('show active');
                                $(".nav-link").removeClass('active');
//                                if (type == 1) {
//                                    $("#nav-contact-tab").addClass('active');
//                                    $("#nav-setting").addClass('show active');
//                                } else {
                                    $("#nav-profile-tab").addClass('active');
                                    $("#nav-profile").addClass('show active');
                                //}
                            }
                        } else {
                            $("#messages").html(res.message);
                        }
                    }
                });
            }

        });


        $(document).on('click','.save_setting',function(){
            var vendor_id = $("#vendors_id").val();
            var insert_type = 'insert_update_setting';
            var lead_time = $("#lead_time").val();
            var order_volume = $("#order_volume").val();
            var moqs = $("#moqs").val();
            var shipping = $("#shipping").val();
            var cbm_per_container = $("#cbm_per_container").val();
            var quantity_discount = $("#quantity_discount").val();
            var warehouse_id = $("#warehouse_id").val();


            if(vendor_id == ''){
                $(".tab-pane").removeClass('show active');
                $(".nav-link").removeClass('active');
                $("#nav-home-tab").addClass('active');
                $("#nav-home").addClass('show active');
            }else{
                $.ajax({
                    type:"POST",
                    url:"{{route('vendors.store')}}",
                    data:{vendor_id:vendor_id,
                        lead_time:lead_time,
                        moqs:moqs,
                        order_volume:order_volume,
                        shipping:shipping,
                        cbm_per_container:cbm_per_container,
                        quantity_discount:quantity_discount,
                        warehouse_id:warehouse_id,
                        insert_type:insert_type
                    },
                    success:function(res){
                        if(res.error == 0){
                            $("#messages").html(res.message);
                            $("#vendors_id").val(vendor_id);
                            $(".tab-pane").removeClass('show active');
                            $(".nav-link").removeClass('active');
                            $("#nav-black-tab").addClass('active');
                            $("#nav-black").addClass('show active');
                        }else{
                            $("#messages").html(res.message);
                        }
                    }
                });
            }
        });
        $(document).on('click','.backout-delete-icon',function(){
            var insert_type = 'delete_vendors_contact';
            var rand_text = makeid(5);
            var supplier_contact_id = $(this).find(".supplier_contact_id").val();
            $(this).closest('tr').addClass(rand_text);
            //var supplier_contact_id = $('.'+rand_text).find(".supplier_contact_id").val();
            //$(this).closest('tr').attr('data-random',supplier_contact_id);

            {{--if(supplier_contact_id != ''){--}}
                {{--$.ajax({--}}
                    {{--type:"POST",--}}
                    {{--url:"{{route('vendors.store')}}",--}}
                    {{--data:{supplier_contact_id:supplier_contact_id,insert_type:insert_type },--}}
                    {{--success:function(res){--}}
                        {{--$("."+rand_text).remove();--}}
                    {{--}--}}
                {{--});--}}
            {{--}else{--}}
                $("."+rand_text).remove();
            // }
        });


        $(document).on('click','.delete-btn-vendors-tab',function(){
            var insert_type = 'delete_vendors_contact';
            var rand_text = makeid(5);
            var vendors_contact_id = $(this).find(".vendors_contact_id").val();
            $(this).closest('tr').addClass(rand_text);
           //var vendors_contact_id = $('.'+rand_text).find(".vendors_contact_id").val();
            //$(this).closest('tr').attr('data-random',vendors_contact_id);

            {{--if(vendors_contact_id != ''){--}}
                {{--$.ajax({--}}
                    {{--type:"POST",--}}
                    {{--url:"{{route('vendors.store')}}",--}}
                    {{--data:{vendors_contact_id:vendors_contact_id,insert_type:insert_type },--}}
                    {{--success:function(res){--}}
                        {{--$("."+rand_text).remove();--}}
                    {{--}--}}
                {{--});--}}
            {{--}else{--}}
                $("."+rand_text).remove();
           // }
        });

        $(function(){
            var rand_text = makeid(5);
            $("#blackout_date tbody > tr").attr("id",rand_text);
            var dtToday = new Date();
            var month = dtToday.getMonth() + 1;
            var day = dtToday.getDate();
            var year = dtToday.getFullYear();
            if(month < 10)
                month = '0' + month.toString();
            if(day < 10)
                day = '0' + day.toString();

            var maxDate = year + '-' + month + '-' + day;
            $('.start_date').attr('min', maxDate);
            $('.last_date').attr('min', maxDate);
            $('.start_date').val(maxDate);
            $('.last_date').val(maxDate);

            //$(".last_date").prop('disabled',true);
            $(".number_of_day").prop('readonly',true);

            $(".start_date").addClass(rand_text+'start_date');
            $(".last_date").addClass(rand_text+'last_date');
            $(".event_name").addClass(rand_text+'event_name');
            $(".number_of_day").addClass(rand_text+'number_of_day');
            $(".type").addClass(rand_text+'type');

            $(".start_date").attr('data-val',rand_text);
            $(".last_date").attr('data-val',rand_text);
            $(".event_name").attr('data-val',rand_text);
            $(".number_of_day").attr('data-val',rand_text);
            $(".type").attr('data-val',rand_text);

        });

        $(document).on('change','.start_date',function(){
            var startdate = $(this).val();
            var randtext = $(this).data('val');
            if(startdate != ''){
                var dtToday = new Date(startdate);
                var month = dtToday.getMonth() + 1;
                var day = dtToday.getDate() + 1;
                var year = dtToday.getFullYear();
                if(month < 10)
                    month = '0' + month.toString();
                if(day < 10)
                    day = '0' + day.toString();

                var maxDate = year + '-' + month + '-' + day;
                $('.'+randtext+'last_date').attr('min', maxDate);
                $('.'+randtext+'last_date').val(maxDate);

                $('.'+randtext+'last_date').prop('disabled',false);
            }
        });

        $(document).on('change','.last_date',function(){
            var randtext = $(this).data('val');
            var start = $('.'+randtext+'start_date').val();
            var end = $(this).val();

            var startDay = new Date(start);
            var endDay = new Date(end);
            var millisecondsPerDay = 1000 * 60 * 60 * 24;

            var millisBetween = endDay.getTime() - startDay.getTime();
            var days = millisBetween / millisecondsPerDay;

            $('.'+randtext+'number_of_day').val(Math.floor(days));
        });


        $(document).on('click',".save_blankout_date",function(){
            var event_names = [];
            var start_dates = [];
            var last_dates = [];
            var number_of_days = [];
            var types = [];
            var backouot_dates = [];

            $("#blackout_date tbody > tr").each(function( index, element ) {
                var event_name = $(this).find(".event_name").val();
                var start_date = $(this).find(".start_date").val();
                var last_date = $(this).find(".last_date").val();
                var number_of_day = $(this).find(".number_of_day").val();
                var type = $(this).find(".type").val();
                var backouot_date = $(this).find(".blackout_date").val();

                event_names.push($.trim(event_name));
                start_dates.push($.trim(start_date));
                last_dates.push($.trim(last_date));
                number_of_days.push($.trim(number_of_day));
                types.push($.trim(type));
                backouot_dates.push($.trim(backouot_date));
            });

            var vendors_id = $("#vendors_id").val();
            //var vendors_id = '20';
            var insert_type = 'insert_update_backout_date';

            if(vendors_id == ''){
                $(".tab-pane").removeClass('show active');
                $(".nav-link").removeClass('active');
                $("#nav-home-tab").addClass('active');
                $("#nav-home").addClass('show active');
            }else {

                $.ajax({
                    type: "POST",
                    url: "{{route('vendors.store')}}",
                    data: {
                        vendors_id: vendors_id,
                        event_names: event_names,
                        start_dates: start_dates,
                        last_dates: last_dates,
                        number_of_days: number_of_days,
                        types: types,
                        insert_type: insert_type,
                        backouot_dates: backouot_dates
                    },
                    success: function (res) {
                        if (res.error == 0) {
                            $("#messages").html(res.message);
                            $("#vendors_id").val(vendors_id);
                            window.location.href = "{{route('vendors.index')}}";
//                            $("#messages").html(res.message);
//                            $(".backouot_dates").val(res.backouot_dates);
//                            $(".tab-pane").removeClass('show active');
//                            $(".nav-link").removeClass('active');
//                            $("#nav-contact-tab").addClass('active');
//                            $("#nav-setting").addClass('show active');
                        } else {
                            $("#messages").html(res.message);
                        }
                    }
                });
            }


        });

        function makeid(length) {
            var result           = '';
            var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;
            for ( var i = 0; i < length; i++ ) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            return result;
        }

//        $(document).ready(function(){
//            var type = $("#type").val();
//            if(type == 1){
//                $('#nav-profile-tab').hide();
//            }else{
//                $('#nav-profile-tab').show();
//            }
//        });
//        $(document).on('change','#type',function(){
//            var type = $(this).val();
//            if(type == 1){
//                $('#nav-profile-tab').hide();
//            }else{
//                $('#nav-profile-tab').show();
//            }
//        });

        //    $(document).ready(function(){
        //        $("select").change(function(){
        //            check();
        //        }).change();
        //        check();
        //    });
        //
        function check() {
            var data= $('#type').val();
            if(data=="1")  {
                $(".primary_first_name").hide();
                $(".primary_last_name").hide();
                $(".primary_email").hide();
                $('.primary_first_name').rules('remove',  {
                    required : true,
                    messages : { required : 'Firstname is required' }
                });
                $('.primary_last_name').rules('remove',  {
                    required : true,
                    messages : { required : 'Lastname is required' }
                });
                $('.primary_email').rules('remove',  {
                    required : true,
                    messages : { required : 'Email is required' }
                });

            }
            else
            {

                $(".primary_first_name").show();
                $(".primary_last_name").show();
                $(".primary_email").show();

                $('.primary_first_name').rules('add',  {
                    required : true,
                    messages : { required : 'Firstname number is required' }
                });
                $('.primary_last_name').rules('add',  {
                    required : true,
                    messages : { required : 'Lastname is required' }
                });
                $('.primary_email').rules('add',  {
                    required : true,
                    messages : { required : 'Email is required' }
                });
            }
        }

    </script>
    <script type="text/javascript">

        $('#country').change(function(){
            var country = $(this).val();
            var request_type = 'Get_all_state';
            if(country){
                $.ajax({
                    type:"POST",
                    url:"{{route('vendors.store')}}",
                    data:{country:country,request_type:request_type},
                    success:function(res){
                        if(res){
                            $("#state").empty();
                            $("#state").append('<option>Select</option>');
                            $.each(res,function(key,value){
                                $("#state").append('<option value="'+key+'">'+key+'('+value+')'+'</option>');
                            });

                        }else{
                            $("#state").empty();
                        }
                    }
                });
            }else{
                $("#state").empty();
                $("#city").empty();
            }
        });
        $('#state').on('change',function(){
            var state = $(this).val();
            var request_type = 'Get_all_city';
            if(state){
                $.ajax({
                    type:"POST",
                    url:"{{ route('vendors.store') }}",
                    data:{state:state,request_type:request_type},
                    success:function(res){
                        if(res){
                            console.log(res);
                            $("#city").empty();
                            $("#city").append('<option>Select</option>');
                            $.each(res,function(key,value){
                                $("#city").append('<option value="'+key+'">'+key+'('+value+')'+'</option>');
                            });

                        }else{
                            $("#city").empty();
                        }
                    }
                });
            }else{
                $("#city").empty();
            }

        });
    </script>
@endsection
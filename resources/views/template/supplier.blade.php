@extends('layouts.master-new')

@section('title') Supplier @endsection

@section('content')
    <div class="suppliers-top-main-box">
        <div class="row">
            <div class="col-md-4">
                <div class="suppliers-first-column">
                    <h2>Suppliers</h2>
                    <input type="hidden" value="{{$marketplace}}" name="marketplaces" id="marketplaces">
                </div>
            </div>
            <div class="col-md-8">
                <div class="suppliers-first-column-two text-left text-md-right">
                    <button type="button" class="btn supplier-down-btn">
                        <img src="asset/images/svg/download.svg" alt="">
                    </button>
                    <button type="button" class="btn import-supplier-btn mx-3">
                        <span> <img src="asset/images/svg/import.svg" alt=""> </span>
                        Import Supplier
                    </button>
                    <button type="button" class="btn add-new-supplier">
                        <span> <img src="asset/images/svg/add-supplier.svg" alt=""></span>
                        Add New Supplier
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">

            </div>
        </div>
        @if(session()->has('message2') || session()->has('message3') || session()->has('message4'))
            <div class="col-lg-12">
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    @if(session()->has('message2'))
                        {!! session('message2') !!}
                    @endif
                    <br>
                    @if(session()->has('message3'))
                        {!! session('message3') !!}
                    @endif
                    <br>
                    @if(session()->has('message4'))
                        {!! session('message4') !!}
                    @endif
                </div>
            </div>
        @endif
    </div>
    <!-- end row -->
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-4">Suppliers List</h4>
            <input type="hidden" name="id"  value="" >
            <div class="table-responsive">
                <table id="datatables" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead class="thead-light">
                        <tr>
                            <th>Supplier Name</th>
                            <th>Country</th>
                            <th>Products</th>
                            <th>Contact</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

@endsection
@section('script')

<script>
     $(document).ready(function () {
            var id = $('#marketplaces').val();
                     loadDashboard(id);
        });
        function loadDashboard(id){
           var suppliers_list='suppliers_list';
            var table = $('#datatables').DataTable({

                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('suppliers.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.id=id,d.request_type=suppliers_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                "columns": [
                    {"data": "supplier_name"},
                    {"data": "country"},
                    {"data": "user_marketplace_id"},
                    {"data": "zipcode"},
                    {"data": "action", "orderable": false}
                ]
            });
        }
</script>
@endsection

@extends('layouts.master-new')

@section('title') Edit Shipping Agent @endsection

@section('content')
    <div class="suppliers-top-main-box">
        <div class="row">
            <div class="col-md-4">
                <div class="suppliers-first-column">
                    <h2>Shipping Agent</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="suppliers-main-box suppliers-main-box-scroll">
        <div class="row">
            <div class="suppliers-logistics-agent-2">
                <form class="main-address-from" name="edit-shipping-agent" id="edit-shipping-agent" action="{{ route('shippingagent.update',$shipping_agent['id']) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="insert_type" value="edit_form">
                    <input type="hidden" name="marketplace_id" id="marketplace_id" value="{{session('MARKETPLACE_ID')}}">
                    <!-- <div class="suppliers-address-box">
                        <ul class="d-flex flex-row align-items-center justify-content-between pb-3">
                            <li>Supplier Name <span>*</span></li>
                            <li>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Enter Supplier Name">
                                </div>
                            </li>
                        </ul>
                    </div> -->
                    <div class="suppliers-address-box">
                        <ul class="d-flex flex-row align-items-center justify-content-between pb-3">
                            <li>Shipping Agent</li>
                            <li>
                                <div class="form-group">
                                    <select class="form-control" id="exampleFormControlSelect1" name="country">
                                        <option>Select Shipping Agent</option>
                                        @foreach($country as $countries)
                                            <option value="{{ $countries['id'] }}" @if($countries['id']== $shipping_agent['country_id']) selected @endif>{{ $countries['country_name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="suppliers-address-box">
                        <ul class="d-flex flex-row align-items-center justify-content-between pb-3">
                            <li>Company</li>
                            <li>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="company_name" id="company_name" placeholder="Enter Company Name" value="{{ $shipping_agent['company_name'] }}">
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="suppliers-address-box">
                        <ul class="d-flex flex-row align-items-center justify-content-between pb-3">
                            <li>First Name</li>
                            <li>
                                <div class="form-group">
                                    <input type="text" class="form-control"  name="first_name" id="last_name" placeholder="Enter First Name" value="{{ $shipping_agent['first_name'] }}">
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="suppliers-address-box">
                        <ul class="d-flex flex-row align-items-center justify-content-between pb-3">
                            <li>Last Name</li>
                            <li>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Enter Last Name" value="{{ $shipping_agent['last_name'] }}">
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="suppliers-address-box">
                        <ul class="d-flex flex-row align-items-center justify-content-between pb-3">
                            <li>Email</li>
                            <li>
                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Enter Email" value="{{ $shipping_agent['email'] }}">
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="suppliers-address-box">
                        <ul class="d-flex flex-row align-items-center justify-content-between pb-3">
                            <li>Phone Number</li>
                            <li>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone Number" value="{{ $shipping_agent['phone'] }}">
                                </div>
                            </li>
                        </ul>
                    </div>
                    <ul class="d-flex flex-row justify-content-between">
                        <li>
                            <button type="submit" class="btn address-cancel-btn">Cancel</button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-primary">Save</button>
                            <button type="submit" class="btn address-save-btn">Save And Next</button>
                        </li>
                    </ul>
                </form>
                <div class="address-save-form">
                    <div class="suppliers-address-box">
                        <ul
                                class="d-flex flex-row align-items-start justify-content-between pb-3">
                            <li>Shipping Agent</li>
                            @php($get_country_name = get_country_name($shipping_agent['country_id']))
                            <li class="address-save-right">
                                <p>{{$get_country_name}}<span class="suppliers-address-box-icon"
                                                                   onclick="editeAdress()"><svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="13.208" height="13.208"
                                                viewBox="0 0 13.208 13.208">
                                                                            <g id="Icon_feather-edit"
                                                                               data-name="Icon feather-edit"
                                                                               transform="translate(1)">
                                                                                <path id="Path_314" data-name="Path 314"
                                                                                      d="M8.46,6H4.213A1.213,1.213,0,0,0,3,7.213v8.494a1.213,1.213,0,0,0,1.213,1.213h8.494a1.213,1.213,0,0,0,1.213-1.213V11.46"
                                                                                      transform="translate(-3 -4.713)"
                                                                                      fill="none" stroke="#227cff"
                                                                                      stroke-linecap="round"
                                                                                      stroke-linejoin="round"
                                                                                      stroke-width="2" />
                                                                                <path id="Path_315" data-name="Path 315"
                                                                                      d="M18.37,3.195a1.287,1.287,0,0,1,1.82,1.82l-5.764,5.764L12,11.386l.607-2.427Z"
                                                                                      transform="translate(-8.36 -2.818)"
                                                                                      fill="#227cff" />
                                                                            </g>
                                                                        </svg>
                                                                    </span></p>
                            </li>
                        </ul>
                    </div>
                    <div class="suppliers-address-box">
                        <ul
                                class="d-flex flex-row align-items-start justify-content-between pb-3">
                            <li>Company</li>
                            <li class="address-save-right">
                                <p>{{$shipping_agent['company_name']}}</p>
                            </li>
                        </ul>
                    </div>
                    <div class="suppliers-address-box">
                        <ul
                                class="d-flex flex-row align-items-start justify-content-between pb-3">
                            <li>First Name</li>
                            <li class="address-save-right">
                                <p>{{$shipping_agent['first_name']}}</p>
                            </li>
                        </ul>
                    </div>
                    <div class="suppliers-address-box">
                        <ul
                                class="d-flex flex-row align-items-start justify-content-between pb-3">
                            <li>Last Name</li>
                            <li class="address-save-right">
                                <p>{{$shipping_agent['last_name']}}</p>
                            </li>
                        </ul>
                    </div>
                    <div class="suppliers-address-box">
                        <ul
                                class="d-flex flex-row align-items-start justify-content-between pb-3">
                            <li>Email</li>
                            <li class="address-save-right">
                                <p>{{$shipping_agent['email']}}</p>
                            </li>
                        </ul>
                    </div>
                    <div class="suppliers-address-box">
                        <ul
                                class="d-flex flex-row align-items-start justify-content-between pb-3">
                            <li>Phone Number</li>
                            <li class="address-save-right">
                                <p>{{$shipping_agent['phone']}}</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@extends('layouts.master-new')

@section('title') Create Shipping Agent @endsection

@section('content')

<div class="suppliers-top-main-box">
    <div class="row">
        <div class="col-md-4">
            <div class="suppliers-first-column">
                <h2>Shipping Agent</h2>
            </div>
        </div>
    </div>
</div>
<div class="suppliers-main-box suppliers-main-box-scroll">
    <div class="row">
        <div class="suppliers-logistics-agent-2">
            <form class="main-address-from d-block" name="create-shipping-agent" id="create-shipping-agent" action="{{ route('shippingagent.store') }}" method="POST" >
                @csrf
                <input type="hidden" name="insert_type" value="create_form">
                <input type="hidden" name="marketplace_id" id="marketplace_id" value="{{session('MARKETPLACE_ID')}}">
                <!-- <div class="suppliers-address-box">
                    <ul class="d-flex flex-row align-items-center justify-content-between pb-3">
                        <li>Supplier Name <span>*</span></li>
                        <li>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Enter Supplier Name">
                            </div>
                        </li>
                    </ul>
                </div> -->
                <div class="suppliers-address-box">
                    <ul class="d-flex flex-row align-items-center justify-content-between pb-3">
                        <li>Shipping Agent</li>
                        <li>
                            <div class="form-group">
                                <select class="form-control" id="exampleFormControlSelect1" id="country" name="country">
                                    <option>Select Shipping Agent</option>
                                    @foreach($country as $countries)
                                    <option value="{{ $countries['id'] }}">{{ $countries['country_name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="suppliers-address-box">
                    <ul class="d-flex flex-row align-items-center justify-content-between pb-3">
                        <li>Company</li>
                        <li>
                            <div class="form-group">
                                <input type="text" class="form-control" name="company_name" id="company_name" placeholder="Enter Company Name">
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="suppliers-address-box">
                    <ul class="d-flex flex-row align-items-center justify-content-between pb-3">
                        <li>First Name</li>
                        <li>
                            <div class="form-group">
                                <input type="text" class="form-control"  name="first_name" id="first_name" placeholder="Enter First Name">
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="suppliers-address-box">
                    <ul class="d-flex flex-row align-items-center justify-content-between pb-3">
                        <li>Last Name</li>
                        <li>
                            <div class="form-group">
                                <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Enter Last Name">
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="suppliers-address-box">
                    <ul class="d-flex flex-row align-items-center justify-content-between pb-3">
                        <li>Email</li>
                        <li>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" id="email" placeholder="Enter Email">
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="suppliers-address-box">
                    <ul class="d-flex flex-row align-items-center justify-content-between pb-3">
                        <li>Phone Number</li>
                        <li>
                            <div class="form-group">
                                <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone Number">
                            </div>
                        </li>
                    </ul>
                </div>
                <ul class="d-flex flex-row justify-content-between">
                    <li>
                        <button type="submit" class="btn address-cancel-btn">Cancel</button>
                    </li>
                    <li>
                        <button type="button" class="btn btn-primary">Save</button>
                        <button type="submit" class="btn address-save-btn">Save And Next</button>
                    </li>
                </ul>
            </form>
        </div>
    </div>
</div>

@endsection
@extends('layouts.master-new')

@section('title') Shipping Agent @endsection

@section('content')
    <div class="suppliers-top-main-box">
        <div class="row">
            <div class="col-md-4">
                <div class="suppliers-first-column">
                    <h2>Shipping Agents</h2>
                    <input type="hidden" value="{{$marketplace}}" name="marketplaces" id="marketplaces">
                </div>
            </div>
            <div class="col-md-8">
                <div class="suppliers-first-column-two text-left text-md-right">
                    <form action="javascript:void(0)" method="post" id="import_csv"  enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <a href="javascript:void(0)" class="btn supplier-down-btn">
                        <span>
                            <img src="asset/images/svg/download.svg" alt="">
                        </span>
                        </a>
                        <input type="file" id="selectedFile" name="insert_type" style="display: none;" />
                        <a href="javascript: void(0);" onclick="document.getElementById('selectedFile').click();" class="btn import-supplier-btn mx-3">
                            <span> <img src="asset/images/svg/import.svg" alt=""> </span>
                            Import Shipping Agents
                        </a>
                        <input type="hidden" name="insert_type" value="upload_csv">
                        {{--<input type="submit" class="btn btn-primary import_csv" value="Import CSV File">--}}
                        <a href="{{ route('shippingagent.create') }}" class="btn add-new-supplier">
                            <span> <img src="asset/images/svg/add-supplier.svg" alt=""></span>
                            Add New Shipping Agent
                        </a>
                    </form>


                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">

            </div>
        </div>
        @if(session()->has('message2') || session()->has('message3') || session()->has('message4'))
            <div class="col-lg-12">
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    @if(session()->has('message2'))
                        {!! session('message2') !!}
                    @endif
                    <br>
                    @if(session()->has('message3'))
                        {!! session('message3') !!}
                    @endif
                    <br>
                    @if(session()->has('message4'))
                        {!! session('message4') !!}
                    @endif
                </div>
            </div>
        @endif
    </div>
    <!-- end row -->
    <div class="suppliers-main-box index-suppliers-main-box">
        <div class="row">
            <div class="col-md-8">
                <div class="suppliers-second-column d-flex flex-row">
                    <div class="suppliers-btn-icons-box">
                        <button type="button" class="btn btn-one filter-one-btn"><svg
                                    xmlns="http://www.w3.org/2000/svg" width="11.411" height="11.411"
                                    viewBox="0 0 11.411 11.411">
                                <path id="Icon_awesome-filter" data-name="Icon awesome-filter"
                                      d="M10.876,0H.536A.535.535,0,0,0,.157.913L4.279,5.036V9.628a.535.535,0,0,0,.228.438L6.29,11.314a.535.535,0,0,0,.842-.438V5.036L11.254.913A.535.535,0,0,0,10.876,0Z"
                                      fill="#495057" />
                            </svg>
                        </button>
                        <button type="button" class="btn btn-one"><svg
                                    xmlns="http://www.w3.org/2000/svg" width="11.411" height="11.411"
                                    viewBox="0 0 11.411 11.411">
                                <svg xmlns="http://www.w3.org/2000/svg" width="7.425" height="11.133"
                                     viewBox="0 0 7.425 11.133">
                                    <path id="Icon_ionic-ios-arrow-round-down"
                                          data-name="Icon ionic-ios-arrow-round-down"
                                          d="M7.283,4.033a.505.505,0,0,1-.712,0L4.216,1.69v8.944a.5.5,0,0,1-1.005,0V1.69L.856,4.041a.509.509,0,0,1-.712,0,.5.5,0,0,1,0-.708L3.358.143h0a.564.564,0,0,1,.159-.1A.48.48,0,0,1,3.71,0a.5.5,0,0,1,.352.143l3.21,3.186A.493.493,0,0,1,7.283,4.033Z"
                                          transform="translate(7.425 11.133) rotate(180)"
                                          fill="#495057" />
                                </svg>

                            </svg>
                            <div class="filter-btn-overlay">4</div>
                        </button>
                    </div>
                    <div class="input-group mx-lg-3">
                        <input type="text" class="form-control" placeholder="Search by Keyword"
                               aria-label="Recipient's username" aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button class="btn" type="button" id="button-addon2"><svg
                                        xmlns="http://www.w3.org/2000/svg" width="12.774" height="12.773"
                                        viewBox="0 0 12.774 12.773">
                                    <g id="Group_9340" data-name="Group 9340"
                                       transform="translate(-579.402 -212)">
                                        <path id="Path_423" data-name="Path 423"
                                              d="M12.922,8.711A4.211,4.211,0,1,1,8.711,4.5,4.211,4.211,0,0,1,12.922,8.711Z"
                                              transform="translate(575.902 208.5)" fill="none"
                                              stroke="#fff" stroke-linecap="round" stroke-linejoin="round"
                                              stroke-width="2" />
                                        <path id="Path_424" data-name="Path 424"
                                              d="M28.149,28.149l-3.174-3.174"
                                              transform="translate(562.613 195.21)" fill="none"
                                              stroke="#fff" stroke-linecap="round" stroke-linejoin="round"
                                              stroke-width="2" />
                                    </g>
                                </svg>
                            </button>
                        </div>
                    </div>
                    <div class="form-group">
                        <select class="form-control" id="exampleFormControlSelect1">
                            <option>Actions</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="suppliers-second-column-last">
                    <div class="form-group ml-auto">
                         @php $countries = get_country_list(); @endphp
                        <select class="form-control" id="select_country">
                            {{--<option>Select Country</option>--}}
                        @foreach($countries as $key => $country)
                            <option value="{{$key}}">{{$country}}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="ui-filter-tags d-flex align-items-center">
                    <p>Filter :</p>
                    <ul class="d-flex align-items-center">
                        <li>
                            <span class="badge">Shea</span>
                            <span class="badge-cross"><svg xmlns="http://www.w3.org/2000/svg" width="9"
                                                           height="9" viewBox="0 0 9 9">
                                                    <g id="Group_9340" data-name="Group 9340"
                                                       transform="translate(-381 -251)">
                                                        <g id="Group_9308" data-name="Group 9308"
                                                           transform="translate(381.469 251.469)">
                                                            <circle id="Ellipse_36" data-name="Ellipse 36" cx="4.5"
                                                                    cy="4.5" r="4.5" transform="translate(-0.469 -0.469)"
                                                                    fill="#f46a6a" />
                                                        </g>
                                                        <g id="Group_9316" data-name="Group 9316"
                                                           transform="translate(383.726 253.727)">
                                                            <line id="Line_279" data-name="Line 279" x1="3.65" y2="3.65"
                                                                  transform="translate(0 0)" fill="none" stroke="#fff"
                                                                  stroke-width="1" />
                                                            <line id="Line_280" data-name="Line 280" x2="3.65" y2="3.65"
                                                                  transform="translate(0 0)" fill="none" stroke="#fff"
                                                                  stroke-width="1" />
                                                        </g>
                                                    </g>
                                                </svg>
                                            </span>
                        </li>
                        <li>
                            <span class="badge">Lable</span>
                            <span class="badge-cross"><svg xmlns="http://www.w3.org/2000/svg" width="9"
                                                           height="9" viewBox="0 0 9 9">
                                                    <g id="Group_9340" data-name="Group 9340"
                                                       transform="translate(-381 -251)">
                                                        <g id="Group_9308" data-name="Group 9308"
                                                           transform="translate(381.469 251.469)">
                                                            <circle id="Ellipse_36" data-name="Ellipse 36" cx="4.5"
                                                                    cy="4.5" r="4.5" transform="translate(-0.469 -0.469)"
                                                                    fill="#f46a6a" />
                                                        </g>
                                                        <g id="Group_9316" data-name="Group 9316"
                                                           transform="translate(383.726 253.727)">
                                                            <line id="Line_279" data-name="Line 279" x1="3.65" y2="3.65"
                                                                  transform="translate(0 0)" fill="none" stroke="#fff"
                                                                  stroke-width="1" />
                                                            <line id="Line_280" data-name="Line 280" x2="3.65" y2="3.65"
                                                                  transform="translate(0 0)" fill="none" stroke="#fff"
                                                                  stroke-width="1" />
                                                        </g>
                                                    </g>
                                                </svg>
                                            </span>
                        </li>
                        <li>
                            <span class="badge">Category </span>
                            <span class="badge-cross"><svg xmlns="http://www.w3.org/2000/svg" width="9"
                                                           height="9" viewBox="0 0 9 9">
                                                    <g id="Group_9340" data-name="Group 9340"
                                                       transform="translate(-381 -251)">
                                                        <g id="Group_9308" data-name="Group 9308"
                                                           transform="translate(381.469 251.469)">
                                                            <circle id="Ellipse_36" data-name="Ellipse 36" cx="4.5"
                                                                    cy="4.5" r="4.5" transform="translate(-0.469 -0.469)"
                                                                    fill="#f46a6a" />
                                                        </g>
                                                        <g id="Group_9316" data-name="Group 9316"
                                                           transform="translate(383.726 253.727)">
                                                            <line id="Line_279" data-name="Line 279" x1="3.65" y2="3.65"
                                                                  transform="translate(0 0)" fill="none" stroke="#fff"
                                                                  stroke-width="1" />
                                                            <line id="Line_280" data-name="Line 280" x2="3.65" y2="3.65"
                                                                  transform="translate(0 0)" fill="none" stroke="#fff"
                                                                  stroke-width="1" />
                                                        </g>
                                                    </g>
                                                </svg>
                                            </span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row bg-color-row py-2 mx-0 mt-3">
            <div class="col-lg-8">
                <div class="table-top-options d-flex flex-row">
                    <button type="button" class="btn" id="main_delete_btn"><svg xmlns="http://www.w3.org/2000/svg"
                                                           width="13.006" height="16.428" viewBox="0 0 13.006 16.428">
                            <path id="_25_Basket" data-name="25 Basket"
                                  d="M9.675,16.428H3.333a1.713,1.713,0,0,1-1.711-1.619l-.6-11.044a1.027,1.027,0,1,1,0-2.054H4.791a1.711,1.711,0,1,1,3.423,0h3.765a1.027,1.027,0,0,1,0,2.054l-.6,11.044A1.711,1.711,0,0,1,9.675,16.428ZM8.214,5.476a.685.685,0,0,0-.685.685v6.846a.685.685,0,1,0,1.369,0V6.16A.685.685,0,0,0,8.214,5.476Zm-3.423,0a.685.685,0,0,0-.684.685v6.846a.684.684,0,1,0,1.368,0V6.16A.685.685,0,0,0,4.791,5.476Z"
                                  fill="#a6b0cf" />
                        </svg>
                    </button>
                    <ul class="d-flex flex-wrap flex-md-row align-items-center">
                        <li><span>Displayaing</span></li>
                        <li class="px-2">

                            <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                {{--@foreach($pagination as $key => $value)--}}
                                    {{--<option value="{{$value}}">{{$value}}</option>--}}
                                {{--@endforeach--}}
                            </select>
                        </li>
                        <li><span>of <span id="total_record">0</span></span></li>
                        <li class="pl-2">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="customSwitch1">
                                <label class="custom-control-label"
                                       for="customSwitch1">Incomplete</label>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div
                        class="table-top-options-right d-flex flex-row align-items-center justify-content-md-end mt-3 mt-md-0">
                    <button type="button" class="btn mr-3"><svg xmlns="http://www.w3.org/2000/svg"
                                                                width="15.077" height="12.669" viewBox="0 0 15.077 12.669">
                            <path id="Icon_material-menu" data-name="Icon material-menu"
                                  d="M0,15.077H12.669V12.564H0ZM0,8.795H12.669V6.282H0ZM0,0V2.513H12.669V0Z"
                                  transform="translate(15.077) rotate(90)" fill="#ced4da" />
                        </svg>
                    </button>
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            {{--@if(!empty($pagination))--}}
                                {{--<li class="page-item"><a class="page-link previous" href="javascript:void(0);"><i--}}
                                                {{--class="fas fa-angle-left"></i></a></li>--}}
                                {{--@foreach($pagination as $key => $value)--}}

                                    {{--@if($key == 1) @php($active = 'page-no-active') @else @php($active = '') @endif--}}
                                    {{--<li class="page-item"><a class="page-no {{$active}} get_range_{{$value}} page_changed change_page{{$key}}" data-range="{{$value}}" href="javascript:void(0);" data-val="{{$key}}">{{$key}}</a></li>--}}
                                {{--@endforeach--}}
                                {{--<li class="page-item"><a class="page-link next" href="javascript:void(0);"><i--}}
                                                {{--class="fas fa-angle-right"></i></a></li>--}}
                                {{--<input type="hidden" id="total_records" value="{{$shipping_agents/(env('PAGINATION'))}}">--}}
                            {{--@endif--}}

                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- <div class="row bg-color-row bg-color-row-2 py-3 mx-0 justify-content-center">
            <div class="col-md-6">
                <div class="table-select-txt">
                    <p>Selected 4 out of 100. <a href="#">Select All</a></p>
                </div>
            </div>
        </div> -->
        <div class="row">
            <div class="col-lg-12 suplier-table-box-col">
                <div class="suplier-table-box">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col" width="30%">
                                <ul class="d-flex flex-row align-items-center table-checkbox-main">
                                    <li>
                                        <div class="form-group form-check">
                                            <input type="checkbox" class="form-check-input"
                                                   id="select_all">
                                        </div>
                                    </li>
                                    <li>
                                        <span id="company">Company</span>
                                        <img class="default_img" src="asset/img/table-up-down.png" alt="">
                                    </li>
                                </ul>
                            </th>
                            <th scope="col" id="first_name">First Name <img class="default_img" src="asset/img/table-up-down-two.png"
                                                         alt=""></th>
                            <th scope="col" id="last_name">Last Name <img class="default_img" src="asset/img/table-up-down-two.png"
                                                          alt=""></th>
                            <th scope="col" id="email">Email <img class="default_img" src="asset/img/table-up-down-two.png"
                                                         alt=""></th>
                            <th scope="col" id="phone_number">Phone Number <img class="default_img" src="asset/img/table-up-down-two.png"
                                                       alt=""></th>
                        </tr>
                        </thead>
                    </table>
                    <div class="table-text-selector">
                        <p>Selected <span class="total_selected_record">0</span> out of <span class="total_record">100</span>. <a href="javascript:void(0)" class="select_all">Select All</a></p>
                    </div>
                    <table class="table table-striped">
                        <tbody id="data_listing">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="ui-filter-card position-absolute">
        <div class="row">
            <div class="ui-filter-head">
                <ul class="d-flex">
                    <li><svg xmlns="http://www.w3.org/2000/svg" width="21.205" height="21.205"
                             viewBox="0 0 21.205 21.205">
                            <path id="Icon_awesome-filter" data-name="Icon awesome-filter"
                                  d="M20.21,0H1a1,1,0,0,0-.7,1.7l7.66,7.661v8.534a.994.994,0,0,0,.424.814l3.313,2.318a1,1,0,0,0,1.564-.814V9.358L20.913,1.7A1,1,0,0,0,20.21,0Z"
                                  fill="#495057" />
                        </svg>
                    </li>
                    <li>
                        <h2>Filter</h2>
                    </li>
                </ul>
                <p>You can pin filters on the page by clicking on <span class="px-2"><svg
                                xmlns="http://www.w3.org/2000/svg" width="10.528" height="15.511"
                                viewBox="0 0 10.528 15.511">
                                            <path id="Icon_metro-pin" data-name="Icon metro-pin"
                                                  d="M17.594,15.049,12.417,12.06a1.328,1.328,0,1,0-1.328,2.3l5.176,2.989a1.328,1.328,0,0,0,1.329-2.3ZM12.939,12.68l4.151,2.166,1.631-4.515-2.781-1.6-3,3.955Zm-2.583,8.928,4.719-5.348-2.136-1.594-2.583,6.943ZM20.385,8.223,16.934,6.23a1,1,0,0,0-1.361.365A2.767,2.767,0,0,0,16.1,9.011l2.5,1.319a2.379,2.379,0,0,0,2.156-.747A1,1,0,0,0,20.385,8.223Z"
                                                  transform="translate(-10.356 -6.096)" fill="#495057" />
                                        </svg>
                                    </span> this button.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="ui-filter-card-input">
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Brand</label>
                        <div class="d-flex align-items-center">
                            <select class="form-control" id="exampleFormControlSelect1">
                                <option>Select Brand</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                            <span class="pl-3">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="10.528" height="15.511"
                                                     viewBox="0 0 10.528 15.511">
                                                    <path id="Icon_metro-pin" data-name="Icon metro-pin"
                                                          d="M17.594,15.049,12.417,12.06a1.328,1.328,0,1,0-1.328,2.3l5.176,2.989a1.328,1.328,0,0,0,1.329-2.3ZM12.939,12.68l4.151,2.166,1.631-4.515-2.781-1.6-3,3.955Zm-2.583,8.928,4.719-5.348-2.136-1.594-2.583,6.943ZM20.385,8.223,16.934,6.23a1,1,0,0,0-1.361.365A2.767,2.767,0,0,0,16.1,9.011l2.5,1.319a2.379,2.379,0,0,0,2.156-.747A1,1,0,0,0,20.385,8.223Z"
                                                          transform="translate(-10.356 -6.096)" fill="#227cff" />
                                                </svg>
                                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="ui-filter-card-input">
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Category</label>
                        <div class="d-flex align-items-center">
                            <select class="form-control" id="exampleFormControlSelect1">
                                <option>Select Brand</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                            <span class="pl-3">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="10.528" height="15.511"
                                                     viewBox="0 0 10.528 15.511">
                                                    <path id="Icon_metro-pin" data-name="Icon metro-pin"
                                                          d="M17.594,15.049,12.417,12.06a1.328,1.328,0,1,0-1.328,2.3l5.176,2.989a1.328,1.328,0,0,0,1.329-2.3ZM12.939,12.68l4.151,2.166,1.631-4.515-2.781-1.6-3,3.955Zm-2.583,8.928,4.719-5.348-2.136-1.594-2.583,6.943ZM20.385,8.223,16.934,6.23a1,1,0,0,0-1.361.365A2.767,2.767,0,0,0,16.1,9.011l2.5,1.319a2.379,2.379,0,0,0,2.156-.747A1,1,0,0,0,20.385,8.223Z"
                                                          transform="translate(-10.356 -6.096)" fill="#ced4da" />
                                                </svg>
                                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pt-4 ui-filter-card-input-price">
            <div class="col-6">
                <div class="row">
                    <div class="col-6 align-self-end">
                        <div class="d-flex align-items-center">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Price Point</label>
                                <div class="d-flex align-items-center">
                                    <input type="email" class="form-control"
                                           id="exampleFormControlInput1" placeholder="$0.00">

                                    <div class="pl-3">-</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 align-self-end">
                        <div class="d-flex align-items-center">
                            <div class="form-group">
                                <input type="email" class="form-control" id="exampleFormControlInput1"
                                       placeholder="$0.00">
                            </div>
                            <span class="pl-3">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="10.528" height="15.511"
                                                     viewBox="0 0 10.528 15.511">
                                                    <path id="Icon_metro-pin" data-name="Icon metro-pin"
                                                          d="M17.594,15.049,12.417,12.06a1.328,1.328,0,1,0-1.328,2.3l5.176,2.989a1.328,1.328,0,0,0,1.329-2.3ZM12.939,12.68l4.151,2.166,1.631-4.515-2.781-1.6-3,3.955Zm-2.583,8.928,4.719-5.348-2.136-1.594-2.583,6.943ZM20.385,8.223,16.934,6.23a1,1,0,0,0-1.361.365A2.767,2.767,0,0,0,16.1,9.011l2.5,1.319a2.379,2.379,0,0,0,2.156-.747A1,1,0,0,0,20.385,8.223Z"
                                                          transform="translate(-10.356 -6.096)" fill="#ced4da" />
                                                </svg>

                                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="row">
                    <div class="col-6 align-self-end">
                        <div class="d-flex align-items-center">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Cost</label>
                                <div class="d-flex align-items-center">
                                    <input type="email" class="form-control"
                                           id="exampleFormControlInput1" placeholder="$0.00">

                                    <div class="pl-3">-</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 align-self-end">
                        <div class="d-flex align-items-center">
                            <div class="form-group">
                                <input type="email" class="form-control" id="exampleFormControlInput1"
                                       placeholder="$0.00">
                            </div>
                            <span class="pl-3">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="10.528" height="15.511"
                                                     viewBox="0 0 10.528 15.511">
                                                    <path id="Icon_metro-pin" data-name="Icon metro-pin"
                                                          d="M17.594,15.049,12.417,12.06a1.328,1.328,0,1,0-1.328,2.3l5.176,2.989a1.328,1.328,0,0,0,1.329-2.3ZM12.939,12.68l4.151,2.166,1.631-4.515-2.781-1.6-3,3.955Zm-2.583,8.928,4.719-5.348-2.136-1.594-2.583,6.943ZM20.385,8.223,16.934,6.23a1,1,0,0,0-1.361.365A2.767,2.767,0,0,0,16.1,9.011l2.5,1.319a2.379,2.379,0,0,0,2.156-.747A1,1,0,0,0,20.385,8.223Z"
                                                          transform="translate(-10.356 -6.096)" fill="#ced4da" />
                                                </svg>

                                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-end pt-4">
            <div class="col-lg-6">
                <div class="ui-filter-card-btn text-right">
                    <button type="button" class="btn ui-filter-cancel">Cancel</button>
                    <button type="button" class="btn ui-filter-applay ml-1">Apply</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>

        var table = $('table');
        $('.default_img').attr("src", "{{ URL::asset('asset/img/table-up-down-two.png')}}");

        $('#company, #first_name, #last_name, #email, #phone_number')
            .wrapInner('<span title="sort this column"/>')
            .each(function(){
                $(this).find('.default_img').attr("src", "{{ URL::asset('asset/img/table-up-down-two.png')}}");

                var th = $(this),
                    thIndex = th.index(),
                    inverse = false;

                th.click(function(){

                    $( '.default_img').each(function( index ) {
                        console.log($(this));
                        $(this).attr("src", "{{ URL::asset('asset/img/table-up-down-two.png')}}");
                    });
                    $(this).find('.default_img').attr("src", "{{ URL::asset('asset/img/table-up-down.png')}}");

                    table.find('.first_field').filter(function(){
                        //console.log($(this).attr(src));
                        //$(this).attr("src",);
                        return $(this).index() === thIndex;

                    }).sortElements(function(a, b){

                        return $.text([a]) > $.text([b]) ?
                            inverse ? -1 : 1
                            : inverse ? 1 : -1;

                    }, function(){

                        // parentNode is the element we want to move
                        return this.parentNode;

                    });
                    table.find('td').filter(function(){
                        return $(this).index() === thIndex;

                    }).sortElements(function(a, b){

                        return $.text([a]) > $.text([b]) ?
                            inverse ? -1 : 1
                            : inverse ? 1 : -1;

                    }, function(){

                        // parentNode is the element we want to move
                        return this.parentNode;

                    });

                    inverse = !inverse;

                });

            });

    </script>
    <script>
        $('.select_all').click(function(e){
            var count_all = $(".total_record").text();

            if($("#select_all").prop('checked') == true) {
                $('input[name=delete_all]').prop('checked', false);
                $("#select_all").prop('checked', false);
                $(".total_selected_record").text(0);
            }else{
                $('input[name=delete_all]').prop('checked', true);
                $("#select_all").prop('checked', true);
                $(".total_selected_record").text(count_all);
            }
        });

        $("#select_all").click(function() {
            var count_all = $(".total_record").text();
            $(".total_selected_record").text(count_all);
            $("input[name=delete_all]").prop("checked", $(this).prop("checked"));
            if($(this).prop('checked') == false){
                $(".total_selected_record").text(0);
            }
        });

        $(document).on('click','input[name=delete_all]',function(){
            //total_inc = Number(1);
            var count_all = $(".total_selected_record").text();
            if (!$(this).prop("checked")) {
                $("#select_all").prop("checked", false);
                var count = parseInt(count_all)  - 1;
                $(".total_selected_record").text(count);
            }else{
                var count = parseInt(count_all) + 1;
                $(".total_selected_record").text(count);
            }
        });

        $(document).on('click','#main_delete_btn',function(){
            var delete_array = [];
            var count_all = $(".total_record").text();
            $("input:checkbox[name=delete_all]:checked").each(function(){
                delete_array.push($(this).val());
            });
            if(delete_array.length > 0){
                var join_selected_values = delete_array.join(",");
                swal({
                        title: "Are you sure?",
                        text: "You want to delete these record",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Delete it!",
                        cancelButtonText: "No, cancel please!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "POST",
                                url:'{{route('shippingagent.store')}}',
                                data:{delete_array:join_selected_values,insert_type:"delete_multiple_records"},
                                dataType: "html",
                                success: function (data) {
                                    var dataResult = JSON.parse(data);
                                    if(dataResult.statusCode==200){
                                        $("input:checkbox[name=delete_all]:checked").each(function(){
                                            var ids = $(this).val();
                                            $("#"+ids).remove();
                                        });
                                        var count = parseInt(count_all)  - delete_array.length ;
                                        $(".total_record").text(count);
                                        $(".total_selected_record").text(0);
                                        swal({
                                            title: "Done!",
                                            text: "It was succesfully deleted!",
                                            type: "success",
                                            timer: 700
                                        });
                                    }
                                }
                            });
                        }
                        else
                        {
                            swal("Cancelled", "", "error");
                        }
                    });
            }else{
                alert('Please select records');
            }

        });

    </script>

    <script>
        $(document).on('click', '.delete_single', function () {
            //e.preventDefault();
            var count_all = $(".total_record").text();
            var id = $(this).data('id');
            var url = "{{URL('shippingagent')}}";
            var destroyurl = url+"/"+id; if(id != ''){

                swal({
                        title: "Are you sure?",
                        text: "You want to delete this record",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Delete it!",
                        cancelButtonText: "No, cancel please!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {

                            $.ajax({
                                type: "DELETE",
                                url:destroyurl,
                                data:{ _token:'{{ csrf_token() }}'},
                                dataType: "html",
                                success: function (data) {
                                    var dataResult = JSON.parse(data);
                                    if(dataResult.statusCode==200){
                                        $("#"+id).remove();
                                        var count = parseInt(count_all)  - 1 ;
                                        $(".total_record").text(count);
                                        swal({
                                            title: "Done!",
                                            text: "It was succesfully deleted!",
                                            type: "success",
                                            timer: 700
                                        });
                                    }
                                }

                            });
                        }
                        else
                        {
                            swal("Cancelled", "", "error");
                        }
                    });
            }else{
                alert('No found record!');
            }

        });
    </script>
    <script>
        var pagecount = 1;
        $(document).ready(function () {
            var get_country = $("#select_country").val();
            get_pagi_country(get_country);
        });

        $(document).on('click','#select_country',function(){
            var get_country = $(this).val();
            get_pagi_country(get_country);
        });

        function get_pagi_country(get_country){
            var request_type = "get_pagi_country";
            $.ajax({
                type:"POST",
                url:"{{ route('shippingagent.store') }}",
                data:{
                    request_type: request_type,
                    get_country:get_country
                },
                dataType:"json",
                success:function(res){
                    if(res.success == 1){
                        $('#inlineFormCustomSelect').html(res.select_pagi);
                        $('.pagination').html(res.paginations);
                        $("#total_record").text(res.count);
                        $(".total_record").text(res.count);
                        getData(get_country);
                    }
                }
            });
        }

        $('#inlineFormCustomSelect').change(function(){
            inlineFormCustomSelect.options[inlineFormCustomSelect.selectedIndex].innerHTML;
            var get_range = $(this).find("option:selected").text();
            $('.pagination li a').removeClass('page-no-active');
            $('.get_range_'+get_range).addClass('page-no-active');
            var get_country = $('#select_country').val();
            getData(get_country);
            //getData();
        });

        $(document).on('click','.page_changed',function(){
            var range = $(this).data('range');
            $('.pagination li a').removeClass('page-no-active');
            $(this).addClass('page-no-active');
            $("#inlineFormCustomSelect option").filter(function() {
                return this.text == range;
            }).attr('selected', true);

            $("#inlineFormCustomSelect option").filter(function() {
                return this.text != range;
            }).attr('selected', false);

            //$("#inlineFormCustomSelect option:contains[text=" + range +"]").attr("selected","selected") ;
            //$('#inlineFormCustomSelect').find('option[text='+range+']').attr('selected','selected');
            pagecount = $(this).data('val');
            var get_country = $('#select_country').val();
            getPagination($(this).data('val'),get_country);
        });

        $(document).on('click','.previous',function(){
            if(pagecount > 1){
                $('.pagination li a').removeClass('page-no-active');
                pagecount = pagecount - 1;
                $('.change_page'+pagecount).addClass('page-no-active');
                var get_country = $('#select_country').val();
                getPagination(pagecount,get_country);
            }
        });
        $(document).on('click','.next',function(){
            var total_records = $("#total_records").val();
            if(pagecount < total_records) {
                $('.pagination li a').removeClass('page-no-active');
                pagecount = pagecount + 1;
                $('.change_page'+pagecount).addClass('page-no-active');
                var get_country = $('#select_country').val();
                getPagination(pagecount,get_country);
            }
        });

        function getData(get_country) {
            var offsetValue = $('#inlineFormCustomSelect').val();
            var request_type = "get_all_shipping_data";
            $.ajax({
                type:"POST",
                url:"{{ route('shippingagent.store') }}",
                data:{
                    offsetValue:offsetValue,
                    request_type: request_type,
                    get_country:get_country
                },
                success:function(res){
                    $("#data_listing").html(res);
                }
            });
        }
        function getPagination(offsetValue,get_country){
            var offset = offsetValue * <?php echo env('PAGINATION'); ?>;
            var mail_offset = offset - <?php echo env('PAGINATION') - 1 ; ?>;

            var offsetValue = mail_offset+'-'+offset;
            var request_type = "get_all_shipping_data";
            $.ajax({
                type:"POST",
                url:"{{ route('shippingagent.store') }}",
                data:{
                    offsetValue:offsetValue,
                    request_type: request_type,
                    get_country:get_country,
                },
                success:function(res){
                    $("#data_listing").html(res);
                }
            });
        }
    </script>

@endsection
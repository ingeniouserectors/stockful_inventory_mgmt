@extends('layouts.master')

@section('title')  Reorder View @endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Reorder View</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('reorder') }}">Reorder</a></li>
                        <li class="breadcrumb-item active">View</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="row">
        <div class="col-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">
             <div class="card-body">

                 
                  <div class="form-group row">

                                 <div class="col-md-2">
                                     <img width="100px" src="{{$product_data['prod_image']}}" title="{{$product_data['prod_names']}}">
                                 </div>
                                 <div class="col-md-10">
                                     {{ $product_data['prod_name']}}
                                     <hr>
                                     {{ $product_data['sku']}}
                                 </div>
                         </div>
                <hr/>
                <div class="form-group row">
                    <label for="example-text-input" class="col-md-2">Total Inventory : </label>
                    <div class="col-md-4">{{$reorder_details->total_inventory}}</div>
                    <label for="example-text-input" class="col-md-2">Days Supply : </label>
                    <div class="col-md-4">{{$reorder_details->days_supply}}</div>
                </div>
                 <div class="form-group row">
                     <label for="example-text-input" class="col-md-2">Reorder Date : </label>
                     <div class="col-md-4">{{date('Y-m-d',strtotime($reorder_details['projected_reorder_date']))}}</div>
                     <label for="example-text-input" class="col-md-2">Projected Reorder Qty: </label>
                     <div class="col-md-4">{{$reorder_details->projected_reorder_qty}}</div>
                 </div>
                 <div class="form-group row">
                     <label for="example-text-input" class="col-md-2">Lead Time Amazon : </label>
                     <div class="col-md-4">{{$reorder_details->lead_time_amazon}}</div>
                     <label for="example-text-input" class="col-md-2">Order Frequency : </label>
                     <div class="col-md-4">{{$reorder_details->order_frequency}}</div>
                 </div>
                 <div class="form-group row">
                     <label for="example-text-input" class="col-md-2">Day Rate : </label>
                     <div class="col-md-4">{{$reorder_details->day_rate}}</div>
                     <label for="example-text-input" class="col-md-2">Blackout Days : </label>
                     <div class="col-md-4">{{$reorder_details->blackout_days}}</div>
                 </div>
                 <div class="form-group row">
                     <label for="example-text-input" class="col-md-2">Days Left To Order : </label>
                     <div class="col-md-4">{{$reorder_details->days_left_to_order}}</div>
                     <label for="example-text-input" class="col-md-2">Projected Order Date Range : </label>
                     <div class="col-md-4">{{$reorder_details->projected_order_date_range}}</div>
                 </div>
                 <hr/>
                 <b>Recent Sales :</b><br/><br/>

                 <div class="form-group row">
                     <label for="example-text-input" class="col-md-2">7 days : </label>
                     <div class="col-md-4">{{$reorder_details->sevendays_sales}}</div>
                     <label for="example-text-input" class="col-md-2">7 days rate : </label>
                     <div class="col-md-4">{{$reorder_details->sevenday_day_rate}}</div>
                 </div>

                 <div class="form-group row">
                     <label for="example-text-input" class="col-md-2">14 days : </label>
                     <div class="col-md-4">{{$reorder_details->fourteendays_sales}}</div>
                     <label for="example-text-input" class="col-md-2">14 days rate : </label>
                     <div class="col-md-4">{{$reorder_details->fourteendays_day_rate}}</div>
                 </div>

                 <div class="form-group row">
                     <label for="example-text-input" class="col-md-2">30 days : </label>
                     <div class="col-md-4">{{$reorder_details->thirtydays_sales}}</div>
                     <label for="example-text-input" class="col-md-2">30 days rate : </label>
                     <div class="col-md-4">{{$reorder_details->thirtyday_day_rate}}</div>
                 </div>
                 <hr/>
                 <b>Trends :</b><br/><br/>

                 <div class="form-group row">
                     <label for="example-text-input" class="col-md-2">7 days Previous: </label>
                     <div class="col-md-4">{{$reorder_details->sevendays_previous}}</div>
                     <label for="example-text-input" class="col-md-2">7 days Previous Day Rate : </label>
                     <div class="col-md-4">{{$reorder_details->sevendays_previous_day_rate}}</div>
                 </div>

                 <div class="form-group row">
                     <label for="example-text-input" class="col-md-2">30 days Previous: </label>
                     <div class="col-md-4">{{$reorder_details->thirtydays_previous}}</div>
                     <label for="example-text-input" class="col-md-2">30 days Previous Day Rate: </label>
                     <div class="col-md-4">{{$reorder_details->thirtydays_previous_day_rate}}</div>
                 </div>

                 <div class="form-group row">
                     <label for="example-text-input" class="col-md-2">30 days Last year: </label>
                     <div class="col-md-4">{{$reorder_details->thirtydays_last_year}}</div>
                     <label for="example-text-input" class="col-md-2">30 days LastYear Day Rate: </label>
                     <div class="col-md-4">{{$reorder_details->thirtydays_last_year_day_rate}}</div>
                 </div>

                 <div class="button-items mt-3" align="center">
                        <a class="btn btn-info" href="{{ url('reorder') }}" role="button" style="width:116px;">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection

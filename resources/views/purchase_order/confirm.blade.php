@extends('layouts.master')

@section('title') Confirm Purchase Order @endsection

@section('content')
    <style>
        .disable_data  {
            cursor: no-drop;
            color: #74788d !important;
            background-color: #eff2f7 !important;
        }
    </style>

    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Confirm Purchase Order</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('purchaseinstances.index') }}">Purchase Order</a></li>
                        <li class="breadcrumb-item active">Confirm Purchase Order</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">
                <div class="card-body">
                    <form name="confirm-purchase_order" id="confirm-purchase_order" action="{{ route('purchaseorder.update',$id) }}" method="POST" onreset="myFunction()">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="input_type" value="Create_form">
                        <b>Purchase Order : </b>
                        <br/><br/>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Date Of Ship</label>
                            <div class="col-md-4">
                                <input class="form-control min-today @error('date_of_ship') is-invalid @enderror" type="date" value="" name="date_of_ship" id="date_of_ship" max="3000-01-01">
                                @if ($errors->has('date_of_ship'))
                                    <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('date_of_ship') }}</strong></span>
                                @endif
                            </div>
                            <label class="col-md-2 col-form-label">Date Arrived </label>
                            <div class="col-md-4">
                                <input class="form-control min-today @error('date_arrieved') is-invalid @enderror" type="date" value="" name="date_arrieved" id="date_arrieved" max="3000-01-01">
                                @if ($errors->has('date_arrieved'))
                                    <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('date_arrieved') }}</strong></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Reorder Date</label>
                            <div class="col-md-4">
                                <input class="form-control min-today @error('reorder_date') is-invalid @enderror" type="date" value="" name="reorder_date" id="reorder_date" max="3000-01-01">
                                @if ($errors->has('reorder_date'))
                                    <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('reorder_date') }}</strong></span>
                                @endif
                            </div>
                            <label class="col-md-2 col-form-label">Day Rate </label>
                            <div class="col-md-4">
                                <input class="form-control @error('day_rate') is-invalid @enderror" type="text" value="" name="day_rate" id="day_rate" >
                                @if ($errors->has('day_rate'))
                                    <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('day_rate') }}</strong></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Day Of Supply</label>
                            <div class="col-md-4">
                                <input class="form-control @error('days_of_supply') is-invalid @enderror" type="text" value="" name="days_of_supply" id="days_of_supply" >
                                @if ($errors->has('days_of_supply'))
                                    <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('days_of_supply') }}</strong></span>
                                @endif
                            </div>
                            <label class="col-md-2 col-form-label">Blackout Days</label>
                            <div class="col-md-4">
                                <input class="form-control @error('blakoute_days') is-invalid @enderror" type="text" value="" name="blakoute_days" id="blakoute_days" >
                                @if ($errors->has('blakoute_days'))
                                    <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('blakoute_days') }}</strong></span>
                                @endif
                            </div>
                        </div>

                        <div class="button-items mt-3">
                            <input class="btn btn-info" type="submit" value="Submit" id="submit">
                            <a class="btn btn-danger waves-effect waves-light" href="{{ route('purchaseinstances.index') }}" role="button">Cancel</a>
                            <input class="btn btn-warning" type="reset" value="Reset">
                        </div>

                    </form>
                </div>
            </div>

                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-4">Track ID : {{$purchase['track_id']}}</h4>
                        <div class="box">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered dt-responsive nowrap datatable" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead class="thead-light">
                                    <tr>
                                        <th>No.</th>
                                        <th>Product</th>
                                        <th>Vendor/Supplier </th>
                                        <th>Warehouse</th>
                                        <th>Projected Units</th>
                                        <th>Items/Cartons</th>
                                        <th>CBM</th>
                                        <th>Containers</th>
                                        <th>Subtotal</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $no = 1; ?>
                                    @if(!empty($purchase_details))
                                        @foreach($purchase_details as $users)
                                            @php($vendors_suppliers_name = ' - ')
                                            @php($get_warehouse_name = ' - ')
                                            @php($product_name = ' - ')

                                            @if($users['supplier'] != 0)
                                                @php($vendors_suppliers_name = get_suppliers_name($users['supplier']))
                                            @endif
                                            @if($users['vendor'] != 0)
                                                @php($vendors_suppliers_name = get_vendors_name($users['vendor']))
                                            @endif
                                            @if($users['warehouse'])
                                                @php($get_warehouse_name = get_warehouse_name($users['warehouse']))
                                            @endif
                                            @if($users['product_id'])
                                                @php($product_name = get_product_details($users['product_id']))
                                            @endif

                                            <tr>
                                                <td>{{$no++}}</td>
                                                <td title="{{$product_name}}">{{substr(strip_tags($product_name),0,25)."..."}}</td>
                                                <td>{{$vendors_suppliers_name}}</td>
                                                <td>{{$get_warehouse_name}}</td>
                                                <td>{{$users['projected_units']}}</td>
                                                <td>{{$users['items_per_cartoons']}}</td>
                                                <td>{{$users['cbm']}}</td>
                                                <td>{{$users['containers']}}</td>
                                                <td>{{$users['subtotal']}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->

@endsection

@section('script')
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script>

        $("#day_rate").keypress(function (e)
        {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
        $("#days_of_supply").keypress(function (e)
        {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
        $("#blakoute_days").keypress(function (e)
        {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
        $(document).ready(function(){
            $('[type="date"].min-today').prop('min', function(){
                return new Date().toJSON().split('T')[0];
            });
        });
    </script>
    <script>
        $(document).ready(function(){
            var id = $('.product_listing').val();
            get_product_datas(id);
        });
        function get_product_datas(id){
            var dataroletype = 'vendors_suppliers_warehouse';
            $.ajax({
                type: "POST",
                url: '{{ route('purchaseinstances.store') }}',
                data: {
                    product_id : id,
                    input_type : dataroletype,
                },
                success: function (response) {
                    $('#vendors_select_id').val(response.vendors_id);
                    $('#suppliers_select_id').val(response.suppliers_id);
                    $('#warehouse_select_id').val(response.warehouseid);
                    $('#vendors_suppliers_name').val(response.vendors_suppliers_name);
                    $('#warehouse_name').val(response.warehousename);
                }
            });
        }
    </script>
    <script type="text/javascript">
     $(function () {

       $('.datatable').DataTable({
           "pageLength": 10,
           'autoWidth': false,
           "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
       });
   });
   </script> 
@endsection
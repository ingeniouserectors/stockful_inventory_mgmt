 @extends('layouts.master')

@section('title') Purchase Order View @endsection

@section('content')


    <style>
        .design{
            cursor: no-drop;
            color: #74788d !important;
            background-color: #eff2f7 !important;
        }

        table, th, td {
          border: 2px solid ;
          border-collapse: collapse;
        }
    </style>

    <div class="row">
        <div class="col-12">

            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Purchase Order View</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('purchaseorder.index') }}">Purchase Order</a></li>
                        <li class="breadcrumb-item active">View</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="row">
        <div class="col-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">

                @php($vendors_suppliers_name = ' - ')
                @if(isset($view->purachse_order_details->vendor) && $view->purachse_order_details->vendor != 0)
                    @php($vendors_suppliers_name = get_vendors_name($view->purachse_order_details->vendor))
                @endif

                @php($warehouse_name = ' - ')
                @if(isset($view->purachse_order_details->warehouse) && $view->purachse_order_details->warehouse != 0)
                    @php($warehouse_name = get_warehouse_name($view->purachse_order_details->warehouse))
                @endif
     
             <div class="card-body">
                                <div class="form-group row">
                                    <div class="column" style="margin-right: 115px"> 
                                    <span style="font-size: 15px;color:black;">Date:</span>&nbsp;&nbsp;{{date('Y-m-d',strtotime($view['created_at']))}}<br><br>
                                     <span style="font-size: 15px;color:black;">Date Arrived :</span>&nbsp;&nbsp;{{$view->date_arrived}}<br><br>
                                     <span style="font-size: 15px;color:black;">Days Of Supply:&nbsp;&nbsp;{{$view->days_of_supply}}

                                </div>
                                   
                                <div class="column" style="margin-right: 115px"> 
                                   <span style="font-size: 15px;color:black;"> Track Id:</span>&nbsp;&nbsp;{{$view->track_id}}<br><br>
                                   <span style="font-size: 15px;color:black;"> Reorder Date:</span>&nbsp;&nbsp;{{$view->reorder_date}}<br><br>
                                    <span style="font-size: 15px;color:black;">Blackout Days:</span>&nbsp;&nbsp;{{$view->blackout_days}}

                                   
                                </div>
                                <div class="column">   
                                   <span style="font-size: 15px;color:black;"> Date Of Ship :</span>&nbsp;&nbsp;{{$view->date_of_ship}}<br><br>
                                    <span style="font-size: 15px;color:black;">Day Rate:</span>&nbsp;&nbsp;{{$view->day_rate}}<br><br>
                                    <span style="font-size: 15px;color:black;">Status:</span>&nbsp;&nbsp; 
                                    {{isset($view->status) && $view->status != '' && $view->status == 2 ? 'completed' : 'pending'}}

                                </div>
                                </div>                                   

                 <div class="box">            
                 <div class="table-responsive">
                 <table class="table table-striped table-bordered dt-responsive nowrap users-datatable" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                     <thead class="thead-light">
                              <tr>
                    
                                <th>Supplier/Vendors</th>
                                <th>Warehouse</th> 
                                <th>Product Name</th> 
                                <th>SKU</th>
                                <th>Projected Units</th>
                                <th>Items/Cartoons</th>
                                <th>CBM</th>
                                <th>Containers</th>
                                <th>Subtotal</th>
                                
                              </tr>
                          </thead>
                           <tbody>
                           @if(!empty($order_details))
                               @foreach($order_details as $order)

                                   @if(isset($order['vendor']) && $order['vendor'] != 0)
                                       @php($vendors_suppliers_name = get_vendors_name($order['vendor']))
                                   @endif

                                   @php($warehouse_name = ' - ')
                                   @if(isset($order['warehouse']) && $order['warehouse'] != 0)
                                       @php($warehouse_name = get_warehouse_name($order['warehouse']))
                                   @endif

                                    @php($product_name = ' - ')
                                   @if(isset($order['product_id']) && $order['product_id'] != 0)
                                       @php($product_name = get_product_details($order['product_id']))
                                   @endif

                                   @php($product_sku = ' - ')
                                   @if(isset($order['product_id']) && $order['product_id'] != 0)
                                       @php($product_sku = get_product_sku($order['product_id']))
                                   @endif

                                    <tr>

                                    <td>{{$vendors_suppliers_name}}</td>
                                    <td>{{$warehouse_name}}</td>
                                     <td title="{{$product_name}}">{{substr(strip_tags($product_name),0,25)."..."}}</td>
                                    <td>{{isset($product_sku) && $product_sku != '' ? $product_sku : ''}}</td>
                                    <td>{{isset($order['projected_units']) && $order['projected_units'] != '' ? $order['projected_units'] : '' }}</td>
                                    <td>{{isset($order['items_per_cartoons']) && $order['items_per_cartoons'] != '' ? $order['items_per_cartoons'] : '' }}</td>
                                    <td>{{isset($order['cbm']) && $order['cbm'] != '' ? $order['cbm']: ''}}</td>
                                    <td>{{isset($order['containers']) && $order['containers'] != '' ? $order['containers'] : ''}}</td>
                                    <td>{{isset($order['subtotal']) && $order['subtotal'] != '' ? $order['subtotal'] : ''}}</td>

                                  </tr>
                               @endforeach
                               @else
                                <tr><td colspan="10" align="center">No record found.</td></tr>
                                    
                               @endif

                          </tbody>
                            </table>
                        </div>
                    </div>                            
                    <div class="button-items mt-3" align="center">
                        <a class="btn btn-info" href="{{ route('purchaseorder.index') }}" role="button" style="width:116px;">Back</a>
                    </div>                                
            </div>
        </div>
    </div>
@endsection

@extends('layouts.master')

@section('title') Reorder @endsection

@section('content')

    <div class="row">
        <div class="col-12">

            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Reorder</h4>
                 <input type="hidden" value="{{$marketplace}}" name="marketplaces" id="marketplaces">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Reorder</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 error_message">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">

            </div>
        </div>
    </div>
    <!-- end row -->
    <div class="card">

        <div class="card-body" style="margin-bottom: 30px;">
            <h4 class="card-title mb-4">Reorder List</h4>
            <div class="box">
                <div class="table-responsive">
                    <table id="datatables" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead class="thead-light">
                        <tr>
                            <th>No.</th>
                            <th>Title</th>
                            <th>Image </th>
                            <th>SKU</th>
                            <th>Price</th>
                            <th>Inventory</th>
                            <th>Days Of Supply</th>
                            <th>Reorder Date</th>
                            <th>Projected Reorder Qty</th>
                            <th>Cost</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div> 

        @endsection
        @section('script')

            <script>
                $(document).ready(function () {
            var id = $('#marketplaces').val();
                     loadDashboard(id);
        });
        function loadDashboard(id){
           var reorder_list='reorder_list';
            var count=1;
            var table = $('#datatables').DataTable({
                "order": [ 0, 'asc' ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('purchaseorder.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.id=id,d.request_type=reorder_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                success:function($res){
                    console.log(res);
                    return false;
                },
                'columnDefs': [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return  row['prod_name'];
                        }
                    },
                    {
                         targets: 2,
                        render: function (data, type, row) {
                            return row['prod_image'];
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, row) {
                            return row['sku'];

                        }
                    },
                    {
                        targets: 4,
                        render: function (data, type, row) {
                            return row['price'];
                        }
                    },
                    {
                        targets: 5,
                        render: function (data, type, row) {
                            return row['total_inventory'];
                        }
                    },
                     {
                        targets: 6,
                        render: function (data, type, row) {
                            return row['days_supply'];
                        }
                    },
                     {
                        targets: 7,
                        render: function (data, type, row) {
                            return row['projected_reorder_date'];
                        }
                    },
                     {
                        targets: 8,
                        render: function (data, type, row) {
                            return row['projected_reorder_qty'];
                        }
                    },
                     {
                        targets: 9,
                        render: function (data, type, row) {
                            return row['sales_price'];
                        }
                    },
                    {
                        targets: 10,
                        render: function (data, type, row) {
                           return row['action']
                        }
                    }
                ]
            });
        }     
            </script>

         <script>
             $(document).on('click','.addtocart',function () {
                 var product_id = $(this).data('product_id');
                 var insert_type = 'Addtopurchaseinstance';
                 $.ajax({
                     type:"POST",
                     url:'{{ route('products.store') }}',
                     data:{product_id:product_id,insert_type:insert_type},
                     datatype:"json",
                     success: function(res){
                         $('.error_message').html(res.message);
                         $("html, body").animate({ scrollTop: 0 }, "slow");
                     },
                     error: function () {
                         console.log('Something wrong');
                     }
                 });
             });
         </script>
@endsection
@extends('layouts.master')

@section('title') Purchase Order List @endsection

@section('content')

    <div class="row">
        <div class="col-12">

            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Purchase Order</h4>
                 <input type="hidden" value="{{$marketplace}}" name="marketplaces" id="marketplaces">

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Purchase Order</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-5">
           
            <a class="btn btn-info waves-effect waves-light mb-3"  href="{{ route('purchaseorder.show','import') }}" role="button"><i class="fa fa-download"></i> Download CSV File</a>
        </div>
        <div class="col-lg-7">
        <form action="{{route('purchaseorder.store')}}" method="post" id="import_csv"  enctype="multipart/form-data">
            {{ csrf_field() }}
                <label>Import CSV file : *</label>
                <input type="hidden" name="insert_type" value="upload_csv">
                <input type="file" name="uploadFile" accept=".csv">
                <input type="submit" class="btn btn-primary import_csv" value="Import CSV File">
                
        </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">

            </div>
        </div>
    </div>
    <!-- end row -->
    <div class="card">

        <div class="card-body" style="margin-bottom: 30px;">
            <h4 class="card-title mb-4">Purchase Order List</h4>
              @if(!empty($Completed_Purchase_order) || !empty($pending_purchase_order))
                                <ul class="nav nav-tabs nav-tabs-custom">
                                    <li class="nav-item"> <a href="#pendding" class="nav-link active" data-toggle="tab"> Pending ({{ count($pending_purchase_order) }}) </a></li>
                                    <li class="nav-item"><a href="#Received"  class="nav-link" data-toggle="tab"> Received ({{ count($Completed_Purchase_order) }})</a></li>
                                </ul>
                            @endif
              <div class="tab-content p-3 text-muted">
            <div class="tab-pane active" id="pendding">                
            <div class="box">
                <div class="table-responsive">
                    <table id="datatables" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead class="thead-light">
                        <tr>
                            <th>No.</th>
                            <th>Date</th>
                            <th>Track Id</th>
                            <th>Date Of Ship </th>
                            <th>Date Arrieved</th>
                            <th>Reorder Date</th>
                            <th>Days Of Supply</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                </div>
            </div>
            <div class="tab-pane" id="Received">                
            <div class="box">
                <div class="table-responsive">
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead class="thead-light">
                        <tr>
                            <th>No.</th>
                            <th>Date</th>
                            <th>Track Id</th>
                            <th>Date Of Ship </th>
                            <th>Date Arrieved</th>
                            <th>Reorder Date</th>
                            <th>Days Of Supply</th>
                            <th>Action</th>
                          
                        </tr>
                        </thead>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
   <script type="text/javascript">
     $(document).ready(function () {
            var id = $('#marketplaces').val();
                     loadDashboard(id);
        });
        function loadDashboard(id){
           var purchase_order_list='purchase_order_list';
            var count=1;
            var table = $('#datatables').DataTable({

                "order": [ 0, 'asc' ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('purchaseorder.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.id=id,d.status=status,d.request_type=purchase_order_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                success:function($res){
                    console.log(res);
                    return false;
                },
                'columnDefs': [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return  row['created_at'];
                        }
                    },
                    {
                         targets: 2,
                        render: function (data, type, row) {
                            return row['track_id'];
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, row) {
                            return row['date_of_ship'];

                        }
                    },
                    {
                        targets: 4,
                        render: function (data, type, row) {
                            return row['date_arrived'];
                        }
                    },
                    {
                        targets: 5,
                        render: function (data, type, row) {
                            return row['reorder_date'];
                        }
                    },
                     {
                        targets: 6,
                        render: function (data, type, row) {
                            return row['days_of_supply'];
                        }
                    },
                     
                    {
                        targets: 7,
                        render: function (data, type, row) {
                           return row['action']
                        }
                    }
                ]
            });
        }
        $(document).ready(function () {
            var id = $('#marketplaces').val();
            
                     loadDashboards(id);
        });
        function loadDashboards(id){
           var comleted_purchase_order_list='comleted_purchase_order_list';
            var count=1;
            var table = $('#datatable').DataTable({

                "order": [ 0, 'asc' ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('purchaseorder.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.id=id,d.status=status,d.request_type=comleted_purchase_order_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                success:function($res){
                    console.log(res);
                    return false;
                },
                'columnDefs': [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return  row['created_at'];
                        }
                    },
                    {
                         targets: 2,
                        render: function (data, type, row) {
                            return row['track_id'];
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, row) {
                            return row['date_of_ship'];

                        }
                    },
                    {
                        targets: 4,
                        render: function (data, type, row) {
                            return row['date_arrived'];
                        }
                    },
                    {
                        targets: 5,
                        render: function (data, type, row) {
                            return row['reorder_date'];
                        }
                    },
                     {
                        targets: 6,
                        render: function (data, type, row) {
                            return row['days_of_supply'];
                        }
                    },
                     
                    {
                        targets: 7,
                        render: function (data, type, row) {
                           return row['action']
                        }
                    }
                ]
            });
        } 
 
            </script>
            <script>
                $(document).on('click', '.confirm_PO', function (e) {
                     e.preventDefault();
                    var id = $(this).data('id');
                    swal({
                     title: "Are you sure?",
                     text: "You want to confirm this record",
                     type: "warning",
                     showCancelButton: true,
                     confirmButtonColor: "#DD6B55",
                     confirmButtonText: "Yes, confirm it!",
                     cancelButtonText: "No, confirm please!",
                     closeOnConfirm: false,
                     closeOnCancel: false
                     },
                     function(isConfirm) {
                     if (isConfirm) {
                         $.ajax({
                         type: "POST",
                         url: "{{ route('purchaseorder.store') }}",
                         data: {id :id,request_type:'confirm_type' },
                         "dataType": 'json',
                         success: function (data) {
                         swal("Done!", "It was succesfully confirmed!", "success");
                            location.reload(true);
                         }
                         });
                     }
                     else
                     {
                        swal("Cancelled", "", "error");
                     }

                     });
                });
               

      
            </script>
@endsection
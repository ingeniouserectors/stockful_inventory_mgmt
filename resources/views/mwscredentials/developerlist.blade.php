@extends('layouts.master')

@section('title')Developer Keys List @endsection

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Developer Keys</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active"> Developer Keys</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
    @if($user_access_create_details == 1)

        <div class="row">
            <div class="col-lg-6">
                <a class="btn btn-info waves-effect waves-light mb-3"  href="{{ route('amazonsettings.create') }}" role="button"> Create/Update Developer Keys</a>
            </div>
            <div class="col-lg-6"></div>
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-4">Developer Keys List</h4>
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead class="thead-light">
                    <tr>
                        <th>No.</th>
                        <th>Marketplace</th>
                        <th>MarketplaceId</th>
                        <th>Access Key</th>
                        <th>Secret Key</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $no =1 @endphp
                    @foreach($developerAccountDetails as $developerAccount)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $developerAccount->marketplace_country }}</td>
                        <td>{{ $developerAccount->marketplace_id }}</td>
                        <td>{{ base64_decode($developerAccount->access_key) }}</td>
                        <td>{{ base64_decode($developerAccount->secret_key) }}</td>
                        <td>{{ ($developerAccount->marketplace_status==0) ? 'InActive' :'Active'  }}</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
        </div>
    </div>
@endsection
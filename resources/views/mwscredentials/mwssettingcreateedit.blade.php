@extends('layouts.master')

@section('title') Create or Edit  Credentials @endsection

@section('content')


    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Create or Edit  Credentials</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('amazonsettings.index') }}">Amazon Marketplace</a></li>
                        <li class="breadcrumb-item active">Create or Edit  Credentials</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
    @if(session()->has('message'))
        {!! session('message') !!}
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    We require your API credentials in order to access information from Amazon. <a href="#amazoninstruction" class="dashed-link" id="show_amazon_instructions" title="Amazon account integration instructions" data-toggle="modal" data-target="#amazoninstruction">Click here for instructions</a>
                    <form action="{{ url('/amazonsettings/'.$user['id']) }}" method="post" onreset="myFunction()">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="row">
                                <?php foreach ($developerAccountDetails as $index => $developerAccount) { ?>
                                <input type="hidden" name="marketplacedetails[]"value="<?php echo(isset($developerAccount['marketplace_id']) ? $developerAccount['marketplace_id'] : '') ?>">
                                <?php } ?>
                                <?php foreach ($developerAccountDetails as $index => $developerAccount) { ?>
                                <input type="hidden" name="country[<?php echo $developerAccount['marketplace_id'] ?>][]"value="<?php echo(isset($developerAccount['marketplace']) ? $developerAccount['marketplace'] : '') ?>">
                                <?php } ?>
                                <?php foreach ($developerAccountDetails as $index => $developerAccount) { ?>
                                <input type="hidden" name="mrketplaceurl[<?php echo $developerAccount['marketplace_id'] ?>][]"value="<?php echo(isset($developerAccount['marketplace_url']) ? $developerAccount['marketplace_url'] : '') ?>">
                                <?php } ?>
                                <div class="input-group mb-3 col-md-6">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-user"></i></span>
                                    </div>
                                    <?php foreach ($developerAccountDetails as $index => $developerAccount) { ?>
                                    <input type="text" class="form-control mws_sellerid" name="mws_sellerid[<?php echo $developerAccount['marketplace_id'] ?>][]"
                                           id="<?php echo $developerAccount['marketplace_id'] ?>"
                                           style="<?php echo($index != 0 ? 'display:none;' : ''); ?>"
                                           value="<?php echo(isset($user['marketpalce'][$developerAccount['marketplace']]) ? base64_decode($user['marketpalce'][$developerAccount['marketplace']]['mws_sellerid']) : '') ?>"
                                           placeholder="MWS Seller ID">
                                    <?php } ?>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <select class="form-control" id="mws_marketplaceid" name="mws_marketplaceid" required>
                                            <?php foreach ($developerAccountDetails as $developerAccount) { ?>
                                                <option value="<?php echo $developerAccount['marketplace_id'] ?>"><?php echo $developerAccount['marketplace_country'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-group mb-3 col-md-6">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-key"></i></span>
                                    </div>
                                    <?php foreach ($developerAccountDetails as $index => $developerAccount) { ?>
                                    <input type="text" class="form-control mws_authtoken" name="mws_authtoken[<?php echo $developerAccount['marketplace_id'] ?>][]"
                                           id="mws_authtoken<?php echo $developerAccount['marketplace_id'] ?>"
                                           style="<?php echo($index != 0 ? 'display:none;' : ''); ?>"
                                           value="<?php echo(isset($user['marketpalce'][$developerAccount['marketplace']]) ? base64_decode($user['marketpalce'][$developerAccount['marketplace']]['mws_authtoken']) : '') ?>"
                                           placeholder="MWS Auth Token">
                                    <?php } ?>
                                </div>
                                <div class="input-group mb-3 col-md-6">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-key"></i></span>
                                    </div>
                                    <?php foreach ($developerAccountDetails as $index => $developerAccount) { ?>
                                    <input type="text" class="form-control amazonstorename" name="amazonstorename[<?php echo $developerAccount['marketplace_id'] ?>][]"
                                           id="amazonstorename<?php echo $developerAccount['marketplace_id'] ?>"
                                           style="<?php echo($index != 0 ? 'display:none;' : ''); ?>"
                                           value="<?php echo(isset($user['marketpalce'][$developerAccount['marketplace']]) ? $user['marketpalce'][$developerAccount['marketplace']]['amazonstorename'] : '') ?>"
                                           placeholder="Amazon Store Name">
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary col-md-2 fa-pull-right">Save / Update</button>
                                <a href="{{ route('amazonsettings.index') }}" class="btn btn-primary col-md-2 fa-pull-right" style="margin-right: 15px;">Back</a>
                            </div>
                        </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->
    <div class="modal fade" id="amazoninstruction" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" >
            <div class="modal-content">
                <div class="modal-header">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>  -->
                    <h4 class="modal-title" id="myModalLabel">Amazon account integration instructions</h4>
                </div>
                <div class="modal-body">
                    <p class="view-result">
                    <div class="alert alert-primary">
                        <ol>
                            <li>Log in to your Amazon Seller Central account.</li>
                            <li>Go to <strong>Settings</strong> then <strong>User Permissions</strong>.</li>
                            <li>Scroll to the bottom of the page and click <strong>'Visit Manage Your Apps'</strong>.</li>
                            <li>On the following page, click <strong>‘Authorise new developer’</strong>.</li>
                            <li>In the Developer’s Name text box, copy and paste <strong>Stockful</strong></li>
                            <li>In the Developer Account Number box, copy and paste
                                <strong><span id="mws_selected_marketplace_id">172775770773</span> </strong>
                                and click <strong>'next'</strong>.
                            </li>
                            <li>Accept the Amazon MWS Licence Agreements and click <strong>Next</strong>.</li>
                            <li>Copy your account identifiers: Seller ID, MWS Authorisation Token and your Marketplace ID
                                (Select your base marketplace) then paste these into the relevant fields on this page.
                            </li>
                        </ol>
                    </div>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

<script>

    $(document).ready(function () {
        var marketplace_id = $('#mws_marketplaceid').val();
        get_marketplace(marketplace_id);
    });
    $(document).on('change','#mws_marketplaceid',function () {
        var marketplace_id = $(this).val();
        get_marketplace(marketplace_id);
    });

    function get_marketplace(marketplace_id){
        var type = 'marketplace_get';
        $.ajax({
            type:"POST",
            url: "{{ route('amazonsettings.store') }}",
            data:{type:type,marketplace_id:marketplace_id},
            dataType: "json",
            success: function(res){
                $('#mws_selected_marketplace_id').html(res.accountno);
            },
            error: function(){
                console.log('error');
            }
        });
    }
</script>
@endsection
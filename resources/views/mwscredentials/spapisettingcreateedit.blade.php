@extends('layouts.master')

@section('title') Create or Edit  Credentials @endsection

@section('content')


    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Create or Edit Credentials</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('amazonsettings.index') }}">Amazon Marketplace</a>
                        </li>
                        <li class="breadcrumb-item active">Create or Edit Credentials</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
    @if(session()->has('message'))
        {!! session('message') !!}
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    We require your API credentials in order to access information from Amazon. <a
                        href="#amazoninstruction" class="dashed-link" id="show_amazon_instructions"
                        title="Amazon account integration instructions" data-toggle="modal"
                        data-target="#amazoninstruction">Click here for instructions</a>
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <select class="form-control" id="mws_marketplaceid" name="mws_marketplaceid">
                                    <?php foreach ($developerAccountDetails as $index=> $developerAccount) { ?>
                                        @if($index== 0)
                                            <option selected="selected" value="<?php echo $developerAccount['marketplace_slug'] ?>"><?php echo $developerAccount['marketplace_country'] ?></option>
                                        @else
                                            <option value="<?php echo $developerAccount['marketplace_slug'] ?>"><?php echo $developerAccount['marketplace_country'] ?></option>
                                        @endif
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <?php foreach ($developerAccountDetails as $index=> $developerAccount) { ?>
                                    @if(!empty($user['sp_a_p_i_user_marketplace']))
                                    @foreach($user['sp_a_p_i_user_marketplace'] as $markatplace)
                                    @if($index== 0 && $markatplace['marketplace_id'] == $developerAccount['id'])
                                        <a href="{{$developerAccount['marketplace_url'] .'/apps/authorize/consent?application_id=' . $developerAccount['app_id'].'&state='. $developerAccount['marketplace_slug'] .'&version=beta'}}" class="{{ $developerAccount['marketplace_slug'] }} btn btn-warning disabled">Authorize Stockful</a>
                                    @elseif($index== 0 && $markatplace['marketplace_id'] != $developerAccount['id'])
                                        <a href="{{$developerAccount['marketplace_url'] .'/apps/authorize/consent?application_id=' . $developerAccount['app_id'].'&state='. $developerAccount['marketplace_slug'] .'&version=beta'}}" class="{{ $developerAccount['marketplace_slug'] }} btn btn-warning">Authorize Stockful</a>
                                    @elseif($index!= 0 && $markatplace['marketplace_id'] == $developerAccount['id'])
                                        <a href="{{$developerAccount['marketplace_url'] .'/apps/authorize/consent?application_id=' . $developerAccount['app_id'].'&state='. $developerAccount['marketplace_slug'] .'&version=beta'}}" class="{{ $developerAccount['marketplace_slug'] }} btn btn-warning d-none disabled">Authorize Stockful</a>
                                    @else
                                        <a href="{{$developerAccount['marketplace_url'] .'/apps/authorize/consent?application_id=' . $developerAccount['app_id'].'&state='. $developerAccount['marketplace_slug'] .'&version=beta'}}" class="{{ $developerAccount['marketplace_slug'] }} btn btn-warning d-none">Authorize Stockful</a>
                                    @endif
                                    @endforeach
                                    @else
                                        @if($index== 0)
                                            <a href="{{$developerAccount['marketplace_url'] .'/apps/authorize/consent?application_id=' . $developerAccount['app_id'].'&state='. $developerAccount['marketplace_slug'] .'&version=beta'}}" class="{{ $developerAccount['marketplace_slug'] }} btn btn-warning">Authorize Stockful</a>
                                        @else
                                            <a href="{{$developerAccount['marketplace_url'] .'/apps/authorize/consent?application_id=' . $developerAccount['app_id'].'&state='. $developerAccount['marketplace_slug'] .'&version=beta'}}" class="{{ $developerAccount['marketplace_slug'] }} btn btn-warning d-none">Authorize Stockful</a>
                                        @endif
                                    @endif
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->
    <div class="modal fade" id="amazoninstruction" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>  -->
                    <h4 class="modal-title" id="myModalLabel">Amazon account integration instructions</h4>
                </div>
                <div class="modal-body">
                    <p class="view-result">
                    <div class="alert alert-primary">
                        <ol>
                            <li>Log in to your Amazon Seller Central account.</li>
                            <li>Go to <strong>Settings</strong> then <strong>User Permissions</strong>.</li>
                            <li>Scroll to the bottom of the page and click <strong>'Visit Manage Your Apps'</strong>.
                            </li>
                            <li>On the following page, click <strong>‘Authorise new developer’</strong>.</li>
                            <li>In the Developer’s Name text box, copy and paste <strong>Stockful</strong></li>
                            <li>In the Developer Account Number box, copy and paste
                                <strong><span id="mws_selected_marketplace_id">172775770773</span> </strong>
                                and click <strong>'next'</strong>.
                            </li>
                            <li>Accept the Amazon MWS Licence Agreements and click <strong>Next</strong>.</li>
                            <li>Copy your account identifiers: Seller ID, MWS Authorisation Token and your Marketplace
                                ID
                                (Select your base marketplace) then paste these into the relevant fields on this page.
                            </li>
                        </ol>
                    </div>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <script>
        $(document).on('change', '#mws_marketplaceid', function () {
            var marketplace_id = $(this).val();
            $(".btn-warning").addClass('d-none');
            $("."+marketplace_id).removeClass('d-none');
        });
    </script>
@endsection

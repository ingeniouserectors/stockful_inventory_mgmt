@extends('layouts.master')

@section('title') Create or Edit  Developer Credentials @endsection

@section('customcss')
<style>
    input[switch] + label {
    width: 75px;
    }

    input[switch]:checked + label::after {
    left: 53px;
    background-color: #eff2f7;
    }
</style>
@endsection


@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Create or Edit Developer Credentials</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('amazonsettings.index') }}" >Mws Developer Keys</a></li>
                        <li class="breadcrumb-item active">Create or Edit Developer Credentials</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
    <!-- end page title -->
    @if(session()->has('message'))
        {!! session('message') !!}
    @endif
    
            <div class="card">
                <div class="card-body">
                    <form action="{{ url('/amazonsettings/'.$developerAccountDetails[0]['id']) }}" method="post" onreset="myFunction()">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <?php foreach ($developerAccountDetails as $index => $developerAccount) { ?>
                        <input type="hidden" name="marketplacedetails[]"value="<?php echo(isset($developerAccount['marketplace_id']) ? $developerAccount['marketplace_id'] : '') ?>">
                        <?php } ?>
                        <div class="form-group row">
                            <div class="input-group mb-3 col-md-6">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                                </div>
                                <?php foreach ($developerAccountDetails as $index => $developerAccount) { ?>
                                <input type="text" class="form-control dev_account" name="developer_accno[<?php echo $developerAccount['marketplace_id'] ?>][]"
                                       id="<?php echo $developerAccount['marketplace_id'] ?>"
                                       style="<?php echo($index != 0 ? 'display:none;' : ''); ?>"
                                       value="<?php echo(isset($developerAccount['developer_accno']) ? $developerAccount['developer_accno'] : '') ?>"
                                       placeholder="MWS Developer Account">
                                <?php } ?>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <select class="form-control" name="marketplace_id[]" required>
                                        <?php foreach ($developerAccountDetails as $developerAccount) { ?>
                                        <option value="<?php echo $developerAccount['marketplace_id'] ?>"><?php echo $developerAccount['marketplace_country'] ?></option>
                                        <?php } ?>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="input-group mb-3 col-md-6">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-key"></i></span>
                                </div>
                                <?php foreach ($developerAccountDetails as $index => $developerAccount) { ?>
                                <input type="text" class="form-control accesskey" name="access_key[<?php echo $developerAccount['marketplace_id'] ?>][]"
                                       id="accesskey<?php echo $developerAccount['marketplace_id'] ?>"
                                       style="<?php echo($index != 0 ? 'display:none;' : ''); ?>"
                                       value="<?php echo(isset($developerAccount['access_key']) ? base64_decode($developerAccount['access_key']) : '') ?>"
                                       placeholder="Access Key">
                                <?php } ?>
                            </div>
                            <div class="input-group mb-3 col-md-6">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-key"></i></span>
                                </div>
                                <?php foreach ($developerAccountDetails as $index => $developerAccount) { ?>
                                <input type="text" class="form-control secretkey" name="secret_key[<?php echo $developerAccount['marketplace_id'] ?>][]"
                                       id="secretkey<?php echo $developerAccount['marketplace_id'] ?>"
                                       style="<?php echo($index != 0 ? 'display:none;' : ''); ?>"
                                       value="<?php echo(isset($developerAccount['secret_key']) ? base64_decode($developerAccount['secret_key']) : '') ?>"
                                       placeholder="Secret Key">
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="input-group mb-3 col-md-12">
                                <?php foreach ($developerAccountDetails as $index => $developerAccount) { ?>
                                    <div class="square-switch marketplace_status" style="<?php echo($index != 0 ? 'display:none;' : ''); ?>" id="marketplace_status<?php echo $developerAccount['marketplace_id'] ?>">
                                        <input type="checkbox" id="square-switch<?php echo $developerAccount['marketplace_id'] ?>"  switch="info" data-id="<?php echo $developerAccount['marketplace_id'] ?>" <?php echo($developerAccount['marketplace_status'] == 1 ? "checked=''" : '') ?>
                                       name="marketplace_status[<?php echo $developerAccount['marketplace_id'] ?>][]" class="marketplace_status_checkbox"
                                       value="<?php echo $developerAccount['marketplace_status'] ?>">
                                        <label for="square-switch<?php echo $developerAccount['marketplace_id'] ?>" data-on-label="Active" data-off-label="Inactive"></label>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="button-items mt-3">
                            <input class="btn btn-info" type="submit" value="Submit" id="submit">
                            <a class="btn btn-danger waves-effect waves-light" href="{{ route('amazonsettings.index') }}" role="button">Cancel</a>
                            <input class="btn btn-warning" type="reset" value="Reset">
                        </div>

                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->

@endsection

@section('script')
<script>

    $(document).ready(function () {
        $("select").change(function () {
            $(this).find("option:selected").each(function () {
                var optionValue = $(this).attr("value");
                $(".dev_account").each(function () {
                    $(".dev_account").hide();
                    $('#' + optionValue).show();
                });
                $(".accesskey").each(function () {
                    $(".accesskey").hide();
                    $('#accesskey' + optionValue).show();
                });
                $(".secretkey").each(function () {
                    $(".secretkey").hide();
                    $('#secretkey' + optionValue).show();
                });
                $(".marketplace_status").each(function () {
                    $(".marketplace_status").hide();
                    $('#marketplace_status' + optionValue).show();
                });
            });
        }).change();
        $(".marketplace_status_checkbox").click(function () {
            var currentValue = $(this).val();
            var UpdatedValue = 1;
            if(currentValue == 1){
                UpdatedValue = 0;
            }
            $('#square-switch' + $(this).attr("data-id")).val(UpdatedValue);
        });

    });

</script>
@endsection

@extends('layouts.master')

@section('title') Create vendors @endsection

@section('content')

<style type="text/css">
    .error
    {
        color:red;
    }
</style>
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Create Vendors</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('vendors.index')}}">Vendors</a></li>
                        <li class="breadcrumb-item active">Create Vendors</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">
                <div class="card-body">

                    <form name="create-vendor" id="create-vendor" action="{{ route('vendors.store') }}" method="POST" onreset="myFunction()">
                        @csrf

                         <input type="hidden" name="insert_type" value="create_form">
                        <input type="hidden" name="marketplace_id" id="marketplace_id" value="{{session('MARKETPLACE_ID')}}">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Vendor Name</label>
                            <div class="col-md-4">
                                <input class="form-control" type="text" name="vendor_name" id="vendor_name">
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label city">Address line 1</label>
                            <div class="col-md-4 city">
                                <input class="form-control" type="text" name="address_line1" id="address_line1">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-email-input" class="col-md-2 col-form-label">Address line 2</label>
                            <div class="col-md-4">
                                <input class="form-control" type="text" name="address_line2" id="address_line2">
                            </div>
                           <label for="example-text-input" class="col-md-2 col-form-label city">Country</label>
                            <div class="col-md-4 country">
                                <select name="country" id="country" class="form-control">
                                    <option value="">Select</option>
                                    @foreach($country_list as  $key=> $country)
                                        <option value=" {{ $key }}">{{ $key."(".$country.")" }}</option>
                                    @endforeach 
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-tel-input"  class="col-md-2 col-form-label phone">States</label>
                            <div class="col-md-4 state">
                                <select name="state" id="state" class="form-control">
                                </select>
                            </div>
                             <label for="example-tel-input"  class="col-md-2 col-form-label country">City</label>
                            <div class="col-md-4 country">
                                <select name="city" id="city" class="form-control">
                                </select>
                            </div>
                         </div>   
                           <div class="form-group row"> 
                            <label for="example-text-input" class="col-md-2 col-form-label state">Zip Code</label>
                            <div class="col-md-4 state">
                                <input class="form-control allow_integer" type="text" name="zipcode" id="zipcode" maxlength="6" >
                            </div>
                        </div>
                       
                        <b> Primary Details : </b><br/>

                        <div class="form-group row">
                            <label for="example-tel-input"  class="col-md-2 col-form-label phone">Fistname</label>
                            <div class="col-md-4 phone">
                                <input  class="form-control" type="text" name="firstname[]" id="firstname" >
                            </div>

                            <label for="example-text-input" class="col-md-2 col-form-label state">Lastname</label>
                            <div class="col-md-4 state">
                                <input class="form-control" type="text" name="lastname[]" id="lastname" >
                            </div>
                        </div>

                        <div class="form-group row">
                           <label for="example-tel-input"  class="col-md-2 col-form-label phone">Email</label>
                                <div class="col-md-4 phone">
                                    <input  class="form-control" type="text" name="email[]" id="email" >
                            </div>
                        </div>

                        <div id="basic_primary_details">
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4 phone">
                            </div>
                            <div class="col-md-4 phone">
                                <a  class=" btn btn-success" type="submit" name="add_primary" id="add_primary" style="color:white;">Add New  Details + </a>
                            </div>
                            <div class="col-md-4 phone">
                            </div>
                        </div>
                        <div class="button-items mt-3">
                            <input class="btn btn-info" type="submit" value="Submit" id="submit">
                            <a class="btn btn-danger waves-effect waves-light" href="{{ route('vendors.index') }}" role="button">Cancel</a>
                            <input class="btn btn-warning" type="reset" value="Reset">
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->

@endsection

@section('script')
    <script>
            $(document).ready(function () {
                var numberIncr = 1;
                $("#add_primary").on('click', function () {
                    $('#basic_primary_details').append($('<div id="rowsdata'+numberIncr+'" class="dynamic-added"><hr><b> Other Primary Details : </b><br/>'+
                            '<div class="form-group row">'+
                            '<label for="example-tel-input"  class="col-md-2 col-form-label phone">Fistname</label>'+
                            '<div class="col-md-4 phone">'+
                            '<input  class="form-control" type="text"name="firstname[' + numberIncr + ']"data-rule-required="true" data-msg-required="Please enter firstname" >'+
                            '</div>'+

                            '<label for="example-text-input" class="col-md-2 col-form-label state">Lastname</label>'+
                            '<div class="col-md-4 state">'+
                            '<input class="form-control" type="text" name="lastname[' + numberIncr + ']" data-rule-required="true" data-msg-required="Please enter lastname">'+
                            '</div>'+
                            '</div>'+ 

                            '<div class="form-group row">'+
                            '<label for="example-tel-input"  class="col-md-2 col-form-label phone">Email</label>'+
                            '<div class="col-md-4 phone">'+
                            '<input type="email" class="comment required form-control" name="email[' + numberIncr + ']" data-rule-required="true" data-msg-required="Please enter email" />'+ 
                            '</div>'+
                            '<div class="col-md-4 phone">'+
                            ' <input class="btn btn-danger" type="submit" style="float: right;" value="Remove  Details &times;" id="remove_primary" data-id="'+numberIncr+'" name="remove_primary[' + numberIncr + ']">'+
                            '</div>'+
                            '</div>'
                            ));
                    numberIncr++;
                });

               
            });

            $(document).on('click','#remove_primary',function(){
                var remove_id = $(this).data('id');
                $('#rowsdata'+remove_id+'').remove();
            });
    </script>
<script type="text/javascript">
    
  $('#country').change(function(){
    var country = $(this).val();
    var request_type = 'Get_all_state';
    if(country){
        $.ajax({
           type:"POST",
           url:"{{ route('vendors.store') }}",
            data:{country:country,request_type:request_type},
           success:function(res){               
            if(res){
                $("#state").empty();
                $("#state").append('<option value="">Select</option>');
                    $.each(res,function(key,value){
                        $("#state").append('<option value="'+key+'">'+key+'('+value+')'+'</option>');
                    });
           
            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
        $("#city").empty();
    }      
   });
  $('#state').on('change',function(){
      var state = $(this).val();
      var request_type = 'Get_all_city';
      if(state){
          $.ajax({
              type:"POST",
              url:"{{ route('vendors.store') }}",
              data:{state:state,request_type:request_type},
              success:function(res){
                  if(res){
                      $("#city").empty();
                      $("#city").append('<option value="">Select</option>');
                          $.each(res, function (key, value) {
                              $("#city").append('<option value="' + key + '">' + key + '(' + value + ')' + '</option>');
                          });

                  }else{
                      $("#city").empty();
                  }
              }
          });
      }else{
          $("#city").empty();
      }

  });
</script>
@endsection
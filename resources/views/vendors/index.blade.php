@extends('layouts.master')

@section('title') Vendors List @endsection

@section('content')
<style type="text/css">
    .sweet
    {
        margin-left:85%;
      margin-top:-28px;

    }
</style>
    <div class="row">
        <div class="col-12">

            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Vendors</h4>
            <input type="hidden" value="{{$marketplace}}" name="marketplaces" id="marketplaces">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Vendors</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="form-group row">
        <div class="col-lg-4">
            <a class="btn btn-info waves-effect waves-light mb-3"  href="{{ route('vendors.create') }}" role="button">Create Vendors</a>
            <a class="btn btn-info waves-effect waves-light mb-3"  href="{{ route('vendors.show','import') }}" role="button"><i class="fa fa-download"></i> Download CSV File</a>
        </div>
        <div class="col-lg-8">
            <form action="{{ route('vendors.store') }}" method="post" id="import_csv"  enctype="multipart/form-data">
                {{ csrf_field() }}
                <label>Import CSV file : *</label>
                <input type="hidden" name="insert_type" value="upload_csv">
                <input type="file" name="uploadFile" accept=".csv">
                <input type="submit" class="btn btn-primary import_csv" value="Import CSV File">
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
           
            <div class="card">

            </div>
            <div class="messages"></div>
        </div>
        @if(session()->has('message2') || session()->has('message3') || session()->has('message4'))
        <div class="col-lg-12">
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                @if(session()->has('message2'))
                    {!! session('message2') !!}
                @endif
                <br>
                @if(session()->has('message3'))
                    {!! session('message3') !!}
                @endif
                <br>
                @if(session()->has('message4'))
                    {!! session('message4') !!}
                @endif
            </div>
        </div>
        @endif
    </div>
   
    <!-- end row -->

<div class="card">
        <div class="card-body">
                <h4 class="card-title mb-4">Vendors List</h4>
                 <input type="hidden" name="id"  value="" >
            <div class="table-responsive">
                <table id="datatables" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead class="thead-light">

                        <tr>
                            <th>No.</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>State</th>
                            <th>Zip Code</th>
                            <th>Country</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>        

@endsection
@section('script')


 <script>

        $(document).ready(function () {
            var id = $('#marketplaces').val();
                     loadDashboard(id);
        });
        function loadDashboard(id){
           var vendors_list='vendors_list';
            var count=1;
            var table = $('#datatables').DataTable({

                "order": [ 0, 'asc' ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('vendors.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.id=id,d.request_type=vendors_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                success:function($res){
                    console.log(res);
                    return false;
                },
                'columnDefs': [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return  row['vendor_name'];
                        }
                    },
                    {
                         targets: 2,
                        render: function (data, type, row) {
                            return row['address_line_1']+ "," + row['address_line_2'] + "," + row['city'];
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, row) {
                            return row['state'];

                        }
                    },
                    {
                        targets: 4,
                        render: function (data, type, row) {
                            return row['zipcode'];
                        }
                    },
                    {
                        targets: 5,
                        render: function (data, type, row) {
                            return row['country'];
                        }
                    },
                    {
                        targets: 6,
                        render: function (data, type, row) {
                           return row['action']
                        }
                    }
                ]
            });
        }

 $(document).on('click', '#button', function () {
    //e.preventDefault();
    var $ele = $(this).parent().parent();
    var id = $(this).data('id');
            var url = "{{URL('vendors')}}";
            var destroyurl = url+"/"+id;


    swal({
             title: "Are you sure?",
            text: "You want to delete this record",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
             
            $.ajax({
                type: "DELETE",
                url:destroyurl, 
                data:{ _token:'{{ csrf_token() }}'},
                dataType: "html",
                success: function (data) {
                    var dataResult = JSON.parse(data);
                if(dataResult.statusCode==200){
                    $ele.fadeOut().remove();
                               swal({
              title: "Done!",
              text: "It was succesfully deleted!",
              type: "success",
              timer: 700
           });
    
                      
                    } 
        }

         });
          }  
    
        else
        {
             swal("Cancelled", "", "error");
        }
            
       
    });

});
    </script>


@endsection
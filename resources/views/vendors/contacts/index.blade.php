@extends('layouts.master')

@section('title') Vendors Contact List @endsection

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">{{ucfirst($vendors->vendor_name)}}'s  Vendor Contact</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('vendors.index') }}">Vendors</a></li>
                        <li class="breadcrumb-item active">{{ucfirst($vendors->vendor_name)}} Contact</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="row">
        <div class="col-lg-5">
                <form action="{{ route('vendorscontact.create') }}">
                    <input type="hidden" name="vendor_id" value="{{$vendors_id}}" id="vendor_id">
                    <button type="submit" class="btn btn-info waves-effect waves-light mb-3">Create Vendors Contact</button>
                    <a class="btn btn-info waves-effect waves-light mb-3"  href="{{ route('vendorscontact.index') }}" role="button"><i class="fa fa-download"></i> Download Contact CSV File</a>
                </form>
        </div>
        <div class="col-lg-7">
            <form action="{{ route('vendorscontact.store') }}" method="post" id="import_csv"  enctype="multipart/form-data">
                {{ csrf_field() }}
                <label>Import CSV file : *</label>
                <input type="hidden" name="vendor_id" value="{{$vendors_id}}">
                <input type="hidden" name="insert_type" value="upload_csv">
                <input type="file" name="uploadFile" accept=".csv">
                <input type="submit" class="btn btn-primary import_csv" value="Import CSV File">
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">

            </div>
        </div>
         @if(session()->has('message2') || session()->has('message3'))
        <div class="col-lg-12">
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                @if(session()->has('message2'))
                    {!! session('message2') !!}
                @endif
                <br>
                @if(session()->has('message3'))
                    {!! session('message3') !!}
                @endif
            </div>
        </div>
        @endif
    </div>

    <!-- end row -->

    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-4">Vendors Contact List</h4>
            <div class="table-responsive">
                <table id="datatables" class="table table-striped table-bordered dt-responsive nowrap users-datatable" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead class="thead-light">
                    <tr>
                        <th>No.</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Is Primary?</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

@endsection
@section('script')

    
    <script>
    



$(document).ready(function () {
            var id = $('#vendor_id').val();
                     loadDashboard(id);

        });
        function loadDashboard(id){
           var vendors_contact_list='vendors_contact_list';
            var count=1;
            var table = $('#datatables').DataTable({

                "order": [ 0, 'asc' ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('vendorscontact.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.id=id,d.request_type=vendors_contact_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                success:function($res){
                    console.log(res);
                    return false;
                },
                'columnDefs': [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return  row['first_name'];
                        }
                    },
                    {
                         targets: 2,
                        render: function (data, type, row) {
                            return row['last_name'];
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, row) {
                            return row['email'];

                        }
                    },
                    {
                        targets: 4,
                        render: function (data, type, row) {
                            return row['primary'];
                        }
                    },
                   
                    {
                        targets: 5,
                        render: function (data, type, row) {
                           return row['action']
                        }
                    }
                ]
            });
        }
    </script>
    <script>
$(document).on('click', '#button', function () {
    //e.preventDefault();
    var $ele = $(this).parent().parent();
    var id = $(this).data('id');
            var url = "{{URL('vendorscontact')}}";
            var destroyurl = url+"/"+id;


    swal({
             title: "Are you sure?",
            text: "You want to delete this record",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
             
            $.ajax({
                type: "DELETE",
                url:destroyurl, 
                data:{ _token:'{{ csrf_token() }}'},
                dataType: "html",
                success: function (data) {
                    var dataResult = JSON.parse(data);
                if(dataResult.statusCode==200){
                    $ele.fadeOut().remove();
                               swal({
              title: "Done!",
              text: "It was succesfully deleted!",
              type: "success",
              timer: 700
           });
    
                      
                    } 
        }

         });
          }  
    
        else
        {
             swal("Cancelled", "", "error");
        }
            
       
    });

});
    </script>    

@endsection
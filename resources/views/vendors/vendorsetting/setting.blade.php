@extends('layouts.master')

@section('title') Vendors Setting @endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Vendors Setting</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Vendors Settings</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">
                <div class="card-body">
                    <form name="vendor-setting-create" id="vendor-setting-create" action="{{ route('vendorsetting.store') }}" method="POST" onreset="myFunction()">
                        @csrf

                        <input type="hidden" name="insert_type" value="setting_form">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Lead Time</label>
                            <div class="col-md-4">
                                <input class="form-control allow_integer" type="text" name="lead_time" id="lead_time" value="{{ isset($vendor_setting['lead_time']) ? $vendor_setting['lead_time'] : '' }}" maxlength="3">
                            </div>
                            <label for="example-text-input " class="col-md-2 col-form-label">Order Volume</label>
                            <div class="col-md-4">
                                <input class="form-control allow_integer" type="text" name="order_volume" id="order_volume" value="{{ isset($vendor_setting['order_volume']) ? $vendor_setting['order_volume'] : '' }}" maxlength="3">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Quantity Discount</label>
                            <div class="col-md-4">
                                <input class="form-control allow_float" type="text" name="quantity_discount" id="quantity_discount" value="{{ isset($vendor_setting['quantity_discount']) ? $vendor_setting['quantity_discount'] : '' }}" maxlength="3">
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">MOQ</label>
                            <div class="col-md-4">
                                <input class="form-control allow_integer" type="text" name="moq" id="moq" value="{{ isset($vendor_setting['moq']) ? $vendor_setting['moq'] : '' }}" maxlength="3">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Shipping</label>
                            <div class="col-md-4">
                                <select class="custom-select" name="shipping" id="shipping">
                                    <option value="">Select Shipping</option>
                                    <option value="1" @if($vendor_setting['shipping']==1) selected @endif>UPS</option>
                                    <option value="2" @if($vendor_setting['shipping']==2) selected @endif>USPS</option>
                                    <option value="3" @if($vendor_setting['shipping']==3) selected @endif>FEDEX</option>
                                    <option value="4" @if($vendor_setting['shipping']==4) selected @endif>Private Carrier</option>
                                    <option value="5" @if($vendor_setting['shipping']==5) selected @endif>Direct</option>
                                    <option value="6" @if($vendor_setting['shipping']==6) selected @endif>Pickup</option>
                                </select>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">Ship To Warehouse</label>
                            <div class="col-md-4">
                                <select class="custom-select" name="ship_to_warehouse" id="ship_to_warehouse" value="{{ isset($vendor_setting['ship_to_warehouse']) ? $vendor_setting['ship_to_warehouse'] : '' }}">
                                    <option value="">Select Warehouse</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">CBM Per Container</label>
                            <div class="col-md-4">
                                <input class="form-control" type="text" name="cbm_per_container" id="cbm_per_container" value="{{ $vendor_setting['cbm_per_container'] }}" maxlength="3">
                            </div>
                        </div>

                        <div class="button-items mt-3">
                            <input class="btn btn-info" type="submit" value="Submit" id="submit">
                            <a class="btn btn-danger waves-effect waves-light" href="{{ url('/index') }}" role="button">Cancel</a>
                            <input class="btn btn-warning" type="reset" value="Reset">
                        </div>
                    </form>

                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->

@endsection
@extends('layouts.master-final')

@section('title') Inventory @endsection

@section('customcss')
    <style>
        #table2Type>div:not(.active){opacity: 0.6;}
        #table2 thead th{
            background-color: #F5F6F7;
            color: #A6B0CF;
        }
        #table2 thead th:hover{
            color: #0c5460;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Inventory</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-5">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-5">
                            <h5 class="text-primary">Stockrate</h5>
                            <h3>83 In Total SKUs</h3>
                            <div id="stockrate-chart" style="height: 250px;"></div>
                        </div>
                        <div class="col-7">
                            <h5 class="text-primary">FBA INVENTORY AGE</h5>
                            <h3>7435 Total Items</h3>
                            <div id="fba-inventory-chart" style="height: 250px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-7">
            <div class="card">
                <div class="card-body">
                    <h5 class="text-primary">Inventory Breakdown</h5>
                    <table class="table table-striped table-borderless mb-0">
                        <thead style="background: white;">
                            <tr>
                                <th></th>
                                <th>Amazon</th>
                                <th>Inbound</th>
                                <th>Warehouse</th>
                                <th>Incoming PO</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="background: white;">Units</td>
                                <td>12342</td>
                                <td>8453</td>
                                <td>16324</td>
                                <td>23439</td>
                                <td>60558</td>
                            </tr>
                            <tr>
                                <td style="background: white;">Cost</td>
                                <td>$143,265.34</td>
                                <td>$101,342.55</td>
                                <td>$236,749.44</td>
                                <td>$378,408.78</td>
                                <td>$2,579,298.33</td>
                            </tr>
                            <tr>
                                <td style="background: white;">Revenue</td>
                                <td>$143,265.34</td>
                                <td>$101,342.55</td>
                                <td>$236,749.44</td>
                                <td>$378,408.78</td>
                                <td>$2,579,298.33</td>
                            </tr>
                            <tr>
                                <td style="background: white;">Profit</td>
                                <td>$143,265.34</td>
                                <td>$101,342.55</td>
                                <td>$236,749.44</td>
                                <td>$378,408.78</td>
                                <td>$2,579,298.33</td>
                            </tr>
                            <tr>
                                <td style="background: white;">ROI</td>
                                <td>17%</td>
                                <td>22%</td>
                                <td>19%</td>
                                <td>18.7%</td>
                                <td>19%</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="table-top-header d-xl-flex flex-row-reverse justify-content-between px-3">
            <div class="form-inline justify-content-end">
                <div class="dropdown">
                    <button type="button" class="btn btn-sm btn-light position-relative noti-icon mr-sm-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="overflow:visible;">
                        <i class="mdi mdi-view-parallel font-size-16"></i>
                        <span class="badge badge-primary badge-pill pos2">4</span>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="columns-dropdown" style="width:190px;">
                        <div class="px-3"><small>Select Columns</small></div>
                        <ul class="toggle-columns-menu" style="height: 150px; overflow-y:scroll;">
                            <li class="px-3 py-1 d-flex align-items-center checked" data-col-id="basic_info-image"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Image</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="basic_info-nickname"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Nickname</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="basic_info-sku"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>SKU</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="basic_info-fnsku"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>FNSKU</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="basic_info-title"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Title</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="basic_info-asin"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>ASIN</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="basic_info-upc"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>UPC</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="basic_info-brand"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Brand</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="basic_info-category"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Category</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="basic_info-variation"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Variation</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="basic_info-lavels"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Labels</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="sources-source_type"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Source Type</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="sources-sources"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Sources</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="sources-mpn"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>MPN</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="sources-qty_per_sku"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>QTY Per SKU</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="dimensions-item_height"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Item Height</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="dimensions-item_width"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Item Width</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="dimensions-item_length"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Item Length</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="dimensions-item_weight"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Item Weight</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="dimensions-amz_dim_weight"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>AMZ DIM Weight</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="dimensions-qty_per_box"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>QTY Per BOx</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="dimensions-box_height"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>BOX Height</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="dimensions-box_length"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>BOX Length</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="dimensions-box_width"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Box Width</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="dimensions-box_weight"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Box Weight</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="dimensions-box_cbm"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Box CBM</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="dimensions-boxes_per_cartoon"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Boxes Per Cartoon</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="dimensions-cartoon_length"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Cartoon Length</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="dimensions-cartoon_width"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Cartoon Width</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="dimensions-cartoon_height"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Cartoon Height</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="dimensions-cartoon_weight"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Cartoon Weight</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="dimensions-cartoon_cbm"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Cartoon CBM</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="dimensions-cartoons_per_pallet"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Cartoon Per Pallet</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="dimensions-pallet_length"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Pallet Length</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="dimensions-pallet_width"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Pallet Width</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="dimensions-pallet_height"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Pallet Height</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="dimensions-pallet_weight"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Pallet Weight</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="dimensions-pallet_cbm"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Pallet CBM</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="quantity_discounts-qd_type"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>QD Type</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="quantity_discounts-moq"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>MOQ</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="quantity_discounts-unit_cost"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Unit Cost</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="quantity_discounts-tiers"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Tiers</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="quantity_discounts-qty_calc"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>QTY Calc</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="quantity_discounts-qd_bundled"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>QD Bundled</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="profit_analysis-unit_cost"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Unit Cost</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="profit_analysis-shipping"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Shipping</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="profit_analysis-customs"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Customs</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="profit_analysis-other_pc"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Other PC</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="profit_analysis-total_landed"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Total Landed</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="profit_analysis-warehouse_storage"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Warehouse Storage</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="profit_analysis-inbound_shipping"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Inbound Shipping</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="profit_analysis-other_ic"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Other Ic</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="profit_analysis-total_inland"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Total Inland</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="profit_analysis-fba"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>FBA</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="profit_analysis-referral"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Referal</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="profit_analysis-storage"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Storage</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="profit_analysis-total_amz_fees"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Total AMZ Fees</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="profit_analysis-total_costs"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Total Costs</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="profit_analysis-price_point"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Price Point</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="profit_analysis-total_profit"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Total Profit</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="profit_analysis-profit_margin"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Profit Margin</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="profit_analysis-roi"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>ROI</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="order_settings-order_volumn"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Order Volumn</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="order_settings-reorder_schedule"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Reorder Schedule</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="lead_time-transit_type"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Transit Type</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="lead_time-po_to_prod"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Po To Prod</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="lead_time-production"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Production</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="lead_time-to_port"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>To Port</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="lead_time-transit"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Transit</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="lead_time-to_warehouse"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>To Warehouse</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="lead_time-to_amazon"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>To Amazon</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="lead_time-safety"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Safety</li><li class="px-3 py-1 d-flex align-items-center checked" data-col-id="lead_time-total_lead_time"><span class="checkbox ml-0 mr-2"><i class="mdi mdi-check"></i></span>Total Lead Time</li></ul>
                    </div>
                </div>
                <div class="pagination">
                        <span class="mx-1" id="pagination-prev" data-toggle="tooltip" data-placement="right" title="" data-original-title="Prev Page">
                            <i class="mdi mdi-chevron-left "></i>
                        </span>
                    <span class="mx-1">1</span>
                    <span class="mx-1 active">4</span>
                    <span class="mx-1">20</span>
                    <span class="mx-1" id="pagination-next" data-toggle="tooltip" data-placement="right" title="" data-original-title="Next Page">
                            <i class="mdi mdi-chevron-right"></i>
                        </span>
                </div>
            </div>
            <div class="form-inline">
                <i class="mdi mdi-trash-can font-size-18 text-light1 mr-3" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>

                <span class="mr-2">Displaying</span>
                <select class="custom-select custom-select-sm form-control-sm my-3 mr-1" id="displaying">
                    <option value="20">1-20</option>
                    <option value="40">1-40</option>
                    <option value="60">1-60</option>
                    <option value="80">1-80</option>.
                    <option value="100">1-100</option>
                </select>
                <div class="d-block">of &nbsp; <span id="total_record"> 100 </span></div>
                <div id="table2Type" class="d-flex h-100 m-0 align-items-center ml-4">
                    <div class="mr-3 active"> <span style="color: #9E55E6;">⬤</span> Overstocked</div>
                    <div class="mr-3"><span style="color: #77D617;">⬤</span> Instock</div>
                    <div class="mr-3"><span style="color: #FFA406;">⬤</span> Reorder Soon</div>
                    <div class="mr-3"><span style="color: #FEEE16;">⬤</span> Order Now</div>
                    <div class=""><span style="color: #F46A6A;">⬤</span> Late Order</div>
                </div>
            </div>
        </div>
        <table class="table table-centered table-bordered" id="table2">
            <thead>
                <tr>
                    <th></th>
                    <th>Image</th>
                    <th>Title</th>
                    <th>ASIN</th>
                    <th>SKU</th>
                    <th>Fulfillable</th>
                    <th>Reserved</th>
                    <th>Receiving</th>
                    <th>Inbound</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><span style="color: #77D617;">⬤</span></td>
                    <td><img src="https://images-na.ssl-images-amazon.com/images/I/71lZzbBj8-L._SL1500_.jpg" class="w-50px" alt=""></td>
                    <td>3 PC Desk Organizer - Galvanized Metal</td>
                    <td>B07MMKYSRG</td>
                    <td>X001YINHRB</td>
                    <td>754</td>
                    <td>34</td>
                    <td>800</td>
                    <td>324 (2)</td>
                </tr>
            </tbody>
        </table>

        <div class="p-4" style="background-color: #F6F6F6;">
            <div class="card">
                <div class="card-body">
                    <table class="table table-striped table-borderless mb-0">
                        <thead style="background: white;">
                        <tr>
                            <th></th>
                            <th>Amazon</th>
                            <th>Inbound</th>
                            <th>Warehouse</th>
                            <th>Incoming PO</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td style="background: white;">Units</td>
                            <td>12342</td>
                            <td>8453</td>
                            <td>16324</td>
                            <td>23439</td>
                            <td>60558</td>
                        </tr>
                        <tr>
                            <td style="background: white;">Cost</td>
                            <td>$143,265.34</td>
                            <td>$101,342.55</td>
                            <td>$236,749.44</td>
                            <td>$378,408.78</td>
                            <td>$2,579,298.33</td>
                        </tr>
                        <tr>
                            <td style="background: white;">Revenue</td>
                            <td>$143,265.34</td>
                            <td>$101,342.55</td>
                            <td>$236,749.44</td>
                            <td>$378,408.78</td>
                            <td>$2,579,298.33</td>
                        </tr>
                        <tr>
                            <td style="background: white;">Profit</td>
                            <td>$143,265.34</td>
                            <td>$101,342.55</td>
                            <td>$236,749.44</td>
                            <td>$378,408.78</td>
                            <td>$2,579,298.33</td>
                        </tr>
                        <tr>
                            <td style="background: white;">ROI</td>
                            <td>17%</td>
                            <td>22%</td>
                            <td>19%</td>
                            <td>18.7%</td>
                            <td>19%</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>
@endsection

@section('script')
    <!-- echarts -->
    <script src="{{ URL::asset('assets/libs/echarts/echarts.min.js')}}"></script>

    <script>
        let stockrateChartDom = document.getElementById('stockrate-chart');
        let stockrateChart = echarts.init(stockrateChartDom);
        let option;

        option = {
            tooltip: {
                trigger: 'item'
            },
            color: ['#65B515', '#F1B44C'],
            legend: {
                bottom: '5%',
                left: '0',
                orient: 'vertical',
            },
            series: [
                {
                    name: '',
                    type: 'pie',
                    radius: ['50px', '70px'],
                    avoidLabelOverlap: false,
                    label: {
                        show: false,
                        position: 'center'
                    },
                    emphasis: {
                        label: {
                            show: true,
                            fontSize: '16',
                        }
                    },
                    labelLine: {
                        show: false
                    },
                    data: [
                        {value: 77, name: 'INSTOCK'},
                        {value: 5, name: 'OUTOFSTOCK'}
                    ]
                }
            ]
        };

        option && stockrateChart.setOption(option);

        var fbaInventoryChartDom = document.getElementById("fba-inventory-chart");
        var fbaInventoryChart = echarts.init(fbaInventoryChartDom);

        option = {
            tooltip: {
                trigger: 'item',
            },
            legend: {
                orient: 'vertical',
                bottom: '5%',
                left: 'left'
            },
            color: ['#227CFF', '#074FB8', '#002253', '#F46A6A'],
            series: [
                {
                    name:'Total sales',
                    type:'pie',
                    radius: ['50px', '70px'],
                    avoidLabelOverlap: false,
                    label: {
                        normal: {
                            show: false,
                            position: 'center'
                        },
                        emphasis: {
                            show: true,
                            textStyle: {
                                fontSize: '30',
                                fontWeight: 'bold'
                            }
                        }
                    },
                    labelLine: {
                        normal: {
                            show: false
                        }
                    },
                    data:[
                        {value:7435, name:'0-90'},
                        {value:300, name:'91-180'},
                        {value:100, name:'181-365'},
                        {value:400, name:'365 +'},
                    ]
                }
            ]
        };
        ;
        if (option && typeof option === "object") {
            fbaInventoryChart.setOption(option, true);
        }

    </script>
@endsection

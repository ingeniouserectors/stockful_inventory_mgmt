@extends('layouts.master-final')
@section('title') Products @endsection
@section('customcss')
    <style>
        body { user-select: none; }

        i.pinned{ color: blue; }

        #tablist li span{ display:none; }
        #tablist li.active span{ display:block; }

        table{
            border-collapse: separate;
            border-spacing: 0;
            margin-bottom: 0 !important;
            white-space: nowrap;
            table-layout: fixed;
            width: auto !important;
        }
        table th:not(:first-child){
            /* padding-right: 2rem!important; */
            position: relative;
        }
        table td{
            background: white;
            height: 80px;
            max-width: 300px;
        }
        table .cellShadow{
            height: calc(100% + 2px);
            width: 20px;
            background-image: linear-gradient(to right, rgba(242, 242, 242, 1), rgba(242, 242, 242,0));
            position: absolute ;
            left: 100% ;
            top: -1px ;
        }
        tbody tr{ color: #3D3D3D70; }
        tbody tr.checked, tbody tr:hover{ color: #495057; }
        tbody tr:hover td{ background: #F8F8FB!important; }
        th span.locking{
            /* position: absolute;
            right: 5px;
            display: inline-block;
            width: 1.5rem; */
            opacity: 0.4;
            cursor: pointer;
        }
        th span.locking{ opacity: 0; }
        th:hover span.locking{ opacity: 0.8; }
        span.locking::after{
            content: '\f023';
            font-family: 'Font Awesome 5 Free';
        }

        .sticky-table-container{
            max-width: 100%;
            overflow: hidden;
            padding: 0 !important;
            transition: width 2s;
        }
        .sticky-table-container table {
            margin-bottom: 0;
            width: 100%;
            max-width: 100%;
            border-spacing: 0;
            padding: 0 !important;
            border-collapse: separate;
        }
        .sticky-table-container table thead tr th{
            /* border: 0; */
            /* position: relative; */
            position: -webkit-sticky;
            position: sticky;
            /* outline: 1px solid #ddd; */
            z-index: 5;
            top: -1px;
            transition: bottom 1s;
        }
        .sticky-table-container table td.locked{
            background-color: #fff;
            /* outline: 1px solid #ddd; */
            /* position: relative; */
            position: -webkit-sticky;
            position: sticky;
            z-index: 10;
            left: -1px;
        }
        .sticky-table-container table thead tr th.locked{
            position: -webkit-sticky;
            position: sticky;
            z-index: 15;
            left: -1px;
        }
        .sticky-table-container table tr th.locked:last-child, .sticky-table-container table tr td.locked:last-child{
            overflow: visible;
        }

        .grip{
            height: 100%;
            width: 2px;
            background: transparent;
            position: absolute ;
            left: calc(100% - 2px) ;
            top: -1px ;
            cursor: col-resize;
        }
        .grip:hover, .grip.dragging{
            background: red;
        }

        /* For Table Row Detail Panel */
        .nav-link{ color: #A6B0CF!important; }
        .nav-link.active{
            background-color: transparent !important;
            border: none !important;
            color: #495057!important;
        }
        .nav-link.active i{ color: #227CFF; }
        .nav-tabs{ border: none !important; }

        /* ScrollBar Customize */
        #scrollbar{
            background-color: #dadada;
            position:fixed;
            height: 10px;
            bottom: 0;
            right: 0;
            z-index: 20;
        }
        #track{
            left: 0;
            position: absolute;
            top: 0;
            height: 100%;
            width: 100%;
        }

        #thumb{
            left: 0;
            position: absolute;
            height: 100%;
            border: 1px solid #dadada;
            background-color: #888;
            border-radius: 9999px;
        }
        #thumb:hover{
            background-color: #555;
            border-color: #555;
        }
        #thumb.grabbing{
            background-color: #787879;
            border-color: #787879;
        }
        ::-webkit-scrollbar {
            width: 10px;
            height: 10px;
        }
        ::-webkit-scrollbar-track {
            background: #dadada;
        }
        ::-webkit-scrollbar-thumb {
            background: #888;
            border-radius: 5px;
            border: 1px solid #dadada;
        }
        ::-webkit-scrollbar-thumb:hover {
            background: #555;
            border-color: #555;
        }

        .hover-text-blue:hover{
            color: #227CFF;
        }

        th.basic_info, #basic_info_tab.active{ background-color: #F3F8FF;}th.basic_info:hover{background-color: #e3efff;}#basic_info_tab.active i, #basic_info_tab i:hover{ color: #227cff; cursor: pointer;}th.sources, #sources_tab.active{ background-color: #FFF7F7;}th.sources:hover{background-color: #f1cccc;}#sources_tab.active i, #sources_tab i:hover{ color: #b26e70; cursor: pointer;}th.dimensions, #dimensions_tab.active{ background-color: #F5EEFC;}th.dimensions:hover{background-color: #ede1fa;}#dimensions_tab.active i, #dimensions_tab i:hover{ color: #8162f7; cursor: pointer;}th.quantity_discounts, #quantity_discounts_tab.active{ background-color: #FAF6F1;}th.quantity_discounts:hover{background-color: #f7e6d4;}#quantity_discounts_tab.active i, #quantity_discounts_tab i:hover{ color: #ff9122; cursor: pointer;}th.profit_analysis, #profit_analysis_tab.active{ background-color: #F7FBF3;}th.profit_analysis:hover{background-color: #e7fdd3;}#profit_analysis_tab.active i, #profit_analysis_tab i:hover{ color: #65b515; cursor: pointer;}th.order_settings, #order_settings_tab.active{ background-color: #F7FBF3;}th.order_settings:hover{background-color: #e7fdd3;}#order_settings_tab.active i, #order_settings_tab i:hover{ color: #65b515; cursor: pointer;}th.lead_time, #lead_time_tab.active{ background-color: #FFFBF6;}th.lead_time:hover{background-color: #fce7cd;}#lead_time_tab.active i, #lead_time_tab i:hover{ color: #f1b44c; cursor: pointer;}

        .filterList{
            position: relative;
        }
        .filterList .btn-rmv{
            display: none;
        }
        .filterList:hover .btn-rmv{
            display: block;
            position: absolute;
            background: #F46A6A;
            color: white;
            right: -0.2rem;
            top: -0.2rem;
            font-size: 1rem;
            line-height: 1rem;
            width: 0.8rem;
            height: 0.8rem;
            border-radius: 50%;
        }

        .tab-basic_info.active i, .tab-basic_info:hover{ color: #227cff;}
        .tab-sources.active i, .tab-sources:hover{ color: #b26e70;}
        .tab-dimensions.active i, .tab-dimensions:hover{ color: #8162f7;}
        .tab-quantity_discounts.active i, .tab-quantity_discounts:hover{ color: #ff9122;}
        .tab-profit_analysis.active i, .tab-profit_analysis:hover{ color: #65b515;}
        .tab-order_setting.active i, .tab-order_setting:hover{ color: #65b515;}
        .tab-lead_time.active i, .tab-lead_time:hover{ color: #f1b44c;}

        .active span{ color: black;}

        .cursor-pointer{cursor: pointer;}

        .btn-days{
            border: none;
            padding: 4px 0;
            border-radius: 3px;
            background: #ecf2f9;
            margin-right: 8px;
            margin-bottom: 8px;
            text-align: center;
        }
        .btn-days:hover{ background: #a7cafd; }
        :checked + label.btn-days{
            background: #227cff;
            color: white;
        }
        .btn-days-weekly{ width: 40px; }
        .btn-days-monthly{ width: 30px; }

        .text-gray{
            color: #A6B0CF;
        }

    </style>
@endsection

@section('content')
    <div class="container-fluid" id="content" :key="renderKey">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0 font-size-18">Products</h4>

            <div class="page-title-right">
                <button type="button" class="btn btn-light1 waves-effect mr-3">
                    <i class="bx bx-download align-middle" > </i>
                </button>
                <button type="button" class="btn btn-outline-light1 waves-effect mr-3">
                    <i class="bx bx-upload align-middle mr-2"></i>Import Products
                </button>
            </div>
        </div>

        <div class="card">
            <div class="p-3" style="display: inline-flex;">
                <div class="dropdown">
                    <button
                        type="button"
                        class="btn btn-sm btn-light position-relative noti-icon mr-sm-3"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="overflow:visible;"
                    >
                        <i class="mdi mdi-filter font-size-16"></i>
                        <span class="badge badge-primary badge-pill pos2">4</span>
                    </button>
                    <div
                        class="dropdown-menu p-3 mt-1"
                        style="width: 480px;"
                    >
                        <form @submit.prevent>
                            <h5>
                                <i class="mdi mdi-filter font-size-16"></i>
                                Filter
                            </h5>
                            <p>
                                You can pin filters on the page by clicking on <i class="mdi mdi-pin mdi-rotate-45"></i> this button.
                            </p>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="country" class="mb-2">Country</label>
                                        <div class="d-inline-flex">
                                            <select v-model="filter_country" class="form-select w-160px">
                                                <option value="">Select Country</option>
                                                @foreach($countries as  $key=> $country)
                                                    <option value="{{ $key }}">{{ $country." (".$key.")" }}</option>
                                                @endforeach
                                            </select>
                                            <button class="btn" @click="filter_country_pinned = !filter_country_pinned">
                                                <i class="mdi mdi-pin mdi-rotate-45"></i>↓
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="" class="mb-2"># of SKUs</label>
                                        <div class="d-inline-flex align-items-center">
                                            <input type="text" v-model="filter_min_sku" class="form-control w-75px">
                                            <span class="px-2"> - </span>
                                            <input type="text" v-model="filter_max_sku" class="form-control w-75px">
                                            <button class="btn" @click="filter_sku_pinned = !filter_sku_pinned">
                                                <i class="mdi mdi-pin mdi-rotate-45"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div>
                            <button class="btn btn-secondary mr-2">Cancel</button>
                            <button class="btn btn-primary" @click="filterData"> Apply </button>
                        </div>
                    </div>
                </div>

                <template v-if="filterLabels.length || filterBrands.length">
                    <button
                        type="button"
                        class="btn btn-sm btn-light mr-sm-3"
                        @click="showFilterRow = !showFilterRow"
                    >
                        <template v-if="showFilterRow"> ↑ </template>
                        <template v-else> ↓ </template>
                    </button>
                </template>

                <div class="input-group mr-sm-3 w-250px">
                    <input type="text" class="form-control" v-model="search" placeholder="Search" @input="getData">
                    <span class="input-group-append">
                        <button class="btn btn-secondary">
                            <i class="bx bx-search-alt-2 align-middle"></i>
                        </button>
                    </span>
                </div>
                <div class="dropdown mr-3">
                    <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Action
                        <i class="mdi mdi-chevron-down"></i>
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#bulkLabelModal">Apply Bulk Label</a>
                        <a class="dropdown-item" href="#">Another Action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </div>
                <select class="form-select w-95px font-size-11 mr-sm-3" v-model="filterLabel" @change="changeFilterLabel">
                    <option value="">Labels</option>
                    <option v-for="label in labels" :value="label.id">@{{ label.label_name }}</option>
                </select>
                <select class="form-select w-95px font-size-11 mr-sm-3" v-model="filterBrand" @change="changeFilterBrand">
                    <option value="">Brands</option>
                    <option v-for="brand in brands" :value="brand">@{{ brand }}</option>
                </select>
                <select class="form-select w-95px font-size-11 mr-sm-3">
                    <option>Category</option>
                </select>
                <select
                    v-if="filter_country_pinned"
                    v-model="filter_country"
                    class="form-select w-160px mr-4"
                    @change="filterData"
                >
                    <option value="">Select Country</option>
                    @foreach($countries as  $key=> $country)
                        <option value="{{ $key }}">{{ $country." (".$key.")" }}</option>
                    @endforeach
                </select>
                <div v-if="filter_sku_pinned">
                    <div class="d-inline-flex align-items-center">
                        <input type="text" v-model="filter_min_sku" class="form-control w-75px" @change="filterData">
                        <span class="px-2"> - </span>
                        <input type="text" v-model="filter_max_sku" class="form-control w-75px" @change="filterData">
                    </div>
                </div>
            </div>
            <template v-if="( filterLabels.length || filterBrands.length ) && showFilterRow">
                <div class="d-flex align-items-center px-3 py-1">
                    <span class="h6 mb-0">Filters</span>
                    <template v-if="filterLabels.length">
                        <button type="button" class="btn btn-light btn-rounded mx-2 py-1 filterList">
                            Label:
                            <span v-for="label in filterLabels" class="h6">@{{ label.label_name }}, </span>
                            <span class="btn-rmv" @click="removeFilterLabels">×</span>
                        </button>
                    </template>
                    <template v-if="filterBrands.length">
                        <button type="button" class="btn btn-light btn-rounded mx-2 py-1 filterList">
                            Brand:
                            <span v-for="brand in filterBrands" class="h6">@{{ brand }}, </span>
                            <span class="btn-rmv" @click="removeFilterBrands">×</span>
                        </button>
                    </template>
                </div>
            </template>
            <div class="table-top-header px-3 d-xl-flex flex-row-reverse justify-content-between" style="position:sticky; top: 70px; z-index: 15;">
                <div class="d-flex align-items-center justify-content-end">
                    <div class="custom-control custom-switch mr-3">
                        <input type="checkbox" class="custom-control-input" id="hidden-checkbox">
                        <label class="custom-control-label " for="hidden-checkbox">Hidden</label>
                    </div>
                    <div class="custom-control custom-switch mr-5">
                        <input type="checkbox" class="custom-control-input" id="incomplete-checkbox">
                        <label class="custom-control-label " for="incomplete-checkbox">Incomplete</label>
                    </div>
                    <div class="dropdown">
                        <button type="button" class="btn btn-sm btn-light position-relative noti-icon mr-sm-3"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="overflow:visible; background-color: #E9EDED;"
                                data-toggle-second="tooltip" data-placement="left" data-original-title="Manage Table Columns"
                        >
                            <i class="mdi mdi-view-parallel font-size-16"></i>
                            <template v-if="fields.length-cols.length">
                                <span class="badge badge-primary badge-pill pos2">@{{ fields.length-cols.length }}</span>
                            </template>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="columns-dropdown" style="width:200px; z-index: 1005;">
                            <form>
                                <p class="px-2"><small>Manage Table Columns</small></p>
                                <div style="height: 60vh; overflow: auto;">
                                    <div class="p-2 hover-bg-blue-50" v-for="field in fields" >
                                        <label class="stockful-checkbox">@{{ field.th }}
                                            <input type="checkbox" v-model="cols" :value="field.td"  @change="updateCols">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="d-flex">
                        <button
                            class="border-0 rounded shadow pagination-btn p-0 mx-1"
                            @click.prevent="prevPage"
                            data-toggle="tooltip" data-placement="top" title="" data-original-title="Prev Page"
                        >
                            <i class="mdi mdi-chevron-left"></i>
                        </button>
                        <template v-if="currentPage!=1">
                            <button class="border-0 bg-transparent pagination-btn p-0 mx-1">
                                1
                            </button>
                        </template>
                        <button class="border-0 rounded shadow pagination-btn p-0 mx-1 btn-primary">
                            @{{ currentPage }}
                        </button>
                        <template v-if="currentPage < lastPage">
                            <button class="border-0 bg-transparent pagination-btn p-0 mx-1">
                                @{{ lastPage }}
                            </button>
                        </template>
                        <button
                            class="border-0 rounded shadow pagination-btn p-0 mx-1"
                            @click.prevent="nextPage"
                            data-toggle="tooltip" data-placement="top" title="" data-original-title="Next Page">
                            <i class="mdi mdi-chevron-right"></i>
                        </button>
                    </div>
                </div>
                <div class="form-inline">
                    <button
                        class="text-light1 mr-4 border-0 bg-transparent p-0"
                        @click="bulkDelete()"
                        data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                        <i class="mdi mdi-trash-can font-size-18"></i>
                    </button>

                    <label for="displaying" class="mr-2">Displaying</label>
                    <select class="custom-select custom-select-sm form-control-sm my-2 mr-1" id="displaying">
                        <option value="20">1-20</option>
                        <option value="40">1-40</option>
                        <option value="60">1-60</option>
                        <option value="80">1-80</option>.
                        <option value="100">1-100</option>
                    </select>
                    <div class="d-block">&nbsp; of &nbsp; @{{ processedRows.length }}</div>

                    <ul id="tablist" class="d-flex h-100 m-0">
                        <li class="h-100 px-2 d-flex align-items-center active" id="basic_info_tab">
                            <i class="mdi mdi-information-outline mdi-18px text-light1 mr-1"
                                data-toggle="tooltip" data-original-title="Basic Info"
                            ></i><span>Basic Info</span>
                        </li>
                        <li class="h-100 px-2 d-flex align-items-center" id="sources_tab">
                            <i class="mdi mdi-account-outline mdi-18px text-light1 mr-1"
                                data-toggle="tooltip" data-original-title="Sources"
                            ></i><span>Sources</span>
                        </li>
                        <li class="h-100 px-2 d-flex align-items-center" id="dimensions_tab">
                            <i class="mdi mdi-square-edit-outline mdi-18px text-light1 mr-1"
                                data-toggle="tooltip" data-original-title="Dimensions"
                            ></i><span>Dimensions</span>
                        </li>
                        <li class="h-100 px-2 d-flex align-items-center" id="quantity_discounts_tab">
                            <i class="mdi mdi-brightness-percent mdi-18px text-light1 mr-1"
                                data-toggle="tooltip" data-original-title="Quantity Discounts"
                            ></i>
                            <span>Quantity Discounts</span>
                        </li>
                        <li class="h-100 px-2 d-flex align-items-center" id="profit_analysis_tab">
                            <i class="mdi mdi-chart-line mdi-18px text-light1 mr-1"
                                data-toggle="tooltip" data-original-title="Profit Analysis"
                            ></i>
                            <span>Profit Analysis</span>
                        </li>
                        <li class="h-100 px-2 d-flex align-items-center" id="order_settings_tab">
                            <i class="mdi mdi-bag-personal-outline mdi-18px text-light1 mr-1"
                                data-toggle="tooltip" data-original-title="Order Setting"></i>
                            <span>Order Setting</span>
                        </li>
                        <li class="h-100 px-2 d-flex align-items-center" id="lead_time_tab">
                            <i class="mdi mdi-clock-outline mdi-18px text-light1 mr-1"
                                data-toggle="tooltip" data-original-title="Lead Time"
                            ></i>
                            <span>Lead Time</span>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="sticky-table-container mb-4" style="overflow: hidden;">
                <table class="table table-bordered table-centered position-relative border-0">
                    <thead>
                        <th class="locked basic_info " style="background: #F3F8FF; border-color: transparent;" >
                            <label class="stockful-checkbox">
                                <input type="checkbox" @change="updateSelectPage" v-model="selectPage" >
                                <span></span>
                            </label>
                            <div v-if="!lastLocked" class="cellShadow"></div>
                        </th>
                        <th v-for="field in columns()"
                            @click="sortBy(field.group, field.td)"
                            :class="field.group" style="border-color: transparent;"
                        >
                            @{{ field.th }}
                            <span>
                                <span :class="(field.td ==sortField && sortDirection=='asc') ? 'text-blue-500 font-weight-bold' : 'text-gray-200'">↑</span>
                                <span :class="(field.td ==sortField && sortDirection=='desc') ? 'text-blue-500 font-weight-bold' : 'text-gray-200'">↓</span>
                            </span>
                            <span v-if="field.group=='basic_info'" class="locking mx-1"></span>
                            <div class="grip"></div>
                        </th>
                    </thead>
                    <tbody>
                        <tr>
                            <template v-if="selectPage">
                                <td :colspan="cols.length + 3"  style="background-color: #F2F7FF;">
                                    <template v-if="selectAll">
                                        Selected all <strong> @{{ processedRows.length }} </strong> products.
                                    </template>
                                    <template v-else>
                                        Selected @{{ selected.length }} out of @{{ processedRows.length }}.
                                        <button class="btn btn-link" @click="selectAllItems">
                                            Select All @{{ processedRows.length }}
                                        </button>
                                    </template>
                                </td>
                            </template>
                        </tr>
                        <template v-for="row in processedRows.slice(perPage * (currentPage - 1), perPage * currentPage)">
                            <tr>
                                <td class="locked">
                                    <label class="stockful-checkbox">
                                        <input type="checkbox" v-model="selected" :value="row.id" @click="updatedSelected">
                                        <span></span>
                                    </label>
                                    <div v-if="!lastLocked" class="cellShadow"></div>
                                </td>
                                <td v-for="field in columns()"
                                    :class="field.group + (field.td == 'title' ? ' w-min-300px' : '')" class="whitespace-normal"
                                    @click="clickTableCell(row,field.group, field.td, row[field.group][field.td])"
                                >
                                    <template v-if="row.id==inputingRowId && field.td==inputingTd">
                                        <input type="text" class="border-0 bg-transparent w-100 outline-0 overflow-auto"
                                               style="margin: -10px;"
                                               v-model="inputingVal"
                                               ref="inputing"
                                               @blur="changeInputing"
                                               @keyup.enter="changeInputing"
                                        >
                                    </template>
                                    <template v-else-if="field.td == 'image'">
                                        <img :src="row[field.group][field.td]" style="max-width:55px; max-height:55px; margin:auto" alt="">
                                    </template>
                                    <template v-else>
                                        @{{ row[field.group][field.td] }}
                                    </template>
                                </td>
                            </tr>
                            <template v-if="openedRow == row.id">
                                <tr>
                                    <td class="text-dark" :colspan="columns().length + 1"  style="background: #F8F8FB;">
                                        <div class="position-sticky p-4" style="left: 0;"
                                            :style="'width: calc( 100vw - 100px - ' + $('.vertical-menu').width() + 'px );'"
                                        >
                                            <div class="d-flex text-light1 h5">
                                                <div  class="mr-3 cursor-pointer tab-basic_info"
                                                        :class = "{ 'active': ( subTab == 'basic_info' )}"
                                                        @click="subTab='basic_info'"
                                                >
                                                    <i class="mdi mdi-information-outline"></i>
                                                    <span>Basic Info</span>
                                                </div>
                                                <div class="mr-3 cursor-pointer tab-sources" :class="{ 'active': ( subTab == 'sources' )}"
                                                        @click="subTab='sources'"
                                                >
                                                    <i class="mdi mdi-account-outline"></i>
                                                    <span>Sources</span>
                                                </div>
                                                <div class="mr-3 cursor-pointer tab-dimensions" :class="{ 'active': ( subTab == 'dimensions' )}"
                                                        @click="subTab='dimensions'"
                                                >
                                                    <i class="mdi mdi-square-edit-outline"></i>
                                                    <span>Dimensions</span>
                                                </div>
                                                <div class="mr-3 cursor-pointer tab-quantity_discounts" :class="{ 'active': ( subTab == 'quantity_discounts' )}"
                                                        @click="subTab='quantity_discounts'"
                                                >
                                                    <i class="mdi mdi-brightness-percent"></i>
                                                    <span>Quantity Discounts</span>
                                                </div>
                                                <div class="mr-3 cursor-pointer tab-profit_analysis" :class="{ 'active': ( subTab == 'profit_analysis' )}"
                                                        @click="subTab='profit_analysis'"
                                                >
                                                    <i class="mdi mdi-chart-line"></i>
                                                    <span>Profit Analysis</span>
                                                </div>
                                                <div class="mr-3 cursor-pointer tab-order_setting" :class="{ 'active': ( subTab == 'order_setting' )}"
                                                        @click="subTab='order_setting'"
                                                >
                                                    <i class="mdi mdi-bag-personal-outline"></i>
                                                    <span>Order Setting</span>
                                                </div>
                                                <div class="mr-3 cursor-pointer tab-lead_time" :class="{ 'active': ( subTab == 'lead_time' )}"
                                                        @click="subTab='lead_time'"
                                                >
                                                    <i class="mdi mdi-clock-outline"></i>
                                                    <span>Lead Time</span>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-body">
                                                    <div v-if="subTab=='basic_info'">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <img :src="row.basic_info.image" width="70%" alt="">
                                                            </div>
                                                            <div class="col-md-5 border-right">
                                                                <div class="d-flex mb-3">
                                                                    <div class="text-primary mr-4">@{{ row.basic_info.brand }}</div>
                                                                    <div class="text-primary">@{{ row.basic_info.category }}</div>
                                                                </div>
                                                                <p class="text-dark mb-3" style="white-space: normal;">
                                                                    @{{ row.basic_info.title }}
                                                                </p>
                                                                <div class="d-flex mb-4">
                                                                    <p class="mr-2">Color</p>
                                                                    <p class="text-dark mr-4">Deep</p>
                                                                    <p class="mr-2">Size</p>
                                                                    <p class="text-dark">2X</p>
                                                                </div>
                                                                <div class="d-flex">
                                                                    <div class="mr-4">
                                                                        <p class="text-primary mb-2">SKU</p>
                                                                        <p class="text-dark mb-3">@{{ row.basic_info.sku }}</p>
                                                                        <p class="text-primary mb-2">FNSKU</p>
                                                                        <p class="text-dark ">@{{ row.basic_info.fnsku }}</p>
                                                                    </div>
                                                                    <div class="mr-4">
                                                                        <p class="text-primary mb-2">ASIN</p>
                                                                        <p class="text-dark mb-3">@{{ row.basic_info.asin }}</p>
                                                                        <p class="text-primary mb-2">UPC</p>
                                                                        <p class="text-dark ">@{{ row.basic_info.upc }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <p class="h5 text-secondary mt-3">Nickname</p>
                                                                <input type="text" class="bg-light rounded border-0 p-2 w-100" v-model="detail_basic_info_nickname">
                                                                <p class="h5 text-secondary mt-4">Labels</p>
                                                                <div class="whitespace-normal">
                                                                    <div
                                                                        v-for="label in detail_basic_info_labels"
                                                                        class="label-autocomplete mr-3 mb-2 d-inline-block filterList"
                                                                    >
                                                                        <input type="text"
                                                                            class="bg-light border-0 rounded-pill py-1 text-center"
                                                                            :value="label" style="display: inline-block;"
                                                                            @input="inputingLabel($event)"
                                                                            :style="'width:' + (( label.length + 4 ) * 8) + 'px'"
                                                                        >
                                                                        <span class="btn-rmv" @click="removeLabel(row, label)">×</span>
                                                                    </div>
                                                                    <div v-if="isAddingLabel"
                                                                        class="label-autocomplete mr-3 mb-2 d-inline-block filterList"
                                                                    >
                                                                        <input type="text"
                                                                            class="bg-light border-0 rounded-pill py-1 text-center"
                                                                            v-model="addingLabel"
                                                                            @input="inputingLabel($event)"
                                                                            @change="addedLabel"
                                                                            :style="'width:' + (( addingLabel.length + 4 ) * 8) + 'px'"
                                                                        >
                                                                        <span class="btn-rmv" @click="isAddingLabel=false; addingLabel=''">×</span>
                                                                    </div>
                                                                </div>
                                                                <button class="mt-4 text-primary border-0 bg-transparent" @click="isAddingLabel=true">
                                                                    <i class="bx bx-plus-circle align-middle"></i>
                                                                    Add Label
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <button class="btn btn-outline-secondary mr-3">Cancel</button>
                                                            <button class="btn btn-primary w-md" @click="saveBasicInfo(row)">Save</button>
                                                        </div>
                                                    </div>
                                                    <div v-if="subTab=='sources'">
                                                        <div class="row">
                                                            <div class="col-md-4 border-right" style="height: 250px">
                                                                <div class="form-inline mb-4">
                                                                    <label class="mr-2">Supplier</label>
                                                                    <div class="custom-control custom-switch mr-2">
                                                                        <input type="checkbox" class="custom-control-input" id="source_type" v-model="product_tmp.source_type">
                                                                        <label class="custom-control-label" for="source_type"></label>
                                                                    </div>
                                                                    <label class="mr-5">Vendor</label>
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" class="custom-control-input"
                                                                               id="1_sources_multi_sources_checkbox"
                                                                               v-model="detail_sources_multi_sources"
                                                                               @change="changeMultiSourcesCheckbox"
                                                                        >
                                                                        <label class="custom-control-label text-dark" for="1_sources_multi_sources_checkbox">Multi Sources</label>
                                                                    </div>
                                                                </div>
                                                                <div>
                                                                    <template v-if="product_tmp.source_type">
                                                                        <select name="" class="form-select border-0 bg-light w-100">
                                                                            <option value="">Select Vendor</option>
                                                                            <template v-for="vendor in vendors">
                                                                                <option :value="vendor.id">@{{ vendor.vendor_name }}</option>
                                                                            </template>
                                                                        </select>
                                                                    </template>
                                                                    <template v-else>
                                                                        <select name="" class="form-select border-0 bg-light w-100">
                                                                            <option value="">Select Supplier</option>
                                                                            <template v-for="supplier in suppliers">
                                                                                <option :value="supplier.id">@{{ supplier.supplier_name }}</option>
                                                                            </template>
                                                                        </select>
                                                                    </template>

                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-inline mb-4">
                                                                    <label class="mr-5">Part Number</label>
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" class="custom-control-input" id="1_sources_multiple_parts_checkbox"
                                                                               v-model="detail_sources_multi_parts"
                                                                               @change="changeMultiPartsCheckbox"
                                                                        >
                                                                        <label class="custom-control-label text-dark" for="1_sources_multiple_parts_checkbox">Multiple Parts</label>
                                                                    </div>
                                                                </div>
                                                                <div class="form-inline">
                                                                    <input type="text" class="form-control border-0 bg-light mr-4" placeholder="Enter MPN">
                                                                    <input type="text" class="form-control border-0 bg-light" placeholder="QTY" style="width: 60px">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <button class="btn btn-outline-secondary mr-3">Cancel</button>
                                                            <button class="btn btn-primary w-md save-next">Save</button>
                                                        </div>
                                                    </div>
                                                    <div v-if="subTab=='dimensions'">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <p class="text-primary">Product Dimensions</p>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <p class="text-primary">PACKAGING Dimensions</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4 border-right">
                                                                <p class="font-weight-bold mb-3">Product Size</p>
                                                                <div class="bg-light py-2 px-3 mb-3 d-flex justify-content-between rounded">
                                                                    <div class="form-inline">
                                                                        <input type="text"
                                                                            class="form-control border-top border-left border-right bg-light p-1" placeholder="L" style="width: 40px"
                                                                            v-model="detail_dimensions_product_size_l"
                                                                        >
                                                                        <label class="mr-2">×</label>
                                                                        <input type="text"
                                                                            class="form-control border-top border-left border-right bg-light p-1" placeholder="W" style="width: 40px"
                                                                            v-model="detail_dimensions_product_size_w"
                                                                        >
                                                                        <label class="mr-2">×</label>
                                                                        <input type="text"
                                                                            class="form-control border-top border-left border-right bg-light p-1" placeholder="H" style="width: 40px"
                                                                            v-model="detail_dimensions_product_size_h"
                                                                        >
                                                                        <label class="mr-2">@{{ detail_dimensions_product_size_unit ? 'cm' : 'in' }}</label>
                                                                    </div>
                                                                    <div class="form-inline">
                                                                        <label class="mr-2">in</label>
                                                                        <div class="custom-control custom-switch">
                                                                            <input type="checkbox" class="custom-control-input" id="1_dimensions_product_size_unit"
                                                                                v-model="detail_dimensions_product_size_unit"

                                                                            >
                                                                            <label class="custom-control-label " for="1_dimensions_product_size_unit"></label>
                                                                        </div>
                                                                        <label class="text-primary">cm</label>
                                                                    </div>
                                                                </div>
                                                                <p class="font-weight-bold mb-3">Weight</p>
                                                                <div class="bg-light py-2 px-3 mb-3 d-flex justify-content-between rounded">
                                                                    <div class="form-inline">
                                                                        <input type="text" class="form-control border-top border-left border-right bg-light p-1" placeholder="L" style="width: 60px"
                                                                            v-model="detail_dimensions_product_weight"
                                                                        >
                                                                        <label class="mr-2">@{{ detail_dimensions_product_weight_unit ? 'kg' : 'lbs' }}</label>
                                                                    </div>
                                                                    <div class="form-inline">
                                                                        <label class="mr-2">lbs</label>
                                                                        <div class="custom-control custom-switch">
                                                                            <input type="checkbox" class="custom-control-input" id="1_dimensions_product_weight_unit"
                                                                                v-model="detail_dimensions_product_weight_unit"
                                                                            >
                                                                            <label class="custom-control-label " for="1_dimensions_product_weight_unit"></label>
                                                                        </div>
                                                                        <label class="text-primary">kg</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <div class="row mb-2">
                                                                    <div class="col-2"></div>
                                                                    <div class="col-1 font-weight-bold">QTY</div>
                                                                    <div class="col-3 d-flex justify-content-between">
                                                                        <div class="font-weight-bold">Size</div>
                                                                        <div class="form-inline rounded">
                                                                            <label class="mr-2">in</label>
                                                                            <div class="custom-control custom-switch">
                                                                                <input type="checkbox" class="custom-control-input" id="1_dimensions_packaging_size_unit"
                                                                                    v-model="detail_dimensions_packaging_size_unit">
                                                                                <label class="custom-control-label " for="1_dimensions_packaging_size_unit"></label>
                                                                            </div>
                                                                            <label class="text-primary">cm</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-3 d-flex justify-content-between">
                                                                        <div class="font-weight-bold">Weight</div>
                                                                        <div class="form-inline rounded">
                                                                            <label class="mr-2">lbs</label>
                                                                            <div class="custom-control custom-switch">
                                                                                <input type="checkbox" class="custom-control-input" id="1_dimensions_packaging_weight_unit"
                                                                                    v-model="detail_dimensions_packaging_weight_unit"
                                                                                >
                                                                                <label class="custom-control-label " for="1_dimensions_packaging_weight_unit"></label>
                                                                            </div>
                                                                            <label class="text-primary">kg</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-3 d-flex justify-content-between">
                                                                        <div class="font-weight-bold">Volume</div>
                                                                        <div class="form-inline rounded">
                                                                            <label class="mr-2">CBF</label>
                                                                            <div class="custom-control custom-switch">
                                                                                <input type="checkbox" class="custom-control-input" id="1_dimensions_packaging_volume_unit"
                                                                                    v-model="detail_dimensions_packaging_volume_unit"
                                                                                >
                                                                                <label class="custom-control-label " for="1_dimensions_packaging_volume_unit"></label>
                                                                            </div>
                                                                            <label class="text-primary">CBM</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row mb-2">
                                                                    <div class="col-2 text-dark font-weight-bold">
                                                                        <label class="col-form-label">Qty Per Box</label>
                                                                    </div>
                                                                    <div class="col-1 font-weight-bold">
                                                                        <div class="bg-light p-1">
                                                                            <input type="text" class="form-control border-top border-left border-right bg-light p-0 w-100" placeholder=""
                                                                                style="height: calc(1.5em + .44rem + 2px);"
                                                                                v-model="detail_dimensions_packaging_qty_per_box_qty"
                                                                            >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-3">
                                                                        <div class="form-inline rounded bg-light p-1">
                                                                            <input type="text" class="form-control border-top border-left border-right bg-light p-0" placeholder="L"
                                                                                style="width: 40px; height: calc(1.5em + .44rem + 2px);"
                                                                                v-model="detail_dimensions_packaging_qty_per_box_size_l"
                                                                            >
                                                                            <label class="mr-2">×</label>
                                                                            <input type="text" class="form-control border-top border-left border-right bg-light p-0" placeholder="W"
                                                                                style="width: 40px; height: calc(1.5em + .44rem + 2px);"
                                                                                v-model="detail_dimensions_packaging_qty_per_box_size_w"
                                                                            >
                                                                            <label class="mr-2">×</label>
                                                                            <input type="text" class="form-control border-top border-left border-right bg-light p-0" placeholder="H"
                                                                                style="width: 40px; height: calc(1.5em + .44rem + 2px);"
                                                                                v-model="detail_dimensions_packaging_qty_per_box_size_h"
                                                                            >
                                                                            <!-- <label class="mr-2">@{{ detail_dimensions_packaging_size_unit ? 'cm' : 'in' }}</label> -->
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-3">
                                                                        <div class="form-inline rounded bg-light p-1">
                                                                            <input type="text" class="form-control border-top border-left border-right bg-light p-0" placeholder=""
                                                                                style="width: 40px; height: calc(1.5em + .44rem + 2px);"
                                                                                v-model="detail_dimensions_packaging_qty_per_box_weight"
                                                                            >
                                                                            <label class="mr-2">@{{ detail_dimensions_packaging_weight_unit ? 'Kg' : 'lbs' }}</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-3">
                                                                        <div class="form-inline rounded bg-light p-1">
                                                                            <input type="text" class="form-control border-top border-left border-right bg-light p-0" placeholder=""
                                                                                style="width: 40px; height: calc(1.5em + .44rem + 2px);"
                                                                                v-model="detail_dimensions_packaging_qty_per_box_volume"
                                                                            >
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row mb-2">
                                                                    <div class="col-2 text-dark font-weight-bold">
                                                                        <label class="col-form-label">Box Per Carton</label>
                                                                    </div>
                                                                    <div class="col-1 font-weight-bold">
                                                                        <div class="bg-light p-1">
                                                                            <input type="text" class="form-control border-top border-left border-right bg-light p-0 w-100" placeholder=""
                                                                                style="height: calc(1.5em + .44rem + 2px);"
                                                                                v-model="detail_dimensions_packaging_box_per_carton_qty"
                                                                            >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-3">
                                                                        <div class="form-inline rounded bg-light p-1">
                                                                            <input type="text" class="form-control border-top border-left border-right bg-light p-0" placeholder="L"
                                                                                style="width: 40px; height: calc(1.5em + .44rem + 2px);"
                                                                                v-model="detail_dimensions_packaging_box_per_carton_size_l"
                                                                            >
                                                                            <label class="mr-2">×</label>
                                                                            <input type="text" class="form-control border-top border-left border-right bg-light p-0" placeholder="W"
                                                                                style="width: 40px; height: calc(1.5em + .44rem + 2px);"
                                                                                v-model="detail_dimensions_packaging_box_per_carton_size_w"
                                                                            >
                                                                            <label class="mr-2">×</label>
                                                                            <input type="text" class="form-control border-top border-left border-right bg-light p-0" placeholder="H"
                                                                                style="width: 40px; height: calc(1.5em + .44rem + 2px);"
                                                                                v-model="detail_dimensions_packaging_box_per_carton_size_h"
                                                                            >
                                                                            <!-- <label class="mr-2">@{{ detail_dimensions_packaging_size_unit ? 'cm' : 'in' }}</label> -->
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-3">
                                                                        <div class="form-inline rounded bg-light p-1">
                                                                            <input type="text" class="form-control border-top border-left border-right bg-light p-0" placeholder=""
                                                                                style="width: 40px; height: calc(1.5em + .44rem + 2px);"
                                                                                v-model="detail_dimensions_packaging_box_per_carton_weight"
                                                                            >
                                                                            <label class="mr-2">@{{ detail_dimensions_packaging_weight_unit ? 'Kg' : 'lbs' }}</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-3">
                                                                        <div class="form-inline rounded bg-light p-1">
                                                                            <input type="text" class="form-control border-top border-left border-right bg-light p-0" placeholder=""
                                                                                style="width: 40px; height: calc(1.5em + .44rem + 2px);"
                                                                                v-model="detail_dimensions_packaging_box_per_carton_volume"
                                                                            >
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row mb-2">
                                                                    <div class="col-2 text-dark font-weight-bold">
                                                                        <label class="col-form-label">Cartons Per Pallet</label>
                                                                    </div>
                                                                    <div class="col-1 font-weight-bold">
                                                                        <div class="bg-light p-1">
                                                                            <input type="text" class="form-control border-top border-left border-right bg-light p-0 w-100" placeholder=""
                                                                                style="height: calc(1.5em + .44rem + 2px);"
                                                                                v-model="detail_dimensions_packaging_carton_per_box_qty"
                                                                            >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-3">
                                                                        <div class="form-inline rounded bg-light p-1">
                                                                            <input type="text" class="form-control border-top border-left border-right bg-light p-0" placeholder="L"
                                                                                style="width: 40px; height: calc(1.5em + .44rem + 2px);"
                                                                                v-model="detail_dimensions_packaging_carton_per_pallet_size_l"
                                                                            >
                                                                            <label class="mr-2">×</label>
                                                                            <input type="text" class="form-control border-top border-left border-right bg-light p-0" placeholder="W"
                                                                                style="width: 40px; height: calc(1.5em + .44rem + 2px);"
                                                                                v-model="detail_dimensions_packaging_carton_per_pallet_size_w"
                                                                            >
                                                                            <label class="mr-2">×</label>
                                                                            <input type="text" class="form-control border-top border-left border-right bg-light p-0" placeholder="H"
                                                                                style="width: 40px; height: calc(1.5em + .44rem + 2px);"
                                                                                v-model="detail_dimensions_packaging_carton_per_pallet_size_h"
                                                                            >
                                                                            <!-- <label class="mr-2">@{{ detail_dimensions_packaging_size_unit ? 'cm' : 'in' }}</label> -->
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-3">
                                                                        <div class="form-inline rounded bg-light p-1">
                                                                            <input type="text" class="form-control border-top border-left border-right bg-light p-0" placeholder=""
                                                                                style="width: 40px; height: calc(1.5em + .44rem + 2px);"
                                                                                v-model="detail_dimensions_packaging_carton_per_pallet_weight"
                                                                            >
                                                                            <label class="mr-2">@{{ detail_dimensions_packaging_weight_unit ? 'Kg' : 'lbs' }}</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-3">
                                                                        <div class="form-inline rounded bg-light p-1">
                                                                            <input type="text" class="form-control border-top border-left border-right bg-light p-0" placeholder=""
                                                                                style="width: 40px; height: calc(1.5em + .44rem + 2px);"
                                                                                v-model="detail_dimensions_packaging_carton_per_pallet_volume"
                                                                            >
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <p>AMZ Dimensional Weight</p>
                                                        </div>
                                                        <div>
                                                            <button class="btn btn-outline-secondary mr-3">Cancel</button>
                                                            <button class="btn btn-primary w-md" @click="saveDimensions(row)">Save</button>
                                                        </div>
                                                    </div>
                                                    <div v-if="subTab=='quantity_discounts'">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <p class="text-primary">MOQ QUANTITY</p>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <p class="text-primary">QUANTITY DISCOUNT</p>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <p class="text-primary">DISCOUNT SETTINGS</p>
                                                            </div>
                                                        </div>
                                                        <div class="row mb-4">
                                                            <div class="col-md-3 border-right">
                                                                <div class="row">
                                                                    <div class="col-6">
                                                                        <p class="text-center font-weight-bold">Unit</p>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <p class="text-center font-weight-bold">Price</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-6">
                                                                        <input type="text" class="form-control border-0 mx-auto bg-light" placeholder="" style="width: 100px;" v-model="detail_quantity_moq_unit">
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <input type="text" class="form-control border-0 mx-auto bg-light" placeholder="" style="width: 100px;" v-model="detail_quantity_moq_price">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 border-right">
                                                                <div class="row">
                                                                    <div class="col-6">
                                                                        <p class="text-center font-weight-bold">Unit</p>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <p class="text-center font-weight-bold">Price</p>
                                                                    </div>
                                                                </div>
                                                                <template v-for="discount in detail_quantity_discounts">
                                                                    <div class="row mb-2">
                                                                        <div class="col-6">
                                                                            <input type="text" class="form-control border-0 mx-auto bg-light" placeholder="" style="width: 100px;" v-model="discount['unit']">
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <input type="text" class="form-control border-0 mx-auto bg-light" placeholder="" style="width: 100px;" v-model="discount['price']">
                                                                        </div>
                                                                    </div>
                                                                </template>
                                                                <div>
                                                                    <button type="button" class="btn btn-link" @click="addNewDiscount">
                                                                        <i class="bx bx-plus-circle font-size-16 align-middle"></i>
                                                                        Add
                                                                    </button>
                                                                </div>

                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="mt-4 d-flex">
                                                                    <label class="col-form-label text-dark font-weight-bold" style="width: 210px;"> Quantity Calculation </label>
                                                                    <select name="" class="form-select" style="width: 214px;">
                                                                        <option value="">Variations</option>
                                                                    </select>
                                                                </div>
                                                                <div class="mt-3 d-flex">
                                                                    <label class="col-form-label text-dark font-weight-bold" style="width: 210px;"> Quantity Bundled With </label>
                                                                    <select name="" class="form-select" style="width: 214px;">
                                                                        <option value="">3 Selected</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <button class="btn btn-outline-secondary mr-3">Cancel</button>
                                                            <button class="btn btn-primary w-md save-next" @click="saveQuantityDiscounts(row)">Save</button>
                                                        </div>
                                                    </div>
                                                    <div v-if="subTab=='profit_analysis'">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <p class="text-primary">LANDED PRODUCT COST</p>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <p class="text-primary">INLAND COST</p>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <p class="text-primary">AMAZON COST</p>
                                                            </div>
                                                            <div class="col-md-3">
                                                            </div>
                                                        </div>
                                                        <div class="row mb-2">
                                                            <div class="col-md-3">
                                                                TOTAL
                                                                <span class="ml-3 text-dark font-weight-bold">$@{{ detail_profit_landed_total }}</span>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <span class="text-primary font-weight-bold position-absolute" style="left: -7px; top: -5px; font-size: 20px;">+</span>
                                                                TOTAL <span class="ml-3 text-dark font-weight-bold">$@{{ detail_profit_inland_total }}</span>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <span class="text-primary font-weight-bold position-absolute" style="left: -7px; top: -5px; font-size: 20px;">+</span>
                                                                TOTAL
                                                                <span class="ml-3 text-dark font-weight-bold">$@{{ detail_profit_amazon_total }}</span>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <span class="text-primary font-weight-bold position-absolute" style="left: -7px; top: -5px; font-size: 20px;">=</span>
                                                                TOTAL COST
                                                                <span class="ml-3 text-dark font-weight-bold">$@{{ detail_profit_total }}</span>
                                                            </div>
                                                        </div>
                                                        <div class="row mb-4">
                                                            <div class="col-md-3 border-right">
                                                                <p class="font-weight-bold mt-4 mb-2 text-gray">UNIT COST</p>
                                                                <div class="rounded bg-light py-2 px-3">
                                                                    <span style="margin-right: -4px;">$</span>
                                                                    <input type="text" class="border-0 bg-transparent" v-model="detail_profit_landed_unit_cost">
                                                                </div>
                                                                <p class="font-weight-bold mt-3 mb-2 text-gray">SHIPPING</p>
                                                                <div class="rounded bg-light py-2 px-3">
                                                                    <span style="margin-right: -4px;">$</span>
                                                                    <input type="text" class="border-0 bg-transparent" v-model="detail_profit_landed_shipping">
                                                                </div>
                                                                <p class="font-weight-bold mt-3 mb-2 text-gray">CUSTOMS</p>
                                                                <div class="rounded bg-light py-2 px-3">
                                                                    <span style="margin-right: -4px;">$</span>
                                                                    <input type="text" class="border-0 bg-transparent" v-model="detail_profit_landed_customs">
                                                                </div>
                                                                <p class="font-weight-bold mt-3 mb-2 text-gray">OTHER</p>
                                                                <div class="rounded bg-light py-2 px-3">
                                                                    <span style="margin-right: -4px;">$</span>
                                                                    <input type="text" class="border-0 bg-transparent" v-model="detail_profit_landed_other">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 border-right">
                                                                <p class="font-weight-bold mt-4 mb-2 text-gray">WAREHOUSE STORAGE</p>
                                                                <div class="rounded bg-light py-2 px-3">
                                                                    <span style="margin-right: -4px;">$</span>
                                                                    <input type="text" class="border-0 bg-transparent" v-model="detail_profit_inland_warehouse_storage">
                                                                </div>
                                                                <p class="font-weight-bold mt-3 mb-2 text-gray">INBOUND SHIPPING</p>
                                                                <div class="rounded bg-light py-2 px-3">
                                                                    <span style="margin-right: -4px;">$</span>
                                                                    <input type="text" class="border-0 bg-transparent" v-model="detail_profit_inland_inbound_shipping">
                                                                </div>
                                                                <p class="font-weight-bold mt-3 mb-2 text-gray">OTHER</p>
                                                                <div class="rounded bg-light py-2 px-3">
                                                                    <span style="margin-right: -4px;">$</span>
                                                                    <input type="text" class="border-0 bg-transparent" v-model="detail_profit_inland_other">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 border-right">
                                                                <div class="mt-5 d-flex">
                                                                    <p class="font-weight-bold mt-3 mb-2 text-gray" style="width: 140px;"> FBA FEE </p>
                                                                    <p class="font-weight-bold mt-3 mb-2" style="width: 140px;"> @{{ row.profit_analysis.fba }} </p>
                                                                </div>
                                                                <div class="mt-2 d-flex">
                                                                    <p class="font-weight-bold mt-3 mb-2 text-gray" style="width: 140px;"> REFERRAL FEE </p>
                                                                    <p class="font-weight-bold mt-3 mb-2" style="width: 140px;"> @{{ row.profit_analysis.referral }} </p>
                                                                </div>
                                                                <div class="mt-2 d-flex">
                                                                    <p class="font-weight-bold mt-3 mb-2 text-gray" style="width: 140px;"> STORAGE FEE </p>
                                                                    <p class="font-weight-bold mt-3 mb-2" style="width: 140px;"> @{{ row.profit_analysis.storage }} </p>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="mt-5 d-flex">
                                                                    <label class="col-form-label text-dark font-weight-bold" style="width: 120px;"> PRICE POINT </label>
                                                                    <label class="col-form-label text-dark font-weight-bold" style="width: 80px;"> @{{ product.price_point}} </label>
                                                                </div>
                                                                <div class="mt-2 d-flex">
                                                                    <label class="col-form-label text-dark font-weight-bold" style="width: 120px;"> TOTAL PROFIT </label>
                                                                    <label class="col-form-label text-success font-weight-bold" style="width: 80px;"> $@{{ detail_profit_total_profit }} </label>
                                                                </div>
                                                                <div class="mt-2 d-flex">
                                                                    <label class="col-form-label text-dark font-weight-bold" style="width: 120px;"> PROFIT MARGIN </label>
                                                                    <label class="col-form-label text-success font-weight-bold" style="width: 80px;"> @{{ product.profit_analysis.profit_margin}} </label>
                                                                    <label class="col-form-label text-primary font-weight-bold" style="width: 100px;"> @{{ product.profit_analysis.roi}} ROI </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <button class="btn btn-outline-secondary mr-3">Cancel</button>
                                                            <button class="btn btn-primary w-md save-next" @click="saveProfitAnalysis(row)">Save</button>
                                                        </div>
                                                    </div>
                                                    <div v-if="subTab=='order_setting'">
                                                        <div class="row">
                                                            <div class="col-md-4 border-right">
                                                                <div class="form-group row mt-5">
                                                                    <label class="col-sm-4 col-form-label text-dark">Order Volume</label>
                                                                    <div class="col-sm-2 pr-0">
                                                                        <input type="text" class="form-control" v-model="detail_order_volume">
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <select name="" class="form-select w-100" v-model="detail_order_type">
                                                                            <option value="day">Days</option>
                                                                            <option value="month">Calendar Month</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-5" style="min-height: 300px;">
                                                                <div class="form-group mt-5">
                                                                    <label for="" class="col-form-label mr-3">Reorder Schedule</label>
                                                                    <select name="reorder_schedule" v-model="detail_order_schedule" class="form-select">
                                                                        <option value="on-demand">On Demand</option>
                                                                        <option value="weekly">Weekly</option>
                                                                        <option value="multi-weekly">Multi Weekly</option>
                                                                        <option value="monthly">Monthly</option>
                                                                        <option value="multi-monthly">Multi Monthly</option>
                                                                    </select>
                                                                </div>
                                                                <div>
                                                                    <template v-if="detail_order_schedule=='multi-weekly'">
                                                                        <p>Select one day to set your weekly schedule.</p>
                                                                        <div v-for="day in ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']" class="d-inline-block mr-2">
                                                                            <input type="checkbox" v-model="detail_order_schedule_multi_weekly" :value="day" :id="'days-weekly' + day" hidden>
                                                                            <label class="btn-days btn-days-weekly" :for="'days-weekly' + day"> @{{ day }} </label>
                                                                        </div>
                                                                    </template>
                                                                    <template v-if="detail_order_schedule=='weekly'">
                                                                        <p>Select one day to set your weekly schedule.</p>
                                                                        <div v-for="day in ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']" class="d-inline-block mr-2">
                                                                            <input type="radio" v-model="detail_order_schedule_weekly" :value="day" :id="'days-weekly' + day" hidden>
                                                                            <label class="btn-days btn-days-weekly" :for="'days-weekly' + day"> @{{ day }} </label>
                                                                        </div>
                                                                    </template>
                                                                    <template v-if="detail_order_schedule=='multi-monthly'">
                                                                        <p>Select one day to set your monthly schedule.</p>
                                                                        <div v-for="i in 5" class="mb-2">
                                                                            <div v-for="j in 7" class="d-inline-block mr-2">
                                                                                <template v-if="(i-1)*7+j < 32">
                                                                                    <input type="checkbox" v-model="detail_order_schedule_multi_monthly" :value="(i-1)*7+j" :id="'days-monthly' + ((i-1)*7+j)" hidden>
                                                                                    <label class="btn-days btn-days-monthly" :for="'days-monthly' + ((i-1)*7+j)"> @{{ (i-1)*7+j }} </label>
                                                                                </template>
                                                                            </div>
                                                                        </div>
                                                                    </template>
                                                                    <template v-if="detail_order_schedule=='monthly'">
                                                                        <p>Select one day to set your monthly schedule.</p>
                                                                        <div v-for="i in 5" class="mb-2">
                                                                            <div v-for="j in 7" class="d-inline-block mr-2">
                                                                                <template v-if="(i-1)*7+j < 32">
                                                                                    <input type="radio" v-model="detail_order_schedule_monthly" :value="(i-1)*7+j" :id="'days-monthly' + ((i-1)*7+j)" hidden>
                                                                                    <label class="btn-days btn-days-monthly" :for="'days-monthly' + ((i-1)*7+j)"> @{{ (i-1)*7+j }} </label>
                                                                                </template>
                                                                            </div>
                                                                        </div>
                                                                    </template>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="btn-group ml-5" role="group">
                                                                    <button class="btn btn-secondary font-size-12" id="supplier-btn" style="width:90px;">Default</button>
                                                                    <button class="btn btn-primary font-size-12" id="shipping-agent-btn" style="width:90px;">Custom</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <button class="btn btn-outline-secondary mr-3">Cancel</button>
                                                            <button class="btn btn-primary w-md save-next" @click="saveOrderSetting(row)">Save</button>
                                                        </div>
                                                    </div>
                                                    <div v-if="subTab=='lead_time'">
                                                        <div class="d-flex justify-content-between align-items-center mb-4">
                                                            <div class="d-flex align-items-center">
                                                                <div class="btn-group mr-5" role="group">
                                                                    <button id="boat-btn" class="btn" :class="detail_lead_time_type=='boat' ? 'btn-primary' : 'btn-light1'"
                                                                            @click="detail_lead_time_type='boat'"
                                                                    >
                                                                        <i class="mdi mdi-ferry"></i> Boat
                                                                    </button>
                                                                    <button id="plane-btn" class="btn" :class="detail_lead_time_type=='plane' ? 'btn-primary' : 'btn-light1'"
                                                                            @click="detail_lead_time_type='plane'"
                                                                    >
                                                                        <i class="mdi mdi-airplane"></i> Plane
                                                                    </button>
                                                                </div>
                                                                <div class="form-inline">
                                                                    <label for="" class="mr-3">Set Default</label>
                                                                    <select name="" class="form-control">
                                                                        <option value="">Boat</option>
                                                                        <option value="">Plane</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="btn-group ml-5" role="group">
                                                                <button class="btn font-size-12"
                                                                        :class="detail_lead_time_set_type == 'default' ? 'btn-primary' : 'btn-secondary'"
                                                                        style="width:90px;"
                                                                        @click="detail_lead_time_set_type = 'default'"
                                                                >Default</button>
                                                                <button class="btn font-size-12"
                                                                        :class="detail_lead_time_set_type == 'custom' ? 'btn-primary' : 'btn-secondary'"
                                                                        style="width:90px;"
                                                                        @click="detail_lead_time_set_type = 'custom'"
                                                                >Custom</button>
                                                            </div>
                                                        </div>
                                                        <template v-if="detail_lead_time_type=='boat'">
                                                            <div class="d-flex justify-content-between mb-4">
                                                                <div class="w-16">
                                                                    <h4>Production</h4>
                                                                    <p class="text-light1"><small>Manufacture your products</small></p>
                                                                    <div class="input-group border-bottom">
                                                                        <input type="text" class="form-control border-0 input-edit w-50"
                                                                               v-model="detail_lead_time_boat_production"
                                                                        >
                                                                        <div class="input-group-append w-50">
                                                                            <span class="input-group-text text-light1 bg-transparent border-0">Days</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="d-none border-bottom">
                                                                        <span class="input-group-text border-0 bg-transparent input-show"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="text-primary align-self-center">
                                                                    <i class="mdi mdi-arrow-right"></i>
                                                                </div>
                                                                <div class="w-16">
                                                                    <h4>To Port</h4>
                                                                    <p class="text-light1"><small>Transported to Port </small></p>
                                                                    <div class="input-group border-bottom">
                                                                        <input type="text" class="form-control border-0 input-edit w-50"
                                                                               v-model="detail_lead_time_boat_port"
                                                                        >
                                                                        <div class="input-group-append w-50">
                                                                            <span class="input-group-text text-light1 bg-transparent border-0">Days</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="d-none border-bottom">
                                                                        <span class="input-group-text border-0 bg-transparent input-show"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="text-primary align-self-center">
                                                                    <i class="mdi mdi-arrow-right"></i>
                                                                </div>
                                                                <div class="w-16">
                                                                    <h4>Transit Time</h4>
                                                                    <p class="text-light1"><small>Leaves port and travels</small></p>
                                                                    <div class="input-group border-bottom">
                                                                        <input type="text" class="form-control border-0 input-edit w-50"
                                                                               v-model="detail_lead_time_boat_transit"
                                                                        >
                                                                        <div class="input-group-append w-50">
                                                                            <span class="input-group-text text-light1 bg-transparent border-0">Days</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="d-none border-bottom">
                                                                        <span class="input-group-text border-0 bg-transparent input-show"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="text-primary align-self-center">
                                                                    <i class="mdi mdi-arrow-right"></i>
                                                                </div>
                                                                <div class="w-16">
                                                                    <h4>To Warehouse</h4>
                                                                    <p class="text-light1"><small>From Port to you</small></p>
                                                                    <div class="input-group border-bottom">
                                                                        <input type="text" class="form-control border-0 input-edit w-50"
                                                                               v-model="detail_lead_time_boat_warehouse"
                                                                        >
                                                                        <div class="input-group-append w-50">
                                                                            <span class="input-group-text text-light1 bg-transparent border-0">Days</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="d-none border-bottom">
                                                                        <span class="input-group-text border-0 bg-transparent input-show"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="text-primary align-self-center">
                                                                    <i class="mdi mdi-arrow-right"></i>
                                                                </div>
                                                                <div class="w-16">
                                                                    <h4>To Amazon</h4>
                                                                    <p class="text-light1"><small>From you to Az </small></p>
                                                                    <div class="input-group border-bottom">
                                                                        <input type="text" class="form-control border-0 input-edit w-50"
                                                                               v-model="detail_lead_time_boat_amazon"
                                                                        >
                                                                        <div class="input-group-append w-50">
                                                                            <span class="input-group-text text-light1 bg-transparent border-0">Days</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="d-none border-bottom">
                                                                        <span class="input-group-text border-0 bg-transparent input-show"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row my-5">
                                                                <div class="col-md-6">
                                                                    <div class="form-inline">
                                                                        <div class="form-group">
                                                                            <label for="" class="font-size-20 mr-2">PO To Production</label>
                                                                            <input type="number" name="" class="form-control w-25 px-1"
                                                                                   v-model="detail_lead_time_boat_po_to_production"
                                                                            >
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-inline">
                                                                        <div class="form-group">
                                                                            <label for="" class="font-size-20 mr-2">Safety</label>
                                                                            <input type="number" name="" class="form-control w-25 px-1"
                                                                                   v-model="detail_lead_time_boat_safety">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-inline">
                                                                        <div class="form-group">
                                                                            <label for="" class="font-size-20 mr-4">Total Lead Time</label>
                                                                            <label for="" class="font-size-20 text-primary">
                                                                                @{{ Number(detail_lead_time_boat_production) + Number(detail_lead_time_boat_port) + Number(detail_lead_time_boat_transit) + Number(detail_lead_time_boat_warehouse) + Number(detail_lead_time_boat_amazon) + Number(detail_lead_time_boat_po_to_production) + Number(detail_lead_time_boat_safety) }} Days
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </template>
                                                        <template v-else>
                                                            <div class="d-flex justify-content-between mb-4">
                                                                <div class="w-16">
                                                                    <h4>Production</h4>
                                                                    <p class="text-light1"><small>Manufacture your products</small></p>
                                                                    <div class="input-group border-bottom">
                                                                        <input type="text" class="form-control border-0 input-edit w-50"
                                                                               v-model="detail_lead_time_plane_production"
                                                                        >
                                                                        <div class="input-group-append w-50">
                                                                            <span class="input-group-text text-light1 bg-transparent border-0">Days</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="d-none border-bottom">
                                                                        <span class="input-group-text border-0 bg-transparent input-show"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="text-primary align-self-center">
                                                                    <i class="mdi mdi-arrow-right"></i>
                                                                </div>
                                                                <div class="w-16">
                                                                    <h4>To Port</h4>
                                                                    <p class="text-light1"><small>Transported to Port </small></p>
                                                                    <div class="input-group border-bottom">
                                                                        <input type="text" class="form-control border-0 input-edit w-50"
                                                                               v-model="detail_lead_time_plane_port"
                                                                        >
                                                                        <div class="input-group-append w-50">
                                                                            <span class="input-group-text text-light1 bg-transparent border-0">Days</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="d-none border-bottom">
                                                                        <span class="input-group-text border-0 bg-transparent input-show"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="text-primary align-self-center">
                                                                    <i class="mdi mdi-arrow-right"></i>
                                                                </div>
                                                                <div class="w-16">
                                                                    <h4>Transit Time</h4>
                                                                    <p class="text-light1"><small>Leaves port and travels</small></p>
                                                                    <div class="input-group border-bottom">
                                                                        <input type="text" class="form-control border-0 input-edit w-50"
                                                                               v-model="detail_lead_time_plane_transit"
                                                                        >
                                                                        <div class="input-group-append w-50">
                                                                            <span class="input-group-text text-light1 bg-transparent border-0">Days</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="d-none border-bottom">
                                                                        <span class="input-group-text border-0 bg-transparent input-show"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="text-primary align-self-center">
                                                                    <i class="mdi mdi-arrow-right"></i>
                                                                </div>
                                                                <div class="w-16">
                                                                    <h4>To Warehouse</h4>
                                                                    <p class="text-light1"><small>From Port to you</small></p>
                                                                    <div class="input-group border-bottom">
                                                                        <input type="text" class="form-control border-0 input-edit w-50"
                                                                               v-model="detail_lead_time_plane_warehouse"
                                                                        >
                                                                        <div class="input-group-append w-50">
                                                                            <span class="input-group-text text-light1 bg-transparent border-0">Days</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="d-none border-bottom">
                                                                        <span class="input-group-text border-0 bg-transparent input-show"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="text-primary align-self-center">
                                                                    <i class="mdi mdi-arrow-right"></i>
                                                                </div>
                                                                <div class="w-16">
                                                                    <h4>To Amazon</h4>
                                                                    <p class="text-light1"><small>From you to Az </small></p>
                                                                    <div class="input-group border-bottom">
                                                                        <input type="text" class="form-control border-0 input-edit w-50"
                                                                               v-model="detail_lead_time_plane_amazon"
                                                                        >
                                                                        <div class="input-group-append w-50">
                                                                            <span class="input-group-text text-light1 bg-transparent border-0">Days</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="d-none border-bottom">
                                                                        <span class="input-group-text border-0 bg-transparent input-show"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row my-5">
                                                                <div class="col-md-6">
                                                                    <div class="form-inline">
                                                                        <div class="form-group">
                                                                            <label for="" class="font-size-20 mr-2">PO To Production</label>
                                                                            <input type="number" name="" class="form-control w-25 px-1"
                                                                                   v-model="detail_lead_time_plane_po_to_production"
                                                                            >
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-inline">
                                                                        <div class="form-group">
                                                                            <label for="" class="font-size-20 mr-2">Safety</label>
                                                                            <input type="number" name="" class="form-control w-25 px-1"
                                                                                   v-model="detail_lead_time_plane_safety">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-inline">
                                                                        <div class="form-group">
                                                                            <label for="" class="font-size-20 mr-4">Total Lead Time</label>
                                                                            <label for="" class="font-size-20 text-primary">
                                                                                @{{ Number(detail_lead_time_plane_production) + Number(detail_lead_time_plane_port) + Number(detail_lead_time_plane_transit) + Number(detail_lead_time_plane_warehouse) + Number(detail_lead_time_plane_amazon) + Number(detail_lead_time_plane_po_to_production) + Number(detail_lead_time_plane_safety) }} Days
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </template>
                                                        <div>
                                                            <button class="btn btn-outline-secondary mr-3">Cancel</button>
                                                            <button class="btn btn-primary w-md save-next" @click="saveLeadTime(row)">Save</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </template>
                        </template>
                    </tbody>
                </table>
                <div id="scrollbar">
                    <div id="track" class="track"></div>
                    <div id="thumb" class="thumb"></div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="bulkLabelModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <form @submit.prevent>
                            <h3>Label -
                                <span class="text-primary1">
                                    <span class="selectedCount">@{{ selected.length ? selected.length : 'No' }}</span> Items Selected
                                </span>
                            </h3>
                            <select name="" class="form-select w-100" v-model="label">
                                <option value="">Select Label</option>
                                <option v-for="label in labels" :value="label.id">@{{ label.label_name }}</option>
                            </select>
                            <div v-if="isNewLabel" class="my-3 position-relative">
                                <input type="text" class="form-control" v-model="newLabel">
                                <i class="mdi mdi-close-circle" style="position:absolute; top: 25%; right: 10px; cursor: pointer;" @click="isNewLabel=false; newLabel=''"></i>
                            </div>
                            <div v-else class="text-primary1 my-2" @click="isNewLabel=true">
                                <i class="bx bx-plus-circle font-size-16 align-middle"></i>
                                Add Label
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <button class="btn btn-light w-100" data-dismiss="modal">Cancel</button>
                                </div>
                                <div class="col-md-6">
                                    <button class="btn btn-primary w-100" @click="applyBulkLabel">Apply</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="addSourceModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <h3 class="h3">Add Sources</h3>
                        <div class="p-3" style="overflow-y: auto; max-height: 80vh;">
                            <div v-for="(source, i) in detail_sources_sources" class="border-bottom mb-3">
                                <h5 class="h5">Source @{{ i + 1 }}</h5>
                                <div class="row">
                                    <div class="col-4">
                                        <label for="" class="col-form-label">Select Source Type</label>
                                    </div>
                                    <div class="col-8 d-flex align-items-center">
                                        <div class="form-inline">
                                            <label class="mr-2" :class="{'text-primary': !source.type }">Supplier</label>
                                            <div class="custom-control custom-switch mr-2">
                                                <input type="checkbox" :id="'source_type_' + i" class="custom-control-input" v-model="source.type">
                                                <label :for="'source_type_' + i" class="custom-control-label"></label>
                                            </div>
                                            <label class="mr-5" :class="{'text-primary': source.type }">Vendor</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-4">
                                        <label for="" class="col-form-label">Select Supplier</label>
                                    </div>
                                    <div class="col-8">
                                        <select name="" class="form-select w-100">
                                            <option value="">Select Supplier</option>
                                            <template v-for="supplier in suppliers">
                                                <option :value="supplier.id">@{{ supplier.supplier_name }}</option>
                                            </template>
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-4">
                                        <label for="" class="col-form-label">Add Parts</label>
                                    </div>
                                    <div class="col-8">
                                        <template v-for="part in source.parts">
                                            <div class="row mb-2">
                                                <div class="col-8">
                                                    <input type="text" name="" id="" class="form-control"
                                                           v-model="part.mpn" placeholder="Type or Select MPN"
                                                    >
                                                </div>
                                                <div class="col-4">
                                                    <input type="text" name="" id="" class="form-control"
                                                           v-model="part.mpn" placeholder="Enter QTY"
                                                    >
                                                </div>
                                            </div>
                                            <div class="mb-3">
                                                <select v-model="part.shipping_option" class="form-select w-100">
                                                    <option value="">Select Shipping Option</option>
                                                    <option value="ships_with_parent">Ships With Parent</option>
                                                    <option value="ships_with_child">Ships With Child</option>
                                                    <option value="ships_seperately">Ships Seperately</option>
                                                </select>
                                            </div>
                                        </template>
                                        <div>
                                            <button type="button" class="btn btn-link p-0" @click="source.parts.push({'mpn': '', 'qty': '', 'shipping_option': ''})">
                                                <i class="bx bx-plus-circle font-size-16 align-middle"></i>
                                                Add Part
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="d-flex justify-content-between">
                            <div>
                                <button type="button" class="btn btn-outline-primary"
                                    @click="detail_sources_sources.push({
                                            type: false,
                                            supplier: '',
                                            parts: [
                                                {
                                                    'mpn': '',
                                                    'qty': '',
                                                    'shipping_option': ''
                                                }
                                            ]
                                    })"
                                >Add Source</button>
                            </div>
                            <div>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-primary">Submit</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="addPartModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <h3 class="h3">Add Parts</h3>
                        <div class="p-3" style="overflow-y: auto; max-height: 80vh;">
                            <h5 class="h5"> SUPPLIER > TINA </h5>
                            <div class="row mb-2">
                                <div class="col-4">
                                    <label for="" class="col-form-label">Add Parts</label>
                                </div>
                                <div class="col-8">
                                    <div class="border-bottom mb-3" v-for="part in detail_sources_parts">
                                        <div class="row mb-2">
                                            <div class="col-8">
                                                <input type="text" name="" id="" class="form-control"
                                                       v-model="part.mpn" placeholder="Type or Select MPN"
                                                >
                                            </div>
                                            <div class="col-4">
                                                <input type="text" name="" id="" class="form-control"
                                                       v-model="part.mpn" placeholder="Enter QTY"
                                                >
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <select v-model="part.shipping_option" class="form-select w-100">
                                                <option value="">Select Shipping Option</option>
                                                <option value="ships_with_parent">Ships With Parent</option>
                                                <option value="ships_with_child">Ships With Child</option>
                                                <option value="ships_seperately">Ships Seperately</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div>
                                        <button type="button" class="btn btn-link p-0" @click="detail_sources_parts.push({'mpn': '', 'qty': '', 'shipping_option': ''})">
                                            <i class="bx bx-plus-circle font-size-16 align-middle"></i>
                                            Add Part
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="d-flex justify-content-between">
                            <div>
                                <button type="button" class="btn btn-outline-primary"
                                        @click="detail_sources_sources.push({
                                            type: false,
                                            supplier: '',
                                            parts: [
                                                {
                                                    'mpn': '',
                                                    'qty': '',
                                                    'shipping_option': ''
                                                }
                                            ]
                                    })"
                                >Add Source</button>
                            </div>
                            <div>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-primary">Submit</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        let groups = {
            'basic_info': {
                title: 'Basic Info',
                icon: 'mdi mdi-information-outline',
                bg: '#F3F8FF',
                bg_h: '#e3efff',
                color: '#227cff',
                lockable: true,
            },
            'sources': {
                title: 'Sources',
                icon: 'mdi mdi-account-outline',
                color: '#b26e70',
                bg: '#FFF7F7',
                bg_h: '#f1cccc',
            },
            'dimensions':{
                title: 'Dimensions',
                icon: 'mdi mdi-square-edit-outline',
                color: '#8162f7',
                bg: '#F5EEFC',
                bg_h: '#ede1fa',
            },
            'quantity_discounts':{
                title: 'Quantity Discounts',
                icon: 'mdi mdi-brightness-percent',
                color: '#ff9122',
                bg: '#FAF6F1',
                bg_h: '#f7e6d4',
            },
            'profit_analysis':{
                title: 'Profit Analysis',
                icon: 'mdi mdi-chart-line',
                color: '#65b515',
                bg: '#F7FBF3',
                bg_h: '#e7fdd3',
            },
            'order_settings': {
                title: 'Order Setting',
                icon: 'mdi mdi-bag-personal-outline',
                color: '#65b515',
                bg: '#F7FBF3',
                bg_h: '#e7fdd3',
            },
            'lead_time': {
                title: 'Lead Time',
                icon: 'mdi mdi-clock-outline',
                color: '#f1b44c',
                bg: '#FFFBF6',
                bg_h: '#fce7cd',
            },
        }

        var content = new Vue({
            el: '#content',
            data: {
                groups: groups,
                fields: [
                    {group: 'basic_info', th: 'Image', td:'image'},
                    {group: 'basic_info', th: 'Nickname', td: 'nickname'},
                    {group: 'basic_info', th: 'SKU', td: 'sku'},
                    {group: 'basic_info', th: 'FNSKU', td: 'fnsku'},
                    {group: 'basic_info', th: 'Title', td:'title'},
                    {group: 'basic_info', th: 'ASIN', td: 'asin'},
                    {group: 'basic_info', th: 'UPC', td: 'upc'},
                    {group: 'basic_info', th: 'Brand', td:'brand'},
                    {group: 'basic_info', th: 'Category', td:'category'},
                    {group: 'basic_info', th: 'Variation', td:'variation'},
                    {group: 'basic_info', th: 'Labels', td:'lavels'},

                    {group: 'sources', th: 'Source Type', td: 'source_type'},
                    {group: 'sources', th: 'Sources', td:'sources'},
                    {group: 'sources', th: 'MPN', td:'mpn'},
                    {group: 'sources', th: 'QTY Per SKU', td:'qty_per_sku'},

                    {group: 'dimensions', th: 'Item Height', td: 'item_height'},
                    {group: 'dimensions', th: 'Item Width', td: 'item_width'},
                    {group: 'dimensions', th: 'Item Length', td: 'item_length'},
                    {group: 'dimensions', th: 'Item Weight', td: 'item_weight'},
                    {group: 'dimensions', th: 'AMZ DIM Weight', td: 'amz_dim_weight'},
                    {group: 'dimensions', th: 'QTY Per BOx', td:'qty_per_box'},
                    {group: 'dimensions', th: 'BOX Height', td: 'box_height'},
                    {group: 'dimensions', th: 'BOX Length', td: 'box_length'},
                    {group: 'dimensions', th: 'Box Width', td: 'box_width'},
                    {group: 'dimensions', th: 'Box Weight', td: 'box_weight'},
                    {group: 'dimensions', th: 'Box CBM', td:'box_cbm'},
                    {group: 'dimensions', th: 'Boxes Per Cartoon', td: 'boxes_per_cartoon'},
                    {group: 'dimensions', th: 'Cartoon Length', td: 'cartoon_length'},
                    {group: 'dimensions', th: 'Cartoon Width', td: 'cartoon_width'},
                    {group: 'dimensions', th: 'Cartoon Height', td: 'cartoon_height'},
                    {group: 'dimensions', th: 'Cartoon Weight', td: 'cartoon_weight'},
                    {group: 'dimensions', th: 'Cartoon CBM', td: 'cartoon_cbm'},
                    {group: 'dimensions', th: 'Cartoon Per Pallet', td: 'cartoons_per_pallet'},
                    {group: 'dimensions', th: 'Pallet Length', td: 'pallet_length'},
                    {group: 'dimensions', th: 'Pallet Width', td: 'pallet_width'},
                    {group: 'dimensions', th: 'Pallet Height', td: 'pallet_height'},
                    {group: 'dimensions', th: 'Pallet Weight', td: 'pallet_weight'},
                    {group: 'dimensions', th: 'Pallet CBM', td: 'pallet_cbm'},

                    {group: 'quantity_discounts', th: 'QD Type', td: 'qd_type'},
                    {group: 'quantity_discounts', th: 'MOQ', td: 'moq'},
                    {group: 'quantity_discounts', th: 'Unit Cost', td: 'unit_cost'},
                    {group: 'quantity_discounts', th: 'Tiers', td: 'tiers'},
                    {group: 'quantity_discounts', th: 'QTY Calc', td: 'qty_calc'},
                    {group: 'quantity_discounts', th: 'QD Bundled', td: 'qd_bundled'},

                    {group: 'profit_analysis', th: 'Unit Cost', td:'unit_cost'},
                    {group: 'profit_analysis', th: 'Shipping', td: 'shipping'},
                    {group: 'profit_analysis', th: 'Customs', td: 'customs'},
                    {group: 'profit_analysis', th: 'Other PC', td: 'other_pc'},
                    {group: 'profit_analysis', th: 'Total Landed', td: 'total_landed'},
                    {group: 'profit_analysis', th: 'Warehouse Storage', td: 'warehouse_storage'},
                    {group: 'profit_analysis', th: 'Inbound Shipping', td: 'inbound_shipping'},
                    {group: 'profit_analysis', th: 'Other Ic', td: 'other_ic'},
                    {group: 'profit_analysis', th: 'Total Inland', td: 'total_inland'},
                    {group: 'profit_analysis', th: 'FBA', td: 'fba'},
                    {group: 'profit_analysis', th: 'Referal', td: 'referral'},
                    {group: 'profit_analysis', th: 'Storage', td: 'storage'},
                    {group: 'profit_analysis', th: 'Total AMZ Fees', td: 'total_amz_fees'},
                    {group: 'profit_analysis', th: 'Total Costs', td: 'total_costs'},
                    {group: 'profit_analysis', th: 'Price Point', td: 'price_point'},
                    {group: 'profit_analysis', th: 'Total Profit', td: 'total_profit'},
                    {group: 'profit_analysis', th: 'Profit Margin', td: 'profit_margin'},
                    {group: 'profit_analysis', th: 'ROI', td: 'roi'},

                    {group: 'order_settings', th: 'Order Volumn', td: 'order_volume'},
                    {group: 'order_settings', th: 'Reorder Schedule', td: 'reorder_schedule'},

                    { group: 'lead_time', th: 'Transit Type', td: 'transit_type'},
                    { group: 'lead_time', th: 'Po To Prod', td: 'po_to_prod'},
                    { group: 'lead_time', th: 'Production', td: 'production'},
                    { group: 'lead_time', th: 'To Port', td: 'to_port'},
                    { group: 'lead_time', th: 'Transit', td: 'transit'},
                    { group: 'lead_time', th: 'To Warehouse', td: 'to_warehouse'},
                    { group: 'lead_time', th: 'To Amazon', td: 'to_amazon'},
                    { group: 'lead_time', th: 'Safety', td: 'safety'},
                    { group: 'lead_time', th: 'Total Lead Time', td: 'total_lead_time'},
                ],
                rows: @json($products),
                vendors: [],
                suppliers: [],
                currentPage: 1,
                perPage: 20,
                selected: [],
                cols: [],
                selectPage: false,
                selectAll: false,
                filter_country: '',
                filter_min_sku: '',
                filter_max_sku: '',
                filter_country_pinned : false,
                filter_sku_pinned : false,
                showFilterRow: true,
                openedRow: 0,
                lastLocked: 0,
                subTab: 'basic_info',
                product: null,
                product_tmp: {
                    source_type: 0,
                },
                search: '',
                sortGroup:'',
                sortField: '',
                sortDirection: '',
                labels: [],
                label: '',
                isNewLabel: false,
                newLabel: '',

                brands: ['BrandA', 'BrandB'],

                isFilter: true,
                filterLabels: [],
                filterLabel: '',
                filterBrands: [],
                filterBrand: '',

                clicks: 0,
                timer: null,
                inputingRowId:'',
                inputingTd: '',
                inputingVal:'',
                isInputingLabel: false,

                detail_basic_info_nickname: '',
                detail_basic_info_labels: [],
                isAddingLabel: false,
                addingLabel: '',

                detail_sources_multi_sources: false,
                detail_sources_sources: [
                    {
                        type: false, // supplier, true - vendor
                        supplier: '',
                        parts: [
                            {
                                'mpn': '',
                                'qty': '',
                                'shipping_option': ''
                            }
                        ]
                    },

                ],

                detail_sources_multi_parts: false,
                detail_sources_parts: [
                    {
                        'mpn': '',
                        'qty': '',
                        'shipping_option': ''
                    }
                ],

                detail_dimensions_product_size_unit: true,
                detail_dimensions_product_size_l: '',
                detail_dimensions_product_size_w: '',
                detail_dimensions_product_size_h: '',
                detail_dimensions_product_weight_unit: true,
                detail_dimensions_product_weight: '',

                detail_dimensions_packaging_size_unit: true,
                detail_dimensions_packaging_weight_unit: true,
                detail_dimensions_packaging_volume_unit: true,
                detail_dimensions_packaging_qty_per_box_qty: '',
                detail_dimensions_packaging_qty_per_box_size_l: '',
                detail_dimensions_packaging_qty_per_box_size_w: '',
                detail_dimensions_packaging_qty_per_box_size_h: '',
                detail_dimensions_packaging_qty_per_box_weight: '',
                detail_dimensions_packaging_qty_per_box_volume: '',
                detail_dimensions_packaging_box_per_carton_qty: '',
                detail_dimensions_packaging_box_per_carton_size_l: '',
                detail_dimensions_packaging_box_per_carton_size_w: '',
                detail_dimensions_packaging_box_per_carton_size_h: '',
                detail_dimensions_packaging_box_per_carton_weight: '',
                detail_dimensions_packaging_box_per_carton_volume: '',
                detail_dimensions_packaging_carton_per_box_qty: '',
                detail_dimensions_packaging_carton_per_box_size_l: '',
                detail_dimensions_packaging_carton_per_box_size_w: '',
                detail_dimensions_packaging_carton_per_box_size_h: '',
                detail_dimensions_packaging_carton_per_box_weight: '',
                detail_dimensions_packaging_carton_per_box_volume: '',

                detail_quantity_moq_unit: '3000+',
                detail_quantity_moq_price: '6.30',
                detail_quantity_discounts: [{'unit': '4000+', 'price': '6.20'}, {'unit': '5000+', 'price': '6.10'}, {'unit': '6000+', 'price': '6.00'}],

                detail_profit_landed_unit_cost: 2,
                detail_profit_landed_shipping: 2,
                detail_profit_landed_customs: 2,
                detail_profit_landed_other: 2,
                detail_profit_inland_warehouse_storage: 0,
                detail_profit_inland_inbound_shipping: 0,
                detail_profit_inland_other: 0,

                detail_order_volume: '',
                detail_order_type: 'day',
                detail_order_schedule: 'on-demand',
                detail_order_schedule_weekly: '',
                detail_order_schedule_multi_weekly: [],
                detail_order_schedule_monthly: '',
                detail_order_schedule_multi_monthly: [],

                detail_lead_time_type: 'boat',
                detail_lead_time_set_type: 'default',
                detail_lead_time_boat_production: '',
                detail_lead_time_boat_port: '',
                detail_lead_time_boat_transit: '',
                detail_lead_time_boat_warehouse: '',
                detail_lead_time_boat_amazon: '',
                detail_lead_time_boat_po_to_production: '',
                detail_lead_time_boat_safety: '',
                detail_lead_time_plane_production: '',
                detail_lead_time_plane_port: '',
                detail_lead_time_plane_transit: '',
                detail_lead_time_plane_warehouse: '',
                detail_lead_time_plane_amazon: '',
                detail_lead_time_plane_po_to_production: '',
                detail_lead_time_plane_safety: '',

                renderKey: 0,

            },
            created(){
                this.cols = this.fields.map(field => field.td)

                fetch("{{ url('get_data') }}", {
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        _token: '{{ csrf_token() }}',
                        type: 'vendors_list'
                    }),
                })
                .then(response => response.json())
                .then(data => {
                    this.vendors = data.vendors_list
                });

                fetch("{{ url('get_data') }}", {
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        _token: '{{ csrf_token() }}',
                        type: 'suppliers_list'
                    }),
                })
                    .then(response => response.json())
                    .then(data => { this.suppliers = data.suppliers_list });

                fetch('{{ route("get_products_labels")}}')
                    .then(response => response.json())
                    .then(data => { this.labels = data })
            },
            updated(){
            },
            mounted(){
            },
            methods: {
                getData(){
                    fetch("{{ url('search_product') }}/" + this.search)
                    .then(response => response.json())
                    .then(data => {
                        this.rows = data.products
                        console.log(data.products)
                    });
                },
                sortBy(group, field){
                    if(this.sortField == field){
                        this.sortDirection = this.sortDirection == 'asc' ? 'desc' : 'asc'
                    }else{
                        this.sortDirection = 'asc'
                    }

                    this.sortField = field
                    this.sortGroup = group
                },

                columns(){
                    return this.fields.filter((field)=>this.cols.includes(field.td))
                },

                bulkDelete(id){
                    if(confirm('Really Delete?')){
                        fetch('{{ route('products_bulk_delete') }}', {
                            method: 'delete', // or 'PUT'
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                _token: '{{ csrf_token() }}',
                                selected : this.selected
                            }),
                        })
                        .then(response => response.json())
                        .then(data => {
                            this.selected = []
                            this.selectPage = false
                            this.selectAll = false
                            // this.products = data
                        })
                        .catch((error) => {
                            console.error('Error:', error);
                        });
                    }
                },

                bulkExport(){
                    let selected = this.selected;
                    fetch('{{ route('suppliers_bulk_export') }}', {
                        method: 'get', // or 'PUT'
                        headers: {
                            'Content-Type': 'application/json',
                        },
                    })
                        .catch((error) => {
                            console.error('Error:', error);
                        });
                },

                updateSelectPage(){
                    if(this.selectPage) {
                        this.selected = this.processedRows.slice(this.perPage * (this.currentPage - 1), this.perPage * this.currentPage).map( i => i.id)
                    }else{
                        this.selected = []
                    }
                    this.selectAll = false
                },

                updatedSelected(){
                    this.selectAll = false
                    this.selectPage = false
                },

                selectAllItems(){
                    this.selectAll = true
                    this.selected = this.processedRows.map( i => i.id)
                },

                filterData(){
                    console.log('Filter');
                },

                nextPage(){
                    if(this.currentPage < this.lastPage) this.currentPage++ ;
                },

                prevPage(){
                    if(this.currentPage > 1 ) this.currentPage-- ;
                },

                updateCols(){

                },

                clickTableCell(row, group, td, val){
                    this.clicks++;
                    if(this.clicks === 1){
                        this.timer = setTimeout( () => {
                            if(this.inputingRowId){
                            }
                                if(this.openedRow == row.id){
                                    this.openedRow = 0
                                }else{
                                    this.openedRow = row.id
                                    this.product = row
                                    this.detail_basic_info_labels = row.basic_info.labels.map(i => i.text)
                                    this.detail_basic_info_nickname = row.basic_info.nickname

                                    this.detail_dimensions_product_size_l = row.dimensions.item_length
                                    this.detail_dimensions_product_size_w = row.dimensions.item_width
                                    this.detail_dimensions_product_size_h = row.dimensions.item_height
                                    this.detail_dimensions_product_size_unit = row.dimensions.unitofdimensions == "inches" ? false : true
                                    this.detail_dimensions_product_weight = row.dimensions.item_weight
                                    this.detail_dimensions_product_weight_unit = row.dimensions.unitofweight == "pounds" ? false : true

                                    this.detail_dimensions_packaging_qty_per_box_qty = row.dimensions.qty_per_box
                                    this.detail_dimensions_packaging_qty_per_box_size_l = row.dimensions.box_length
                                    this.detail_dimensions_packaging_qty_per_box_size_w = row.dimensions.box_width
                                    this.detail_dimensions_packaging_qty_per_box_size_h = row.dimensions.box_height
                                    this.detail_dimensions_packaging_qty_per_box_weight = row.dimensions.box_weight
                                    this.detail_dimensions_packaging_qty_per_box_volume = row.dimensions.box_cbm

                                    this.detail_dimensions_packaging_box_per_carton_qty = row.dimensions.boxes_per_carton
                                    this.detail_dimensions_packaging_box_per_carton_size_l = row.dimensions.carton_length
                                    this.detail_dimensions_packaging_box_per_carton_size_w = row.dimensions.carton_width
                                    this.detail_dimensions_packaging_box_per_carton_size_h = row.dimensions.carton_height
                                    this.detail_dimensions_packaging_box_per_carton_weight = row.dimensions.carton_weight
                                    this.detail_dimensions_packaging_box_per_carton_volume = row.dimensions.carton_cbm

                                    this.detail_dimensions_packaging_carton_per_box_qty = row.dimensions.cartons_per_pallet
                                    this.detail_dimensions_packaging_carton_per_pallet_size_l = row.dimensions.pallet_length
                                    this.detail_dimensions_packaging_carton_per_pallet_size_w = row.dimensions.pallet_width
                                    this.detail_dimensions_packaging_carton_per_pallet_size_h = row.dimensions.pallet_height
                                    this.detail_dimensions_packaging_carton_per_pallet_weight = row.dimensions.pallet_weight
                                    this.detail_dimensions_packaging_carton_per_pallet_volume = row.dimensions.pallet_cbm
                                }
                                subTab = 'basic_info'
                            this.clicks = 0
                        }, 300)
                    }else{
                        clearTimeout(this.timer)
                        if(td != 'image'){
                            this.inputingRowId = row.id
                            this.inputingGroup = group
                            this.inputingTd = td
                            this.inputingVal = val
                            this.$nextTick(() => {
                                this.$refs['inputing'][0].focus()
                            })
                        }
                        this.clicks = 0
                    }
                },

                focusInput(){
                    this.$refs.inputing.focus();
                },

                applyBulkLabel(){
                    $('#bulkLabelModal').modal('hide')
                    fetch("{{route('products_bulk_label')}}", {
                        method: 'post',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            _token: '{{ csrf_token() }}',
                            selected: this.selected,
                            label: this.label,
                            newLabel: this.newLabel
                        }),
                    })
                    .then(res => {
                        this.newLabel = ''
                        this.label = ''
                    })
                },

                changeFilterLabel(){
                    this.filterLabels.push(this.filterLabel)
                },
                changeFilterBrand(){
                    this.filterBrands.push(this.filterBrand)
                },

                removeFilterLabels(){
                    this.filterLabels = []
                },
                removeFilterBrands(){
                    this.filterBrands = []
                },

                changeInputing(){

                    fetch("{{ url('product_cell_change') }}", {
                        method: 'post',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            _token: '{{ csrf_token() }}',
                            product_id: this.inputingRowId,
                            val: this.inputingVal,
                            group: this.inputingGroup,
                            field: this.inputingTd,
                        })
                    })
                    .then(res => res.json())
                    .then(data => {
                        if(data.success){
                            this.rows.find(i => (i.id == this.inputingRowId))[this.inputingGroup][this.inputingTd] = this.inputingVal
                            this.inputingRowId = null
                        }else{
                            console.log(data)
                        }
                    })
                },

                inputingLabel(e){
                    e.target.style.width = ( e.target.value.length + 1 ) * 8 + 'px'
                    this.isInputingLabel = true
                },

                addedLabel(){
                    fetch("{{ url('product_add_label') }}", {
                        method: 'post',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            _token: '{{ csrf_token() }}',
                            product_id: this.openedRow,
                            label: this.addingLabel,
                            lables: this.detail_basic_info_labels,
                        }),
                    })
                    .then(res => res.json())
                    .then(data => {
                        if(data.success){
                            this.detail_basic_info_labels.push(this.addingLabel)
                            this.isAddingLabel = false
                            this.addingLabel = ''
                        }else{
                            console.log(data)
                        }
                    })
                },

                removeLabel(row, val){
                    fetch("{{ url('product_delete_label') }}", {
                        method: 'post',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            _token: '{{ csrf_token() }}',
                            product_id: row.id,
                            label: val,
                        })
                    })
                    .then(res => res.json())
                    .then(data => {
                        if(data.success){
                            const index = this.detail_basic_info_labels.indexOf(val)
                            this.detail_basic_info_labels.splice(index, 1)
                        }else{
                            console.log(data)
                        }
                    })
                },

                addNewDiscount(){
                    this.detail_quantity_discounts.push({
                        'unit': '',
                        'price': ''
                    })
                },

                changeMultiSourcesCheckbox(){
                    if(this.detail_sources_multi_sources){
                        $('#addSourceModal').modal('show')
                    }
                },

                changeMultiPartsCheckbox(){
                    if(this.detail_sources_multi_parts){
                        $('#addPartModal').modal('show')
                    }
                },

                saveDimensions(row){
                  fetch('{{ url('product_detail_save') }}', {
                      method: 'post',
                      headers: {
                          'Content-Type': 'application/json',
                      },
                      body: JSON.stringify({
                          _token: '{{ csrf_token() }}',
                          tab: 'dimensions',
                          product_id: this.openedRow,
                          product_size_unit: this.detail_dimensions_product_size_unit,
                          product_size_l: this.detail_dimensions_product_size_l,
                          product_size_w: this.detail_dimensions_product_size_w,
                          product_size_h: this.detail_dimensions_product_size_h,
                          product_weight_unit: this.detail_dimensions_product_weight_unit,
                          product_weight: this.detail_dimensions_product_weight,

                          packaging_size_unit: this.detail_dimensions_packaging_size_unit ? 'cm' : 'in',
                          packaging_weight_unit: this.detail_dimensions_packaging_weight_unit ? 'kg' : 'lbs',
                          packaging_volume_unit: this.detail_dimensions_packaging_volume_unit ? 'CBM' : 'CBF',
                          packaging_qty_per_box_qty: this.detail_dimensions_packaging_qty_per_box_qty,
                          packaging_qty_per_box_size_l: this.detail_dimensions_packaging_qty_per_box_size_l,
                          packaging_qty_per_box_size_w: this.detail_dimensions_packaging_qty_per_box_size_w,
                          packaging_qty_per_box_size_h: this.detail_dimensions_packaging_qty_per_box_size_h,
                          packaging_qty_per_box_weight: this.detail_dimensions_packaging_qty_per_box_weight,
                          packaging_qty_per_box_volume: this.detail_dimensions_packaging_qty_per_box_volume,
                          packaging_box_per_carton_qty: this.detail_dimensions_packaging_box_per_carton_qty,
                          packaging_box_per_carton_size_l: this.detail_dimensions_packaging_box_per_carton_size_l,
                          packaging_box_per_carton_size_w: this.detail_dimensions_packaging_box_per_carton_size_w,
                          packaging_box_per_carton_size_h: this.detail_dimensions_packaging_box_per_carton_size_h,
                          packaging_box_per_carton_weight: this.detail_dimensions_packaging_box_per_carton_weight,
                          packaging_box_per_carton_volume: this.detail_dimensions_packaging_box_per_carton_volume,
                          packaging_carton_per_box_qty: this.detail_dimensions_packaging_carton_per_box_qty,
                          packaging_carton_per_box_size_l: this.detail_dimensions_packaging_carton_per_pallet_size_l,
                          packaging_carton_per_box_size_w: this.detail_dimensions_packaging_carton_per_pallet_size_w,
                          packaging_carton_per_box_size_h: this.detail_dimensions_packaging_carton_per_pallet_size_h,
                          packaging_carton_per_box_weight: this.detail_dimensions_packaging_carton_per_pallet_weight,
                          packaging_carton_per_box_volume: this.detail_dimensions_packaging_carton_per_pallet_volume,

                      })
                  })
                },

                saveBasicInfo(row){
                    fetch('{{ url('product_detail_save') }}', {
                        method: 'post',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            _token: '{{ csrf_token() }}',
                            tab: 'basic_info',
                            product_id: this.openedRow,
                            nickname: this.detail_basic_info_nickname,
                            labels: this.detail_basic_info_labels,
                        })
                    })
                },

                saveQuantityDiscounts(row){
                    fetch('{{ url('product_detail_save') }}', {
                        method: 'post',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            _token: '{{ csrf_token() }}',
                            tab: 'quantity_discounts',
                            product_id: this.openedRow,
                            moq_quantity_unit: this.detail_quantity_moq_unit,
                            moq_quantity_price: this.detail_quantity_moq_price,
                            quantity_discounts: this.detail_quantity_discounts,
                        })
                    })
                },

                saveProfitAnalysis(row){
                    fetch('{{ url('product_detail_save') }}', {
                        method: 'post',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            _token: '{{ csrf_token() }}',
                            tab: 'profit_analysis',
                            product_id: this.openedRow,
                            landed_total : this.detail_profit_landed_total,
                            landed_unit_cost : this.detail_profit_landed_unit_cost,
                            landed_shipping : this.detail_profit_landed_shipping,
                            landed_customs : this.detail_profit_landed_customs,
                            landed_other : this.detail_profit_landed_other,
                            inland_total : this.detail_profit_inland_total,
                            inland_warehouse_storage : this.detail_profit_inland_warehouse_storage,
                            inland_inbound_shipping : this.detail_profit_inland_inbound_shipping,
                            inland_other : this.detail_profit_inland_other,
                            amazon_total : this.detail_profit_amazon_total,
                            fba : this.product.profit_analysis.fba,
                            referral: this.product.profit_analysis.referral,
                            storage: this.product.profit_analysis.storage,
                            price_point: this.product.price_point,
                            total_profit: this.detail_profit_total_profit,
                            profit_margin: this.product.profit_analysis.profit_margin,
                            roi: this.product.profit_analysis.roi,
                        })
                    })
                },

                saveOrderSetting(row){
                    fetch('{{ url('product_detail_save') }}', {
                        method: 'post',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            _token: '{{ csrf_token() }}',
                            tab: 'order_setting',
                            product_id: this.openedRow,
                            order_volume: this.detail_order_volume,
                            order_type: this.detail_order_type,
                            order_schedule: this.detail_order_schedule,
                            order_schedule_weekly: this.detail_order_schedule_weekly,
                            order_schedule_multi_weekly: this.detail_order_schedule_multi_weekly,
                            order_schedule_monthly: this.detail_order_schedule_monthly,
                            order_schedule_multi_monthly: this.detail_order_schedule_multi_monthly,
                        })
                    })
                },

                saveLeadTime(row){
                    fetch('{{ url('product_detail_save') }}', {
                        method: 'post',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            _token: '{{ csrf_token() }}',
                            tab: 'lead_time',
                            product_id: this.openedRow,
                            lead_time_type:this.detail_lead_time_type,
                            lead_time_set_type:this.detail_lead_time_set_type,
                            lead_time_boat_production:this.detail_lead_time_boat_production,
                            lead_time_boat_port:this.detail_lead_time_boat_port,
                            lead_time_boat_transit:this.detail_lead_time_boat_transit,
                            lead_time_boat_warehouse:this.detail_lead_time_boat_warehouse,
                            lead_time_boat_amazon:this.detail_lead_time_boat_amazon,
                            lead_time_boat_po_to_production:this.detail_lead_time_boat_po_to_production,
                            lead_time_boat_safety:this.detail_lead_time_boat_safety,
                            lead_time_plane_production:this.detail_lead_time_plane_production,
                            lead_time_plane_port:this.detail_lead_time_plane_port,
                            lead_time_plane_transit:this.detail_lead_time_plane_transit,
                            lead_time_plane_warehouse:this.detail_lead_time_plane_warehouse,
                            lead_time_plane_amazon:this.detail_lead_time_plane_amazon,
                            lead_time_plane_po_to_production:this.detail_lead_time_plane_po_to_production,
                            lead_time_plane_safety:this.detail_lead_time_plane_safety,
                        })
                    })
                },
            },

            computed: {
                lastPage(){
                    return Math.floor( this.processedRows.length / this.perPage) + ( this.processedRows.length % this.perPage ? 1 : 0 )
                },

                processedRows: function(){
                    // let filtered = this.rows.filter(item => {
                    //     let props = Object.values(item)
                    //     console.log(props)

                    //     // return props.some(prop => !this.search || ((typeof prop === 'string') ? prop.includes(this.search) : false))
                    //     return props.some(prop => !this.search || ((typeof prop === 'string') ? prop.includes(this.search) : prop.toString(10).includes(this.search)))
                    // })
                    // .filter(item => {
                    //     if(this.inComplete)  return !item.completeFlag;
                    //     return true;
                    // })

                    return this.rows.sort((a,b) => {
                        let modifier = 1;
                        if(this.sortDirection){
                            if(this.sortDirection === 'desc') modifier = -1;
                            if(a[this.sortGroup][this.sortField] < b[this.sortGroup][this.sortField]) return -1 * modifier;
                            if(a[this.sortGroup][this.sortField] > b[this.sortGroup][this.sortField]) return 1 * modifier;
                        }
                        return 0;
                    });
                },

                detail_profit_landed_total(){
                    return Number(this.detail_profit_landed_unit_cost) + Number(this.detail_profit_landed_shipping) + Number(this.detail_profit_landed_customs) + Number(this.detail_profit_landed_other);
                },

                detail_profit_inland_total(){
                    return Number(this.detail_profit_inland_warehouse_storage) + Number(this.detail_profit_inland_inbound_shipping) + Number(this.detail_profit_inland_other)
                },

                detail_profit_amazon_total(){
                    return Number(this.product.profit_analysis.fba.slice(1)) + Number(this.product.profit_analysis.referral.slice(1)) + Number(this.product.profit_analysis.storage.slice(1))
                },

                detail_profit_total(){
                    return this.detail_profit_landed_total + this.detail_profit_inland_total + this.detail_profit_amazon_total
                },

                detail_profit_total_profit(){
                    return this.product.profit_analysis.price_point.slice(1) - this.detail_profit_total 
                }
            }
        })

        $(function() {
            let d = $(document)
            let scrollbar = $('#scrollbar')
            let thumb = $('#thumb')
            let track = $('#track')
            let tableContainer = $('.sticky-table-container')
            let table = $('table')
            let tabList = $('#tablist')
            const gripsContainer = $('.grips')
            let resizableTh = null

            function initScrollbar() {
                scrollbar.width($('.page-content').innerWidth())
                const scrollRatio = tableContainer.width() / table.outerWidth()
                thumb.width(scrollRatio * 100 + '%')

                let pos = {x: 0, left: 0}

                const mouseDownThumbHandler = function (e) {
                    pos = {
                        x: e.clientX,
                        left: tableContainer.scrollLeft()
                    }

                    thumb.addClass('grabbing');
                    d.find('body').addClass('grabbing');

                    d.on('mousemove', mouseMoveHandler);
                    d.on('mouseup', mouseUpHandler)
                }

                const mouseMoveHandler = function (e) {
                    const dx = e.clientX - pos.x
                    tableContainer.scrollLeft(pos.left + dx / scrollRatio)
                }

                const mouseUpHandler = function (e) {
                    thumb.removeClass('grabbing');
                    d.find('body').removeClass('grabbing');

                    d.off('mousemove', mouseMoveHandler);
                    d.off('mouseup', mouseUpHandler);
                };

                const trackClickHandler = function (e) {
                    const percentage = (e.clientX - track.offset().left) / track.width()
                    // c.scrollLeft(percentage * (t.width() - c.width()))
                }

                // $(c).on('scroll', scrollContentHandler)
                $(thumb).on('mousedown', mouseDownThumbHandler)
                $(track).on('click', trackClickHandler)
            }

            initScrollbar()
            $(window).resize(initScrollbar);
            $('#vertical-menu-btn').click(initScrollbar)

            tableContainer.on('scroll', function () {
                window.requestAnimationFrame(function () {
                    thumb.css('left', tableContainer.scrollLeft() * 100 / table.outerWidth() + '%')
                })

                const position = $(this).scrollLeft()
                let start = 0

                for (const group in groups) {
                    start = table.find('th.' + group + ':visible').not('.locked').position().left
                    // start = table.find('th.' + group + ':visible').not('.locked').position().left - lockedWidth()
                    if (start - 100 < position && position < start + 100) {
                        tabList.find('li').removeClass('active')
                        tabList.find('#' + group + '_tab').addClass('active')
                        break;
                    }
                }
            })

            $('#tablist li').on('click', function () {
                $('#tablist li').removeClass('active')
                $(this).addClass('active')
                const left = $('th.' + $(this)[0].id.replace('_tab', '') +':visible').not('.locked').position().left - lockedWidth()
                // const left = $('th.' + $(this)[0].id.replace('_tab', '') + ':visible').not('.locked').position().left
                tableContainer.scrollLeft(left)
            })

            function lockedWidth(){
                let total = 0
                $('th.locked').each(function() {
                    total += parseInt($(this).outerWidth());
                });
                return total
            }

            function moveColumn(current, target) {
                table.find('tr').each(function (i, row) {
                    const cells = $(row).children()
                    cells.eq(target).after(cells.eq(current))
                })
            }

            function onClickLockableThead(e) {
                e.preventDefault()
                e.stopPropagation()
                let prevLastLockedCellIndex = $('th.locked').length ? $('th').index($('th.locked').last()) : 0
                const th = $(this).parent()
                let currentCellIndex = $('th').index(th)
                // table.find('[data-col-id=' + th.data('colId') + ']').toggleClass('locked')
                table.find('tr').each(function (i, row) {
                    $($(row).children().eq(currentCellIndex)).toggleClass('locked')
                })
                if (th.hasClass('locked')) {
                    ((currentCellIndex - 1) > prevLastLockedCellIndex) && moveColumn(currentCellIndex, prevLastLockedCellIndex)
                } else {
                    th.css('left', '');
                    (currentCellIndex < prevLastLockedCellIndex) && moveColumn(currentCellIndex, prevLastLockedCellIndex)
                }
                syncShadow()
            }

            function syncShadow(){
                $('.cellShadow').remove()
                const colId = $('th.locked').length ? $('th').index($('th.locked').last()) : 0
                const shadow = $('<div class="cellShadow"></div>')
                table.find('tr > :nth-child(' + (colId + 1) + ')').append(shadow)

                $(".sticky-table-container table tr").each( function () {
                    offset = 0;
                    $(this).find(".locked").each( function () {
                        $(this).css('left', offset);
                        offset += $(this).outerWidth();
                    })
                })
            }

            $('th span.locking').on('click', onClickLockableThead )
                .on('mousedown', function(e){ e.stopPropagation() })

            d.on('mousedown', '.grip', function(e){
                $(this).addClass('dragging')

                resizableTh = $(this).parent()
                resizableTh.ox = e.pageX
                resizableTh.ow = $(this).parent().width()
                resizableTh.otw = table.outerWidth()

                d.on('mousemove', function(e){
                    let offset = e.pageX - resizableTh.ox
                    resizableTh.width(resizableTh.ow + offset)
                    table.css('min-width', resizableTh.otw + offset)
                })

                d.on('mouseup', function(e){
                    d.off('mousemove').off('mouseup')
                    if(!resizableTh) return
                    $('.dragging').removeClass('dragging')
                    resizableTh = null
                })
            })
        })
    </script>
@endsection

@extends('layouts.master')

@section('title') Inventory List @endsection

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18"> Inventory</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active"> Inventory</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
        @if(session()->has('message'))
            {!! session('message') !!}
        @endif

    <!-- end row -->
    <div class="card marketplace_wise_data">
        <input type="hidden" value="{{$marketplace}}" name="marketplaces" id="marketplaces">
        <div class="card-body">
                <h4 class="card-title mb-4">Inventory List</h4>
            <div class="table-responsive">
                <table id="datatables" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead class="thead-light">
                        <tr>
                            <th>No.</th>
                            <th>SKU</th>
                            <th>Quantity Available</th>
                            <th>Afn-fulfilled-quantity</th>
                            <th>Current Stock</th>
                            <th>FC Transfer</th>
                            <th>FC Processing</th>
                            <th>Customer Order</th>
                            <th>Reserved Qty</th>
                            <th>Afn Research qty</th>
                            <th>Afn Unsellable qty</th>
                            <th>Afn-warehouse-qty</th>
                            <th>Amazon Inbound Receving Qty</th>
                            <th>Amazon Inbound Shipment Qty</th>
                            <th>Amazon Inbound Working Qty</th>
                            <th>AFN Total Qty</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>        

@endsection
@section('script')
<script>
    $(document).ready(function () {
        var id = $('#marketplaces').val();
        loadDashboard(id);
    });
    function loadDashboard(id){
        var count=1;
        var insert_type = 'get_inventory';
        var table = $('#datatables').DataTable({
            "order": [ 0, 'asc' ],
            "bSort": true,
            "paging": true,
            "bInfo": true,
            "bDestroy": true,
            "bFilter": true,
            "searching": true,
            "bPaginate": true,
            "bProcessing": true,
            "language": {
                "loadingRecords": '&nbsp;',
                "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
            },
            'ajax': {
                "type": "POST",
                "url": "{{ route('mws_inventory.store') }}",
                "data": function (d) {
                    d._token= "{{csrf_token()}}",d.id=id,d.insert_type=insert_type
                },
                "dataType": 'json',
                "dataSrc": "",
                "timeout":1000000,
                "async": true,
                "cache": true
            },
            'columnDefs': [
                {
                    targets: 0,
                    render: function (data, type, row) {
                        return row['id'];
                    }
                },
                {
                    targets: 1,
                    render: function (data, type, row) {
                        return row['sku'];
                    }
                },
                {
                    targets: 2,
                    render: function (data, type, row) {
                        return row['qty_available'];
                    }
                },
                {
                    targets: 3,
                    render: function (data, type, row) {
                        return row['afn_qty'];

                    }
                },
                {
                    targets: 4,
                    render: function (data, type, row) {
                        return row['current_stock'];
                    }
                },
                {
                    targets: 5,
                    render: function (data, type, row) {
                        return row['fc_transfer'];
                    }
                },
                {
                    targets: 6,
                    render: function (data, type, row) {
                        return row['fc_processing'];
                    }
                },
                {
                    targets: 7,
                    render: function (data, type, row) {
                        return row['customer_order']
                    }
                },
                {
                    targets: 8,
                    render: function (data, type, row) {
                        return row['reserved_qty']
                    }
                },
                {
                    targets: 9,
                    render: function (data, type, row) {
                        return row['afn_research_qty']
                    }
                },
                {
                    targets: 10,
                    render: function (data, type, row) {
                        return row['afn_sellable_qty']
                    }
                },
                {
                    targets: 11,
                    render: function (data, type, row) {
                        return row['afn_warehouse_qty']
                    }
                },
                {
                    targets: 12,
                    render: function (data, type, row) {
                        return row['amz_rece_inbound_qty']
                    }
                },
                {
                    targets: 13,
                    render: function (data, type, row) {
                        return row['amz_ship_inbound_qty']
                    }
                },
                {
                    targets: 14,
                    render: function (data, type, row) {
                        return row['amz_working_inbound_qty']
                    }
                },
                {
                    targets: 15,
                    render: function (data, type, row) {
                        return row['afn_total_qty']
                    }
                }
            ]
        });
    }
    </script>
@endsection

<div class="card marketplace_wise_data">
    <div class="card-body">
        <h4 class="card-title mb-4">Inventory List</h4>
        <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap users-datatable" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                <thead class="thead-light">
                <tr>
                    <th>No.</th>
                    <th>Condition</th>
                    <th>Total Supply Quantity</th>
                    <th>Fnsku</th>
                    <th>Instock Supply Quantity</th>
                    <th>Asin</th>
                    <th>SKU</th>
                </tr>
                </thead>
                <tbody>
                <?php $no = 1; ?>
                    @if(!empty($inventory_details))
                        @foreach($inventory_details as $users)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$users['condition']}}</td>
                                <td>{{strlen($users['total_supply_quantity'])}}</td>
                                <td>{{$users['fnsku']}}</td>
                                <td>{{$users['instock_supply_quantity']}}</td>
                                <td>{{$users['asin']}}</td>
                                <td>{{$users['sellersku']}}</td>
                            </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
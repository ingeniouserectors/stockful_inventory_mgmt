@extends('layouts.master')

@section('title') Settings @endsection

@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Settings</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Settings</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
          @if(session()->has('message'))
             {!! session('message') !!}
            @endif
            <div class="card">
                <div class="card-body">
                    <form name="crate-user" id="crate-user" action="{{ route('setting.store') }}" method="POST" onreset="myFunction()">
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-4">
                                <div class="custom-control custom-checkbox mb-2">
                                    <?php (isset($detail['registration_setting']) && $detail['registration_setting']=='1') ? $reg_check="checked" : $reg_check=""; ?>
                                    <input type="checkbox" class="custom-control-input" id="registration" name="registration" value="1" {{ $reg_check }}>
                                    <label class="custom-control-label" for="registration">Registration</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="custom-control custom-checkbox mb-2">
                                    <?php (isset($detail['email_settings'])  && $detail['email_settings']=='1')? $mail_check="checked" : $mail_check=""; ?>
                                    <input type="checkbox" class="custom-control-input" id="email" name="email" value="1" {{ $mail_check }}>
                                    <label class="custom-control-label" for="email">Email</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="custom-control custom-checkbox mb-2">
                                    <?php (isset($detail['forget_password_settings'])  && $detail['forget_password_settings']=='1') ? $forgot_check="checked" : $forgot_check=""; ?>
                                    <input type="checkbox" class="custom-control-input" id="forgot_password" name="forgot_password" value="1" {{ $forgot_check }}>
                                    <label class="custom-control-label" for="forgot_password">Forget password</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <div class="custom-control custom-checkbox mb-2">
                                    <?php (isset($detail['mws_settings'])  && $detail['mws_settings']=='1')? $mws_check="checked" : $mws_check=""; ?>
                                    <input type="checkbox" class="custom-control-input" id="mws" name="mws" value="1" {{ $mws_check }}>
                                    <label class="custom-control-label" for="mws">Mws</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="custom-control custom-checkbox mb-2">
                                    <?php (isset($detail['maintenance_mode']) && $detail['maintenance_mode']=='1') ? $main_check="checked" : $main_check=""; ?>
                                    <input type="checkbox" class="custom-control-input" id="maintenance" name="maintenance" value="1" {{ $main_check }}>
                                    <label class="custom-control-label" for="maintenance">Maintenance</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="custom-control custom-checkbox mb-2">
                                    <?php (isset($detail['affiliate_settings'])  && $detail['affiliate_settings']=='1')? $aff_check="checked" : $aff_check=""; ?>
                                    <input type="checkbox" class="custom-control-input" id="affiliate" name="affiliate" value="1" {{ $aff_check }}>
                                    <label class="custom-control-label" for="affiliate">Affiliate</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <div class="custom-control custom-checkbox mb-2">
                                    <?php (isset($detail['support_ticket_settings'])  && $detail['support_ticket_settings']=='1')? $support_check="checked" : $support_check=""; ?>
                                    <input type="checkbox" class="custom-control-input" id="support_ticket" name="support_ticket" value="1" {{ $support_check }}>
                                    <label class="custom-control-label" for="support_ticket">Support ticket</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fetch_month" class="col-md-4 col-form-label">Historical fetch number months</label>
                            <div class="col-md-4">
                                <input class="form-control allow_integer" type="text" value="{{ $detail['historical_fetch_number_months'] }}" name="fetch_month" id="fetch_month" maxlength="3">
                            </div>
                        </div>

                        <div class="button-items mt-3">
                            <input class="btn btn-info" type="submit" value="Submit" id="submit">
                            <a class="btn btn-danger waves-effect waves-light" href="{{ url('/index') }}" role="button">Cancel</a>
                            <input class="btn btn-warning" type="reset" value="Reset">
                        </div>

                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->

@endsection

@section('script')
   
@endsection
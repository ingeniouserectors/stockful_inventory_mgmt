   
    <!-- end page title -->
    <div class="row">
        <div class="col-lg-12">
            <span class="error_message"></span>
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Inbound Shipment List</h4>
                    <div class="table-responsive">
                        <table id="datatables" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead class="thead-light">
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Shipment ID </th>
                                <th>Shipment Status</th>
                                <th>Sent</th>
                                <th>Received</th>
                            </tr>
                            </thead>
                            <tbody>

                <?php $no = 1; ?>
                @if(!empty($inbound_shipment_data))
                    @foreach ($inbound_shipment_data as $inc=>$inbound)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $inbound['inbound_shipment']['ShipmentName']}}</td>
                            <td>{{ $inbound['inbound_shipment']['ShipmentId']}}</td>
                            <td>{{$inbound['inbound_shipment']['ShipmentStatus']}}</td>
                            <td>{{$inbound['QuantityShipped']}}</td>
                            <td>{{$inbound['QuantityReceived']}}</td>
                        </tr>

                    @endforeach
                 @else
                    <td colspan="6" align="center">
                        No Record found.
                    </td>
                @endif
                </tbody>
                        </table>
                    </div>
                    <!-- end table-responsive -->
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
@section('script')
    <script>
        $(function () {
        $('#datatables').DataTable({
            "pageLength": 10,
            'autoWidth': false,
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                      
            
            ]
        });
    });
          

    </script>
    
@endsection

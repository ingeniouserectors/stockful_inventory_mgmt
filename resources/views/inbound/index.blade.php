@extends('layouts.master')

@section('title')  Inbound List @endsection

@section('content')
    <style>
        .error{
            border-color: red;
        }

    </style>
  <style>
    .success-icon::before{
    content: '+';    
    height: 14px;
    width: 14px;
    display: block;
    position: absolute;
    color: white;
    border: 2px solid white;
    border-radius: 14px;
    box-shadow: 0 0 3px #444;
    box-sizing: content-box;
    text-align: center;
    text-indent: 0 !important;
    font-family: 'Times New Roman', Times, serif;
    line-height: 14px;


    
    background-color: #0275d8;}
        .failer-icon::after{
     content: '-';       
    height: 14px;
    width: 14px;
    display: block;
    position: absolute;
    color: white;
    border: 2px solid white;
    border-radius: 14px;
    box-shadow: 0 0 3px #444;
    box-sizing: content-box;
    text-align: center;
    text-indent: 0 !important;
    font-family: 'Courier New', Courier, monospace;
    line-height: 14px;       
    
    background-color: #d33333;
        }
        td .fancybox-button{background: rgba(222, 216, 216, 0.6);display: inline-block; width: 120px; height: auto; margin: 5px !important;}
        td .fancybox-button[css-attr="video"]{width: 220px;}
        .tdname span{margin:0;padding:5px 0 0 0;}
        .table-news-image{height:100px; width:100px; object-fit:cover; display:inline-block; padding:5px;}
        .table-news-text{max-height:200px; overflow:auto;}
    </style>  
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18"> Inbound</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active"> Inbound</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <input type="hidden" value="{{$marketplace}}" name="marketplaces" id="marketplaces">
    <!-- end page title -->
    <div class="row">
        <div class="col-lg-12">
            <span class="error_message"></span>
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4"> Inbound List</h4>
                    <div class="table-responsive">
                        <table id="datatables" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead class="thead-light">
                            <tr>
                                <th></th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Price</th>
                                <th>SKU</th>
                                <th>ASIN</th>
                               <!--  <th>Action</th> -->
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- end table-responsive -->
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            var id = $('#marketplaces').val();
            
            loadDashboard(id);
        });
       

        function loadDashboard(id){
            var count=1;
            var product_list = 'product_list';
            var table = $('#datatables').DataTable({

                "order": [ 0, 'asc' ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('inbound.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.id=id,d.request_type=product_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                'columnDefs': [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return row[''];
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return row['prod_image'];
                        }
                    },
                    {
                        targets: 2,
                        render: function (data, type, row) {
                            var result = row['prod_name'].substring(0, 25);
                            pro_name = '<span title="'+row['prod_name']+'">+result+</span>';
                            return row['prod_name'];
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, row) {
                            return row['your_price'];

                        }
                    },
                    {
                        targets: 4,
                        render: function (data, type, row) {
                            return row['sku'];
                        }
                    },
                    {
                        targets: 5,
                        render: function (data, type, row) {
                            return row['asin'];
                        }
                    },
                ]
            });
        }

        

    </script>
    <script>
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).on('click','.view_media',function(){
        var prod_id = $(this).attr('data-val');
        var $obj = $(this);
        $tr = $(this).parents('tr');
        var data_class = $("#row_"+prod_id).find("i").hasClass('success-icon');
        console.log(data_class);
        if(data_class === true) {
            if (prod_id != '') {
                var inbound_shipment_list = 'inbound_shipment_list';
                $.ajax({
                    type: "POST",
                    url: '{{ route('inbound.store') }}',
                    data:{prod_id:prod_id,request_type:inbound_shipment_list},
                    dataType: "json",
                    beforeSend: function () {
                        $(".view_media").find("i").removeClass("success-icon").addClass("failer-icon");
                        $(".view_media").find("i").removeClass("success-icon").addClass("failer-icon");
                        $(".view_media").find("i").not($obj.find("i")).removeClass("fa-minus").addClass("success-icon");
                        $(".view_media").find("i").not($obj.find("i")).removeClass("failer-icon").addClass("success-icon");
                        $('.newstr').remove();
                    },
                    success: function (res) {

                        $("#wait").css("display", "none");
                        if (typeof res.error != 'undefined' && res.error == 0) {
                            $tr.after('<tr class="media_' + prod_id + ' newstr"><td colspan="6">' + res.view + '</td><tr>');
                            $obj.find("i").addClass("failer-icon");
                            $obj.find("i").removeClass("success-icon");

                            $obj.find("i").addClass("failer-icon");
                            $obj.find("i").removeClass("success-icon");

                        } else if (typeof res.error != 'undefined' && res.error == 1) {
                            alert(res.msg);
                        }
                    }
                });
            }
        }else{
            $("#row_").find("i").hasClass('success-icon');
            $obj.find("i").addClass("success-icon");
            $obj.find("i").removeClass("failer-icon");
            $obj.find("i").addClass("success-icon");
            $obj.find("i").removeClass("failer-icon");
            $(".media_"+prod_id).toggle();
        }
    });

    </script>
@endsection

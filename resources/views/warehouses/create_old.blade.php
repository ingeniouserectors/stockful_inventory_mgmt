@extends('layouts.master')

@section('title') Create Warehouse @endsection

@section('content')
@php($country_list=get_country_list());


    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Create Warehouse</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('warehouses.index')}}">Warhouse</a></li>
                        <li class="breadcrumb-item active">Create Warehouse</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">
                <div class="card-body">

                    <form name="create-warehouse" id="create-warehouse" action="{{route('warehouses.store')}}" method="POST" onreset="myFunction()">
                        @csrf
                        <input type="hidden" name="insert_type" value="create_form">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Type</label>
                            <div class="col-md-4">
                                <select class="custom-select" name="type" id=type>
                                    <option value="1" @if(['type']=='1') selected @endif selected>Self</option>
                                    <option value="2" @if(['type']=='2') selected @endif>Third Party</option>
                                </select>
                            </div>
                            <input type="hidden" name="marketplace_id" id="marketplace_id" value="{{session('MARKETPLACE_ID')}}">

                            <label for="example-text-input" class="col-md-2 col-form-label city">Warehouse Name</label>
                            <div class="col-md-4 city">
                                <input class="form-control" type="text" name="warehouse_name" id="warehouse_name">
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Address line 1</label>
                            <div class="col-md-4 city">
                                <input class="form-control" type="text" name="address_line_1" id="address_line_1">
                            </div>
                            <label for="example-email-input" class="col-md-2 col-form-label">Address line 2</label>
                            <div class="col-md-4">
                                <input class="form-control" type="text" name="address_line_2">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label city">Country</label>
                            <div class="col-md-4 country">
                                <select name="country" id="country" class="form-control">
                                    <option value="">Select</option>
                                    @foreach($country_list as  $key=> $country)
                                        <option value=" {{ $key }}">{{ $key."(".$country.")" }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <label for="example-tel-input"  class="col-md-2 col-form-label phone">States</label>
                            <div class="col-md-4 state">
                                <select name="state" id="state" class="form-control">
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-tel-input"  class="col-md-2 col-form-label country">City</label>
                            <div class="col-md-4 country">
                                <select name="city" id="city" class="form-control">
                                </select>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label state">Zip Code</label>
                            <div class="col-md-4 state">
                                <input class="form-control allow_integer" type="text" name="zipcode" id="zipcode" maxlength="6" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-tel-input"  class="col-md-2 col-form-label primary_first_name">Primary Fistname</label>
                            <div class="col-md-4 primary_first_name">
                                <input  class="form-control" type="text" name="primary_first_name" >
                            </div>

                            <label for="example-text-input" class="col-md-2 col-form-label primary_last_name">Primary Lastname</label>
                            <div class="col-md-4 primary_last_name">
                                <input class="form-control" type="text" name="primary_last_name" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-tel-input"  class="col-md-2 col-form-label primary_email">Primary Email</label>
                            <div class="col-md-4 primary_email">
                                <input  class="form-control" type="email" name="primary_email">
                            </div>
                        </div>

                        <div class="button-items mt-3">
                            <input class="btn btn-info" type="submit" value="Submit" id="submit">
                            <a class="btn btn-danger waves-effect waves-light" href="{{ route('warehouses.index') }}" role="button">Cancel</a>
                            <input class="btn btn-warning" type="reset" value="Reset">
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->

@endsection

@section('script')
<script>

    $(document).ready(function(){
        $("select").change(function(){
            check();
        }).change();
        check();
    });
    
    function check() {
      var data= $('#type').val();
         if(data=="1")  {
             $(".primary_first_name").hide();
             $(".primary_last_name").hide();
             $(".primary_email").hide();
             $('.primary_first_name').rules('remove',  {
                 required : true,
                 messages : { required : 'Firstname is required' }
             });
             $('.primary_last_name').rules('remove',  {
                 required : true,
                 messages : { required : 'Lastname is required' }
             });
             $('.primary_email').rules('remove',  {
                 required : true,
                 messages : { required : 'Email is required' }
             });
             
         } 
         else 
         {
             
             $(".primary_first_name").show();
             $(".primary_last_name").show();
             $(".primary_email").show();

                 $('.primary_first_name').rules('add',  {
                     required : true,
                     messages : { required : 'Firstname number is required' }
                 });
                 $('.primary_last_name').rules('add',  {
                     required : true,
                     messages : { required : 'Lastname is required' }
                 });
                 $('.primary_email').rules('add',  {
                     required : true,
                     messages : { required : 'Email is required' }
                 });
         }
    } 
        
    </script>
<script type="text/javascript">
    
  $('#country').change(function(){
    var country = $(this).val();
    var request_type = 'Get_all_state';
    if(country){
        $.ajax({
           type:"POST",
           url:"{{route('warehouses.store')}}",
           data:{country:country,request_type:request_type},
           success:function(res){               
            if(res){
                $("#state").empty();
                $("#state").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#state").append('<option value="'+key+'">'+key+'('+value+')'+'</option>');
                });
           
            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
        $("#city").empty();
    }      
   });
    $('#state').on('change',function(){
    var state = $(this).val();
    var request_type = 'Get_all_city';
    if(state){
        $.ajax({
           type:"POST",
           url:"{{ route('warehouses.store') }}",
           data:{state:state,request_type:request_type},
           success:function(res){               
            if(res){
          console.log(res);
                $("#city").empty();
                $("#city").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#city").append('<option value="'+key+'">'+key+'('+value+')'+'</option>');
                });
           
            }else{
               $("#city").empty();
            }
           }
        });
    }else{
        $("#city").empty();
    }
        
   });    
</script>    
@endsection
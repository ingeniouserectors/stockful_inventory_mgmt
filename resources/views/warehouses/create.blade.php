@extends('layouts.master-new')

@section('title') Create Warehouse @endsection

@section('content')
@php($country_list=get_country_list())

            <div class="suppliers-top-main-box">
                <span id="messages"> </span>
                <div class="row">
                    <div class="col-md-4">
                        <div class="suppliers-first-column">
                            <h2>Warehouses</h2>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <div class="suppliers-first-column-two text-left text-md-right">
                            <button type="button" class="btn import-supplier-btn mx-3"><span><svg
                                            xmlns="http://www.w3.org/2000/svg" width="13.406" height="13.406"
                                            viewBox="0 0 13.406 13.406">
                                                <g id="Icon_feather-upload" data-name="Icon feather-upload"
                                                   transform="translate(-3.5 -3.5)">
                                                    <path id="Path_316" data-name="Path 316"
                                                          d="M15.906,22.5v2.535A1.267,1.267,0,0,1,14.639,26.3H5.767A1.267,1.267,0,0,1,4.5,25.035V22.5"
                                                          transform="translate(0 -10.396)" fill="none" stroke="#a6b0cf"
                                                          stroke-linecap="round" stroke-linejoin="round"
                                                          stroke-width="2" />
                                                    <path id="Path_317" data-name="Path 317"
                                                          d="M16.837,7.668,13.668,4.5,10.5,7.668"
                                                          transform="translate(-3.465)" fill="none" stroke="#a6b0cf"
                                                          stroke-linecap="round" stroke-linejoin="round"
                                                          stroke-width="2" />
                                                    <path id="Path_318" data-name="Path 318" d="M18,4.5v7.6"
                                                          transform="translate(-7.797)" fill="none" stroke="#a6b0cf"
                                                          stroke-linecap="round" stroke-linejoin="round"
                                                          stroke-width="2" />
                                                </g>
                                            </svg>
                                        </span> Import Supplier</button>
                            <button type="button" class="btn add-new-supplier"><span><svg
                                            xmlns="http://www.w3.org/2000/svg" width="11.25" height="11.25"
                                            viewBox="0 0 11.25 11.25">
                                                <g id="Icon_ionic-ios-add-circle-outline"
                                                   data-name="Icon ionic-ios-add-circle-outline"
                                                   transform="translate(-3.125 -3.125)">
                                                    <path id="Path_308" data-name="Path 308"
                                                          d="M15.6,12.85H13.677V10.925a.413.413,0,0,0-.827,0V12.85H10.925a.4.4,0,0,0-.413.413.4.4,0,0,0,.413.413H12.85V15.6a.4.4,0,0,0,.413.413.411.411,0,0,0,.413-.413V13.677H15.6a.413.413,0,1,0,0-.827Z"
                                                          transform="translate(-4.514 -4.514)" fill="#fff" stroke="#fff"
                                                          stroke-width="0.5" />
                                                    <path id="Path_309" data-name="Path 309"
                                                          d="M8.75,4.1A4.65,4.65,0,1,1,5.46,5.46,4.621,4.621,0,0,1,8.75,4.1m0-.724A5.375,5.375,0,1,0,14.125,8.75,5.374,5.374,0,0,0,8.75,3.375Z"
                                                          fill="#fff" stroke="#fff" stroke-width="0.5" />
                                                </g>
                                            </svg>
                                        </span> Add New Supplier</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="suppliers-main-box suppliers-main-box-scroll">
                <div class="row">
                    <div class="col-lg-12">
                        <nav class="suppliers-nav-box">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Basic Details</a>
                                <a class="nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Contacts</a>
                                <a class="nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-setting" role="tab" aria-controls="nav-setting" aria-selected="false">Settings</a>
                            </div>
                        </nav>
                        <div class="row">
                            <div class="col-lg-10 pt-5">
                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                         aria-labelledby="nav-home-tab">
                                        <form class="main-address-from vendors-tab-01">
                                            <div class="suppliers-address-box">
                                                <ul
                                                        class="d-flex flex-row align-items-center justify-content-between pb-3">
                                                    <li>Warehouse type</li>
                                                    <li>
                                                        <div class="form-group">
                                                            <select class="custom-select" id=type>
                                                                <option value="1" @if(['type']=='1') selected @endif selected>Self</option>
                                                                <option value="2" @if(['type']=='2') selected @endif>Third Party</option>
                                                            </select>
                                                        </div>
                                                        <!-- <div class="form-group">
                                                            <input type="text" class="form-control"
                                                                placeholder="Enter Supplier Name">
                                                        </div> -->
                                                    </li>
                                                </ul>
                                            </div>
                                            <input type="hidden" id="warehouse_id" value="">
                                            <input type="hidden" id="marketplace_id" value="{{session('MARKETPLACE_ID')}}">
                                            <div class="suppliers-address-box">
                                                <ul
                                                        class="d-flex flex-row align-items-center justify-content-between pb-3">
                                                    <li>Warehouse Name</li>
                                                    <li>
                                                        <!-- <div class="form-group">
                                                            <select class="form-control"
                                                                id="exampleFormControlSelect1">
                                                                <option>Select Country</option>
                                                                <option>Bangladesh</option>
                                                                <option>USA</option>
                                                                <option>India</option>
                                                                <option>Pakistan</option>
                                                            </select>
                                                        </div> -->
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" id="warehouse_name" required placeholder="Enter Warehouse Name" required>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="suppliers-address-box">
                                                <ul
                                                        class="d-flex flex-row align-items-center justify-content-between pb-3">
                                                    <li>Country</li>
                                                    <li>
                                                        <div class="form-group country">
                                                            <select name="country" id="country" class="form-control" required>
                                                                <option value="">Select Country</option>
                                                                @foreach($country_list as  $key=> $country)
                                                                    <option value=" {{ $key }}">{{ $key."(".$country.")" }}</option>
                                                                @endforeach
                                                            </select>

                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="suppliers-address-box">
                                                <ul
                                                        class="d-flex flex-row align-items-center justify-content-between pb-3">
                                                    <li>State</li>
                                                    <li>
                                                        <div class="form-group state">
                                                            <select name="state" id="state" class="form-control" required>
                                                            </select>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="suppliers-address-box">
                                                <ul
                                                        class="d-flex flex-row align-items-center justify-content-between pb-3">
                                                    <li>City</li>
                                                    <li>
                                                        <div class="form-group city">
                                                            <select name="city" id="city" class="form-control" required>
                                                            </select>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="suppliers-address-box">
                                                <ul
                                                        class="d-flex flex-row align-items-center justify-content-between pb-3">
                                                    <li>Postal Code (optional) </li>
                                                    <li>
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="zipcode" id="zipcode" maxlength="6" placeholder="Enter Postal Code" required>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="suppliers-address-box">
                                                <ul
                                                        class="d-flex flex-row align-items-start justify-content-between pb-3">
                                                    <li>Address (optional) </li>
                                                    <li>
                                                        <div class="form-group pb-3">
                                                            <input class="form-control" type="text" name="address_line_1" id="address_line_1" placeholder="Enter Address Line">
                                                        </div>
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="address_line_2" id="address_line_2" placeholder="Enter Address Line Two">
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>

                                            <ul class="d-flex flex-row justify-content-between">
                                                <li>
                                                    <button type="submit" class="btn address-cancel-btn">Cancel</button>
                                                </li>
                                                <li>
                                                    <button type="button" class="btn btn-primary save_datas" data-val="1">Save</button>
                                                    <button type="button" class="btn address-save-btn save_datas" data-val="2">Save And Next</button>
                                                </li>
                                            </ul>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="nav-profile" role="tabpanel"
                                         aria-labelledby="nav-profile-tab">
                                        <div class="suplier-table-box">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th scope="col" width="20%">First Name <img
                                                                src="assets/img/table-up-down-two.png" alt="">
                                                    </th>
                                                    <th scope="col" width="20%">Last Name <img
                                                                src="assets/img/table-up-down-two.png" alt="">
                                                    </th>
                                                    <th scope="col" width="20%">
                                                        <ul
                                                                class="d-flex flex-row align-items-center table-checkbox-main">
                                                            <li>
                                                                <span>Title</span>
                                                                <img src="assets/img/table-up-down.png"
                                                                     alt="">
                                                            </li>
                                                        </ul>
                                                    </th>
                                                    <th scope="col" width="20%">Email <span
                                                                style="color: #227CFF">[?]</span> <img
                                                                src="assets/img/table-up-down-two.png" alt="">
                                                    </th>
                                                    <th scope="col" width="20%">Phone Number <img
                                                                src="assets/img/table-up-down-two.png" alt="">
                                                    </th>
                                                </tr>
                                                </thead>
                                            </table>
                                            <div class="card">
                                                <div class="card-body">
                                                    <div id="table" class="table-editable">
                                                        <div class="table-text-selector table-add">
                                                            <p>Click to add first contact <a href="#"><span
                                                                            class="mr-1 ml-2">
                                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                                     width="11.25" height="11.25"
                                                                                     viewBox="0 0 11.25 11.25">
                                                                                    <g id="Icon_ionic-ios-add-circle-outline"
                                                                                       data-name="Icon ionic-ios-add-circle-outline"
                                                                                       transform="translate(0.25 0.25)">
                                                                                        <path id="Path_308"
                                                                                              data-name="Path 308"
                                                                                              d="M15.6,12.85H13.677V10.925a.413.413,0,0,0-.827,0V12.85H10.925a.4.4,0,0,0-.413.413.4.4,0,0,0,.413.413H12.85V15.6a.4.4,0,0,0,.413.413.411.411,0,0,0,.413-.413V13.677H15.6a.413.413,0,1,0,0-.827Z"
                                                                                              transform="translate(-7.889 -7.889)"
                                                                                              fill="#227cff"
                                                                                              stroke="#227cff"
                                                                                              stroke-width="0.5" />
                                                                                        <path id="Path_309"
                                                                                              data-name="Path 309"
                                                                                              d="M8.75,4.1A4.65,4.65,0,1,1,5.46,5.46,4.621,4.621,0,0,1,8.75,4.1m0-.724A5.375,5.375,0,1,0,14.125,8.75,5.374,5.374,0,0,0,8.75,3.375Z"
                                                                                              transform="translate(-3.375 -3.375)"
                                                                                              fill="#227cff"
                                                                                              stroke="#227cff"
                                                                                              stroke-width="0.5" />
                                                                                    </g>
                                                                                </svg>
                                                                            </span><i style="font-style: normal;">Add
                                                                        Contacts</i></a></p>
                                                        </div>
                                                        <table class="table table-striped vendors-tab-rows">
                                                            <tbody>
                                                            <tr>
                                                                <input type="hidden" id="warehouse_contact_id" value="">
                                                                <th scope="row" width="20%"
                                                                    contenteditable="true">
                                                                    <ul
                                                                            class="d-flex flex-row align-items-center table-checkbox-main">
                                                                        <li class="first_name">
                                                                            Josh
                                                                        </li>
                                                                    </ul>
                                                                </th>
                                                                <td contenteditable="true" width="20%" class="last_name">
                                                                    Bochner</td>
                                                                <td contenteditable="true" width="20%" class="title">
                                                                    Title 2</td>
                                                                <td contenteditable="true" width="20%" class="emails">
                                                                    joshbochner@gmail.com
                                                                </td>
                                                                <td width="20%" contenteditable="true">
                                                                    <ul
                                                                            class="d-flex justify-content-between align-items-center">
                                                                        <li><span class="phone_number">+00 - 1234567890</span>
                                                                        </li>
                                                                        <li class="delete-btn-vendors-tab">
                                                                            <i class="fas fa-trash-alt"></i>
                                                                        </li>
                                                                    </ul>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <div class="table-add my-3">
                                                            <ul class="table-footer-add-btn">
                                                                <li>
                                                                    <p><a href="#">
                                                                                    <span class="mr-1 ml-2">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                                             width="11.25" height="11.25"
                                                                                             viewBox="0 0 11.25 11.25">
                                                                                            <g id="Icon_ionic-ios-add-circle-outline"
                                                                                               data-name="Icon ionic-ios-add-circle-outline"
                                                                                               transform="translate(0.25 0.25)">
                                                                                                <path id="Path_308"
                                                                                                      data-name="Path 308"
                                                                                                      d="M15.6,12.85H13.677V10.925a.413.413,0,0,0-.827,0V12.85H10.925a.4.4,0,0,0-.413.413.4.4,0,0,0,.413.413H12.85V15.6a.4.4,0,0,0,.413.413.411.411,0,0,0,.413-.413V13.677H15.6a.413.413,0,1,0,0-.827Z"
                                                                                                      transform="translate(-7.889 -7.889)"
                                                                                                      fill="#227cff"
                                                                                                      stroke="#227cff"
                                                                                                      stroke-width="0.5" />
                                                                                                <path id="Path_309"
                                                                                                      data-name="Path 309"
                                                                                                      d="M8.75,4.1A4.65,4.65,0,1,1,5.46,5.46,4.621,4.621,0,0,1,8.75,4.1m0-.724A5.375,5.375,0,1,0,14.125,8.75,5.374,5.374,0,0,0,8.75,3.375Z"
                                                                                                      transform="translate(-3.375 -3.375)"
                                                                                                      fill="#227cff"
                                                                                                      stroke="#227cff"
                                                                                                      stroke-width="0.5" />
                                                                                            </g>
                                                                                        </svg>
                                                                                    </span><i
                                                                                    style="font-style: normal;">Add
                                                                                Contacts</i>
                                                                        </a>
                                                                    </p>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <ul
                                                            class="d-flex flex-row justify-content-between address-btn-box">
                                                        <li>
                                                            <button type="submit"
                                                                    class="btn address-cancel-btn">Cancel</button>
                                                        </li>
                                                        <li>
                                                            <button type="button"
                                                                    class="btn btn-primary save_contacts">Save</button>
                                                            <button type="submit"
                                                                    class="btn address-save-btn save_contacts">Save And
                                                                Next</button>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- Editable table -->
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="nav-setting" role="tabpanel"
                                         aria-labelledby="nav-setting-tab">
                                        <div class="setting-vendors-form">
                                            <div class="row">
                                                <div class="suppliers-logistics-agent">
                                                    <ul class="d-flex flex-row align-items-center suppliers-logistics-agent-box">
                                                        <li class="pr-2">
                                                            <span>Lead Time</span>
                                                        </li>
                                                        <li class="ml-3 suppliers-logistics-agent-li">
                                                            <div class="form-group">
                                                                <select class="form-control" id="lead_time">
                                                                    <option value="">Enter Days</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                </select>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <ul class="d-flex flex-row justify-content-between pt-5">
                                                        <li>
                                                            <button type="submit" class="btn address-cancel-btn">Cancel</button>
                                                        </li>
                                                        <li>
                                                            <button type="button" class="btn btn-primary data_settings" data-val="1">Save</button>
                                                            <button type="button" class="btn address-save-btn ml-2 data_settings" data-val="2">Save And Next</button>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection

@section('script')
<script>

    $(document).on("click",".save_contacts",function(){
        var first_names = [];
        var last_names = [];
        var titles = [];
        var emailss = [];
        var phone_numbers = [];
        var warehouse_contact_ids = [];

        $("table > tbody > tr").each(function( index, element ) {
            //datass[index] = [];
            var first_name = $(this).find(".first_name").text();
            var last_name = $(this).find(".last_name").text();
            var title = $(this).find(".title").text();
            var emails = $(this).find(".emails").text();
            var phone_number = $(this).find(".phone_number").text();
            var warehouse_contact_id = $(this).find(".warehouse_contact_id").val();

            first_names. push($.trim(first_name));
            last_names. push($.trim(last_name));
            titles. push($.trim(title));
            emailss. push($.trim(emails));
            phone_numbers. push($.trim(phone_number));
            warehouse_contact_ids. push($.trim(warehouse_contact_id));
        });

        var warehouse_id = $("#warehouse_id").val();
        var insert_type = 'insert_contacts';

        if(warehouse_id == ''){
            $(".tab-pane").removeClass('show active');
            $(".nav-link").removeClass('active');
            $("#nav-home-tab").addClass('active');
            $("#nav-home").addClass('show active');
        }else {

            $.ajax({
                type: "POST",
                url: "{{route('warehouses.store')}}",
                data: {
                    warehouse_id: warehouse_id,
                    first_names: first_names,
                    last_names: last_names,
                    emails: emailss,
                    insert_type: insert_type,
                    warehouse_contact_id: warehouse_contact_ids
                },
                success: function (res) {
                    if (res.error == 0) {
                        $("#messages").html(res.message);
                        //$(".vendors_contact_id").val(res.vendors_contact_id);
                        $(".tab-pane").removeClass('show active');
                        $(".nav-link").removeClass('active');
                        $("#nav-contact-tab").addClass('active');
                        $("#nav-setting").addClass('show active');
                    } else {
                        $("#messages").html(res.message);
                    }
                }
            });
        }
    });


    $(document).on('click','.save_datas',function(){
        var types = $(this).data('val');
        var type = $("#type").val();
        var warehouse_name = $('#warehouse_name').val();
        var country = $('#country').val();
        var state = $('#state').val();
        var city = $('#city').val();
        var zipcode = $('#zipcode').val();
        var address_line_1 = $('#address_line_1').val();
        var address_line_2 = $('#address_line_2').val();
        var marketplace_id = $("#marketplace_id").val();
        var warehouse_id = $("#warehouse_id").val();
        var insert_type = 'insert_update_basic_details';
        $.ajax({
            type:"POST",
            url:"{{route('warehouses.store')}}",
            data:{warehouse_id:warehouse_id,marketplace_id:marketplace_id,type:type,warehouse_name:warehouse_name,country:country,state:state,city:city,zipcode:zipcode,address_line_1:address_line_1,address_line_2:address_line_2,insert_type:insert_type},
            success:function(res){
                if(res.error == 0){
                    $("#messages").html(res.message);
                    $("#warehouse_id").val(res.warehouse_id);
                    if(types == 1){
                    }else{
                        $(".tab-pane").removeClass('show active');
                        $(".nav-link").removeClass('active');
                        if(type == 1){
                            $("#nav-contact-tab").addClass('active');
                            $("#nav-setting").addClass('show active');
                        }else{
                            $("#nav-profile-tab").addClass('active');
                            $("#nav-profile").addClass('show active');
                        }
                    }
                }else{
                    $("#messages").html(res.message);
                }
            }
        });

    });

    $(document).on('click','.data_settings',function(){
        var types = $(this).data('val');
        var type = $("#type").val();
        var warehouse_id = $("#warehouse_id").val();
        var lead_time = $("#lead_time").val();
        var insert_type = 'insert_update_setting';
        if(warehouse_id == ''){
            $(".tab-pane").removeClass('show active');
            $(".nav-link").removeClass('active');
            $("#nav-home-tab").addClass('active');
            $("#nav-home").addClass('show active');
        }else{
            $.ajax({
                type:"POST",
                url:"{{route('warehouses.store')}}",
                data:{warehouse_id:warehouse_id,type:type,insert_type:insert_type,lead_time:lead_time},
                success:function(res){
                    if(res.error == 0){
                        $("#messages").html(res.message);
                        $("#warehouse_id").val(res.warehouse_id);
                        if(types == 1){
                        }else{
                            window.location.href = "{{route('warehouses.index')}}"
                        }
                    }else{
                        $("#messages").html(res.message);
                    }
                }
            });
        }
    });

    $(document).on('click','.delete-btn-vendors-tab',function(){
        var insert_type = 'delete_vendors_contact';
        var rand_text = makeid(5);
        var vendors_contact_id = $(this).find(".vendors_contact_id").val();
        $(this).closest('tr').addClass(rand_text);
        //var vendors_contact_id = $('.'+rand_text).find(".vendors_contact_id").val();
        //$(this).closest('tr').attr('data-random',vendors_contact_id);

        {{--if(vendors_contact_id != ''){--}}
            {{--$.ajax({--}}
                {{--type:"POST",--}}
                {{--url:"{{route('vendors.store')}}",--}}
                {{--data:{vendors_contact_id:vendors_contact_id,insert_type:insert_type },--}}
                {{--success:function(res){--}}
                    {{--$("."+rand_text).remove();--}}
                {{--}--}}
            {{--});--}}
        {{--}else{--}}
            $("."+rand_text).remove();
        // }
    });

    function makeid(length) {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    $(document).ready(function(){
        var type = $("#type").val();
        if(type == 1){
            $('#nav-profile-tab').hide();
        }else{
            $('#nav-profile-tab').show();
        }
    });
    $(document).on('change','#type',function(){
        var type = $(this).val();
        if(type == 1){
            $('#nav-profile-tab').hide();
        }else{
            $('#nav-profile-tab').show();
        }
    });

//    $(document).ready(function(){
//        $("select").change(function(){
//            check();
//        }).change();
//        check();
//    });
//
    function check() {
      var data= $('#type').val();
         if(data=="1")  {
             $(".primary_first_name").hide();
             $(".primary_last_name").hide();
             $(".primary_email").hide();
             $('.primary_first_name').rules('remove',  {
                 required : true,
                 messages : { required : 'Firstname is required' }
             });
             $('.primary_last_name').rules('remove',  {
                 required : true,
                 messages : { required : 'Lastname is required' }
             });
             $('.primary_email').rules('remove',  {
                 required : true,
                 messages : { required : 'Email is required' }
             });
             
         } 
         else 
         {
             
             $(".primary_first_name").show();
             $(".primary_last_name").show();
             $(".primary_email").show();

                 $('.primary_first_name').rules('add',  {
                     required : true,
                     messages : { required : 'Firstname number is required' }
                 });
                 $('.primary_last_name').rules('add',  {
                     required : true,
                     messages : { required : 'Lastname is required' }
                 });
                 $('.primary_email').rules('add',  {
                     required : true,
                     messages : { required : 'Email is required' }
                 });
         }
    } 
        
    </script>
<script type="text/javascript">
    
  $('#country').change(function(){
    var country = $(this).val();
    var request_type = 'Get_all_state';
    if(country){
        $.ajax({
           type:"POST",
           url:"{{route('warehouses.store')}}",
           data:{country:country,request_type:request_type},
           success:function(res){               
            if(res){
                $("#state").empty();
                $("#state").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#state").append('<option value="'+key+'">'+key+'('+value+')'+'</option>');
                });
           
            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
        $("#city").empty();
    }      
   });
    $('#state').on('change',function(){
    var state = $(this).val();
    var request_type = 'Get_all_city';
    if(state){
        $.ajax({
           type:"POST",
           url:"{{ route('warehouses.store') }}",
           data:{state:state,request_type:request_type},
           success:function(res){               
            if(res){
          console.log(res);
                $("#city").empty();
                $("#city").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#city").append('<option value="'+key+'">'+key+'('+value+')'+'</option>');
                });
           
            }else{
               $("#city").empty();
            }
           }
        });
    }else{
        $("#city").empty();
    }
        
   });    
</script>    
@endsection
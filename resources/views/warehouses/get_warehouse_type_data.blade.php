<div class="card marketplace_wise_data">
    <div class="card-body">
        <h4 class="card-title mb-4">Warhouse List</h4>
        <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                <thead class="thead-light">
                <tr>
                    <th>No.</th>
                    <th>Warehouse Name</th>
                    <th>Address</th>
                    <th>State</th>
                    <th>Zip Code</th>
                    <th>Country</th>
                    <?php if($type == 2){?>
                    <th>Name</th>
                    <th>Email</th>
                    <?php }?>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $no = 1; ?>

                @if(!empty($warhouse_details))
                    @foreach($warhouse_details as $users)

                     <?php      
                $cnname = "";
                    $state_name = "";
                    $city_name = "";
                    if(isset($users['country_id']) && $users['country_id'] != ""){ 
                       $cnname = get_country_fullname($users['country_id']); 
                    }
                    if(isset($users['state']) && $users['state'] != "" && (isset($users['country_id'])) && $users['country_id'] != ""){ 
                       $state_name = get_state_fullname($users['country_id'],$users['state']); 
                    }

                    if(isset($users['city']) && $users['city'] != "" && (isset($users['state'])) && $users['state'] != "" && (isset($users['country_id'])) && $users['country_id'] != ""){ 
                       $city_name = get_city_fullname($users['country_id'],$users['state'],$users['city']); 
                    }

                    ?>
                        <tr>
                            <td>{{$no++}}</td>
                            <td><a href="{{ route('warehouses.show',$users['id']) }}">{{$users['warehouse_name']}}</a></td>
                            <td>{{$users['address_line_1']}} {{$users['address_line_2'] != '' ? ', '.$users['address_line_2'] : ''}}{{$users['city'] != '' ? ','.$city_name : '' }}</td>
                            <td>{{$state_name}}</td>
                            <td>{{$users['zipcode']}}</td>
                            <td>{{$cnname}}</td>
                            <?php if($type == 2){ ?>
                            <td>{{$users['primary_first_name'] }} {{ $users['primary_last_name']}}</td>
                            <td>{{$users['primary_email']}}</td>
                            <?php } ?>
                            <td>
                                <a class="btn btn-primary btn-sm btn-rounded waves-effect waves-light" href="{{route('warehouses.edit',$users['id']) }}" role="button"><i class="fas fa-edit"></i></a>
                                
                                    <button type="submit" id="button" data-id="{{$users['id']}}" class="btn btn-danger btn-sm btn-rounded waves-effect waves-light"><i class="fas fa-trash-alt"></i></button>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr><td colspan="10" align="center">No record found.</td></tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@section('script')
<script>
$(document).on('click', '#button', function () {
    //e.preventDefault();
    var $ele = $(this).parent().parent();
    var id = $(this).data('id');
            var url = "{{URL('warehouses')}}";
            var destroyurl = url+"/"+id;


    swal({
             title: "Are you sure?",
            text: "You want to delete this record",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
             
            $.ajax({
                type: "DELETE",
                url:destroyurl, 
                data:{ _token:'{{ csrf_token() }}'},
                dataType: "html",
                success: function (data) {
                    var dataResult = JSON.parse(data);
                if(dataResult.statusCode==200){
                    $ele.fadeOut().remove();
                               swal({
              title: "Done!",
              text: "It was succesfully deleted!",
              type: "success",
              timer: 700
           });
    
                      
                    } 
        }

         });
          }  
    
        else
        {
             swal("Cancelled", "", "error");
        }
            
       
    });

});
</script>

@endsection
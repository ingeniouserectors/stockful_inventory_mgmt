<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<h2>Reset Password</h2>

<div>
    Password Reset request at {{ config('app.name') }}.
    Please follow the link below to verify your email address
    <a href=" {{ URL::to('password/reset/' . $confirmation_code) }}"> Confirm Email </a>.<br/>

</div>
</body>
</html>
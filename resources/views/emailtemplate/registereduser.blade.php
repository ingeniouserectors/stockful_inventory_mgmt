<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Verify Your Email Address</h2>

<div>
    Thanks for creating an account with the {{ config('app.name') }}.
    Please follow the link below to verify your email address
    <a href=" {{ URL::to('auth/verify/' . $confirmation_code) }}"> Confirm Email </a>.<br/>
    @if(!empty($user_email) && !empty($user_password))
        Email - {{ $user_email }}<br/>
        Password - {{ $user_password }}<br/>
    @endif
</div>

</body>
</html>
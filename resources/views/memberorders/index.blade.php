@extends('layouts.master')

@section('title') Order List @endsection

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Order</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Order</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <a class="btn btn-info waves-effect waves-light mb-3"  href="{{ route('memberorder.show','import') }}" role="button"><i class="fa fa-download"></i> Download CSV File</a>
        </div>
        <div class="col-lg-7">
            <form action="{{ route('memberorder.store') }}" method="post" id="import_csv"  enctype="multipart/form-data">
                {{ csrf_field() }}
                <label>Import CSV file : *</label>
                <input type="hidden" name="insert_type" value="upload_csv">
                <input type="file" name="uploadFile" accept=".csv">
                <input type="submit" class="btn btn-primary import_csv" value="Import CSV File">
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">

            </div>
        </div>
    </div>
    <!-- end row -->
    <div class="card marketplace_wise_data">
        <div class="card-body">
                <h4 class="card-title mb-4">Order List</h4>
            <input type="hidden" value="{{$marketplace}}" name="marketplaces" id="marketplaces">
            <div class="table-responsive">
                <table id="datatables" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead class="thead-light">
                        <tr>
                            <th>No.</th>
                            <th>Amazon Order ID</th>
                            <th>Purchase Date</th>
                            <th>Product Name</th>
                            <th>SKU</th>
                            <th>ASIN</th>
                            <th>Quantity</th>
                            <th>Item Price</th>
                            <th>Ship State</th>
                            <th>Ship Country</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>        

@endsection
@section('script')
    <script>
        $(document).ready(function () {
            var id = $('#marketplaces').val();
            loadDashboard(id);
        });
        function loadDashboard(id){
            var count = 1;
            var table = $('#datatables').DataTable({
                "order": [ 0, 'asc' ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('memberorder.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.id=id
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },

                'columnDefs': [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return row['amazon_order_id'];
                        }
                    },
                    {
                        targets: 2,
                        render: function (data, type, row) {
                            var purchase = row['purchase_date'];
                            var newPurchase = purchase.split('T')[0];
                            return newPurchase;
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, row) {
                            var result = row['product_name'].substring(0, 25)+'...';
                            pro_name = '<span title="'+row['product_name']+'">'+result+'</span>';
                            //return row['product_name'];
                            return pro_name;
                        }
                    },
                    {
                        targets: 4,
                        render: function (data, type, row) {
                            return row['sku'];
                        }
                    },
                    {
                        targets: 5,
                        render: function (data, type, row) {
                            return row['asin'];
                        }
                    },
                    {
                        targets: 6,
                        render: function (data, type, row) {
                            return row['quantity'];
                        }
                    },
                    {
                        targets: 7,
                        render: function (data, type, row) {
                            return row['item_price'];
                        }
                    },
                    {
                        targets: 8,
                        render: function (data, type, row) {
                            return row['ship_state'];
                        }
                    },
                    {
                        targets: 9,
                        render: function (data, type, row) {
                            return row['ship_country'];
                        }
                    }
                ]
            });
        }
    </script>
@endsection
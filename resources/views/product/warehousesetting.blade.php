@extends('layouts.master')

@section('title') warhouse Setting @endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18"> Warhouse Setting</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('products.index')}}"> Products</a></li>
                        <li class="breadcrumb-item active">Warhouse settings</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="row">
        <div class="col-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">
                <div class="card-body">     
                    <div class="form-group row">
                         <div class="col-md-2">
                             <img width="100px" src="{{$product_data['prod_image']}}" title="{{$product_data['mws_product']['prod_names']}}">
                         </div>
                         <div class="col-md-10">
                             {{ $product_data['prod_name']}}
                             <hr>
                             {{ $product_data['sku']}}
                         </div>
                    </div>
                    <form name="product-warehouse-set" id="product-warehouse-set" action="{{route('products.store')}}" method="POST" onreset="myFunction()">
                        @csrf
                         
                        <input type="hidden" name="insert_type" value="warehose_form">
                      <input type="hidden" name="warehouse_edit_id" value="{{isset($warehouse_setting['id']) ? $warehouse_setting['id'] : ''}}">  <input type="hidden" name="product_id" id="product_id" value="{{$product_id}}">          
                        <div class="vendors_form">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Select Warehouse : </label>
                                <div class="col-md-10">
                                    <select class="custom-select" name="warehouses_id" id="warehouses_id">
                                        <option value="">Select Warehouse</option>
                                            @foreach($warehouse as $users)
                                                <option value="{{$users['id']}}" {{$users['id'] == $warehouse_setting['warehouse_id'] ? 'selected="selected"' : '' }} >{{$users['warehouse_name']}}</option>
                                             @endforeach
                                    </select>
                                </div>
                            </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Qty</label>
                            <div class="col-md-4">
                                <input class="form-control allow_integer" type="text" name="qty" id="qty" value=" {{ isset($warehouse_setting['qty']) ? $warehouse_setting['qty'] : '' }}" maxlength="3">
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">Lead Time</label>
                            <div class="col-md-4">
                                <input class="form-control allow_integer" type="text" name="lead_time" id="lead_time" value=" {{ isset($warehouse_setting['lead_time']) ? $warehouse_setting['lead_time'] : '' }}" maxlength="3">
                            </div>
                        </div>
                         <div class="button-items mt-3">
                            <input class="btn btn-info" type="submit" value="Submit" id="submit">
                            <a class="btn btn-danger waves-effect waves-light" href="{{ route('products.index') }}" role="button">Cancel</a>
                            <input class="btn btn-warning" type="reset" value="Reset">
                        </div>
                            
                    </form>

                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->

@endsection

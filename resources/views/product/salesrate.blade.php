@extends('layouts.master')

@section('title')Projected Reorder for product @endsection
@section('content')


    <style>
        .loader {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('/assets/images/loader.gif') 50% 50% no-repeat rgb(249,249,249);
            opacity: .8;
        }
        .adjust
        {
            float:right;
             margin-top: -16px;
        }
    </style>

    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Projected Reorder for product</h4>
                <?php $previous_urls = str_replace(url('/'), '', url()->previous())?>
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('products.index')}}"> {{$previous_urls == '/reorder' ? 'Reorder' : 'Mws Products' }}</a></li>
                        <li class="breadcrumb-item active">Projected Reorder for product</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
                <div class="loader" hidden></div>
            <span class="error_message"></span>
            <div class="card">
                <div class="card-body">

                    <form name="order_logic" id="order_logic" action="javascript:void(0)" method="POST" onreset="myFunction()">
                        @csrf
                        <input type="hidden" name="insert_type" value="label_add_sales_calculation">
                        <input type="hidden" name="product_id" value="{{Request::segment(2)}}">
                        <input type="hidden" name="user_marketplace_id" id="user_marketplace_id" value="{{session('MARKETPLACE_ID')}}">
                        <input type="hidden" name="product_assign" id="product_assign" value="">
                         <div class="form-group row">

                                 <div class="col-md-2">
                                     <img width="100px" src="{{$product_sales_rate['mws_product']['prod_image']}}" title="{{$product_sales_rate['mws_product']['prod_names']}}">
                                 </div>
                                 <div class="col-md-10">
                                     {{ $product_sales_rate['mws_product']['prod_name']}}
                                     <hr>
                                     {{ $product_sales_rate['mws_product']['sku']}}
                                 </div>

                         </div>

                        <br><br>

                        <div class="form-group row logic_label_name">
                            <label for="example-text-input" class="col-md-2 col-form-label">Logic Label Name</label>
                            <div class="col-md-4">
                                <input class="form-control" type="text" name="logic_label_name" id="logic_label_name">
                            </div>
                        </div>
                        <b>Supply History in Percentage(%)</b><br/><br/>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Last Year</label>
                            <div class="col-md-4">
                                <input type="range" name="supply_last_year_sales_percentage" id="supply_last_year_sales_percentage" value="0" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage supply_last_year_sales_percentage adjust">0.00%</span>
                            </div>
                        </div>
                        <b>Supply Recent Sales in Percentage(%)</b><br/><br/>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Last 7 Days</label>
                            <div class="col-md-4">
                                <input type="range" name="supply_recent_last_7_day_sales_percentage" id="supply_recent_last_7_day_sales_percentage" value="{{$data['supply_recent_last_7_day_sales_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage supply_recent_last_7_day_sales_percentage adjust">0.00%</span>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">Last 14 Days</label>
                            <div class="col-md-4">
                                <input type="range" name="supply_recent_last_14_day_sales_percentage" id="supply_recent_last_14_day_sales_percentage" value="{{$data['supply_recent_last_14_day_sales_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage supply_recent_last_14_day_sales_percentage adjust">0.00%</span>
                            </div>
                        </div>

                        <div class="form-group row">

                            <label for="example-text-input" class="col-md-2 col-form-label">Last 30 Days </label>
                            <div class="col-md-4">
                                <input type="range" name="supply_recent_last_30_day_sales_percentage" id="supply_recent_last_30_day_sales_percentage" value="{{$data['supply_recent_last_30_day_sales_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage supply_recent_last_30_day_sales_percentage adjust">0.00%</span>
                            </div>
                        </div>

                        <b>Supply Trends in Percentage(%)</b><br/><br/>


                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Year Over Year Historical</label>
                            <div class="col-md-4">
                                <input type="range" name="supply_trends_year_over_year_historical_percentage" id="supply_trends_year_over_year_historical_percentage" value="{{$data['supply_trends_year_over_year_historical_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage supply_trends_year_over_year_historical_percentage adjust">0.00%</span>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">Year Over Year Current</label>
                            <div class="col-md-4">
                                <input type="range" name="supply_trends_year_over_year_current_percentage" id="supply_trends_year_over_year_current_percentage" value="{{$data['supply_trends_year_over_year_current_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage supply_trends_year_over_year_current_percentage adjust">0.00%</span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Multi Month</label>
                            <div class="col-md-4">
                                <input type="range" name="supply_trends_multi_month_trend_percentage" id="supply_trends_multi_month_trend_percentage" value="{{$data['supply_trends_multi_month_trend_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage supply_trends_multi_month_trend_percentage adjust">0.00%</span>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">30 Over 30 Days</label>
                            <div class="col-md-4">
                                <input type="range" name="supply_trends_30_over_30_days_percentage" id="supply_trends_30_over_30_days_percentage" value="{{$data['supply_trends_30_over_30_days_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage supply_trends_30_over_30_days_percentage adjust">0.00%</span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Multi Week </label>
                            <div class="col-md-4">
                                <input type="range" name="supply_trends_multi_week_trend_percentage" id="supply_trends_multi_week_trend_percentage" value="{{$data['supply_trends_multi_week_trend_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage supply_trends_multi_week_trend_percentage adjust">0.00%</span>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">7 Over 7 Days </label>
                            <div class="col-md-4">
                                <input type="range" name="supply_trends_7_over_7_days_percentage" id="supply_trends_7_over_7_days_percentage" value="{{$data['supply_trends_7_over_7_days_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage supply_trends_7_over_7_days_percentage adjust">0.00%</span>
                            </div>
                        </div>

                        <b>Reorder History in Percentage(%)</b><br/><br/>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Last Year</label>
                            <div class="col-md-4">
                                <input type="range" name="reorder_last_year_sales_percentage" id="reorder_last_year_sales_percentage" value="{{$data['reorder_last_year_sales_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage reorder_last_year_sales_percentage adjust">0.00%</span>
                            </div>
                        </div>
                        <b>Reorder Recent Sales in Percentage(%)</b><br/><br/>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Last 7 Days</label>
                            <div class="col-md-4">
                                <input type="range" name="reorder_recent_last_7_day_sales_percentage" id="reorder_recent_last_7_day_sales_percentage" value="{{$data['reorder_recent_last_7_day_sales_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage reorder_recent_last_7_day_sales_percentage adjust">0.00%</span>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">Last 14 Days</label>
                            <div class="col-md-4">
                                <input type="range" name="reorder_recent_last_14_day_sales_percentage" id="reorder_recent_last_14_day_sales_percentage" value="{{$data['reorder_recent_last_14_day_sales_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage reorder_recent_last_14_day_sales_percentage adjust">0.00%</span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Last 30 Days </label>
                            <div class="col-md-4">
                                <input type="range" name="reorder_recent_last_30_day_sales_percentage" id="reorder_recent_last_30_day_sales_percentage" value="{{$data['reorder_recent_last_30_day_sales_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage reorder_recent_last_30_day_sales_percentage adjust">0.00%</span>
                            </div>
                        </div>

                        <b>Reorder Trends in Percentage(%)</b><br/><br/>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Year Over Year Historical</label>
                            <div class="col-md-4">
                                <input type="range" name="reorder_trends_year_over_year_historical_percentage" id="reorder_trends_year_over_year_historical_percentage" value="{{$data['reorder_trends_year_over_year_historical_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage reorder_trends_year_over_year_historical_percentage  adjust">0.00%</span>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">Year Over Year Current</label>
                            <div class="col-md-4">
                                <input type="range" name="reorder_trends_year_over_year_current_percentage" id="reorder_trends_year_over_year_current_percentage" value="{{$data['reorder_trends_year_over_year_current_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage reorder_trends_year_over_year_current_percentage adjust">0.00%</span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Multi Month</label>
                            <div class="col-md-4">
                                <input type="range" name="reorder_trends_multi_month_trend_percentage" id="reorder_trends_multi_month_trend_percentage" value="{{$data['reorder_trends_multi_month_trend_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage reorder_trends_multi_month_trend_percentage adjust">0.00%</span>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">30 Over 30 Days</label>
                            <div class="col-md-4">
                                <input type="range" name="reorder_trends_30_over_30_days_percentage" id="reorder_trends_30_over_30_days_percentage" value="{{$data['reorder_trends_30_over_30_days_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage reorder_trends_30_over_30_days_percentage adjust">0.00%</span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Multi Week </label>
                            <div class="col-md-4">
                                <input type="range" name="reorder_trends_multi_week_trend_percentage" id="reorder_trends_multi_week_trend_percentage" value="{{$data['reorder_trends_multi_week_trend_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage reorder_trends_multi_week_trend_percentage adjust">0.00%</span>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">7 Over 7 Days</label>
                            <div class="col-md-4">
                                <input type="range" name="reorder_trends_7_over_7_days_percentage" id="reorder_trends_7_over_7_days_percentage" value="{{$data['reorder_trends_7_over_7_days_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage reorder_trends_7_over_7_days_percentage adjust">0.00%</span>
                            </div>
                        </div>
                        <b>Stock in Percentage(%)</b><br/><br/>
                        <div class="form-group row">

                            <label for="example-text-input" class="col-md-2 col-form-label">Safety Stock </label>
                            <div class="col-md-4">
                                <input type="range" name="reorder_safety_stock_percentage" id="reorder_safety_stock_percentage" value="{{$data['reorder_safety_stock_percentage']}}" min="0" max="100"><span class="btn btn-info btn-sm btn-rounded waves-effect waves-light percentage reorder_safety_stock_percentage adjust">0.00%</span>
                            </div>
                        </div>

                        <hr><br/>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Projected Reorder Date : </label>
                            <div class="col-md-4">
                                <input type="text" name="projected_reorder_date" id="projected_reorder_date" value="{{$product_sales_rate['projected_reorder_date'] != '' ? date('Y-m-d',strtotime($product_sales_rate['projected_reorder_date'])) : '' }}" class="form-control" readonly>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">Projected Reorder Quantity :</label>
                            <div class="col-md-4">
                                <input type="text" name="projected_reorder_qty" id="projected_reorder_qty" value="{{$product_sales_rate['projected_reorder_qty']}}" class="form-control" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Projected Order Date Range : </label>
                            <div class="col-md-4">
                                <input type="text" name="projected_order_date_range" id="projected_order_date_range" value="{{$product_sales_rate['projected_order_date_range']}}" class="form-control" readonly>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">Total Inventory : </label>
                            <div class="col-md-4">
                                <input type="text" name="total_inventory" id="total_inventory" value="{{$product_sales_rate['total_inventory']}}" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Lead Time Amazon : </label>
                            <div class="col-md-4">
                                <input type="text" name="lead_time_amazon" id="lead_time_amazon" value="{{$product_sales_rate['lead_time_amazon']}}" class="form-control" readonly>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">Order Frequency : </label>
                            <div class="col-md-4">
                                <input type="text" name="order_frequency" id="order_frequency" value="{{$product_sales_rate['order_frequency']}}" class="form-control" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Blackout Days : </label>
                            <div class="col-md-4">
                                <input type="text" name="blackout_days" id="blackout_days" value="{{$product_sales_rate['blackout_days']}}" class="form-control" readonly>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">Days Left To Order : </label>
                            <div class="col-md-4">
                                <input type="text" name="days_left_to_order" id="days_left_to_order" value="{{$product_sales_rate['days_left_to_order']}}" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Days Supply : </label>
                            <div class="col-md-4">
                                <input type="text" name="days_supply" id="days_supply" value="{{$product_sales_rate['days_supply']}}" class="form-control" readonly>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">Days Rate : </label>
                            <div class="col-md-4">
                                <input type="type" name="day_rate" id="day_rate" value="{{isset($product_sales_rate['day_rate']) ? $product_sales_rate['day_rate'] : '0'}}" min="0" max="100" class="form-control" readonly>
                            </div>
                        </div>

                        <b>Recent Sales </b><br/><br/>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">7 Days : </label>
                            <div class="col-md-4">
                                <input type="text" name="sevendays_sales" id="sevendays_sales" value="{{$product_sales_rate['sevendays_sales']}}" class="form-control" readonly>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">7 Days Rate : </label>
                            <div class="col-md-4">
                                <input type="type" name="" id="" value="{{isset($product_sales_rate['sevenday_day_rate']) ? $product_sales_rate['sevenday_day_rate'] : '0'}}" min="1" max="100" class="form-control" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">14 Days : </label>
                            <div class="col-md-4">
                                <input type="text" name="fourteendays_sales" id="fourteendays_sales" value="{{$product_sales_rate['fourteendays_sales']}}" class="form-control" readonly>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">14 Days Rate : </label>
                            <div class="col-md-4">
                                <input type="type" name="fourteendays_day_rate" id="fourteendays_day_rate" value="{{isset($product_sales_rate['fourteendays_day_rate']) ? $product_sales_rate['fourteendays_day_rate'] : '0'}}" min="1" max="100" class="form-control" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">30 Days : </label>
                            <div class="col-md-4">
                                <input type="text" name="thirtydays_sales" id="thirtydays_sales" value="{{$product_sales_rate['thirtydays_sales']}}" class="form-control" readonly>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">30 Days Rate : </label>
                            <div class="col-md-4">
                                <input type="type" name="thirtyday_day_rate" id="thirtyday_day_rate" value="{{isset($product_sales_rate['thirtyday_day_rate']) ? $product_sales_rate['thirtyday_day_rate'] : '0'}}" min="1" max="100" class="form-control">
                            </div>
                        </div>

                        <b>Trends </b><br/><br/>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">7 Days Previous : </label>
                            <div class="col-md-4">
                                <input type="text" name="sevendays_previous" id="sevendays_previous" value="{{$product_sales_rate['sevendays_previous']}}" class="form-control" readonly>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">7 Days Previous Rate : </label>
                            <div class="col-md-4">
                             
                               <input type="type" name="sevendays_previous_day_rate" id="sevendays_previous_day_rate" value="{{isset($product_sales_rate['sevendays_previous_day_rate']) ? $product_sales_rate['sevendays_previous_day_rate'] : '0' }}" min="1" max="100" class="form-control" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">30 Days Previous : </label>
                            <div class="col-md-4">
                                <input type="text" name="thirtydays_previous" id="thirtydays_previous" value="{{$product_sales_rate['thirtydays_previous']}}" class="form-control" readonly>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">30 Days Previous Rate : </label>
                            <div class="col-md-4">
                                <input type="type" name="thirtydays_previous_day_rate" id="thirtydays_previous_day_rate" value="{{isset($product_sales_rate['thirtydays_previous_day_rate']) ? $product_sales_rate['thirtydays_previous_day_rate'] : '0'}}" min="1" max="100" class="form-control" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">30 Days Last Year : </label>
                            <div class="col-md-4">
                                <input type="text" name="thirtydays_last_year" id="thirtydays_last_year" value="{{$product_sales_rate['thirtydays_last_year']}}" class="form-control" readonly>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">30Days LastYear Rate :</label>
                            <div class="col-md-4">
                                <input type="type" name="thirtydays_last_year_day_rate" id="30days_last_year_day_rate" value="{{isset($product_sales_rate['thirtydays_last_year_day_rate']) ? $product_sales_rate['thirtydays_last_year_day_rate'] : '0'}}" min="1" max="100" class="form-control" readonly>
                            </div>
                        </div>

                        <div class="button-items mt-3">
                           <div class="button-items mt-3" align="center">
                               <a class="btn btn-primary waves-effect waves-light calculation" href="javascript:void(0);" role="button">ReCalculate</a>
                               <a class="btn btn-primary waves-effect waves-light add_label" href="javascript:void(0);" role="button">Add as Supply Label</a>
                               <input class="btn btn-warning reset_button" type="button" value="Reset">
                               <a class="btn btn-primary waves-effect waves-light addtocart" data-product_id="{{ $product_sales_rate['mws_product']['id']}}" href="javascript:void(0);" role="button">Add to PI</a>
                            <a class="btn btn-info" href="{{ url()->previous() }}" role="button" style="width:116px;">Back</a>
                        </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- end row -->
@endsection
@section('script')
<script>
    $(document).on('click','.addtocart',function () {
        var product_id = $(this).data('product_id');
        var insert_type = 'Addtopurchaseinstance';
        $.ajax({
            type:"POST",
            url:'{{ route('products.store') }}',
            data:{product_id:product_id,insert_type:insert_type},
            datatype:"json",
            success: function(res){
                $('.error_message').html(res.message);
                $("html, body").animate({ scrollTop: 0 }, "slow");
            },
            error: function () {
                console.log('Something wrong');
            }
        });
    });
$(document).ready(function () {
    $('.logic_label_name').hide();
	$('.add_label').hide();
    var obj = $('input[type=range]');
    $.each(obj,function (index,element) {
        var val = ($(element).val() - $(element).attr('min')) / ($(element).attr('max') - $(element).attr('min'));
        var percent = val * 100;
        var percentage_range = percent.toFixed(2)+'%';
        $(element).css('background-image',
            '-webkit-gradient(linear, left top, right top, ' +
            'color-stop(' + percent + '%, #556ee6), ' +
            'color-stop(' + percent + '%, #eff2f7)' +
            ')');
        $(element).css('background-image',
            '-webkit-gradient(linear, left top, right top, ' +
            'color-stop(' + percent + '%, #556ee6), ' +
            'color-stop(' + percent + '%, #eff2f7)' +
            ')');

        $(element).css('background-image',
            '-moz-linear-gradient(left center, #556ee6 0%, #556ee6 ' + percent + '%, #eff2f7 ' + percent + '%, #eff2f7 100%)');

    });
});

$(document).on('click','.calculation',function(){

    var supply_history = $("#supply_last_year_sales_percentage"). val();
    var supply_num2=$("#supply_recent_last_7_day_sales_percentage"). val();
    var supply_num3=$("#supply_recent_last_14_day_sales_percentage").val();
    var supply_num4=$("#supply_recent_last_30_day_sales_percentage").val();
    var Supply_Recent_Sales_total=parseInt(supply_num2)+parseInt(supply_num3)+parseInt(supply_num4);
    var supply_num5=$("#supply_trends_year_over_year_historical_percentage").val();
    var supply_num6=$("#supply_trends_year_over_year_current_percentage").val();
    var supply_num7=$("#supply_trends_multi_month_trend_percentage").val();
    var supply_num8=$("#supply_trends_30_over_30_days_percentage").val();
    var supply_num9=$("#supply_trends_multi_week_trend_percentage").val();
    var supply_num10=$("#supply_trends_7_over_7_days_percentage").val();
    var Supply_Trends_Percentage_Total=parseInt(supply_num5)+parseInt(supply_num6)+parseInt(supply_num7)+parseInt(supply_num8)+parseInt(supply_num9)+parseInt(supply_num10);

    var Reorder_history=$("#reorder_last_year_sales_percentage"). val();
    var reorder_num2=$("#reorder_recent_last_7_day_sales_percentage"). val();
    var reorder_num3=$("#reorder_recent_last_14_day_sales_percentage").val();
    var reorder_num4=$("#reorder_recent_last_30_day_sales_percentage").val();
    var Reorder_Recent_Sales_Total=parseInt(reorder_num2)+parseInt(reorder_num3)+parseInt(reorder_num4);

    var reorder_num5=$("#reorder_trends_year_over_year_historical_percentage").val();
    var reorder_num6=$("#reorder_trends_year_over_year_current_percentage").val();
    var reorder_num7=$("#reorder_trends_multi_month_trend_percentage").val();
    var reorder_num8=$("#reorder_trends_30_over_30_days_percentage").val();
    var reorder_num9=$("#reorder_trends_multi_week_trend_percentage").val();
    var reorder_num10=$("#reorder_trends_7_over_7_days_percentage").val();
    var Reorder_Trends_Percentage_Total=parseInt(reorder_num5)+parseInt(reorder_num6)+parseInt(reorder_num7)+parseInt(reorder_num8)+parseInt(reorder_num9)+parseInt(reorder_num10);

    if((supply_history==100) && (Supply_Recent_Sales_total !=0) )
    {
        $('.SupplyInvoice').addClass('error');
        $('.error_message').html('<div class="alert alert-danger">Already supply  history percentage is 100<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');
        $(this).attr('readonly', false);
        $(this).removeClass('disable_class');
        window.scrollTo(0,0);
        return false;
    }

    else if((supply_history !=100) && (Supply_Recent_Sales_total > 100)){
        $('.SupplyInvoice').addClass('error');
        $('.error_message').html('<div class="alert alert-danger">Supply recent sales percentage the total of all the fields exceeds 100<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');
        $(this).attr('readonly', false);
        $(this).removeClass('disable_class');
        window.scrollTo(0,0);
        return false;

    }else if((supply_history !=100) && (Supply_Recent_Sales_total < 100)){
        $('.SupplyInvoice').addClass('error');
        $('.error_message').html('<div class="alert alert-danger">Supply recent sales percentage must be 100 and at present the total of all the fields exceeds 100<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');
        $(this).attr('readonly', false);
        $(this).removeClass('disable_class');
        window.scrollTo(0,0);
        return false;
    }


    if(Supply_Trends_Percentage_Total > 100){
        $('.SupplyInvoice').addClass('error');
        $('.error_message').html('<div class="alert alert-danger">Supply trends  percentage the total of all the fields exceeds 100<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');
        $(this).attr('readonly', false);
        $(this).removeClass('disable_class');
        window.scrollTo(0,0);
        return false;

    }else if(Supply_Trends_Percentage_Total < 100){
        $('.SupplyInvoice').addClass('error');
        $('.error_message').html('<div class="alert alert-danger">Supply trends   percentage must be 100 and at present the total of all the fields exceeds 100<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');
        $(this).attr('readonly', false);
        $(this).removeClass('disable_class');
        window.scrollTo(0,0);
        return false;
    }
    if((Reorder_history==100) && (Reorder_Recent_Sales_Total !=0) )
    {
        $('.SupplyInvoice').addClass('error');
        $('.error_message').html('<div class="alert alert-danger">Already reorder history percentage is 100<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');
        $(this).attr('readonly', false);
        $(this).removeClass('disable_class');
        window.scrollTo(0,0);
        return false;
    }
    else if((Reorder_history !=100) && (Reorder_Recent_Sales_Total > 100)){
        $('.ReorderInvoice').addClass('error');
        $('.error_message').html('<div class="alert alert-danger">Reorder recent sales percentage the total of all the fields exceeds 100 100<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');
        $(this).attr('readonly', false);
        $(this).removeClass('disable_class');
        window.scrollTo(0,0);
        return false;
    }else if((Reorder_history !=100) && (Reorder_Recent_Sales_Total < 100)){
        $('.ReorderInvoice').addClass('error');
        $('.error_message').html('<div class="alert alert-danger">Reorder recent sales percentage must be 100 and at present the total of all the fields exceeds 100<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');
        $(this).attr('readonly', false);
        $(this).removeClass('disable_class');
        window.scrollTo(0,0);
        return false;
    }

    if(Reorder_Trends_Percentage_Total > 100){
        $('.ReorderInvoice').addClass('error');
        $('.error_message').html('<div class="alert alert-danger">Reorder trends  percentage the total of all the fields exceeds 100<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');
        $(this).attr('readonly', false);
        $(this).removeClass('disable_class');
        window.scrollTo(0,0);
        return false;
    }else if(Reorder_Trends_Percentage_Total < 100){
        $('.ReorderInvoice').addClass('error');
        $('.error_message').html('<div class="alert alert-danger">Reorder trends percentage the total of all the fields exceeds 100<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');
        $(this).attr('readonly', false);
        $(this).removeClass('disable_class');
        window.scrollTo(0,0);
        return false;
    }

    var product_id = '{{ Request::segment(2)}}';
    var insert_type = 'get_calculation_product';
    $('.loader').show();
    $.ajax({
        type:"POST",
        url:'{{ url('product_calculation') }}',
        data: $('#order_logic').serialize(),
        datatype:"json",
        success: function(res){
            //$('.error_message').html(res.message);
            $('.loader').hide();
			var newData = JSON.parse(res);
			$("#projected_reorder_date").val(newData['projected_reorder_date']);
			$("#projected_reorder_qty").val(newData['projected_reorder_qty']);
			$("#projected_order_date_range").val(newData['projected_order_date_range']);
			$("#days_left_to_order").val(newData['days_left_to_order']);
			$("#days_supply").val(newData['days_supply']);
			$("#day_rate").val(newData['day_rate']);
			$('.add_label').show();
            $("html, body").animate({ scrollTop: 0 }, "slow");
            return false;
        },
        error: function () {
            console.log('Something wrong');
        }
    });
});

$(document).on('click','.save_label',function() {
    $.confirm({
        title: 'Assign Product!',
        content: 'You want to assign this Label to product ? ',
        buttons: {
            yes: function () {
                var display = 1;
                $('#product_assign').val(display);
                assign_product(display);
            },
            no: function () {
                var display = 0;
                $('#product_assign').val(display);
                assign_product(display);
            },
        }
    });
});
function assign_product(display){
    var logic_label_name = $('#logic_label_name').val();
    if(logic_label_name == ''){
        $('#logic_label_name').addClass('error');
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    }else {
        $('#logic_label_name').removeClass('error');
        $.ajax({
            type: "POST",
            url: '{{ route('products.store') }}',
            data: $('#order_logic').serialize(),
            datatype: "json",
            success: function (res) {
                $('.error_message').html(res.message);
                $("html, body").animate({ scrollTop: 0 }, "slow");
            },
            error: function () {
                console.log('Something wrong');
            }
        });
    }
}

$(document).on('click','.add_label',function(){
    $('.logic_label_name').show();
    $(this).removeClass('add_label');
    $(this).addClass('save_label');
    $(this).text('Save label');
    $("html, body").animate({ scrollTop: 0 }, "slow");
    //window.scrollTo(0, 0);
});
 </script>
@endsection

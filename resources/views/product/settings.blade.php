@extends('layouts.master')

@section('title') Product Setting @endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Product Setting</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('/products') }}"> Products</a></li>
                        <li class="breadcrumb-item active">settings</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">
                <div class="card-body">
                     
                    @php($vendors_default = get_default_vendor_setting())
                    @php($suppliers_default = get_default_suppliers_setting())
                    <form id="settings-product-assign" action="{{ route('products.store') }}" method="POST" onreset="myFunction()">
                        @csrf
                        <input type="hidden" name="insert_type" value="">
                        <input type="hidden" name="vendors_edit_id" value="{{isset($vendor_setting['id']) ? $vendor_setting['id'] : ''}}">
                        <input type="hidden" name="suppliers_edit_id" value="{{isset($supplier_setting['id']) ? $supplier_setting['id'] : ''}}">
                            <div class="form-group row">
                                     <div class="col-md-2">
                                         <img width="100px" src="{{$product_data['prod_image']}}" title="{{$product_data['prod_names']}}">
                                     </div>
                                     <div class="col-md-10">
                                         {{ $product_data['prod_name']}}
                                         <hr>
                                         {{ $product_data['sku']}}
                                     </div>
                                </div>
                        <input type="hidden" name="product_id" id="product_id" value="{{$product_id}}">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Select User Type</label>
                            <div class="col-md-4">
                                <div class="custom-control custom-radio mb-3">
                                    <input type="radio" id="vendors" name="customRadio" class="custom-control-input form-control" value="vendors" {{$isvendors == 1 ? 'checked' : ''}} {{$isvendors == 0 && $issuppliers == 0 ? 'checked' : ''}}>
                                    <label class="custom-control-label" for="vendors">Vendors</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="suppliers" name="customRadio" class="custom-control-input form-control" value="suppliers" {{$issuppliers == 1 ? 'checked' : ''}}>
                                    <label class="custom-control-label" for="suppliers">Suppliers</label>
                                </div>
                            </div>
                        </div>

                        <div class="vendors_form">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Select Vendors : </label>
                                <div class="col-md-10">
                                    <select class="custom-select" name="vendors_id" id="vendors_id">
                                        <option value="">Select vendors</option>
                                        @if($vendors)
                                            @foreach($vendors as $vendor)
                                                <option value="{{$vendor['id']}}" {{$vendor['id'] == $vendor_setting['vendor_id'] ? 'selected="selected"' : '' }} >{{$vendor['vendor_name']}}</option>
                                             @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Lead Time</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_integer" type="text" name="lead_time" id="lead_time" value="{{ $vendor_setting['lead_time']!='' ? $vendor_setting['lead_time'] : $vendors_default['lead_time'] }}" maxlength="3">
                                </div>
                                <label for="example-text-input" class="col-md-2 col-form-label">Order Volume</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_integer" type="text" name="order_volume" id="order_volume" value="{{ $vendor_setting['order_volume'] != '' ? $vendor_setting['order_volume'] : $vendors_default['order_volume'] }}" maxlength="3">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Quantity Discount</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_float" type="text" name="quantity_discount" id="quantity_discount" value="{{ $vendor_setting['quantity_discount'] != '' ? $vendor_setting['quantity_discount'] : $vendors_default['quantity_discount'] }}" maxlength="3">
                                </div>
                                <label for="example-text-input" class="col-md-2 col-form-label">MOQ</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_integer" type="text" name="moq" id="moq" value="{{ $vendor_setting['moq'] != '' ? $vendor_setting['moq'] : $vendors_default['moq'] }}" maxlength="3">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Shipping</label>
                                <div class="col-md-4">
                                    <select class="custom-select" name="shipping" id="shipping">
                                        <option value="">Select Shipping</option>
                                        <option value="1" @if($vendor_setting['shipping']==1) selected @elseif($vendors_default['shipping'] == 1) selected @endif>UPS</option>
                                        <option value="2" @if($vendor_setting['shipping']==2) selected @elseif($vendors_default['shipping'] == 2) selected @endif>USPS</option>
                                        <option value="3" @if($vendor_setting['shipping']==3) selected @elseif($vendors_default['shipping'] == 3) selected @endif>FEDEX</option>
                                        <option value="4" @if($vendor_setting['shipping']==4) selected @elseif($vendors_default['shipping'] == 4) selected @endif>Private Carrier</option>
                                        <option value="5" @if($vendor_setting['shipping']==5) selected @elseif($vendors_default['shipping'] == 5) selected @endif>Direct</option>
                                        <option value="6" @if($vendor_setting['shipping']==6) selected @elseif($vendors_default['shipping'] == 6) selected @endif>Pickup</option>
                                    </select>
                                </div>
                                <label for="example-text-input" class="col-md-2 col-form-label">Ship To Warehouse</label>
                                <div class="col-md-4">
                                    <select class="custom-select" name="ship_to_warehouse" id="ship_to_warehouse" value="{{ $vendor_setting['ship_to_warehouse'] != '' ?  $vendor_setting['ship_to_warehouse'] : $vendors_default['ship_to_warehouse'] }}">


                                        <option value="">Select Warehouse</option>

                                        @if($warehouse)
                                            @foreach($warehouse as $house)
                                                <option value="{{$house['id']}}" {{$house['id'] == $vendor_setting['ship_to_warehouse'] ? 'selected="selected"' : '' }}>{{$house['warehouse_name']}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">CBM Per Container</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_float" type="text" name="CBM_Per_Container" id="CBM_Per_Container" value="{{ $vendor_setting['CBM_Per_Container'] != '' ? $vendor_setting['CBM_Per_Container'] : $vendors_default['CBM_Per_Container'] }}" maxlength="9">
                                </div>
                                <label for="example-text-input" class="col-md-2 col-form-label">Inventory Level</label>
                                <div class="col-md-4">
                                    <select class="custom-select" name="Inventory_level" id="Inventory_level">
                                        <option value="1" @if($vendor_setting['Inventory_level']==1) selected @elseif($vendors_default['Inventory_level'] == 1) selected @endif>Standard</option>
                                        <option value="2" @if($vendor_setting['Inventory_level']==2) selected @elseif($vendors_default['Inventory_level'] == 2) selected @endif>Seasonal</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Cost</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_integer" type="text" name="Cost" id="Cost" value="{{ $vendor_setting['Cost'] != '' ? $vendor_setting['Cost'] : @$vendors_default['Cost'] }}">
                                </div>
                                <label for="example-text-input" class="col-md-2 col-form-label">Cost + Expenses</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_integer" type="text" name="Cost_Expenses" id="Cost_Expenses" value="{{ $vendor_setting['Cost_Expenses'] != '' ? $vendor_setting['Cost_Expenses'] : $vendors_default['Cost_Expenses'] }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Qty Per Carton</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_float" type="text" name="Qty_per_Carton" id="Qty_per_Carton" value="{{ $vendor_setting['Qty_per_Carton'] != '' ? $vendor_setting['Qty_per_Carton'] : $vendors_default['Qty_per_Carton'] }}" maxlength="9">
                                </div>
                                <label for="example-text-input" class="col-md-2 col-form-label">Cartons Per Pallet</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_float" type="text" name="Cartons_per_pallet" id="Cartons_per_pallet" value="{{ $vendor_setting['Cartons_per_pallet'] != '' ? $vendor_setting['Cartons_per_pallet'] : $vendors_default['Cartons_per_pallet'] }}" maxlength="9">
                                </div>
                            </div>

                            <b> Box Domensions Details : </b><br/>

                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Height</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_float" type="text" name="Box_Domensions_Height" id="Box_Domensions_Height" value="{{ $vendor_setting['Box_Domensions_Height'] != '' ? $vendor_setting['Box_Domensions_Height'] : $vendors_default['Box_Domensions_Height'] }}" maxlength="9">
                                </div>

                                <label for="example-text-input" class="col-md-2 col-form-label">Width</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_float" type="text" name="Box_Domensions_Width" id="Box_Domensions_Width" value="{{ $vendor_setting['Box_Domensions_Width'] != '' ? $vendor_setting['Box_Domensions_Width'] : $vendors_default['Box_Domensions_Width'] }}" maxlength="9">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Weight</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_float" type="text" name="Box_Domensions_Weight" id="Box_Domensions_Weight" value="{{ $vendor_setting['Box_Domensions_Weight'] != '' ? $vendor_setting['Box_Domensions_Weight'] : $vendors_default['Box_Domensions_Weight'] }}" maxlength="9">
                                </div>
                            </div>

                        </div>


                        <div class="suppliers_form">
                            
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Select Suppliers : </label>
                                <div class="col-md-10">
                                    <select class="custom-select" name="suppliers_id" id="suppliers_id">
                                        <option value="0">Select Suppliers</option>
                                        @if($suppliers)
                                            @foreach($suppliers as $supplier)
                                                <option value="{{$supplier['id']}}" {{$supplier['id'] == @$supplier_setting['supplier_id'] ? 'selected="selected"' : '' }} >{{$supplier['supplier_name']}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Lead Time</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_integer" type="text" name="lead_times" id="lead_times" value="{{ $supplier_setting['lead_time'] != '' ? $supplier_setting['lead_time']: $suppliers_default['lead_time']}}" maxlength="3">
                                </div>
                                <label for="example-text-input" class="col-md-2 col-form-label">Order Volume</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_integer" type="text" name="order_volumes" id="order_volumes" value="{{ $supplier_setting['order_volume'] != '' ? $supplier_setting['order_volume'] :  @$suppliers_default['order_volume']}}" maxlength="3">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Quantity Discount</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_float" type="text" name="quantity_discounts" id="quantity_discounts" value="{{ $supplier_setting['quantity_discount'] != '' ? $supplier_setting['quantity_discount'] : $suppliers_default['quantity_discount'] }}" maxlength="3">
                                </div>
                                <label for="example-text-input" class="col-md-2 col-form-label">MOQ</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_integer" type="text" name="moqs" id="moqs" value="{{ $supplier_setting['moq'] != '' ? $supplier_setting['moq'] : $suppliers_default['moq']}}" maxlength="3">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">CBM Per Container</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_float" type="text" name="CBM_Per_Containers" id="CBM_Per_Containers" value="{{ $supplier_setting['CBM_Per_Container'] != '' ? $supplier_setting['CBM_Per_Container'] : $suppliers_default['CBM_Per_Container'] }}" maxlength="9">
                                </div>
                                <label for="example-text-input" class="col-md-2 col-form-label">Production Time</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_integer" type="text" name="Production_Times" id="Production_Times" value="{{ $supplier_setting['Production_Time'] != '' ? $supplier_setting['Production_Time'] : $suppliers_default['Production_Time'] }}" maxlength="3">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Boat To Port</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_integer" type="text" name="Boat_To_Ports" id="Boat_To_Ports" value="{{ $supplier_setting['Boat_To_Port'] != '' ? $supplier_setting['Boat_To_Port'] : $suppliers_default['Boat_To_Port']}}" maxlength="3">
                                </div>
                                <label for="example-text-input" class="col-md-2 col-form-label">Port To Warehouse</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_integer" type="text" name="Port_To_Warehouses" id="Port_To_Warehouses" value="{{ $supplier_setting['Port_To_Warehouse'] != '' ? $supplier_setting['Port_To_Warehouse'] : $suppliers_default['Port_To_Warehouse']}}" maxlength="3">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Warehouse Receipt</label>
                                <div class="col-md-4">
                                    <select class="custom-select" name="Warehouse_Receipts" id="Warehouse_Receipts">
                                        <option value="">Select Receipt</option>
                                    </select>
                                </div>
                                <label for="example-text-input" class="col-md-2 col-form-label">Ship To Specific Warehouse</label>
                                <div class="col-md-4">
                                    <select class="custom-select" name="Ship_To_Specific_Warehouses" id="Ship_To_Specific_Warehouses" value="{{ $supplier_setting['Ship_To_Specific_Warehouse'] }}">
                                        <option value="">Select Warehouse</option>
                                        @if($warehouse)
                                            @foreach($warehouse as $house)
                                                <option value="{{$house['id']}}" {{$house['id'] == $supplier_setting['Ship_To_Specific_Warehouse'] ? 'selected="selected"' : '' }}>{{$house['warehouse_name']}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Cost</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_integer" type="text" name="Costs" id="Costs" value="{{ $supplier_setting['Cost'] != '' ? $supplier_setting['Cost'] : $suppliers_default['Cost'] }}">
                                </div>
                                <label for="example-text-input" class="col-md-2 col-form-label">Cost + Expenses</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_integer" type="text" name="Cost_Expensess" id="Cost_Expensess" value="{{ $supplier_setting['Cost_Expenses'] != '' ? $supplier_setting['Cost_Expenses'] : $suppliers_default['Cost_Expenses'] }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Qty per Carton</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_float" type="text" name="Qty_per_Cartons" id="Qty_per_Cartons" value="{{ $supplier_setting['Qty_per_Carton'] != '' ? $supplier_setting['Qty_per_Carton'] : $suppliers_default['Qty_per_Carton'] }}" maxlength="9">
                                </div>
                                <label for="example-text-input" class="col-md-2 col-form-label">Cartons per Pallet</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_float" type="text" name="Cartons_per_pallets" id="Cartons_per_pallets" value="{{ $supplier_setting['Cartons_per_pallet'] != '' ? $supplier_setting['Cartons_per_pallet'] : $suppliers_default['Cartons_per_pallet'] }}" maxlength="9">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Inventory Level</label>
                                <div class="col-md-4">
                                    <select class="custom-select" name="Inventory_levels" id="Inventory_levels">
                                        <option value="1" @if($supplier_setting['Inventory_level']==1) selected @elseif($suppliers_default['Inventory_level'] == 1) selected @endif>Standard</option>
                                        <option value="2" @if(@$supplier_setting['Inventory_level']==2) selected @elseif($suppliers_default['Inventory_level'] == 2) selected @endif>Seasonal</option>
                                    </select>
                                </div>
                            </div>

                            <b> Box Domensions Details : </b><br/>

                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Height</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_float" type="text" name="Box_Domensions_Heights" id="Box_Domensions_Heights" value="{{ $supplier_setting['Box_Domensions_Height'] != '' ? $supplier_setting['Box_Domensions_Height'] : $suppliers_default['Box_Domensions_Height'] }}" maxlength="9">
                                </div>

                                <label for="example-text-input" class="col-md-2 col-form-label">Width</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_float" type="text" name="Box_Domensions_Widths" id="Box_Domensions_Widths" value="{{ $supplier_setting['Box_Domensions_Width'] != '' ? $supplier_setting['Box_Domensions_Width'] : $suppliers_default['Box_Domensions_Width'] }}" maxlength="9">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Weight</label>
                                <div class="col-md-4">
                                    <input class="form-control allow_float" type="text" name="Box_Domensions_Weights" id="Box_Domensions_Weights" value="{{ $supplier_setting['Box_Domensions_Weight'] != '' ? $supplier_setting['Box_Domensions_Weight'] : $suppliers_default['Box_Domensions_Weight'] }}" maxlength="9">
                                </div>
                            </div>
                        </div>

                        <div class="button-items mt-3">
                            <div class="vendors_button">
                                <input class="btn btn-info vendors" type="submit" value="Submit">
                                <a class="btn btn-danger waves-effect waves-light" href="{{ route('products.index') }}" role="button">Cancel</a>
                                <input class="btn btn-warning" type="reset" value="Reset">
                            </div>
                            <div class="supplier_button">
                                <input class="btn btn-info suppliers" type="submit" value="Submit">
                                <a class="btn btn-danger waves-effect waves-light" href="{{ route('products.index') }}" role="button">Cancel</a>
                                <input class="btn btn-warning" type="reset" value="Reset">
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->

@endsection
@section('script')

    <script>
        $(document).ready(function(){
            var valuedata = $('input[name="customRadio"]:checked').val();
            if(valuedata == 'vendors'){
                $('.suppliers_form').hide();
                $('.vendors_form').show();
                $('.supplier_button').hide();
                $('.vendors_button').show();
            }else{
                $('.vendors_form').hide();
                $('.suppliers_form').show();
                $('.supplier_button').show();
                $('.vendors_button').hide();
            }
        });
        $(document).on('click','input[name="customRadio"]',function(){
            var valuedata = $(this).val();
            if(valuedata == 'vendors'){
                $('.suppliers_form').hide();
                $('.vendors_form').show();
                $('.supplier_button').hide();
                $('.vendors_button').show();
            }else{
                $('.vendors_form').hide();
                $('.suppliers_form').show();
                $('.supplier_button').show();
                $('.vendors_button').hide();
            }
        });
    </script>

@endsection


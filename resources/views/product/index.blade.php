@extends('layouts.master')

@section('title')  Product List @endsection

@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18"> Products</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active"> Products</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
<div class="row">
    <input type="hidden" name="insert_type" value="search_data">
    <input type="hidden" value="{{$marketplace}}" name="marketplaces" id="marketplaces">
    <div class="col-sm-6">
        <div class="form-group">
            <input type="text" name="brand_name" id="brand_name" value="{{@$input_text['brand_name']}}" class="form-control" placeholder="Enter Brand Name">
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <select name="vendors" id="vendors" class="form-control">
                <option value="">Select vendors</option>
                @if(!empty($vendors))
                    @foreach($vendors as $list)
                        <option value="{{$list->id}}" {{$list->id == @$input_text['vendors'] ? 'selected' : '' }}>{{$list->vendor_name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <select name="suppliers" id="suppliers" class="form-control">
                <option value="">Select Suppliers</option>
                @if(!empty($suppliers))
                    @foreach($suppliers as $list)
                        <option value="{{$list->id}}" {{$list->id == @$input_text['suppliers'] ? 'selected' : ''}}>{{$list->supplier_name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>
    <div class="col-sm-6">
        <a href="javascript:void(0);" class="btn btn-primary search_data"><i class="fa fa-search"></i> Search</a>
        <a href="{{route('products.index')}}" class="btn btn-primary"><i class="fa fa-filter"></i> Clear</a>
    </div>
</div>
        </div>
    </div>
    <br/>
    <!-- end page title -->
    <div class="row">
        <div class="col-lg-12">
            <span class="error_message"></span>
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4"> Product List</h4>
                    <div class="table-responsive">
                        <table id="datatables" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead class="thead-light">
                            <tr>
                                <th>No.</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Price</th>
                                <th>SKU</th>
                                <th>ASIN</th>
                                <th>Quantity Available</th>
                                <!--<th>UPC</th>  -->
                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- end table-responsive -->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                   <b> Apply Order logic </b>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                      <form name="create-product_assign_supply_logic" id="userForm" action="{{ route('products.store') }}" method="POST">
                            @csrf
                        <input type="hidden" name="Product_id" id="Product_id" value="" >
                           <input type="hidden" value="{{$marketplace}}" name="marketplaces" id="marketplaces">
                          <input type="hidden" name="input_type" value="Creates_form">
                          <div class="form-group row">
                                    <label class="col-md-6">Select Label Name : </label>
                                    <div class="col-md-12">
                                        <select class="custom-select form-control" name="supply_logic_id" id="supply_logic_id">
                                            <option value="">Select Label</option>
                                        </select>
                                        <span id="error"></span>
                                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btnAdd" id="btnAdd" data-dismiss="modal">Submit</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
            </div>

        </div>
    </div>

    </div>
    <!-- end row -->
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            var id = $('#marketplaces').val();
            var vendors = $('#vendors').val();
            var suppliers = $('#suppliers').val();
            var brand_name = $('#brand_name').val();
            loadDashboard(id,vendors,suppliers,brand_name);
        });
        $(document).on('click','.search_data',function () {
            var id = $('#marketplaces').val();
            var vendors = $('#vendors').val();
            var suppliers = $('#suppliers').val();
            var brand_name = $('#brand_name').val();
            loadDashboard(id,vendors,suppliers,brand_name);
        });

        function loadDashboard(id,vendors,suppliers,brand_name){
            var count=1;
            var insert_type = 'product_filter';
            var table = $('#datatables').DataTable({
                "order": [ 0, 'asc' ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('products.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.id=id,d.vendors=vendors,d.suppliers=suppliers,d.brand_name=brand_name,d.insert_type=insert_type
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                'columnDefs': [
                    {
                        targets: 0,
                        render: function (data, type, row) {
                            return row['id'];
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return row['prod_image'];
                        }
                    },
                    {
                        targets: 2,
                        render: function (data, type, row) {
                            var result = row['prod_name'].substring(0, 25);
                            pro_name = '<span title="'+row['prod_name']+'">+result+</span>';
                            return row['prod_name'];
                        }
                    },
                    {
                        targets: 3,
                        render: function (data, type, row) {
                            return row['your_price'];

                        }
                    },
                    {
                        targets: 4,
                        render: function (data, type, row) {
                            return row['sku'];
                        }
                    },
                    {
                        targets: 5,
                        render: function (data, type, row) {
                            return row['asin'];
                        }
                    },
                    {
                        targets: 6,
                        render: function (data, type, row) {
                            return row['quantity_available'];
                        }
                    },
                    {
                        targets: 7,
                        render: function (data, type, row) {
                            return row['action']
                        }
                    }
                ]
            });
        }

       
        $(document).on('click','.addtocart',function () {
            var product_id = $(this).data('product_id');
            var insert_type = 'Addtopurchaseinstance';
            $.ajax({
                type:"POST",
                url:'{{ route('products.store') }}',
                data:{product_id:product_id,insert_type:insert_type},
                datatype:"json",
                success: function(res){
                    $('.error_message').html(res.message);
                     $("html, body").animate({ scrollTop: 0 }, "slow");
                },
                error: function () {
                    console.log('Something wrong');
                }
            });
        });
    </script>
    <script>
        $(document).on('click','.addtoid',function () {
            var product_id = $(this).data('product_id');
            var products = [];
            var html='';
            var htmls = '';
            var insert_type = 'selected_product_assign_supply_logic';
            products = <?php  echo json_encode($logic_label_name) ; ?>;

            html += '<option value="">Select label</option>';
            $.each(products, function( index, value ) {
                html += '<option value="' + value['id'] + '" title="' + value['logic_label_name'] + '">' + value['logic_label_name'] + '</option>';
            });
            $('#supply_logic_id').html(html);
            $("#Product_id").val(product_id);
           $.ajax({
                type:"POST",
                url:'{{ route('products.store') }}',
                data:{insert_type:insert_type, product_id:product_id},
                datatype:"json",
                success: function(res){
                    if(res.error == 0){
                        htmls += '<option value="">Select label</option>';
                        $.each(products, function( index, value ) {
                            if(res.label_id != '' && value['id'] == res.label_id){
                                var selected = 'selected="selected"';
                            }else{
                                var selected = '';
                            }
                            htmls += '<option value="' + value['id'] + '" title="' + value['logic_label_name'] + '" '+selected+'>' + value['logic_label_name'] + '</option>';
                            $('#supply_logic_id').html(htmls);
                        });

                    }
                },

                error: function () {
                    console.log('Something wrong');
                }
            });
        });

        $(".btnAdd").click(function(){
            var insert_type = 'Creates_form';
            var supply_logic_id= $('#supply_logic_id').val();
            var pro_id = $("#Product_id").val();
            var error = document.getElementById("error")

            if(supply_logic_id ==''){
                 error.textContent = "Please select label";
                 error.style.color = "red";
                 $("#error").load(location.href + " #error");
                 return false;
            }else {
                $.ajax({
                    type: "POST",
                    url: '{{ route('products.store') }}',
                    data: {insert_type: insert_type, supply_logic_id: supply_logic_id, pro_id: pro_id},
                    datatype: "json",
                    success: function (res) {
                        $('.error_message').html(res.message);
                        $("html, body").animate({scrollTop: 0}, "slow");
                        setTimeout(function(){ $('.error_message').html(""); }, 5000);
                        var id = $('#marketplaces').val();
                        var vendors = $('#vendors').val();
                        var suppliers = $('#suppliers').val();
                        var brand_name = $('#brand_name').val();
                        loadDashboard(id,vendors,suppliers,brand_name);
        
                        
                    },

                    error: function () {
                        console.log('Something wrong');
                    }
                });
            }
        });

    </script>
@endsection

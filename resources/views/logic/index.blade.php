@extends('layouts.master-final')
@section('title', 'Logic')

@section('customcss')
    <link rel="stylesheet" href="https://unpkg.com/vue-range-component@1.0.3/dist/vue-range-slider.min.css">
    <style>
        table th{
            border: none !important;
        }

        .vue-range-slider.slider-component .slider .slider-dot{
            background-color: black;
        }

        .custom-control-input:checked~.custom-control-label::before{
            border-color: #227CFF;
            background-color: #227CFF;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid" id="content" :key="renderKey">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0 font-size-18">Logic</h4>
        </div>

        <div class="card">
            <div class="row p-2">
                <div class="col-md-6 form-inline">
                    <button
                        id="main_delete_btn"
                        class="text-light1 mr-4 border-0 bg-transparent"
                        @click="bulkDelete()"
                        data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                        <i class="mdi mdi-trash-can font-size-18"></i>
                    </button>
                    <label for="displaying" class="mr-2">Displaying</label>
                    <select class="custom-select custom-select-sm form-control-sm mr-2" v-model="perPage">
                        <option value="20">1-20</option>
                        <option value="40">1-40</option>
                        <option value="60">1-60</option>
                        <option value="80">1-80</option>
                        <option value="100">1-100</option>
                    </select>
                    <label for="" class="mr-4">of @{{ processedRows.length }}</label>
                </div>

                <div class="col-md-6 d-flex justify-content-end align-items-center">
                    <div class="dropdown">
                        <button type="button" class="btn btn-sm btn-light position-relative noti-icon mr-sm-3"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="overflow:visible;"
                                data-toggle-second="tooltip" data-placement="left" data-original-title="Manage Table Columns"
                        >
                            <i class="mdi mdi-view-parallel font-size-16"></i>
                            <template v-if="fields.length-cols.length">
                                <span class="badge badge-primary badge-pill pos2">@{{ fields.length-cols.length }}</span>
                            </template>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="columns-dropdown" style="width:200px;">
                            <form>
                                <p class="px-2"><small>Manage Table Columns</small></p>
                                <div class="p-2 hover-bg-blue-50" v-for="field in fields" :key="field.slug">
                                    <label class="stockful-checkbox">@{{ field.caption }}
                                        <input type="checkbox" v-model="cols" :value="field.slug"  @change="updateCols">
                                        <span></span>
                                    </label>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="d-flex">
                        <button
                            class="border-0 rounded bg-light shadow pagination-btn p-0 mx-1"
                            @click.prevent="prevPage"
                            data-toggle="tooltip" data-placement="top" title="" data-original-title="Prev Page"
                        >
                            <i class="mdi mdi-chevron-left"></i>
                        </button>
                        <template v-if="currentPage!=1">
                            <button class="border-0 bg-transparent pagination-btn p-0 mx-1">
                                1
                            </button>
                        </template>
                        <button class="border-0 rounded shadow pagination-btn p-0 mx-1 btn-primary">
                            @{{ currentPage }}
                        </button>
                        <template v-if="currentPage < lastPage">
                            <button class="border-0 bg-transparent pagination-btn p-0 mx-1">
                                @{{ lastPage }}
                            </button>
                        </template>
                        <button
                            class="border-0 rounded shadow pagination-btn p-0 mx-1"
                            @click.prevent="nextPage"
                            data-toggle="tooltip" data-placement="top" title="" data-original-title="Next Page">
                            <i class="mdi mdi-chevron-right"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="overflow-hidden">
                <table class="table table-bordered table-centered position-relative border-0 m-0">
                    <thead>
                        <tr style="background: #F5F6F7;">
                            <th>
                                <label class="stockful-checkbox">
                                    <input type="checkbox" @change="updateSelectPage" v-model="selectPage">
                                    <span></span>
                                </label>
                            </th>

                            <th v-for="field in columns()" :key="field.slug" @click="sortBy(field.slug)">
                                @{{ field.caption }}
                                <span :class="(field.slug==sortField && sortDirection=='asc') ? 'text-blue-500 font-weight-bold' : 'text-gray-200'">↑</span>
                                <span :class="(field.slug==sortField && sortDirection=='desc') ? 'text-blue-500 font-weight-bold' : 'text-gray-200'">↓</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <template v-if="selectPage">
                                <td :colspan="cols.length + 3" class="text-center" style="background-color: #F2F7FF;">
                                    <template v-if="selectAll">
                                        Selected all <strong> @{{ processedRows.length }} </strong> Rows.
                                    </template>
                                    <template v-else>
                                        Selected @{{ selected.length }} out of @{{ processedRows.length }}.
                                        <button class="btn btn-link" @click="selectAll=true">
                                            Select All @{{ processedRows.length }}
                                        </button>
                                    </template>
                                </td>
                            </template>
                        </tr>
                        <template v-for="row in processedRows.slice(perPage * (currentPage - 1), perPage * currentPage)" :key="row.id">
                            <tr @click="openedRow == row.id ? openedRow = null : openedRow = row.id">
                                <td style="border-right: none;">
                                    <label class="stockful-checkbox">
                                        <input type="checkbox" v-model="selected" :value="row.id" @click="updatedSelected">
                                        <span></span>
                                    </label>
                                </td>
                                <td v-for="field in columns()" :key="field.id" style="border-left:none;">
                                    @{{ row.id }}
                                </td>
                            </tr>
                            <template v-if="openedRow == row.id">
                                <tr>
                                    <td :colspan="columns().length + 1" class="p-4" style="background: #f8f8fb;">
                                        <div class="row mb-2">
                                            <div class="col-12 d-flex justify-content-end">
                                                <select name="" id="" class="border-0 bg-transparent mr-3">
                                                    <option value="">PO #12345</option>
                                                </select>
                                                <button type="button" class="btn btn-dark waves-effect waves-light mr-3">Add To PO</button>
                                                <button type="button" class="btn btn-outline-primary waves-effect waves-light mr-3">View PO</button>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-4">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="d-flex border-bottom">
                                                            <h5 class="h5 p-2 mr-2 mb-0"
                                                                :class="detail_logic_type ? 'text-primary border-bottom border-primary' : 'text-light1'"
                                                                @click="detail_logic_type=true"
                                                            >Reorder Logic</h5>
                                                            <h5 class="h5 p-2 mr-2 mb-0"
                                                                :class="!detail_logic_type ? 'text-primary border-bottom border-primary' : 'text-light1'"
                                                                @click="detail_logic_type=false"
                                                            >DOS Logic</h5>
                                                        </div>
                                                        <div class="d-flex justify-content-between my-4">
                                                            <select name="" id="" class="border-0 h5 mb-0">
                                                                <option value="">Standing Chalkboard</option>
                                                            </select>
                                                            <div class="d-flex align-items-center">
                                                                <h6 class="h6 mr-2 mb-0">Custom</h6>
                                                                <div class="custom-control custom-switch">
                                                                    <input type="checkbox" class="custom-control-input" id="customSwitch-is-custom" v-model="detail_is_custom">
                                                                    <label class="custom-control-label" for="customSwitch-is-custom"></label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="d-flex justify-content-between">
                                                            <h5 class="h5 border-bottom rounded py-2 m-0" style="width: 60px;">
                                                                @{{ detail_sales[0] }}%
                                                            </h5>
                                                            <div class="w-50 d-flex align-items-center">
                                                                <vue-range-slider
                                                                    :width="'100%'"
                                                                    :bg-style="bgStyle" :tooltip-style="tooltipStyle" :process-style="processStyle"
                                                                    v-model="detail_sales"
                                                                >
                                                                </vue-range-slider>
                                                            </div>
                                                            <h5 class="h5 border-bottom rounded py-2 m-0" style="width: 60px;">
                                                                @{{ detail_sales[1] }}%
                                                            </h5>
                                                        </div>
                                                        <div class="d-flex justify-content-between mt-2">
                                                            <h5 class="h5">Recent Sales</h5>
                                                            <h5 class="h5">Historical Sales</h5>
                                                        </div>
                                                        <h5 class="h5 text-primary1 mt-4">RECENT SALES</h5>
                                                        <div class="d-flex justify-content-between mt-4">
                                                            <p class="h5">7 Days: </p>
                                                            <div class="border-bottom">
                                                                <input type="text" class="border-0 h5" style="width: 50px;" v-model="detail_recent_sales_seven_days">
                                                                <span class="h5 text-light1">%</span>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex justify-content-between mt-4">
                                                            <p class="h5">14 Days: </p>
                                                            <div class="border-bottom">
                                                                <input type="text" class="border-0 h5" style="width: 50px;" v-model="detail_recent_sales_fourteen_days">
                                                                <span class="h5 text-light1">%</span>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex justify-content-between mt-4">
                                                            <p class="h5">30 Days: </p>
                                                            <div class="border-bottom">
                                                                <input type="text" class="border-0 h5" style="width: 50px;" v-model="detail_recent_sales_thirty_days">
                                                                <span class="h5 text-light1">%</span>
                                                            </div>
                                                        </div>
                                                        <div class="text-primary1 my-2">
                                                            <i class="bx bx-plus-circle font-size-16 align-middle" style=""></i>
                                                            Add Custom
                                                        </div>
                                                        <hr>
                                                        <div class="d-flex justify-content-between">
                                                            <h5 class="h5 border-bottom rounded py-2 m-0" style="width: 60px;">
                                                                @{{ detail_trend }}%
                                                            </h5>
                                                            <div class="w-75 d-flex align-items-center">
                                                                <vue-range-slider
                                                                    :width="'100%'"
                                                                    :bg-style="bgStyle" :tooltip-style="tooltipStyle" :process-style="processStyle"
                                                                    v-model="detail_trend"
                                                                >
                                                                </vue-range-slider>
                                                            </div>
                                                        </div>
                                                        <h5 class="h5 mt-2">TREND</h5>
                                                        <div class="d-flex justify-content-between mt-4">
                                                            <p class="h5">YOY: </p>
                                                            <div class="border-bottom">
                                                                <input type="text" class="border-0 h5" style="width: 50px;" v-model="detail_trend_yoy">
                                                                <span class="h5 text-light1">%</span>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex justify-content-between mt-4">
                                                            <p class="h5">Monthly: </p>
                                                            <div class="border-bottom">
                                                                <input type="text" class="border-0 h5" style="width: 50px;" v-model="detail_trend_monthly">
                                                                <span class="h5 text-light1">%</span>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex justify-content-between mt-4">
                                                            <p class="h5">Weekly: </p>
                                                            <div class="border-bottom">
                                                                <input type="text" class="border-0 h5" style="width: 50px;" v-model="detail_trend_weekly">
                                                                <span class="h5 text-light1">%</span>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex justify-content-between mt-4">
                                                            <p class="h5">30 Over 30: </p>
                                                            <div class="border-bottom">
                                                                <input type="text" class="border-0 h5" style="width: 50px;" v-model="detail_trend_30_over_30">
                                                                <span class="h5 text-light1">%</span>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex justify-content-between mt-4 mb-4">
                                                            <p class="h5">7 Over 7: </p>
                                                            <div class="border-bottom">
                                                                <input type="text" class="border-0 h5" style="width: 50px;" v-model="detail_trend_7_over_7">
                                                                <span class="h5 text-light1">%</span>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="d-flex mt-4 mb-4">
                                                            <p class="h4 mr-4">Add Safety </p>
                                                            <div class="border-bottom">
                                                                <input type="text" class="border-0 h5" style="width: 30px;" v-model="detail_add_safety">
                                                                <span class="h5 text-light1">%</span>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex justify-content-between mt-4 mb-3">
                                                            <p class="h4 mr-4">Recommended Settings </p>
                                                            <div class="d-flex align-items-center">
                                                                <div class="custom-control custom-switch">
                                                                    <input type="checkbox" id="customSwitch-recommended-settings" class="custom-control-input" v-model="detail_recommended_settings">
                                                                    <label for="customSwitch-recommended-settings" class="custom-control-label"></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <p>You have 7 pending items to review to more accurately adjust your custom logic.</p>
                                                        <button class="btn btn-primary w-100">Save</button>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-8">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="d-flex">
                                                            <div class="mr-4">
                                                                <p class="h6">REORDER DATE</p>
                                                                <p class="h5">
                                                                    Feb 1,2020
                                                                    <span style="color: #F1B44C; font-size: 12px;">15 Days Left</span>
                                                                </p>
                                                            </div>
                                                            <div class="mr-4">
                                                                <p class="h6 mr-3">Actual Reorder Date</p>
                                                                <p class="h5">
                                                                    Feb 10,2020
                                                                </p>
                                                            </div>
                                                            <div class="mr-4">
                                                                <p class="h6 mr-3">Order Date Range</p>
                                                                <p class="h5">
                                                                    May 1 , 2020 - June 1 , 2020
                                                                    <span class="text-primary1" style="font-size: 12px;">30 Days</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="d-flex">
                                                            <div class="mr-4">
                                                                <p class="h6">INVENTORY</p>
                                                                <p class="h5">5000</p>
                                                            </div>
                                                            <div class="mr-4">
                                                                <p class="h6">DOS</p>
                                                                <p class="h5">90</p>
                                                            </div>
                                                            <div class="mr-4">
                                                                <p class="h6">REORDER QTY</p>
                                                                <p class="h5">4300</p>
                                                            </div>
                                                            <div class="mr-4">
                                                                <p class="h6">REORDER COST</p>
                                                                <p class="h5">$15,530</p>
                                                            </div>
                                                            <div class="mr-4">
                                                                <p class="h6">PROFIT</p>
                                                                <p class="h5">$4.350</p>
                                                            </div>
                                                            <div class="mr-4">
                                                                <p class="h6">ROI</p>
                                                                <p class="h5">72.9%</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <template v-if="detail.settings_applied_card_show">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <div class="d-flex justify-content-between align-items-center">
                                                                <p class="h6 text-primary1 m-0">SETTINGS APPLIED</p>
                                                                <button class="btn btn-light py-1" @click="detail.settings_applied_card_show = false">↑</button>
                                                            </div>
                                                            <div class="d-flex justify-content-between align-items-center mt-2">
                                                                <p class="h5 m-0">RECENT SALES
                                                                    <span class="border-bottom">30%</span>
                                                                </p>
                                                                <p class="m-0">CALCULATED DAY RATE: <span class="font-weight-medium">9</span></p>
                                                            </div>
                                                            <table class="table table-borderless mb-0">
                                                                <tbody>
                                                                <tr>
                                                                    <td>SETTINGS</td>
                                                                    <td>DAYS</td>
                                                                    <td>UNIT SOLD</td>
                                                                    <td>DAY RATE</td>
                                                                    <td>DOS</td>
                                                                    <td>STOCKOUT DATE</td>
                                                                    <td>REORDER DATE</td>
                                                                    <td>REORDER DATE RANGE</td>
                                                                    <td>REORDER QTY</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="border-bottom">50%</td>
                                                                    <td class="bg-light">7</td>
                                                                    <td class="bg-light">400</td>
                                                                    <td class="bg-light">10</td>
                                                                    <td class="bg-light">90</td>
                                                                    <td class="bg-light">Feb 10, 2020</td>
                                                                    <td class="bg-light">Feb 1, 2020</td>
                                                                    <td class="bg-light">Feb 1- Feb 10, 2020</td>
                                                                    <td class="bg-light">400</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="border-bottom">50%</td>
                                                                    <td>14</td>
                                                                    <td>400</td>
                                                                    <td>10</td>
                                                                    <td>90</td>
                                                                    <td>Feb 10, 2020</td>
                                                                    <td>Feb 1, 2020</td>
                                                                    <td>Feb 1- Feb 10, 2020</td>
                                                                    <td>400</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="border-bottom">50%</td>
                                                                    <td class="bg-light">30</td>
                                                                    <td class="bg-light">400</td>
                                                                    <td class="bg-light">10</td>
                                                                    <td class="bg-light">90</td>
                                                                    <td class="bg-light">Feb 10, 2020</td>
                                                                    <td class="bg-light">Feb 1, 2020</td>
                                                                    <td class="bg-light">Feb 1- Feb 10, 2020</td>
                                                                    <td class="bg-light">400</td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                            <div class="d-flex justify-content-between align-items-center mt-4">
                                                                <p class="h5 m-0">HISTORICAL SALES
                                                                    <span class="border-bottom mx-2 px-2">70%</span>
                                                                </p>
                                                                <div class="d-flex">
                                                                    <p class="m-0 mr-3">DOS: <span class="font-weight-medium">95</span></p>
                                                                    <p class="m-0 mr-3">Reorder Date : <span class="font-weight-medium">Feb 10 , 2020</span></p>
                                                                    <p class="m-0">Reorder Qty : <span class="font-weight-medium">800</span></p>
                                                                </div>
                                                            </div>
                                                            <table class="table table-borderless mb-0">
                                                                <tbody>
                                                                <tr>
                                                                    <td>MONTH</td>
                                                                    <td>INVENTORY</td>
                                                                    <td>LAST YEAR SALES</td>
                                                                    <td>DAY RATE</td>
                                                                </tr>
                                                                <tr style="background: #F8F9FA;">
                                                                    <td>Jan</td>
                                                                    <td>600</td>
                                                                    <td>300</td>
                                                                    <td>10</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Feb</td>
                                                                    <td>600</td>
                                                                    <td>300</td>
                                                                    <td>10</td>
                                                                </tr>
                                                                <tr style="background: #F8F9FA;">
                                                                    <td>Apr</td>
                                                                    <td>600</td>
                                                                    <td>300</td>
                                                                    <td>10</td>
                                                                </tr>
                                                                <tr style="background: #E8F8D7;">
                                                                    <td>Apr 15</td>
                                                                    <td>600</td>
                                                                    <td>300</td>
                                                                    <td>10</td>
                                                                </tr>
                                                                <tr style="background: #DAF4C0;">
                                                                    <td>May 15</td>
                                                                    <td>600</td>
                                                                    <td>300</td>
                                                                    <td>10</td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                            <div class="d-flex align-items-center mt-4">
                                                                <p class="h5 m-0">
                                                                    TRENDS
                                                                    <span class="border-bottom mx-2 px-2">50%</span>
                                                                </p>
                                                                <div>
                                                                    <p class="m-0 mr-3">CALC.TREND: <span class="font-weight-medium"><span style="color: #77D617;">+</span>132.21%</span></p>
                                                                </div>
                                                            </div>

                                                            <table class="table table-borderless mb-0">
                                                                <tbody>
                                                                <tr>
                                                                    <td class="border-bottom">50%</td>
                                                                    <td>YOY</td>
                                                                    <td><span style="color: #77D617;">+</span>276.34%</td>
                                                                    <td class="border-bottom">50%</td>
                                                                    <td>WEEKLY</td>
                                                                    <td><span style="color: #F68989;">-</span>276.34%</td>
                                                                    <td class="border-bottom">0%</td>
                                                                    <td>30 OVER 30</td>
                                                                    <td><span style="color: #77D617;">+</span>276.34%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="border-bottom">50%</td>
                                                                    <td>YOY</td>
                                                                    <td><span style="color: #77D617;">+</span>276.34%</td>
                                                                    <td class="border-bottom">50%</td>
                                                                    <td>WEEKLY</td>
                                                                    <td><span style="color: #F68989;">-</span>276.34%</td>
                                                                </tr>
                                                                </tbody>
                                                            </table>

                                                        </div>
                                                    </div>
                                                </template>
                                                <template v-else>
                                                    <div class="d-flex justify-content-end">
                                                        <button class="btn btn-outline-secondary" @click="detail.settings_applied_card_show = true">
                                                            <i class="mdi mdi-flask-round-bottom-empty-outline mdi-rotate-180"></i> Logic + Data
                                                        </button>
                                                    </div>
                                                </template>
                                                <div class="d-flex">
                                                    <div class="m-2 h5" :class="detail_forecast_tab == 'table' ? 'text-dark' : 'text-light1'" @click="detail_forecast_tab = 'table'" style="cursor: pointer;">
                                                        <i class="mdi mdi-table-large" :class="detail_forecast_tab == 'table' ? 'text-primary1' : 'text-light1'"></i>
                                                        Table
                                                    </div>
                                                    <div class="m-2 h5" :class="detail_forecast_tab == 'calendar' ? 'text-dark' : 'text-light1'" @click="detail_forecast_tab = 'calendar'" style="cursor: pointer;">
                                                        <i class="mdi mdi-calendar-blank" :class="detail_forecast_tab == 'calendar' ? 'text-primary1' : 'text-light1'"></i>
                                                        Calendar
                                                    </div>
                                                    <div class="m-2 h5" :class="detail_forecast_tab == 'graph' ? 'text-dark' : 'text-light1'" @click="detail_forecast_tab = 'graph'" style="cursor: pointer;">
                                                        <i class="mdi mdi-chart-line-variant" :class="detail_forecast_tab == 'graph' ? 'text-primary1' : 'text-light1'"></i>
                                                        Graph
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-body">
                                                        <template v-if="detail_forecast_tab == 'table'">
                                                            <table class="table table-borderless mb-0">
                                                                <tbody>
                                                                <tr>
                                                                    <td>MONTH</td>
                                                                    <td>INVENTORY</td>
                                                                    <td>PO</td>
                                                                    <td>STOCKOUT DAYS</td>
                                                                    <td>BLACKOUT DAYS</td>
                                                                    <td>CALC.DAY RATE</td>
                                                                    <td>PROJECTED DEMAND</td>
                                                                    <td>PROJECTED SALES</td>
                                                                </tr>
                                                                <tr style="background: #F8F9FA">
                                                                    <td>Jan</td>
                                                                    <td>600</td>
                                                                    <td>0</td>
                                                                    <td>0</td>
                                                                    <td>0</td>
                                                                    <td>5</td>
                                                                    <td>100</td>
                                                                    <td>100</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Feb</td>
                                                                    <td>600</td>
                                                                    <td>0</td>
                                                                    <td>0</td>
                                                                    <td>0</td>
                                                                    <td>5</td>
                                                                    <td>100</td>
                                                                    <td>100</td>
                                                                </tr>
                                                                <tr style="background: #F8F9FA;">
                                                                    <td>Mar</td>
                                                                    <td>600</td>
                                                                    <td>0</td>
                                                                    <td>0</td>
                                                                    <td>0</td>
                                                                    <td>5</td>
                                                                    <td>100</td>
                                                                    <td>100</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Apr</td>
                                                                    <td>600</td>
                                                                    <td>0</td>
                                                                    <td>0</td>
                                                                    <td>0</td>
                                                                    <td>5</td>
                                                                    <td>100</td>
                                                                    <td>100</td>
                                                                </tr>
                                                                <tr style="background: #DEEBFF;">
                                                                    <td>May 15</td>
                                                                    <td>600</td>
                                                                    <td>0</td>
                                                                    <td>0</td>
                                                                    <td>0</td>
                                                                    <td>5</td>
                                                                    <td>100</td>
                                                                    <td>100</td>
                                                                </tr>
                                                                <tr style="background: #BED9FF">
                                                                    <td>Jun 15</td>
                                                                    <td>600</td>
                                                                    <td>0</td>
                                                                    <td>0</td>
                                                                    <td>0</td>
                                                                    <td>5</td>
                                                                    <td>100</td>
                                                                    <td>100</td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                            <div class="d-flex justify-content-end mt-4">
                                                                <div class="ml-4">
                                                                    <p class="mb-2">PROJECTED SALES</p>
                                                                    <h5>200</h5>
                                                                </div>
                                                                <div class="ml-4">
                                                                    <p class="mb-2">ADD SAFETY</p>
                                                                    <h5><span class="border-bottom">10%</span> 20 </h5>
                                                                </div>
                                                                <div class="text-primary1 ml-4">
                                                                    <p class="mb-2">REORDER QTY</p>
                                                                    <h5>220</h5>
                                                                </div>
                                                            </div>
                                                        </template>
                                                        <template v-else-if="detail_forecast_tab == 'calendar'">
                                                            <div class="d-flex justify-content-end align-items-center">
                                                                <div class="custom-control custom-switch mr-3">
                                                                    <input type="checkbox" id="customSwitch-show-adjusted-sales" class="custom-control-input">
                                                                    <label for="customSwitch-show-adjusted-sales" class="custom-control-label">
                                                                        Show Adjusted Sales
                                                                    </label>
                                                                </div>
                                                                <button class="btn px-1">
                                                                    <i class="mdi mdi-chevron-double-left"></i>
                                                                </button>
                                                                <button class="btn px-1">
                                                                    <i class="mdi mdi-chevron-left mdi-24px"></i>
                                                                </button>
                                                                <button class="btn px-1">
                                                                    <i class="mdi mdi-chevron-right mdi-24px"></i>
                                                                </button>
                                                                <button class="btn px-1">
                                                                    <i class="mdi mdi-chevron-double-right"></i>
                                                                </button>
                                                            </div>
                                                            <div class="row justify-content-between">
                                                                <div class="col-auto" v-for="i in 6">
                                                                    <h5 class="text-dark my-4 ml-1">@{{ months[detail.calendar_tab.start.month + i] }} @{{ detail.calendar_tab.start.year }}</h5>
                                                                    <div class="row no-gutters" v-for="j in 5">
                                                                        <div class="col" v-for="k in 7">
                                                                            <div v-if="(j-1)*7+k < 32" class="bg-light rounded d-flex align-items-center justify-content-center m-1" style="width: 28px; height: 28px;">
                                                                                @{{ (j-1)*7+k }}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                            <div class="d-flex align-items-center">
                                                                <div class="rounded d-flex align-items-center justify-content-center m-1" style="width: 28px; height: 28px; background: #495057;"></div>
                                                                <h6 class="mb-0 mr-3">Blackout Days</h6>
                                                                <div class="rounded d-flex align-items-center justify-content-center m-1" style="width: 28px; height: 28px; background: #227CFF;"></div>
                                                                <h6 class="mb-0 mr-3">Reorder Date </h6>
                                                                <div class="rounded d-flex align-items-center justify-content-center m-1" style="width: 28px; height: 28px; background: #556EE6;"></div>
                                                                <h6 class="mb-0 mr-3">Reorder Date Range</h6>
                                                                <div class="rounded d-flex align-items-center justify-content-center m-1" style="width: 28px; height: 28px; background: #F46A6A;"></div>
                                                                <div class="rounded d-flex align-items-center justify-content-center m-1 position-relative" style="width: 28px; height: 28px; background: #CED4DA;">
                                                                    <div class="rounded-circle" style="position: absolute; top: -3px; right: -3px; width: 10px; height: 10px; background: #F46A6A;"></div>
                                                                </div>
                                                                <h6 class="mb-0 mr-3">Stockout</h6>
                                                                <div class="rounded d-flex align-items-center justify-content-center m-1 position-relative" style="width: 28px; height: 28px; background: #CED4DA;">
                                                                    <div class="rounded-circle" style="position: absolute; top: -3px; right: -3px; width: 10px; height: 10px; background: #77D617;"></div>
                                                                </div>
                                                                <h6 class="mb-0 mr-3">Incoming PO</h6>
                                                            </div>
                                                        </template>
                                                        <template v-else-if="detail_forecast_tab == 'graph'">
                                                            <div class="row no-gutters">
                                                                <div class="col-auto">
                                                                    <button class="btn rounded border mr-4 mb-3">
                                                                        Last Year Sales
                                                                    </button>
                                                                </div>
                                                                <div class="col-auto">
                                                                    <button class="btn rounded border mr-4 mb-3">
                                                                        Sales
                                                                    </button>
                                                                </div>
                                                                <div class="col-auto">
                                                                    <button class="btn rounded border mr-4 mb-3">
                                                                        Historical Inventory
                                                                    </button>
                                                                </div>
                                                                <div class="col-auto">
                                                                    <button class="btn rounded border mr-4 mb-3">
                                                                        Stocked Out
                                                                    </button>
                                                                </div>
                                                                <div class="col-auto">
                                                                    <button class="btn rounded border mr-4 mb-3">
                                                                        Incoming PO
                                                                    </button>
                                                                </div>
                                                                <div class="col-auto">
                                                                    <button class="btn rounded border mr-4 mb-3">
                                                                        Adjusted Sales
                                                                    </button>
                                                                </div>
                                                                <div class="col-auto">
                                                                    <button class="btn rounded border mr-4 mb-3">
                                                                        Inventory
                                                                    </button>
                                                                </div>
                                                                <div class="col-auto">
                                                                    <button class="btn rounded border mr-4 mb-3">
                                                                        Price
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <line-chart :chartdata="detail.graph_tab.chartdata" :options="detail.graph_tab.options"></line-chart>
                                                            <div class="d-flex align-items-center">
                                                                <div class="rounded d-flex align-items-center justify-content-center m-1" style="width: 28px; height: 28px; background: #FF9122;"></div>
                                                                <h6 class="mb-0 mr-3">Price</h6>
                                                                <div class="rounded d-flex align-items-center justify-content-center m-1" style="width: 28px; height: 28px; background: #227CFF;"></div>
                                                                <h6 class="mb-0 mr-3">Sales</h6>
                                                                <div class="rounded d-flex align-items-center justify-content-center m-1" style="width: 28px; height: 28px; background: #227CFF;"></div>
                                                                <h6 class="mb-0 mr-3">Reorder Date</h6>
                                                                <div class="rounded d-flex align-items-center justify-content-center m-1" style="width: 28px; height: 28px; background: #5BC7FF;"></div>
                                                                <h6 class="mb-0 mr-3">Reorder Date Range</h6>
                                                                <div class="rounded-circle d-flex align-items-center justify-content-center m-1" style="width: 28px; height: 28px; background: #77D617;"></div>
                                                                <h6 class="mb-0 mr-3">Incoming PO</h6>
                                                                <div class="rounded d-flex align-items-center justify-content-center m-1" style="width: 28px; height: 28px; background: #F46A6A;"></div>
                                                                <div class="rounded d-flex align-items-center justify-content-center m-1 position-relative" style="width: 28px; height: 28px; background: #CED4DA;">
                                                                    <div class="rounded-circle" style="position: absolute; top: -3px; right: -3px; width: 10px; height: 10px; background: #F46A6A;"></div>
                                                                </div>
                                                                <h6 class="mb-0 mr-3">Stockout</h6>
                                                                <div class="rounded d-flex align-items-center justify-content-center m-1 position-relative" style="width: 28px; height: 28px; background: #CED4DA;">
                                                                    <div class="rounded-circle" style="position: absolute; top: -3px; right: -3px; width: 10px; height: 10px; background: #77D617;"></div>
                                                                </div>
                                                                <h6 class="mb-0 mr-3">Incoming PO</h6>
                                                            </div>
                                                        </template>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-body">
                                                        <h6 class="mb-4"><i class="mdi mdi-bookmark" style="color: #FF9122;"></i>Insights</h6>
                                                        <div class="d-flex justify-content-between">
                                                            <div class="mr-4">
                                                                <h6>Prime Day</h6>
                                                                <p>
                                                                    Lorem ipsum dolor sit amet, consectetur
                                                                    adipiscing elit.
                                                                </p>
                                                            </div>
                                                            <div class="mx-4">
                                                                <h6>Diwali</h6>
                                                                <p>
                                                                    Lorem ipsum dolor sit amet, consectetur
                                                                    adipiscing elit.
                                                                </p>
                                                            </div>
                                                            <div class="ml-4">
                                                                <h6>Christmas</h6>
                                                                <p>
                                                                    Lorem ipsum dolor sit amet, consectetur
                                                                    adipiscing elit.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </template>
                        </template>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection

@section('script')
    <script src="{{ asset('js/lib/vue-range-slider.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
    <script src="https://unpkg.com/vue-chartjs/dist/vue-chartjs.min.js"></script>
    <script>
        Vue.component('line-chart', {
            extends: VueChartJs.Bar,
            props: {
                chartdata: {
                    type: Object,
                    default: null
                },
                options: {
                    type: Object,
                    default: null
                }
            },
            mounted () {
                this.renderChart(this.chartdata, this.options)
            }
        })

        let content = new Vue({
            el: '#content',
            data: {
                currentPage: 1,
                perPage: 20,
                selectPage: false,
                selectAll: false,
                selected: [],

                fields: [
                    { caption: 'Image', slug: 'image' },
                    { caption: 'Title', slug: 'title' },
                    { caption: 'ASIN', slug: 'asin' },
                    { caption: 'SKU', slug: 'sku' },
                    { caption: 'Date TO REORDER', slug: 'date_to_reorder' },
                    { caption: 'Reorder Qty', slug: 'reorder_qty' },
                    { caption: 'Reorder Cast', slug: 'reorder_cast' },
                    { caption: 'Reorder Profit', slug: 'reorder_profit' },
                    { caption: 'Reorder ROI', slug: 'reorder_roi' },
                ],
                cols: [],

                rows:[],

                openedRow: null,

                sortField: null,
                sortDirection: null,

                detail_logic_type: true, // true - Reorder Logic, false - DOS Logic
                detail_is_custom: true,
                detail_sales: [0, 100],
                detail_recent_sales_seven_days: '',
                detail_recent_sales_fourteen_days: '',
                detail_recent_sales_thirty_days: '',

                detail_trend: 50,
                detail_trend_yoy: 50,
                detail_trend_monthly: 50,
                detail_trend_weekly: 50,
                detail_trend_30_over_30: 50,
                detail_trend_7_over_7: 50,
                detail_add_safety: 2,

                detail_recommended_settings: true,
                detail_forecast_tab: 'graph',

                detail: {
                    settings_applied_card_show: false,
                    calendar_tab: {
                        start: {
                            year: 2020,
                            month: 0,
                        }
                    },
                    graph_tab: {
                        chartdata: {
                            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                            datasets: [
                                {
                                    label: 'Line',
                                    type: 'line',
                                    data: [30, 25, 10, 30, 35,  75, 35]
                                },
                                {
                                    label: 'Data One',
                                    backgroundColor: '#E2E6F0',
                                    data: [40, 39, 10, 40, 39, 80, 40]
                                },
                                {
                                    label: 'Data Two',
                                    backgroundColor: '#CAD0E5',
                                    data: [40, 39, 10, 40, 39, 80, 40]
                                },

                            ]
                        },
                        options: {responsive: true, maintainAspectRatio: false}
                    }
                },

                renderKey: 0,

                months : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ],
            },

            created(){
                {{--if({{$cols}})    --}}
                this.cols = this.fields.map(i => i.slug)

                for( i=0 ; i < 90; i++){ this.rows.push({id: i})}

                this.min = 0
                this.max = 100
                this.bgStyle = {
                    backgroundColor: '#CEE2FF',
                }
                this.tooltipStyle = {
                    display: 'none',
                }
                this.processStyle = {
                    backgroundColor: '#227CFF'
                }
            },

            computed: {
                processedRows: function(){
                    return this.rows
                    // let filtered = this.rows.filter(item => {
                    //     let props = Object.values(item)
                    //
                    //     return props.some(prop => !this.search || ((typeof prop === 'string') ? prop.includes(this.search) : false))
                    //     // return props.some(prop => !this.search || ((typeof prop === 'string') ? prop.includes(this.search) : prop.toString(10).includes(this.search)))
                    // })
                    //     .filter(item => {
                    //         if(this.inComplete)  return !item.completeFlag;
                    //         return true;
                    //     })
                    //
                    // return filtered.sort((a,b) => {
                    //     let modifier = 1;
                    //     if(this.sortDirection === 'desc') modifier = -1;
                    //     if(this.sortField == 'country'){
                    //         if(this.countries[a.country_id] < this.countries[b.country_id]) return -1 * modifier;
                    //         if(this.countries[a.country_id] > this.countries[b.country_id]) return 1 * modifier;
                    //     }else{
                    //         if(a[this.sortField] < b[this.sortField]) return -1 * modifier;
                    //         if(a[this.sortField] > b[this.sortField]) return 1 * modifier;
                    //     }
                    //     return 0;
                    // });
                },

                lastPage(){
                    return Math.floor( this.processedRows.length / this.perPage) + ( this.processedRows.length % this.perPage ? 1 : 0 )
                },

            },

            methods:{
                updateSelectPage(){
                    if(this.selectPage) {
                        this.selected = this.processedRows.slice(this.perPage * (this.currentPage - 1), this.perPage * this.currentPage).map( i => i.id)
                    }else{
                        this.selected = []
                    }
                    this.selectAll = false
                },
                updatedSelected(){
                    this.selectAll = false
                    this.selectPage = false
                },

                columns(){
                    return this.fields.filter( field => this.cols.includes(field.slug))
                },

                updateCols(){
                    {{--fetch("{{ route('update_vendors_cols') }}", {--}}
                    {{--    method: 'POST',--}}
                    {{--    headers: {--}}
                    {{--        'Content-Type': 'application/json',--}}
                    {{--    },--}}
                    {{--    body: JSON.stringify({--}}
                    {{--        _token: '{{ csrf_token() }}',--}}
                    {{--        cols : this.cols--}}
                    {{--    }),--}}
                    {{--})--}}
                },

                nextPage(){
                    if(this.currentPage < this.lastPage) this.currentPage++ ;
                },

                prevPage(){
                    if(this.currentPage > 1 ) this.currentPage-- ;
                },

                sortBy(field){
                    if(this.sortField == field){
                        this.sortDirection = this.sortDirection == 'asc' ? 'desc' : 'asc'
                    }else{
                        this.sortDirection = 'asc'
                    }

                    this.sortField = field
                },
            }
        })
    </script>
@endsection


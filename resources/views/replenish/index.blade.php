@extends('layouts.master')

@section('title') Replenish List @endsection

@section('content')

    <div class="row">
        <div class="col-12">

            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Replenish</h4>
                <input type="hidden" value="{{$marketplace}}" name="marketplaces" id="marketplaces">

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Replenish</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">

            </div>
        </div>
    </div>
    <!-- end row -->
    <div class="card">

        <div class="card-body" style="margin-bottom: 30px;">
            <h4 class="card-title mb-4">Replenish List</h4>
            <div class="box">
                <div class="table-responsive">
                    <table id="datatables" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead class="thead-light">
                        <tr>
                            <th>No.</th>
                            <th>Image</th>
                            <th>Title</th>
                            <th>SKU</th>
                            <th>ASIN</th>
                            <th>Price</th>
                            <!--<th>Inventory</th>
                            <th>Days Of Supply</th>
                            <th>Replenish Date</th>-->
                            <th>Projected Units</th>
                            <!--<th>Current Inventory</th>
                            <th>POs</th>
                            <th>Warehouses</th>
                            <th>FBA</th>-->
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

@endsection
        @section('script')
            <script>
                $(document).ready(function () {
                    loadDashboard();
                });

                function loadDashboard(){
                    var count=1;
                    var insert_type = 'replenish_data';
                    var table = $('#datatables').DataTable({
                        "order": [ 0, 'asc' ],
                        "bSort": true,
                        "paging": true,
                        "bInfo": true,
                        "bDestroy": true,
                        "bFilter": true,
                        "searching": true,
                        "bPaginate": true,
                        "bProcessing": true,
                        "language": {
                            "loadingRecords": '&nbsp;',
                            "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                        },
                        'ajax': {
                            "type": "POST",
                            "url": "{{ route('replenish.store') }}",
                            "data": function (d) {
                                d._token= "{{csrf_token()}}",d.insert_type=insert_type
                            },
                            "dataType": 'json',
                            "dataSrc": "",
                            "timeout":1000000,
                            "async": true,
                            "cache": true
                        },
                        'columnDefs': [
                            {
                                targets: 0,
                                render: function (data, type, row) {
                                    return row['id'];
                                }
                            },
                            {
                                targets: 1,
                                render: function (data, type, row) {
                                    return row['prod_image'];
                                }
                            },
                            {
                                targets: 2,
                                render: function (data, type, row) {
                                    return row['prod_name'];
                                }
                            },
                            {
                                targets: 3,
                                render: function (data, type, row) {
                                    return row['sku'];

                                }
                            },
                            {
                                targets: 4,
                                render: function (data, type, row) {
                                    return row['asin'];
                                }
                            },
                            {
                                targets: 5,
                                render: function (data, type, row) {
                                    return row['price'];
                                }
                            },
                            /*{
                                targets: 6,
                                render: function (data, type, row) {
                                    return row['inventory'];
                                }
                            },
                            {
                                targets: 7,
                                render: function (data, type, row) {
                                    return row['days_of_supply']
                                }
                            },
                            {
                                targets: 8,
                                render: function (data, type, row) {
                                    return row['replenish_date']
                                }
                            },*/
                            {
                                targets: 6,
                                render: function (data, type, row) {
                                    return row['projected_units']
                                }
                            },
                            /*{
                                targets: 10,
                                render: function (data, type, row) {
                                    return row['current_inventory']
                                }
                            },
                            {
                                targets: 11,
                                render: function (data, type, row) {
                                    return row['pos']
                                }
                            },
                            {
                                targets: 12,
                                render: function (data, type, row) {
                                    return row['warehouses']
                                }
                            },
                            {
                                targets: 13,
                                render: function (data, type, row) {
                                    return row['fba']
                                }
                            },*/
                            {
                                targets: 7,
                                render: function (data, type, row) {
                                    return row['action']
                                }
                            }
                        ]
                    });
                }
                </script>

                <script>
                $(document).on('click', '.confirm_shipment', function (e) {
                     e.preventDefault();
                    var id = $(this).data('id');
                    swal({
                     title: "Are you sure?",
                     text: "You want to confirm this shipment",
                     type: "warning",
                     showCancelButton: true,
                     confirmButtonColor: "#DD6B55",
                     confirmButtonText: "Yes, confirm it!",
                     cancelButtonText: "No, confirm please!",
                     closeOnConfirm: false,
                     closeOnCancel: false
                     },
                     function(isConfirm) {
                     if (isConfirm) {
                         $.ajax({
                         type: "POST",
                         url: "{{ route('replenish.store') }}",
                         data: {id :id,request_type:'confirm_type' },
                         dataType: "html",
                         success: function (data) {
                         swal("Done!", "It was succesfully confirmed!", "success");
                            location.reload(true);
                         }
                         });
                     }
                     else
                     {
                        swal("Cancelled", "", "error");
                     }
                     });
                });
               
</script>
@endsection
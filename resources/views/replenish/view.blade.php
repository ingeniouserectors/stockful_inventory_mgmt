@extends('layouts.master')

@section('title') Replenish Purchase Order View @endsection

@section('content')

    <style>
        .design{
            font-style:bold;
            font-size:14px;
        }
    </style>

    <style>
        table, th, td {
            border: 2px solid ;
            border-collapse: collapse;
        }
    </style>
    <div class="row">
        <div class="col-12">

            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Replenish Purchase Order View</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('replenish.index')}}">Replenish</a></li>
                        <li class="breadcrumb-item active"> Replenish Purchase Order View</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="row">
        <div class="col-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">

                <div class="card-body">
                    <div class="form-group row">
                        <div class="form-group row">
                                     <div class="col-md-2">
                                         <img width="100px" src="{{$product_data['prod_image']}}" title="{{$product_data['prod_names']}}">
                                     </div>
                                     <div class="col-md-10">
                                         {{ $product_data['prod_name']}}
                                         <hr>
                                         {{ $product_data['sku']}}
                                     </div>
                                </div>
                    </div>
                    <div class="container">

    
  <!-- Button to Open the Modal -->
                    <div align="right" style="margin-bottom:15px;">
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                        Add to shipment
                      </button>
                    </div>
                      <!-- The Modal -->
                      <div class="modal" id="myModal">
                        <div class="modal-dialog">
                          <div class="modal-content">     
                            <!-- Modal Header -->
                              <form class="" id="view-replenish" action="{{ route('replenish.store') }}" method="post">
                                  @csrf
                                  <input type="hidden" name="product_id" id="product_id" value="{{$product_data['id']}}">
                                  <input type="hidden" name="sku" id="sku" value="{{$product_data['sku']}}">
                                  <input type="hidden" name="insert_type" value="create_shipment" >

                                  <div class="modal-header">

                             <h4 class="modal-title span7 text-left" id="myModalLabel"><span class="title">Shipment create on Amazon</span></h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                                <div class="form-group row">
                                    <label for="input_email" class="control-label col-md-3">Quantity:</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control allow_integer" min="1" id="quantity" name="quantity" placeholder="">
                                    </div>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Create shipment</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                              </form>
                        </div>
                      </div>  
                    </div>

                    <div class="box">

                        <table class="table table-striped table-bordered dt-responsive nowrap datatable" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead class="thead-light">
                            <tr>
                                <th>No.</th>
                                <th>PO</th>
                                <th>Vendor/Supplier </th>
                                <th>Warehouse</th>
                                <th>Days Of Supply</th>
                                <th>Replenish Date</th>
                                <th>Projected Units</th>
                                <th>FBA</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1; ?>
                            @if(!empty($product_details_data))
                                @foreach($product_details_data as $users)
                                    @php($get_po_from_po_details = ' - ')
                                    @php($get_po_from_po_details_days_of_supply = ' - ')
                                    @php($vendors_suppliers_name = ' - ')
                                    @php($get_warehouse_name = ' - ')

                                    @if($users['supplier'] != 0)
                                        @php($vendors_suppliers_name = get_suppliers_name($users['supplier']))
                                    @endif
                                    @if($users['vendor'] != 0)
                                        @php($vendors_suppliers_name = get_vendors_name($users['vendor']))
                                    @endif
                                    @if($users['warehouse'])
                                        @php($get_warehouse_name = get_warehouse_name($users['warehouse']))
                                    @endif
                                    @if($users['purchase_orders_id'])
                                        @php($get_po_from_po_details_days_of_supply = get_po_from_po_details_days_of_supply($users['purchase_orders_id']))
                                        @php($get_po_from_po_details = get_po_from_po_details($users['purchase_orders_id']))
                                    @endif

                                    <tr id="row{{$users['id']}}">
                                        @php($get_purchase_order = get_purchase_order_details($users['purchase_orders_id']))
                                        @php($get_warehouse_lead_time = get_warehouse_leadtime($users['warehouse']))
                                        @php($date_cur_plus = ' - ')
                                        <?php $arrived_date = $get_purchase_order->date_arrived;
                                              $lead_time = $get_warehouse_lead_time == 1 ? ' +'.$get_warehouse_lead_time.' day' : ' +'.$get_warehouse_lead_time.' days';
                                              $date_cur_plus = date('Y-m-d', strtotime($arrived_date. ' + '.$get_warehouse_lead_time.' days'));
                                        ?>


                                        <td id="rows_no{{$users['id']}}">{{$no++}}</td>
                                        <td id="rows_no{{$users['purchase_orders_id']}}">{{$get_po_from_po_details}}</td>
                                        <td id="product_suppliers_name{{$users['id']}}">{{$vendors_suppliers_name}}</td>
                                        <td id="warehouse{{$users['id']}}">{{$get_warehouse_name}}</td>
                                        <td id="days_of_supply{{$users['id']}}"> {{$get_po_from_po_details_days_of_supply}} </td>
                                        <td id="replenish_date{{$users['id']}}"> {{$date_cur_plus}} </td>
                                        <td id="projected_units{{$users['id']}}">{{$users['projected_units']}}</td>
                                        <td id="replenish_date{{$users['id']}}"> - </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="button-items mt-3" align="center">
                    <a class="btn btn-info" href="{{ route('replenish.index') }}" role="button" style="width:116px;">Back</a>
                </div>
            </div>

            <!-- </div> -->
        </div>
        @endsection
@section('script')
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script>
    $(function () {
        $('.datatable').DataTable({
            "pageLength": 10,
            'autoWidth': false,
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
        });
    });
</script>
@endsection

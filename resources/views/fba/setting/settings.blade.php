@extends('layouts.master')

@section('title') FBA Setting @endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">FBA Setting</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">FBA Settings</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            @if(session()->has('message'))
                {!! session('message') !!}
            @endif
            <div class="card">
                <div class="card-body">
                    <form name="fba-setting" id="fba-setting" action="{{ route('fbasetting.store') }}" method="POST" onreset="myFunction()">
                        @csrf
                        <input type="hidden" name="insert_type" value="setting_form">

                        <input type="hidden" name="marketplace_id" id="marketplace_id" value="{{$marketplaces}}">

                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Supply Days</label>
                            <div class="col-md-8">
                                <input class="form-control allow_integer" type="text" name="supply_days" id="supply_days" value="{{ isset($fba_setting['supply_days']) ? $fba_setting['supply_days'] : '' }}" maxlength="2">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-md-2 col-form-label">Ship Boxes : </label>
                            <div class="col-md-4">
                                <input type="checkbox" id="ship_boxes-{{ isset($fba_setting['id']) ? $fba_setting['id'] : '' }}" value="1" name="ship_boxes" switch="success" @if(isset($fba_setting['ship_boxes']) ?$fba_setting['ship_boxes'] : '' == 1) checked @endif/>
                                <label for="ship_boxes-{{ isset($fba_setting['id']) ? $fba_setting['id'] : '' }}" data-on-label="Yes" data-off-label="No"></label>
                            </div>
                            <label for="example-text-input" class="col-md-2 col-form-label">Ship Pallets : </label>
                            <div class="col-md-4">
                                <input type="checkbox" id="ship_pallets-{{ isset($fba_setting['id']) ? $fba_setting['id'] : '' }}" value="1" name="ship_pallets" switch="success" @if(isset($fba_setting['ship_pallets']) ? $fba_setting['ship_pallets'] : '' == 1) checked @endif/>
                                <label for="ship_pallets-{{ isset($fba_setting['id']) ? $fba_setting['id'] : '' }}" data-on-label="Yes" data-off-label="No"></label>
                            </div>
                        </div>
                        <div class="button-items mt-3">
                            <input class="btn btn-info" type="submit" value="Submit" id="submit">
                            <a class="btn btn-danger waves-effect waves-light" href="{{ url('/index') }}" role="button">Cancel</a>
                            <input class="btn btn-warning" type="reset" value="Reset">
                        </div>
                    </form>

                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->

@endsection
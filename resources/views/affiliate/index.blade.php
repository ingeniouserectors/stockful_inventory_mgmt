@extends('layouts.master')

@section('title')  Affiliate Program List @endsection

@section('content')

@php(@$userRole = \App\Models\Users::with('user_assigned_role')->select('users.id')->where(array('id' => Auth::user()->id))->first())
@php(@$finalRole = $userRole['user_assigned_role']['user_role_id'])


        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18"> Affiliate Program</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active"> Affiliate Program</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        @php($get_check_setting = get_check_access('affiliate_module'))
        @php($get_usercheck_access = get_user_check_access('affiliate_module', 'create'))

        @if($get_check_setting == 1)
            @if($get_usercheck_access == 1)
                <div class="row">
                    <div class="col-lg-6">
                        <a class="btn btn-info waves-effect waves-light mb-3"  href="{{ route('affiliate_programs.create') }}" role="button"> Invite User</a>
                    </div>
                    <div class="col-lg-6"></div>
                </div>
            @endif
        @else
            @if($get_usercheck_access == 1)
                <div class="row">
                    <div class="col-lg-6">
                        <a class="btn btn-info waves-effect waves-light mb-3"  href="{{ route('affiliate_programs.create') }}" role="button"> Invite User</a>
                    </div>
                    <div class="col-lg-6"></div>
                </div>
            @endif
        @endif

        @if(session()->has('message'))
            {!! session('message') !!}
        @endif
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        @if($roleId==3 || $roleId==4)
                            <h4 class="card-title mb-4">Affiliate User List</h4>
                            <div class="table-responsive">
                                <table id="datatables" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead class="thead-light">
                                    <tr>
                                        <th>No.</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Total Commission</th>
                                    </tr>
                                    </thead>
                                    
                                </table>
                            </div>
                            @elseif($roleId==1 || $roleId==2)
                        <h4 class="card-title mb-4">Member List</h4>
                        <div class="table-responsive">
                                <table id="datatables" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead class="thead-light">
                                    <tr>
                                        <th>No.</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Total Commission</th>
                                        <th>Total Paid Commission</th>
                                        <th>Outstanding Commission</th>
                                    </tr>
                                </thead>
                                </table>
                        </div>
                        <!-- end table-responsive -->
                            @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function () {
                     loadDashboard();
        });
 
        function loadDashboard(){
           var affiliate_list='affiliate_list';
            var count=1;
            var table = $('#datatables').DataTable({

                "order": [ 0, 'asc' ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('affiliate_programs.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.request_type=affiliate_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                success:function($res){
                    console.log(res);
                    //return false;
                },
                'columnDefs': [
                  
                    
                     {
                        targets: 0,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return  row['name'];
                        }
                    },
                    {
                         targets: 2,
                        render: function (data, type, row) {
                            return row['email'];
                        }
                    },
                    {
                         targets: 3,
                        render: function (data, type, row) {
                            return row['total Commission'];
                        }
                    }
                    
                    
                ]
            });
        }
</script>
<script>

 @if(@$finalRole == 1 || @$finalRole ==2) 
  $(document).ready(function () {
                     loadDashboard();
        });

        function loadDashboard(){
           var affiliate_list='affiliate_list';
            var count=1;
            var table = $('#datatables').DataTable({

                "order": [ 0, 'asc' ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('affiliate_programs.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.request_type=affiliate_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                success:function($res){
                    console.log(res);
                    //return false;
                },
                'columnDefs': [
                  
                     
                     {
                        targets: 0,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return  row['name'];
                        }
                    },
                    {
                         targets: 2,
                        render: function (data, type, row) {
                            return row['email'];
                        }
                    },
                    {
                         targets: 3,
                        render: function (data, type, row) {
                            return row['total Commission'];
                        }
                    },
                    {
                         targets: 4,
                        render: function (data, type, row) {
                            return row['total paid Commission'];
                        }
                    },
                    {
                         targets: 5,
                        render: function (data, type, row) {
                            return row['Outstanding Commission'];
                        }
                    }  
                    
                ]
            });
        }

     @endif

 @if(@$finalRole == 3 || @$finalRole == 4)
        $(document).ready(function () {
           
                     loadDashboard();
        });
 
        function loadDashboard(){
           var affiliate_list='affiliate_list';
            var count=1;
            var table = $('#datatables').DataTable({

                "order": [ 0, 'asc' ],
                "bSort": true,
                "paging": true,
                "bInfo": true,
                "bDestroy": true,
                "bFilter": true,
                "searching": true,
                "bPaginate": true,
                "bProcessing": true,
                "language": {
                    "loadingRecords": '&nbsp;',
                    "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span>'
                },
                'ajax': {
                    "type": "POST",
                    "url": "{{ route('affiliate_programs.store') }}",
                    "data": function (d) {
                        d._token= "{{csrf_token()}}",d.request_type=affiliate_list
                    },
                    "dataType": 'json',
                    "dataSrc": "",
                    "timeout":1000000,
                    "async": true,
                    "cache": true
                },
                success:function($res){
                    console.log(res);
                    //return false;
                },
                'columnDefs': [
                  
                    
                     {
                        targets: 0,
                        render: function (data, type, row) {
                            return  count++;
                        }
                    },
                    {
                        targets: 1,
                        render: function (data, type, row) {
                            return  row['name'];
                        }
                    },
                    {
                         targets: 2,
                        render: function (data, type, row) {
                            return row['email'];
                        }
                    },
                    {
                         targets: 3,
                        render: function (data, type, row) {
                            return row['total Commission'];
                        }
                    }
                    
                    
                ]
            });
        }

        @endif

</script>
@endsection
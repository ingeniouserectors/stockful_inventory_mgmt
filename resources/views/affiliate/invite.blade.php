@extends('layouts.master')

@section('title') Invite User @endsection

@section('content')
    <style>
    .mail_format{
        border:1px solid lightgrey;
        margin: 2%;
        padding: 4% ;
    }
    </style>

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0 font-size-18">Invite user</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ url('/index') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{route('affiliate_programs.index')}}">Affiliate Program</a></li>
                            <li class="breadcrumb-item">Invite User</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->
        <span class="invalid_email_message"></span>
        <div class="row">
            <div class="col-12">
                @if(session()->has('message'))
                    {!! session('message') !!}
                @endif
                <div class="card">
                    <div class="card-body">
                        <form name="create-user"  action="{{ route('affiliate_programs.store') }}" method="POST" onreset="myFunction()" onsubmit="return validateFormsu()">
                            @csrf

                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Link</label>
                                <div class="col-md-4">
                                    
                                    <input class="form-control" type="hidden" value="0" name="flag" id="flag">
                                    <input class="form-control" type="hidden" value="{{ $name }}" name="name" id="name">
                                    <input class="form-control" type="hidden" value="invite_mail" name="request_type" >
                                    <input class="form-control @error('link') is-invalid @enderror" type="text"  readonly value="{{ url('/affiliate/'.$id) }}" name="link" id="link">
                                    @if ($errors->has('link'))
                                        <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('link') }}</strong></span>
                                    @endif
                                </div>
                                <div class="col-md-4"></div>
                            </div>

                            <div class="form-group row">
                                <label for="example-email-input" class="col-md-2 col-form-label">Email</label>
                                <div class="col-md-4">
                                    <input class="form-control @error('email') is-invalid @enderror" type="email" value="{{ old('email') }}" name="email" id="email">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('email') }}</strong></span>
                                    @endif
                                    <span class="email_message"></span>
                                </div>
                                <div class="col-md-4">
                                    <a class="add_field_button btn btn-success inner" id="submit" style="color: white;" >Add to list</a>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Mail Format : </label>
                                <div class="table-responsive mail_format" >
                                    <h2>Invite Link</h2>

                                    <div>
                                        {{--Thanks for creating an account with the {{ config('app.name') }}.--}}
                                        Please follow the link below to register your account
                                        <a href="javascript:void(0);"> Register Link </a>.<br/>
                                        {{--<a href=" {{ URL::to('auth/verify/' . $confirmation_code) }}"> Confirm Email </a>.<br/>--}}
                                    </div>
                                </div>
                            </div>


                            <div class="table-responsive">
                                <table class="table mb-0" id="invite_user_table">
                                    <thead>
                                        <tr>
                                            <th>Email</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody class="input_fields_wrap">

                                    </tbody>
                                </table>
                            </div>

                            <div class="button-items mt-3">
                            <input class="btn btn-info" type="submit" value="Send Invitation" id="submit">
                            <a class="btn btn-danger waves-effect waves-light" href="{{ url('/affiliate_programs') }}" role="button">Cancel</a>
                            <input class="btn btn-warning" type="reset" value="Reset">
                        </div>

                        </form>
                    </div>
                </div>
            </div> <!-- end col -->
        </div>
        <!-- end row -->

@endsection

@section('script')
    <script src="{{ URL::asset('assets/js/pages/dashboard.init.js')}}"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    
   
    <script>
        function validateFormsu() {
            var xl = $("#flag").val();
            if (xl == "0") {
                $('.email_message').html("<p style='color: red;'>Please enter email</p>");
                return false;
            }
        }

        $(document).ready(function() {
            var max_fields      = 10; //maximum input boxes allowed
            var wrapper         = $(".input_fields_wrap"); //Fields wrapper
            var add_button      = $(".add_field_button"); //Add button ID  
            var x = 1; //initlal text box count
            
            $(add_button).click(function(e){ //on add input button click
                $('.email_message').html("");
                var value = $('#email').val();
                var emailRegex = new RegExp(/^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$/i);
                var valid = emailRegex.test(value);
                if (!valid) {
                    $('.email_message').html("<p style='color: red;'>Please enter email</p>");
                    return false;
                }
               
                if(value !='' && value != null){

                    $.ajax({
                        type: "post",
                        url: '../check_email_exists',
                        data:{email:value,_token:$('meta[name="csrf-token"]').attr('content')},
                        dataType:"json",
                        success: function (res) {
                            if(res.error == 1){
                                $('.invalid_email_message').html(res.message);

                            }else{
                          
                                var values = $("input[name='emails[]']")
                                    .map(function(){return $(this).val();}).get();
                                
                                if ($.inArray(value, values) >= 0) {
                                    $('.invalid_email_message').html('<div class="alert alert-danger">Email already exists !<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');
                                }else{
                                    
                                    $('#email').val('');
                                    e.preventDefault();
                                    if(x < max_fields){ //max input box allowed
                                        x++; //text box increment
                                        $('#flag').val('1');
                                        $(wrapper).append('<tr id="row_'+x+'">' +
                                            '<td><input class="form-control" type="email" readonly value="'+ value +'" name="emails[]" id="emails"></td> ' +
                                            '<td><a href="#" class="btn btn-danger btn-sm btn-rounded waves-effect waves-light" onclick="remove_email('+x+')"><i class="fas fa-trash-alt"></i></a></td>' +
                                            '</tr>'
                                        );
                                    }
                                }
                            }
                        },
                        error: function(){
                            console.log('error');
                        }
                    });
                } else {
                  alert("Please enter valid email"); return false;
                }
            });
            $(wrapper).on("click",".remove_field", function(e){e.preventDefault();x--;})

        });
        //user click on remove text
        function remove_email(id) {
            $('#row_'+id).remove();
        }

    </script>
@endsection
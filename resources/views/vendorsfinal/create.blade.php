@extends('layouts.master-final')

@section('title')  Vendor @endsection

@section('customcss')
<style>
    .custom-control-input{ z-index: 1; }
    .error{ border-color: coral !important; }
    .saved{ display: none; }
    .saved button{ visibility: hidden; }
    .saved:hover button{ visibility: visible; }
    .schedule-panels button{
        border: none;
        padding: 4px 0;
        border-radius: 3px;
        background: #ecf2f9;
        margin-right: 8px;
        margin-bottom: 8px;
    }
    .schedule-panels button:hover{ background: #a7cafd; }
    .schedule-panels button.checked{
        background: #227cff;
        color: white;
    }
    .weekly-days button{ width: 40px; }
    .monthly-days button{ width: 30px; }
    .form-table td{ vertical-align: top; }
    span.error{ color: red; }
    .shippingCase{ display: none; }
    table input{
        width: 120px;
    }

    table thead { background-color: #F8F9FA !important; }
    table thead th:hover { background-color: #eceff1; }

    input.error{
        border-bottom: 1px solid red;
    }
</style>
@endsection
@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Vendors</h4>
                <div class="page-title-right"></div>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="card">
        <div class="card-body">
            <ul class="nav font-weight-bold tablist w-max-900px" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="basic-details-tab" data-toggle="tab" href="#basic-details" role="tab"> Basic Details </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contacts-tab" data-toggle="tab" href="#contacts" role="tab"> Contacts </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="lead-time-tab" data-toggle="tab" href="#lead-time" role="tab"> Lead Time </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="order-settings-tab" data-toggle="tab" href="#order-settings" role="tab"> Order Settings </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="blackout-dates-tab" data-toggle="tab" href="#blackout-dates" role="tab"> Blackout Dates </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="shipping-tab" data-toggle="tab" href="#shipping" role="tab"> Shipping </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="payment-terms-tab" data-toggle="tab" href="#payment-terms" role="tab"> Payment Terms </a>
                </li>
            </ul>

            <div class="tab-content twitter-bs-wizard-tab-content  w-max-900px">
                <div class="tab-pane active" id="basic-details" role="tabpanel">
                    <form id="basic_details_form">
                        <table class="form-table" style="border-collapse: separate; border-spacing: 10px;">
                            <tr>
                                <td width="150px"><label class="col-form-label"> Vendor Type <span class="text-danger">*</span> </label></td>
                                <td width="360px" style="padding: 0;">
                                    <div class="inputing">
                                        <div class="btn-group" id="vendor_type">
                                            <button class="btn btn-primary grpBtn w-120px" style="font-size: 12px;" data-val="1" data-text="Domestic">Domestic</button>
                                            <button class="btn btn-dark grpBtn w-120px" style="font-size: 12px;" data-val="2" data-text="International">International</button>
                                        </div>
                                    </div>
                                    <div class="saved">
                                        <div>
                                            <label for="" class="col-form-label" id="vendor_type_label"></label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="" class="col-form-label">Vendor Name <span class="text-danger">*</span></label></td>
                                <td>
                                    <div class="inputing">
                                        <input type="text" class="form-control" id="vendor_name" name="vendor_name"
                                            placeholder="Enter Vendor Name"
                                            required value=""
                                            data-toggle="popover"
                                            data-trigger="hover"
                                            data-html="true"
                                            data-offset="125% 0"
                                            data-content="<h6>Vendor Name</h6><p>This is the company name of your vendor.</p><h6>To Add Contacts</h6><p>You can add contacts on the contact page.</p>"
                                        >
                                    </div>
                                    <div class="saved">
                                        <div>
                                            <label for="" class="col-form-label" id="vendor_name_label"></label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="" class="col-form-label"> Country <span class="text-danger">*</span></label></td>
                                <td>
                                    <div class="inputing">
                                        <select name="country" id="country" class="form-select countries w-100">
                                            <option value=""> Select Country</option>
                                            @foreach($country_list as  $key=> $country)
                                                <option value="{{ $key }}">{{ $country." (".$key.")" }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="saved">
                                        <div>
                                            <label for="" class="col-form-label" id="country_label"></label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="" class="col-form-label">Address <span class="text-danger">*</span></label></td>
                                <td>
                                    <div class="inputing">
                                        <div>
                                            <input type="text" class="form-control" id="address_line_1" name="address_line_1"
                                               placeholder="Enter Address Line One"
                                               value=""
                                            >
                                            <input type="text" class="form-control mt-2" id="address_line_2" name="address_line_2"
                                                   placeholder="Enter Address Line Two"
                                                   value=""
                                            >
                                        </div>
                                    </div>
                                    <div class="saved">
                                        <div>
                                            <label for="" class="col-form-label" id="address_line_1_label"></label>
                                        </div>
                                        <div>
                                            <label for="" class="col-form-label" id="address_line_2_label"></label>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td><label for="" class="col-form-label">State <span class="text-danger">*</span></label></td>
                                <td>
                                    <div class="inputing">
                                        <select name="state" id="state" class="form-select states w-100">
                                            <option value="">Select State</option>
                                            @if(!empty($state_list))
                                                @foreach($state_list as $key=> $state)
                                                    <option value="{{ $key }}">{{ $state ." (".$key.")"  }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="saved">
                                        <div>
                                            <label for="" class="col-form-label" id="state_label"></label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="" class="col-form-label">Postal Code <span class="text-danger">*</span></label></td>
                                <td>
                                    <div class="inputing">
                                        <input type="text" class="form-control" name="zipcode" id="zipcode" maxlength="8" placeholder="Enter Postal Code" value="">
                                    </div>
                                    <div class="saved">
                                        <div>
                                            <label for="" class="col-form-label" id="postal_code_lable"></label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="d-flex justify-content-between">
                                        <div>
                                            <a href="{{URL('vendors')}}" class="btn btn-outline-secondary">Cancel</a>
                                        </div>
                                        <div class="text-right">
                                            <button class="btn btn-outline-primary w-md save">Save</button>
                                            <button class="btn btn-primary w-md save next">Save And Next</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>

                <div class="tab-pane" id="contacts" role="tabpanel">
                    <form>
                        <table id="list" class="table dataTable border-bottom dt-responsive nowrap mb-5">
                            <thead class="">
                                <tr>
                                    <th class="sorting col1">First Name</th>
                                    <th class="sorting col2">Last Name</th>
                                    <th class="sorting col3">Title</th>
                                    <th class="sorting col4">Email</th>
                                    <th class="sorting col5">Phone Number</th>
                                    <td class=""></td>
                                </tr>
                            </thead>
                            <tbody class="contact_table_body">
                                <tr class="add-row-tr">
                                    <td colspan="6">
                                        <span class="text-primary1 add-row">
                                            <i class="bx bx-plus-circle font-size-16 align-middle"></i>
                                            Add Contacts
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="row mt-5">
                            <div class="col-md-8">
                                <div class="row justify-content-between">
                                    <div class="col-4">
                                        <a href="{{URL('vendors')}}" class="btn btn-outline-secondary">Cancel</a>
                                    </div>
                                    <div class="col-8 text-right">
                                        <button class="btn btn-outline-primary w-md save" data-val="1">Save</button>
                                        <button class="btn btn-primary w-md save next" data-val="2">Save And Next</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>

                <div class="tab-pane" id="lead-time" role="tabpanel">
                    <p class="mb-4">
                        This is your default lead time for this vendor. You can also modify and set specific lead
                        times at the product level where it differs from your default.
                    </p>
                    <form class="domesticCase" id="domestic_panel_form">
                        <div class="d-flex mb-4">
                            <div class="w-20">
                                <h4>Order Prep</h4>
                                <p class="text-light1"><small>Time to prepare your order</small></p>
                                <div class="input-group border-bottom">
                                    <input type="text" class="form-control border-0 input-edit w-50 calculate allow-numeric" id="order_prep_days" name="order_prep_days" maxlength="4">
                                    <div class="input-group-append w-50">
                                        <span class="input-group-text text-light1 bg-transparent border-0">Days</span>
                                    </div>
                                </div>
                                <div class="d-none border-bottom">
                                    <span class="input-group-text border-0 bg-transparent input-show"></span>
                                </div>
                            </div>
                            <div class="text-primary align-self-center mx-4">
                                <i class="mdi mdi-arrow-right"></i>
                            </div>
                            <div class="w-20">
                                <h4>To Warehouse</h4>
                                <p class="text-light1"><small>From Port to you</small></p>
                                <div class="input-group border-bottom">
                                    <input type="text" class="form-control border-0 input-edit w-50 calculate llow-numeric" id="to_warehouse_days" name="to_warehouse_days" maxlength="4">
                                    <div class="input-group-append w-50">
                                        <span class="input-group-text text-light1 bg-transparent border-0">Days</span>
                                    </div>
                                </div>
                                <div class="d-none border-bottom">
                                    <span class="input-group-text border-0 bg-transparent input-show"></span>
                                </div>
                            </div>
                            <div class="text-primary align-self-center mx-4">
                                <i class="mdi mdi-arrow-right"></i>
                            </div>
                            <div class="w-20">
                                <h4>To Amazon</h4>
                                <p class="text-light1"><small>From you to Az </small></p>
                                <div class="input-group border-bottom">
                                    <input type="text" class="form-control border-0 input-edit w-50 calculate allow-numeric" id="to_amazon" name="to_amazon" maxlength="4">
                                    <div class="input-group-append w-50">
                                        <span class="input-group-text text-light1 bg-transparent border-0">Days</span>
                                    </div>
                                </div>
                                <div class="d-none border-bottom">
                                    <span class="input-group-text border-0 bg-transparent input-show"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row no-gutters mb-4">
                            <div class="col-md-5 col-sm-6 pr-5">
                                <div class="form-inline mb-2">
                                    <div class="form-group">
                                        <label for="" class="font-size-20 mr-2">PO To Prep</label>
                                        <input type="number" name="po_to_prep_days" id="po_to_prep_days" class="form-control w-25 px-1 calculate allow-numeric" min="0" max="9999">
                                    </div>
                                </div>
                                <p><small>
                                        How long does it take for this vendor to begin preparing your order.
                                    </small></p>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="form-inline mb-2">
                                    <label for="" class="font-size-20 mr-2">Safety:</label>
                                    <input type="number" name="safety_days" id="safety_days" class="form-control mr-2 px-1 calculate allow-numeric" style="width:70px;" min="0" max="9999">
                                    <label for="" class="font-size-20">days</label>
                                </div>
                                <p><small>
                                        Add extra days to pad delivery
                                    </small></p>
                            </div>
                            <div class="col-md-3 col-sm-12 bg-light2 px-3 d-flex align-items-center">
                                <div>
                                    <h4>Total Lead Time</h4>
                                    <div class="input-group border-bottom">
                                        <input type="text" name="total_lead_time_days" id="total_lead_time_days" class="form-control border-0 input-edit bg-transparent text-info font-size-20 w-50 allow-numeric" maxlength="6">
                                        <div class="input-group-append w-50 text-right">
                                            <span class="input-group-text text-light1 bg-transparent border-0">Days</span>
                                        </div>
                                    </div>
                                    <div class="d-none border-0 p-2">
                                        <span class="border-0  bg-transparent text-info font-size-20" id="total_lead_time_days_label"></span>&nbsp; &nbsp;
                                        <i class="mdi mdi-square-edit-outline input-show text-info font-size-20"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4>Lead Time Check</h4>
                        <p class="mb-4">
                            You can set automatic follow ups to your vendor contacts email to confirm or report time changes. Setup your
                            preferred contacts per each part of the product's journey here. To view and control the email template, see how it
                            works or set one global setting for all vendors, go to the settings page.
                        </p>

                        <h4>Order Prep</h4>
                        <div class="row mb-4">
                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-info d-flex align-items-center">
                                    <input type="checkbox" class="custom-control-input checked_loop_data" name="emailing[order_prep][][checked]" id="demostic_order_prep">
                                    <label class="custom-control-label mr-3" for="demostic_order_prep"><span class="font-size-11">Send an email </span></label>
                                    <input type="number" name="emailing[order_prep][][days]" id="demostic_order_prep_days" class="form-control input-sm mr-2" style="width:70px;" min="0" max="9999">
                                    <label for="" class="m-0"><span class="font-size-11">  days to confirm expected ship date.</span></label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="d-flex align-items-center">
                                    <label for="" class="mr-3 m-0">Select Contact</label>
                                    <div class="position-relative" x-data="{ show: false }">
                                        <button
                                            @click.prevent="show = !show"
                                            @keydown.escape="show = false"
                                            class="btn btn-outline-secondary w-180px d-flex justify-content-between"
                                        >
                                            <span id="domestic_order_prep_contact">Select Contact</span> <i class="mdi mdi-chevron-down"></i>
                                        </button>
                                        <select name="emailing[order_prep][][email][]" multiple hidden></select>
                                        <div
                                            x-show="show"
                                            @click.away="show = false"
                                            class="shadow-lg position-absolute rounded w-100 py-2 bg-white vendor_contacts"
                                            style="z-index: 5;"
                                            data-name="domestic_order_prep_contact"
                                        ></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h4>Ship Confirmation</h4>
                        <div class="row mb-4">
                            <div class="col-md-6 d-flex align-items-center">
                                <div class="custom-control custom-checkbox custom-checkbox-info mr-2">
                                    <input type="checkbox" class="custom-control-input" id="domestic_ship_confirmation" name="emailing[ship_confirmation][][checked]">
                                    <label class="custom-control-label" for="domestic_ship_confirmation">
                                        <span class="font-size-11"> Send an email to confirm your products are in transit. </span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="d-flex align-items-center">
                                    <label for="" class="mr-3 m-0">Select Contact</label>
                                    <div class="position-relative" x-data="{ show: false }">
                                        <button
                                            @click.prevent="show = !show"
                                            @keydown.escape="show = false"
                                            class="btn btn-outline-secondary w-180px d-flex justify-content-between"
                                        >
                                            <span id="domestic_ship_confirmation_select_contact">Select Contact</span> <i class="mdi mdi-chevron-down"></i>
                                        </button>
                                        <select name="emailing[ship_confirmation][][email][]" multiple hidden></select>
                                        <div
                                            x-show="show"
                                            @click.away="show = false"
                                            class="shadow-lg position-absolute rounded w-100 py-2 bg-white vendor_contacts"
                                            style="z-index: 5;"
                                            data-name="domestic_ship_confirmation_contact"
                                        ></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h4>To Warehouse</h4>
                        <div class="row mb-2">
                            <div class="col-md-6 d-flex align-items-center">
                                <div class="custom-control custom-checkbox custom-checkbox-info">
                                    <input type="checkbox" class="custom-control-input" id="domestic_to_warehouse" name="emailing[to_warehouse][][checked]">
                                    <label class="custom-control-label" for="domestic_to_warehouse">
                                        <span class="font-size-11"> Send an email to confirm your products arrived at the port. </span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="d-flex align-items-center">
                                    <label for="" class="mr-3 m-0">Select Contact</label>
                                    <div class="position-relative" x-data="{ show: false }">
                                        <button
                                            @click.prevent="show = !show"
                                            @keydown.escape="show = false"
                                            class="btn btn-outline-secondary w-180px d-flex justify-content-between"
                                        >
                                            <span id="domestic_warehouse">Select Contact</span> <i class="mdi mdi-chevron-down"></i>
                                        </button>
                                        <select name="emailing[to_warehouse][][email][]" multiple hidden></select>
                                        <div
                                            x-show="show"
                                            @click.away="show = false"
                                            class="shadow-lg position-absolute rounded w-100 py-2 bg-white vendor_contacts"
                                            style="z-index: 5;"
                                            data-name="domestic_to_warehouse_contact"
                                        ></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="internationalCase">
                        <div class="d-flex justify-content-between align-items-center mb-4">
                            <div class="btn-group" role="group">
                                <button id="boat-btn" class="btn btn-primary">
                                    <i class="mdi mdi-ferry"></i>
                                    Boat
                                </button>
                                <button id="plane-btn" class="btn btn-light1">
                                    <i class="mdi mdi-airplane"></i>
                                    Plane
                                </button>
                            </div>
                            <div class="form-inline">
                                <label for="" class="mr-3">Set Default</label>
                                <select name="lead_time_check"  id="lead_time_check" class="form-select">
                                    @foreach($lead_time_type as $type)
                                        <option value="{{$type['id']}}">{{ $type['type'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <form class="" id="boat-panel">
                            <div class="d-flex justify-content-between mb-4">
                                <div class="w-16">
                                    <h4>Order Prepr</h4>
                                    <!--<h4>Production</h4>-->
                                    <p class="text-light1">
                                        <small>Time to prepare your order</small>
                                        <!--<small>Manufacture your products</small>-->
                                    </p>
                                    <div class="input-group border-bottom">
                                        <input type="text" class="form-control border-0 input-edit w-50 calculate allow-numeric" name="product_manuf_days_boat" id="product_manuf_days_boat" maxlength="4">
                                        <div class="input-group-append w-50">
                                            <span class="input-group-text text-light1 bg-transparent border-0"
                                            id="product_manuf_days_boat_span">Days</span>
                                        </div>
                                    </div>
                                    <div class="d-none border-bottom">
                                        <span class="input-group-text border-0 bg-transparent input-show"></span>
                                    </div>
                                </div>
                                <div class="text-primary align-self-center">
                                    <i class="mdi mdi-arrow-right"></i>
                                </div>
                                <div class="w-16">
                                    <h4>To Port</h4>
                                    <p class="text-light1">
                                        <small>Transported to Port</small>
                                    </p>
                                    <div class="input-group border-bottom">
                                        <input type="text" class="form-control border-0 input-edit w-50 calculate allow-numeric" name="to_port_days_boat" id="to_port_days_boat" maxlength="4">
                                        <div class="input-group-append w-50">
                                            <span class="input-group-text text-light1 bg-transparent border-0"
                                            id="to_port_days_boat_span">Days</span>
                                        </div>
                                    </div>
                                    <div class="d-none border-bottom">
                                        <span class="input-group-text border-0 bg-transparent input-show"></span>
                                    </div>
                                </div>
                                <div class="text-primary align-self-center">
                                    <i class="mdi mdi-arrow-right"></i>
                                </div>
                                <div class="w-16">
                                    <h4>Transit Time</h4>
                                    <p class="text-light1">
                                        <small>Leaves port and travels</small>
                                    </p>
                                    <div class="input-group border-bottom">
                                        <input type="text" class="form-control border-0 input-edit w-50 calculate allow-numeric" name="transit_time_days_boat" id="transit_time_days_boat" maxlength="4">
                                        <div class="input-group-append w-50">
                                            <span class="input-group-text text-light1 bg-transparent border-0"
                                            id="transit_time_days_boat_span">Days</span>
                                        </div>
                                    </div>
                                    <div class="d-none border-bottom">
                                        <span class="input-group-text border-0 bg-transparent input-show"></span>
                                    </div>
                                </div>
                                <div class="text-primary align-self-center">
                                    <i class="mdi mdi-arrow-right"></i>
                                </div>
                                <div class="w-16">
                                    <h4>To Warehouse</h4>
                                    <p class="text-light1">
                                        <small>From Port to you</small>
                                    </p>
                                    <div class="input-group border-bottom">
                                        <input type="text" class="form-control border-0 input-edit w-50 calculate allow-numeric" name="to_warehouse_days_boat" id="to_warehouse_days_boat" maxlength="4">
                                        <div class="input-group-append w-50">
                                    <span class="input-group-text text-light1 bg-transparent border-0"
                                            id="to_warehouse_days_boat_span">Days</span>
                                        </div>
                                    </div>
                                    <div class="d-none border-bottom">
                                        <span class="input-group-text border-0 bg-transparent input-show"></span>
                                    </div>
                                </div>
                                <div class="text-primary align-self-center">
                                    <i class="mdi mdi-arrow-right"></i>
                                </div>
                                <div class="w-16">
                                    <h4>To Amazon</h4>
                                    <p class="text-light1">
                                        <small>From you to Az</small>
                                    </p>
                                    <div class="input-group border-bottom">
                                        <input type="text" class="form-control border-0 input-edit w-50 calculate allow-numeric" name="to_amazon_boat" id="to_amazon_boat" maxlength="4">
                                        <div class="input-group-append w-50">
                                    <span class="input-group-text text-light1 bg-transparent border-0"
                                            id="to_amazon_boat_span">Days</span>
                                        </div>
                                    </div>
                                    <div class="d-none border-bottom">
                                        <span class="input-group-text border-0 bg-transparent input-show"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row no-gutters mb-4">
                                <div class="col-md-5 col-sm-6 pr-5">
                                    <div class="form-inline mb-2">
                                        <div class="form-group">
                                            <!--<label for="" class="font-size-20 mr-2">PO To Production</label>  -->
                                            <label for="" class="font-size-20 mr-2">PO To Prep</label>
                                            <input type="number" id="po_to_production_days_boat" class="form-control w-25 px-1 calculate allow-numeric" name="po_to_production_days_boat" min="0" max="9999">
                                        </div>
                                    </div>
                                    <p>
                                        <!--<small>
                                            How long does it take for this vendor to get your order into production once
                                            you confirm the PO is submitted?
                                        </small>  -->
                                        <small>
                                            How long does it take for this vendor to begin preparing your order.
                                        </small>
                                    </p>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form-inline mb-2">
                                        <label for="" class="font-size-20 mr-2">Safety:</label>
                                        <input type="number" name="safety_days_boat" id="safety_days_boat" class="form-control mr-2 px-1 calculate allow-numeric"
                                                style="width:70px;" min="0" max="9999">
                                        <label for="" class="font-size-20">days</label>
                                    </div>
                                    <p>
                                        <small>
                                            Add extra days to pad delivery
                                        </small>
                                    </p>
                                </div>
                                <div class="col-md-3 col-sm-12 bg-light2 px-3 d-flex align-items-center">
                                    <div>
                                        <h4>Total Lead Time</h4>
                                        <div class="input-group border-bottom">
                                            <input type="text"
                                                    class="form-control border-0 input-edit bg-transparent text-info font-size-20 w-50 allow-numeric" name="total_lead_time_days_boat" id="total_lead_time_days_boat" maxlength="6">
                                            <div class="input-group-append w-50 text-right">
                                                <span class="input-group-text text-light1 bg-transparent border-0">Days</span>
                                            </div>
                                        </div>
                                        <div class="d-none border-0 p-2">
                                            <span class="border-0  bg-transparent text-info font-size-20" id="total_lead_time_days_label_boat"></span>&nbsp;
                                            &nbsp;
                                            <i class="mdi mdi-square-edit-outline input-show text-info font-size-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h4>Lead Time Check</h4>
                            <p class="mb-4">
                                You can set automatic follow ups to your vendor contacts email to confirm or report time changes. Setup your preferred contacts per each part of the product's journey here. To view and control the email template, see how it works or set one global setting for all vendors, go to the settings page.
                            </p>

                            <h4>Production</h4>
                            <div class="row mb-2">
                                <div class="col-md-6">
                                    <div class="custom-control custom-checkbox custom-checkbox-info d-flex align-items-center">
                                        <input type="checkbox" class="custom-control-input checked_loop_data"
                                            name="emailing[production][0][checked]"
                                            id="boat_production_mail_1"
                                        >
                                        <label class="custom-control-label mr-3" for="boat_production_mail_1">
                                            <span class="font-size-11">Send an email </span>
                                        </label>
                                        <input type="number"  class="form-control input-sm mr-2 w-70px"
                                            name="emailing[production][0][days]"
                                            min="0" max="9999"
                                        >
                                        <label for="" class="m-0"><span class="font-size-11"> days to confirm production has begun</span></label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="d-flex align-items-center">
                                        <label for="" class="mr-3 m-0">Select Contact</label>
                                        <div class="position-relative" x-data="{ show: false }">
                                            <button
                                                @click.prevent="show = !show"
                                                @keydown.escape="show = false"
                                                class="btn btn-outline-secondary w-180px d-flex justify-content-between"
                                            >
                                                <span id="select_contact_confirm_production">Select Contact</span>
                                                <i class="mdi mdi-chevron-down"></i>
                                            </button>
                                            <select name="emailing[production][0][email][]" multiple hidden>
                                            </select>
                                            <div
                                                x-show="show"
                                                @click.away="show = false"
                                                class="shadow-lg position-absolute rounded w-100 py-2 bg-white vendor_contacts"
                                                style="z-index: 5;"
                                                data-name="boat_production_1_contact"
                                            >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-md-6">
                                    <div class="custom-control custom-checkbox custom-checkbox-info d-flex align-items-center">
                                        <input type="checkbox" class="custom-control-input checked_loop_data"
                                            name="emailing[production][1][checked]"
                                            id="boat_production_mail_2"
                                        >
                                        <label class="custom-control-label mr-3" for="boat_production_mail_2">
                                            <span class="font-size-11">Send an email </span>
                                        </label>
                                        <input type="number"  class="form-control input-sm mr-2 w-70px"
                                            name="emailing[production][1][days]"
                                            min="0" max="9999"
                                        >
                                        <label for="" class="m-0"><span class="font-size-11">  days from the end of production</span></label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="d-flex align-items-center">
                                        <label for="" class="mr-3 m-0">Select Contact</label>
                                        <div class="position-relative" x-data="{ show: false }">
                                            <button
                                                @click.prevent="show = !show"
                                                @keydown.escape="show = false"
                                                class="btn btn-outline-secondary w-180px d-flex justify-content-between"
                                            >
                                                <span id="select_contact_confirm_production">Select Contact</span>
                                                <i class="mdi mdi-chevron-down"></i>
                                            </button>
                                            <select name="emailing[production][1][email][]" multiple hidden>
                                            </select>
                                            <div
                                                x-show="show"
                                                @click.away="show = false"
                                                class="shadow-lg position-absolute rounded w-100 py-2 bg-white vendor_contacts"
                                                style="z-index: 5;"
                                                data-name="boat_production_2_contact"
                                            >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h4>Port Departure</h4>
                            <div class="row mb-4">
                                <div class="col-md-6 d-flex align-items-center">
                                    <div class="custom-control custom-checkbox custom-checkbox-info ">
                                        <input
                                            type="checkbox" class="custom-control-input checked_loop_data"
                                            id="boat_port_departure"
                                            name="emailing[port_departure][][checked]"
                                        >
                                        <label class="custom-control-label mr-3" for="boat_port_departure">
                                            <span class="font-size-11">
                                                Send an email to confirm your products left the vendor and are on their way to the port.
                                            </span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="d-flex align-items-center">
                                        <label for="" class="mr-3 m-0">Select Contact</label>
                                        <div class="position-relative" x-data="{ show: false }">
                                            <button
                                                @click.prevent="show = !show"
                                                @keydown.escape="show = false"
                                                class="btn btn-outline-secondary w-180px d-flex justify-content-between"
                                            >
                                                <span id="select_contact_port_depature">Select Contact</span> <i class="mdi mdi-chevron-down"></i>
                                            </button>
                                            <select name="emailing[port_departure][][email][]" multiple hidden></select>
                                            <div
                                                x-show="show"
                                                @click.away="show = false"
                                                class="shadow-lg position-absolute rounded w-100 py-2 bg-white vendor_contacts"
                                                style="z-index: 5;"
                                                data-name="boat_port_departure_contact">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h4>In Transit</h4>
                            <div class="row mb-4">
                                <div class="col-md-6 d-flex align-items-center">
                                    <div class="custom-control custom-checkbox custom-checkbox-info mr-2">
                                        <input type="checkbox" class="custom-control-input checked_loop_data"
                                            id="boat_in_transist"
                                            name="emailing[in_transit][][checked]"
                                        >
                                        <label class="custom-control-label" for="boat_in_transist">
                                            <span class="font-size-11"> Send an email to confirm your products are in transit.</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="d-flex align-items-center">
                                        <label for="" class="mr-3 m-0">Select Contact</label>
                                        <div class="position-relative" x-data="{ show: false }">
                                            <button
                                                @click.prevent="show = !show"
                                                @keydown.escape="show = false"
                                                class="btn btn-outline-secondary w-180px d-flex justify-content-between"
                                            >
                                                <span id="select_contact_in_transit">Select Contact</span>
                                                <i class="mdi mdi-chevron-down"></i>
                                            </button>
                                            <select name="emailing[in_transit][][email][]" multiple hidden></select>
                                            <div
                                                x-show="show"
                                                @click.away="show = false"
                                                class="shadow-lg position-absolute rounded w-100 py-2 bg-white vendor_contacts"
                                                style="z-index: 5;"
                                                data-name="boat_in_transist_contact"
                                            ></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h4>To Warehouse</h4>
                            <div class="row mb-2">
                                <div class="col-md-6 d-flex align-items-center">
                                    <div class="custom-control custom-checkbox custom-checkbox-info">
                                        <input
                                            type="checkbox" class="custom-control-input checked_loop_data"
                                            id="boat_to_warehouse_1"
                                            name="emailing[to_warehouse][0][checked]"
                                        >
                                        <label class="custom-control-label" for="boat_to_warehouse_1">
                                            <span class="font-size-11">
                                                Send an email to confirm your products arrived at the port.
                                            </span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="d-flex align-items-center">
                                        <label for="" class="mr-3 m-0">Select Contact</label>
                                        <div class="position-relative" x-data="{ show: false }">
                                            <button
                                                @click.prevent="show = !show"
                                                @keydown.escape="show = false"
                                                class="btn btn-outline-secondary w-180px d-flex justify-content-between"
                                            >
                                                <span id="select_contact_confirm_product_at_port">Select Contact</span> <i class="mdi mdi-chevron-down"></i>
                                            </button>
                                            <select name="emailing[to_warehouse][0][email][]" multiple hidden></select>
                                            <div
                                                x-show="show"
                                                @click.away="show = false"
                                                class="shadow-lg position-absolute rounded w-100 py-2 bg-white vendor_contacts"
                                                style="z-index: 5;"
                                                data-name="boat_to_warehouse_1_contact"
                                            >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-6 d-flex align-items-center">
                                    <div class="custom-control custom-checkbox custom-checkbox-info">
                                        <input type="checkbox" class="custom-control-input checked_loop_data"
                                            id="boat_to_warehouse_2"
                                            name="emailing[to_warehouse][1][checked]"
                                        >
                                        <label class="custom-control-label" for="boat_to_warehouse_2">
                                            <span class="font-size-11">
                                                Send an email to confirm your products left the port and are in transit to your warehouse.
                                            </span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="d-flex align-items-center">
                                        <label for="" class="mr-3 m-0">Select Contact</label>
                                        <div class="position-relative" x-data="{ show: false }">
                                            <button
                                                @click.prevent="show = !show"
                                                @keydown.escape="show = false"
                                                class="btn btn-outline-secondary w-180px d-flex justify-content-between"
                                            >
                                                <span id="select_contact_confirm_product_left_port">Select Contact</span>
                                                <i class="mdi mdi-chevron-down"></i>
                                            </button>
                                            <select name="emailing[to_warehouse][1][email][]" multiple hidden></select>
                                            <div
                                                x-show="show"
                                                @click.away="show = false"
                                                class="shadow-lg position-absolute rounded w-100 py-2 bg-white vendor_contacts"
                                                style="z-index: 5;"
                                                data-name="boat_to_warehouse_2_contact"
                                            ></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-md-6 d-flex align-items-center">
                                    <div class="custom-control custom-checkbox custom-checkbox-info">
                                        <input type="checkbox" class="custom-control-input checked_loop_data"
                                            id="boat_to_warehouse_3"
                                            name="emailing[to_warehouse][2][checked]"
                                        >
                                        <label class="custom-control-label" for="boat_to_warehouse_3">
                                            <span class="font-size-11"> Send email to confirm warehouse receipt </span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="d-flex align-items-center">
                                        <label for="" class="mr-3 m-0">Select Contact</label>
                                        <div class="position-relative" x-data="{ show: false }">
                                            <button
                                                @click.prevent="show = !show"
                                                @keydown.escape="show = false"
                                                class="btn btn-outline-secondary w-180px d-flex justify-content-between"
                                            >
                                                <span id="select_contact_confirm_warehouse_receipt">Select Contact</span> <i class="mdi mdi-chevron-down"></i>
                                            </button>
                                            <select name="emailing[to_warehouse][2][email][]" multiple hidden></select>
                                            <div
                                                x-show="show"
                                                @click.away="show = false"
                                                class="shadow-lg position-absolute rounded w-100 py-2 bg-white vendor_contacts"
                                                style="z-index: 5;"
                                                data-name="boat_to_warehouse_3_contact"
                                            ></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <form class="d-none" id="plane-panel">
                            <div class="d-flex justify-content-between mb-4">
                                <div class="w-16">
                                    <h4>Order Prepr</h4>
                                    <!--<h4>Production</h4>-->
                                    <p class="text-light1">
                                        <small>Time to prepare your order</small>
                                        <!--<small>Manufacture your products</small>-->
                                    </p>
                                    <div class="input-group border-bottom">
                                        <input type="text" class="form-control border-0 input-edit w-50 calculate allow-numeric" id="product_manuf_days_plane" name="product_manuf_days_plane" maxlength="4">
                                        <div class="input-group-append w-50">
                                    <span class="input-group-text text-light1 bg-transparent border-0"
                                            id="product_manuf_days_plane_span">Days</span>
                                        </div>
                                    </div>
                                    <div class="d-none border-bottom">
                                        <span class="input-group-text border-0 bg-transparent input-show"></span>
                                    </div>
                                </div>
                                <div class="text-primary align-self-center">
                                    <i class="mdi mdi-arrow-right"></i>
                                </div>
                                <div class="w-16">
                                    <h4>To Port</h4>
                                    <p class="text-light1">
                                        <small>Transported to Port</small>
                                    </p>
                                    <div class="input-group border-bottom">
                                        <input type="text" class="form-control border-0 input-edit w-50 calculate allow-numeric" id="to_port_days_plane" name="to_port_days_plane" maxlength="4">
                                        <div class="input-group-append w-50">
                                    <span class="input-group-text text-light1 bg-transparent border-0"
                                            id="to_port_days_plane_span">Days</span>
                                        </div>
                                    </div>
                                    <div class="d-none border-bottom">
                                        <span class="input-group-text border-0 bg-transparent input-show"></span>
                                    </div>
                                </div>
                                <div class="text-primary align-self-center">
                                    <i class="mdi mdi-arrow-right"></i>
                                </div>
                                <div class="w-16">
                                    <h4>Transit Time</h4>
                                    <p class="text-light1">
                                        <small>Leaves port and travels</small>
                                    </p>
                                    <div class="input-group border-bottom">
                                        <input type="text" class="form-control border-0 input-edit w-50 calculate allow-numeric" id="transit_time_days_plane" name="transit_time_days_plane" maxlength="4">
                                        <div class="input-group-append w-50">
                                    <span class="input-group-text text-light1 bg-transparent border-0"
                                            id="transit_time_days_plane_span">Days</span>
                                        </div>
                                    </div>
                                    <div class="d-none border-bottom">
                                        <span class="input-group-text border-0 bg-transparent input-show"></span>
                                    </div>
                                </div>
                                <div class="text-primary align-self-center">
                                    <i class="mdi mdi-arrow-right"></i>
                                </div>
                                <div class="w-16">
                                    <h4>To Warehouse</h4>
                                    <p class="text-light1">
                                        <small>From Port to you</small>
                                    </p>
                                    <div class="input-group border-bottom">
                                        <input type="text" class="form-control border-0 input-edit w-50 calculate allow-numeric" id="to_warehouse_days_plane" name="to_warehouse_days_plane" maxlength="4">
                                        <div class="input-group-append w-50">
                                    <span class="input-group-text text-light1 bg-transparent border-0"
                                            id="to_warehouse_days_plane_span">Days</span>
                                        </div>
                                    </div>
                                    <div class="d-none border-bottom">
                                        <span class="input-group-text border-0 bg-transparent input-show"></span>
                                    </div>
                                </div>
                                <div class="text-primary align-self-center">
                                    <i class="mdi mdi-arrow-right"></i>
                                </div>
                                <div class="w-16">
                                    <h4>To Amazon</h4>
                                    <p class="text-light1">
                                        <small>From you to Az</small>
                                    </p>
                                    <div class="input-group border-bottom">
                                        <input type="text" class="form-control border-0 input-edit w-50 calculate allow-numeric" id="to_amazon_plane" name="to_amazon_plane" maxlength="4">
                                        <div class="input-group-append w-50">
                                    <span class="input-group-text text-light1 bg-transparent border-0"
                                            id="to_amazon_plane_span">Days</span>
                                        </div>
                                    </div>
                                    <div class="d-none border-bottom">
                                        <span class="input-group-text border-0 bg-transparent input-show"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row no-gutters mb-4">
                                <div class="col-md-5 col-sm-6 pr-5">
                                    <div class="form-inline mb-2">
                                        <div class="form-group">
                                            <!-- <label for="" class="font-size-20 mr-2">PO To Production</label>  -->
                                            <label for="" class="font-size-20 mr-2">PO To Prep</label>
                                            <input type="number"  class="form-control w-25 px-1 calculate allow-numeric" id="po_to_production_days_plane" name="po_to_production_days_plane" min="0" max="9999">
                                        </div>
                                    </div>
                                    <p>
                                        <!--<small>
                                            How long does it take for this vendor to get your order into production once
                                            you confirm the PO is submitted?
                                        </small>  -->
                                        <small>
                                            How long does it take for this vendor to begin preparing your order.
                                        </small>
                                    </p>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form-inline mb-2">
                                        <label for="" class="font-size-20 mr-2">Safety:</label>
                                        <input type="number" name="safety_days_plane" id="safety_days_plane" class="form-control mr-2 px-1 calculate allow-numeric"
                                                style="width:70px;" min="0" max="9999">
                                        <label for="" class="font-size-20">days</label>
                                    </div>
                                    <p>
                                        <small>
                                            Add extra days to pad delivery
                                        </small>
                                    </p>
                                </div>
                                <div class="col-md-3 col-sm-12 bg-light2 px-3 d-flex align-items-center">
                                    <div>
                                        <h4>Total Lead Time</h4>
                                        <div class="input-group border-bottom">
                                            <input type="text"
                                                    class="form-control border-0 input-edit bg-transparent text-info font-size-20 w-50 allow-numeric" id="total_lead_time_days_plane" name="total_lead_time_days_plane" maxlength="6">
                                            <div class="input-group-append w-50 text-right">
                                                <span class="input-group-text text-light1 bg-transparent border-0" id="total_lead_time_days_plane_span">Days</span>
                                            </div>
                                        </div>
                                        <div class="d-none border-0 p-2">
                                            <span class="border-0  bg-transparent text-info font-size-20" id="total_lead_time_days_label_plane"></span>&nbsp;
                                            &nbsp;
                                            <i class="mdi mdi-square-edit-outline input-show text-info font-size-20"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h4>Lead Time Check</h4>
                            <p class="mb-4">
                                You can set automatic follow ups to your vendor contacts email to confirm or report time
                                changes. Setup your
                                preferred contacts per each part of the product's journey here. To view and control the
                                email template, see how it
                                works or set one global setting for all vendors, go to the settings page.
                            </p>

                            <h4>Production</h4>
                            <div class="row mb-2">
                                <div class="col-md-6">
                                    <div class="custom-control custom-checkbox custom-checkbox-info d-flex align-items-center">
                                        <input type="checkbox" class="custom-control-input checked_loop_data"
                                            name="emailing[production][0][checked]"
                                            id="plane_production_mail_1"
                                        >
                                        <label class="custom-control-label mr-3" for="plane_production_mail_1">
                                            <span class="font-size-11">Send an email </span>
                                        </label>
                                        <input type="number"  class="form-control input-sm mr-2 w-70px"
                                            name="emailing[production][0][days]"
                                            min="0" max="9999"
                                        >
                                        <label for="" class="m-0"><span class="font-size-11"> days to confirm production has begun</span></label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="d-flex align-items-center">
                                        <label for="" class="mr-3 m-0">Select Contact</label>
                                        <div class="position-relative" x-data="{ show: false }">
                                            <button
                                                @click.prevent="show = !show"
                                                @keydown.escape="show = false"
                                                class="btn btn-outline-secondary w-180px d-flex justify-content-between"
                                            >
                                                <span id="select_contact_confirm_production">Select Contact</span> <i class="mdi mdi-chevron-down"></i>
                                            </button>
                                            <select name="emailing[production][0][email][]" multiple hidden>
                                            </select>
                                            <div
                                                x-show="show"
                                                @click.away="show = false"
                                                class="shadow-lg position-absolute rounded w-100 py-2 bg-white vendor_contacts"
                                                style="z-index: 5;"
                                                data-name="plane_production_1_contact">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-md-6">
                                    <div class="custom-control custom-checkbox custom-checkbox-info d-flex align-items-center">
                                        <input type="checkbox" class="custom-control-input checked_loop_data"
                                            name="emailing[production][1][checked]"
                                            id="plane_production_mail_2"
                                        >
                                        <label class="custom-control-label mr-3" for="plane_production_mail_2">
                                            <span class="font-size-11">Send an email </span>
                                        </label>
                                        <input type="number"  class="form-control input-sm mr-2 w-70px"
                                            name="emailing[production][1][days]"
                                            min="0" max="9999"
                                        >
                                        <label for="" class="m-0"><span class="font-size-11">  days from the end of production</span></label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="d-flex align-items-center">
                                        <label for="" class="mr-3 m-0">Select Contact</label>
                                        <div class="position-relative" x-data="{ show: false }">
                                            <button
                                                @click.prevent="show = !show"
                                                @keydown.escape="show = false"
                                                class="btn btn-outline-secondary w-180px d-flex justify-content-between"
                                            >
                                                <span id="select_contact_confirm_production">Select Contact</span>
                                                <i class="mdi mdi-chevron-down"></i>
                                            </button>
                                            <select name="emailing[production][1][email][]" multiple hidden>
                                            </select>
                                            <div
                                                x-show="show"
                                                @click.away="show = false"
                                                class="shadow-lg position-absolute rounded w-100 py-2 bg-white vendor_contacts"
                                                style="z-index: 5;"
                                                data-name="plane_production_2_contact"
                                            >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h4>Port Departure</h4>
                            <div class="row mb-4">
                                <div class="col-md-6 d-flex align-items-center">
                                    <div class="custom-control custom-checkbox custom-checkbox-info ">
                                        <input type="checkbox" class="custom-control-input checked_loop_data"
                                            id="plane_port_departure" name="emailing[port_departure][][checked]"
                                        >
                                        <label class="custom-control-label mr-3" for="plane_port_departure">
                                            <span class="font-size-11">Send an email to confirm your products left the vendor and are on their way to the port.</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="d-flex align-items-center">
                                        <label for="" class="mr-3 m-0">Select Contact</label>
                                        <div class="position-relative" x-data="{ show: false }">
                                            <button
                                                @click.prevent="show = !show"
                                                @keydown.escape="show = false"
                                                class="btn btn-outline-secondary w-180px d-flex justify-content-between"
                                            >
                                                <span id="plane_port_to_departure">Select Contact</span> <i class="mdi mdi-chevron-down"></i>
                                            </button>
                                            <select name="emailing[port_departure][][email][]" multiple hidden></select>
                                            <div
                                                x-show="show"
                                                @click.away="show = false"
                                                class="shadow-lg position-absolute rounded w-100 py-2 bg-white vendor_contacts"
                                                style="z-index: 5;"
                                                data-name="plane_port_departure_contact"
                                            ></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h4>In Transit</h4>
                            <div class="row mb-4">
                                <div class="col-md-6 d-flex align-items-center">
                                    <div class="custom-control custom-checkbox custom-checkbox-info mr-2">
                                        <input type="checkbox" class="custom-control-input checked_loop_data"
                                            id="plane_in_transist" name="emailing[in_transit][][checked]"
                                        >
                                        <label class="custom-control-label" for="plane_in_transist"><span class="font-size-11"> Send an email to confirm your products are in transit.</span></label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="d-flex align-items-center">
                                        <label for="" class="mr-3 m-0">Select Contact</label>
                                        <div class="position-relative" x-data="{ show: false }">
                                            <button
                                                @click.prevent="show = !show"
                                                @keydown.escape="show = false"
                                                class="btn btn-outline-secondary w-180px d-flex justify-content-between"
                                            >
                                                <span id="plane_in_transit">Select Contact</span> <i class="mdi mdi-chevron-down"></i>
                                            </button>
                                            <select name="emailing[in_transit][][email][]" multiple hidden></select>
                                            <div
                                                x-show="show"
                                                @click.away="show = false"
                                                class="shadow-lg position-absolute rounded w-100 py-2 bg-white vendor_contacts"
                                                style="z-index: 5;"
                                                data-name="plane_in_transist_contact"
                                            ></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h4>To Warehouse</h4>
                            <div class="row mb-2">
                                <div class="col-md-6 d-flex align-items-center">
                                    <div class="custom-control custom-checkbox custom-checkbox-info">
                                        <input type="checkbox" class="custom-control-input checked_loop_data"
                                            id="plane_to_warehouse_1" name="emailing[to_warehouse][0][email]"
                                        >
                                        <label class="custom-control-label" for="plane_to_warehouse_1">
                                            <span class="font-size-11">
                                                Send an email to confirm your products arrived at the port.
                                            </span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="d-flex align-items-center">
                                        <label for="" class="mr-3 m-0">Select Contact</label>
                                        <div class="position-relative" x-data="{ show: false }">
                                            <button
                                                @click.prevent="show = !show"
                                                @keydown.escape="show = false"
                                                class="btn btn-outline-secondary w-180px d-flex justify-content-between"
                                            >
                                                <span id="plane_confirm_arrived_port">Select Contact</span> <i class="mdi mdi-chevron-down"></i>
                                            </button>
                                            <select name="emailing[to_warehouse][0][email][]" multiple hidden></select>
                                            <div
                                                x-show="show"
                                                @click.away="show = false"
                                                class="shadow-lg position-absolute rounded w-100 py-2 bg-white vendor_contacts"
                                                style="z-index: 5;"
                                                data-name="plane_to_warehouse_1_contact"
                                            ></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-6 d-flex align-items-center">
                                    <div class="custom-control custom-checkbox custom-checkbox-info">
                                        <input type="checkbox" class="custom-control-input checked_loop_data"
                                            id="plane_to_warehouse_2" name="emailing[to_warehouse][1][checked]"
                                        >
                                        <label class="custom-control-label" for="plane_to_warehouse_2">
                                            <span class="font-size-11">
                                                Send an email to confirm your products left the port and are in transit to your warehouse.
                                            </span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="d-flex align-items-center">
                                        <label for="" class="mr-3 m-0">Select Contact</label>
                                        <div class="position-relative" x-data="{ show: false }">
                                            <button
                                                @click.prevent="show = !show"
                                                @keydown.escape="show = false"
                                                class="btn btn-outline-secondary w-180px d-flex justify-content-between"
                                            >
                                                <span id="plane_confirm_left_port">Select Contact</span>
                                                <i class="mdi mdi-chevron-down"></i>
                                            </button>
                                            <select name="emailing[to_warehouse][1][email][]" multiple hidden></select>
                                            <div
                                                x-show="show"
                                                @click.away="show = false"
                                                class="shadow-lg position-absolute rounded w-100 py-2 bg-white vendor_contacts"
                                                style="z-index: 5;"
                                                data-name="plane_to_warehouse_2_contact"
                                            ></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-md-6 d-flex align-items-center">
                                    <div class="custom-control custom-checkbox custom-checkbox-info">
                                        <input type="checkbox" class="custom-control-input checked_loop_data"
                                            id="plane_to_warehouse_3" name="emailing[to_warehouse][2][checked]"
                                        >
                                        <label class="custom-control-label" for="plane_to_warehouse_3">
                                            <span class="font-size-11">
                                                Send email to confirm warehouse receipt
                                            </span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="d-flex align-items-center">
                                        <label for="" class="mr-3 m-0">Select Contact</label>
                                        <div class="position-relative" x-data="{ show: false }">
                                            <button
                                                @click.prevent="show = !show"
                                                @keydown.escape="show = false"
                                                class="btn btn-outline-secondary w-180px d-flex justify-content-between"
                                            >
                                                <span id="plane_confirm_warehouse_receipt">Select Contact</span> <i class="mdi mdi-chevron-down"></i>
                                            </button>
                                            <select name="emailing[to_warehouse][2][email][]" multiple hidden></select>
                                            <div
                                                x-show="show"
                                                @click.away="show = false"
                                                class="shadow-lg position-absolute rounded w-100 py-2 bg-white vendor_contacts"
                                                style="z-index: 5;"
                                                data-name="plane_to_warehouse_3_contact"
                                            ></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="row mt-5">
                        <div class="col-md-8">
                            <div class="row justify-content-between">
                                <div class="col-4">
                                    <a href="{{URL('vendors')}}" class="btn btn-outline-secondary">Cancel</a>
                                </div>
                                <div class="col-8 text-right">
                                    <button class="btn btn-outline-primary w-md save" data-updatetype="1">Save</button>
                                    <button class="btn btn-primary w-md save next" data-updatetype="1">Save And Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="order-settings">
                    <form>
                        <table class="form-table" style="border-collapse: separate; border-spacing: 10px;">
                            <tr>
                                <td width="150px"><label class="col-form-label"> Order Volume </label></td>
                                <td width="360px" style="padding: 0;">
                                    <div class="inputing">
                                        <div class="d-flex justify-content-between">
                                            <input type="text" class="form-control"  name="order_volume" id="order_volume" style="width:100px; margin-right: 10px;">
                                            <select name="order_volume_type" id="order_volume_type" class="form-select" style="width:250px;">
                                                @foreach($order_volume_type as $volume_type)
                                                    <option value="{{ $volume_type['id'] }}">{{ $volume_type['volume_type'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="saved">
                                        <div>
                                            <label class="col-form-label order_volume" style="padding-left: 0.75rem;"></label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align:top;"><label for="" class="col-form-label">Reorder Schedule</label></td>
                                <td>
                                    <div class="inputing">
                                        <select name="reorder_schedule" id="reorder_schedule" class="form-select w-100">
                                            <option value="on_demand">On Demand</option>
                                            <option value="weekly">Weekly</option>
                                            <option value="bi_weekly">Bi-Weekly</option>
                                            <option value="monthly">Monthly</option>
                                            <option value="bi_monthly">Bi-Monthly</option>
                                        </select>
                                        <div class="schedule-panels mt-3">
                                            <div class="on_demand-panel"></div>
                                            <div class="weekly-panel d-none">
                                                <p>Select one day to set your weekly schedule.</p>
                                                <div class="weekly-days day-btns" data-number="1"></div>
                                            </div>
                                            <div class="bi_weekly-panel d-none">
                                                <p>Select two days to set your bi-weekly schedule.</p>
                                                <div class="weekly-days day-btns" data-number="2"></div>
                                            </div>
                                            <div class="monthly-panel d-none">
                                                <p>Select one day to set your monthly schedule.</p>
                                                <div class="monthly-days day-btns" data-number="1"></div>
                                            </div>
                                            <div class="bi_monthly-panel d-none">
                                                <p>Select two days to set your bi-monthly schedule.</p>
                                                <div class="monthly-days day-btns" data-number="2"></div>
                                            </div>
                                            <select class="" name="days[]" multiple hidden>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="saved">
                                        <div class="d-flex align-items-center reorder_schedule">
                                            <label class="col-form-label mr-4" style="padding-left: 0.75rem;">On Demand</label>
                                            <label for="" class="col-form-label w-30px py-1 text-center border border-secondary rounded mr-2">1</label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="d-flex justify-content-between">
                                        <div>
                                            <a href="{{URL('vendors')}}" class="btn btn-outline-secondary">Cancel</a>
                                        </div>
                                        <div class="text-right">
                                            <button class="btn btn-outline-primary w-md save">Save</button>
                                            <button class="btn btn-primary w-md save next">Save And Next</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>

                <div class="tab-pane" id="blackout-dates">
                    <form>
                        <table id="blackout-dates-table" class="table dataTable border-bottom dt-responsive nowrap"
                            style="border-collapse: collapse; border-spacing: 0; width: 100%;"
                        >
                            <thead>
                                <tr>
                                    <th style="display: none;"></th>
                                    <th class="sorting bcol1">Event Name</th>
                                    <th class="sorting bcol2" width="18%">Start Date</th>
                                    <th class="sorting bcol3" width="18%">End Date</th>
                                    <th class="sorting bcol4" width="20%">Number Of Days</th>
                                    <th class="sorting bcol5">Type</th>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="add-row-tr">
                                    <td style="display: none;"></td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <span class="btn text-primary1 add-new-btn">
                                            <i class="bx bx-plus-circle font-size-16 align-middle"></i>
                                            Add New Event
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="row mt-5">
                            <div class="col-md-8">
                                <div class="row justify-content-between">
                                    <div class="col-4">
                                        <a href="{{URL('vendors')}}" class="btn btn-outline-secondary">Cancel</a>
                                    </div>
                                    <div class="col-8 text-right">
                                        <button class="btn btn-outline-primary w-md save" data-type="1">Save</button>
                                        <button class="btn btn-primary w-md save next" data-type="2">Save And Next</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="tab-pane" id="shipping">
                    <form action="">
                        <table class="form-table" style="border-collapse: separate; border-spacing: 10px;">
                            <tbody class="domesticCase">
                                <tr>
                                    <td width="150px"><label class="col-form-label"> Shipping </label></td>
                                    <td>
                                        <div class="inputing">
                                            <div class="btn-group">
                                                <button class="btn btn-primary font-size-12 supplier-btn selectShippingBtn selected" style="width:118px;" data-val="vendorCase">Vendor</button>
                                                <button class="btn btn-dark font-size-12 px-1 shipping-agent-btn selectShippingBtn" style="width:118px;" data-val="shippingCase">Shipping Carrier</button>
                                            </div>
                                        </div>
                                        <div class="saved">
                                            <div>
                                                <label for="" class="col-form-label" id="shipping_shipping_type"></label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="vendorCase">
                                    <td><label for="" class="col-form-label">Delivery Schedule</label></td>
                                    <td>
                                        <div class="inputing">
                                            <select name="delivery_schedule" id="delivery_schedule" class="form-select w-100">
                                                <option value="on-demand">On Demand</option>
                                                <option value="weekly">Weekly</option>
                                                <option value="monthly">Monthly</option>
                                            </select>
                                            <div class="schedule-panels mt-3">
                                                <div class="on_demand-panel"></div>
                                                <div class="weekly-panel d-none">
                                                    <p>Select 1 or more days to set the vendors weekly schedule.</p>
                                                    <div class="weekly-days day-btns"></div>
                                                </div>
                                                <div class="monthly-panel d-none">
                                                    <p>Select 1 or more dates to set the vendors monthly schedule.</p>
                                                    <div class="monthly-days day-btns"></div>
                                                </div>
                                                <select class="" name="days[]" id="shipping_delivery_schedule_days" multiple hidden>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="saved">
                                            <div>
                                                <label class="col-form-label mr-4" id="shipping_delivery_schedule_saved"></label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="vendorCase" >
                                    <td class="weekly-cutoff"><label for="" class="col-form-label">Cutoff Delivery Time</label></td>
                                    <td class="weekly-cutoff">
                                        <div class="mb-2" id="cutoff-Sun">
                                            <div class="inputing">
                                                <div class="d-flex justify-content-between">
                                                    <label for="" class="col-form-label w-60px">Sun</label>
                                                    <select name="cutoff_delivery_time[weekly][sun][schedule]" class="form-select w-120px cutoff_day"></select>
                                                    <div class="w-150px">
                                                        <div class="input-group">
                                                            <input name="cutoff_delivery_time[weekly][sun][time]" type="text" placeholder="Select Time" class="form-control timepicker border-right-0" data-provide="timepicker">
                                                            <div class="input-group-append ">
                                                                <span class="input-group-text bg-transparent"><i class="mdi mdi-clock-outline"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="saved">
                                                <div class="">
                                                    <label class="col-form-label w-60px mr-4">Sun</label>
                                                    <label class="col-form-label w-60px mr-4" id="shipping_cutoff_delivery_time_sun_schedule_saved"></label>
                                                    <label class="col-form-label w-60px" id="shipping_cutoff_delivery_time_sun_time_saved"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-2" id="cutoff-Mon">
                                            <div class="inputing">
                                                <div class="d-flex justify-content-between">
                                                    <label for="" class="col-form-label w-60px">Mon</label>
                                                    <select name="cutoff_delivery_time[weekly][mon][schedule]" class="form-select w-120px cutoff_day"></select>
                                                    <div class="w-150px">
                                                        <div class="input-group">
                                                            <input name="cutoff_delivery_time[weekly][mon][time]" type="text" placeholder="Select Time" class="form-control timepicker border-right-0" data-provide="timepicker">
                                                            <div class="input-group-append ">
                                                                <span class="input-group-text bg-transparent"><i class="mdi mdi-clock-outline"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="saved">
                                                <div class="">
                                                    <label class="col-form-label w-60px mr-4">Mon</label>
                                                    <label class="col-form-label w-60px mr-4" id="shipping_cutoff_delivery_time_mon_schedule_saved"></label>
                                                    <label class="col-form-label w-60px" id="shipping_cutoff_delivery_time_mon_time_saved"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-2" id="cutoff-Tue">
                                            <div class="inputing">
                                                <div class="d-flex justify-content-between">
                                                    <label for="" class="col-form-label w-60px">Tue</label>
                                                    <select name="cutoff_delivery_time[weekly][tue][schedule]" class="form-select w-120px cutoff_day"></select>
                                                    <div class="w-150px">
                                                        <div class="input-group">
                                                            <input name="cutoff_delivery_time[weekly][tue][time]" type="text" placeholder="Select Time" class="form-control timepicker border-right-0" data-provide="timepicker">
                                                            <div class="input-group-append ">
                                                                <span class="input-group-text bg-transparent"><i class="mdi mdi-clock-outline"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="saved">
                                                <div class="">
                                                    <label class="col-form-label w-60px mr-4">Tue</label>
                                                    <label class="col-form-label w-60px mr-4" id="shipping_cutoff_delivery_time_tue_schedule_saved"></label>
                                                    <label class="col-form-label w-60px" id="shipping_cutoff_delivery_time_tue_time_saved"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-2" id="cutoff-Wed">
                                            <div class="inputing">
                                                <div class="d-flex justify-content-between">
                                                    <label for="" class="col-form-label w-60px">Wed</label>
                                                    <select name="cutoff_delivery_time[weekly][wed][schedule]" class="form-select w-120px cutoff_day"></select>
                                                    <div class="w-150px">
                                                        <div class="input-group">
                                                            <input name="cutoff_delivery_time[weekly][wed][time]" type="text" placeholder="Select Time" class="form-control timepicker border-right-0" data-provide="timepicker">
                                                            <div class="input-group-append ">
                                                                <span class="input-group-text bg-transparent"><i class="mdi mdi-clock-outline"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="saved">
                                                <div class="">
                                                    <label class="col-form-label w-60px mr-4">Wed</label>
                                                    <label class="col-form-label w-60px mr-4" id="shipping_cutoff_delivery_time_wed_schedule_saved"></label>
                                                    <label class="col-form-label w-60px" id="shipping_cutoff_delivery_time_wed_time_saved"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-2" id="cutoff-Thu">
                                            <div class="inputing">
                                                <div class="d-flex justify-content-between">
                                                    <label for="" class="col-form-label w-60px">Thu</label>
                                                    <select name="cutoff_delivery_time[weekly][thu][schedule]" class="form-select w-120px cutoff_day"></select>
                                                    <div class="w-150px">
                                                        <div class="input-group">
                                                            <input name="cutoff_delivery_time[weekly][thu][time]" type="text" placeholder="Select Time" class="form-control timepicker border-right-0" data-provide="timepicker">
                                                            <div class="input-group-append ">
                                                                <span class="input-group-text bg-transparent"><i class="mdi mdi-clock-outline"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="saved">
                                                <div class="">
                                                    <label class="col-form-label w-60px mr-4">Thu</label>
                                                    <label class="col-form-label w-60px mr-4" id="shipping_cutoff_delivery_time_thu_schedule_saved"></label>
                                                    <label class="col-form-label w-60px" id="shipping_cutoff_delivery_time_thu_time_saved"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-2" id="cutoff-Fri">
                                            <div class="inputing">
                                                <div class="d-flex justify-content-between">
                                                    <label for="" class="col-form-label w-60px">Fri</label>
                                                    <select name="cutoff_delivery_time[weekly][fri][schedule]" class="form-select w-120px cutoff_day"></select>
                                                    <div class="w-150px">
                                                        <div class="input-group">
                                                            <input name="cutoff_delivery_time[weekly][fri][time]" type="text" placeholder="Select Time" class="form-control timepicker border-right-0" data-provide="timepicker">
                                                            <div class="input-group-append ">
                                                                <span class="input-group-text bg-transparent"><i class="mdi mdi-clock-outline"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="saved">
                                                <div class="">
                                                    <label class="col-form-label w-60px mr-4">Fri</label>
                                                    <label class="col-form-label w-60px mr-4" id="shipping_cutoff_delivery_time_fri_schedule_saved"></label>
                                                    <label class="col-form-label w-60px" id="shipping_cutoff_delivery_time_fri_time_saved"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-2" id="cutoff-Sat">
                                            <div class="inputing">
                                                <div class="d-flex justify-content-between">
                                                    <label for="" class="col-form-label w-60px">Sat</label>
                                                    <select name="cutoff_delivery_time[weekly][sat][schedule]" class="form-select w-120px cutoff_day"></select>
                                                    <div class="w-150px">
                                                        <div class="input-group">
                                                            <input name="cutoff_delivery_time[weekly][sat][time]" type="text" placeholder="Select Time" class="form-control timepicker border-right-0" data-provide="timepicker">
                                                            <div class="input-group-append ">
                                                                <span class="input-group-text bg-transparent"><i class="mdi mdi-clock-outline"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="saved">
                                                <div class="">
                                                    <label class="col-form-label w-60px mr-4">Sat</label>
                                                    <label class="col-form-label w-60px mr-4" id="shipping_cutoff_delivery_time_sat_schedule_saved"></label>
                                                    <label class="col-form-label w-60px" id="shipping_cutoff_delivery_time_sat_time_saved"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="vendorCase" id="monthly-cutoff">
                                    <td class="monthly-cutoff"><label for="" class="col-form-label">Cutoff Delivery Time</label></td>
                                    <td class="monthly-cutoff">
                                        <div class="inputing">
                                            <div class="d-flex justify-content-between">
                                                <div style="display:inline-flex;">
                                                    <input type="text" name="cutoff_delivery_time[monthly][days]" class="form-control w-120px">
                                                    <label for="" class="col-form-label ml-2">Days</label>
                                                </div>
                                                <div class="w-150px" style="display:inline-flex">
                                                    <label for="" class="col-form-label mr-2">At</label>
                                                    <div class="input-group">
                                                        <input name="cutoff_delivery_time[monthly][time]" id="timepicker2" type="text" placeholder="Select Time" class="form-control timepicker border-right-0" data-provide="timepicker">
                                                        <div class="input-group-append ">
                                                            <span class="input-group-text bg-transparent"><i class="mdi mdi-clock-outline"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="saved">
                                            <div class="">
                                                <label class="col-form-label w-60px mr-4" id="shipping_cutoff_delivery_time_monthly_schedule_saved"></label>
                                                <label class="col-form-label w-60px mr-4">Days</label>
                                                <label class="col-form-label w-60px" id="shipping_cutoff_delivery_time_monthly_time_saved"></label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="shippingCase">
                                    <td><label for="" class="col-form-label">Shipping Carrier</label></td>
                                    <td>
                                        <div class="inputing position-relative">
                                            <select id="shipping_carrier_option" name="shipping_carrier_id" class="form-select w-100">
                                                @if(!empty($shipping_carrierss))
                                                    @foreach($shipping_carrierss as $carrierDetail)
                                                        @if($carrierDetail['carrier_name'] != '')
                                                            <option value="{{$carrierDetail['id']}}" {{ (isset($shippingDetails['shipping_carrier_id']) ? ($carrierDetail['id'] == $shippingDetails['shipping_carrier_id'] ? 'selected' : '') : '') }}>{{ $carrierDetail['carrier_name'] }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </select>
                                            <div class="position-absolute w-135px" style="left: 102%; top: 0;">
                                                <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#addNewShippingCarrierModal">Add New Carrier</button>

                                                <div class="modal fade" id="addNewShippingCarrierModal" tabindex="-1" role="dialog">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <form action=""></form>
                                                            <form id="addNewShippingCarrierForm" novalidate="novalidate">
                                                                <div class="modal-body">
                                                                    <h4 class="mb-5">Add New Carrier</h4>
                                                                    <div class="form-group row mb-4">
                                                                        <label for="" class="col-sm-3 col-form-label">Name</label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" class="form-control" id="carrier_name" name="carrier_name" placeholder="Enter Carrier Name">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mb-4">
                                                                        <div class="col-6">
                                                                            <button type="button" class="btn btn-outline-secondary w-100" data-dismiss="modal">Cancel</button>
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <button type="submit" class="btn btn-primary w-100" id="add_shipping_carrier_submit">Add Carrier</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="saved">
                                            <div>
                                                <label for="" class="col-form-label" id="shipping_shipping_carrier_saved"></label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="shippingCase">
                                    <td><label for="" class="col-form-label">Tracking No.</label></td>
                                    <td>
                                        <div class="inputing">
                                            <div class="d-flex justify-content-between">
                                                <div style="padding-top:0.57rem;">
                                                    <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="tracking_provided" id="ship_carrier_trackingnoyes"  {{ (isset($shippingDetails['tracking_provided']) ? ($shippingDetails['tracking_provided'] == 1 ? 'checked' : '') : '') }} value="1" style="width: auto;">
                                                        <label class="form-check-label" for="ship_carrier_trackingnoyes">Yes</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="tracking_provided" id="ship_carrier_trackingnono" {{ (isset($shippingDetails['tracking_provided']) ? ($shippingDetails['tracking_provided'] == 0 ? 'checked' : '') : '') }} value="0" style="width: auto;">
                                                        <label class="form-check-label" for="ship_carrier_trackingnono">No</label>
                                                    </div>
                                                </div>
                                                <div class="form-group row pr-0">
                                                    <label for="" class="col-sm-5 col-form-label">Provided By</label>
                                                    <div class="col-sm-7 pr-0">
                                                        <select name="provided_by" class="form-select w-100">
                                                            <option value="Shipping Carrier">Shipping Carrier</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="saved">
                                            <div><label for="" class="col-form-label">Provided by <span id='shipping_tracking_no_provided_by_saved'></span></label></div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                            <tbody class="internationalCase">
                                <tr >
                                    <td width="150px"><label class="col-form-label"> Logistics Handler </label></td>
                                    <td>
                                        <div class="inputing">
                                            <div class="btn-group">
                                                <button class="btn btn-primary font-size-12 supplier-btn selectShippingBtn selected" style="width:118px;" data-val="vendorCase">Vendor</button>
                                                <button class="btn btn-dark font-size-12 px-1 shipping-agent-btn selectShippingBtn" style="width:118px;" data-val="shippingCase">Shipping Agent</button>
                                            </div>
                                        </div>
                                        <div class="saved">
                                            <div>
                                                <label for="" class="col-form-label" id="logistics_handler_saved"></label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="shippingCase">
                                    <td><label for="" class="col-form-label">Shipping Agent</label></td>
                                    <td>
                                        <div class="inputing position-relative">
                                            <select name="shipping_agent" id="shipping_agent" class="form-control">
                                                @if(!empty($shipping_agents))
                                                    @foreach($shipping_agents as $key=> $shipping_agent)
                                                        @if(isset($shippingDetails->shipping_agent_id))
                                                            <option value="{{ $shipping_agent['id'] }}" {{ ($shipping_agent['id'] == $shippingDetails->shipping_agent_id ? 'selected' : '') }}>{{ $shipping_agent['first_name'] .' '. $shipping_agent['last_name']  }}</option>
                                                        @else
                                                            <option value="{{ $shipping_agent['id'] }}">{{ $shipping_agent['first_name'] .' '. $shipping_agent['last_name']  }}</option>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <option></option>
                                                @endif
                                            </select>
                                            <div class="position-absolute" style="left: 102%; top: 0; width: 135px;">
                                                <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#addNewAgentModal">Add New Agent</button>

                                                <div class="modal fade" id="addNewAgentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <form id="add_shipping_agent_form" novalidate="novalidate">
                                                                <div class="modal-body">
                                                                    <h4 class="mb-5">Add Shipping Agent</h4>
                                                                    <div class="form-group row mb-4">
                                                                        <label for="" class="col-sm-3 col-form-label">Company</label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" class="form-control" id="agent_company" name="agent_company" placeholder="Enter Company Name">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row mb-4">
                                                                        <label for="" class="col-sm-3 col-form-label">First Name</label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" class="form-control" id="agent_first_name" name="agent_first_name" placeholder="Enter First Name">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row mb-4">
                                                                        <label for="" class="col-sm-3 col-form-label">Last Name</label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" class="form-control" id="agent_last_name" name="agent_last_name" placeholder="Enter Last Name">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row mb-4">
                                                                        <label for="" class="col-sm-3 col-form-label">Email</label>
                                                                        <div class="col-sm-9">
                                                                            <input type="email" class="form-control" id="agent_email" name="agent_email" placeholder="Enter Email">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row mb-4">
                                                                        <label for="" class="col-sm-3 col-form-label">Phone Number</label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" class="form-control" id="agent_phone" name="agent_phone" placeholder="Enter Phone Number">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mb-4">
                                                                        <div class="col-6">
                                                                            <button type="button" class="btn btn-outline-secondary w-100" data-dismiss="modal">Cancel </button>
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <a id="add_shipping_agent" href="javascript:void(0)" class="btn btn-primary w-100"> Add Agent </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="saved">
                                            <div>
                                                <label for="" id="shipping_agent_saved" class="col-form-label"></label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="" class="col-form-label">LCL Settings</label></td>
                                    <td>
                                        <div class="inputing">
                                            <div class="d-flex">
                                                <div class="w-230px d-flex justify-content-between align-items-center">
                                                    <div class="custom-control custom-checkbox">
                                                        @if(isset($shippingDetails->lcl_cost_settings))
                                                            <input type="checkbox" name="lcl_cost_for" id="lcl-cost-for"
                                                                class="custom-control-input"
                                                                {{$shippingDetails->lcl_cost_settings == 0 ? '' : 'checked'}}
                                                            >
                                                        @else
                                                            <input type="checkbox" name="lcl_cost_for" id="lcl-cost-for"
                                                                class="custom-control-input"
                                                            >
                                                        @endif
                                                        <label class="custom-control-label" for="lcl-cost-for" style="font-size:13px;">LCL Cost Per</label>
                                                    </div>
                                                    <select name="lcl_cost_per" id="lcl_cost_per" class="form-control p-1 w-80px">
                                                        @if(isset($shippingDetails->lcl_cost_per))
                                                            <option value="CBM" {{$shippingDetails->lcl_cost_per =='CBM' ? 'selected' : ''}}>
                                                                CBM
                                                            </option>
                                                            <option value="KG" {{$shippingDetails->lcl_cost_per =='KG' ? 'selected' : ''}}>
                                                                KG
                                                            </option>
                                                        @else
                                                            <option value="CBM">CBM</option>
                                                            <option value="KG">KG</option>
                                                        @endif
                                                    </select>
                                                </div>
                                                <div class="w-90px mx-3 input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text bg-transparent pr-1">$</span>
                                                    </div>
                                                    <input type="text" id="lcl_cost" name="lcl_cost"
                                                        class="form-control border-left-0 p-0"
                                                        style="padding-top: 0.57rem;"
                                                        value="{{(isset($shippingDetails->lcl_cost) ? $shippingDetails->lcl_cost : '')}}"
                                                    >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="saved">
                                            <div>
                                                <label for="" class="col-form-label mr-4" id="lcl_cost_for_saved"></label>
                                                <label for="" class="col-form-label mr-4" id="lcl_cost_per_saved"></label>
                                                <label for="" class="col-form-label" id="lcl_cost_saved"></label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="" class="col-form-label">Container Settings</label></td>
                                    <td>
                                        <div class="inputing">
                                            <div class="d-flex">
                                                <div class="w-230px">
                                                    <label for="" class="col-form-label text-light1" style="width: 230px;">SIZE</label>
                                                </div>
                                                <div class="mx-3">
                                                    <label for="" class="col-form-label text-light1">COST</label>
                                                </div>
                                            </div>
                                            <div class="d-flex"> <!--20ft-->
                                                <div class="d-flex align-items-center py-1 border-right w-230px">
                                                    <div class="custom-control custom-checkbox w-60px">
                                                        <input type="checkbox"  id="20ft_checkbox"
                                                            name="twentyft_checkbox"
                                                            class="custom-control-input"
                                                        >
                                                        <label class="custom-control-label" for="20ft" style="font-size:13px;">20ft</label>
                                                    </div>
                                                    <div class="px-3">
                                                        =
                                                    </div>
                                                    <div class="w-40px">
                                                        <input type="text" name="twentyft_cost_per" class="form-control text-center form-control-sm p-0">
                                                    </div>
                                                    <div class="px-3">
                                                        CBM
                                                    </div>
                                                </div>
                                                <div class="mx-3 py-1">
                                                    <div class="input-group input-group-sm w-90px ">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text bg-transparent pr-1">$</span>
                                                        </div>
                                                        <input type="text" name="twentyft_cost" id="20ft_cost"
                                                            class="form-control border-left-0 p-0"
                                                            style="padding-top: 0.57rem;"
                                                        >
                                                    </div>
                                                </div>
                                            </div> <!--end of 20ft-->
                                            <div class="d-flex"> <!--40ft-->
                                                <div class="d-flex align-items-center py-1 border-right w-230px">
                                                    <div class="custom-control custom-checkbox w-60px">
                                                        <input type="checkbox"
                                                            name="fourtyft_checkbox"
                                                            id="40ft_checkbox"
                                                            class="custom-control-input"
                                                        >
                                                        <label class="custom-control-label" for="" style="font-size:13px;">40ft</label>
                                                    </div>
                                                    <div class="px-3">
                                                        =
                                                    </div>
                                                    <div class="w-40px">
                                                        <input type="text" name="fourtyft_cost_per" class="form-control text-center form-control-sm p-0">
                                                    </div>
                                                    <div class="px-3">
                                                        CBM
                                                    </div>
                                                </div>
                                                <div class="mx-3 py-1 w-90px ">
                                                    <div class="input-group input-group-sm">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text bg-transparent pr-1">$</span>
                                                        </div>
                                                        <input type="text" id="40ft_cost"
                                                            name="fourtyft_cost"
                                                            class="form-control border-left-0 p-0"
                                                            style="padding-top: 0.57rem;"
                                                        >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="d-flex"> <!--40HQ-->
                                                <div class="d-flex align-items-center py-1 border-right w-230px">
                                                    <div class="custom-control custom-checkbox w-60px">
                                                        <input type="checkbox" id="40hq_checkbox"
                                                            name="fourtyhq_checkbox"
                                                            class="custom-control-input"
                                                        >
                                                        <label class="custom-control-label" for="" style="font-size:13px;">40HQ</label>
                                                    </div>
                                                    <div class="px-3">
                                                        =
                                                    </div>
                                                    <div class="w-40px">
                                                        <input type="text" id="40hq_cost_per"
                                                            name="fourtyhq_cost_per"
                                                            class="form-control text-center form-control-sm p-0"
                                                        >
                                                    </div>
                                                    <div class="px-3">
                                                        CBM
                                                    </div>
                                                </div>
                                                <div class="mx-3 py-1 w-90px ">
                                                    <div class="input-group input-group-sm">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text bg-transparent pr-1">$</span>
                                                        </div>
                                                        <input type="text" id="20hq_cost" name="fourtyhq_cost"
                                                            class="form-control border-left-0 p-0"
                                                            style="padding-top: 0.57rem;"
                                                            value="{{(isset($container_details[2]['container_cost']) ? $container_details[2]['container_cost'] : '')}}"
                                                        >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="d-flex"> <!--45HQ-->
                                                <div class="d-flex align-items-center py-1 border-right w-230px">
                                                    <div class="custom-control custom-checkbox w-60px">
                                                        <input type="checkbox" name="fortyfivehq_checkbox" id="45hq_checkbox"
                                                            class="custom-control-input"
                                                        >
                                                        <label class="custom-control-label" for="" style="font-size:13px;">45HQ</label>
                                                    </div>
                                                    <div class="px-3">
                                                        =
                                                    </div>
                                                    <div class="w-40px">
                                                        <input type="text" id="45hq_cost_per"
                                                            name="fourtyfivehq_cost_per"
                                                            class="form-control text-center form-control-sm p-0"
                                                        >
                                                    </div>
                                                    <div class="px-3">
                                                        CBM
                                                    </div>
                                                </div>
                                                <div class="mx-3 py-1 w-90px ">
                                                    <div class="input-group input-group-sm">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text bg-transparent pr-1">$</span>
                                                        </div>
                                                        <input type="text" id="45hq_cost" name="fourtyfivehq_cost"
                                                            class="form-control border-left-0 p-0"
                                                            style="padding-top: 0.57rem;"
                                                        >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="saved" id="shipping_container_settings_saved">
                                            <div class="d-flex">
                                                <div class="w-230px">
                                                    <label for="" class="col-form-label text-light1" style="width: 230px;">SIZE</label>
                                                </div>
                                                <div class="mx-3">
                                                    <label for="" class="col-form-label text-light1">COST</label>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="" class="col-form-label">By Air</label></td>
                                    <td>
                                        <div class="inputing">
                                            @if(!empty($ship_to_air_details))
                                                @foreach($ship_to_air_details as $air_detail)
                                                    <div class="d-flex py-1">
                                                        <select name="shipping_air[][type]" id="shipping_air_type" class="form-control w-150px">
                                                            <option value="Standard" {{($air_detail['ship_type'] =='Standard' ? 'selected' : '' )}}>
                                                                Standard
                                                            </option>
                                                            <option value="Express" {{($air_detail['ship_type'] =='Express' ? 'selected' : '' )}}>
                                                                Express
                                                            </option>
                                                        </select>
                                                        <div class="input-group flex-nowrap w-90px mx-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text bg-transparent pr-1">$</span>
                                                            </div>
                                                            <input type="text" id="shipping_air[][price]" name="shipping_air[][price]"
                                                                class="form-control border-left-0 pl-1"
                                                                style="padding-top: 0.57rem;"
                                                            >
                                                        </div>
                                                        <select name="shipping_air[][messure_in]" id="shipping_air_messure_in" class="form-control w-90px">
                                                            <option value="KG" {{($air_detail['messuare_in'] =='KG' ? 'selected' : '' )}}>
                                                                KG
                                                            </option>
                                                            <option value="LBS" {{($air_detail['messuare_in'] =='LBS' ? 'selected' : '' )}}>
                                                                LBS
                                                            </option>
                                                            <option value="CBM" {{($air_detail['messuare_in'] =='CBM' ? 'selected' : '' )}}>
                                                                CBM
                                                            </option>
                                                        </select>
                                                    </div>
                                                @endforeach
                                            @else
                                                <div class="d-flex py-1">
                                                    <select name="shipping_air[][type]" id="shipping_air_type" class="form-control w-150px">
                                                        <option value="Standard">Standard</option>
                                                        <option value="Express">Express</option>
                                                    </select>
                                                    <div class="input-group flex-nowrap w-90px mx-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text bg-transparent pr-1">$</span>
                                                        </div>
                                                        <input type="text" id="shipping_air[][price]" name="shipping_air[][price]"
                                                                class="form-control border-left-0 pl-1"
                                                                style="padding-top: 0.57rem;" value="">
                                                    </div>
                                                    <select name="shipping_air[][messure_in]" id="shipping_air_messure_in" class="form-control w-90px">
                                                        <option value="KG">KG</option>
                                                        <option value="LBS">LBS</option>
                                                        <option value="CBM">CBM</option>
                                                    </select>
                                                </div>
                                            @endif
                                            <span class="btn text-primary1 add-new-btn p-0">
                                                <i class="bx bx-plus-circle font-size-16 align-middle"></i>
                                                Add
                                            </span>
                                        </div>
                                        <div class="saved">
                                            <div id="shipping_by_air_saved"></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="" class="col-form-label">Duties Cost:</label></td>
                                    <td>
                                        <div class="inputing">
                                            <div class="input-group flex-nowrap w-90px">
                                                <input type="text" id="duties_cost_percentage" name="duties_cost_percentage"
                                                    class="form-control border-right-0 pr-1"
                                                    style="padding-top: 0.57rem;"
                                                    value="{{(isset($shippingDetails->duties_cost) ? $shippingDetails->duties_cost : '')}}"
                                                >
                                                <div class="input-group-append">
                                                    <span class="input-group-text bg-transparent pl-0">%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="saved">
                                            <div>
                                                <label for="" class="col-form-label" id="shipping_duties_cost"></label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                            <tbody>
                                <tr>
                                    <td><label for="" class="col-form-label">Ship to Warehouse</label></td>
                                    <td class="w-380px">
                                        <div class="inputing">
                                            <select name="warehouses[]" id="warehouse_selection_ship_carrier" class="form-control select2 select2-multiple" multiple="multiple">
                                                @if(!empty($warehouse))
                                                    @foreach($warehouse as $key=> $warehouseDetail)
                                                        {{ $selectedValue = 0 }}
                                                        @if(!empty($shiptoWarehouseDetails))
                                                            @foreach($shiptoWarehouseDetails as $shipToWarehouse)
                                                                @if($shipToWarehouse['warehouse_value'] == $warehouseDetail['id'])
                                                                    {{  $selectedValue = 1 }}
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                        @if($warehouseDetail['warehouse_name'] != '')
                                                            <option value="{{ $warehouseDetail['id'] }}" {{$selectedValue == 1 ? 'selected' :'' }}>{{ $warehouseDetail['warehouse_name']  }}</option>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <option></option>
                                                @endif
                                            </select>
                                        </div>
                                        <div class="saved">
                                            <div id="ship_to_warehouse_saved"></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <div class="inputing">
                                            <div class="custom-control custom-checkbox mb-4">
                                                <input type="checkbox" name="direct_to_amazon" class="custom-control-input" id="direct-to-amazon">
                                                <label class="custom-control-label" for="direct-to-amazon">Direct To Amazon</label>
                                            </div>
                                        </div>
                                        <div class="saved"><div></div></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                <a href="{{URL('vendors')}}" class="btn btn-outline-secondary">Cancel</a>
                                            </div>
                                            <div class="text-right">
                                                <button class="btn btn-outline-primary w-md save" data-type="1">Save</button>
                                                <button class="btn btn-primary w-md save next" data-type="2">Save And Next</button>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>

                <div class="tab-pane" id="payment-terms">
                    <form id="payment_forms">
                        <div class="form-group row mb-3">
                            <label for="" class="col-sm-2 col-form-label">Deposit</label>
                            <div class="col-sm-10 row">
                                <div class="col-sm-2 px-1">
                                    <!-- <input type="text" class="form-control" placeholder="%"> -->
                                    <div class="input-group mb-2 mr-sm-3">
                                        <input type="text" class="form-control border-right-0" id="deposit_per" name="deposit_per"
                                               placeholder="" onkeypress="return isNumber(event,this)" maxlength="12">
                                        <div class="input-group-append">
                                            <div class="input-group-text bg-transparent">%</div>
                                        </div>
                                    </div>
                                </div>
                                <label for="" class="col-sm-1 col-form-label">Due</label>
                                <div class="col-sm-2 px-1">
                                    <input type="text" class="form-control" id="deposit_due" name="deposit_due" maxlength="12" onkeypress="return isNumber(event,this)">
                                </div>
                                <label for="" class="col-sm-1 col-form-label">Days</label>
                                <div class="col-sm-2 px-1">
                                    <select name="deposit_day" id="deposit_day" class="form-select">
                                        <option value="1">Before</option>
                                        <option value="2">After</option>
                                    </select>
                                </div>
                                <div class="col-sm-4 px-1">
                                    @if(!empty($payment_term))
                                        <select name="desposit_term_id" id="desposit_term_id" class="form-select">
                                            @foreach($payment_term as $type)
                                                <option value="{{$type['id']}}">{{$type['payment_type']}}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">Balance</label>
                            <div class="col-sm-10 row">
                                <div class="col-sm-2 px-1">
                                    <div class="input-group mb-2 mr-sm-3">
                                        <input type="text" class="form-control border-right-0" id="balance_per" name="balance_per"
                                               placeholder="" onkeypress="return isNumber(event,this)" maxlength="12">
                                        <div class="input-group-append">
                                            <div class="input-group-text bg-transparent">%</div>
                                        </div>
                                    </div>
                                </div>
                                <label for="" class="col-sm-1 col-form-label">Due</label>
                                <div class="col-sm-2 px-1">
                                    <input type="text" class="form-control" id="balance_due" maxlength="12" name="balance_due" onkeypress="return isNumber(event,this)" maxlength="12">
                                </div>
                                <label for="" class="col-sm-1 col-form-label">Days</label>
                                <div class="col-sm-2 px-1">
                                    <select name="balance_day" id="balance_day" class="form-select">
                                        <option value="1">Before</option>
                                        <option value="2">After</option>
                                    </select>
                                </div>
                                <div class="col-sm-4 px-1">
                                    @if(!empty($payment_term))
                                        <select name="balance_term_id" id="balance_term_id" class="form-select">
                                            @foreach($payment_term as $type)
                                                <option value="{{$type['id']}}">{{$type['payment_type']}}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row mt-5">
                            <div class="col-md-8">
                                <div class="row justify-content-between">
                                    <div class="col-4">
                                        <a href="{{URL('vendors')}}" class="btn btn-outline-secondary">Cancel</a>
                                    </div>
                                    <div class="col-8 text-right">
                                        <button class="btn btn-outline-primary w-md save save_payment" data-val="0">Save</button>
                                        <button class="btn btn-primary w-md save next save_payment" data-val="1">Save And Next</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
<script src="{{ asset('js/jquery.validate.js')}}"></script>
<script src="{{ asset('js/my.validate.js')}}"></script>
<script src="{{ asset('assetsnew/js/jquery.serializejson.js')}}"></script>
<script src="{{ asset('assetsnew/js/jquery.sortElements.js')}}"></script>

<script>
    // apply sorting in contact listing
    function sort_table(){
        var table = $('#list');
        $('.sorting')
            .each(function(){
                var th = $(this),
                    thIndex = th.index(),
                    inverse = false;

                th.on('click',function(){
                    console.log($(this));
                    table.find('td').filter(function(){
                        return $(this).index() === thIndex;
                    }).sortElements(function(a, b){
                        return $.text([a]) > $.text([b]) ?
                            inverse ? -1 : 1
                            : inverse ? 1 : -1;

                    }, function(){

                        return this.parentNode;

                    });

                    inverse = !inverse;

                });
        });
    }
    //apply sorting in blackout date sorting
    function bsort_table(){
        var table = $('#blackout-dates-table');
        $('.sorting')
        .wrapInner('<span title="sort this column"/>')
        .each(function(){

            var th = $(this),
                thIndex = th.index(),
                inverse = false;

            th.click(function(){

                table.find('td').filter(function(){

                    return $(this).index() === thIndex;

                }).sortElements(function(a, b){
                    var sa = $.text([a]);
                    var sb = $.text([b]);

                    var ia = parseInt(sa);
                    var ib = parseInt(sb);

                    if (!isNaN(ia) && !isNaN(ib)) {
                        return ia > ib ? inverse ? -1 : 1 : inverse ? 1 : -1;
                    }

                    return sa > sb ?
                        inverse ? -1 : 1
                        : inverse ? 1 : -1;

                }, function(){

                    // parentNode is the element we want to move
                    return this.parentNode;

                });

                inverse = !inverse;

            });

        });
    }

    $(function(){
        /**
            Variables
         */
    //    var vendor = {"id":95,"type":1};
        var vendor = {};

        var marketplace_id = {{session('MARKETPLACE_ID')}};
        var cardBody = $('.card-body');
        let errMsg = $('<div class="alert alert-danger alert-dismissible" style="position:fixed; width:400px; top: 80px; right:10px;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4>Error! </h4> <p class="msg"></p> </div>');
        let successMsg = $('<div class="alert alert-success alert-dismissible" style="position:fixed; width:400px; top: 80px; right:10px;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4>Success! </h4> <p class="msg"></p> </div>');

        const VENDOR_TYPE_DOMESTIC = 1
        const VENDOR_TYPE_INTERNATIONAL = 2

        /**
            Common Functions
         */
        function showAlert(type, msg){
            $(".alert").remove();
            let alrt = type ? successMsg : errMsg ;
            alrt.find('.msg').text(msg)
            cardBody.prepend(alrt)
            setTimeout(() => {
                $('.alert').remove()
            }, 10000);
        }

        function isNumber(evt,obj) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains)
                if (charCode == 46) return false;
            if (charCode == 46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function makeid(length) {
            var result = '';
            var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;
            for (var i = 0; i < length; i++) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            return result;
        }

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) ||/^[A-Za-z0-9\-\_]+$/.test(value);
        }, "Letters only please");

        /**
            Initailize
         */
        sort_table();
        bsort_table();

        $(".timepicker").timepicker({
            icons: {
                up: "mdi mdi-chevron-up",
                down: "mdi mdi-chevron-down"
            }
        }),

        $(".allow-numeric").bind("keypress", function (e) {
            var keyCode = e.which ? e.which : e.keyCode

            if (!(keyCode >= 48 && keyCode <= 57)) {
            $(".error").css("display", "inline");
            return false;
            }else{
            $(".error").css("display", "none");
            }
        });

        $.fn.datepicker.defaults.format = "mm/dd/yyyy";

        $(".select2").select2();

        $('[data-toggle="tab"]').not('#basic-details-tab').on('click', function(e){
            if(!vendor.id) {
                e.stopPropagation()
                showAlert(false, 'Please fill up basic details.')
            }
        })

        $('[data-toggle="tab"]').on('show.bs.tab', function(){
            if(vendor.type == VENDOR_TYPE_INTERNATIONAL){
                $('.internationalCase').css('display', '')
                $('.domesticCase').css('display', 'none')
            }else{
                $('.domesticCase').css('display', '')
                $('.internationalCase').css('display', 'none')
            }
        })

        var editButton = $('<button class="btn text-primary p-0 ml-2"><i class="mdi mdi-square-edit-outline"></i></button>')
        editButton.on('click', function(e){
            e.preventDefault()
            td = $(this).parents('td')
            td.find('.inputing').css('display', 'block');
            td.find('.saved').css('display', '')
        })
        $('div.saved>div').append(editButton);

        let weeklyDaysDiv = $('.weekly-days')
        let weeklyDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
        let days= 1
        weeklyDays.forEach(function(i){
            let btn = $('<button class="date" data-val="'+ i +'"/>')
            days++
            btn.text(i)
            btn.on('click', onClickDateBtn)
            weeklyDaysDiv.append(btn)
        })

        let monthlyDaysDiv = $('.monthly-days')
        for(i = 0; i < 5; i++){
            let div = $('<div/>')
            for(j = 1; j < 8; j++ ){
                if( i * 7 + j < 32){
                    let btn = $('<button class="date" data-val="'+(i * 7 + j)+'"/>')
                    btn.text(i * 7 + j)
                    btn.on('click', onClickDateBtn)
                    div.append(btn)
                }
            }
            monthlyDaysDiv.append(div)
        }

        function onClickDateBtn(e){
            e.preventDefault()
            $(this).toggleClass('checked')
            let date = $(this).text()
            let opt = $(this).parents('.schedule-panels').find('option[value="'+ date + '"]')
            opt.prop('selected', !opt.prop('selected'))
            $(this).parents('.schedule-panels').find('select').trigger('change')
        }

        $('.grpBtn').on('click', function(e){
            e.preventDefault();
            $(this).addClass('btn-primary').addClass('selected').removeClass('btn-dark')
            $(this).siblings().addClass('btn-dark').removeClass('btn-primary').removeClass('selected')
        })

        /**
            Basic Details
         */

        let basic_details = $('#basic-details')
        let basic_details_form = basic_details.find('form')

        basic_details_form.validate({ // initialize the plugin
            rules: {
                vendor_name: {required: true},
                country: {required: true},
                address_line_1: {required: true},
                state: {required: true},
                zipcode: {required: true,lettersonly:true},
            },
            messages: {
                vendor_name: { required: "Enter vendors name"},
                country: { required: "Select country"},
                address_line_1: { required: "Enter address"},
                state: { required: "Enter state"},
                zipcode: { required: "Enter postal code",lettersonly:"Only Alphabets, Numbers & hyphen allowed."}
            }
        });

        $('#country').change(function () {
            var country = $(this).val();
            var request_type = 'Get_all_state';
            if (country) {
                $.ajax({
                    type: "POST",
                    url: "{{route('vendors.store')}}",
                    data: {_token:'{{ csrf_token() }}',country: country, request_type: request_type},
                    success: function (res) {
                        if (res) {
                            $("#state").empty();
                            $("#state").append('<option value="">Select</option>');
                            $.each(res, function (key, value) {
                                $("#state").append('<option value="' + key + '">' + value + ' ('+ key +')</option>');
                            });

                        } else {
                            $("#state").empty();
                        }
                    }
                });
            } else {
                $("#state").empty();
                $("#city").empty();
            }
        });

        basic_details.on('click', '.save', function (e) {
            e.preventDefault();
            if(!basic_details_form.valid()) return;
            let isNext = $(this).hasClass('next')
            var vendor_id = vendor.id ? vendor.id : ''
            let vendor_type = $('#vendor_type').find('.btn-primary').data('val')
            let vendor_type_text = $('#vendor_type').find('.btn-primary').data('text')
            let data = basic_details_form.serializeJSON()
            var countrytext = $('#country option:selected').text();
            var statetext = $('#state option:selected').text();

            $.ajax({
                type: "POST",
                url: "{{route('vendors.store')}}",
                data: $.extend({
                    _token:'{{ csrf_token() }}',
                    insert_type: 'insert_update_basic_details',
                    vendor_id: vendor_id,
                    marketplace_id: {{session('MARKETPLACE_ID')}},
                    vendor_type: vendor_type
                }, data),
                success: function (res) {
                    if (res.success) {
                        showAlert(true, res.message)
                        basic_details.find('.inputing').css('display', 'none')
                        basic_details.find('.saved').css('display', 'block')

                        $("#vendor_name_label").text(data.vendor_name);
                        $("#vendor_type_label").text(vendor_type_text);
                        $("#country_label").text(countrytext);
                        $("#address_line_1_label").text(data.address_line_1);
                        $("#address_line_2_label").text(data.address_line_2);
                        $("#state_label").text(statetext);
                        $("#postal_code_lable").text(data.zipcode);

                        vendor.id = res.vendor_id
                        vendor.type = vendor_type

                        isNext && $('#contacts-tab').click()
                    } else {
                        showAlert(false, res.message)
                    }
                }
            });
        });

        /**
            Contacts
         */
        let contacts = $('#contacts')

        contacts.find('th').click(function(e){
            e.preventDefault()
            e.stopPropagation()
            if($(this).hasClass('sorting') || $(this).hasClass('sorting_desc')){
                contacts.find('th').attr('class', 'sorting');
                $(this).attr('class', 'sorting_asc')
            } else if($(this).hasClass('sorting_asc')){
                $(this).attr('class', 'sorting_desc')
            }
        })

        contacts.find("tbody").on('focusin', 'td', function(e){
            if($(e.target).is("input")){
                $(this).css("border", "1px solid #E1EDFF");
            }
        });

        contacts.find("tbody").on('focusout', 'td', function(){
            $(this).css('border', 'none')
            $(this).css('border-top', '1px solid #eff2f7');
        });

        contacts.on('click', '.add-row', function(e){
            e.preventDefault()
            $('#contacts .add-row-tr').before('\
                <tr>\
                    <input name="contacts[][id]" type="hidden" value="">\
                    <td class="align-middle edit">\
                        <input name="contacts[][first_name]" type="text" placeholder="First Name" value="" class="v-required">\
                    </td>\
                    <td  class="align-middle edit">\
                        <input name="contacts[][last_name]" type="text" placeholder="Last Name" value="" class="v-required">\
                    </td>\
                    <td  class="align-middle edit">\
                        <input name="contacts[][title]" type="text" placeholder="Title" value="" class="v-required">\
                    </td>\
                    <td  class="align-middle edit">\
                        <input name="contacts[][email]" type="text" placeholder="Email" value="" class="v-required v-email">\
                    </td>\
                    <td  class="align-middle edit">\
                        <input name="contacts[][phone_number]" type="tel" placeholder="Phone number" value="" class="v-required v-phone">\
                    </td>\
                    <td  class="align-middle edit">\
                        <i class="mdi mdi-star font-size-18" data-toggle="tooltip" title="Make Primary Contact"></i>\
                    </td>\
                </tr>\
            ')
            $('.mdi-star').tooltip();
        });

        contacts.find('tbody').on('click', '.mdi-star', function(e){
            e.preventDefault();
            contacts.find('tbody tr').removeClass('active');
            $(this).parents('tr').addClass('active');
        })

        contacts.on("click", ".save", function(e) {
            e.preventDefault();

            let isValid = true

            contacts.find('.v-required').each(function(){
                if(!$(this).val()){
                    $(this).addClass('error');
                    isValid = false
                    $(this).on('input', function(){
                        if($(this).val()){
                            $(this).removeClass('error')
                        }else{
                            $(this).addClass('error')
                        }
                    })
                }
            })

            contacts.find('.v-email').each(function(){
                if(!validateEmail($(this).val())){
                    $(this).addClass('error');
                    isValid = false
                    $(this).on('input', function(){
                        if(validateEmail($(this).val())){
                            $(this).removeClass('error')
                        }else{
                            $(this).addClass('error')
                        }
                    })
                }
            })

            contacts.find('.v-phone').each(function(){
                if(!validatePhone($(this).val())){
                    $(this).addClass('error');
                    isValid = false
                    $(this).on('input', function(){
                        if(validatePhone($(this).val())){
                            $(this).removeClass('error')
                        }else{
                            $(this).addClass('error')
                        }
                    })
                }
            })

            if(!isValid) return;

            let data = contacts.find('form').serializeJSON()
            let isNext = $(this).hasClass('next');
            let primary_contact = contacts.find('form table tbody tr').index(contacts.find('form table tbody tr.active'))

            $.ajax({
                type: "POST",
                url: "{{route('vendors.store')}}",
                data: $.extend({
                    _token:'{{ csrf_token() }}',
                    insert_type: 'insert_contacts',
                    vendor_id: vendor.id,
                    primary_contacts: primary_contact
                }, data),
                success: function (res) {
                    if(res.success){
                        if(res.success_msg != ''){
                            showAlert(true, res.success_msg)
                        }

                        $('.vendor_contacts').each(function(){
                            let self = $(this)
                            let form = $('<div/>')
                            $(data.contacts).each(function(i, contact){
                                form.append('\
                                    <div class="custom-control custom-checkbox custom-checkbox-primary pl-5 py-1">\
                                        <input type="checkbox" data-contact="' + contact.email +'"  class="custom-control-input" name="' + self.data('name') + '[]' + '" id="' + self.data('name') + '-' + i + '">\
                                        <label class="custom-control-label" for="' + self.data('name') + '-' + i + '">' + contact.first_name +' '+ contact.last_name + '</label>\
                                    </div>\
                                ')
                                self.prev().append('<option value="'+contact.email+'">'+contact.email+'</option>')
                            })
                            $(this).html('').append(form)
                            let count = $(this).parents('.dropdown-menu').find('input:checkbox:checked').length
                            let str = count ? ( count + ' Contacts Selected') : 'Select Contact'
                            $(this).parents('.dropdown').find('button span').text(str)
                            let email = $(this).next().text()
                            let opt = $(this).parents('.dropdown').find('option[value="'+ email + '"]')
                            opt.prop('selected', !opt.prop('selected'))
                            //$(this).html('').append(form)
                        })

                        isNext && $("#lead-time-tab").click()
                    }else{
                        if(res.message != ''){
                            showAlert(false, res.message)
                        }
                        //showAlert(false, res.message)
                    }
                }
            });
        });

        /**
         *  Lead time
         */
        var lead_time = $('#lead-time')

        lead_time.find('form.domesticCase').validate({
            rules: {
                order_prep_days: {required: true,digits:true},
                to_warehouse_days: {required: true,digits:true},
                to_amazon: {required: true,digits:true},
                po_to_prep_days: {required: true,digits:true},
                safety_days: {required: true,digits:true},
                total_lead_time_days: {required: true,digits:true},
            },
            errorPlacement: function(error, element) {

                if (element.attr("name")== "order_prep_days") {
                    error.insertBefore("#order_prep_days");
                }
                if (element.attr("name")== "to_warehouse_days") {
                    error.insertBefore("#to_warehouse_days");
                }
                if (element.attr("name")== "to_amazon") {
                    error.insertBefore("#to_amazon");
                }
                if (element.attr("name")== "po_to_prep_days") {
                    error.insertBefore(element.parent('.form-group'));
                }
                if (element.attr("name")== "safety_days") {
                    error.insertBefore(element.parent('.form-inline'));
                }
                if (element.attr("name")== "total_lead_time_days") {
                    error.insertBefore("#total_lead_time_days");
                }
            },
            messages: {
                order_prep_days: { required: "Enter order prep days",digits:"Please enter only numbers"},
                to_warehouse_days: { required: "Enter warehouse days",digits:"Please enter only numbers"},
                to_amazon: { required: "Enter to amazon days",digits:"Please enter only numbers"},
                po_to_prep_days: {required: "Enter po to prep days",digits:"Please enter only numbers"},
                safety_days: {required: "Enter safety days",digits:"Please enter only numbers"},
                total_lead_time_days: {required: "Enter total lead time days",digits:"Please enter only numbers"},
            }
        });

        $('#boat-btn').click(function(e){
            e.preventDefault()
            $('#boat-btn').removeClass('btn-light1').addClass('btn-primary');
            $('#plane-btn').removeClass('btn-primary').addClass('btn-light1');
            //$("#lead_time_check").val(1);
            $('form#boat-panel').removeClass('d-none')
            $('form#plane-panel').addClass('d-none')
        })
        $('#plane-btn').click(function(e){
            e.preventDefault()
            $('#plane-btn').removeClass('btn-light1').addClass('btn-primary')
            $('#boat-btn').removeClass('btn-primary').addClass('btn-light1')
            //$("#lead_time_check").val(2);
            $('form#boat-panel').addClass('d-none')
            $('form#plane-panel').removeClass('d-none')
        })
        $('.input-edit').focus(function(e){
            $(this).parent().removeClass('border-bottom').css('border-bottom', '1px solid #227CFF');
        })
        $('.input-edit').on('change blur', function(){
            if($(this).val()){
                $(this).parent().next().removeClass('d-none').find('span').html($(this).val() + ' Days');
                $(this).parent().addClass('d-none');
            }else{
                $(this).parent().addClass('border-bottom');
            }
        })
        $('.input-show').click(function(){
            $(this).parent().addClass('d-none');
            $(this).parent().prev().removeClass('d-none').find('input').focus();
        })

        $('form#boat-panel').validate({ // initialize the plugin
            rules: {
                product_manuf_days_boat: {required: true,digits: true},
                to_port_days_boat: {required: true,digits: true},
                transit_time_days_boat: {required: true,digits: true},
                to_warehouse_days_boat: {required: true,digits: true},
                to_amazon_boat: {required: true,digits: true},
                po_to_production_days_boat: {required: true,digits: true},
                safety_days_boat: {required: true,digits: true},
                total_lead_time_days_boat: {required: true,digits: true},
            },

            errorPlacement: function(error, element) {

                if (element.attr("name")== "product_manuf_days_boat") {
                    error.insertBefore("#product_manuf_days_boat");
                }
                if (element.attr("name")== "to_port_days_boat") {
                    error.insertBefore("#to_port_days_boat");
                }
                if (element.attr("name")== "transit_time_days_boat") {
                    error.insertBefore("#transit_time_days_boat");
                }
                if (element.attr("name")== "to_warehouse_days_boat") {
                    error.insertBefore("#to_warehouse_days_boat");
                }
                if (element.attr("name")== "to_amazon_boat") {
                    error.insertBefore("#to_amazon_boat");
                }
                if (element.attr("name")== "po_to_production_days_boat") {
                    error.insertBefore(element.parent('.form-group'));
                }
                if (element.attr("name")== "safety_days_boat") {
                    error.insertBefore(element.parent('.form-inline'));
                }
                if (element.attr("name")== "total_lead_time_days_boat") {
                    error.insertBefore("#total_lead_time_days_boat");
                }
            },
            messages: {
                //product_manuf_days_boat: { required: "Enter product manufacture days", digits:"Enter only numbers"},
                product_manuf_days_boat: { required: "Enter order prep days", digits:"Enter only numbers"},
                to_port_days_boat: { required: "Enter transported to port days", digits:"Enter only numbers"},
                transit_time_days_boat: { required: "Enter transit time days", digits:"Enter only numbers"},
                to_warehouse_days_boat: { required: "Enter warehouse days", digits:"Enter only numbers"},
                to_amazon_boat: { required: "Enter to amazon days", digits:"Enter only numbers"},
                //po_to_production_days_boat: {required: "Enter po to production days", digits:"Enter only numbers"},
                po_to_production_days_boat: {required: "Enter po to prep days", digits:"Enter only numbers"},
                safety_days_boat: {required: "Enter safety days", digits:"Enter only numbers"},
                total_lead_time_days_boat: {required: "Enter total lead time days", digits:"Enter only numbers"},
            },
        });

        $('form#plane-panel').validate({ // initialize the plugin
            rules: {
                product_manuf_days_plane: {required: true,digits:true},
                to_port_days_plane: {required: true,digits:true},
                transit_time_days_plane: {required: true,digits:true},
                to_warehouse_days_plane: {required: true,digits:true},
                to_amazon_plane: {required: true,digits:true},
                po_to_production_days_plane: {required: true,digits:true},
                safety_days_plane: {required: true,digits:true},
                total_lead_time_days_plane: {required: true,digits:true},
            },
            errorPlacement: function(error, element) {

                if (element.attr("name")== "product_manuf_days_plane") {
                    error.insertBefore("#product_manuf_days_plane");
                }
                if (element.attr("name")== "to_port_days_plane") {
                    error.insertBefore("#to_port_days_plane");
                }
                if (element.attr("name")== "transit_time_days_plane") {
                    error.insertBefore("#transit_time_days_plane");
                }
                if (element.attr("name")== "to_warehouse_days_plane") {
                    error.insertBefore("#to_warehouse_days_plane");
                }
                if (element.attr("name")== "to_amazon_plane") {
                    error.insertBefore("#to_amazon_plane");
                }
                if (element.attr("name")== "po_to_production_days_plane") {
                    error.insertBefore(element.parent('.form-group'));
                }
                if (element.attr("name")== "safety_days_plane") {
                    error.insertBefore(element.parent('.form-inline'));
                }
                if (element.attr("name")== "total_lead_time_days_plane") {
                    error.insertBefore("#total_lead_time_days_plane");
                }
            },
            messages: {
                //product_manuf_days_plane: { required: "Enter product manufacture days", digits:"Enter only numbers"},
                product_manuf_days_plane: { required: "Enter order prep days", digits:"Enter only numbers"},
                to_port_days_plane: { required: "Enter transported to port days", digits:"Enter only numbers"},
                transit_time_days_plane: { required: "Enter transit time days", digits:"Enter only numbers"},
                to_warehouse_days_plane: { required: "Enter warehouse days", digits:"Enter only numbers"},
                to_amazon_plane: { required: "Enter to amazon days", digits:"Enter only numbers"},
                //po_to_production_days_plane: {required: "Enter po to production days", digits:"Enter only numbers"},
                po_to_production_days_plane: {required: "Enter po to prep days", digits:"Enter only numbers"},
                safety_days_plane: {required: "Enter safety days", digits:"Enter only numbers"},
                total_lead_time_days_plane: {required: "Enter total lead time days", digits:"Enter only numbers"},
            }
        });

        lead_time.find('.vendor_contacts').on('change', 'input:checkbox', function(){
            let count = $(this).parents('.position-absolute').find('input:checkbox:checked').length
            let str = count ? ( count + ' Contacts Selected') : 'Select Contact'
            $(this).parents('.position-relative').find('button span').text(str)
            let email = $(this).data('contact')
            let opt = $(this).parents('.position-relative').find('option[value="'+ email + '"]')
            opt.prop('selected', !opt.prop('selected'))
        })

        lead_time.on("click", ".save", function (e){
            e.preventDefault();

            let lead_time_id = vendor.lead_time_id ? vendor.lead_time_id : ''
            let data
            if(lead_time.find('form.domesticCase').is(':visible')){
                if(!lead_time.find('form.domesticCase').valid()) return;
                data = lead_time.find('form.domesticCase').serializeJSON()
            }else{
                if($('form#boat-panel').is(':visible')){
                    // if($('form#boat-panel').valid()) return;
                    data = $('form#boat-panel').serializeJSON()
                }else{
                    // if($('form#plane-panel').valid()) return;
                    data = $('form#plane-panel').serializeJSON()
                }
            }

            isNext = $(this).hasClass('next')

            $.ajax({
                type: "POST",
                url: "{{route('vendors.store')}}",
                data: $.extend({
                    _token:'{{ csrf_token() }}',
                    insert_type:'insert_update_lead_time',
                    vendor_id: vendor.id,
                    lead_time_id: lead_time_id,
                }, data),
                success: function (res) {
                    if (res.success) {
                        showAlert(true, res.message)
                        vendor.lead_time_id = res.lead_time_id;
                        isNext && $('#order-settings-tab').click()
                    } else {
                        showAlert(false, 'Error')
                    }
                },
                error: function(){
                    showAlert(false, 'Error')
                }
            });
        });

        $(document).on('change','.calculate',function(){
            if($(".internationalCase").is(":visible")) {
                if($("#boat-panel").is(":visible")){
                    var product_manuf_days_boat = $("#product_manuf_days_boat").val() ? $("#product_manuf_days_boat").val() : 0 ;
                    var to_port_days_boat = $("#to_port_days_boat").val() ? $("#to_port_days_boat").val() : 0 ;
                    var transit_time_days_boat = $("#transit_time_days_boat").val() ? $("#transit_time_days_boat").val() : 0 ;
                    var to_warehouse_days_boat = $("#to_warehouse_days_boat").val() ? $("#to_warehouse_days_boat").val() : 0 ;
                    var to_amazon_boat  = $("#to_amazon_boat").val() ? $("#to_amazon_boat").val() : 0 ;
                    var po_to_production_days_boat = $("#po_to_production_days_boat").val() ? $("#po_to_production_days_boat").val() : 0 ;
                    var safety_days_boat = $("#safety_days_boat").val() ? $("#safety_days_boat").val() : 0 ;
                    var total_lead_time_boat = parseInt(product_manuf_days_boat)+parseInt(to_port_days_boat)+parseInt(transit_time_days_boat)+parseInt(to_warehouse_days_boat)+parseInt(to_amazon_boat)+parseInt(po_to_production_days_boat)+parseInt(safety_days_boat);
                    $("#total_lead_time_days_boat").val(total_lead_time_boat);
                    $("#total_lead_time_days_label_boat").html(total_lead_time_boat + ' Days');
                }else{
                    var product_manuf_days_plane = $("#product_manuf_days_plane").val() ? $("#product_manuf_days_plane").val() : 0 ;
                    var to_port_days_plane = $("#to_port_days_plane").val() ? $("#to_port_days_plane").val() : 0 ;
                    var transit_time_days_plane = $("#transit_time_days_plane").val() ? $("#transit_time_days_plane").val() : 0 ;
                    var to_warehouse_days_plane = $("#to_warehouse_days_plane").val() ? $("#to_warehouse_days_plane").val() : 0 ;
                    var to_amazon_plane  = $("#to_amazon_plane").val() ? $("#to_amazon_plane").val() : 0 ;
                    var po_to_production_days_plane = $("#po_to_production_days_plane").val() ? $("#po_to_production_days_plane").val() : 0 ;
                    var safety_days_plane = $("#safety_days_plane").val() ? $("#safety_days_plane").val() : 0 ;
                    var total_lead_time_plane = parseInt(product_manuf_days_plane)+parseInt(to_port_days_plane)+parseInt(transit_time_days_plane)+parseInt(to_warehouse_days_plane)+parseInt(to_amazon_plane)+parseInt(po_to_production_days_plane)+parseInt(safety_days_plane);
                    $("#total_lead_time_days_plane").val(total_lead_time_plane);
                    $("#total_lead_time_days_label_plane").html(total_lead_time_plane + ' Days');
                }
            }
            else{
                var order_prep_days = $("#order_prep_days").val() ? $("#order_prep_days").val() : 0 ;
                var po_to_prep_days = $("#po_to_prep_days").val() != '' ?  $("#po_to_prep_days").val() : 0 ;
                var to_warehouse_days = $("#to_warehouse_days").val() ? $("#to_warehouse_days").val() : 0;
                var to_amazon  = $("#to_amazon").val() ? $("#to_amazon").val() : 0;
                var safety_days = $("#safety_days").val() ? $("#safety_days").val() : 0;
                var total_lead_time = parseInt(order_prep_days)+parseInt(po_to_prep_days)+parseInt(to_warehouse_days)+parseInt(to_amazon)+parseInt(safety_days);
                $("#total_lead_time_days").val(total_lead_time);
                $("#total_lead_time_days_label").html(total_lead_time + ' Days');
            }

        });

        /**
            Order Settings
         */
        var order_settings = $('#order-settings')
        order_settings.find('form').validate({
            ignore: [],
            rules: {
                order_volume: {
                    required: true,
                    number: true
                },
                'days[]': {
                    required: function(){
                        return $('#reorder_schedule').val() != 'on_demand'
                    },
                    minlength: function(){
                        if($('#reorder_schedule').val() == 'weekly' || $('#reorder_schedule').val() == 'monthly') return 1;
                        if($('#reorder_schedule').val() == 'bi_weekly' || $('#reorder_schedule').val() == 'bi_monthly') return 2;
                    },
                    maxlength: function(){
                        if($('#reorder_schedule').val() == 'weekly' || $('#reorder_schedule').val() == 'monthly') return 1;
                        if($('#reorder_schedule').val() == 'bi_weekly' || $('#reorder_schedule').val() == 'bi_monthly') return 2;
                    },
                }
            },
            errorPlacement: function (error, element) {
               $(error).insertAfter(element.parents(".inputing"));
           }
        })

        let schedule_type = {
            on_demand: 'On Demand',
            weekly: 'Weekly',
            bi_weekly: 'Bi-Weekly',
            monthly: 'Monthly',
            bi_monthly: 'Bi-Monthly'
        }
        order_settings.find('#reorder_schedule').change(function(){
            type = schedule_type[$(this).val()]
            let td = $(this).parents('td')
            td.find('.saved label').eq(0).text(type)
            order_settings.find('.schedule-panels>div').addClass('d-none')
            order_settings.find('.schedule-panels').find('.' + $(this).val() + '-panel').removeClass('d-none')
            let slt = $(this).next().find('select')
            slt.empty()
            if($(this).val() == 'weekly' || $(this).val() == 'bi_weekly'){
                let weeklyDays = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
                weeklyDays.forEach(function(i){
                    slt.append('<option value="'+i+'">'+i+'</option>')
                })
            }else{
                for(i=1; i<32; i++){
                    slt.append('<option value="'+i+'">'+i+'</option>')
                }
            }

        })

        order_settings.on('click', '.save', function(e){
            e.preventDefault();
            if(!order_settings.find('form').valid()) return;

            var isNext = $(this).hasClass('next');
            let data = order_settings.find('form').serializeJSON()

            $.ajax({
                type: "POST",
                url: "{{route('vendors.store')}}",
                data: $.extend({
                    _token:'{{ csrf_token() }}',
                    insert_type:'insert_update_order_setting',
                    vendor_id: vendor.id,
                }, data),
                success: function (res) {
                    if (res.success) {
                        showAlert(true, res.message)
                        order_settings.find('.inputing').css('display', 'none')
                        order_settings.find('.saved').css('display', 'block')

                        let types = ['Days', 'Calendar Months']

                        $('.saved label.order_volume').text(data['order_volume'] + ' ' + types[data['order_volume_type'] - 1])
                        $('.saved .reorder_schedule label').remove()
                        $('.saved .reorder_schedule button').before('<label class="col-form-label mr-4" style="padding-left: 0.75rem;">' + data.reorder_schedule + '</label>')
                        $(data.days).each(function(i){
                            $('.saved .reorder_schedule button').before('<label for="" class="col-form-label min-w-30px px-1 py-1 text-center border border-secondary rounded mr-2">' + data.days[i] + '</label>')
                        })

                        $("#lead_time_id").val(res.lead_time_id);

                        if(isNext){
                            $('#blackout-dates-tab').click();
                        }
                    } else {
                        showAlert(false, res.message)
                    }
                }
            });
        })

        /**
        Blackout Dates
         */
        var blackout_dates = $('#blackout-dates')

        blackout_dates.find("form").validate({
            onkeyup: false,
            onclick: false,
            onfocusout: false,
            rules: {
                "event_name[]": {
                    required: true,
                },
                "start_date[]": {
                    required: true,
                },
                "end_date[]": {
                    required: true,
                },
            },
            errorPlacement: function (error, element) {
               return
            }
        });

        blackout_dates.find('th').click(function(e){
            e.preventDefault()
            e.stopPropagation()
            if($(this).hasClass('sorting') || $(this).hasClass('sorting_desc')){
                blackout_dates.find('th').attr('class', 'sorting');
                $(this).attr('class', 'sorting_asc')
            } else if($(this).hasClass('sorting_asc')){
                $(this).attr('class', 'sorting_desc')
            }
        })

        blackout_dates.find("tbody").on('focusin', 'td', function(e){
            if($(e.target).is("input")){
                $(this).css("border", "1px solid #E1EDFF");
            }
        });

        blackout_dates.find("tbody").on('focusout', 'td', function(){
            $(this).css('border', 'none')
            $(this).css('border-top', '1px solid #eff2f7');
        });

        blackout_dates.on('click', '.add-new-btn', function(e){
            e.preventDefault()
            let table = blackout_dates.find('table tbody');
            table.find('.no-list').remove();
            table.find('.add-row-tr').before('\
                <tr>\
                    <td class="align-middle">\
                        <input type="text" name="events[][name]" placeholder="Event Name">\
                    </td>\
                    <td  class="align-middle">\
                        <div class="input-group date" data-provide="datepicker" data-date-orientation="bottom" data-date-autoclose="true">\
                            <input type="text" name="events[][start_date]" placeholder="MM/DD/YY" class="form-control border-0">\
                            <div class="input-group-addon d-flex align-items-center">\
                                <i class="mdi mdi-calendar"></i>\
                            </div>\
                        </div>\
                    </td>\
                    <td  class="align-middle">\
                        <div class="input-group date" data-provide="datepicker" data-date-orientation="bottom" data-date-autoclose="true">\
                            <input type="text" name="events[][end_date]" placeholder="MM/DD/YY" class="form-control border-0">\
                            <div class="input-group-addon d-flex align-items-center">\
                                <i class="mdi mdi-calendar"></i>\
                            </div>\
                        </div>\
                    </td>\
                    <td  class="align-middle">\
                        <input type="text" name="events[][number_of_days]" class="bg-transparent days" placeholder="Number of Days" value="" disabled>\
                    </td>\
                    <td  class="align-middle">\
                        <select class="border-0" id="type" name="events[][type]">\
                            <option value="1">Production</option>\
                            <option value="2">Office</option>\
                        </select>\
                    </td>\
                    <td  class="align-middle">\
                        <i class="mdi mdi-trash-can-outline font-size-18 text-danger row-hover-action remove_new_data" data-toggle="tooltip" title="Delete"></i>\
                    </td>\
                </tr>\
            ')
            $('.mdi-trash-can-outline').tooltip();
        });

        blackout_dates.on('click','.remove_new_data',function(){
            $(this).closest('tr').remove();
        });

        blackout_dates.on('change', '.date input',  function(e){
            e.preventDefault()
            dates = $(this).parents('tr').find('.date input');
            date1 = new Date($(dates[0]).val())
            date2 = new Date($(dates[1]).val())
            time = Math.abs(date1 - date2)
            days = Math.ceil(time/(3600 * 24 *1000))
            if(days){
                $(this).parents('tr').find('.days').val(days)
            }
        })

        blackout_dates.on('click', ".save", function (e) {
            e.preventDefault();
            if( !blackout_dates.find("form").valid() ) return

            data = blackout_dates.find('form').serializeJSON()
            isNext = $(this).hasClass('next')

            $.ajax({
                type: "POST",
                url: "{{route('vendors.store')}}",
                data: {
                    _token:'{{ csrf_token() }}',
                    insert_type: 'insert_update_backout_date',
                    vendors_id: vendor.id,
                    events: data.events,
                },
                success: function (res) {
                    if (res.success) {
                        showAlert(true, res.message)
                        if(isNext) $('#shipping-tab').click()
                    } else {
                        showAlert(false, res.message)
                    }
                },
                error: function(){
                    showAlert(false, 'Server Error!!!')
                }
            });
        });

        $(document).on('change', '.start_date', function () {
            var startdate = $(this).val();
            var randtext = $(this).data('val');
            if (startdate != '') {
                var dtToday = new Date(startdate);
                var month = dtToday.getMonth() + 1;
                var day = dtToday.getDate() + 1;
                var year = dtToday.getFullYear();
                if (month < 10)
                    month = '0' + month.toString();
                if (day < 10)
                    day = '0' + day.toString();

                var maxDate = year + '-' + month + '-' + day;
                $('.' + randtext + 'last_date').attr('min', maxDate);
                $('.' + randtext + 'last_date').val(maxDate);

                $('.' + randtext + 'last_date').prop('disabled', false);
            }
        });

        $(document).on('change', '.last_date', function () {
            var randtext = $(this).data('val');
            var start = $('.' + randtext + 'start_date').val();
            var end = $(this).val();

            var startDay = new Date(start);
            var endDay = new Date(end);
            var millisecondsPerDay = 1000 * 60 * 60 * 24;

            var millisBetween = endDay.getTime() - startDay.getTime();
            var days = millisBetween / millisecondsPerDay;

            $('.' + randtext + 'number_of_day').val(Math.floor(days));
        });


        /**
            Shipping
        */

        let shipping = $('#shipping')
        let shippingForm = $('#shipping').find('form')
        let addNewShippingCarrierForm = $('#addNewShippingCarrierForm')

        $(".weekly-cutoff").hide()
        $(".monthly-cutoff").hide()

        $("#shipping_delivery_schedule_days").change(function(t) {
            if($('#delivery_schedule').val() == 'weekly' && $(this).val().length){
                $(".weekly-cutoff").show()
                $(".weekly-cutoff>div").hide()
                $.each($(this).val(), function(i,v) {
                    $("#cutoff-" + v).show()
                })
            }else {
                $(".weekly-cutoff").hide()
            }
        });

        $("#delivery_schedule").on('change', function(){
            shipping.find('.date').removeClass('checked')
            if($(this).val() != 'weekly'){
                $('.weekly-cutoff').hide()
            }
            if($(this).val() == 'monthly'){
                $('.monthly-cutoff').show()
            }else{
                $('.monthly-cutoff').hide()
            }
        })

        $('#addNewShippingCarrierForm').validate({
            rules: {
                carrier_name: {required: true}
            }
        })

        $(document).on('click', '#add_shipping_carrier_submit', function (e) {
            e.preventDefault()
            if(!addNewShippingCarrierForm.valid()) return;

            $.ajax({
                type: "POST",
                url: "{{route('vendors.store')}}",
                data: $.extend({
                    _token: '{{ csrf_token() }}',
                    insert_type: 'create_shipping_carrier',
                    marketplace_id: {{session('MARKETPLACE_ID')}},
                }, addNewShippingCarrierForm.serializeJSON()),
                success: function (res) {
                    if (res.success) {
                        showAlert(true, res.message)
                        $('#addNewShippingCarrierModal').modal('toggle');
                        if (res.shipping_carrier_array != '') {
                            $("#shipping_carrier_option").empty();
                            $("#shipping_carrier_option").append('<option>Select</option>');
                            $.each(res.shipping_carrier_array, function (key, value) {
                                $("#shipping_carrier_option").append('<option value="' + value.id + '" ' + (value.id == res.shipping_carrier_id ? 'selected' : '') + '>' + value.carrier_name + '</option>');
                            });
                        }
                    } else {
                        showAlert(false,res.message);
                    }
                }
            });
        });

        $("#add_shipping_agent_form").validate({
            rules: {
                agent_company: {required: true},
                agent_first_name: {required: true},
                agent_last_name: {required: true},
                agent_email: {required: true},
                agent_phone: {required: true},
            },
            messages: {
                agent_company: { required: "enter agent company"},
                agent_first_name: { required: "enter agent first name"},
                agent_last_name: { required: "enter agent last name"},
                agent_email: { required: "enter agent email"},
                agent_phone: { required: "enter agent phone"},
            }
        });

        $(document).on('click', '#add_shipping_agent', function () {
            var company_name = $('#agent_company').val();
            var first_name = $('#agent_first_name').val();
            var last_name = $('#agent_last_name').val();
            var email_address = $('#agent_email').val();
            var phone_number = $('#agent_phone').val();
            var marketplace_id = {{session('MARKETPLACE_ID')}};
            if (company_name == '') {
                $("#agent_company").css("border", "1px solid red");
                $("#agent_company").focus();
            } else if (email_address == '') {
                $("#agent_email").css("border", "1px solid red");
                $("#agent_email").focus();
            } else {
                $.ajax({
                    type: "POST",
                    url: "{{route('suppliers.store')}}",
                    data: {
                        _token: '{{ csrf_token() }}',
                        company_name: company_name,
                        marketplace_id: marketplace_id,
                        first_name: first_name,
                        last_name: last_name,
                        email: email_address,
                        phone: phone_number,
                        insert_type: 'create_shipping_agent'
                    },
                    success: function (res) {
                        if (res.error == 0) {
                            $("#messages").html(res.message);
                            //  $("#supplier_id").val(res.supplier_id);
                            console.log(res.shipping_agent_array);
                            $('#addNewAgentModal').modal('toggle');
                            if (res.shipping_agent_array != '') {
                                $("#shipping_agent").empty();
                                $("#shipping_agent").append('<option>Select</option>');
                                $.each(res.shipping_agent_array, function (key, value) {
                                    $("#shipping_agent").append('<option value="' + value.id + '" ' + (value.id == res.shipping_agent_id ? 'selected' : '') + '>' + value.first_name + ' ' + value.last_name + '</option>');
                                });
                            }
                        } else {
                            $("#messages").html(res.message);
                        }
                    }
                });
            }
        });

        shipping.find('.selectShippingBtn').on('click', function(e){
            e.preventDefault()
            $(this).addClass('btn-primary').addClass('selected').removeClass('btn-dark')
            $(this).siblings().addClass('btn-dark').removeClass('btn-primary').removeClass('selected')
            let cls = $(this).siblings().data('val')
            $(this).parents('tbody').find('tr').css('display', 'table-row')
            $(this).parents('tbody').find('.' + cls).css('display', 'none')
        })

        shipping.on('click', '.add-new-btn', function(e){
            e.preventDefault();
            $(this).before('\
                <div class="d-flex py-1">\
                    <select name="shipping_air[][type]" id="shipping_air_type" class="form-control w-150px">\
                        <option value="Standard">Standard</option>\
                        <option value="Express">Express</option>\
                    </select>\
                    <div class="input-group flex-nowrap w-90px mx-3">\
                        <div class="input-group-prepend">\
                            <span class="input-group-text bg-transparent pr-1">$</span>\
                        </div>\
                        <input type="text" id="shipping_air_price" name="shipping_air[][price]"\
                            class="form-control border-left-0 pl-1"\
                            style="padding-top: 0.57rem;" value="">\
                    </div>\
                    <select name="shipping_air[][messure_in]" id="shipping_air[][messure_in]" class="form-control w-90px">\
                        <option value="KG">KG</option>\
                        <option value="LBS">LBS</option>\
                        <option value="CBM">CBM</option>\
                    </select>\
                </div>\
            ');
        });

        $(weeklyDays).each( (i, v) => {
            shipping.find('.cutoff_day').append(`<option value="${v}">${v}</option>`)
        })

        shipping.find('#delivery_schedule').on('change', function(){
            type = schedule_type[$(this).val()]
            let td = $(this).parents('td')
            td.find('.saved label').eq(0).text(type)
            shipping.find('.schedule-panels>div').addClass('d-none')
            shipping.find('.schedule-panels').find('.' + $(this).val() + '-panel').removeClass('d-none')
            let slt = $(this).next().find('select')
            slt.empty()
            if($(this).val() == 'weekly' || $(this).val() == 'bi_weekly'){
                let weeklyDays = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
                weeklyDays.forEach(function(i){
                    slt.append('<option value="'+i+'">'+i+'</option>')
                })
            }else{
                for(i=1; i<32; i++){
                    slt.append('<option value="'+i+'">'+i+'</option>')
                }
            }

        })

        shipping.on('click', '.save', function (e) {
            e.preventDefault();
            data = shippingForm.serializeJSON();
            // if(!shippingForm.valid()) { return false; }
            if(vendor.type == VENDOR_TYPE_DOMESTIC){
                data.shipping_type = $('.domesticCase .selectShippingBtn.selected').data('val') == 'vendorCase' ? 1 : 2
            }else{
                data.logistics_handler_type = $('.internationalCase .selectShippingBtn.selected').data('val') == 'vendorCase' ? 1 : 2
            }

            let isNext = $(this).hasClass('next')
            $.ajax({
                type: "POST",
                url: "{{route('vendors.store')}}",
                data: $.extend({
                    _token:'{{ csrf_token() }}',
                    insert_type:'insert_update_shipping_details',
                    vendor_id: vendor.id,
                    vendor_type: vendor.type,
                }, data),
                success: function (res) {
                    if (res.success) {
                        showAlert(true, res.message)

                        var option_all = $("#warehouse_selection_ship_carrier option:selected").map(function () {
                            return $(this).text();
                        }).get().join(', ');


                        shipping.find('.inputing').css('display', 'none')
                        shipping.find('.saved').css('display', 'block')

                        $('#shipping_cutoff_delivery_time_sun_schedule_saved').text(data.cutoff_delivery_time['weekly']['sun']['schedule'])
                        $('#shipping_cutoff_delivery_time_mon_schedule_saved').text(data.cutoff_delivery_time['weekly']['mon']['schedule'])
                        $('#shipping_cutoff_delivery_time_tue_schedule_saved').text(data.cutoff_delivery_time['weekly']['tue']['schedule'])
                        $('#shipping_cutoff_delivery_time_wed_schedule_saved').text(data.cutoff_delivery_time['weekly']['wed']['schedule'])
                        $('#shipping_cutoff_delivery_time_thu_schedule_saved').text(data.cutoff_delivery_time['weekly']['thu']['schedule'])
                        $('#shipping_cutoff_delivery_time_fri_schedule_saved').text(data.cutoff_delivery_time['weekly']['fri']['schedule'])
                        $('#shipping_cutoff_delivery_time_sat_schedule_saved').text(data.cutoff_delivery_time['weekly']['sat']['schedule'])
                        $('#shipping_cutoff_delivery_time_sun_time_saved').text(data.cutoff_delivery_time['weekly']['sun']['time'])
                        $('#shipping_cutoff_delivery_time_mon_time_saved').text(data.cutoff_delivery_time['weekly']['mon']['time'])
                        $('#shipping_cutoff_delivery_time_tue_time_saved').text(data.cutoff_delivery_time['weekly']['tue']['time'])
                        $('#shipping_cutoff_delivery_time_wed_time_saved').text(data.cutoff_delivery_time['weekly']['wed']['time'])
                        $('#shipping_cutoff_delivery_time_thu_time_saved').text(data.cutoff_delivery_time['weekly']['thu']['time'])
                        $('#shipping_cutoff_delivery_time_fri_time_saved').text(data.cutoff_delivery_time['weekly']['fri']['time'])
                        $('#shipping_cutoff_delivery_time_sat_time_saved').text(data.cutoff_delivery_time['weekly']['sat']['time'])

                        $('#shipping_cutoff_delivery_time_monthly_schedule_saved').text(data.cutoff_delivery_time['monthly']['days'])
                        $('#shipping_cutoff_delivery_time_monthly_time_saved').text(data.cutoff_delivery_time['monthly']['time'])

                        $('#shipping_delivery_schedule_saved').text( data.delivery_schedule ).siblings('label').remove()
                        $(data.days).each(function(i){
                            $('#shipping_delivery_schedule_saved').after('<label for="" class="col-form-label min-w-30px px-1 py-1 text-center border border-secondary rounded mr-2">' + data.days[i] + '</label>')
                        })

                        $('#ship_to_warehouse_saved label').remove()
                        $('#ship_to_warehouse_saved button').before('<label for="" class="col-form-label">' + option_all + '</label>')
                        // $('#ship_to_warehouse_saved button').before('<label for="" class="col-form-label">' + option_all + '</label>')
                        // $(data.warehouses).each(function(i){
                        //     $('#ship_to_warehouse_saved button').before('<label for="" class="col-form-label">' + data.warehouses[i] + '</label>')
                        // })

                        //$('#shipping_shipping_carrier_saved').text(data.shipping_carrier_option)
                        $('#shipping_shipping_carrier_saved').text($( "#shipping_carrier_option option:selected" ).text())
                        $('#shipping_tracking_no_provided_by_saved').text(data.provided_by)
                        //$('#shipping_agent_saved').text(data.shipping_agent)
                        $('#shipping_agent_saved').text($( "#shipping_agent option:selected" ).text())

                        $('label#logistics_handler_saved').text(data.logistics_handler_type == 2 ? 'Shipping Agent' : 'Vendor')
                        $('label#shipping_shipping_type').text(data.shipping_type == 2 ? 'Shipping Carrier' : 'Vendor')

                        if(data.lcl_cost_for){
                            $('#lcl_cost_for_saved').text('LCL Cost Per')
                            $('#lcl_cost_per_saved').text(data.lcl_cost_per)
                            $('#lcl_cost_saved').text('$' + data.lcl_cost)
                        }else{
                            $('#lcl_cost_for_saved').text('')
                            $('#lcl_cost_per_saved').text('')
                            $('#lcl_cost_saved').text('')
                        }

                        $('#shipping_container_settings_saved .values').remove()

                        if(data.twentyft_checkbox){
                            $('#shipping_container_settings_saved').append(`\
                            <div class="d-flex values">\
                                <div class="w-230px">\
                                    <label for="" class="col-form-label" style="width: 230px;">20ft &nbsp;&nbsp;&nbsp; = &nbsp;&nbsp;&nbsp; ${data.twentyft_cost_per} CBM</label>\
                                </div>\
                                <div class="mx-3">\
                                    <label for="" class="col-form-label">$${data.twentyft_cost}</label>\
                                </div>\
                            </div>\
                            `)
                        }
                        if(data.fourtyft_checkbox){
                            $('#shipping_container_settings_saved').append(`\
                            <div class="d-flex values">\
                                <div class="w-230px">\
                                    <label for="" class="col-form-label" style="width: 230px;">40ft &nbsp;&nbsp;&nbsp; = &nbsp;&nbsp;&nbsp; ${data.fourtyft_cost_per} CBM</label>\
                                </div>\
                                <div class="mx-3">\
                                    <label for="" class="col-form-label">$${data.fourtyft_cost}</label>\
                                </div>\
                            </div>\
                            `)
                        }
                        if(data.fourtyhq_checkbox){
                            $('#shipping_container_settings_saved').append(`\
                            <div class="d-flex values">\
                                <div class="w-230px">\
                                    <label for="" class="col-form-label" style="width: 230px;">40HQ &nbsp;&nbsp;&nbsp; = &nbsp;&nbsp;&nbsp; ${data.fourtyhq_cost_per} CBM</label>\
                                </div>\
                                <div class="mx-3">\
                                    <label for="" class="col-form-label">$${data.fourtyhq_cost}</label>\
                                </div>\
                            </div>\
                            `)
                        }
                        if(data.fourtyhq_checkbox){
                            $('#shipping_container_settings_saved').append(`\
                            <div class="d-flex values">\
                                <div class="w-230px">\
                                    <label for="" class="col-form-label" style="width: 230px;">45HQ &nbsp;&nbsp;&nbsp; = &nbsp;&nbsp;&nbsp; ${data.fourtyfivehq_cost_per} CBM</label>\
                                </div>\
                                <div class="mx-3">\
                                    <label for="" class="col-form-label">$${data.fourtyfivehq_cost}</label>\
                                </div>\
                            </div>\
                            `)
                        }
                        $('#shipping_by_air_saved .values').remove()
                        $(data['shipping_air']).each(function(i){
                            $('#shipping_by_air_saved button').before(`<div class="values"><label for="" class="col-form-label">${data.shipping_air[i]['type']} &nbsp;&nbsp; $${data.shipping_air[i]['price']} &nbsp;&nbsp; ${data.shipping_air[i]['messure_in']}</label></div>`)
                        })
                        $('#shipping_duties_cost').text(`${data.duties_cost_percentage}%`)

                        if(isNext) $('#payment-terms-tab').click()
                    } else {
                        showAlert(false, res.message)
                    }
                },
                error: function(res){
                    showAlert(false, 'Server Error')
                }
            });
        });


        /**
            Payment Terms
         */

        var payment_terms = $('#payment-terms')

        payment_terms.on('click','.save',function(e){
            e.preventDefault()
            let isNext = $(this).hasClass('next')
            let data = payment_terms.find('form').serializeJSON()

            $.ajax({
                type: "POST",
                url: "{{route('vendors.store')}}",
                data: $.extend({
                    _token: '{{ csrf_token() }}',
                    insert_type:'insert_update_payment',
                    vendors_supplier_id:vendor.id,
                }, data),
                dataType: "json",
                success: function (data) {
                    if(!isNext){
                        showAlert(data.success, data.message)
                    }else{
                        window.location.href = "{{route('vendors.index')}}";
                    }
                }
            });
        });
    })
    $(document).on('click','.remove_new_data',function(){
            $(this).closest('tr').remove();
        });
        $(document).on('click', '.remove_button', function () {
            //e.preventDefault();
            var insert_type = 'remove_vendors_blackout_date';
            var count_all = $(".total_record").text();
            var id = $(this).data('id');
            if (id != '') {

                swal({
                        title: "Are you sure?",
                        text: "You want to delete this record",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Delete it!",
                        cancelButtonText: "No, cancel please!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function (isConfirm) {
                        if (isConfirm) {

                            $.ajax({
                                type: "POST",
                                url: "{{route('vendors.store')}}",
                                data: {_token: '{{ csrf_token() }}',id:id,insert_type:insert_type},
                                dataType: "html",
                                success: function (data) {
                                    $("#" + id).remove();
                                    swal({
                                        title: "Done!",
                                        text: "It was succesfully deleted!",
                                        type: "success",
                                        timer: 700
                                    });
                                }

                            });
                        }
                        else {
                            swal("Cancelled", "", "error");
                        }
                    });
            } else {
                alert('No found record!');
            }
        });
</script>
@endsection

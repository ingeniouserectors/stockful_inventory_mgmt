@extends('layouts.master-final')

@section('title') Vendors Import Messages @endsection

@section('customcss')
    <style>
        table td{
            vertical-align: middle !important;
        }
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0 font-size-18">Vendors</h4>
           	<a href="{{ url('vendors') }}" class="btn btn-primary waves-effect waves-light">
                    <!-- <i class="bx bx-plus-circle align-middle mr-2"></i> -->Back To Vendor
                </a>
        </div>
    </div>
</div>
@if($total_count != '')
	<div class="alert alert-danger">
		Total {{$total_count != '' ? $total_count : 0 }} rows uploaded {{$success_count != '' ? $success_count : 0}} successfully uploaded and {{$error_count != '' ? $error_count : 0}} failed to upload
	</div>
@endif	
@if(!empty($error_msg))
	<table id="list" class="table dataTable table-striped dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
        <thead class="" style="background-color: #49505710;">
	        <tr>
	            <td>S/N</td>
	            <td>Vendor</td>
	            <td>Error</td>
	        </tr>
        </thead>
       	<tbody>
       		<?php
       		for($i=0;$i<count($error_msg);$i++){
       			//dump($error_msg[$i]);
       		?>
       			<tr>
       				<td>{{$i+1}}</td>
       				<td>{{($error_msg[$i]['vendor_name'] ?? '')}}</td>
       				<td>{{$error_msg[$i]['message']}}</td>
       			</tr>
       		</tbody>
       		<?php
       		}
       		?>
       	</tbody>
       
    </table>
<!-- @else
	@if($success_msg)
	<div class="alert alert-success">
		{{($success_msg ?? '')}}
	</div>
	@else
	<p>No Data Found</p>
	@endif -->
@endif
@endsection
<?php
\Session::forget('error_msg_array');
        \Session::forget('success_message');
?>
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMwsAdjustedInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mws_adjusted_inventories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_marketplace_id')->nullable();
            $table->integer('product_id')->nullable();
            $table->string('sku')->nullable();
            $table->integer('quantity')->nullable();
            $table->dateTime('date')->nullable();
            $table->integer('total_sales')->nullable();
            $table->integer('total_return')->nullable();
            $table->integer('adjusted_inventory')->nullable();
            $table->float('average_sales')->nullable();
            $table->float('mod_sales')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mws_adjusted_inventories');
    }
}

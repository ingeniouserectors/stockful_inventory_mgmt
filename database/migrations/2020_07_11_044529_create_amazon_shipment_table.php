<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmazonShipmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amazon_shipment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('shipment_id')->nullable();
            $table->integer('product_id')->nullable();
            $table->string('destination_name')->nullable();
            $table->string('api_shipment_id')->nullable();
            $table->string('seller_sku')->nullable();
            $table->string('fulfillment_network_sku')->nullable();
            $table->integer('qty')->nullable();
            $table->string('ship_to_address_name')->nullable();
            $table->string('ship_to_address_line1')->nullable();
            $table->string('ship_to_address_line2')->nullable();
            $table->string('ship_to_city')->nullable();
            $table->string('ship_to_state_code')->nullable();
            $table->string('ship_to_country_code')->nullable();
            $table->string('ship_to_postal_code')->nullable();
            $table->string('label_prep_type')->nullable();
            $table->integer('total_units')->nullable();
            $table->string('fee_per_unit_currency_code')->nullable();
            $table->double('fee_per_unit_value')->nullable();
            $table->double('total_fee_value')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amazon_shipment');
    }
}

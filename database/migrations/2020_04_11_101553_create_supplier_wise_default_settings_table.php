<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierWiseDefaultSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_wise_default_settings', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->integer('supplier_id');
        $table->integer('lead_time');
        $table->integer('order_volume');
        $table->double('quantity_discount');
        $table->integer('moq');
        $table->double('CBM_Per_Container');
        $table->integer('Production_Time');
        $table->integer('Boat_To_Port');
        $table->integer('Port_To_Warehouse');
        $table->integer('Warehouse_Receipt');
        $table->integer('Ship_To_Specific_Warehouse');
        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_wise_default_settings');
    }
}

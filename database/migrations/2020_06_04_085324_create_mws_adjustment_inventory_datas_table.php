<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMwsAdjustmentInventoryDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mws_adjustment_inventory_datas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_marketplace_id');
            $table->integer('product_id');
            $table->dateTime('adjusted_date');
            $table->string('transaction_item_id');
            $table->string('fnsku');
            $table->string('sku');
            $table->text('product_name');
            $table->string('fulfillment_center_id');
            $table->integer('quantity');
            $table->text('reason');
            $table->text('disposition');
            $table->integer('reconciled');
            $table->integer('unreconciled');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mws_adjustment_inventory_datas');
    }
}

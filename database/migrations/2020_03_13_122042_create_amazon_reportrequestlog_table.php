<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmazonReportrequestlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amazon_reportrequestlog', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_marketplace_id')->nullable();
            $table->string('request_id')->nullable();
            $table->string('report_id')->nullable();
            $table->string('status')->nullable();
            $table->string('report_type')->nullable();
            $table->string('request_response')->nullable();
            $table->tinyInteger('processed')->default(0)->nullable();
            $table->tinyInteger('amz_dateitetration_id')->default(0)->nullable();
            $table->dateTime('available_at')->nullable();
            $table->dateTime('last_checked')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amazon_reportrequestlog');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('purchase_orders_id')->nullable();
            $table->integer('product_id')->nullable();
            $table->integer('supplier')->nullable();
            $table->integer('vendor')->nullable();
            $table->integer('warehouse')->nullable();
            $table->integer('projected_units')->nullable();
            $table->double('items_per_cartoons')->nullable();
            $table->double('cbm')->nullable();
            $table->integer('containers')->nullable();
            $table->double('subtotal')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_order_details');
    }
}

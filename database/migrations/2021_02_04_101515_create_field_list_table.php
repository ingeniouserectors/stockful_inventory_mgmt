<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFieldListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('field_list', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('module_id')->nullable();
            $table->string('original_name')->nullable();
            $table->string('display_name')->nullable();
            $table->string('field_type')->nullable();
            $table->string('referance_table')->nullable();
            $table->string('referance_table_field')->nullable();
            $table->string('table_name')->nullable();
            $table->string('display_field')->nullable();
            $table->string('join_with')->nullable();
            $table->enum('join_type',['0','1','2'])->default('0')->comment('0-no join,1-join this table fields to other table,2-join other table fields to this table')->nullable();
            $table->enum('active',['0','1'])->default('1')->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('field_list');
    }
}

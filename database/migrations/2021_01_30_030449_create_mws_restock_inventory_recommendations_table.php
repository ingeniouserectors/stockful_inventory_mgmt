<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMwsRestockInventoryRecommendationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mws_restock_inventory_recommendations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_marketplace_id');
            $table->integer('product_id');
            $table->string('country')->nullable();
            $table->string('product_name')->nullable();
            $table->string('fnsku')->nullable();
            $table->string('sku')->nullable();
            $table->string('asin')->nullable();
            $table->string('condition')->nullable();
            $table->string('supplier')->nullable();
            $table->string('supplier_part_no')->nullable();
            $table->string('currency_code')->nullable();
            $table->decimal('price')->nullable();
            $table->decimal('sales_last_30_days')->nullable();
            $table->integer('units_sold_last_30_days')->nullable();
            $table->integer('total_units')->nullable();
            $table->integer('inbound_inventory')->nullable();
            $table->integer('available_inventory')->nullable();
            $table->integer('fc_transfer')->nullable();
            $table->integer('fc_processing')->nullable();
            $table->integer('customer_order')->nullable();
            $table->integer('unfulfillable')->nullable();
            $table->string('fulfilled_by')->nullable();
            $table->integer('days_of_supply')->nullable();
            $table->string('alert')->nullable();
            $table->integer('recommended_replenishment_quantity')->nullable();
            $table->date('recommended_ship_date')->nullable();
            $table->string('inventory_level_threshold_published_current_month')->nullable();
            $table->string('current_month_very_low_inventory_threshold')->nullable();
            $table->string('current_month_minimum_inventory_threshold')->nullable();
            $table->string('current_month_maximum_inventory_threshold')->nullable();
            $table->string('current_month_very_high_inventory_threshold')->nullable();
            $table->string('inventory_level_threshold_published_next_month')->nullable();
            $table->string('next_month_very_low_inventory_threshold')->nullable();
            $table->string('next_month_minimum_inventory_threshold')->nullable();
            $table->string('next_month_maximum_inventory_threshold')->nullable();
            $table->string('next_month_very_high_inventory_threshold')->nullable();
            $table->integer('utilization')->nullable();
            $table->integer('maximum_shipment_quantity')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mws_restock_inventory_recommendations');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMwsDeveloperAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mws_developer_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('developer_accno')->nullable();
            $table->string('marketplace_id')->nullable();
            $table->string('access_key')->nullable();
            $table->string('secret_key')->nullable();
            $table->string('marketplace_url')->nullable();
            $table->string('marketplace')->nullable();
            $table->tinyInteger('marketplace_status')->default(0);
            $table->string('marketplace_country')->nullable();
            $table->timestamps();
        });
        DB::table('mws_developer_accounts')->insert([
            ['developer_accno'=> '','marketplace_id' => 'A2Q3Y263D00KWC','access_key'=> base64_encode(''),'secret_key'=>base64_encode(''),'marketplace_url' => 'https://mws.amazonservices.com','marketplace' => 'BR','marketplace_status' => '0'],
            ['developer_accno'=> '','marketplace_id' => 'A2EUQ1WTGCTBG2','access_key'=> base64_encode(''),'secret_key'=>base64_encode(''),'marketplace_url' => 'https://mws.amazonservices.ca','marketplace' => 'CA','marketplace_status' => '0'],
            ['developer_accno'=> '','marketplace_id' => 'A1AM78C64UM0Y8','access_key'=> base64_encode(''),'secret_key'=>base64_encode(''),'marketplace_url' => 'https://mws.amazonservices.com.mx','marketplace' => 'MX','marketplace_status' => '0'],
            ['developer_accno'=> '','marketplace_id' => 'ATVPDKIKX0DER','access_key'=> base64_encode(''),'secret_key'=>base64_encode(''),'marketplace_url' => 'https://mws.amazonservices.com','marketplace' => 'US','marketplace_status' => '0'],
            ['developer_accno'=> '','marketplace_id' => 'A2VIGQ35RCS4UG','access_key'=> base64_encode(''),'secret_key'=>base64_encode(''),'marketplace_url' => 'https://mws.amazonservices.ae','marketplace' => 'AE','marketplace_status' => '0'],
            ['developer_accno'=> '','marketplace_id' => 'A1PA6795UKMFR9','access_key'=> base64_encode(''),'secret_key'=>base64_encode(''),'marketplace_url' => 'https://mws-eu.amazonservices.com','marketplace' => 'DE','marketplace_status' => '0'],
            ['developer_accno'=> '','marketplace_id' => 'ARBP9OOSHTCHU','access_key'=> base64_encode(''),'secret_key'=>base64_encode(''),'marketplace_url' => 'https://mws-eu.amazonservices.com','marketplace' => 'EG','marketplace_status' => '0'],
            ['developer_accno'=> '','marketplace_id' => 'A1RKKUPIHCS9HS','access_key'=> base64_encode(''),'secret_key'=>base64_encode(''),'marketplace_url' => 'https://mws-eu.amazonservices.com','marketplace' => 'ES','marketplace_status' => '0'],
            ['developer_accno'=> '','marketplace_id' => 'A13V1IB3VIYZZH','access_key'=> base64_encode(''),'secret_key'=>base64_encode(''),'marketplace_url' => 'https://mws-eu.amazonservices.com','marketplace' => 'FR','marketplace_status' => '0'],
            ['developer_accno'=> '','marketplace_id' => 'A1F83G8C2ARO7P','access_key'=> base64_encode(''),'secret_key'=>base64_encode(''),'marketplace_url' => 'https://mws-eu.amazonservices.com','marketplace' => 'GB','marketplace_status' => '0'],
            ['developer_accno'=> '','marketplace_id' => 'A21TJRUUN4KGV','access_key'=> base64_encode(''),'secret_key'=>base64_encode(''),'marketplace_url' => 'https://mws.amazonservices.in','marketplace' => 'IN','marketplace_status' => '0'],
            ['developer_accno'=> '','marketplace_id' => 'APJ6JRA9NG5V4','access_key'=> base64_encode(''),'secret_key'=>base64_encode(''),'marketplace_url' => 'https://mws-eu.amazonservices.com','marketplace' => 'IT','marketplace_status' => '0'],
            ['developer_accno'=> '','marketplace_id' => 'A1805IZSGTT6HS','access_key'=> base64_encode(''),'secret_key'=>base64_encode(''),'marketplace_url' => 'https://mws-eu.amazonservices.com','marketplace' => 'NL','marketplace_status' => '0'],
            ['developer_accno'=> '','marketplace_id' => 'A17E79C6D8DWNP','access_key'=> base64_encode(''),'secret_key'=>base64_encode(''),'marketplace_url' => 'https://mws-eu.amazonservices.com','marketplace' => 'SA','marketplace_status' => '0'],
            ['developer_accno'=> '','marketplace_id' => 'A33AVAJ2PDY3EV','access_key'=> base64_encode(''),'secret_key'=>base64_encode(''),'marketplace_url' => 'https://mws-eu.amazonservices.com','marketplace' => 'TR','marketplace_status' => '0'],
            ['developer_accno'=> '','marketplace_id' => 'A19VAU5U5O7RUS','access_key'=> base64_encode(''),'secret_key'=>base64_encode(''),'marketplace_url' => 'https://mws-fe.amazonservices.com','marketplace' => 'SG','marketplace_status' => '0'],
            ['developer_accno'=> '','marketplace_id' => 'A39IBJ37TRP1C6','access_key'=> base64_encode(''),'secret_key'=>base64_encode(''),'marketplace_url' => 'https://mws.amazonservices.com.au 	','marketplace' => 'AU','marketplace_status' => '0'],
            ['developer_accno'=> '','marketplace_id' => 'A1VC38T7YXB528','access_key'=> base64_encode(''),'secret_key'=>base64_encode(''),'marketplace_url' => 'https://mws.amazonservices.jp','marketplace' => 'JP','marketplace_status' => '0'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mws_developer_accounts');
    }
}

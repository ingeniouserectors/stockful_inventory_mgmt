<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMwsProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mws_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_marketplace_id');
            $table->string('sku')->nullable();
            $table->string('fnsku')->nullable();
            $table->string('asin')->nullable();
            $table->string('upc')->nullable();
            $table->text('prod_name')->nullable();
            $table->text('prod_image')->nullable();
            $table->string('prod_group')->nullable();
            $table->string('brand')->nullable();
            $table->string('fulfilledby')->nullable();
            $table->string('haslocalinventory')->nullable();
            $table->string('currency')->nullable();
            $table->decimal('your_price','10','2')->default('0');
            $table->decimal('sales_price','10','2')->default('0');
            $table->decimal('longestside','10','2')->default('0');
            $table->decimal('medianside','10','2')->default('0');
            $table->decimal('shortestside','10','2')->default('0');
            $table->decimal('lengthandgridth','10','2')->default('0');
            $table->string('unitofdimensions')->nullable();
            $table->decimal('itempackweight','10','2')->default('0');
            $table->string('unitofweight')->nullable();
            $table->string('productsizeweightband')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mws_products');
    }
}

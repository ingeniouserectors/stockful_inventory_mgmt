<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierDefaultSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_default_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('lead_time')->nullable();
            $table->integer('order_volume')->nullable();
            $table->double('quantity_discount')->nullable();
            $table->integer('moq')->nullable();
            $table->double('CBM_Per_Container')->nullable();
            $table->integer('Production_Time')->nullable();
            $table->integer('Boat_To_Port')->nullable();
            $table->integer('Port_To_Warehouse')->nullable();
            $table->integer('Warehouse_Receipt')->nullable();
            $table->integer('Ship_To_Specific_Warehouse')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_default_settings');
    }
}

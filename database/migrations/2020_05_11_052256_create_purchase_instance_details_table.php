<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseInstanceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_instance_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('purchesinstances_id')->nullable();
            $table->integer('product_id')->nullable();
            $table->integer('supplier')->nullable();
            $table->integer('vendor')->nullable();
            $table->integer('warehouse')->nullable();
            $table->integer('projected_units')->nullable();
            $table->integer('items_per_cartoons')->nullable();
            $table->integer('containers')->nullable();
            $table->double('cbm')->nullable();
            $table->double('subtotal')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_instance_details');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTotalTrendRateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_total_trend_rate', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_marketplace_id')->nullable();
            $table->integer('prod_id')->nullable();
            $table->string('month_year')->nullable();
            $table->decimal('sales_total')->default(0.00)->nullable();
            $table->decimal('day_rate')->nullable();
            $table->decimal('trend_percentage')->default(0.00)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_total_trend_rate');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMwsOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mws_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_marketplace_id');
            $table->integer('product_id');
            $table->string('amazon_order_id')->nullable();
            $table->string('merchant_order_id')->nullable();
            $table->string('purchase_date')->nullable();
            $table->string('last_updated_date')->nullable();
            $table->string('order_status')->nullable();
            $table->string('fulfillment_channel')->nullable();
            $table->string('sales_channel')->nullable();
            $table->string('order_channel')->nullable();
            $table->string('ship_service_level')->nullable();
            $table->string('product_name')->nullable();
            $table->string('sku')->nullable();
            $table->string('asin')->nullable();
            $table->string('item_status')->nullable();
            $table->integer('quantity')->nullable();
            $table->string('currency')->nullable();
            $table->decimal('item_price',8,2)->nullable();
            $table->decimal('item_tax',8,2)->nullable();
            $table->decimal('shipping_price',8,2)->nullable();
            $table->decimal('shipping_tax',8,2)->nullable();
            $table->decimal('gift_wrap_price',8,2)->nullable();
            $table->decimal('gift_wrap_tax',8,2)->nullable();
            $table->decimal('item_promotion_discount',8,2)->nullable();
            $table->decimal('ship_promotion_discount',8,2)->nullable();
            $table->string('ship_city')->nullable();
            $table->string('ship_state')->nullable();
            $table->string('ship_postal_code')->nullable();
            $table->string('ship_country')->nullable();
            $table->string('promotion_ids')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mws_orders');
    }
}

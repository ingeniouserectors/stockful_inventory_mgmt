<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('vendor_id');
            $table->integer('product_id');
            $table->integer('lead_time')->nullable();
            $table->integer('order_volume')->nullable();
            $table->double('quantity_discount')->nullable();
            $table->integer('moq')->nullable();
            $table->integer('shipping')->nullable();
            $table->integer('ship_to_warehouse')->nullable();
            $table->double('CBM_Per_Container')->nullable();
            $table->decimal('Cost',8,2)->nullable();
            $table->decimal('Cost_Expenses',8,2)->nullable();
            $table->double('Box_Domensions_Height')->nullable();
            $table->double('Box_Domensions_Width')->nullable();
            $table->double('Box_Domensions_Weight')->nullable();
            $table->double('Qty_per_Carton')->nullable();
            $table->double('Cartons_per_pallet')->nullable();
            $table->enum('Inventory_level',['1','2'])->comment('1-Standard,2-Seasonal')->default(1)->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_products');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMwsOrderReturnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mws_order_returns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_marketplace_id');
            $table->integer('product_id');
            $table->string('order_id')->nullable();
            $table->datetime('return_date')->nullable();
            $table->string('sku')->nullable();
            $table->string('asin')->nullable();
            $table->string('fnsku')->nullable();
            $table->text('product_name')->nullable();
            $table->integer('quantity')->nullable();
            $table->string('fulfillment_center_id')->nullable();
            $table->string('detailed_disposition')->nullable();
            $table->string('reason')->nullable();
            $table->string('status')->nullable();
            $table->string('license_plate_number')->nullable();
            $table->text('customer_comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mws_order_returns');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInboundShipmentItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inbound_shipment_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('inbound_shipment_id')->nullable();
            $table->integer('product_id')->nullable();
            $table->integer('QuantityShipped')->nullable();
            $table->integer('QuantityReceived')->nullable();
            $table->integer('QuantityInCase')->nullable();
            $table->integer('PrepDetailsList')->nullable();
            $table->date('ReleaseDate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inbound_shipment_items');
    }
}

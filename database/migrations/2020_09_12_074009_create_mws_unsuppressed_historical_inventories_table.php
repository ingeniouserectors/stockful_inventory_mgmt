<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMwsUnsuppressedHistoricalInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mws_unsuppressed_historical_inventories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_marketplace_id');
            $table->integer('product_id');
            $table->string('sku');
            $table->string('fnsku');
            $table->string('asin');
            $table->string('condition');
            $table->decimal('yourprice');
            $table->string('mfn_listing_exists');
            $table->integer('mfn_fulfilment_qty');
            $table->string('afn_listing_exists');
            $table->integer('afn_warehouse_qty');
            $table->integer('afn_fulfillable_qty');
            $table->integer('afn_unsellable_qty');
            $table->integer('afn_reserved_qty');
            $table->integer('afn_total_qty');
            $table->decimal('per_unit_volume');
            $table->integer('afn_inbound_working_qty');
            $table->integer('afn_inbound_shipment_qty');
            $table->integer('afn_inbound_receving_qty');
            $table->integer('afn_research_qty');
            $table->integer('afn_reserved_future_supply');
            $table->integer('afn_future_supply_buyable');
            $table->dateTime('inventory_for_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mws_unsuppressed_historical_inventories');
    }
}

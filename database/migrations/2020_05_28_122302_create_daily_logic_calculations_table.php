<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDailyLogicCalculationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_logic_calculations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_marketplace_id')->nullable();
            $table->integer('prod_id')->nullable();
            $table->integer('total_inventory')->nullable();
            $table->integer('lead_time_amazon')->nullable();
            $table->integer('order_frequency')->nullable();
            $table->decimal('day_rate')->nullable();
            $table->decimal('days_supply')->default(0.00)->nullable();
            $table->integer('blackout_days')->default(0.00)->nullable();
            $table->dateTime('projected_reorder_date')->nullable();
            $table->integer('projected_reorder_qty')->nullable();
            $table->integer('days_left_to_order')->nullable();
            $table->string('projected_order_date_range')->nullable();
            $table->integer('sevendays_sales')->nullable();
            $table->decimal('sevenday_day_rate')->default(0.00)->nullable();
            $table->integer('fourteendays_sales')->nullable();
            $table->decimal('fourteendays_day_rate')->default(0.00)->nullable();
            $table->integer('thirtydays_sales')->nullable();
            $table->decimal('thirtyday_day_rate')->default(0.00)->nullable();
            $table->integer('sevendays_previous')->nullable();
            $table->decimal('sevendays_previous_day_rate')->default(0.00)->nullable();
            $table->integer('thirtydays_previous')->nullable();
            $table->decimal('thirtydays_previous_day_rate')->default(0.00)->nullable();
            $table->integer('thirtydays_last_year')->nullable();
            $table->decimal('thirtydays_last_year_day_rate')->default(0.00)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_logic_calculations');
    }
}

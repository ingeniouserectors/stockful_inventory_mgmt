<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMwsExcessInventoryData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mws_excess_inventory_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_marketplace_id');
            $table->integer('product_id');
            $table->date('request_date');
            $table->string('msku');
            $table->string('fnsku');
            $table->string('asin');
            $table->string('product_name');
            $table->string('condition')->nullable();
            $table->string('fulfilled_by')->nullable();
            $table->integer('total_qty_sellable');
            $table->integer('estimated_excess');
            $table->integer('days_of_supply');
            $table->integer('excess_threshold');
            $table->integer('units_sold_last_7_days');
            $table->integer('units_sold_last_30_days');
            $table->integer('units_sold_last_60_days');
            $table->integer('units_sold_last_90_days');
            $table->string('alert');
            $table->string('your_price');
            $table->string('lowest_price_including_shipping');
            $table->string('buybox_price');
            $table->string('recommended_action');
            $table->string('healty_inventory_level');
            $table->string('recommended_sales_price');
            $table->string('recommended_sales_duration_days');
            $table->string('estimated_total_storage_cost');
            $table->string('recommended_removal_qty');
            $table->string('estimated_cost_saving_of_removal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mws_excess_inventory_data');
    }
}

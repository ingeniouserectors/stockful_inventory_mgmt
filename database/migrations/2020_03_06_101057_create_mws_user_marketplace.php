<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMwsUserMarketplace extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mws_user_marketplace', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->nullable();
            $table->string('api_token')->nullable();
            $table->string('mws_sellerid')->nullable();
            $table->string('mws_marketplaceid')->nullable();
            $table->string('mws_authtoken')->nullable();
            $table->string('country')->nullable();
            $table->string('mws_report_url')->nullable();
            $table->string('amazonstorename')->nullable();
            $table->tinyInteger('apiactive')->default(0)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mws_user_marketplace');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDefaultLogicSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('default_logic_setting', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_marketplace_id')->nullable();
            $table->string('logic_label_name')->nullable();
            $table->decimal('supply_last_year_sales_percentage')->default(0.00);
            $table->decimal('supply_recent_last_7_day_sales_percentage')->default(0.00);
            $table->decimal('supply_recent_last_14_day_sales_percentage')->default(0.00);
            $table->decimal('supply_recent_last_30_day_sales_percentage')->default(0.00);
            $table->decimal('supply_trends_year_over_year_historical_percentage')->default(0.00);
            $table->decimal('supply_trends_year_over_year_current_percentage')->default(0.00);
            $table->decimal('supply_trends_multi_month_trend_percentage')->default(0.00);
            $table->decimal('supply_trends_30_over_30_days_percentage')->default(0.00);
            $table->decimal('supply_trends_multi_week_trend_percentage')->default(0.00);
            $table->decimal('supply_trends_7_over_7_days_percentage')->default(0.00);
            $table->decimal('reorder_last_year_sales_percentage')->default(0.00);
            $table->decimal('reorder_recent_last_7_day_sales_percentage')->default(0.00);
            $table->decimal('reorder_recent_last_14_day_sales_percentage')->default(0.00);
            $table->decimal('reorder_recent_last_30_day_sales_percentage')->default(0.00);
            $table->decimal('reorder_trends_year_over_year_historical_percentage')->default(0.00);
            $table->decimal('reorder_trends_year_over_year_current_percentage')->default(0.00);
            $table->decimal('reorder_trends_multi_month_trend_percentage')->default(0.00);
            $table->decimal('reorder_trends_30_over_30_days_percentage')->default(0.00);
            $table->decimal('reorder_trends_multi_week_trend_percentage')->default(0.00);
            $table->decimal('reorder_trends_7_over_7_days_percentage')->default(0.00);
            $table->decimal('reorder_safety_stock_percentage')->default(0.00);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('default_logic_setting');
    }
}

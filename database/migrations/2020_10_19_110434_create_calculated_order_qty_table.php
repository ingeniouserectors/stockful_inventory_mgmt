<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalculatedOrderQtyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calculated_order_qty', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_marketplace_id');
            $table->integer('product_id');
            $table->date('order_date')->nullable();
            $table->integer('total_qty_pending')->nullable();
            $table->integer('total_qty_shipping')->nullable();
            $table->integer('total_qty_shipped')->nullable();
            $table->integer('total_qty_shipping_shipped')->nullable();
            $table->integer('total_qty_pending')->nullable();
            $table->integer('total_qty')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calculated_order_qty');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMwsImportOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mws_import_order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_marketplace_id')->nullable();
            $table->string('product_id')->nullable();
            $table->string('product_name')->nullable();
            $table->string('sku')->nullable();
            $table->string('asin')->nullable();
            $table->string('amazon_order_id')->nullable();
            $table->string('purchase_date')->nullable();
            $table->integer('quantity')->nullable();
            $table->string('currency')->nullable();
            $table->decimal('item_price')->nullable();
            $table->decimal('item_tax')->nullable();
            $table->decimal('shipping_price')->nullable();
            $table->decimal('shipping_tax')->nullable();
            $table->string('ship_city')->nullable();
            $table->string('ship_state')->nullable();
            $table->string('ship_country')->nullable();
            $table->string('ship_postal_code')->nullable();
            $table->string('order_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mws_import_order');
    }
}

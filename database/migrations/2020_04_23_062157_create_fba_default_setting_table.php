<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFbaDefaultSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fba_default_setting', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('supply_days')->default(0)->nullable();
            $table->integer('ship_boxes')->default(0)->nullable();
            $table->integer('ship_pallets')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fba_default_setting');
    }
}

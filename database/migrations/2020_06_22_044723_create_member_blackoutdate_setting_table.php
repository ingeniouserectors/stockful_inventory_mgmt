<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberBlackoutdateSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_blackoutdate_setting', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_marketplace_id')->nullable();
            $table->integer('country_id')->nullable();
            $table->date('blackout_date');
            $table->string('reason')->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_blackoutdate_setting');
    }
}

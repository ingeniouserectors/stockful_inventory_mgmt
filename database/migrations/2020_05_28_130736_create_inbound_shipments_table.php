<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInboundShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inbound_shipments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_marketplace_id')->nullable();
            $table->string('ShipmentId')->nullable();
            $table->string('ShipmentName')->nullable();
            $table->text('ShipFromAddress')->nullable();
            $table->text('DestinationFulfillmentCenterId')->nullable();
            $table->string('LabelPrepType')->nullable();
            $table->string('ShipmentStatus')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inbound_shipments');
    }
}

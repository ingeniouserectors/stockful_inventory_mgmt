<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('supplier_id');
            $table->integer('product_id');
            $table->integer('lead_time');
            $table->integer('order_volume');
            $table->double('quantity_discount');
            $table->integer('moq');
            $table->double('CBM_Per_Container');
            $table->integer('Production_Time');
            $table->integer('Boat_To_Port');
            $table->integer('Port_To_Warehouse');
            $table->integer('Warehouse_Receipt');
            $table->integer('Ship_To_Specific_Warehouse');
            $table->decimal('Cost',8,2)->nullable();
            $table->decimal('Cost_Expenses',8,2)->nullable();
            $table->double('Box_Domensions_Height')->nullable();
            $table->double('Box_Domensions_Width')->nullable();
            $table->double('Box_Domensions_Weight')->nullable();
            $table->double('Qty_per_Carton')->nullable();
            $table->double('Cartons_per_pallet')->nullable();
            $table->enum('Inventory_level',['1','2'])->comment('1-Standard,2-Seasonal')->default(1)->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_products');
    }
}

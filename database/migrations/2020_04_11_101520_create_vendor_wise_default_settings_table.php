<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorWiseDefaultSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_wise_default_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('vendor_id');
            $table->integer('lead_time')->nullable();
            $table->integer('order_volume')->nullable();
            $table->double('quantity_discount')->nullable();
            $table->integer('moq')->nullable();
            $table->integer('shipping')->nullable();
            $table->integer('ship_to_warehouse')->nullable();
            $table->double('cbm_per_container')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_wise_default_settings');
    }
}

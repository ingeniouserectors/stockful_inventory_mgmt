<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('registration_setting')->default(0);
            $table->tinyInteger('email_settings')->default(0);
            $table->tinyInteger('forget_password_settings')->default(0);
            $table->tinyInteger('mws_settings')->default(0);
            $table->tinyInteger('maintenance_mode')->default(0);
            $table->tinyInteger('affiliate_settings')->default(0);
            $table->tinyInteger('support_ticket_settings')->default(0);
            $table->tinyInteger('historical_fetch_number_months')->default(18);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}

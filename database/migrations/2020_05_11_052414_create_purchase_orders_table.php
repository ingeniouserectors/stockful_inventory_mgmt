<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('referance_id')->nullable();
            $table->integer('user_marketplace_id')->nullable();
            $table->integer('purchesinstances_id')->nullable();
            $table->integer('track_id')->nullable();
            $table->date('date_of_ship')->nullable();
            $table->date('date_arrived')->nullable();
            $table->date('reorder_date')->nullable();
            $table->double('day_rate')->nullable();
            $table->double('days_of_supply')->nullable();
            $table->double('blackout_days')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}

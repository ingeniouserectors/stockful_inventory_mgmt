<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmzdateiterationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amzdateiteration', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_marketplace_id');
            $table->dateTime('startDate');
            $table->dateTime('endDate');
            $table->enum('OrderStatus',['0','1']);
            $table->enum('InventoryStatus',['0','1']);
            $table->enum('ReimburseStatus',['0','1']);
            $table->enum('ReturnStatus',['0','1']);
            $table->enum('FinanceStatus',['0','1']);
            $table->enum('FeedbackStatus',['0','1']);
            $table->enum('CustomersalesStatus',['0','1']);
            $table->enum('InventoryshipmentStatus',['0','1']);
            $table->enum('ShipmentOrderRemovalStatus',['0','1']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amzdateiteration');
    }
}

<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(ApplicationModuleTableSeeder::class);
        $this->call(RoleModuleTableSeeder::class);
        $this->call(UserModuleTableSeeder::class);
        $this->call(UserdetailsModuleTableSeeder::class);
        $this->call(User_assigned_roleModuleTableSeeder::class);
        $this->call(Role_accessModuleTableSeeder::class);
        $this->call( User_accessModuleTableSeeder::class);
    }
}

class ApplicationModuleTableSeeder extends Seeder
{

    public function run()
    {
        \App\Models\Application_modules::create(['module' => 'user_module']);
        \App\Models\Application_modules::create(['module' => 'role_module']);
        \App\Models\Application_modules::create(['module' => 'mws_module']);
        \App\Models\Application_modules::create(['module' => 'payment_gateway_module']);
        \App\Models\Application_modules::create(['module' => 'subscription_module']);
        \App\Models\Application_modules::create(['module' => 'affiliate_module']);
        \App\Models\Application_modules::create(['module' => 'support_ticket_module']);
    }
}

class RoleModuleTableSeeder extends Seeder
{
    public function run()
    {
        \App\Models\Userrole::create(['role' => 'Super Admin']);
        \App\Models\Userrole::create(['role' => 'Admin']);
        \App\Models\Userrole::create(['role' => 'Member']);
        \App\Models\Userrole::create(['role' => 'Affiliate']);
        \App\Models\Userrole::create(['role' => 'Sub Member']);
    }
}


class UserModuleTableSeeder extends Seeder
{
    public function run()
    {
        \App\Models\Users::create(['name' => 'Super admin', 'email' => env('FROM_EMAIL'), 'password' => '$2y$10$g.DN6KsYk6Qagjx0Y6JDvO2ZIC1geTcVN5yPZAahGCGtG6XcoApTi', 'active' => '1']);
    }
}


class UserdetailsModuleTableSeeder extends Seeder
{
    public function run()
    {
        \App\Models\Userdetails::create(['user_id' => '1', 'company' => 'super admin']);
    }
}

class User_assigned_roleModuleTableSeeder extends Seeder
{
    public function run()
    {
        \App\Models\User_assigned_role::create(['user_id' => '1', 'user_role_id' => '1']);
    }
}

class Role_accessModuleTableSeeder extends Seeder
{
    public function run()
    {
        \App\Models\Role_access_modules::create(['role_id'=>'1','application_module_id'=>'1','create'=>'1','edit'=>'1','delete'=>'1','view'=>'1','access'=>'1']);
        \App\Models\Role_access_modules::create(['role_id'=>'2','application_module_id'=>'1','create'=>'0','edit'=>'0','delete'=>'0','view'=>'0','access'=>'0']);
        \App\Models\Role_access_modules::create(['role_id'=>'3','application_module_id'=>'1','create'=>'1','edit'=>'1','delete'=>'1','view'=>'1','access'=>'1']);
        \App\Models\Role_access_modules::create(['role_id'=>'4','application_module_id'=>'1','create'=>'0','edit'=>'0','delete'=>'0','view'=>'0','access'=>'0']);
        \App\Models\Role_access_modules::create(['role_id'=>'1','application_module_id'=>'2','create'=>'1','edit'=>'1','delete'=>'1','view'=>'1','access'=>'1']);
        \App\Models\Role_access_modules::create(['role_id'=>'2','application_module_id'=>'2','create'=>'0','edit'=>'0','delete'=>'0','view'=>'0','access'=>'0']);
        \App\Models\Role_access_modules::create(['role_id'=>'3','application_module_id'=>'2','create'=>'0','edit'=>'0','delete'=>'0','view'=>'0','access'=>'0']);
        \App\Models\Role_access_modules::create(['role_id'=>'4','application_module_id'=>'2','create'=>'0','edit'=>'0','delete'=>'0','view'=>'0','access'=>'0']);
        \App\Models\Role_access_modules::create(['role_id'=>'1','application_module_id'=>'3','create'=>'1','edit'=>'1','delete'=>'1','view'=>'1','access'=>'1']);
        \App\Models\Role_access_modules::create(['role_id'=>'2','application_module_id'=>'3','create'=>'0','edit'=>'0','delete'=>'0','view'=>'0','access'=>'0']);
        \App\Models\Role_access_modules::create(['role_id'=>'3','application_module_id'=>'3','create'=>'1','edit'=>'1','delete'=>'1','view'=>'1','access'=>'1']);
        \App\Models\Role_access_modules::create(['role_id'=>'4','application_module_id'=>'3','create'=>'0','edit'=>'0','delete'=>'0','view'=>'0','access'=>'0']);
        \App\Models\Role_access_modules::create(['role_id'=>'1','application_module_id'=>'4','create'=>'1','edit'=>'1','delete'=>'1','view'=>'1','access'=>'1']);
        \App\Models\Role_access_modules::create(['role_id'=>'2','application_module_id'=>'4','create'=>'0','edit'=>'0','delete'=>'0','view'=>'0','access'=>'0']);
        \App\Models\Role_access_modules::create(['role_id'=>'3','application_module_id'=>'4','create'=>'0','edit'=>'0','delete'=>'0','view'=>'0','access'=>'0']);
        \App\Models\Role_access_modules::create(['role_id'=>'4','application_module_id'=>'4','create'=>'0','edit'=>'0','delete'=>'0','view'=>'0','access'=>'0']);
        \App\Models\Role_access_modules::create(['role_id'=>'1','application_module_id'=>'5','create'=>'1','edit'=>'1','delete'=>'1','view'=>'1','access'=>'1']);
        \App\Models\Role_access_modules::create(['role_id'=>'2','application_module_id'=>'5','create'=>'0','edit'=>'0','delete'=>'0','view'=>'0','access'=>'0']);
        \App\Models\Role_access_modules::create(['role_id'=>'3','application_module_id'=>'5','create'=>'0','edit'=>'0','delete'=>'0','view'=>'0','access'=>'0']);
        \App\Models\Role_access_modules::create(['role_id'=>'4','application_module_id'=>'5','create'=>'0','edit'=>'0','delete'=>'0','view'=>'0','access'=>'0']);
        \App\Models\Role_access_modules::create(['role_id'=>'1','application_module_id'=>'6','create'=>'1','edit'=>'1','delete'=>'1','view'=>'1','access'=>'1']);
        \App\Models\Role_access_modules::create(['role_id'=>'2','application_module_id'=>'6','create'=>'0','edit'=>'0','delete'=>'0','view'=>'0','access'=>'0']);
        \App\Models\Role_access_modules::create(['role_id'=>'3','application_module_id'=>'6','create'=>'0','edit'=>'0','delete'=>'0','view'=>'0','access'=>'0']);
        \App\Models\Role_access_modules::create(['role_id'=>'4','application_module_id'=>'6','create'=>'0','edit'=>'0','delete'=>'0','view'=>'0','access'=>'0']);
        \App\Models\Role_access_modules::create(['role_id'=>'1','application_module_id'=>'7','create'=>'1','edit'=>'1','delete'=>'1','view'=>'1','access'=>'1']);
        \App\Models\Role_access_modules::create(['role_id'=>'2','application_module_id'=>'7','create'=>'0','edit'=>'0','delete'=>'0','view'=>'0','access'=>'0']);
        \App\Models\Role_access_modules::create(['role_id'=>'3','application_module_id'=>'7','create'=>'0','edit'=>'0','delete'=>'0','view'=>'0','access'=>'0']);
        \App\Models\Role_access_modules::create(['role_id'=>'4','application_module_id'=>'7','create'=>'0','edit'=>'0','delete'=>'0','view'=>'0','access'=>'0']);

    }
}

class User_accessModuleTableSeeder extends Seeder
{
    public function run()
    {
        \App\Models\User_access_modules::create(['user_id'=>'1','application_module_id'=>'1','create'=>'1','edit'=>'1','delete'=>'1','view'=>'1','access'=>'1']);
        \App\Models\User_access_modules::create(['user_id'=>'1','application_module_id'=>'2','create'=>'1','edit'=>'1','delete'=>'1','view'=>'1','access'=>'1']);
        \App\Models\User_access_modules::create(['user_id'=>'1','application_module_id'=>'3','create'=>'1','edit'=>'1','delete'=>'1','view'=>'1','access'=>'1']);
        \App\Models\User_access_modules::create(['user_id'=>'1','application_module_id'=>'4','create'=>'1','edit'=>'1','delete'=>'1','view'=>'1','access'=>'1']);
        \App\Models\User_access_modules::create(['user_id'=>'1','application_module_id'=>'5','create'=>'1','edit'=>'1','delete'=>'1','view'=>'1','access'=>'1']);
        \App\Models\User_access_modules::create(['user_id'=>'1','application_module_id'=>'6','create'=>'1','edit'=>'1','delete'=>'1','view'=>'1','access'=>'1']);
        \App\Models\User_access_modules::create(['user_id'=>'1','application_module_id'=>'7','create'=>'1','edit'=>'1','delete'=>'1','view'=>'1','access'=>'1']);
    }

}    



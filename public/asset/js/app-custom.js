$(document).ready(function () {
    $('#editeTable').editableTableWidget();
    var rand_text = makeid(5);

    var newRowContent = "<tr><th scope='row' id='"+rand_text+"'><input id='back-out-input' type='text' placeholder='Event Name' data-val="+rand_text+" class='event_name "+rand_text+"event_name'></th>";
    newRowContent += "<th scope='row'><input id='back-out-input' type='date' data-val="+rand_text+" class='start_date "+rand_text+"start_date'></th>";
    newRowContent += "<th scope='row'><input id='back-out-input' type='date' data-val="+rand_text+" class='last_date "+rand_text+"last_date'></th>";
    newRowContent += "<th scope='row'><input id='back-out-input' type='text' data-val="+rand_text+" value='0' placeholder='Number Of Days' class='number_of_day "+rand_text+"number_of_day'></th>";
    newRowContent += "<th scope='row' class='d-flex'><input id='back-out-input' type='text' data-val="+rand_text+" class='type "+rand_text+"type value='Production' placeholder='Type'><div class='backout-delete-icon'><i class='fas fa-trash-alt'></i></div></th></tr/>";
    $('.back-out-add').on('click', function () {
        var rand_texts = makeid(5);
        var dtToday = new Date();
        var month = dtToday.getMonth() + 1;
        var day = dtToday.getDate();
        var year = dtToday.getFullYear();
        if(month < 10)
            month = '0' + month.toString();
        if(day < 10)
            day = '0' + day.toString();

        var maxDate = year + '-' + month + '-' + day;
        $('.'+rand_texts+'start_date').attr('min', maxDate);
        $('.'+rand_texts+'last_date').attr('min', maxDate);
        $('.'+rand_texts+'start_date').val(maxDate);
        $('.'+rand_texts+'last_date').val(maxDate);

        $('.'+rand_texts+'last_date').prop('disabled',true);
        $('.'+rand_texts+'number_of_day').prop('readonly',true);

        var newRowContents = "<tr><th scope='row' id='"+rand_texts+"'><input id='back-out-input' type='text' placeholder='Event Name' data-val="+rand_texts+" class='event_name "+rand_texts+"event_name'></th>";
        newRowContents += "<th scope='row'><input id='back-out-input' type='date' data-val="+rand_texts+" class='start_date "+rand_texts+"start_date'></th>";
        newRowContents += "<th scope='row'><input id='back-out-input' type='date' data-val="+rand_texts+" class='last_date "+rand_texts+"last_date'></th>";
        newRowContents += "<th scope='row'><input id='back-out-input' type='text' data-val="+rand_texts+" value='0' placeholder='Number Of Days' class='number_of_day "+rand_texts+"number_of_day'></th>";
        newRowContents += "<th scope='row' class='d-flex'><input id='back-out-input' type='text' data-val="+rand_text+" class='type "+rand_texts+"type value='Production' placeholder='Type'><div class='backout-delete-icon'><i class='fas fa-trash-alt'></i></div></th></tr/>";
        $('.back-out-tbody').append(newRowContents);
    });

    $('.filter-one-btn').on('click', function(){
        $('.ui-filter-card').toggleClass('active');
    });

    $('.ui-filter-cancel').on('click', function(){
        $('.ui-filter-card').removeClass('active');
    })
    // $(window).on('click', function(e){
    //     if(!$(e.target).hasClass('ui-filter-card')){

    //     }
    // })

    $('.suppliers-settings-option').on('change', function(){
        var getOption = $('.suppliers-settings-option option:selected').text();
        if(getOption == 'Bi-Monthly'){
            $('.suppliers-settings-btns-month').css({
                display: 'block'
            })
            $('.suppliers-settings-btns-weekly').css({
                display: 'none'
            })
        }else{
            $('.suppliers-settings-btns-month').css({
                display: 'none'
            })
            $('.suppliers-settings-btns-weekly').css({
                display: 'block'
            })
        }
    })
});

saveAddress = () => {
    let getSaveAddress = document.querySelector('.address-save-form');
    let getFormAddress = document.querySelector('.main-address-from');
    getSaveAddress.style.display = 'block';
    getFormAddress.style.display = 'none';
    console.log(getSaveAddress);
}

saveDetails = () => {
    let getSaveAddress = document.querySelector('.details-save-form');
    let getFormAddress = document.querySelector('.main-details-from');
    getSaveAddress.style.display = 'block';
    getFormAddress.style.display = 'none';
    console.log(getSaveAddress);
}

editeAdress = () => {
    let getSaveAddress = document.querySelector('.address-save-form');
    let getFormAddress = document.querySelector('.main-address-from');
    getSaveAddress.style.display = 'none';
    getFormAddress.style.display = 'block';
}

editeDetails = () => {
    let getSaveAddress = document.querySelector('.details-save-form');
    let getFormAddress = document.querySelector('.main-details-from');
    getSaveAddress.style.display = 'none';
    getFormAddress.style.display = 'block';
}

const $tableID = $('#table');
const $BTN = $('#export-btn');
const $EXPORT = $('#export');

const newTr = `
<tr>
                                                            <th scope="row" width="20%"
                                                                contenteditable="true">
                                                                <ul
                                                                        class="d-flex flex-row align-items-center table-checkbox-main">
                                                                    <input type="hidden" class="vendors_contact_id rows_ids" value="">
                                                                    <li class="first_name">
                                                                        Josh
                                                                    </li>
                                                                </ul>
                                                            </th>
                                                            <td contenteditable="true" width="20%" class="last_name">
                                                                Bochner</td>
                                                            <td contenteditable="true" width="20%" class="title">
                                                                Title 2</td>
                                                            <td contenteditable="true" width="20%" class="emails">
                                                                joshbochner@gmail.com
                                                            </td>
                                                            <td width="20%" contenteditable="true">
                                                                <ul
                                                                        class="d-flex justify-content-between align-items-center">
                                                                    <li><span class="phone_number">+00 - 1234567890</span>
                                                                    </li>
                                                                    <li class="delete-btn-vendors-tab">
                                                                        <i class="fas fa-trash-alt"></i>
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                        </tr>`;

$('.table-add').on('click', 'i', () => {

    const $clone = $tableID.find('tbody tr').last().clone(true).removeClass('hide table-line');

    if ($tableID.find('tbody tr').length === 0) {

        $('tbody').append(newTr);
    }

    $tableID.find('table').append($clone);
});

$tableID.on('click', '.table-remove', function () {

    $(this).parents('tr').detach();
});

$tableID.on('click', '.table-up', function () {

    const $row = $(this).parents('tr');

    if ($row.index() === 0) {
        return;
    }

    $row.prev().before($row.get(0));
});

$tableID.on('click', '.table-down', function () {

    const $row = $(this).parents('tr');
    $row.next().after($row.get(0));
});

// A few jQuery helpers for exporting only
jQuery.fn.pop = [].pop;
jQuery.fn.shift = [].shift;

$BTN.on('click', () => {

    const $rows = $tableID.find('tr:not(:hidden)');
    const headers = [];
    const data = [];

    // Get the headers (add special header logic here)
    $($rows.shift()).find('th:not(:empty)').each(function () {

        headers.push($(this).text().toLowerCase());
    });

    // Turn all existing rows into a loopable array
    $rows.each(function () {
        const $td = $(this).find('td');
        const h = {};

        // Use the headers from earlier to name our hash keys
        headers.forEach((header, i) => {

            h[header] = $td.eq(i).text();
        });

        data.push(h);
    });

    // Output the result
    $EXPORT.text(JSON.stringify(data));
});

suppliersSettingEdit = () => {
    let getSuppliersList = document.querySelector('.suppliers-settings-li-01');
    let getSuppliersList2 = document.querySelector('.suppliers-settings-li-02');
    let getSuppliersListTxt = document.querySelector('.suppliers-settings-li-txt');
    let getSuppliersMonthTxt = document.querySelector('.suppliers-settings-btns-month-txt');
    let getSuppliersList2Ul = document.querySelector('.suppliers-settings-li-02.suppliers-settings-li-02-show ul');
    let getSuppliersListMainBox = document.querySelector('.suppliers-settings-main-box');
    getSuppliersListMainBox.classList.add('deactive');
    getSuppliersListMainBox.classList.remove('active');
    getSuppliersList2Ul.style.float = 'left';

    getSuppliersList.style.display = 'block';
    getSuppliersList2.style.display = 'block';
    getSuppliersListTxt.style.display = 'none';
    getSuppliersMonthTxt.style.display = 'none';
}

saveMonth = () => {
    let getSuppliersList = document.querySelector('.suppliers-settings-li-01');
    let getSuppliersList2 = document.querySelector('.suppliers-settings-li-02');
    let getSuppliersListTxt = document.querySelector('.suppliers-settings-li-txt');
    let getSuppliersMonthTxt = document.querySelector('.suppliers-settings-btns-month-txt');
    let getSuppliersList2Ul = document.querySelector('.suppliers-settings-li-02.suppliers-settings-li-02-show ul');
    let getSuppliersListMainBox = document.querySelector('.suppliers-settings-main-box');
    getSuppliersListMainBox.classList.add('active');
    getSuppliersListMainBox.classList.remove('deactive');

    getSuppliersList.style.display = 'none';
    getSuppliersList2.style.display = 'none';
    getSuppliersList2.classList.add('active');
    getSuppliersListTxt.style.display = 'block';
    getSuppliersMonthTxt.style.display = 'block';
    getSuppliersList2Ul.style.float = 'right';
}

// backOutAdd = () => {

//     let getBackoutTable = document.querySelector('.blackout-tbody');
//     getBackoutTable.appendChild(mkBackoutRow);
//     console.log(mkBackoutRow);
// }
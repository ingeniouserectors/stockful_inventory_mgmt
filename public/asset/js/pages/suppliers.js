$(document).ready(function() {
    $('.selected-row-number').hide();

    $('.row-checkbox').change(function(){
        if(!$(this).checked){
            $('.selected-row-number').hide();
            $('#select-all').prop('checked', false)
        }
    });

    $('#select-all').change(function(e){
        if($(this).prop('checked')){
            $('.row-checkbox').prop('checked', true);
            $('.selected-row-number').show();
            $('#number-of-selected').text(4)
        }else{
            $('.row-checkbox').prop('checked', false).trigger('change');
        }
    })

    $('#select-all-link').click(function(e){
        $('.selected-row-number').show();
        $('#select-all').prop('checked', true)
        $('.row-checkbox').prop('checked', true)
        $('#number-of-selected').text('100')
    })

    // $('th').click(function(e){
    //     e.preventDefault()
    //     e.stopPropagation()
    //     if($(this).hasClass('sorting') || $(this).hasClass('sorting_desc')){
    //         $('th').attr('class', 'sorting');
    //         $(this).attr('class', 'sorting_asc')
    //     } else if($(this).hasClass('sorting_asc')){
    //         $(this).attr('class', 'sorting_desc')
    //     }
    // })

    $('#toggle-columns input:checkbox').attr('checked', true).change(function(){
        var shcolumn = '.' + $(this).attr('name')
        $(shcolumn).toggle()
    })
});

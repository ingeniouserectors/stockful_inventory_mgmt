$(document).ready(function() {
    $('.selected-row-number').hide();

    $('tbody').on('change', '.row-checkbox', function(){
        if(!$(this).checked){
            $('.selected-row-number').hide();
            $('#select-all').prop('checked', false)
        }
    });

    $('#select-all').change(function(e){
        if($(this).prop('checked') && $('tbody tr').length > 2){
            $('.row-checkbox').prop('checked', true);
            $('.selected-row-number').show();
            $('#number-of-selected').text(4)
        }else{
            $('.row-checkbox').prop('checked', false).trigger('change');
        }
    })

    $('#select-all-link').click(function(e){
        $('.selected-row-number').show();
        $('#select-all').prop('checked', true)
        $('.row-checkbox').prop('checked', true)
        $('#number-of-selected').text('100')
    })

    $('table tbody').on('click', '.add-row', function(e){
        e.preventDefault()
        $('table th').show()
        $('table td').show()
        $('#table-columns input:checkbox').attr('checked', true)

        $('.no-list').remove();
        if(!$('.add-row-tr').length){
            $('tbody').append('\
                <tr class="add-row-tr">\
                    <td colspan="7">\
                        <span class="text-primary1 add-row">\
                            <i class="bx bx-plus-circle font-size-16 align-middle"></i>\
                            Add New Shipping Agent\
                        </span>\
                    </td>\
                </tr>\
            ')
        }
        
        $('.add-row-tr').before('\
            <tr>\
                <td>\
                    <div class="custom-control custom-checkbox">\
                        <input type="checkbox" class="custom-control-input row-checkbox" id="row-'+ $("tbody tr").length +'">\
                        <label class="custom-control-label" for="row-'+ $("tbody tr").length +'"></label>\
                    </div>\
                </td>\
                <td class="align-middle col1">\
                    <input type="text" placeholder="Company Name">\
                </td>\
                <td  class="align-middle col2">\
                    <input type="text" placeholder="First Name">\
                </td>\
                <td  class="align-middle col3">\
                    <input type="text" placeholder="Last Name">\
                </td>\
                <td  class="align-middle col4">\
                    <input type="email" placeholder="Email">\
                </td>\
                <td  class="align-middle col5">\
                    <input type="text" placeholder="Phone Number">\
                </td>\
                <td class="align-middle">\
                    <i class="mdi mdi-trash-can-outline font-size-18 text-danger row-hover-action" data-toggle="tooltip" title="Delete"></i>\
                </td>\
            </tr>\
        ')
        
        $('i[data-toggle="tooltip"]').tooltip(); 
    });

    $("tbody").on('focusin', 'td', function(e){
        if(!$(e.target).is("input:checkbox")){
            $(this).css("border", "1px solid #E1EDFF");
        }
    });

    $("tbody").on('focusout', 'td', function(){
        $(this).css('border', 'none')
        $(this).css('border-top', '1px solid #eff2f7');
    });

    $('th').click(function(e){
        e.preventDefault()
        e.stopPropagation()
        if($(this).hasClass('sorting') || $(this).hasClass('sorting_desc')){
            $('th').attr('class', 'sorting');
            $(this).attr('class', 'sorting_asc')
        } else if($(this).hasClass('sorting_asc')){
            $(this).attr('class', 'sorting_desc')
        }
    })

    $('#toggle-columns input:checkbox').attr('checked', true).change(function(){
        var shcolumn = '.' + $(this).attr('name')
        $(shcolumn).toggle()
    })
} );
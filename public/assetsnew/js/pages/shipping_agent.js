$(document).ready(function() {
    $('.selected-row-number').hide();

    $('tbody').on('change', '.row-checkbox', function(){
        if(!$(this).checked){
            $('.selected-row-number').hide();
            $('#select-all').prop('checked', false)
        }
    });

    $('#select-all').change(function(e){

        if($(this).prop('checked') && $('tbody tr').length > 2){
            $('.row-checkbox').prop('checked', true);
            $('.selected-row-number').show();
            //$('#number-of-selected').text(4)
            $("#number-of-selected").text($("#total_record").text());
            $(".total_record").text($("#total_record").text());
        }else{
            $('.row-checkbox').prop('checked', false).trigger('change');
        }
    })

    $('#select-all-link').click(function(e){
        $('.selected-row-number').show();
        $('#select-all').prop('checked', true)
        $('.row-checkbox').prop('checked', true)
        //$('#number-of-selected').text('100')
        $("#number-of-selected").text($("#total_record").text());
        $(".total_record").text($("#total_record").text());
    })
    // checked_arr = [];
    // $.each($('#toggle-columns input:checkbox:not(:checked)'),function(idx,val){
    //     console.log(val);
    // //     $id = $(this).attr('data-id');
    // //     $text = $(this).text();

    // //    // console.log($id);
    // //     if($id != 'undefined' &&  $id != ''){
    // //         checked_arr.push($id);
    // //     }
    // });
    // console.log(checked_arr);

    

    $("tbody").on('focusin', 'td', function(e){
        if(!$(e.target).is("input:checkbox")){
            $(this).css("border", "1px solid #E1EDFF");
        }
    });

    $("tbody").on('focusout', 'td', function(){
        $(this).css('border', 'none')
        $(this).css('border-top', '1px solid #eff2f7');
    });

    $('th').click(function(e){
        e.preventDefault()
        e.stopPropagation()
        if($(this).hasClass('sorting') || $(this).hasClass('sorting_desc')){
            $('th').attr('class', 'sorting');
            $(this).attr('class', 'sorting_asc')
        } else if($(this).hasClass('sorting_asc')){
            $(this).attr('class', 'sorting_desc')
        }
    })
});
//     setTimeout(function(){ 
//         $value = localStorage.getItem("checked_arr");
//         $val = [];
//         if($value.length > 0){
//             $val = $value.split(',');
//             $.each($('#toggle-columns input:checkbox'),function(idx,val){
//                 $id = $(this).attr('id');
//                 if($id != 'undefined' &&  $id != '' && ($val.indexOf($id) != -1)){
//                     var shcolumn = '.' + $(this).attr('name');
//                      $('#'+$id).prop('checked', false);
//                     // console.log(shcolumn);
//                      $(shcolumn).toggle();
//                      $(shcolumn).parent('td').hide();
//                 }
//             });
//             var uncheckedFilter = $('#toggle-columns input:checkbox:not(:checked)').length;
//             if(uncheckedFilter == 0){
//                 $('.filedFilter').text('');
//             }else{
//                 $('.filedFilter').text(uncheckedFilter);
//             }
//             //$('#toggle-columns input:checkbox').attr('checked', true).trigger('change');        
//         }
//      }, 1000);
//     // localStorage.clear();
    

// } );

// $('#toggle-columns input:checkbox').attr('checked', true).change(function(){
//     var shcolumn = '.' + $(this).attr('name');
//     $(shcolumn).toggle();
//     $(shcolumn).parent('td').toggle();
//     var checked_arr = [];
    
    
//     var uncheckedFilter = $('#toggle-columns input:checkbox:not(:checked)').length;

//     $.each($('#toggle-columns input:checkbox:not(:checked)'),function(idx,val){
//         $id = $(this).attr('id');
//         if($id != 'undefined' &&  $id != ''){
//             checked_arr.push($id);
//         }
//     });
//     localStorage.setItem("checked_arr", checked_arr);

//     if(uncheckedFilter == 0){
//         $('.filedFilter').text('');
//     }else{
//         $('.filedFilter').text(uncheckedFilter);
//     }
// });


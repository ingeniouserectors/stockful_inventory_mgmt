$(document).ready(function() {
    $('.selected-row-number').hide();

    $('.row-checkbox').change(function(){
        if(!$(this).checked){
            $('.selected-row-number').hide();
            $('#select-all').prop('checked', false)
        }
    });

    $('#select-all').change(function(e){
        if($(this).prop('checked')){
            $('.row-checkbox').prop('checked', true);
            $('.selected-row-number').show();
            var totalselected = $('.row-checkbox:checked').length;
            $('#number-of-selected').text(totalselected)
        }else{
            $('.row-checkbox').prop('checked', false).trigger('change');
        }
    })

    $('body').on('click','#select-all-link',function(){
        $('.selected-row-number').show();
        $('#select-all').prop('checked', true)
        $('.row-checkbox').prop('checked', true)
        var totalselected = $('.total_listing_count').val();
        $('#number-of-selected').text(totalselected)
        $('.checkdeleteall').val(1);
    })

    $('th').click(function(e){
        e.preventDefault()
        e.stopPropagation()
        if($(this).hasClass('sorting') || $(this).hasClass('sorting_desc')){
            $('th').attr('class', 'sorting');
            $(this).attr('class', 'sorting_asc')
        } else if($(this).hasClass('sorting_asc')){
            $(this).attr('class', 'sorting_desc')
        }
    })

    // $('#toggle-columns input:checkbox').attr('checked', true).change(function(){
    //     var shcolumn = '.' + $(this).attr('name');
    //     $(shcolumn).toggle();
    //     var uncheckedFilter = $('#toggle-columns input:checkbox:not(:checked)').length;
    //     if(uncheckedFilter == 0){
    //         $('.filedFilter').text('');
    //     }else{
    //         $('.filedFilter').text(uncheckedFilter);
    //     }
    // })
});

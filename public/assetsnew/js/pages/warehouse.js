$(document).ready(function() {

    $('#contacts-tab').hide();
    // basic_details
    var basic_details = $('#basic-details')

    if($('#warehouse_type').val() == '1'){
        $('#users-tab').click()
    }else{
        $('#contacts-tab').show();
        $("#users-tab").hide();
        $('#contacts-tab').click()
    }
    $(".nav-link").removeClass('active');
    $(".tab-pane").removeClass('active');
    $("#basic-details").addClass('active');
   // $("#basic-details").show();
    $("#basic-details-tab").addClass('active');

    basic_details.on('click', '#domestic', function(e){
        e.preventDefault()
        $(this).addClass('btn-primary').removeClass('btn-dark')
        basic_details.find('#international').removeClass('btn-primary').addClass('btn-dark')
    })
    basic_details.on('click', '#international', function(e){
        e.preventDefault()
        $(this).addClass('btn-primary').removeClass('btn-dark')
        basic_details.find('#domestic').removeClass('btn-primary').addClass('btn-dark')
    })

    basic_details.find('.val-label').hover(function(e){
        $(this).find('button').removeClass('invisible')
    }, function(e){
        $(this).find('button').addClass('invisible')
    })

    basic_details.find('.val-label').click(function(e){
        e.preventDefault()
        $(this).parents('.col-sm-9').addClass('d-none');
        $(this).parents('.col-sm-9').next().removeClass('d-none');
    })

    basic_details.on('click', '.save', function(e){
        e.preventDefault();
        // basic_details.find('form:nth-child(1)').addClass('d-none')
        // basic_details.find('form:nth-child(2)').removeClass('d-none')
        // basic_details.find('.form-group').find('.col-sm-9:nth-child(3)').addClass('d-none')
        // basic_details.find('.form-group').find('.col-sm-9:nth-child(2)').removeClass('d-none')
    })
    
    basic_details.on('click', '.save-next', function(e){
        e.preventDefault();
        
        // if($('#warehouse_type').val() == 'self'){
        //     $('#users-tab').click()
        // }else{
        //     $('#contacts-tab').click()
        // }
    })


    // Contacts
    var contacts = $('#contacts')
    // var contactsData = [];
    contacts.find('th').click(function(e){
        e.preventDefault()
        e.stopPropagation()
        if($(this).hasClass('sorting') || $(this).hasClass('sorting_desc')){
            $(this).removeClass('sorting');
            $(this).removeClass('sorting_desc');
            $(this).addClass('sorting_asc')
        } else if($(this).hasClass('sorting_asc')){
            $(this).removeClass('sorting');
            $(this).removeClass('sorting_asc');
            $(this).addClass('sorting_desc')
        }
    })

    contacts.find("tbody").on('focusin', 'td', function(){
        $(this).css("border", "1px solid #E1EDFF");
    });

    contacts.find("tbody").on('focusout', 'td', function(){
        $(this).css('border', 'none')
        $(this).css('border-top', '1px solid #eff2f7');
    });

    contacts.on('click', '.add-row', function(e){
        e.preventDefault()
        $('.no-list').remove();
        if(!$('#contacts .add-row-tr').length){
            $('#contacts tbody').append('\
                <tr class="add-row-tr">\
                    <td colspan="7" class="text-center">\
                        <span class="text-primary1 add-row">\
                            <i class="bx bx-plus-circle font-size-16 align-middle"></i>\
                            Add Contacts\
                        </span>\
                    </td>\
                </tr>\
            ')
        }

        $('#contacts .add-row-tr').before('\
            <tr >\
                <td style="display: none;"></td>\
                <td class="align-middle">\
                    <input class="primary_contact" type="hidden" value="0">\
                    <input type="text" placeholder="First Name" class="first_name" maxlength="50" value=""><span class="col1"></span>\
                </td>\
                <td  class="align-middle">\
                    <input type="text" placeholder="Last Name" class="last_name" maxlength="50" value=""><span class="col2"></span>\
                </td>\
                <td  class="align-middle">\
                    <input type="text" placeholder="Title" class="title" value="" maxlength="100"><span class="col3"></span>\
                </td>\
                <td  class="align-middle">\
                    <input type="email" placeholder="Email" class="emails" value="" maxlength="50"><span class="col4"></span>\
                </td>\
                <td  class="align-middle">\
                    <input type="tel" placeholder="Phone number" class="phone_number" value="" maxlength="12"><span class="col5"></span>\
                </td>\
                <td  class="align-middle">\
                    <i class="mdi mdi-star font-size-18" data-toggle="tooltip" title="Make Primary Contact"></i>\
                </td>\
            </tr>\
        ')
        $('.mdi-star').tooltip(); 
        // contacts.find('table').DataTable();
    });

    contacts.find('tbody').on('click', '.mdi-star', function(e){
        e.preventDefault();
        $('.primary').val(0);
        contacts.find('tbody tr').removeClass('active');
        $(this).parents('tr').addClass('active');
        $('.primary_contact').val('0');
        $(this).closest('tr').find('.primary_contact').val('1');
    })

    contacts.on('click', '.save', function(e){
        e.preventDefault();
        basic_details.find('form:nth-child(1)').addClass('d-none')
        basic_details.find('form:nth-child(2)').removeClass('d-none')
        basic_details.find('.form-group').find('.col-sm-9:nth-child(3)').addClass('d-none')
        basic_details.find('.form-group').find('.col-sm-9:nth-child(2)').removeClass('d-none')
    })
    
    contacts.on('click', '.save-next', function(e){
        e.preventDefault();
        $('#settings-tab').click()
    })

    // Users
    var users = $('#users')
    // users.on('click', '.save', function(e){
    //     e.preventDefault();
    //     // users.find('form:nth-child(1)').addClass('d-none')
    //     // users.find('form:nth-child(2)').removeClass('d-none')
    //     // users.find('.form-group').find('.col-sm-8:nth-child(3)').addClass('d-none')
    //     // users.find('.form-group').find('.col-sm-8:nth-child(2)').removeClass('d-none')
    // })

    // users.on('click', '.save-next', function(e){
    //     e.preventDefault();
    //     $('#settings-tab').click()
    // })

    users.find('.val-label').hover(function(e){
        $(this).find('button').removeClass('invisible')
    }, function(e){
        $(this).find('button').addClass('invisible')
    })

    users.find('.val-label').click(function(e){
        e.preventDefault()
        $(this).parents('.col-sm-8').addClass('d-none');
        $(this).parents('.col-sm-8').next().removeClass('d-none');
    })

    //Settings
    var settings = $('#settings')
    
    // settings.on('click', '.save', function(e){
    //     e.preventDefault();
    //     // settings.find('form:nth-child(1)').addClass('d-none')
    //     // settings.find('form:nth-child(2)').removeClass('d-none')
    //     // settings.find('.form-group').find('.col-sm-8:nth-child(3)').addClass('d-none')
    //     // settings.find('.form-group').find('.col-sm-8:nth-child(2)').removeClass('d-none')
    // })

    settings.find('.val-label').hover(function(e){
        $(this).find('button').removeClass('invisible')
    }, function(e){
        $(this).find('button').addClass('invisible')
    })

    settings.find('.val-label').click(function(e){
        e.preventDefault()
        $(this).parents('.col-sm-8').addClass('d-none');
        $(this).parents('.col-sm-8').next().removeClass('d-none');
    })
    
    // settings.on('click', '.save-next', function(e){
    //     e.preventDefault();
    // })

    $('.select2').select2();

});
$('#warehouse_type').change(function(e){
    if($('#warehouse_type').val() == '1'){
        $('#contacts-tab').hide();
        $('#users-tab').show();
    }else{
        $('#contacts-tab').show();
        $('#users-tab').hide();
    }
})
$('#warehouse_type_edit').change(function(e){
    if($('#warehouse_type_edit').val() == '1'){
        $('#contacts-tab').hide();
        $('#users-tab').show();
    }else{
        $('#contacts-tab').show();
        $('#users-tab').hide();
    }
})
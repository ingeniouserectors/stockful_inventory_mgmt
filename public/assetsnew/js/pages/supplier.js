$(document).ready(function() {
    // basic_details
    var basic_details = $('#basic-details')

    basic_details.find('.val-label').hover(function(e){
        $(this).find('button').removeClass('invisible')
    }, function(e){
        $(this).find('button').addClass('invisible')
    })

    basic_details.find('.val-label').click(function(e){
        e.preventDefault()
        $(this).parents('.col-sm-9').addClass('d-none');
        $(this).parents('.col-sm-9').next().removeClass('d-none');
    })

    basic_details.on('click', '.save', function(e){
        e.preventDefault();
        // basic_details.find('form:nth-child(1)').addClass('d-none')
        // basic_details.find('form:nth-child(2)').removeClass('d-none')
        // basic_details.find('.form-group').find('.col-sm-9:nth-child(3)').addClass('d-none')
        // basic_details.find('.form-group').find('.col-sm-9:nth-child(2)').removeClass('d-none')
    })

    basic_details.on('click', '.save-next', function(e){
        e.preventDefault();
        // $('#contacts-tab').click()
    })


    // Contacts
    var contacts = $('#contacts')
    // var contactsData = [];
    contacts.find('th').click(function(e){
        e.preventDefault()
        e.stopPropagation()
       /* if($(this).hasClass('sorting') || $(this).hasClass('sorting_desc')){
            contacts.find('th').attr('class', 'sorting');
            $(this).attr('class', 'sorting_asc')
        } else if($(this).hasClass('sorting_asc')){
            $(this).attr('class', 'sorting_desc')
        }*/

        if($(this).hasClass('sorting') || $(this).hasClass('sorting_desc')){
            $(this).removeClass('sorting');
            $(this).removeClass('sorting_desc');
            $(this).addClass('sorting_asc')
        } else if($(this).hasClass('sorting_asc')){
            $(this).removeClass('sorting');
            $(this).removeClass('sorting_asc');
            $(this).addClass('sorting_desc')
        }
    })

    contacts.find("tbody").on('focusin', 'td', function(){
        $(this).css("border", "1px solid #E1EDFF");
    });

    contacts.find("tbody").on('focusout', 'td', function(){
        $(this).css('border', 'none')
        $(this).css('border-top', '1px solid #eff2f7');
    });

    contacts.on('click', '.add-row', function(e){
        e.preventDefault()
        $('.no-list').remove();
        if(!$('#contacts .add-row-tr').length){
            $('#contacts tbody').append('\
                <tr class="add-row-tr">\
                    <td colspan="6">\
                        <span class="text-primary1 add-row">\
                            <i class="bx bx-plus-circle font-size-16 align-middle"></i>\
                            Add Contacts\
                        </span>\
                    </td>\
                </tr>\
            ')
        }

        $('#contacts .add-row-tr').before('\
            <tr>\
                <td style="display: none;"></td>\
                <td class="align-middle edit">\
                    <input class="primary_contact" type="hidden" value="0">\
                    <input id="first_name" type="text" placeholder="First Name" value="" maxlength="50">\
                    <span class="col1"></span>\
                </td>\
                <td  class="align-middle edit">\
                    <input id="last_name" type="text" placeholder="Last Name" value="" maxlength="50"><span class="col2"></span>\
                </td>\
                <td  class="align-middle edit">\
                    <input id="title" type="text" placeholder="Title" value="" maxlength="100"><span class="col3"></span>\
                </td>\
                <td  class="align-middle edit">\
                    <input id="emails" type="email" placeholder="Email" value="" maxlength="50"><span class="col4"></span>\
                </td>\
                <td  class="align-middle edit">\
                    <input id="phone_number" type="tel" placeholder="Phone number" value="" maxlength="12"><span class="col5"></span>\
                </td>\
                <td  class="align-middle edit">\
                    <i class="mdi mdi-star font-size-18" data-toggle="tooltip" title="Make Primary Contact"></i>\
                </td>\
            </tr>\
        ')
        $('.mdi-star').tooltip();
        // contacts.find('table').DataTable();
    });

    contacts.find('tbody').on('click', '.mdi-star', function(e){
        e.preventDefault();
        contacts.find('tbody tr').removeClass('active');
        $(this).parents('tr').addClass('active');
        $('.primary_contact').val('0');
        $(this).closest('tr').find('.primary_contact').val('1');
    })

    contacts.on('click', '.save', function(e){
        e.preventDefault();
        // basic_details.find('form:nth-child(1)').addClass('d-none')
        // basic_details.find('form:nth-child(2)').removeClass('d-none')
        // basic_details.find('.form-group').find('.col-sm-9:nth-child(3)').addClass('d-none')
        // basic_details.find('.form-group').find('.col-sm-9:nth-child(2)').removeClass('d-none')
    })

    contacts.on('click', '.save-next', function(e){
        e.preventDefault();
        $('#lead-time-tab').click()
    })

    // Lead Time
    var lead_time = $('#lead-time')
    $('#boat-btn').click(function(e){
        e.preventDefault()
        $('#boat-btn').removeClass('btn-light1').addClass('btn-primary')
        $('#plane-btn').removeClass('btn-primary').addClass('btn-light1')
        $('#plane-panel').addClass('d-none')
        $('#boat-panel').removeClass('d-none')
        $("#lead_time_check").val(1);
    })
    $('#plane-btn').click(function(e){
        e.preventDefault()
        $('#plane-btn').removeClass('btn-light1').addClass('btn-primary')
        $('#boat-btn').removeClass('btn-primary').addClass('btn-light1')
        $('#plane-panel').removeClass('d-none')
        $('#boat-panel').addClass('d-none')
        $("#lead_time_check").val(2);
    })
    $('.input-edit').focus(function(e){
        $(this).parent().removeClass('border-bottom').css('border-bottom', '1px solid #227CFF');
    })
    $('.input-edit').on('change blur', function(){
        if($(this).val()){
            $(this).parent().next().removeClass('d-none').find('span').html($(this).val() + ' Days');
            $(this).parent().addClass('d-none');
        }else{
            $(this).parent().addClass('border-bottom');
        }
    })
    $('.input-show').click(function(){
        $(this).parent().addClass('d-none');
        $(this).parent().prev().removeClass('d-none').find('input').focus();
    })
    lead_time.on('click', '.save', function(e){
        e.preventDefault();
    })

    lead_time.on('click', '.save-next', function(e){
        e.preventDefault();
        $('#order-settings-tab').click()
    })

    // Order Settings
    // var order_settings = $('#order-settings')
    // order_settings.find('#reorder_schedule').change(function(){
    //     $('.schedule-panels').children().addClass('d-none')
    //     switch($('#reorder_schedule').val()){
    //         case 'weekly':
    //             $('#weekly-panel').removeClass('d-none')
    //             break;
    //         case 'bi-weekly':
    //             $('#bi-weekly-panel').removeClass('d-none')
    //             break;
    //         case 'monthly':
    //             $('#monthly-panel').removeClass('d-none')
    //             break;
    //         case 'bi-monthly':
    //             $('#bi-monthly-panel').removeClass('d-none')
    //             break;
    //     }
    // })

    // order_settings.find('.input-show').click(function(e){
    //     // $(this).parent().next().removeClass('d-none');
    //     // $(this).parent().removeClass('d-flex').hide()
    //     e.preventDefault();
    //     order_settings.find('form:nth-child(2)').addClass('d-none')
    //     order_settings.find('form:nth-child(1)').removeClass('d-none')
    // })

    // order_settings.find('.input-show-edit').click(function(e){
    //     // $(this).parent().next().removeClass('d-none');
    //     // $(this).parent().removeClass('d-flex').hide()
    //     e.preventDefault();
    //     order_settings.find('#order_setting_form_edit').addClass('d-none')
    //     order_settings.find('#order_setting_form').removeClass('d-none')
    // })
    // order_settings.on('click', '.save', function(e){
    //     e.preventDefault();
    //     // order_settings.find('form:nth-child(1)').addClass('d-none')
    //     // order_settings.find('form:nth-child(2)').removeClass('d-none')
    // })

    // order_settings.on('click', '.save-next', function(e){
    //     e.preventDefault();
    //     //$('#blackout-dates-tab').click()
    // })

    // Blackout Dates
    var blackout_dates = $('#blackout-dates')

    blackout_dates.find('th').click(function(e){
        e.preventDefault()
        e.stopPropagation()
        /*if($(this).hasClass('sorting') || $(this).hasClass('sorting_desc')){
            blackout_dates.find('th').attr('class', 'sorting');
            $(this).attr('class', 'sorting_asc')
        } else if($(this).hasClass('sorting_asc')){
            $(this).attr('class', 'sorting_desc')
        }*/

        if($(this).hasClass('sorting') || $(this).hasClass('sorting_desc')){
            $(this).removeClass('sorting');
            $(this).removeClass('sorting_desc');
            $(this).addClass('sorting_asc')
        } else if($(this).hasClass('sorting_asc')){
            $(this).removeClass('sorting');
            $(this).removeClass('sorting_asc');
            $(this).addClass('sorting_desc')
        }
    })

    blackout_dates.find("tbody").on('focusin', 'td', function(e){
        if($(e.target).is("input")){
            $(this).css("border", "1px solid #E1EDFF");
        }
    });

    blackout_dates.find("tbody").on('focusout', 'td', function(){
        $(this).css('border', 'none')
        $(this).css('border-top', '1px solid #eff2f7');
    });

    blackout_dates.on('click', '.add-new-btn', function(e){
        e.preventDefault()
        let table = blackout_dates.find('table tbody');
        table.find('.no-list').remove();
        // if(!table.find('.add-row-tr').length){
        //     table.append('\
        //         <tr class="add-row-tr">\
        //             <td colspan="6">\
        //                 <button class="btn text-primary1 add-new-btn">\
        //                     <i class="bx bx-plus-circle font-size-16 align-middle"></i>\
        //                     Add New Event\
        //                 </button>\
        //             </td>\
        //         </tr>\
        //     ')
        // }
        table.find('.add-row-tr').before('\
            <tr>\
                <td class="align-middle">\
                    <input type="text" id="event_name" placeholder="Event Name">\
                </td>\
                <td  class="align-middle">\
                    <div class="input-group date" data-provide="datepicker" data-date-orientation="bottom" data-date-autoclose="true">\
                        <input type="text" id="start_date" placeholder="MM/DD/YY" class="form-control border-0">\
                        <div class="input-group-addon d-flex align-items-center">\
                            <i class="mdi mdi-calendar"></i>\
                        </div>\
                    </div>\
                </td>\
                <td  class="align-middle">\
                    <div class="input-group date" data-provide="datepicker" data-date-orientation="bottom" data-date-autoclose="true">\
                        <input type="text" id="end_date" placeholder="MM/DD/YY" class="form-control border-0">\
                        <div class="input-group-addon d-flex align-items-center">\
                            <i class="mdi mdi-calendar"></i>\
                        </div>\
                    </div>\
                </td>\
                <td  class="align-middle">\
                    <input type="text" id="number_of_days" class="bg-transparent days" placeholder="Number of Days" value="" disabled>\
                </td>\
                <td  class="align-middle">\
                    <select class="border-0" id="type">\
                        <option value="1">Production</option>\
                        <option value="2">Office</option>\
                    </select>\
                </td>\
                <td  class="align-middle">\
                    <i class="mdi mdi-trash-can-outline font-size-18 text-danger row-hover-action remove_new_data" data-toggle="tooltip" title="Delete"></i>\
                </td>\
            </tr>\
        ')
        $('.mdi-trash-can-outline').tooltip();
    });

    blackout_dates.on('change', '.date input',  function(e){
        e.preventDefault()
        dates = $(this).parents('tr').find('.date input');
        date1 = new Date($(dates[0]).val())
        date2 = new Date($(dates[1]).val())
        time = Math.abs(date1 - date2)
        days = Math.ceil(time/(3600 * 24 *1000))
        if(days){
            $(this).parents('tr').find('.days').val(days)
        }
    })

    blackout_dates.on('click', '.save', function(e){
        e.preventDefault();
    })

    blackout_dates.on('click', '.save-next', function(e){
        e.preventDefault();
        $('#shipping-tab').click()
    })

    // Shipping
    var shipping = $('#shipping')
    shipping.find(".select2").select2();

    shipping.find('#supplier-btn').click(function(e){
        e.preventDefault();
        $(this).removeClass('btn-dark').addClass('btn-primary')
        $('#shipping-agent-btn').addClass('btn-dark').removeClass('btn-primary')
        $('#shipping-agent').addClass("d-none")
        $('#logistic_type').val('0');
    })

    shipping.find('#shipping-agent-btn').click(function(e){
        e.preventDefault()
        $(this).removeClass('btn-dark').addClass('btn-primary')
        $('#supplier-btn').addClass('btn-dark').removeClass('btn-primary')
        $('#shipping-agent').removeClass("d-none")
        $('#logistic_type').val('1');
    })

    shipping.on('click', '.add-new-btn', function(e){
        e.preventDefault();
        $(this).before('\
            <div class="row mb-2">\
                <div class="col-sm-6">\
                    <select name="" id="shipping_air_type" class="form-control">\
                        <option value="">Standard</option>\
                        <option value="">Express</option>\
                    </select>\
                </div>\
                <div class="col-sm-3 px-1">\
                    <div class="input-group flex-nowrap">\
                        <div class="input-group-prepend">\
                            <span class="input-group-text bg-transparent pr-1">$</span>\
                        </div>\
                        <input type="text" id="shipping_air_price" class="form-control border-left-0 pl-1" style="padding-top: 0.57rem;">\
                    </div>\
                </div>\
                <div class="col-sm-3 px-1">\
                    <select name="" id="shipping_air_messure_in" class="form-control">\
                        <option value="">KG</option>\
                        <option value="">LBS</option>\
                        <option value="">CBM</option>\
                    </select>\
                </div>\
            </div>\
        ');
    });

    shipping.on('click', '.save', function(e){
        e.preventDefault();
        // shipping.find('form:nth-child(1)').addClass('d-none')
        // shipping.find('form:nth-child(2)').removeClass('d-none')
    })

    shipping.on('click', '.save-next', function(e){
        e.preventDefault();
        //$('#payment-terms-tab').click()
    })

    // Payment Terms
    var payment_terms = $('#payment-terms')
    payment_terms.on('click', '.save', function(e){
        e.preventDefault();
    })

    payment_terms.on('click', '.save-next', function(e){
        e.preventDefault();
    })

});
